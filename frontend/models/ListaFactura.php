<?php
namespace frontend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;
//use backend\models\Compra;//Prueba

	class ListaFactura
	{
		public static function getContenidoFactura() {
			if(is_string(Yii::$app->session->get('fact'))){
				return JSON::decode(Yii::$app->session->get('fact'), true);
			}
			else
				return Yii::$app->session->get('fact');
		}

		public static function setContenidoFactura($fact) {
			return Yii::$app->session->set('fact', JSON::encode($fact));
		}
		
		//----------------------------------------------------------//Estas 2 funciones son unicas para obtener el total desde el index de cabeza-caja
		/*public static function getTotalauxiliar() {
			return Yii::$app->session->get('totalu');
		}

		public static function setTotalauxiliar($totalu) {
			return Yii::$app->session->set('totalu', $totalu);
		}
		*/
	
	}
