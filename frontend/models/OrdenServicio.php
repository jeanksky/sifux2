<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_orden_servicio".
 *
 * @property integer $idOrdenServicio
 * @property string $fecha
 * @property double $montoServicio
 * @property integer $idCliente
 * @property integer $placa
 * @property integer $idMecanico
 * @property string $radio
 * @property string $alfombra
 * @property string $herramienta
 * @property string $repuesto
 * @property integer $cantOdometro
 * @property string $cantCombustible
 * @property string $observaciones
 * @property string $observacionesServicio
 * @property integer $estado
 * @property string $fechaCierre
 *
 * @property TblDetalleOrdServicio[] $tblDetalleOrdServicios
 * @property TblVehiculos $placa0
 */
class OrdenServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'tbl_orden_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'fechaCierre'], 'safe'],
            [['montoServicio'], 'number'],
            [['idCliente', 'placa', 'idMecanico', 'cantOdometro', 'estado'], 'integer'],
            [['placa', 'idMecanico', 'radio', 'alfombra', 'herramienta', 'repuesto', 'cantOdometro', 'cantCombustible'], 'required'],
            [['radio', 'alfombra', 'herramienta', 'repuesto'], 'string', 'max' => 2],
            [['cantCombustible'], 'string', 'max' => 6],
            [['observaciones'], 'string', 'max' => 150],
            [['observacionesServicio'], 'string', 'max' => 5000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idOrdenServicio' => 'Id Orden Servicio',
            'fecha' => 'Fecha',
            'montoServicio' => 'Monto Servicio',
            'idCliente' => 'Id Cliente',
            'placa' => 'Placa',
            'idMecanico' => 'Id Mecanico',
            'radio' => 'Radio',
            'alfombra' => 'Alfombra',
            'herramienta' => 'Herramienta',
            'repuesto' => 'Repuesto',
            'cantOdometro' => 'Cant Odometro',
            'cantCombustible' => 'Cant Combustible',
            'observaciones' => 'Observaciones',
            'observacionesServicio' => 'Observaciones Servicio',
            'estado' => 'Estado',
            'fechaCierre' => 'Fecha Cierre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getTblDetalleOrdServicios()
    {
        return $this->hasMany(DetalleOrdServicio::className(), ['idOrdenServicio' => 'idOrdenServicio'])
        ->viaTable('tbl_orden_servicio', ['idOrdenServicio' => 'idOrdenServicio']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaca0()
    {
        return $this->hasOne(TblVehiculos::className(), ['placa' => 'placa']);
    }
}
