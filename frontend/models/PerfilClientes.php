<?php

namespace frontend\models;


use Yii;
use common\models\User;

/**
 * This is the model class for table "tbl_clientes".
 *
 * @property integer $idCliente
 * @property string $tipoIdentidad
 * @property string $nombreCompleto
 * @property string $identificacion
 * @property string $genero
 * @property string $fechNacimiento
 * @property string $telefono
 * @property string $fax
 * @property string $celular
 * @property string $email
 * @property string $direccion
 * @property string $credito
 * @property string $documentoGarantia
 * @property string $diasCredito
 * @property string $autorizacion
 * @property string $montoMaximoCredito
 * @property string $montoTotalEjecutado
 * @property string $estadoCuenta
 */
class PerfilClientes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_clientes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipoIdentidad', 'nombreCompleto', 'identificacion', 'telefono', 'celular', 'direccion', 'credito'], 'required'],
            [['fechNacimiento'], 'safe'],
            [['montoMaximoCredito', 'montoTotalEjecutado'], 'number'],
            [['tipoIdentidad'], 'string', 'max' => 9],
            [['nombreCompleto'], 'string', 'max' => 80],
            [['identificacion'], 'string', 'max' => 20],
            [['genero', 'telefono', 'fax', 'celular', 'estadoCuenta'], 'string', 'max' => 10],
            [['email'], 'string', 'max' => 30],
            [['direccion'], 'string', 'max' => 130],
            [['credito', 'documentoGarantia', 'diasCredito', 'autorizacion'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCliente' => 'Código',
            'tipoIdentidad' => 'Tipo entidad',
            'nombreCompleto' => 'Propietario',
            'identificacion' => 'Identificación',
            'genero' => 'Género',
            'fechNacimiento' => 'Fecha de nacimiento',
            'telefono' => 'Teléfono',
            'fax' => 'Fax',
            'celular' => 'Celular',
            'email' => 'E-mail',
            'direccion' => 'Dirección',
            'credito' => 'Crédito',
            'documentoGarantia' => 'Documento garantía',
            'diasCredito' => 'Días de crédito',
            'autorizacion' => 'Autorización de facturas vencidas ',
            'montoMaximoCredito' => 'Monto máximo crédito',
            'montoTotalEjecutado' => 'Monto total ejecutado',
            'estadoCuenta' => 'Estado cuenta',
        ];
    }

     public function beforeSave($insert)
        {
            if (parent::beforeSave($insert)&&($this->fechNacimiento != '1970-01-01')) {
              return $this->fechNacimiento = date('Y-m-d', strtotime($this->fechNacimiento));
             
            } else {
                return  $this->fechNacimiento = '0000-00-00'; 
                 
            }
        }
        
    public function afterFind()
      { 

      // $this->fechNacimiento = date('d-m-Y', strtotime( $this->fechNacimiento));
      //  parent::afterFind();

        if ($this->tipoIdentidad === 'Física') {

             $this->fechNacimiento = date('d-m-Y', strtotime( $this->fechNacimiento));
             return parent::afterFind();
        
        } else {
            
             return $this->fechNacimiento = '00-00-0000';
        }

      }

    public static function getCliente()
        {

            $idCliente = User::Cliente();
            
            $sql = "SELECT idCliente FROM tbl_clientes WHERE identificacion = :idC";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":idC",$idCliente);
            $result = $command->queryOne();
             return $result['idCliente'];
        }


        public static function getName()
        {

            $idCliente = User::Cliente();
            
            $sql = "SELECT nombreCompleto FROM tbl_clientes WHERE identificacion = :idC";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":idC",$idCliente);
            $result = $command->queryOne();
             return $result['nombreCompleto'];
        }

}
