<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\EncabezadoFactura;

/**
 * EncabezadoFacturaSearch represents the model behind the search form about `frontend\models\EncabezadoFactura`.
 */
class EncabezadoFacturaSearch extends EncabezadoFactura
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCabeza_Factura', 'idOrdenServicio', 'codigoVendedor'], 'integer'],
            [['fecha_inicio', 'fecha_final', 'idCliente', 'estadoFactura', 'tipoFacturacion'], 'safe'],
            [['porc_descuento', 'iva', 'total_a_pagar', 'subtotal'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EncabezadoFactura::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCabeza_Factura' => $this->idCabeza_Factura,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_final' => $this->fecha_final,
            'idOrdenServicio' => $this->idOrdenServicio,
            'porc_descuento' => $this->porc_descuento,
            'iva' => $this->iva,
            'total_a_pagar' => $this->total_a_pagar,
            'codigoVendedor' => $this->codigoVendedor,
            'subtotal' => $this->subtotal,
        ]);

        $query->andFilterWhere(['like', 'idCliente', $this->idCliente])
            ->andFilterWhere(['like', 'estadoFactura', $this->estadoFactura])
            ->andFilterWhere(['like', 'tipoFacturacion', $this->tipoFacturacion]);

        return $dataProvider;
    }
}
