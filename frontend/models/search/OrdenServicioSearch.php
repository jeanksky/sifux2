<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\OrdenServicio;

/**
 * OrdenServicioSearch represents the model behind the search form about `frontend\models\OrdenServicio`.
 */
class OrdenServicioSearch extends OrdenServicio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrdenServicio', 'idCliente', 'placa', 'idMecanico', 'cantOdometro', 'estado'], 'integer'],
            [['fecha', 'radio', 'alfombra', 'herramienta', 'repuesto', 'cantCombustible', 'observaciones', 'observacionesServicio', 'fechaCierre'], 'safe'],
            [['montoServicio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenServicio::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idOrdenServicio' => $this->idOrdenServicio,
            'fecha' => $this->fecha,
            'montoServicio' => $this->montoServicio,
            'idCliente' => $this->idCliente,
            'placa' => $this->placa,
            'idMecanico' => $this->idMecanico,
            'cantOdometro' => $this->cantOdometro,
            'estado' => $this->estado,
            'fechaCierre' => $this->fechaCierre,
        ]);

        $query->andFilterWhere(['like', 'radio', $this->radio])
            ->andFilterWhere(['like', 'alfombra', $this->alfombra])
            ->andFilterWhere(['like', 'herramienta', $this->herramienta])
            ->andFilterWhere(['like', 'repuesto', $this->repuesto])
            ->andFilterWhere(['like', 'cantCombustible', $this->cantCombustible])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'observacionesServicio', $this->observacionesServicio]);

        return $dataProvider;
    }
}
