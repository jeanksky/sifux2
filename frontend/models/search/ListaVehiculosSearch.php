<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\ListaVehiculos;
use frontend\models\PerfilClientes;

/**
 * ListaVehiculosSearch represents the model behind the search form about `frontend\models\ListaVehiculos`.
 */
class ListaVehiculosSearch extends ListaVehiculos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['placa', 'idCliente', 'anio'], 'integer'],
            [['modelo', 'marca', 'combustible', 'motor', 'cilindrada_motor', 'version'], 'safe'],
            [['tbl_marcas.nombre_marca', 'tbl_modelo.nombre_modelo'], 'safe'], //para traer datos de la tabla tbl_clientes
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $idcliente = PerfilClientes::getCliente();
        $query = ListaVehiculos::find()->where(['=','idCliente',$idcliente]);
        //------------------------------------------------
        //para comunicar tbl_vehiculos con tbl_marcas
        $query->leftJoin([
        'tbl_marcas'
      ], 'tbl_marcas.codMarca = tbl_vehiculos.marca');
        //------------------------------------------------
        //para comunicar tbl_vehiculos con tbl_modelo
        $query->leftJoin([
        'tbl_modelo'
      ], 'tbl_modelo.codModelo = tbl_vehiculos.modelo');
        //------------------------------------------------
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'placa' => $this->placa,
            'idCliente' => $this->idCliente,
            'anio' => $this->anio,
        ]);

        $query->andFilterWhere(['like', 'modelo', $this->modelo])
            ->andFilterWhere(['like', 'marca', $this->marca])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'motor', $this->motor])
            ->andFilterWhere(['like', 'cilindrada_motor', $this->cilindrada_motor])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'tbl_marcas.nombre_marca', $this->getAttribute('tbl_marcas.nombre_marca')])//para busquedas de la tabla tbl_marcas
            ->andFilterWhere(['like', 'tbl_modelo.nombre_modelo', $this->getAttribute('tbl_modelo.nombre_modelo')]);//para busquedas de la tabla tbl_modelo

        return $dataProvider;
    }
}
