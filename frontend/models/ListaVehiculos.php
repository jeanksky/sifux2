<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_vehiculos".
 *
 * @property integer $placa
 * @property integer $idCliente
 * @property integer $anio
 * @property string $modelo
 * @property string $marca
 * @property string $combustible
 * @property string $motor
 * @property string $cilindrada_motor
 * @property string $version
 *
 * @property TblOrdenServicio[] $tblOrdenServicios
 */
class ListaVehiculos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_vehiculos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['placa', 'anio', 'modelo', 'marca', 'combustible', 'motor', 'cilindrada_motor', 'version'], 'required'],
            [['placa', 'idCliente', 'anio'], 'integer'],
            [['modelo', 'marca'], 'string', 'max' => 80],
            [['combustible'], 'string', 'max' => 20],
            [['motor'], 'string', 'max' => 50],
            [['cilindrada_motor', 'version'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'placa' => 'Placa',
            'idCliente' => 'Id Cliente',
            'anio' => 'Año',
            'modelo' => 'Modelo',
            'marca' => 'Marca',
            'combustible' => 'Combustible',
            'motor' => 'Motor',
            'cilindrada_motor' => 'Cilindrada Motor',
            'version' => 'Version',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblOrdenServicios()
    {
        return $this->hasMany(TblOrdenServicio::className(), ['placa' => 'placa']);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_marcas.nombre_marca','tbl_modelo.nombre_modelo']);
    }
}
