<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Html;
use frontend\models\Facturas;
use frontend\models\search\FacturasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Empresa;
use backend\models\Compra_view;//factura para la vista
use backend\models\Clientes;//para obtener el cliente
use backend\models\Funcionario;//para obtener Funcionario
use backend\models\FormasPago;//para obtener FormasPago
use backend\models\DetalleFacturas;//para obtener DetalleFacturas
use backend\models\ProductoServicios;//para obtener ProductoServicios

/**
 * FacturasController implements the CRUD actions for Facturas model.
 */
class FacturasController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Facturas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacturasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Facturas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Facturas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Facturas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Facturas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Facturas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Facturas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Facturas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Facturas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    //accion que me imprime para hoja normal de carta
    public function actionReport($id) {

        $empresaimagen = new Empresa();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();

      //  $id = Compra_view::getCabezaFactura();
        $factura = $this->findModel($id);
        $content = '
        <div class="panel panel-default">
            <div class="panel-body">
                <table>
                    <tbody>
                    <tr>
                    <td>
                    <img src="'. $empresaimagen->getImageurl() .'" style="height: 120px;">
                    </td>
                    <td style="text-align: left;">
                    <strong>'.$empresa->nombre.'</strong><br>
                    <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                    <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                    <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                    <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                    <strong>Dirección:</strong> '.$empresa->email.'
                    </td>
                    <tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Factura N°: '.$id. '<br>Fecha: '.$factura->fecha_inicio.'</h4>';

        $content .= $this->renderAjax('view',['model' => $factura]);
       /* $content = $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);*/

       $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Factura</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";

                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';

        echo $imprimir;

        Compra_view::setCabezaFactura(null);
    }//fin actionReport

    //esta funsion me imprime el footer con los valores que necesito darle a los botones
    public function actionFooter(){
        if(Yii::$app->request->isAjax){
            $id = strval(Yii::$app->request->post('id'));//obtengo el valor de la factura para poder enviarlo en la accion del boton
            $footer = '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir completo', ['/facturas/report','id'=>$id], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Imprime factura completa'
                ]);
            echo $footer;
        }
    }//fin action footer
}
