<?php
namespace frontend\controllers;

use Yii;
use common\models\AccessHelpers;
use common\models\LoginFormc;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use yii\helpers\Url;
use backend\models\Empresa;
//-----------Renovar password---------------------
use yii\web\Session;
use frontend\config\param;
use common\models\FormRecoverPass;
use common\models\FormResetPass;
use kartik\widgets\Growl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error' , 'recoverpass', 'resetpass'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','changepassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginFormc();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    // public function actionChangepassword()
    // {
    //     $model = new User;
    //     $modeluser = User::find()->where([
    //         'username'=>Yii::$app->user->identity->username])->one();

    //     if($model->load(Yii::$app->request->post())){
    //         if($model->validate()){
    //             try{
    //                 //Encriptar el contraseña
    //                  $modeluser->password = $modeluser->newpass;
    //                  // $modeluser->updatePassword($model->newpass);
    //                    // $model->password_hash = $model->setPassword($model->newpass);
    //                    // $model->auth_key = $model->generateAuthKey();

    //                  if($modeluser->save()){
    //                     Yii::$app->getSession()->setFlash(
    //                         'success','<span class="glyphicon glyphicon-ok-sign">
    //                         </span> <strong>Éxito!</strong><br><br>La contraseña ha sido cambiada.'
    //                     );
    //                     return $this->render('changepassword',['model'=>$model]);
    //                 }else{
    //                     Yii::$app->getSession()->setFlash(
    //                         'error','<span class="glyphicon glyphicon-warning-sign">
    //                         </span> <strong>Error!</strong><br><br>La contraseña no ha sido cambiada.'
    //                     );
    //                     return $this->render('changepassword',['model'=>$model]);
    //                 }
    //             }catch(Exception $e){
    //                 Yii::$app->getSession()->setFlash(
    //                     'error',"{$e->getMessage()}"
    //                 );
    //                 return $this->render('changepassword',['model'=>$model]);
    //             }
    //         }else{
    //             return $this->render('changepassword',[
    //                 'model'=>$model
    //             ]);
    //         }
    //     }else{
    //         return $this->render('changepassword',[
    //             'model'=>$model
    //         ]);
    //     }
    // }

     public function actionChangepassword()
    {
        $u = new User();
        $model = User::find()->where([
            'username'=>Yii::$app->user->identity->username])->one();

             if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                    //Encriptación  de contraseña
                    $model->verification_code = $_POST['User']['newpass'];
                    $model->password_hash = Yii::$app->security->generatePasswordHash($model->verification_code);
                    $model->auth_key = $u->generateAuthKey2();

                    if($model->save()){
                        Yii::$app->getSession()->setFlash(
                            'success','<span class="glyphicon glyphicon-ok-sign">
                            </span> <strong>Éxito!</strong><br><br>La contraseña ha sido cambiada.'
                        );
                        return $this->redirect(['changepassword', 'model' => $model]);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','<span class="glyphicon glyphicon-warning-sign">
                            </span> <strong>Error!</strong><br><br>La contraseña no ha sido cambiada.'
                        );
                        return $this->render('changepassword',['model'=>$model]);
                    }

                    }else{
                        return $this->render('changepassword',[
                            'model'=>$model
                        ]);
                    }
    }

   /* public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $operacion = str_replace("/", "-", Yii::$app->controller->route);

        //Se quita 'site-contact'
        $permitirSiempre = ['site-captcha', 'site-signup', 'site-index', 'site-recoverpass', 'site-resetpass', 'site-changepassword', 'site-error', 'site-about', 'site-login', 'site-logout'];

        if (in_array($operacion, $permitirSiempre)) {
            return true;
        }

        if (!AccessHelpers::getAcceso($operacion)) {
            echo $this->render('/site/nopermitido');
            return false;
        }

        return true;
    }*/

    //---------------------------renovación de passwrd------------------------
    public function actionRecoverpass()
    {
         //Instancia para validar el formulario
         $model = new FormRecoverPass;

         //Mensaje que será mostrado al usuario en la vista
         $msg = null;

         if ($model->load(Yii::$app->request->post()))
         {
          if ($model->validate())
          {
           //Buscar al usuario a través del email
           $table = User::find()->where("email=:email", [":email" => $model->email]);

           //Si el usuario existe
           if ($table->count() == 1)
           {
            //Crear variables de sesión para limitar el tiempo de restablecido del password
            //hasta que el navegador se cierre
            $session = new Session;
            $session->open();

            //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
            $session["recover"] = $this->randKey("abcdef0123456789", 200);
            $recover = $session["recover"];

            //También almacenaremos el id del usuario en una variable de sesión
            //El id del usuario es requerido para generar la consulta a la tabla users y
            //restablecer el password del usuario
            $table = User::find()->where("email=:email", [":email" => $model->email])->one();
            $session["id_recover"] = $table->id;

            //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario
            //para que lo introduzca en un campo del formulario de reseteado
            //Es guardada en el registro correspondiente de la tabla users
            $verification_code = $this->randKey("abcdef0123456789", 8);
            //Columna verification_code
            //$table->verification_code = $verification_code;
            //Guardamos los cambios en la tabla users
            $email = $model->email;
            $sql = "UPDATE user SET verification_code = :verification_code WHERE email = :email";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":verification_code", $verification_code);
                        $command->bindParam(":email", $email);


            if ($command->execute())
            {
            //Creamos el mensaje que será enviado a la cuenta de correo del usuario

            $subject = "Recuperación de clave de acceso";
            $content= '
            <!---->

            <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
            <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">

            <br>
            <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
              <tbody><tr>
                <td align="center" valign="top">
                  <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                    <tbody><tr>
                        <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                          <table>
                            <tbody><tr>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                                &nbsp;&nbsp;&nbsp;&nbsp;

                              </td>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                       <br> <a ><img width="180" height=""src="'.Url::base(true).'/img/logo_sistema.png" class="CToWUd"></a>
                        <br><br><br>
                        <p style="color:#0e4595;font-family: Segoe UI;font-size:22px;line-height:1.5;margin:0 0 25px;text-align:center!important" align="center"><strong>Recuperar clave de acceso a lutuX</strong> </p><br>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left"> Estimado(a) usuario, accede a lutuX y crea una nueva contraseña.</p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Hemos generado el siguiente código de verificación con el que podrás restablecer tu contraseña.</p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Código de verificación: <strong>'.$verification_code.'</strong></p>
            <br>
            <div style="text-align:center!important" align="center"><a href="'.Url::home(true).'?r=site%2Fresetpass" rel="nofollow" style="background:#5CB85C;border:1px solid rgba(92, 184, 92);border-radius:5px;color:#fff;display:inline-block;font-size:13px;font-weight:bold;min-height:20px;padding:9px 15px;text-decoration:none" target="_blank">CREAR NUEVA CONTRASEÑA</a></div>
            <br><br><br><br>

                      </td>
                    </tr>
                  </tbody></table>
                </td>
              </tr>
            </tbody></table>
            <br></div></div><div class="adL">

            </div></div></div>


            <!---->
            ';
           // $content .= "<p>Copie el siguiente código de verificación para restablecer su password ... ";
           // $content .= "<strong>".$verification_code."</strong></p>";
           // $content .= "<p><a href='http://localhost/lutux2/backend/web/index.php?r=site%2Fresetpass'>Recuperar password</a></p>";
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            //Enviamos el correo
            Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
            ->setTo($model->email)
            //->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
            ->setFrom($empresa->email_notificaciones)
            ->setSubject($subject)
            ->setHtmlBody($content)
            ->send();

            //Vaciar el campo del formulario
            $model->email = null;

            //Mostrar el mensaje al usuario
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su contraseña.');
            //$msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su password";
            }
            else {
              Yii::$app->getSession()->setFlash('error', '<span class="fa fa-exclamation-triangle fa-4x"></span> <strong>Error!</strong><br><br> Tenemos un problema, vuelva a intentarlo más tarde, gracias.');
            }
           }
           else //El usuario no existe
           {
            Yii::$app->getSession()->setFlash('error', '<span class="fa fa-exclamation-triangle fa-4x"></span> <strong>Error!</strong><br><br> El correo electrónico que ha ingresado no corresponde a ningun usuarios registrado.');
           }
          }
          else
          {
           $model->getErrors();
          }
         }
         //Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
         return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
    }

    //Accion para resetear el password y envial info al correo
    public function actionResetpass()
    {
         //Instancia para validar el formulario
         $model = new FormResetPass;

         //Mensaje que será mostrado al usuario
         $msg = null;

         //Abrimos la sesión
         $session = new Session;
         $session->open();

         //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
         if (empty($session["recover"]) || empty($session["id_recover"]))
         {
          return $this->redirect(["site/index"]);
         }
         else
         {

          $recover = $session["recover"];
          //El valor de esta variable de sesión la cargamos en el campo recover del formulario
          $model->recover = $recover;

          //Esta variable contiene el id del usuario que solicitó restablecer el password
          //La utilizaremos para realizar la consulta a la tabla users
          $id_recover = $session["id_recover"];

         }

         //Si el formulario es enviado para resetear el password
         if ($model->load(Yii::$app->request->post()))
         {
          if ($model->validate())
          {
           //Si el valor de la variable de sesión recover es correcta
           if ($recover == $model->recover)
           {
            //Preparamos la consulta para resetear el password, requerimos el email, el id
            //del usuario que fue guardado en una variable de session y el código de verificación
            //que fue enviado en el correo al usuario y que fue guardado en el registro
            //$table2 = User::findOne(["email" => $model->email, "id" => $id_recover, "verification_code" => $model->verification_code]);
            //$table2 = User::findOne(["id" => $id_recover]);
            //$table2 = User::find()->where("email=:email, id=:id, verification_code=:code", [":email" => $model->email, ":id" => $id_recover, ":code" => $model->verification_code])->one();
            $u = new User();
            //Encriptar el password
            //  $table->password = crypt($model->password, Yii::$app->params["salt"]);
            $password_hash = $u->setPassword2($model->password);
            $auth_key = $u->generateAuthKey2();
            //$u->setPassword($model->password);
            //$u->generateAuthKey();

            $email = $model->email;
            $verification_code = $model->verification_code;
            $sql = "UPDATE user SET password_hash = :password_hash, auth_key = :auth_key WHERE email = :email AND id = :id_recover AND verification_code = :verification_code";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":password_hash", $password_hash);
                        $command->bindParam(":auth_key", $auth_key);
                        $command->bindParam(":id_recover", $id_recover);
                        $command->bindParam(":email", $email);
                        $command->bindParam(":verification_code", $verification_code);


            //Si la actualización se lleva a cabo correctamente
            if ($command->execute())
            {


            //------Envio email y password por email------------
            //http://www.yiiframework.com/doc-2.0/guide-tutorial-mailing.html
            //Creamos el mensaje que será enviado a la cuenta de correo del usuario
            $subject = "Modificación de contraseña exitosa";

            $content = '

            <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
            <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">

            <br>
            <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
              <tbody><tr>
                <td align="center" valign="top">
                  <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                    <tbody><tr>
                        <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                          <table>
                            <tbody><tr>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                                &nbsp;&nbsp;&nbsp;&nbsp;

                              </td>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                        <br><a ><img width="180" height=""src="'.Url::base(true).'/img/logo_sistema.png" class="CToWUd"></a>
                        <br><br>
                        <p style="color:#0e4595;font-family: Segoe UI;font-size:22px;line-height:1.5;margin:0 0 25px;text-align:center!important" align="center"><strong></strong> </p><br>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Estimado(a) usuario, tu contraseña ha sido modificada exitosamente.</p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Ya puedes iniciar sesión a nuestro sistema con tu nueva contraseña.<br>Contraseña: <strong>'.$model->password.'</strong></p>
            <br>
            <div style="text-align:center!important" align="center"><a href="'.Url::home(true).'" rel="nofollow" style="background:#5CB85C;border:1px solid rgba(92, 184, 92);border-radius:5px;color:#fff;display:inline-block;font-size:13px;font-weight:bold;min-height:20px;padding:9px 15px;text-decoration:none" target="_blank">INICIAR SESIÓN</a></div>
            <br><br><br><br>

                      </td>
                    </tr>
                  </tbody></table>
                </td>
              </tr>
            </tbody></table>
            <br></div></div><div class="adL">

            </div></div></div>


            ';

            //$content = "<p>Excelente se ha modificado su contraseña. Los siguientes datos se les han sido proporcionado:</p>";
            //$content .= "<p><strong>Contraseña: </strong>".$model->password."</p>";
            //$content .= "<p>Ya puedes iniciar sesión con tu nueva contraseña.</p>";
            //$content .= "<p><a href='http://localhost/ProyectosNelux/aplicaciones_web/lutux/backend/web/index.php'>Iniciar sesión</a></p>";
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            //Enviamos el correo
            Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
            ->setTo($model->email)
            ->setFrom($empresa->email_notificaciones)
            ->setSubject($subject)
            ->setHtmlBody($content)
            ->send();
            //------------------------------------------



             //Destruir las variables de sesión
             $session->destroy();

             //Vaciar los campos del formulario
             $model->email = null;
             $model->password = null;
             $model->password_repeat = null;
             $model->recover = null;
             $model->verification_code = null;

              Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La contraseña se ha actualizado correctamente.');
              echo "<meta http-equiv='refresh' content='3; ".Url::toRoute("site/login")."'>";
             //$msg = "Enhorabuena, contraseña actualizado correctamente, redireccionando a la página de login ...";
             $msg .= "<meta http-equiv='refresh' content='5;url='/>";
            // $msg .= header('Refresh:5;url='. $this->redirect(''));
             //$msg .= "<meta http-equiv='refresh' content='5; ".Url::toRoute("site/login")."'>";
            }
            else
            {
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Ha ocurrido un error, compruebe que su código de verificación sea el correcto.');

             //$msg = "Ha ocurrido un error";
            }

           }
           else
           {
            $model->getErrors();
           }
          }
         }

         return $this->render("resetpass", ["model" => $model, "msg" => $msg]);

    }//fin actionResetpass()

    //Funcion para password aleatorio
    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

}
