<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ListaVehiculos;
use frontend\models\ListaFactura;
use frontend\models\search\ListaVehiculosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OrdenServicio;

/**
 * ListaVehiculosController implements the CRUD actions for ListaVehiculos model.
 */
class ListaVehiculosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ListaVehiculos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ListaVehiculosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ListaVehiculos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        //Definimos c como null para comprobación
        $c=null;
        ListaFactura::setContenidoFactura(null);
        $facturas = ListaFactura::getContenidoFactura();

        //Me obtiene los datos del encabezado correspondiente
        $query = new yii\db\Query();
        $data = $query->select(['estadoFactura','idCabeza_Factura','idOrdenServicio','fecha_inicio','fecha_final','subtotal','porc_descuento','iva','total_a_pagar'])
             ->from('tbl_encabezado_factura')
             ->where(['=','idCliente', $model->idCliente])
             ->andWhere(['!=','idOrdenServicio', ''])
             ->distinct()
             ->all();
        //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
        foreach($data as $row){
            $estadoFactura = $row['estadoFactura'];
            $idCabeza_Factura = $row['idCabeza_Factura'];
            $fecha_inicio = $row['fecha_inicio'];
            $fecha_final = $row['fecha_final'];
            $subtotal = $row['subtotal'];
            $porc_descuento = $row['porc_descuento'];
            $iva = $row['iva'];
            $total_a_pagar = $row['total_a_pagar'];
            $estadoFactura = $row['estadoFactura'];
            $idOrdenServicio = $row['idOrdenServicio'];

            $Orden = OrdenServicio::find()->where(['idOrdenServicio'=>$idOrdenServicio])->one();

            if($Orden->placa==$model->placa)
            {
                $_POST['idOrdenServicio']= $idOrdenServicio;
                $_POST['estadoFactura']= $estadoFactura;
                $_POST['idCabeza_Factura']= $idCabeza_Factura;
                $_POST['fecha_inicio']= $fecha_inicio;
                $_POST['fecha_final']= $fecha_final;
                $_POST['subtotal']= $subtotal;
                $_POST['porc_descuento']= $porc_descuento;
                $_POST['iva']= $iva;
                $_POST['total_a_pagar']= $total_a_pagar;

                $facturas[] = $_POST;;
            }
            ListaFactura::setContenidoFactura($facturas);
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ListaVehiculos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ListaVehiculos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->placa]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ListaVehiculos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->placa]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ListaVehiculos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ListaVehiculos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ListaVehiculos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ListaVehiculos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
