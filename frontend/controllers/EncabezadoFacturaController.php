<?php

namespace frontend\controllers;

use Yii;
use frontend\models\EncabezadoFactura;
use frontend\models\search\EncabezadoFacturaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Compra_update;
use backend\models\Funcionario;
use backend\models\ProductoServicios;

/**
 * EncabezadoFacturaController implements the CRUD actions for EncabezadoFactura model.
 */
class EncabezadoFacturaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EncabezadoFactura models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EncabezadoFacturaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EncabezadoFactura model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $submit = false)
    {
        $model = $this->findModel($id);
        $c=null;

        Compra_update::setContenidoFactura(null);
        //Abrimos el contenido
        $compra = Compra_update::getContenidoFactura();

        //Me obtiene los datos del detalle del encabezado correspondiente
                $query = new yii\db\Query();
                $data = $query->select(['codProdServicio','cantidad','precio_unitario','precio_por_cantidad', 'descuento_producto', 'subtotal_iva'])
                    ->from('tbl_detalle_facturas')
                    ->where(['=','idCabeza_factura', $model->idCabeza_Factura])
                    ->distinct()
                    ->all();
        //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
                foreach($data as $row){
                    $codProdServicio = $row['codProdServicio'];
                    $cantidad = $row['cantidad'];
                    $precio_unitario = $row['precio_unitario'];
                    $descuento = $row['descuento_producto'];
                    $iva = $row['subtotal_iva'];
                   if ($codProdServicio>=0) { 
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        if($c==null)
                        {   
                            $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                            $_POST['cantidad']= $codProdServicio != "0" ? $cantidad : 1;//por una extraña razón la cantidad de orden de servicio se incrementa al agregar un producto, por eso le agrego forzadamente cantidad 1 todas las veces.
                            $_POST['cod']= $codProdServicio;
                            $_POST['desc']= $codProdServicio != "0" ? $modelProd->nombreProductoServicio : 'Orden de Servicio';
                            $_POST['precio']= $precio_unitario;
                            //$_POST['dcto']= ?;//recordar que esto se cambia
                            $_POST['dcto'] = $descuento;
                            //Iva en forma manual, se puede cargar de la DB si se desea
                            $_POST['iva'] = $iva;
                            
                            $compra[] = $_POST;
                            //echo $model->nombreProductoServicio;
                        }
                        //Guardamos el contenido    
                        Compra_update::setContenidoFactura($compra);
                    }
                }

        return $this->renderAjax('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new EncabezadoFactura model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EncabezadoFactura();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EncabezadoFactura model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EncabezadoFactura model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EncabezadoFactura model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EncabezadoFactura the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EncabezadoFactura::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
