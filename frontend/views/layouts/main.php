<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\checkbox\CheckboxX;
use kartik\icons\Icon;
//use backend\widgets\Alert;
use kartik\widgets\AlertBlock;
use kartik\alert\Growl;
use kartik\alert\Alert;
use common\models\User;
use frontend\models\PerfilClientes;
//Icon::map($this);
/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);
//http://www.yiiframework.com/extension/yii2-backuprestore/ para respaldo de base de datos
?>
<?php $this->beginPage() ?>
<?php $empresa = new User(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<link rel="shortcut icon" href="<?php echo $empresa->getFavicon()?>" type="image/x-icon" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="dist/js/bootstrap-checkbox.min.js" defer></script>
    <style type="text/css">

    #semitransparent {
    padding: 10px;
    z-index: 0;
    background-color: white;
    opacity: 0.9;
    -moz-opacity: 0.9;
    -khtml-opacity: 0.90;
    /* filter: alpha(opacity=30);*/
    /*filter:progid:DXImageTransform.Microsoft.Alpha(opacity=30);*/
    border-style: double;
    border-color: #efefef;
    }

        body
        {
       /*  margin: 0;
         padding: 0;
         color: #555;
         font: normal 10pt Arial,Helvetica,sans-serif;*/
         /*background: black; */
         background-image: url(../images/bg.jpg);
         width: 100%;
         background-attachment: fixed;
        /* background-image: url(../images/background2.jpg);
         -webkit-background-size: cover;
         -moz-background-size: cover;
         -o-background-size: cover;
         background-size: cover;

         background-attachment: fixed;
         background-repeat: no-repeat;
        background-position: center center;*/

        }

        /*color de fondo a la barra menu */
        .navbar-inverse {
            background-color: #4169e1;
            border-color: #156DFC;
        }

        /*color letra del menú sin seleccionar*/
        .navbar-inverse .navbar-nav > li > a {
          color: #FFFFFF;
        }

        /*color menu sin seleccionar */
        .navbar-inverse .navbar-nav > .open > a,
        .navbar-inverse .navbar-nav > .open > a:hover,
        .navbar-inverse .navbar-nav > .open > a:focus {
          color: #000000;
          background-color: #156DFC;
        }

        /*color menu seleccionado */
        .navbar-inverse .navbar-nav > .active > a,
        .navbar-inverse .navbar-nav > .active > a:hover,
        .navbar-inverse .navbar-nav > .active > a:focus {
          color: #BEEDFB;
          background-color: #156DFC;
        }

        .navbar-brand {
          float: left;
          height: 50px;
          padding: 0px 0px;
          font-size: 18px;
          line-height: 20px;
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            height: 60px;
            width: 100%;
            /* border-color: #020202;*/
           /* background-color: #4169E1; */
            background-color: #efefef;
        }
    </style>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php

        $cliente = PerfilClientes::getCliente();
        $name = PerfilClientes::getName();
           // public $submenuTemplate  = '\n<ul>\n{items}\n</ul>\n';
            $admin = (isset(Yii::$app->user->identity->tipo_usuario) and Yii::$app->user->identity->tipo_usuario == 'Administrador') ? true : false ;
    //http://www.bsourcecode.com/yiiframework2/menu-widget-in-yii-framework-2-0/
    //http://www.yiiframework.com/extension/yii2-icons/#hh1
    //'label'=>'<img title="Agregar" src="'.Yii::app()->request->baseUrl.'/images/iconos/agregar.png">'  
            NavBar::begin([
                'brandLabel' => '<img src="../images/LogoMenu.png" margin="-4 sspx" style="text-align:right" height="50" width="">',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
               // ['label' => Icon::show('home') . 'Home', 'url' => ['/site/index']],
                ['label' => '<span class="glyphicon glyphicon-home"></span>', 'url' => ['/site/index']],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => '<span class="glyphicon glyphicon-log-in"></span> Iniciar sesión', 'url' => ['/site/login']];
            } else {

                $menuItems[] = ['label' => '<span class="glyphicon glyphicon-equalizer"></span> Expediente vehicular',
                                    'url' => ['/lista-vehiculos/index'],
                                ];
                $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"></span> Mi perfil',
                 'url' => ['/perfil-clientes/view','id'=>$cliente]];

                $menuItems[] = ['label' => '<span class="glyphicon glyphicon-log-out"></span>',
                                    'url' => ['#'],
                                    'options'=>['class'=>'dropdown'],
                                    'template' => '<a href="{url}" class="href_class">{label}</a>',
                                    'items' => [
                                        ['label' => 'Cerrar sesión [ ' .$name. ' ]',
                                        'url' => ['/site/logout'],
                                        'linkOptions' => ['data-method' => 'post']
                                        ],
                                    ]
                                ];
                /*
                $menuItems[] = [
                    'label' => 'Salir (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];*/
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                'encodeLabels' => false,

                //'submenuTemplate' => '\n<ul class="dropdown-menu" role="menu">\n{items}\n</ul>\n',//Submenú
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
        </div>
    </div>
    <?php
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    ?>
    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; Nelux Professional Services, todos los derechos reservados <?= date('Y') ?></p>
        <p class="pull-right"><?= $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y')//Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
