<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use backend\models\Empresa;
use common\models\User;
use yii\helpers\Url;

$empresa = new Empresa();
$this->title = $empresa->getEmpresa();
?>
<style type="text/css">
    .semitransparent{
    margin: auto;
    background-color: rgba(256, 256, 256, 0.83);
    border-radius: 5px;
    box-shadow: 0px 3px 7px #A5A3A3;
    padding: 10px;
    }

    #bg{
       position:fixed;
       /*top:0;
       left:0;*/
       z-index:-1;
       width: 100%;
       height: -100%;

       top: 0%;
       left: 0px;
    }

</style>

<body>
<video id="bg" alt="background" src="../images/video.mp4" autoplay loop ></video>
<div class="site-index">

    <div class="jumbotron">
    <center>
      <div class="semitransparent">
    <?php $user = new User();?>

    <h2><?php echo $empresa->getEmpresa()?></h2>

        <img src="<?php echo $user->getImageurl() ?>" style="width: 360px;">

        <p class="lead">Bienvenido al Sistema de Control Vehicular</p>
    </div>
        <p>
            <!--a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a-->
            <?php
            if((isset(Yii::$app->user->identity->tipo_usuario) and Yii::$app->user->identity->tipo_usuario == 'Administrador')){
                // echo Html::a('Actualizar información del Negocio',  '../web/index.php?r=empresa%2Fupdate&id=1', ['class'=>'btn btn-success']);
            } else {
                echo "";
            }

            ?>
        </p>
        </center>
        </div>

    <!-- <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div> -->
</div>
</body>
