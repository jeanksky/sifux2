<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\Growl;
use kartik\widgets\AlertBlock;
?>
 
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_ALERT,
            'useSessionFlash' => true,
            'delay' => 3000,
        ]);?>
 
<h1>Restaurar la contraseña</h1>
<legend></legend>
<div class="col-md-12">
<div class="col-md-6">
<p class="text-justify">Para restablecer tu contraseña, introduce tu dirección de correo electrónico que hayas asociado a tu cuenta.</p>
<br>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>
    <div class="form-group">
        <?= $form->field($model, "email")->
 widget(\yii\widgets\MaskedInput::className(), [
    'name' => 'input-36',
    'clientOptions' => [
    'alias' =>  'email'
    ],
])->label("Correo electrónico") ?>    
    </div>

    <?= Html::submitButton("Enviar", ["class" => "btn btn-primary"]) ?>

<?php $form->end() ?>
</div>
</div>