<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Atención!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
    <p>No tiene permiso para acceder a esta página. Si usted necesita el permiso, pongase en contacto con el administrador</p>
    </div>
</div>