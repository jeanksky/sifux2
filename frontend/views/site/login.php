<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Iniciar sesión';
//$this->params['breadcrumbs'][] = $this->title;
?>

<style>

   #center{
       margin: auto;
       width: 400px;
       background-color: rgba(256, 256, 256, 0.83);
       border-radius: 5px;
       box-shadow: 0px 3px 7px #A5A3A3;
       padding: 10px;
   }

   .well {
   min-height: 20px;
   padding: 19px;
   margin-bottom: 20px;
   background-color: #f5f5f5;
   border: 1px solid #e3e3e3;
   border-radius: 4px;
   -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
   box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}
#bg{
   position:fixed;
   /*top:0;
   left:0;*/
   z-index:-1;
   width: 100%;
   height: -100%;

   top: 0%;
   left: 0px;
}



   h2 {
       font-family: 'Segoe UI Light';
       src: url('/surface/Assets/css/fonts/all/light/segoeuil.eot');
       src: url('/surface/Assets/css/fonts/all/light/segoeuil.eot?#iefix') format('embedded-opentype'),
       url('/surface/Assets/css/fonts/all/light/segoeuil.woff') format('woff'),
       url('/surface/Assets/css/fonts/all/light/segoeuil.svg#SegoeUILight') format('svg');
       font-weight: normal;
       font-style: normal;
   }


</style>
<br><br><br>
<body>
<video id="bg" alt="background" src="../images/video.mp4" autoplay loop ></video>
<!--img id="bg" src="../images/background.jpg"  alt="background" /-->

<div class="row">
       <div class="panel panel-default" id="center">
           <div class="legend">
           <center><img src="../images/LogoMenu.png" height="100%" width="100%"></center>
               <legend>
                   <center><h2>Iniciar sesión</h2></center>
               </legend>
           </div>
           <!-- /.panel-heading -->
           <div>
            <div class="form-group">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <!--?= $form->field($model, 'username') ?-->
                <!--input type="text" class="form-control" placeholder="Usuario"-->
                <?= $form->field($model, 'username', [
                   'inputTemplate' => '<div class="form-group input-group"><span class="input-group-addon"><a data-toggle="tab" class="glyphicon glyphicon-user"></a></span>{input}</div>',
                   ])->label('Usuario'); ?>

                   <!--?= $form->field($model, 'password')->passwordInput() ?-->
                   <?= $form->field($model, 'password', [
                      'inputTemplate' => '<div class="form-group input-group"><span class="input-group-addon"><a data-toggle="tab" class="glyphicon glyphicon-lock"></a></span>{input}</div>',
                       ])->passwordInput()->label('Contraseña'); ?>

                   <!--?= $form->field($model, 'rememberMe')->checkbox()->label('Recuérdame') ?-->
               </div>
               <br>

                   <div class="form-group">
                       <?= Html::submitButton('Ingresar', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                   <div style="text-align:right">
                       <!--a href="/site/recoverpass">¿Olvidaste tu contraseña?</a-->
                       <?= Html::a('¿Olvidaste tu contraseña?', ['site/recoverpass'], ['class'=>'']) ?>
                   </div>
                   </div>
               </div>


               <?php ActiveForm::end(); ?>

               <!-- /.panel-body -->
           </div>
           <!-- /.panel -->
   </div></body>
