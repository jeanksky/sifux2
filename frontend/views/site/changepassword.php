<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\PerfilClientes;
use kartik\widgets\AlertBlock;

$cliente = PerfilClientes::getCliente();
$modelCliente = PerfilClientes::find()->where(['idCliente'=>$cliente])->one();
$this->title = 'Cambio de contraseña';
$this->params['breadcrumbs'][] = ['label' => $modelCliente->nombreCompleto, 'url' => ['/perfil-clientes/view', 'id' => $cliente]];
$this->params['breadcrumbs'][] = ['label' => 'Actualizar mis datos', 'url' => ['/perfil-clientes/update', 'id' => $cliente]];
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="site-changepassword">

<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_ALERT,
            'useSessionFlash' => true,
            'delay' => 4000,

        ]);?>

    <h1><?= Html::encode($this->title) ?></h1>
    <legend></legend>
    
    <div class="col-lg-5">
    <p><?= Html::a('Regresar', ['/perfil-clientes/update','id'=>$cliente], ['class' => 'btn btn-default']) ?></p>
    <p>Por favor, rellene los siguientes campos para cambiar la contraseña.</p>
    <br>
    <?php $form = ActiveForm::begin([
        'id'=>'changepassword-form',
        // 'options'=>['class'=>'form-horizontal'],
        // 'fieldConfig'=>[
        //     'template'=>"{label}\n<div class=\"col-lg-3\">
        //                 {input}</div>\n<div class=\"col-lg-5\">
        //                 {error}</div>",
        //     'labelOptions'=>['class'=>'col-lg-3 control-label'],
        // ],
    ]); ?>
        <?= $form->field($model,'oldpass',['inputOptions'=>[
            'placeholder'=>''
        ]])->passwordInput() ?>
        
        <?= $form->field($model,'newpass',['inputOptions'=>[
            'placeholder'=>''
        ]])->passwordInput() ?>
        
        <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
            'placeholder'=>''
        ]])->passwordInput() ?>
        
        <div class="form-group">
        <!-- <div class="col-lg-offset-2 col-lg-11"> -->
                <?= Html::submitButton('Cambiar contraseña',[
                    'class'=>'btn btn-primary'
                ]) ?>
         <!-- </div> -->
        </div>
    <?php ActiveForm::end(); ?>

</div>
</div>