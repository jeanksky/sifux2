<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PerfilClientes */

$this->title = 'Create Perfil Clientes';
$this->params['breadcrumbs'][] = ['label' => 'Perfil Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perfil-clientes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
