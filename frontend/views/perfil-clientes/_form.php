<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
//use yii\bootstrap\ActiveForm;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;    
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
//ancho de celda GridView yii2
//http://www.yiiframework.com/forum/index.php/topic/3760-best-way-to-hideshow-elements-dinamically/
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript">
     
/*    $(document).ready(function()
    {
        $( "#nombre" ).val( "Esperando...." );
    });*/
    $(function(){

        $('#tipo :radio').change(function(){
        var valor = $(this).val();
        if (valor == 'Física')
              {
                //var net_output = some_variable + 5;
                $('#contenido_a_mostrar').show("slow");
                $('#contenido_a_mostrar2').show("slow");
               // $('#nombre').val("Funciona"); 
                //alert( "Seleccionó juridica");
              }
        else if (valor == 'Jurídica')
        {
            $('#contenido_a_mostrar').hide("slow");
            $('#contenido_a_mostrar2').hide("slow");
        }
        });
        $('#credito :radio').change(function(){
            var valor2 = $(this).val();
            if (valor2 == 'Si')
                  {
                    $('#panel-body').show("slow");
                  }
            else if (valor2 == 'No')
            {
                $('#panel-body').hide("slow");
            }
        });
    });
  /*  function muestra_oculta(id){
        if (document.getElementById){ //se obtiene el id
            var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
            el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
        }
    }
    window.onload = function(){//hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente
        muestra_oculta('contenido_a_mostrar');// "contenido_a_mostrar" es el nombre que le dimos al DIV 
        muestra_oculta('contenido_a_mostrar2');// "contenido_a_mostrar2" es el nombre que le dimos al DIV 
        muestra_oculta('panel-body');// "contenido_a_mostrar2" es el nombre que le dimos al DIV 
    }*/
</script>



<div class="perfil-clientes-form"><br>


<!--Al hace llamado a la función solo tienes que idicar el nombre del DIV entre parentesis -->
<!--p><a style='cursor: pointer;' onclick="muestra_oculta('contenido_a_mostrar')" title="">Mostrar / Ocultar</a></p-->

<!--div id="contenido_a_mostrar">
<p>Este contenido tiene que mostrarse con el link</p>
</div-->



    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'enableClientScript' => true]); ?>
    <!--?php //Para patalla modal______________________
    /* $form = ActiveForm::begin([
    'id' => 'solicitante-form',
    'enableAjaxValidation' => true,
    //'enableClientScript' => true,
   // 'enableClientValidation' => true,
    ]); */ ?-->
    
    <div class="col-md-12" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Ingrese los datos que se le solicitan en el siguiente formulario.
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                                <div class="">
                                    <div class="col-md-3">
                                    
                                        <?= $form->field($model, 'identificacion')->textInput(['maxlength' => 20, 'id'=>'identi','readonly'=>true]) ?>

                                        <?= $form->field($model, 'nombreCompleto')->textInput(['maxlength' => 80, 'id'=>'nombre','readonly'=>true]) ?>
                                        
                                        <div id="contenido_a_mostrar">
                                        <?php 
                                        if ( $model->tipoIdentidad !== 'Jurídica') { ?>
                                        <?= $form->field($model, 'genero')->textInput(['maxlength' => 20, 'id'=>'identi','readonly'=>true]) ?>
                                        <?php } ?>
                                        </div>
                                                    
                                                </div>
                                                <div class="col-md-3">
                                                    <?php 
                                        if ( $model->tipoIdentidad !== 'Jurídica') { ?>
                                                   <?= $form->field($model,'fechNacimiento')->
                                                       widget(DatePicker::className(),[
                                                           'name' => 'check_issue_date',
                                                            'disabled' => true,
                                                            'pluginOptions' => [
                                                                'format' => 'dd-M-yyyy',
                                                                'todayHighlight' => true,
                                                                'autoclose' => true,
                                                            ]
                                                        ]);?>
                                                        <?php } ?>
                                
                                                    <?= $form->field($model, 'telefono')->
                                                    widget(\yii\widgets\MaskedInput::className(), [
                                                            'mask' => '9999-99-99',
                                                    ]) ?>

                                                    <?= $form->field($model, 'fax')->
                                                    widget(\yii\widgets\MaskedInput::className(), [
                                                            'mask' => '9999-99-99',
                                                    ]) ?>
                                                    
                                                    
                                                    
                                                    
                                                </div><div class="col-md-3"> 
                                                    <?= $form->field($model, 'celular')->
                                                    widget(\yii\widgets\MaskedInput::className(), [
                                                            'mask' => '9999-99-99',
                                                    ]) ?>

                                                    <?= $form->field($model, 'email')->
                                                     widget(\yii\widgets\MaskedInput::className(), [
                                                        'name' => 'input-36',
                                                        'clientOptions' => [
                                                        'alias' =>  'email'
                                                         ],
                                                    ]) ?>
                                            </div> 
                                             </div><div class="col-md-3"> 

                                                    <?= $form->field($model, 'direccion')->textarea(['maxlength' => 130]) ?>
                                                    <!-- <div  class="pull-right">
                                                      <button class="btn btn-default" ><a data-toggle="tab" href="#tab2">Siguiente</a></button>  
                                                </div> -->
                                                <div class="form-group" style="line-height:3.5;" align="right">
                                                <?= Html::submitButton($model->isNewRecord ? 'Ingresar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                                 </div>
                                            </div> 
                                        </div><br>
                                    
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>

                    </div>
                        

        </div> <br>
    </div>
    <?php ActiveForm::end(); ?>
</div>

