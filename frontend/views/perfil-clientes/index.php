<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PerfilClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Perfil Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perfil-clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Perfil Clientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idCliente',
            'tipoIdentidad',
            'nombreCompleto',
            'identificacion',
            'genero',
            // 'fechNacimiento',
            // 'telefono',
            // 'fax',
            // 'celular',
            // 'email:email',
            // 'direccion',
            // 'credito',
            // 'documentoGarantia',
            // 'diasCredito',
            // 'autorizacion',
            // 'montoMaximoCredito',
            // 'montoTotalEjecutado',
            // 'estadoCuenta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
