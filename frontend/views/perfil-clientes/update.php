<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model frontend\models\PerfilClientes */

$this->title = $model->nombreCompleto;
// $this->params['breadcrumbs'][] = ['label' => 'Perfil de Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombreCompleto, 'url' => ['view', 'id' => $model->idCliente]];
$this->params['breadcrumbs'][] = 'Actualizar mis datos';
?>
<div class="perfil-clientes-update">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>
<div class="col-lg-12">
    <h3><?= Html::encode($this->title) ?></h3><br>
    <?php $empresa = new User(); ?>
   </div> 
    <div class="col-lg-12">
    <p style="text-align:right">
        <?= Html::a('Regresar', ['view', 'id' => $model->idCliente], ['class' => 'btn btn-default']) ?>
       
       
       <?= Html::a('Cambiar contraseña', ['/site/changepassword'], ['class' => 'btn btn-success']) ?>
       </p>
    </div>  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
