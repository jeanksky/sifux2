<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use frontend\models\PerfilClientes;

/* @var $this yii\web\View */
/* @var $model frontend\models\PerfilClientes */

$this->title = $model->nombreCompleto;
// $this->params['breadcrumbs'][] = ['label' => 'Perfil Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perfil-clientes-view">
<div class="col-lg-12">
    <h3><?= Html::encode($this->title) ?></h3>

    <p style="text-align:right">
        <?= Html::a('Regresar', ['/site/index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar datos', ['update', 'id' => $model->idCliente], ['class' => 'btn btn-primary']) ?>
    </p>
</div>
<div class="col-lg-6">
    <div id="semitransparente">
        <?= DetailView::widget([
            'model' => $model,
            'hAlign'=> DetailView::ALIGN_LEFT ,
            'attributes' => [
                // 'idCliente',
                'tipoIdentidad',
                'identificacion',
                ['attribute'=>'nombreCompleto',  'valueColOptions'=>['style'=>'width:40%'] ],
                ['attribute' => 'genero',
                        'visible' =>$model->tipoIdentidad !== 'Jurídica'], 
                // 'fechNacimiento',
                ['attribute' => 'fechNacimiento',
                        'visible' =>$model->tipoIdentidad !== 'Jurídica'], 
                         
            ],
        ]) ?>
    </div>
</div>
<div class="col-lg-6">
    <div id="semitransparente">
        <?= DetailView::widget([
            'model' => $model,
            'hAlign'=> DetailView::ALIGN_LEFT ,
            'attributes' => [
                'telefono',
                'fax',
                'celular',
                 'email:email',
                ['attribute'=>'direccion',  'valueColOptions'=>['style'=>'width:40%'] ],
            ],
        ]) ?>
    </div>
</div>
<div class="col-lg-6">
    <div id="semitransparente">
        <!--?= DetailView::widget([
            // 'model' => $model,
            // 'hAlign'=> DetailView::ALIGN_LEFT ,
            // 'attributes' => [
            //     'email:email',
            //     ['attribute'=>'direccion',  'valueColOptions'=>['style'=>'width:20%'] ],
            //     'credito',
            //     'documentoGarantia',
            //     'diasCredito',
            //     'autorizacion',
            //    // 'montoMaximoCredito',
            //     [
            //     'attribute' => 'montoMaximoCredito',
            //     'format' => ['decimal',2],
            //      ],
            //      [
            //     'attribute' => 'montoTotalEjecutado',
            //     'format' => ['decimal',2],
            //      ],
            //     //'montoTotalEjecutado',
            //     'estadoCuenta',
            ],
        ]) ?-->
    </div>
</div>

</div>
