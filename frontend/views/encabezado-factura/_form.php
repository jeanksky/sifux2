<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\EncabezadoFactura */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="encabezado-factura-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha_inicio')->textInput() ?>

    <?= $form->field($model, 'fecha_final')->textInput() ?>

    <?= $form->field($model, 'idCliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idOrdenServicio')->textInput() ?>

    <?= $form->field($model, 'porc_descuento')->textInput() ?>

    <?= $form->field($model, 'iva')->textInput() ?>

    <?= $form->field($model, 'total_a_pagar')->textInput() ?>

    <?= $form->field($model, 'estadoFactura')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipoFacturacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigoVendedor')->textInput() ?>

    <?= $form->field($model, 'subtotal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
