<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Funcionario;
use backend\models\FormasPago;
use frontend\models\Compra_update;

/* @var $this yii\web\View */
/* @var $model frontend\models\EncabezadoFactura */

$this->title = $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Encabezado Facturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php 
    $products = Compra_update::getContenidoFactura();

   ?>
<div class="encabezado-factura-view">

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>Factura No. <?= Html::encode($this->title) ?></center>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="col-lg-12">
                    <?php
                        if(@$modelF = Funcionario::find()->where(['idFuncionario'=>$model->codigoVendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                            {
                                echo "<div class='col-lg-4'>";
                                echo "<h4><strong>Vendedor</strong>: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2." ";  
                                echo "</h4>";
                                echo "</div>";
                            }
                    ?>
                    <div class="col-lg-4">
                    <?php 
                        echo '<h4><strong>Estado de la factura</strong>: '.$model->estadoFactura.'</h4>';
                    ?>
                    </div>
                    <div class="col-lg-4">
                    <?php 
                        echo '<h4><strong>Fecha ingreso</strong>: '.$model->fecha_inicio;
                        echo "</h4>";
                    ?>
                    </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-4">
                <?php
                    if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
                        echo "<h4><strong>Tipo de pago</strong>: ".$modelP->desc_forma." "; 
                        echo "</h4>";
                    }
                ?>
                </div>
                <div class="col-lg-2">
               
                </div>
                <div class="col-lg-2">
                
                </div>
                <div class="col-lg-4">
                    <?php 
                        $fecha_final = '';
                            if ($model->fecha_final=='') {
                                $fecha_final = 'Pendiente';
                            } else {
                                $fecha_final = $model->fecha_final;
                            }
                        echo '<h4><strong>Fecha de cancelación</strong>: '.$fecha_final;
                        echo "</h4>";
                    ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div id="conten_pro">
                    <?php
                        echo '<table class="items table table-striped">';
                        echo '<thead>';
                        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th></tr>',
                                'CANTIDAD',
                                'CÓDIGO',
                                'DESCRIPCIÓN',
                                'PRECIO UNITARIO',
                                'DESCUENTO',
                                'IVA %',
                                'TOTAL'
                                );
                        echo '</thead>';
                    if($products) {
                        echo '<tbody>';
                            foreach($products as $position => $product) { 
                                
                                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>',                    
                                        $product['cantidad'],
                                        $product['cod'],
                                        $product['desc'], 
                                        number_format($product['precio'], 2),
                                        number_format($product['cantidad']*(($product['dcto'])),2),//Descuento
                                        $product['iva'],
                                        number_format(($product['precio']*$product['cantidad']),2)
                                           
                                        );     
                            }
                        echo '</tbody>';
                    }
                    echo '</table><div class="well pull-right extended-summary resumen">';
                    echo Compra_update::getTotalm(); 
                    echo '</div>';
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
