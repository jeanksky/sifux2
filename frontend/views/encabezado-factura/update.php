<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\EncabezadoFactura */

$this->title = 'Update Encabezado Factura: ' . ' ' . $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Encabezado Facturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idCabeza_Factura, 'url' => ['view', 'id' => $model->idCabeza_Factura]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="encabezado-factura-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
