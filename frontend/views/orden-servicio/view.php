<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\OrdenServicio */

$this->title = $model->idOrdenServicio;
$this->params['breadcrumbs'][] = ['label' => 'Orden Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-servicio-update">

    <!--?= //Html::encode($this->title) ?-->
    <!--?= //Html::a('Regresar a la factura', ['regresar-factura'], ['class' => 'btn btn-default']) ?-->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

