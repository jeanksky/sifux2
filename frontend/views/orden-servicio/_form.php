<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Familia;
use kartik\widgets\Select2;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use backend\models\Servicios;
use backend\models\DetalleOrdServicio;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model frontend\models\OrdenServicio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Datos de Orden de servicio <strong>ID:</strong>
            <?php $fechaCierre = $model->fechaCierre == '' ? 'Pendiente' : $model->fechaCierre;
            echo $model->idOrdenServicio.'<strong>, Fecha de inicio:</strong> '.$model->fecha; echo '<strong>, Fecha de cierre:</strong> '.$fechaCierre; ?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

            <div class="orden-servicio-form">

                <!--?php //$form = ActiveForm::begin(/*['method' => 'post']*/); ?-->
                <?php $form = ActiveForm::begin([
                    'id' => 'solicitante-form',
                    'method' => 'post',
                    'enableAjaxValidation' => true,
                    'enableClientScript' => true,
                    'enableClientValidation' => true,
                ]); ?>

                
                <div class="col-lg-12">
                    <div class="well col-lg-12">
                        <label>Incluye</label><br>
                        <legend></legend>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'radio')->radioList(['Si' => 'Si', 'No' => 'No']); ?>
                        
                            <?= $form->field($model, 'alfombra')->radioList(['Si' => 'Si', 'No' => 'No']); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'herramienta')->radioList(['Si' => 'Si', 'No' => 'No']); ?>

                            <?= $form->field($model, 'repuesto')->radioList(['Si' => 'Si', 'No' => 'No']); ?>

                        </div>  
                        <div class="col-lg-4">
                            <?= $form->field($model, 'cantOdometro')->textInput(['readonly'=>true, "onkeypress"=>"return isNumber(event)"])->label('Cantidad de Odometro'); ?>
                            <?= $form->field($model, 'cantCombustible')->radioList(['R'=>'R','1/4'=>'1/4','1/3'=>'1/3','F'=>'F'])->label('Cantidad de Combustible');?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'observacionesServicio')->textarea(['readonly'=>true, 'maxlength' => true])  ?>
                        </div>

                    </div></div>
                    <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos de los servicios brindados correspondientes a la presente factura
                        </div>
                        
                        <div class="panel-body">
                            <!--label>Categorías seleccionadas en la Orden de Servicio</label-->
                                <div id="tipo" class="col-lg-12">
                                                                        
                                    <div class="form-group">
                                        <?php 
                                            //Me obtiene los datos del detalle del detalle de orden de servicio
                                            $query = new yii\db\Query();
                                            $data = $query->select(['familia','producto'])
                                                ->from('tbl_detalle_ord_servicio')
                                                ->where(['=','idOrdenServicio', $model->idOrdenServicio])
                                                ->distinct()
                                                ->all();
                                            $filtro_f = null;
                                            $filtro_s = null;
                                            $num = 1;
                                            
                                            //Recorre los productos y familias correspondientes a esa orden de servicio
                                            foreach($data as $row){
                                                //condicion opcional para comprovar si hay datos duplicados
                                                if (/*$row['familia']!=$filtro_f &&*/ $row['producto']!=$filtro_s) {
                                                     //$modelFamilia = Familia::find()->where(['codFamilia'=>$row['familia']])->one();
                                                     //echo 'Familia: '.$modelFamilia->descripcion.'<br>';
                                                     
                                                     //Obtengo una instancia de Servicios donde el codigo de servicio es igual al servicio que recorre el bucle
                                                     $modelServicio = Servicios::find()->where(['codProdServicio'=>$row['producto']])->one();
                                                     //Obtenemos una instancia de familia donde el código de familia es igual al servicio obtenido de la instancia anterior
                                                     $fami = Familia::find()->where(['codFamilia'=>$modelServicio->codFamilia])->one();
                                                     //agregamos en la variable precioyiva el precio total de todos los servicios con el 13% de inpuesto de venta.
                                                     $precioyiva = $modelServicio->precioMinServicio + ($modelServicio->precioMinServicio*0.13);

                                                    //echo $num.'. ('.$fami->descripcion.') '.$modelServicio->nombreProductoServicio.' ₡'.$precioyiva.'<br>';
                                                     
                                                     echo '<div class="col-lg-4"><div class="well col-lg-12">
                                                            <strong>'.$fami->descripcion.'</strong><br>
                                                            <legend></legend>
                                                            '.$modelServicio->nombreProductoServicio.' ₡'.$precioyiva.'
                                                          </div></div>';
                             
                                                     $num++;// Contador.
                                                     $filtro_f = $row['familia']; //Iguala familia al filtro para que no se vuelva a tomar en cuenta
                                                     $filtro_s = $row['producto'];//Iguala producto al filtro para que no se vuelva a tomar en cuenta
                                                }
                                                
                                            }
                                        ?>
                                        <!--?= /*$form->field($model, 'famili')->checkboxList( 
                                        \yii\helpers\ArrayHelper::Map($familias, 
                                            'codFamilia', 
                                            'descripcion'), 
                                        [ 'unselect'=>NULL, 
                                          'separator' => $espacio, 
                                          'item' => function($index, $label, $name, $checked, $value) { 
                                            $label='<br>'.$label;
                                            return  '<label class="modal-radio" id="dicSer"  >' . Html::checkbox($name, $checked, [ 'class'=>'checked','id'=>'o'.$value.'p','onClick'=>'if (this.checked) mostrarSer('.$value.'); else ocultarSer('.$value.')', 'label' => $label, 'value' => $value, 'data-bind' => 'checked:fieldName']) . '</label>';
                                            } 
                                        ])->label(false);*/ ?-->


                                    </div>
                                </div>
                                <!--div class="col-lg-12"><br-->
                                    <!--label>Servicios seleccionados en la Orden de Servicio</label-->
                                    <!--div class="tab-pane fade" id="res"-->
                                    <!--?= /*$form->field($model, 'servi')
                                              ->checkboxList($T_se, [
                                                                     'unselect'=>NULL, 
                                                                     'separator' => $espacio,
                                                                     'item' => function($index, $label, $name, $checked, $value) { 
                                                                                    $label='<br>'.$label;
                                                                                    return  '<label class="cl-ser" style="display: none;" id="content" >' . Html::checkbox($name, $checked, ['id'=>'vaSer', 'onClick'=>'if (this.checked) sumarMontSer('.$value.'); else restarMontSer('.$value.')' , 'label' => $label, 'value' => $value]) . '</label>';   
                                                                                 }
                                                                    ])->label(false)*/ ?-->
                                    <!--input type="text" id="org"-->
                                    <!--/div-->

                                    <!--Aquí en este div es donde se muestran los servicios-->
                                    <!--div id="recargar"></div-->
                                <!--/div-->
                            
                        </div>
                        <div class="panel-body">
                            
                            <?= $form->field($model, 'observaciones')->textarea(['readonly'=>true,'maxlength' => true])  ?>
                            
                            <div class="col-lg-4"><div id "montoSL"></div>
                            <!--?= //$form->field($model,'montoServicio')->textInput() ?-->

                            <!-- envio los datos en un textimput oculto -->
                            <!--?= //$form->field($model, 'montoServicio')->textInput(['id' => 'montoS','readonly'=>true , 'type' => 'hidden']) ?-->
                            
                            </div> 

                        </div>

                       
                        
                    </div>
                </div>
                    

                        <?php ActiveForm::end(); ?>

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>



                                            
                                    

