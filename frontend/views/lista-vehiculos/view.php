<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\ListaFactura;
use frontend\models\OrdenServicio;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use backend\models\Marcas;
use backend\models\Modelo;

/* @var $this yii\web\View */
/* @var $model frontend\models\ListaVehiculos */
$facturas_ = ListaFactura::getContenidoFactura();
$this->title = $model->placa;
$this->params['breadcrumbs'][] = ['label' => 'Mi lista vehicular', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Placa '.$this->title.' - Expediente';

$marca = Marcas::findOne($model->marca);
$modelo = Modelo::findOne($model->modelo);
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

//funcion para obtener el footer de la modal que necesita enviar el valor id de la factura a imprimir
function agrega_footer(id){
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('facturas/footer') ?>",
        type:"post",
        data: { 'id' : id },
        success: function(data){
            $("#footer_modal").html(data);
        },
        error: function(msg, status,err){
         alert('No pasa74');
        }
    });
}//fin

    function refrescar(){
    $("#barra").load(location.href+' #barra',''); //refrescar el div
}
setInterval('refrescar()',2000);

$(document).on('click', '#activity-index-link-orden', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalOrden').modal();
                    }
                );
            }));
$(document).on('click', '#activity-index-link-factura', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalFactura').modal();
                    }
                );
            }));
$(document).ready(function(){
    $('[data-toggle="modal"]').tooltip();
});
</script>

<style type="text/css">

</style>
<div class="lista-vehiculos-view">

    <h1>Expediente del vehículo Placa: <?= Html::encode($this->title) ?></h1>

    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <!--?= /*Html::a('Update', ['update', 'id' => $model->placa], ['class' => 'btn btn-primary'])*/ ?-->
        <!--?= /*Html::a('Delete', ['delete', 'id' => $model->placa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])*/ ?-->
    </p>
    <div class="col-lg-4">
    <h3><small>Marca:</small> <?= $marca->nombre_marca ?> <small>Modelo:</small> <?= $modelo->nombre_modelo ?></h3>
    <h3><small>Año:</small> <?= $model->anio ?></h3>
    </div>
    <div class="col-lg-8">
        <div id="barra">
            <div class="panel panel-info">
            <div class="panel-heading">Si este vehículo está en el taller la barra muestra el % del trabajo acabado. En 100% su vehiculo esta listo.</div>
            <div class="panel-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                      aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $model->periodo_en_manten.'%'; ?>">
                        <?php echo $model->periodo_en_manten.'%'; ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 style="text-align:center">Facturas correspondientes a este vehículo</h2>
            <p style="text-align:center">Accede a las Facturas y Orden de Servicio correspondiente para ver su respectivo detalle, para su mayor control y seguimiento.</p>
        </div>
        <div id="conten_pro">
              <div class="table-responsive">
                <div id="semitransparente"> 
                <?php
                //http://www.programacion.com.py/tag/yii
                    //echo '<div class="grid-view">';
                    echo '<table class="items table table-striped">';
                    echo '<thead>';
                    printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th class="actions button-column">&nbsp;</th></tr>',
                            'ID',
                            'ESTADO',
                            'FECHA INICIO',
                            'FECHA FINAL',
                            'SUBTOTAL',
                            'DESCUENTO',
                            'IVA',
                            'TOTAL'
                            );
                    echo '</thead>';
                if($facturas_) {
                    echo '<tbody>';
                        foreach($facturas_ as $position => $factura) {

                            $mostrarOrden = Html::a('', ['#'], [
                                'class'=>'glyphicon glyphicon-scale',
                                'id' => 'activity-index-link-orden',
                                'data-toggle' => 'modal',
                                'data-target' => '#modalOrden',
                                'data-url' => Url::to(['orden-servicio/view', 'id' => $factura['idOrdenServicio']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Procede a mostrar la orden de servicio')]);
                            $mostrarFactura = Html::a('', ['#'], [
                                'class'=>'glyphicon glyphicon-list-alt',
                                'id' => 'activity-index-link-factura',
                                'data-toggle' => 'modal',
                                'onclick'=>'javascript:agrega_footer('.$factura['idCabeza_Factura'].')',//asigna id de factura para imprimir
                                'data-target' => '#modalFactura',
                                'data-url' => Url::to(['encabezado-factura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Procede a mostrar la factura')]);

                            $mostrarAccion = $mostrarOrden.' '.$mostrarFactura;

                            $fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
                            $fecha_final = '';
                            if ($factura['fecha_final']=='') {
                                $fecha_final = 'Pendiente';
                            } else {
                                $fecha_final = date('d-m-Y', strtotime( $factura['fecha_final'] ));
                            }

                            printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="actions button-column">%s</td></tr>',
                            $factura['idCabeza_Factura'],
                            $factura['estadoFactura'],
                            $fecha_inicio,
                            $fecha_final,
                            number_format($factura['subtotal'],2),
                            number_format($factura['porc_descuento'],2),
                            number_format($factura['iva'],2),
                            number_format($factura['total_a_pagar'],2),
                            $mostrarAccion
                            );
                        }
                    echo '</tbody>';
                }
                echo '</table>';
                ?>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
         Modal::begin([
                'id' => 'modalOrden',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Orden de servicio</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
            ]);
            echo "<div class='well'></div>";

            Modal::end();
    ?>
    <?php
         Modal::begin([
                'id' => 'modalFactura',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Factura</h4></center>',
                //'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
                'footer' => '<div id="footer_modal"></div>',
            ]);
            echo "<div class='well'></div>";

            Modal::end();
    ?>
</div>
