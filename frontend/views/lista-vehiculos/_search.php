<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\ListaVehiculosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lista-vehiculos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'placa') ?>

    <?= $form->field($model, 'idCliente') ?>

    <?= $form->field($model, 'anio') ?>

    <?= $form->field($model, 'modelo') ?>

    <?= $form->field($model, 'marca') ?>

    <?php // echo $form->field($model, 'combustible') ?>

    <?php // echo $form->field($model, 'motor') ?>

    <?php // echo $form->field($model, 'cilindrada_motor') ?>

    <?php // echo $form->field($model, 'version') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
