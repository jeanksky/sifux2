<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Marcas;
use backend\models\Modelo;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ListaVehiculosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mi lista vehicular';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
        $('[data-toggle="tool-tip"]').tooltip();
    });
</script>

<div class="lista-vehiculos-index">
    <p style="text-align:right">
        <?= Html::a('Ver mis otras facturas', ['facturas/index'], ['class' => 'btn btn-success']) ?>
    </p>
    <center>
        <h1><?= Html::encode($this->title) ?></h1>
        <p>Selecciona el vehiculo para consultar su expediente</p>
    </center>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="table-responsive">
    <div id="semitransparente">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'placa',
            //'idCliente',
            [
                'attribute' => 'tbl_marcas.nombre_marca',
                'format' => 'raw',
                'label' => 'Marcas',
                'value' => function ($model, $key, $index, $grid) {
                    $marca = Marcas::findOne($model->marca);
                    if (@$marca)
                      return $marca->nombre_marca;
                    else {
                      return 'Sin marca';
                    }
                },
                'options'=>['style'=>'width:30%'],
            ],
            [
                'attribute' => 'tbl_modelo.nombre_modelo',
                'label' => 'Modelos',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                    $modelo = Modelo::findOne($model->modelo);
                    if (@$modelo)
                      return $modelo->nombre_modelo;
                    else {
                      return 'Sin modelo';
                    }
                },
                'options'=>['style'=>'width:50%'],
            ],
            'anio',
            'combustible',
            'motor',
            'cilindrada_motor',
            'version',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                        return Html::a('<span class=""></span>', ['/lista-vehiculos/view', 'id' => $model->placa], [
                            'class'=>'glyphicon glyphicon-folder-open',
                            'id' => 'activity-index-link-update',
                            'data-toggle' => 'tool-tip',
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Mostrar expediente'),
                            ]);
                    },
            ],
            'options'=>['style'=>'width:70px'],],
        ],
    ]); ?>

</div>
</div>
</div>
