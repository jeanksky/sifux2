<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\FacturasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mis facturas';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

//funcion para obtener el footer de la modal que necesita enviar el valor id de la factura a imprimir
function agrega_footer(id){
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('facturas/footer') ?>",
        type:"post",
        data: { 'id' : id },
        success: function(data){
            $("#footer_modal").html(data);
        },
        error: function(msg, status,err){
         alert('No pasa74');
        }
    });
}//fin

$(document).on('click', '#activity-index-link-factura', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalFactura').modal();
                    }
                );
            }));
$(document).ready(function(){
        $('[data-toggle="modal"]').tooltip();
    });
</script>
<div class="facturas-index">
    <p style="text-align:right">
        <?= Html::a('Mostrar mi lista de vehículos', ['lista-vehiculos/index'], ['class' => 'btn btn-success']) ?>
    </p>
    <center>
        <h1><?= Html::encode($this->title) ?></h1>
        <p>La tabla muestra todas las facturas que no han correspondido a ninguna Orden de Servicio</p>
    </center>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idCabeza_Factura',
            'estadoFactura',
            'fecha_inicio',
            'fecha_final',
            [
                'attribute' => 'subtotal','format' => ['decimal',2],
                // 'value' => function ($model, $key, $index, $grid) {return '₡'.$model->total_a_pagar;},
                'options'=>['style'=>'width:20%'],
            ],
            [
                'attribute' => 'porc_descuento','format' => ['decimal',2],
                // 'value' => function ($model, $key, $index, $grid) {return '₡'.$model->total_a_pagar;},
                'options'=>['style'=>'width:15%'],
            ],
            [
                'attribute' => 'iva','format' => ['decimal',2],
                // 'value' => function ($model, $key, $index, $grid) {return '₡'.$model->total_a_pagar;},
                'options'=>['style'=>'width:15%'],
            ],
            [
                'attribute' => 'total_a_pagar','format' => ['decimal',2],
                // 'value' => function ($model, $key, $index, $grid) {return '₡'.$model->total_a_pagar;},
                'options'=>['style'=>'width:20%'],
            ],
            //'idCliente',
            //'idOrdenServicio',
            // 'porc_descuento',
            // 'iva',
            // 'total_a_pagar',
            // 'tipoFacturacion',
            // 'codigoVendedor',
            //

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                        return Html::a('', ['#'], [
                                'class'=>'glyphicon glyphicon-list-alt',
                                'id' => 'activity-index-link-factura',
                                'data-toggle' => 'modal',
                                'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
                                'data-target' => '#modalFactura',
                                'data-url' => Url::to(['encabezado-factura/view', 'id' => $model->idCabeza_Factura]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Mostrar detalle factura')]);
                    },
                ]
            ],
        ],
    ]); ?>
<?php
         Modal::begin([
                'id' => 'modalFactura',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Factura</h4></center>',
                //'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
                'footer' => '<div id="footer_modal"></div>',
            ]);
            echo "<div class='well'></div>";

            Modal::end();
    ?>
</div>
