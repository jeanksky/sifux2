<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Funcionario;
use backend\models\Clientes;
use backend\models\FormasPago;
use backend\models\Compra_view;

/* @var $this yii\web\View */
/* @var $model backend\models\FacturasDia */

$this->title = $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Facturas Dias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php 
$products = Compra_view::getContenidoFactura(); 
if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
    echo '<strong>Cliente: </strong>'.$cliente->nombreCompleto.'<br>
      <strong>Teléfono: </strong>'.$cliente->telefono.'<br>
      <strong>Dirección: </strong>'.$cliente->direccion.'<br>';
} else{
    echo '<strong>Cliente: </strong>'.$model->idCliente.'<br><br><br>';
}


?>


<div class="facturas-dia-view">
    <div class="panel panel-default"> 
        <div class="panel-heading">
            
            <table>
                <tr>
                    <td>
                        <?php 
                            //echo '<strong>Fecha ingreso</strong>: '.$model->fecha_inicio;
                        ?>
                        <!--br-->
                        <?php
                            if(@$modelF = Funcionario::find()->where(['idFuncionario'=>$model->codigoVendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                {
                                   
                                    echo "<strong>Vendedor</strong>: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2." "; 
                                    echo "<br>";
                                }
                        ?>
                        
                        <?php 
                            echo '<strong>Estado de la factura</strong>: '.$model->estadoFactura;
                        ?>
                        
                    </td>
                    <td>
                        <?php 
                          /*  $fecha_final = '';
                                if ($model->fecha_final=='') {
                                    $fecha_final = 'Pendiente';
                                } else {
                                    $fecha_final = $model->fecha_final;
                                }
                                echo '<strong>Fecha Final</strong>: '.$fecha_final; */
                        ?><!--br-->
                        <?php

                            if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
                                if ($model->estadoFactura!='Crédito') {
                                    echo "<strong>Tipo de pago</strong>: ".$modelP->desc_forma."<br>"; 
                                }
                                else {
                                     
                                    echo '<strong>Fecha de vencimiento:</strong> '.$model->fecha_vencimiento.'<br>';
                                }
                            }
                            echo "<strong>Observación</strong>: ".$model->observacion;
                        ?>
                        
                            
                    </td>
                </tr>
            </table>
            </div>
            <div class="panel-body">
            <div class="col-lg-12">
                <?php
                        echo '<table class="items table table-striped">';
                        echo '<thead>';
                        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th></tr>',
                                'CAN',
                                'CÓD',
                                'DESCRIPCIÓN',
                                'PREC UNIT',
                                'DESC',
                                'IVA %',
                                'TOTAL'
                                );
                        echo '</thead>';

                        if($products) {
                        echo '<tbody>';
                            foreach($products as $position => $product) { 
                                
                                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>',                    
                                        $product['cantidad'],
                                        $product['cod'],
                                        $product['desc'], 
                                        number_format($product['precio'], 2),
                                        number_format($product['cantidad']*(($product['dcto'])),2),//Descuento
                                        $product['iva'],
                                        number_format(($product['precio']*$product['cantidad']),2)
                                           
                                        );     
                            }
                        echo '</tbody>';
                    }
                        echo '</table><div class="" >';
                        echo Compra_view::getTotalm(); 
                        echo '</div>';
                ?>
            </div>
        </div>
    </div>

</div>
