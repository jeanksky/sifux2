<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_tipo_cambio".
 *
 * @property integer $idTipo_cabio
 * @property integer $idTipo_moneda
 * @property integer $idCategoria
 * @property string $fecha_hora
 * @property string $valor_tipo_cambio
 */
class TipoCambio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tipo_cambio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTipo_moneda', 'idCategoria', 'fecha_hora', 'valor_tipo_cambio'], 'required'],
            [['idTipo_moneda', 'idCategoria'], 'integer'],
            [['fecha_hora'], 'safe'],
            [['valor_tipo_cambio'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipo_cabio' => 'Id Tipo Cabio',
            'idTipo_moneda' => 'Id Tipo Moneda',
            'idCategoria' => 'Id Categoria',
            'fecha_hora' => 'Fecha Hora',
            'valor_tipo_cambio' => 'Valor Tipo Cambio',
        ];
    }
}
