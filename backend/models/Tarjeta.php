<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_tarjeta".
 *
 * @property integer $idTarjeta
 * @property string $nombreTarjeta
 */
class Tarjeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tarjeta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreTarjeta'], 'required'],
            [['nombreTarjeta'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTarjeta' => 'Id Tarjeta',
            'nombreTarjeta' => 'Nombre de Tarjeta',
        ];
    }
}
