<?php

namespace backend\models;
use backend\models\ComprasInventario;
use Yii;
use mPDF;

/**
 * This is the model class for table "tbl_compras".
 *
 * @property integer $idCompra
 * @property integer $numFactura
 * @property string $fechaRegistro
 * @property string $fechaVencimiento
 * @property integer $idProveedor
 * @property string $formaPago
 * @property double $subTotal
 * @property double $descuento
 * @property double $impuesto
 * @property double $total
 */
class ComprasInventario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_compras';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numFactura', 'n_interno_prov', 'fechaDocumento', 'fechaRegistro', 'formaPago', 'subTotal', /*'descuento',*/ 'impuesto', 'total', 'idProveedor'], 'required'],
            [['idProveedor','idOrdenCompra'], 'integer'],
              [['numFactura'], 'string', 'max' => 50],
            //[['numFactura'], 'unique'],
            //http://www.yiiframework.com/doc-2.0/yii-validators-uniquevalidator.html
            [['numFactura'], 'unique', 'targetAttribute' => ['numFactura', 'idProveedor'], 'message'=>'Este {attribute} ya ha sido registrado con este proveedor.'],
            [['descuento','fechaRegistro', 'fechaDocumento', 'fechaVencimiento'], 'safe'],
            //[['subTotal', 'impuesto', 'total'], 'number'],
          //  [['descuento'], 'string', 'max' => 100],//esto es temporal y lo estoy cambieando a numero en el controlador
            [['formaPago','n_interno_prov'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCompra' => 'Id Compra',
            'numFactura' => 'No. FE (Factura electrónica) o No. Factura',
            'n_interno_prov' => 'No. Interno Proveedor; Sino repita el No. FE',
            'fechaDocumento' => 'Fecha Documento',
            'fechaRegistro' => 'Fecha Registro',
            'fechaVencimiento' => 'Fecha Vencimiento',
            'idProveedor' => 'Proveedor',
            'formaPago' => 'Forma de Pago',
            'subTotal' => 'Subtotal',
            'descuento' => 'Descuento',
            'impuesto' => 'Impuesto',
            'total' => 'Total',
            'idOrdenCompra'=>'Orden de compra',
        ];
    }

/*public function unico($attribute)
  {
    $origem = self::find()->where( "numFactura = $this->numFactura")->one();
    $d=12;
         if($origem == $attribute)
         {
            return $this->addError($attribute, 'este ya existe' );
         }
  }*/

    public function beforeSave($insert)
    {
            $this->fechaDocumento = date('Y-m-d', strtotime( $this->fechaDocumento));
            $this->fechaRegistro = date('Y-m-d', strtotime( $this->fechaRegistro));
            $this->fechaVencimiento = date('Y-m-d', strtotime( $this->fechaVencimiento));
            if ($this->fechaVencimiento == '1970-01-01') {
                 $this->fechaVencimiento = '';
             }
            return parent::beforeSave($insert);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_proveedores.nombreEmpresa']);
    }


    public function afterFind()
    {
          $this->fechaDocumento = date('d-m-Y', strtotime( $this->fechaDocumento));
          $this->fechaRegistro = date('d-m-Y', strtotime( $this->fechaRegistro));
          $this->fechaVencimiento = date('d-m-Y', strtotime( $this->fechaVencimiento));
          if ($this->fechaVencimiento == '01-01-1970') {
               $this->fechaVencimiento = '';
           }
          return parent::afterFind();

    }

    public function _pdf_show_re($id, $stylesheet, $pdf_html, $archivo_pdf)
    {
      $pdf = new mPDF();
      $pdf->title = 'Recepción ID. '.$id;
      $pdf->WriteHTML($stylesheet,1);
      $pdf->WriteHTML($pdf_html,2);
      $pdf->Output($archivo_pdf,'I');
    }
}
