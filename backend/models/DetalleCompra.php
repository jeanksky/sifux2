<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_detalle_compra".
 *
 * @property integer $idDetalleCompra
 * @property integer $idCompra
 * @property integer $noEntrada
 * @property string $codProdServicio
 * @property string $precioCompra
 * @property integer $cantidadEntrada
 * @property string $descuento
 * @property string $exImpuesto
 * @property double $porcentUtilidad
 * @property double $preciosinImp
 * @property double $precioconImp
 * @property string $usuario
 * @property string $estado
 *
 * @property TblCompras $idCompra0
 */
class DetalleCompra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_detalle_compra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'idCompra', 'noEntrada',*/ 'codProdServicio', 'codigo_proveedor', 'precioCompra', 'cantidadEntrada',/* 'descuento',*/ 'exImpuesto', 'codigo_proveedor', 'porcentUtilidad', 'preciosinImp', 'precioconImp', 'usuario', 'estado'], 'required'],
            [['descuento'], 'safe'],
            [['idCompra', 'noEntrada', 'cantidadEntrada'], 'integer'],
            //['precioCompra', 'no_repetido'],
            //[['precioCompra', 'descuento', 'porcentUtilidad', 'preciosinImp', 'precioconImp'], 'number'],
            [['codProdServicio', 'estado'], 'string', 'max' => 30],
            [['exImpuesto'], 'string', 'max' => 3],
            [['usuario','codigo_proveedor'], 'string', 'max' => 50]
        ];
    }

    public function no_repetido($attribute,$params)
    {
      if ($this->precioCompra == '22'){
        $this->addError('precioCompra', "Campo requerido");
      }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDetalleCompra' => 'Id Detalle Compra',
            'idCompra' => 'Compra',
            'noEntrada' => 'No Entrada',
            'codProdServicio' => 'Código',
            'codigo_proveedor' => 'Código Proveedor',
            'precioCompra' => 'Precio Compra',
            'cantidadEntrada' => 'Cantidad de Entrada',
            'descuento' => '% Descuento',
            'exImpuesto' => 'Exento de Impuesto',
            'porcentUtilidad' => '% Utilidad',
            'preciosinImp' => 'Precio SIN IMPUESTO',
            'precioconImp' => 'Precio + IMPUESTO',
            'usuario' => 'Usuario Registra',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompra0()
    {
        return $this->hasOne(TblCompras::className(), ['idCompra' => 'idCompra']);
    }
}
