<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_tipo_movimientos".
 *
 * @property string $codigoTipo
 * @property string $descripcion
 * @property string $acciones
 */
class TipoMovimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tipo_movimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'descripcion', 'acciones'], 'required'],
            // [['codigoTipo'], 'string', 'max' => 25],
            [['descripcion'], 'string', 'max' => 200],
            [['acciones'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codigoTipo' => 'Codigo',
            'descripcion' => 'Descripción',
            'acciones' => 'Acciones',
        ];
    }
}
