<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_tipo_documento".
 *
 * @property integer $idTipo_documento
 * @property string $descripcion
 * @property string $tipo_movimiento
 * @property string $estado
 */
class TipoDocumento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tipo_documento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion', 'tipo_movimiento', 'estado'], 'required'],
            [['descripcion'], 'string', 'max' => 80],
            [['tipo_movimiento'], 'string', 'max' => 40],
            [['estado'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipo_documento' => 'Id Tipo Documento',
            'descripcion' => 'Descripcion',
            'tipo_movimiento' => 'Tipo Movimiento',
            'estado' => 'Estado',
        ];
    }
}
