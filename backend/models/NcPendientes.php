<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_nc_pendientes".
 *
 * @property integer $idNCPendiente
 * @property string $idCliente
 * @property string $fecha
 * @property string $concepto
 * @property string $monto
 */
class NcPendientes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_nc_pendientes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['monto'], 'number'],
            [['idCliente'], 'string', 'max' => 40],
            [['concepto'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idNCPendiente' => 'Id Ncpendiente',
            'idCliente' => 'Id Cliente',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'monto' => 'Monto',
        ];
    }
}
