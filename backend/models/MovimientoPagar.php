<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_movi_pagar".
 *
 * @property integer $idMov
 * @property integer $idTramite_pago
 * @property integer $idCompra
 * @property string $fecha_mov
 * @property string $tipo_mov
 * @property string $detalle
 * @property string $monto_anterior
 * @property string $monto_movimiento
 * @property string $saldo_pendiente
 * @property string $usuario
 * @property string $proveedor
 */
class MovimientoPagar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_movi_pagar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'numFactura',*/ 'fecha_mov', 'tipo_mov', 'detalle', /*'monto_anterior',*/ 'monto_movimiento', /*'saldo_pendiente',*/ 'usuario', 'proveedor'], 'required'],
            [['idTramite_pago', 'idCompra'], 'integer'],
            [['fecha_mov', 'idCompra'], 'safe'],
            [['monto_anterior', 'monto_movimiento', 'saldo_pendiente'], 'number'],
            [['tipo_mov'], 'string', 'max' => 2],
             [['numFactura'], 'string', 'max' => 20],
            [['detalle'], 'string', 'max' => 120],
            [['usuario'], 'string', 'max' => 40],
            [['proveedor'], 'string', 'max' => 80]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMov' => 'Id Mov',
            'numFactura' => 'num factura',
            'fecha_mov' => 'Fecha Mov',
            'tipo_mov' => 'Tipo Mov',
            'detalle' => 'Detalle',
            'monto_anterior' => 'Monto Anterior',
            'monto_movimiento' => 'Monto Movimiento',
            'saldo_pendiente' => 'Saldo Pendiente',
            'usuario' => 'Usuario',
            'proveedor' => 'Proveedor',
        ];
    }
}
