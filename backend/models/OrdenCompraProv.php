<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_orden_compra_prov".
 *
 * @property integer $idOrdenCompra
 * @property integer $idCompra
 * @property integer $idProveedor
 * @property string $fecha_registro
 * @property string $fecha_ingreso_mercaderia
 * @property string $prioridad
 * @property integer $idContacto_pedido
 * @property integer $id_transporte
 * @property string $observaciones
 * @property string $estado
 */
class OrdenCompraProv extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_orden_compra_prov';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCompra', 'idProveedor', 'idContacto_pedido', 'id_transporte'], 'integer'],
            [['idProveedor', 'fecha_registro', 'prioridad', 'idContacto_pedido', 'id_transporte', 'estado'], 'required'],
            [['fecha_registro', 'fecha_ingreso_mercaderia'], 'safe'],
            [['prioridad'], 'string', 'max' => 5],
            [['observaciones'], 'string', 'max' => 200],
            [['estado'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idOrdenCompra' => 'Id Orden Compra',
            'idCompra' => 'Id Compra',
            'idProveedor' => 'Id Proveedor',
            'fecha_registro' => 'Fecha Registro',
            'fecha_ingreso_mercaderia' => 'Fecha Ingreso Mercaderia',
            'prioridad' => 'Prioridad',
            'idContacto_pedido' => 'Id Contacto Pedido',
            'id_transporte' => 'Id Transporte',
            'observaciones' => 'Observaciones',
            'estado' => 'Estado',
        ];
    }
}
