<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_dev_productos_devueltos".
 *
 * @property integer $idProd_dev
 * @property integer $idDetalleFactura
 * @property integer $idDevolucion
 * @property string $cantidad_dev
 * @property string $descuento
 * @property string $iv
 * @property string $total
 * @property string $fecha_hora
 * @property string $estado
 */
class DevProductosDevueltos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_dev_productos_devueltos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDetalleFactura', 'idDevolucion'], 'integer'],
            [['cantidad_dev', 'descuento', 'iv', 'total'], 'number'],
            [['fecha_hora'], 'safe'],
            [['estado'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProd_dev' => 'Id Prod Dev',
            'idDetalleFactura' => 'Id Detalle Factura',
            'idDevolucion' => 'Id Devolucion',
            'cantidad_dev' => 'Cantidad Dev',
            'descuento' => 'Descuento',
            'iv' => 'Iv',
            'total' => 'Total',
            'fecha_hora' => 'Fecha Hora',
            'estado' => 'Estado',
        ];
    }
}
