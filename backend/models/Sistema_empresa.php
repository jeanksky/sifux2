<?php
namespace backend\models;

use common\models\Cn_db_nelux_fr;
use backend\models\Empresa;

class Sistema_empresa extends Cn_db_nelux_fr
{
    public function __construct()
    {
        parent::__construct();
    }

    //esta funcion me permite obtener el estado del sistema con el alquiler y asi poder mostrar una notificación
    //al usuario de que estṕa por vencer la licencia mensual o ya está vencida
    public function empresa_alert()
    {
        $empresa_alert = Empresa::find()->where(['idEmpresa'=>1])->one();
        $sql = 'SELECT estado FROM nps_sistemas_alquiler INNER JOIN tbl_sistema_empresa
        ON nps_sistemas_alquiler.id_sistemas_alquiler = tbl_sistema_empresa.id_sistema_alquiler
        WHERE tbl_sistema_empresa.cod_empresa = "' . $empresa_alert->cod_empresa . '"';
        $result = $this->_db->query($sql);
        /*fetch_all si es para varios / fetch_assoc si lo uso para una linea */
        //$su = $result->fetch_all(MYSQLI_ASSOC);
        $sistema_alquiler_elegido = $result->fetch_assoc();
        return $sistema_alquiler_elegido['estado'];
    }

    //esta funcion me permite obtener la licencia mensual en la que está sujeto el sistema.
    public function licencia_sistema()
    {
      $empresa_alert = Empresa::find()->where(['idEmpresa'=>1])->one();
      $sql = 'SELECT licencia FROM nps_sistemas_alquiler INNER JOIN tbl_sistema_empresa
      ON nps_sistemas_alquiler.id_sistemas_alquiler = tbl_sistema_empresa.id_sistema_alquiler
      WHERE tbl_sistema_empresa.cod_empresa = "' . $empresa_alert->cod_empresa . '"';
      $result = $this->_db->query($sql);
      /*fetch_all si es para varios / fetch_assoc si lo uso para una linea */
      //$su = $result->fetch_all(MYSQLI_ASSOC);
      $sistema_alquiler_elegido = $result->fetch_assoc();
      if (@$sistema_alquiler_elegido['licencia']) {
        return $sistema_alquiler_elegido['licencia'];
      }else {
        return '(FUERA DE ENLACE)';
      }

    }

    public function notificaciones_nelux($modo)
    {
      $empresa_alert = Empresa::find()->where(['idEmpresa'=>1])->one();
      if ($modo == 'lista') {
        $sql = 'SELECT * FROM nps_notifica INNER JOIN nps_notifica_sistema INNER JOIN tbl_sistema_empresa
        ON nps_notifica.idNotifi = nps_notifica_sistema.idNotifi
        AND nps_notifica_sistema.id_sistema_alquiler = tbl_sistema_empresa.id_sistema_alquiler
        WHERE nps_notifica.fecha_hora > "'.date("Y-m-d 00:00:00").'" AND tbl_sistema_empresa.cod_empresa = "' . $empresa_alert->cod_empresa . '"';
        $result = $this->_db->query($sql);
        $notificaciones = $result->fetch_all(MYSQLI_ASSOC);
        $claf = '';
        foreach ($notificaciones as $key => $value) {
          if ($value['prioridad']=='baja') {
            $claf .= '<div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              ID: '.$value['idNotifi'].' <strong>Notificación:</strong> [ '.date('d-m-Y h:i:s A', strtotime($value['fecha_hora'])).' ]<br>'.$value['notificacion'].'
            </div>';
          } else {
            $claf .= '<div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              ID: '.$value['idNotifi'].' <strong>Notificación importante:</strong> [ '.date('d-m-Y h:i:s A', strtotime($value['fecha_hora'])).' ]<br>'.$value['notificacion'].'
            </div>';
          }
        }
        return $claf;
      } else {
        $sql = 'SELECT COUNT(*) AS cantidad FROM nps_notifica INNER JOIN nps_notifica_sistema INNER JOIN tbl_sistema_empresa
        ON nps_notifica.idNotifi = nps_notifica_sistema.idNotifi
        AND nps_notifica_sistema.id_sistema_alquiler = tbl_sistema_empresa.id_sistema_alquiler
        WHERE nps_notifica.fecha_hora > "'.date("Y-m-d 00:00:00").'" AND tbl_sistema_empresa.cod_empresa = "' . $empresa_alert->cod_empresa . '"';
        $result = $this->_db->query($sql);
        $notificaciones = $result->fetch_assoc();
        return $notificaciones['cantidad'];
      }

    }

    public function estado_hacienda()
    {
      $sql = 'SELECT * FROM nps_estado_hacienda';
      $result = $this->_db->query($sql);
      $resultado = $result->fetch_assoc();
      return array('status'=>$resultado['status'], 'last_update'=>Date('d-m-Y / h:i:s A',strtotime($resultado['last_update'])));
    }
}
