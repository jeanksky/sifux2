<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_producto_pedido".
 *
 * @property integer $idProducto_pedido
 * @property integer $idOrdenCompra
 * @property integer $codProdServicio
 * @property integer $idProveedor
 */
class ProductoPedido extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_producto_pedido';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrdenCompra', 'idProveedor'], 'integer'],
            [['codProdServicio'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProducto_pedido' => 'Id Producto Pedido',
            'idOrdenCompra' => 'Id Orden Compra',
            'codProdServicio' => 'Cod Prod Servicio',
            'idProveedor' => 'Id Proveedor',
        ];
    }
}
