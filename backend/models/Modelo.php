<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_modelo".
 *
 * @property string $codModelo
 * @property string $codMarca
 */
class Modelo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_modelo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codMarca','nombre_modelo', 'slug'], 'required'],
            [['nombre_modelo','slug'], 'unique'],
            [['nombre_modelo','slug'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre_modelo' => 'Modelo',
            'slug' => 'Slug',
            'codMarca' => 'Marca',
        ];
    }

    public static function getSubCatList($codMarca)
    {
    $data = Modelo::find()
       ->where(['codMarca'=>$codMarca])
       ->select(['codModelo','codModelo' ])->asArray()->all();

    return $data;

    }

 protected function getModelo($codMarca){
     $datas = Modelo::find()->where(['codMarca'=>$codMarca])->all();
     return $this->MapData($datas,'codModelo','codModelo');
 }
 protected function MapData($datas,$fieldId,$fieldName){
     $obj = [];
     foreach ($datas as $key => $value) {
         array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}]);
     }
     return $obj;
 }

 public function attributes()
 {
     return array_merge(parent::attributes(), ['tbl_marcas.nombre_marca']);
 }

}
