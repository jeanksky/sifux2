<?php

namespace backend\models;
use backend\models\Clientes;
use backend\models\DetalleFacturas;
use backend\models\OrdenServicio;
use yii\helpers\ArrayHelper;
use backend\models\ProductoServicios;
use backend\models\AgentesComisiones;


use Yii;

/**
 * This is the model class for table "tbl_encabezado_factura".
 *
 * @property integer $idCabeza_Factura
 * @property string $fecha_inicio
 * @property string $fecha_final
 * @property integer $idCliente
 * @property integer $idOrdenServicio
 * @property integer $porc_descuento
 * @property integer $iva
 * @property double $total_a_pagar
 * @property string $estadoFactura
 *
 * @property TblDetalleFacturas[] $tblDetalleFacturas
 * @property TblClientes $idCliente0
 * @property TblOrdenServicio $idOrdenServicio0
 * @property TblHistCancelFacturaCr[] $tblHistCancelFacturaCrs
 * @property TblMedioPago[] $tblMedioPagos
 */
class EncabezadoPrefactura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    //public $vendedor;
    //public $clienteN;
    //public $descripcion;
    //public $detalles;
    /*public $precio_cant;*/

    public static function tableName()
    {
        return 'tbl_encabezado_factura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //['vendedor', 'safe'],
            //['clienteN', 'safe'],
            //['descripcion', 'safe'],
            //['detalles', 'safe'],
        //    [['fecha_inicio','idCliente'/*,'idOrdenServicio'  'porc_descuento', 'iva', 'total_a_pagar',*/, 'estadoFactura',/* 'clienteN',*/'subtotal'], 'required'],
          //  [['fecha_inicio', 'fecha_final'], 'safe'],
         //   [['idCliente', 'idOrdenServicio', 'porc_descuento', 'iva','codigoVendedor','tipoFacturacion'], 'integer'],
           // [['total_a_pagar', 'subtotal'], 'number'],
            //[['estadoFactura'], 'string', 'max' => 20]


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCabeza_Factura' => 'Código',
            'fecha_inicio' => 'Fecha inicio',
            //'fecha_final' => 'Fecha final',
            'idCliente' => 'Código',
            'idOrdenServicio' => 'Orden Servicio',
            'porc_descuento' => 'Descuento % ',
            'iva' => 'Iva %',
            'total_a_pagar' => 'Total',
            'estadoFactura' => 'Estado',
            'tipoFacturacion' => 'Tipo Pago',
            'codigoVendedor' => 'Código',
            'subtotal'  => 'Subtotal',
           /* 'vendedor' =>'Vendedor',
            'cliente' => 'Cliente',
            'descripcion' => 'Descripción',*/
            /*'precio_u' => 'Precio unidad',
            'precio_cant' => 'Precio cantidad',*/
        ];
    }

    /*public function afterSave($insert, $changedAttributes){
        \Yii::$app->db->createCommand()->delete('tbl_detalle_facturas', 'idCabeza_factura = '.(int) $this->idCabeza_Factura)->execute();
        //if (is_array($this->producto) || is_object($this->producto)) {
                foreach ($this->producto as $idP) {
                    $rf_ = new DetalleFacturas();
                    $rf_->idCabeza_factura = $this->idCabeza_Factura;
                    $rf_->codProdServicio = $idP;
                    $rf_->cantidad = $this->$cant;
                    $rf_->precio_unitario = $this->$precio_u;
                    $rf_->precio_por_cantidad = $this->$precio_cant;
                    $rf_->save();
                }
          //  }
         }*/


    //    public function init()
    // {
    //     parent::init();

    //     $this->producto = [
    //         [
    //             'producto' =>  ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio', 'nombreProductoServicio'),

    //         ],
    //     ];
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblDetalleFacturas()
    {
        return $this->hasMany(DetalleFacturas::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente0()
    {
        return $this->hasOne(TblClientes::className(), ['idCliente' => 'idCliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrdenServicio0()
    {
        return $this->hasOne(TblOrdenServicio::className(), ['idOrdenServicio' => 'idOrdenServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblHistCancelFacturaCrs()
    {
        return $this->hasMany(TblHistCancelFacturaCr::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblMedioPagos()
    {
        return $this->hasMany(TblMedioPago::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_factura_ot.idCliente' ,'tbl_clientes.nombreCompleto', 'tbl_orden_servicio.idMecanico']);
    }

    //antes de guardar
    public function beforeSave($insert)
    {
            $this->fecha_inicio = date('Y-m-d', strtotime( $this->fecha_inicio));
            //$this->fecha_final = date('Y-m-d', strtotime( $this->fecha_final));
            return parent::beforeSave($insert);
    }

    //despues de buscar
      public function afterFind()
      {

            $this->fecha_inicio = date('d-m-Y', strtotime( $this->fecha_inicio));
            if ($this->fecha_final!='') {
                $this->fecha_final = date('d-m-Y', strtotime( $this->fecha_final));
            }

            if ($this->fecha_vencimiento!='') {
                $this->fecha_vencimiento = date('d-m-Y', strtotime( $this->fecha_vencimiento));
            }

            return parent::afterFind();

          }

        public function getFullName($idFun)
        {
           // $fullname = new Funcionario();

           $sql = "SELECT nombre, apellido1, apellido2 FROM tbl_funcionario WHERE idFuncionario = :idF";
           $command = \Yii::$app->db->createCommand($sql);
           $command->bindValue(":idF", $idFun);
           $result = $command->queryOne();

            return $result['nombre'].' '.$result['apellido1'].' '.$result['apellido2'];
        }

        function money_format($format, $number)
          {
              $regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.
                        '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
              if (setlocale(LC_MONETARY, 0) == 'C') {
                  setlocale(LC_MONETARY, '');
              }
              $locale = localeconv();
              preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
              foreach ($matches as $fmatch) {
                  $value = floatval($number);
                  $flags = array(
                      'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
                                     $match[1] : ' ',
                      'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
                      'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
                                     $match[0] : '+',
                      'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
                      'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
                  );
                  $width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
                  $left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
                  $right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
                  $conversion = $fmatch[5];

                  $positive = true;
                  if ($value < 0) {
                      $positive = false;
                      $value  *= -1;
                  }
                  $letter = $positive ? 'p' : 'n';

                  $prefix = $suffix = $cprefix = $csuffix = $signal = '';

                  $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
                  switch (true) {
                      case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
                          $prefix = $signal;
                          break;
                      case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
                          $suffix = $signal;
                          break;
                      case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
                          $cprefix = $signal;
                          break;
                      case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
                          $csuffix = $signal;
                          break;
                      case $flags['usesignal'] == '(':
                      case $locale["{$letter}_sign_posn"] == 0:
                          $prefix = '(';
                          $suffix = ')';
                          break;
                  }
                  if (!$flags['nosimbol']) {
                      $currency = $cprefix .
                                  ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
                                  $csuffix;
                  } else {
                      $currency = '';
                  }
                  $space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';

                  $value = number_format($value, $right, $locale['mon_decimal_point'],
                           $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
                  $value = @explode($locale['mon_decimal_point'], $value);

                  $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
                  if ($left > 0 && $left > $n) {
                      $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
                  }
                  $value = implode($locale['mon_decimal_point'], $value);
                  if ($locale["{$letter}_cs_precedes"]) {
                      $value = $prefix . $currency . $space . $value . $suffix;
                  } else {
                      $value = $prefix . $value . $space . $currency . $suffix;
                  }
                  if ($width > 0) {
                      $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
                               STR_PAD_RIGHT : STR_PAD_LEFT);
                  }

                  $format = str_replace($fmatch[0], $value, $format);
              }
              return $format;
          }

          public function movimientocantidad($val, $cant, $cod)
            {
                 $bandera = null;
                 $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :idproducto";
                 $command = \Yii::$app->db->createCommand($sql);
                 $command->bindParam(":idproducto", $cod_);
                 $cantidad = $command->queryOne();
                 $cantidadnueva = $val > $cant ? $cantidad['cantidadInventario'] - ($val - $cant) : $cantidad['cantidadInventario'] + ($cant - $val);
             if ($bandera == null) {

                    $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                    $command2 = \Yii::$app->db->createCommand($sql2);
                    $command2->bindParam(":cantidad", $cantidadnueva);
                    $command2->bindParam(":codProdServicio", $cod);
                    $command2->execute();
                }
                return true;
            }

          public function productoslista(){
            return ProductoServicios::find()->where(["=",'cantidadInventario','1'])->all();

          }

    public function getAgentesComisiones()
    {
        return AgentesComisiones::find()
        ->where(['=','estadoAgenteComision','Activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('idAgenteComision ASC')
        ->all();
    }

}
