<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;


	class MovimientosSesion 
	{

		public static function getContenidoMovimiento() {
			if(is_string(Yii::$app->session->get('movimiento'))){
				return JSON::decode(Yii::$app->session->get('movimiento'), true);
			}
			else
				return Yii::$app->session->get('movimiento');
		}

		public static function setContenidoMovimiento($movimiento) {
			return Yii::$app->session->set('movimiento', JSON::encode($movimiento));
		}		
		
		//----------------------------------------------------------
		public static function getFecha() {
			return Yii::$app->session->get('fecham');
		}
		public static function setFecha($fecham) {
			return Yii::$app->session->set('fecham', $fecham);
		}
		//----------------------------------------------------------
		public static function getTipo() {
			return Yii::$app->session->get('tipo');
		}
		public static function setTipo($tipo) {
			return Yii::$app->session->set('tipo', $tipo);
		}
		//----------------------------------------------------------
		public static function getReferencia() {
			return Yii::$app->session->get('referencia');
		}

		public static function setReferencia($referencia) {
			return Yii::$app->session->set('referencia', $referencia);
		}

		//----------------------------------------------------------
		public static function getObservaciones() {
			return Yii::$app->session->get('observaciones');
		}

		public static function setObservaciones($observaciones) {
			return Yii::$app->session->set('observaciones', $observaciones);
		}
		//----------------------------------------------------------
		public static function getEstado() {
			return Yii::$app->session->get('estado');
		}

		public static function setEstado($estado) {
			return Yii::$app->session->set('estado', $estado);
		}
		// ----------------------------------------------------------
		public static function getUsuario() {
			return Yii::$app->session->get('usuario');
		}
		public static function setUsuario($usuario) {
			return Yii::$app->session->set('usuario', $usuario);
		}
		}