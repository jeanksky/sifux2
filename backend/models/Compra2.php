<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;
use backend\models\ProductoServicios;//Prueba


	class Compra2
	{

		public static function getContenidoCompra() {
			if(is_string(Yii::$app->session->get('compra'))){
				return JSON::decode(Yii::$app->session->get('compra'), true);
			}
			else
				return Yii::$app->session->get('compra');
		}

		public static function setContenidoCompra($compra) {
			return Yii::$app->session->set('compra', JSON::encode($compra));
		}
		//----------------------------------------------------------
		/*public static function getContenidoCheque() {
			if(is_string(Yii::$app->session->get('cheque')))
				return CJSON::decode(Yii::$app->session->get('cheque'), true);
			else
				return Yii::$app->session->get('cheque');
		}

		public static function setContenidoCheque($cheque) {
			return Yii::$app->session->set('cheque', CJSON::encode($cheque));
		}*/
		//----------------------------------------------------------
		/*public static function getProveedor() {
			return Yii::$app->session->get('proveedor');
		}

		public static function setProveedor($proveedor) {
			return Yii::$app->session->set('proveedor', $proveedor);
		}*/
		//----------------------------------------------------------
		public static function getCabezaFactura() {//es auxiliar
			return Yii::$app->session->get('cabfauxiliar');
		}
		public static function setCabezaFactura($cabfauxiliar) {
			return Yii::$app->session->set('cabfauxiliar', $cabfauxiliar);
		}
		//----------------------------------------------------------
		public static function getVendedor() {
			return Yii::$app->session->get('vendedor');
		}
		public static function setVendedor($vendedor) {
			return Yii::$app->session->set('vendedor', $vendedor);
		}
		//----------------------------------------------------------
		public static function getCliente() {
			return Yii::$app->session->get('cliente');
		}

		public static function setCliente($cliente) {
			return Yii::$app->session->set('cliente', $cliente);
		}
		//----------------------------------------------------------
		public static function getEstadoFactura() {
			return Yii::$app->session->get('estado');
		}

		public static function setEstadoFactura($estado) {
			return Yii::$app->session->set('estado', $estado);
		}
		//----------------------------------------------------------
		public static function getFechaInicial() {
			return Yii::$app->session->get('fecha');
		}

		public static function setFechaInicial($fecha) {
			return Yii::$app->session->set('fecha', $fecha);
		}
		//----------------------------------------------------------
		public static function getPago() {
			return Yii::$app->session->get('pagoc');
		}

		public static function setPago($pagoc) {
			return Yii::$app->session->set('pagoc', $pagoc);
		}
		//----------------------------------------------------------
		public static function getMedio() {
			return Yii::$app->session->get('medio');
		}

		public static function setMedio($medio) {
			return Yii::$app->session->set('medio', $medio);
		}
		//----------------------------------------------------------
		public static function getComproba() {
			return Yii::$app->session->get('comprobacion');
		}

		public static function setComproba($comprobacion) {
			return Yii::$app->session->set('comprobacion', $comprobacion);
		}

		//----------------------------------------------------------
		public static function getObserva() {
			return Yii::$app->session->get('observa');
		}

		public static function setObserva($observa) {
			return Yii::$app->session->set('observa', $observa);
		}
		//----------------------------------------------------------
		/*public static function getDoc() {
			return Yii::$app->session->get('doc');
		}

		public static function setDoc($doc) {
			return Yii::$app->session->set('doc', $doc);
		}*/
		//----------------------------------------------------------
		public static function getTotal($par=false) {
			$precio=$subtotal=$total=$iva=$dcto=$temporal=$ivamostrar=0; $subtotal_ = 0; $dcto_ =0; $i=0;
			if(@$compra=Compra::getContenidoCompra())
			foreach($compra as $product)  {
				//$model=Productos::model()->findByPk($product['id_producto']);
				//$resultado=ProductoProveedor::getProductoProveedor($model->id_producto);
				$subtotal+=@$product['cantidad']*@$product['precio'];
				//$dcto+=(@$product['cantidad']*@$product['precio']*@$product['dcto'])/100;
				//$iva+=(((@$product['cantidad']*@$product['precio'])-((@$product['cantidad']*@$product['precio']*@$product['dcto'])/100))*@$product['iva'])/100;

				//$precio=(@$product['precio']-((@$product['precio']*@$product['dcto'])/100))*@$product['cantidad'];
				//Resultado total de todos los descuentos
				$dcto+=((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//$dcto+=(@$product['dcto'])*@$product['cantidad'];
				$temporal = $subtotal-$dcto;
				//$temporal = $subtotal-$dcto;

				//$temporal_ = 0;
				//Resultado impuesto venta
				//$per=1+(@$product['iva']/100);
				//$iva+=(float)($precio-($precio/$per));
				//$iva+=(float)($temporal*(@$product['iva']/100));
				$pro_ser = ProductoServicios::find()->where(['codProdServicio'=>$product['cod']])->one();
				if ($pro_ser->exlmpuesto == "Si") {
					$subtotal_+=@$product['cantidad']*@$product['precio'];
					$dcto_+=((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
					//$temporal_ = $subtotal_-$dcto;
					//$iva=(float)($temporal-($temporal*Yii::$app->params['impuesto']));
					//$iva += (float)($subtotal_*(@$product['iva']/100));
					$ivamostrar += ($subtotal_-$dcto_)*Yii::$app->params['impuesto'];
				}

					//Resultado del total a pagar
					$total = ($temporal/*-$dcto*/)+$ivamostrar;
					//$total+=$precio;
				//}
				//else{
					//Resultado del total a pagar
				//	$total = ($subtotal/*-$dcto*/);
				//}

		}
		if($par==true) return array(
								'subtotal'=>$subtotal,
								'dcto'=>$dcto,
								'iva'=>$ivamostrar,
								'total'=>$total
								);
		else
			return  '  <div class="resumen">
    <h3>RESUMEN</h3>
   <table class="table">
    <tbody>
        <tr>
             <td class="detail-view">
                <table>
                	<tr>
                        <th align="left">SUBTOTAL: </th>
                        <td align="right">'.number_format($subtotal,2).'</td>
                    </tr>
				    <tr>
                        <th align="left">TOTAL DESCUENTO: &nbsp;</th>
                        <td align="right">'.number_format($dcto,2).'</td>
                    </tr>
                    <tr>
                        <th align="left">TOTAL IV: </th>
                        <td align="right"><span>'.number_format($ivamostrar,2).'</span></td>
                    </tr>
					 <tr>
                        <th align="left">TOTAL A PAGAR: </th>
                        <td align="right"><span><strong>'.number_format($total,2).'</strong></span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
	</table>
    </div>';
		}

	//----------------------------------------------------------
		public static function getVendedornull() {
			return Yii::$app->session->get('venull');
		}

		public static function setVendedornull($venull) {
			return Yii::$app->session->set('venull', $venull);
		}

	}
