<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_contactos_pedidos".
 *
 * @property integer $idContacto_pedido
 * @property integer $codProveedores
 * @property string $nombre
 * @property string $telefono_ofic
 * @property string $extension
 * @property string $celular
 * @property string $email
 */
class ContactosPedidos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_contactos_pedidos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProveedores', 'nombre', 'telefono_ofic', 'celular', 'email'], 'required'],
            [['codProveedores'], 'integer'],
            [['nombre', 'email'], 'string', 'max' => 80],
            [['telefono_ofic', 'extension', 'celular'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idContacto_pedido' => 'Id Contacto Pedido',
            'codProveedores' => 'Cod Proveedores',
            'nombre' => 'Nombre',
            'telefono_ofic' => 'Telefono Ofic',
            'extension' => 'Extension',
            'celular' => 'Celular',
            'email' => 'Email',
        ];
    }
}
