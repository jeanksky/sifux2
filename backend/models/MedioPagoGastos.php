<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_medio_pago_gastos".
 *
 * @property integer $idMedioPagoGastos
 * @property string $descripcionMedioPagoGastos
 */
class MedioPagoGastos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_medio_pago_gastos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcionMedioPagoGastos'], 'required'],
            [['descripcionMedioPagoGastos'], 'string', 'max' => 14]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMedioPagoGastos' => 'Id Medio Pago Gastos',
            'descripcionMedioPagoGastos' => 'Descripcion Medio Pago Gastos',
        ];
    }
}
