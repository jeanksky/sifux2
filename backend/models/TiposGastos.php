<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_tipos_gastos".
 *
 * @property integer $idTipoGastos
 * @property string $descripcionTipoGastos
 * @property integer $idCategoriaGasto
 * @property string $estadoTipoGastos
 */
class TiposGastos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tipos_gastos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcionTipoGastos', 'idCategoriaGasto', 'estadoTipoGastos'], 'required'],
            [['idCategoriaGasto'], 'integer'],
            [['descripcionTipoGastos'], 'string', 'max' => 50],
            [['estadoTipoGastos'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipoGastos' => 'Id Tipo Gastos',
            'descripcionTipoGastos' => 'Descripcion',
            'idCategoriaGasto' => 'Categoria Gasto',
            'estadoTipoGastos' => 'Estado',
        ];
    }
    
    //funcion para buscar SEARCH dato de otra tabla
     public function attributes()
    {
         return array_merge(parent::attributes(), ['tbl_categoria_gastos.descripcionCategoriaGastos']);
    }//fin de la funcion

    //funcion que busca los parametros que solo tienen como activo en CateriaGastos
    public function getCategoriaGastos()
    {
        return CategoriaGastos::find()
        ->where(['=','estadoCategoriaGastos','activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('descripcionCategoriaGastos ASC')
        ->all();
    }
}
