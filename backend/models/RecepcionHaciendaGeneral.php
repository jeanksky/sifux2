<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_recepcion_hacienda".
 *
 * @property integer $reha_id
 * @property integer $reha_factun_id
 * @property string $reha_tipo_documento
 * @property string $reha_resolucion
 * @property string $reha_detalle
 * @property string $reha_respuesta_hacienda
 * @property string $reha_fecha_hora
 * @property string $reha_clave_recepcion
 * @property resource $reha_xml_recepcion
 * @property resource $reha_xml_respuesta
 */
class RecepcionHaciendaGeneral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_recepcion_hacienda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reha_factun_id', 'reha_tipo_documento', 'reha_resolucion', 'reha_detalle', 'reha_respuesta_hacienda', 'reha_fecha_hora', 'reha_clave_recepcion', 'reha_xml_recepcion', 'reha_xml_respuesta'], 'required'],
            [['reha_factun_id'], 'integer'],
            [['reha_fecha_hora'], 'safe'],
            [['reha_xml_recepcion', 'reha_xml_respuesta'], 'string'],
            [['reha_tipo_documento'], 'string', 'max' => 30],
            [['reha_resolucion'], 'string', 'max' => 25],
            [['reha_detalle'], 'string', 'max' => 210],
            [['reha_respuesta_hacienda'], 'string', 'max' => 2000],
            [['reha_clave_recepcion'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reha_id' => 'Reha ID',
            'reha_factun_id' => 'Reha Factun ID',
            'reha_tipo_documento' => 'Reha Tipo Documento',
            'reha_resolucion' => 'Reha Resolucion',
            'reha_detalle' => 'Reha Detalle',
            'reha_respuesta_hacienda' => 'Reha Respuesta Hacienda',
            'reha_fecha_hora' => 'Reha Fecha Hora',
            'reha_clave_recepcion' => 'Reha Clave Recepcion',
            'reha_xml_recepcion' => 'Reha Xml Recepcion',
            'reha_xml_respuesta' => 'Reha Xml Respuesta',
        ];
    }
}
