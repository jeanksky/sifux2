<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_devoluciones_producto".
 *
 * @property integer $idDevolucion
 * @property integer $idFactura
 * @property string $descripcion_devolucion
 * @property string $estado_devolucion_producto
 * @property string $fecha_hora
 * @property string $usuario
 * @property string $subtotal
 * @property string $descuento
 * @property string $impuesto_venta
 * @property string $total
 */
class DevolucionesProducto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_devoluciones_producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idFactura','idMov'], 'integer'],
            [['fecha_hora'], 'safe'],
            [['subtotal', 'descuento', 'impuesto_venta', 'total'], 'number'],
            [['descripcion_devolucion'], 'string', 'max' => 1000],
            [['estado_devolucion_producto'], 'string', 'max' => 11],
            [['usuario'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDevolucion' => 'Id Devolucion',
            'idFactura' => 'Id Factura',
            'descripcion_devolucion' => 'Descripcion Devolucion',
            'estado_devolucion_producto' => 'Estado Devolucion Producto',
            'fecha_hora' => 'Fecha Hora',
            'usuario' => 'Usuario',
            'subtotal' => 'Subtotal',
            'descuento' => 'Descuento',
            'impuesto_venta' => 'Impuesto Venta',
            'total' => 'Total',
        ];
    }
}
