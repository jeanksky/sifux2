<?php

namespace backend\models;
use backend\models\Clientes;

use Yii;

/**
 * This is the model class for table "tbl_vehiculos".
 *
 * @property integer $placa
 * @property integer $idCliente
 * @property integer $anio
 * @property string $modelo
 * @property string $marca
 * @property string $combustible
 * @property string $motor
 * @property string $cilindrada_motor
 * @property string $version
 *
 * @property TblOrdenServicio[] $tblOrdenServicios
 * @property TblClientes $idCliente0
 */
class Vehiculos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_vehiculos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['placa', /*'idCliente',*/ 'anio', 'modelo', 'marca', 'combustible', 'motor', 'cilindrada_motor'], 'required'],
            [['idCliente', 'anio','modelo', 'marca'], 'integer'],
            //[['modelo', 'marca'], 'string', 'max' => 80],
            [['combustible'], 'string', 'max' => 20],
            [['placa'], 'string', 'max' => 10],
            [['motor'], 'string', 'max' => 50],
            [['cilindrada_motor', 'version'], 'string', 'max' => 40],
            [['placa'],'unique'],
            [['placa'],'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'placa' => 'Placa',
            'idCliente' => 'Cliente',
            'anio' => 'Año',
            'modelo' => 'Modelo',
            'marca' => 'Marca',
            'combustible' => 'Combustible',
            'motor' => 'Motor',
            'cilindrada_motor' => 'Cilindrada motor',
            'version' => 'Versión',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblOrdenServicios()
    {
        return $this->hasMany(TblOrdenServicio::className(), ['placa' => 'placa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
  /*  public function getIdCliente0()
    {
        return $this->hasOne(TblClientes::className(), ['idCliente' => 'idCliente']);
    }*/

    //Para obtener datos de otra tabla enlazada a esta
    //http://artifexsoft.com/mostrar-y-filtrar-por-campos-relacionados-en-yii2/
    //https://vicmonmena.wordpress.com/2015/07/18/relaciones-entre-entidades-parte-ii/
    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_clientes.nombreCompleto','tbl_marcas.nombre_marca','tbl_modelo.nombre_modelo']);
    }
}
