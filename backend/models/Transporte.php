<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_transporte".
 *
 * @property integer $id_transporte
 * @property string $nombre_transporte
 * @property string $telefono
 * @property string $direccion
 * @property string $sitio_web
 * @property string $contacto
 * @property string $email_contacto
 */
class Transporte extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_transporte';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_transporte'], 'required'],
            [['nombre_transporte'], 'string', 'max' => 40],
            [['telefono'], 'string', 'max' => 12],
            [['direccion'], 'string', 'max' => 100],
            [['sitio_web', 'email_contacto'], 'string', 'max' => 80],
            [['contacto'], 'string', 'max' => 60],
            [['estado'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_transporte' => 'Id Transporte',
            'nombre_transporte' => 'Nombre del transporte',
            'telefono' => 'Teléfono',
            'direccion' => 'Dirección',
            'sitio_web' => 'Sitio Web',
            'contacto' => 'Contacto',
            'email_contacto' => 'Email del contacto',
            'estado' => 'Estado',
        ];
    }
}
