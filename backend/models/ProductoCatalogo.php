<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_producto_catalogo".
 *
 * @property integer $id_Prod_marca
 * @property string $codProdServicio
 * @property integer $id_marcas
 */
class ProductoCatalogo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_producto_catalogo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_marcas'], 'integer'],
            [['codProdServicio','codigo'], 'required'],
            [['codProdServicio','codigo'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_Prod_marca' => 'Id  Prod Marca',
            'codProdServicio' => 'Cod Prod Servicio',
            'id_marcas' => 'Id Marcas',
            'codigo' => 'Código'
        ];
    }
}
