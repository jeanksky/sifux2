<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;
use backend\models\Compra;//Prueba
use backend\models\EncabezadoPrefactura;
	class Compra_update
	{
		public static function getContenidoComprau() {
			if(is_string(Yii::$app->session->get('comprau'))){
				return JSON::decode(Yii::$app->session->get('comprau'), true);
			}
			else
				return Yii::$app->session->get('comprau');
		}

		public static function setContenidoComprau($comprau) {
			return Yii::$app->session->set('comprau', JSON::encode($comprau));
		}
		//----------------------------------------------------------
		/*public static function getContenidoCheque() {
			if(is_string(Yii::$app->session->get('cheque')))
				return CJSON::decode(Yii::$app->session->get('cheque'), true);
			else
				return Yii::$app->session->get('cheque');
		}

		public static function setContenidoCheque($cheque) {
			return Yii::$app->session->set('cheque', CJSON::encode($cheque));
		}*/
		//----------------------------------------------------------
		/*public static function getProveedor() {
			return Yii::$app->session->get('proveedor');
		}

		public static function setProveedor($proveedor) {
			return Yii::$app->session->set('proveedor', $proveedor);
		}*/
		//----------------------------------------------------------
		public static function getCabezaFactura() {
			return Yii::$app->session->get('cabf');
		}
		public static function setCabezaFactura($cabf) {
			return Yii::$app->session->set('cabf', $cabf);
		}
		//----------------------------------------------------------
		public static function getVendedoru() {
			return Yii::$app->session->get('vendedoru');
		}
		public static function setVendedoru($vendedoru) {
			return Yii::$app->session->set('vendedoru', $vendedoru);
		}
		//----------------------------------------------------------
		public static function getClienteu() {
			return Yii::$app->session->get('clienteu');
		}

		public static function setClienteu($clienteu) {
			return Yii::$app->session->set('clienteu', $clienteu);
		}
		//----------------------------------------------------------
		public static function getEstadoFacturau() {
			return Yii::$app->session->get('estadou');
		}

		public static function setEstadoFacturau($estadou) {
			return Yii::$app->session->set('estadou', $estadou);
		}
		//----------------------------------------------------------
		public static function getFechaInicialu() {
			return Yii::$app->session->get('fechau');
		}

		public static function setFechaInicialu($fechau) {
			return Yii::$app->session->set('fechau', $fechau);
		}
		//----------------------------------------------------------
		public static function getPagou() {
			return Yii::$app->session->get('pagocu');
		}

		public static function setPagou($pagocu) {
			return Yii::$app->session->set('pagocu', $pagocu);
		}
		//----------------------------------------------------------

		public static function getAgenteComisionu() {
			return Yii::$app->session->get('ageComi');
		}

		public static function setAgenteComisionu($ageComi) {
			return Yii::$app->session->set('ageComi', $ageComi);
		}

		//----------------------------------------------------------

		public static function getMediou() {
			return Yii::$app->session->get('mediou');
		}

		public static function setMediou($mediou) {
			return Yii::$app->session->set('mediou', $mediou);
		}
		//----------------------------------------------------------
		public static function getCuentau() {
			return Yii::$app->session->get('cuentau');
		}

		public static function setCuentau($cuentau) {
			return Yii::$app->session->set('cuentau', $cuentau);
		}
		//----------------------------------------------------------
		public static function getFechadepo() {
			return Yii::$app->session->get('fechadepo');
		}

		public static function setFechadepo($fechadepo) {
			return Yii::$app->session->set('fechadepo', $fechadepo);
		}
		//----------------------------------------------------------
		public static function getComprobau() {
			return Yii::$app->session->get('comprobacionu');
		}

		public static function setComprobau($comprobacionu) {
			return Yii::$app->session->set('comprobacionu', $comprobacionu);
		}
		//----------------------------------------------------------//Estas 2 funciones son unicas para obtener el total desde el index de cabeza-caja
		public static function getTotalauxiliar() {
			return Yii::$app->session->get('totalu');
		}

		public static function setTotalauxiliar($totalu) {
			return Yii::$app->session->set('totalu', $totalu);
		}
		//----------------------------------------------------------
		/*public static function getDoc() {
			return Yii::$app->session->get('doc');
		}

		public static function setDoc($doc) {
			return Yii::$app->session->set('doc', $doc);
		}*/
		//----------------------------------------------------------
		public static function getTotalu($par=false) {
			$subtotal=$total=$iva=$dcto=$dcto_imp=$descuento=0;
			$prod_con_impuesto = 0;
			$prod_sin_impuesto = 0;
			if(@$comprau=Compra_update::getContenidoComprau())
			foreach($comprau as $product){
				//obtengo el subtotal
				$subtotal += @$product['cantidad']*@$product['precio'];

				//obtengo descuento de los productos que no tienen impuestos
				$dcto += $product['iva'] != 0 ? 0 : $product['dcto']*@$product['cantidad'];//((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//obtengo descuento de los productos que tienen impuestos
				$dcto_imp += $product['iva'] == 0 ? 0 : $product['dcto']*@$product['cantidad'];//((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//suma de ambos descuento
				$descuento = $dcto + $dcto_imp;
				//obtengo la suma del precio + cantidad de los productos sin impuesto
				$prod_sin_impuesto += $product['iva'] != 0 ? 0 : @$product['cantidad']*$product['precio'];
				//obtengo la suma del precio + cantidad de los productos con impuesto
				$prod_con_impuesto += $product['iva'] == 0 ? 0 : @$product['cantidad']*$product['precio'];

				//Para obtener el impuesto resto el descuento de los productos con descuento con los productos con impuestos
				//luego los multiplico por el 0.13 que es el 13% del impuesto de venta
				$iva = ($prod_con_impuesto - $dcto_imp) * Yii::$app->params['impuesto'];
				//tp es el total de productos con y sin impuesto
				$tp = $prod_sin_impuesto + $prod_con_impuesto;
				//el total a pagar resto los dos descuentos al tp para luego sumarlo al iva
				$total = ($tp-($dcto+$dcto_imp))+$iva;

		}
		if($par==true) {
			return array(
				'subtotal'=>number_format(($subtotal),2),
				'dcto'=>number_format(($descuento),2),
				'iva'=>number_format(($iva),2),
				'total'=>$total
										);}
		else{
			$idCab = Compra::getCabezaFactura(); //desde auxiliar del archivo Compra
			$subtotal_ = number_format($subtotal,2);
			$dcto_ = number_format($descuento,2);
			$iva_ = number_format($iva,2);
			$total_ = number_format($total,2);
			$sql = "UPDATE tbl_encabezado_factura SET subtotal = :subtotal, porc_descuento = :descuento, iva = :iva, total_a_pagar = :total WHERE idCabeza_Factura = :idCab";
		    $command = \Yii::$app->db->createCommand($sql);
		    $command->bindParam(":subtotal", $subtotal);
		    $command->bindParam(":descuento", $descuento);
		    $command->bindParam(":iva", $iva);
		    $command->bindParam(":total", $total);
		    $command->bindValue(":idCab", $idCab);
		    $command->execute();

			$servicio = ($subtotal - $descuento)*(Yii::$app->params['porciento_servicio']/100);
			$model = EncabezadoPrefactura::findOne($idCab);
			if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
				return  '
				<div class="resumen">
				   <h4>SERVICIO 10% (EX):<span style="float:right">'.number_format($servicio,2).'</span></h4>
				   <h3>RESUMEN</h3><hr>
					 <h4>SUBTOTAL:<span style="float:right">'.number_format($subtotal+$servicio,2).'</span></h4>
					 <h4>TOTAL DESCUENTO:<span style="float:right">'.number_format($descuento,2).'</span></h4>
					 <h4>TOTAL IV:<span style="float:right">'.number_format($iva,2).'</span></h4>
					 <h4>TOTAL A PAGAR:<span style="float:right">'.number_format($total+$servicio,2).'</span></h4>
				</div>';
			} else {
				return  '
				<div class="resumen">
				   <h3>RESUMEN</h3><hr>
					 <h4>SUBTOTAL:<span style="float:right">'.number_format($subtotal,2).'</span></h4>
					 <h4>TOTAL DESCUENTO:<span style="float:right">'.number_format($descuento,2).'</span></h4>
					 <h4>TOTAL IV:<span style="float:right">'.number_format($iva,2).'</span></h4>
					 <h4>TOTAL A PAGAR:<span style="float:right">'.number_format($total,2).'</span></h4>
				</div>';
			}

			}

		}



	}
