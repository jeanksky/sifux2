<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Xml_file extends Model
{
  public $archivo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['archivo'], 'safe'],
            [['archivo'], 'required']
        ];
    }
}
