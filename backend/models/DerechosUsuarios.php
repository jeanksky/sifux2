<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_derechos_usuarios".
 *
 * @property integer $idDerechoUsuario
 * @property integer $idUsuario
 * @property string $caja
 * @property string $recepcion
 * @property string $mecanica
 * @property string $Otros
 *
 * @property User $idUsuario0
 */
class DerechosUsuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_derechos_usuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUsuario'], 'required'],
            [['idUsuario'], 'integer'],
            [['caja', 'recepcion', 'mecanica', 'Otros'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDerechoUsuario' => 'Id Derecho Usuario',
            'idUsuario' => 'Id Usuario',
            'caja' => 'Caja',
            'recepcion' => 'Recepcion',
            'mecanica' => 'Mecanica',
            'Otros' => 'Otros',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return  $this->hasOne(User::className(), ['id' => 'idUsuario']);
    }
    
}
