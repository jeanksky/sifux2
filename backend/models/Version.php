<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_version".
 *
 * @property string $codVersion
 */
class Version extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_version';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codVersion'], 'required'],
            [['codVersion'], 'unique'],
            [['codVersion'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codVersion' => 'Versión',
        ];
    }
}
