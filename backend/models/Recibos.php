<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_recibos".
 *
 * @property integer $idRecibo
 * @property string $fechaRecibo
 * @property string $monto
 */
class Recibos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_recibos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fechaRecibo'], 'safe'],
            [['monto'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRecibo' => 'Id Recibo',
            'fechaRecibo' => 'Fecha Recibo',
            'monto' => 'Monto',
        ];
    }
}
