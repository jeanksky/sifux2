<?php
namespace backend\models;
//--------------Esta clase me permite darle una nueva contraseña a un usuario ya sea cliente o Funcionario y notificarle al correo electronico
use backend\models\Usuarios;
use backend\models\UsuariosClientes;
use common\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\Url;
use backend\models\Funcionario;
use backend\models\Clientes;
//use backend\models\UsuariosDerechos;

use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\ContactForm;
use backend\models\Empresa;
/**
 * Signup form
 */
class SignupForm extends Model
{
   /* public $username;
    public $email;
    public $password;
    public $nombre;
    public $estado;
    public $codigo;*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
    /*      ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Ya este nombre de usuario está siendo utilizado.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Ya este email está siendo utilizado.'],
*/
           /* ['password', 'match', 'pattern' => "/^.{4,16}$/", 'message' => 'Mínimo 4 y máximo 16 caracteres'],
            ['password', 'required'],
            ['password', 'string', 'min' => 4],*/
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Contraseña',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signupU()
    {
     //   if ($this->validate()) {
            $userP = new Usuarios();
            $model_f = new Funcionario();

            $userP->username = $model_f->getUsername();//$this->username;
            $usuario = $model_f->getUsername();

            $userP->email = $model_f->getEmail();//$this->email;
            $correo = $model_f->getEmail();

            $pass = $this->randKey("abcdef0123456789", 8);
            $userP->setPassword($pass/*$model_f->getPass()*/);
                      //$userP->setPassword($this->password/*$model_f->getPass()*/);
            $userP->generateAuthKey();
            $userP->nombre = $model_f->getName();
            $userP->codigo = $model_f->getEstado();
                     //  $userP->codigo = $model_f->getCodigo();
            $userP->tipo_usuario = $model_f->getTipoUsuario();
            $userP->verification_code = $pass;
            //------Envio email y password por email------------
            //Creamos el mensaje que será enviado a la cuenta de correo del usuario
            // $subject = "Se acaba de crear un usuario para usted en el sistema";
            // $content = "<p>Los siguientes datos les han sido proporcionado</p>";
            // $content .= "<p><strong>Usuario: </strong>".$usuario."</p>";
            // $content .= "<p><strong>Contraseña: </strong>".$pass."</p>";
            // $content .= "<p>Esta contraseña es generada por el sistema, se recomienda que pronto cambie su contraseña.</p>";
            // $content .= "<p><a href='http://localhost/ProyectosNelux/aplicaciones_web/lutux/backend/web/index.php?r=site%2Frecoverpass'>Quiero cambiar mi contraseña ya...</a></p>";
            if ($userP->save()) {
            $subject = "Bienvenido(a) a lutuX";

            $content = '
            <div class="table-responsive">
            <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
            <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
            <br>
            <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
              <tbody><tr>
                <td align="center" valign="top">
                  <table class="text-justify" width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="text-align:justify;border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                    <tr>
                      <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                        <br><a ><img width="300" height=""src="'.Url::base(true).'/img/logo_sistema.png" class="CToWUd"></a>
                    </tr>
                    <tbody><tr>
                        <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                        <br>
                        <p style="color:#ffffff;font-family: Segoe UI;font-size:20px;line-height:1.5;margin:0 0 25px;text-align:center!important" align="center"><strong>Bienvenido(a) '.$model_f->getName().' a lutuX</strong></p>
                        </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="text-align:justify;background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">

                        <br><br>
                        <p style="color:#0e4595;font-family: Segoe UI;font-size:24px;line-height:1.5;margin:0 30px 25px;text-align:center!important" align="center"><strong></strong> </p><br>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:15px;line-height:1.5;margin:0 20px 25px;text-align:justify!important" align="left">Estimado(a) '.$model_f->getName().', <br><br>Le informamos que se le ha creado una cuenta en nuestro sistema de control vehicular.
            <p style="text-align : justify!important;color:#0e4595;font-family: Segoe UI;font-size:15px;line-height:1.5;margin:0 40px 25px;text-align:justify!important" align="left"><strong>Información de su cuenta:</strong><br><br>Propietario(a): '.$model_f->getName().'
            <br>Usuario: '.$usuario.'<br>Contraseña: '.$pass.'</p><br>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:15px;line-height:1.5;margin:0 20px 25px;text-align:justify!important" align="left">
            Nota: La contraseña es generada por el sistema, se le recomienda cambiarla por una de su preferencia.</p>
            <br>
            <div style="text-align:center!important" align="center"><a href="'.Url::home(true).'?r=site%2Frecoverpass" rel="nofollow" style="background:#5CB85C;border:1px solid rgba(92, 184, 92);border-radius:5px;color:#fff;display:inline-block;font-size:13px;font-weight:bold;min-height:20px;padding:9px 15px;text-decoration:none" target="_blank">CAMBIAR CONTRASEÑA</a></div>
            <br><br><br><br>

                      </td>
                    </tr>
                  </tbody> </table>
                </td>
              </tr>
            </tbody> </table>
            <br><br>
            </div>
        </div> </div>
            ';
            //Enviamos el correo
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
            ->setTo($correo)
            ->setFrom($empresa->email_notificaciones)
            ->setSubject($subject)
            ->setHtmlBody($content)
            ->send();
            //------------------------------------------

                return $userP;
            }
    //    }

        return null;
    }

    public function signupC()
    {
        $userC = new Usuarios();
        $model_c = new Clientes();
        $dirbase = Url::home(true);
        $dirbaselimp = substr($dirbase, 0, -22);

            $userC->username = $model_c->getUsername();//$this->username;
            $usuario = $model_c->getUsername();

            $userC->email = $model_c->getEmail();//$this->email;
            $correo = $model_c->getEmail();

            $pass = $this->randKey("abcdef0123456789", 8);
            $userC->setPassword($pass);

            $userC->generateAuthKey();
            $userC->nombre = $model_c->getfullName();
            $userC->codigo = 1;

            $userC->tipo_usuario = 'Cliente';
            $userC->verification_code = $pass;
            if ($userC->save()) {
            //------Envio email y password por email------------
            //Creamos el mensaje que será enviado a la cuenta de correo del usuario
            $subject = "Bienvenido(a) a lutuX";

            $content = '
            <div class="table-responsive">
            <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
            <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
            <br>
            <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
              <tbody><tr>
                <td align="center" valign="top">
                  <table class="text-justify" width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="text-align:justify;border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                    <tr>
                      <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                        <br><a ><img width="300" height=""src="'.Url::base(true).'/img/logo_sistema.png" class="CToWUd"></a>
                    </tr>
                    <tbody><tr>
                        <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                        <br>
                        <p style="color:#ffffff;font-family: Segoe UI;font-size:20px;line-height:1.5;margin:0 0 25px;text-align:center!important" align="center"><strong>Bienvenido(a) '.$model_c->getfullName().' a lutuX</strong></p>
                        </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="text-align:justify;background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">

                        <br><br>
                        <p style="color:#0e4595;font-family: Segoe UI;font-size:24px;line-height:1.5;margin:0 30px 25px;text-align:center!important" align="center"><strong></strong> </p><br>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:15px;line-height:1.5;margin:0 20px 25px;text-align:justify!important" align="left">Estimado(a) '.$model_c->getfullName().', <br><br>Le informamos que se le ha creado una cuenta en nuestro sistema de control vehicular. <br><br> Le agradecemos por preferirnos, para nosotros es un gusto servirle.
            Sabemos lo importante que es para usted tener los detalles de su vehículo, le hemos creado un perfil en nuestro sistema,
            donde podrá consultar sus datos personales y los datos del servicio que le hemos brindado a su(s) vehículo(s).
             </p>
            <p style="text-align : justify!important;color:#0e4595;font-family: Segoe UI;font-size:15px;line-height:1.5;margin:0 40px 25px;text-align:justify!important" align="left"><strong>Información de su cuenta:</strong><br><br>Propietario(a): '.$model_c->getfullName().'
            <br>Usuario: '.$usuario.'<br>Contraseña: '.$pass.'</p><br>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:15px;line-height:1.5;margin:0 20px 25px;text-align:justify!important" align="left">
            Nota: La contraseña es generada por el sistema, se le recomienda cambiarla por una de su preferencia.</p>
            <br>
            <div style="text-align:center!important" align="center"><a href="'.$dirbaselimp.'/frontend/web/index.php?r=site%2Frecoverpass" rel="nofollow" style="background:#5CB85C;border:1px solid rgba(92, 184, 92);border-radius:5px;color:#fff;display:inline-block;font-size:13px;font-weight:bold;min-height:20px;padding:9px 15px;text-decoration:none" target="_blank">CAMBIAR CONTRASEÑA</a></div>
            <br><br><br><br>

                      </td>
                    </tr>
                  </tbody> </table>
                </td>
              </tr>
            </tbody> </table>
            <br><br>
            </div>
        </div> </div>
            ';



            //$content = "<h1><strong>Le damos la bienvenida a nuestra empresa.</strong></h1>";
            //$content .= "<p>Estimado ".$model_c->getfullName()." le agradecemos por preferirnos, para nosotros es un gusto servirle.
           // Y como sabemos lo importante que es para usted tener los detalles de su vehículo hemos creado un perfil en nuestro sistema para usted,
           // donde puedes consultar tus datos personales y los datos del servício que le hemos brindado a su(s) vehículo(s).
           //  </p>";
           // $content .= "<p><strong>Los siguientes datos les han sido proporcionado<strong></p>";
           // $content .= "<p><strong>Propietario: </strong>".$model_c->getfullName()."</p>";
            //$content .= "<p><strong>Usuario: </strong>".$usuario."</p>";
           // $content .= "<p><strong>Contraseña: </strong>".$pass."</p>";
           // $content .= "<p>Esta contraseña es generada por el sistema, se recomienda que pronto cambie su contraseña.</p>";
            //$content .= "<br><p><a href='http://localhost/ProyectosNelux/aplicaciones_web/lutux/backend/web/index.php?r=site%2Frecoverpass'>Deseo cambiar mi contraseña ya...</a></p>";

            //Enviamos el correo
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
            ->setTo($correo)
            ->setFrom($empresa->email_notificaciones)
            ->setSubject($subject)
            ->setHtmlBody($content)
            ->send();
            //------------------------------------------

                return $userC;
            }
    //    }

        return null;
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
}
