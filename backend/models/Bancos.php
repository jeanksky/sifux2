<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_bancos".
 *
 * @property integer $idBanco
 * @property string $nombreBanco
 */
class Bancos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_bancos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreBanco'], 'required'],
            [['nombreBanco'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBanco' => 'Id Banco',
            'nombreBanco' => 'Nombre del Banco',
        ];
    }
}
