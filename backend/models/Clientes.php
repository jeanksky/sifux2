<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_clientes".
 *
 * @property integer $idCliente
 * @property string $tipoIdentidad
 * @property string $nombreCompleto
 * @property string $identificacion
 * @property string $genero
 * @property string $fechNacimiento
 * @property string $telefono
 * @property string $fax
 * @property string $celular
 * @property string $email
 * @property string $direccion
 * @property string $credito
 * @property string $documentoGarantia
 * @property string $diasCredito
 * @property string $autorizacion
 * @property double $montoMaximoCredito
 * @property double $montoTotalEjecutado
 * @property string $estadoCuenta
 *
 * @property TblEncabezadoFactura[] $tblEncabezadoFacturas
 * @property TblOrdenServicio[] $tblOrdenServicios
 * @property TblVehiculos[] $tblVehiculos
 */
class Clientes extends \yii\db\ActiveRecord
{
  public $password;
  public $montoTotalEjecutado_ot;
  public $dias_ot;
  public $autorizacion_ot;
  public $estado_ot;
    /**
     * @inheritdoc
     */
    //public $username; //para confirmar eliminacion de cllientes
    //public $password; //para confirmar eliminacion de cllientes

    public static function tableName()
    {
        return 'tbl_clientes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [['tipoIdentidad', 'nombreCompleto', 'identificacion',
        /*genero', 'fechNacimiento', 'telefono', 'fax',*/
        'celular', 'email', /*'direccion',*/ 'credito',
        /*'provincia', 'canton', 'distrito'*/
        /*,'documentoGarantia', 'diasCredito', 'autorizacion',
        'montoMaximoCredito', 'montoTotalEjecutado', 'estadoCuenta'*/], 'required'],
             [['fechNacimiento'], 'default', 'value' => function ($model, $attribute) {
              if ($this->tipoIdentidad == 'Jurídica'){
        return date('Y-m-d', strtotime($attribute === '-0001-11-30' ? '' : ''));}

          elseif ( $this->tipoIdentidad == 'Física'){
            return date('Y-m-d', strtotime($attribute === '-0001-11-30' ? 'fechNacimiento' : ''));
          }}
          ],
            [['montoMaximoCredito', 'montoTotalEjecutado'], 'number'],
            [['ot','iv'], 'safe'],
            [['tipoIdentidad'], 'string', 'max' => 9],
            [['nombreCompleto'], 'string', 'max' => 490],
            [['identificacion'], 'string', 'max' => 20],
            [['genero', 'telefono', 'fax', 'celular', 'estadoCuenta'], 'string', 'max' => 10],
            [['email'], 'string', 'max' => 130],
            [['provincia', 'canton', 'distrito', 'barrio', 'direccion'], 'string', 'max' => 130],
            [['credito', 'documentoGarantia', 'diasCredito', 'autorizacion'], 'string', 'max' => 2],
            [['identificacion', 'email'], 'unique'],

        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCliente' => 'Código',
            'tipoIdentidad' => 'Tipo entidad*',
            'nombreCompleto' => 'Propietario*',
            'identificacion' => 'Identificación*',
            'genero' => 'Género',
            'fechNacimiento' => 'Fecha de nacimiento',
            'telefono' => 'Tel.Oficina',
            'fax' => 'Fax',
            'celular' => 'Tel.Movil*',
            'email' => 'E-mail*',
            'provincia' => 'Provin. / estado',
            'canton' => 'Cantón',
            'distrito' => 'Distrito',
            'barrio' => 'Pueblo / Barrio',
            'ot' => '¿Requiere O.T?',
            'iv' => 'Impuesto',
            'direccion' => 'Dirección',
            'credito' => '¿Requiere Crédito?',
            'documentoGarantia' => 'Documento garantía',
            'diasCredito' => 'Días crédito',
            'autorizacion' => 'Autorización de facturas vencidas ',
            'montoMaximoCredito' => 'Monto máximo crédito',
            'montoTotalEjecutado' => 'Monto total ejecutado',
            'estadoCuenta' => 'Estado cuenta',
            'montoTotalEjecutado_ot' => '',
            'dias_ot' => 'Días O.T',
            'autorizacion_ot' => 'Autorización O.T',
            'estado_ot' => 'Estado O.T'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblEncabezadoFacturas()
    {
        return $this->hasMany(TblEncabezadoFactura::className(), ['idCliente' => 'idCliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblOrdenServicios()
    {
        return $this->hasMany(TblOrdenServicio::className(), ['idCliente' => 'idCliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblVehiculos()
    {
        return $this->hasMany(TblVehiculos::className(), ['idCliente' => 'idCliente']);
    }



/*
     public function beforeSave()
  {
   if(parent::beforeSave())
  {
   $this->fechNacimiento= Yii::app()->dateformatter->format("yyyy-MM-dd",$this->fechNacimiento);
   parent::beforeSave();

   return true;
  }
  else return false;
  }


  public function afterFind()
  {
   $this->fechNacimiento= Yii::app()->dateformatter->format("dd-MM-yyyy",$this->fechNacimiento);
   parent::afterFind();
  }*/

    public function getStatusList()
        {

                    return $this->nombreCompleto;

        }
    public function getfullName()
        {
                if ($this->load(Yii::$app->request->post())){
                    return $this->nombreCompleto;
                }
        }

    public function getUsername()
        {
            if ($this->load(Yii::$app->request->post())){
                return $this->identificacion;
            }
        }

    public function getEmail()
        {
            if ($this->load(Yii::$app->request->post())){
                return $this->email;
            }
        }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)&&($this->fechNacimiento != '-0001-11-30')) {
          return $this->fechNacimiento = date('Y-m-d', strtotime($this->fechNacimiento));

        } else {
            return  $this->fechNacimiento = '';

        }
    }

     // public function afterFind()
     //    {
     //        //PHP dates are displayed as dd/mm/yyyy
     //        //MYSQL dates are stored as yyyy-mm-dd
     //        $this->fechNacimiento = date('d-m-Y', strtotime($this->fechNacimiento));

     //       return parent::afterFind();

     //    }

    public function afterFind()
      {

      // $this->fechNacimiento = date('d-m-Y', strtotime( $this->fechNacimiento));
      //  parent::afterFind();

        if ($this->tipoIdentidad === 'Física') {

             $this->fechNacimiento = date('d-m-Y', strtotime( $this->fechNacimiento));
             return parent::afterFind();

        } else {

             return $this->fechNacimiento = '';
        }

      }

    protected function getPlaca($idCliente){
     $datas = Vehiculos::find()->where(['idCliente'=>$idCliente])->all();
     return $this->MapData($datas,'placa','placa');

 }

    protected function MapData($datas,$fieldId,$fieldName){
     $obj = [];
     foreach ($datas as $key => $value) {
         array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}]);
     }
     return $obj;
 }

 public static function getNombreClienteOt()
   {
       return Clientes::find()
       ->where(['=','ot','Si'])
       //->andwhere(['=','tipo','Servicio'])
       ->orderBy('idCliente ASC')
       ->asArray()
       ->all();
   }

}
