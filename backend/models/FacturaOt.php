<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_factura_ot".
 *
 * @property integer $idFot
 * @property integer $idCabeza_Factura
 * @property integer $idCliente
 * @property string $placa
 * @property integer $codMarca
 * @property integer $codModelo
 * @property string $codVersion
 * @property integer $ano
 * @property string $motor
 * @property string $cilindrada
 * @property string $combustible
 * @property string $transmision
 */
class FacturaOt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_factura_ot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCabeza_Factura', 'idCliente', 'codMarca', 'codModelo', 'ano'], 'integer'],
            [['placa'], 'string', 'max' => 10],
            [['codVersion', 'cilindrada', 'transmision'], 'string', 'max' => 40],
            [['motor'], 'string', 'max' => 50],
            [['combustible'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFot' => 'Id Fot',
            'idCabeza_Factura' => 'Id Cabeza  Factura',
            'idCliente' => 'Id Cliente',
            'placa' => 'Placa',
            'codMarca' => 'Cod Marca',
            'codModelo' => 'Cod Modelo',
            'codVersion' => 'Cod Version',
            'ano' => 'Ano',
            'motor' => 'Motor',
            'cilindrada' => 'Cilindrada',
            'combustible' => 'Combustible',
            'transmision' => 'Transmision',
        ];
    }
}
