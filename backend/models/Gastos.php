<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_gastos".
 *
 * @property integer $idGastos
 * @property integer $idProveedorGastos
 * @property integer $idTipoGastos
 * @property integer $idMedioPagoGastos
 * @property integer $idBancos
 * @property integer $idCuentaBancariaLocal
 * @property string $numeroDocumentoTransferencias
 * @property string $numeroCheque
 * @property integer $digitosTarjeta
 * @property string $numeroAutorizacionTarjeta
 * @property string $numeroDocumento
 * @property string $fechaDocumento
 * @property string $fechaPago
 * @property string $detalleGasto
 * @property string $fechaRegistro
 * @property string $usuarioRegistra
 * @property string $fechaAplicada
 * @property string $usuarioAplica
 * @property string $estadoGasto
 */
class Gastos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_gastos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProveedorGastos', 'idTipoGastos', 'idMedioPagoGastos', 'numeroDocumento', 'fechaDocumento', 'fechaPago', 'detalleGasto', 'fechaRegistro', 'usuarioRegistra','montoGasto'], 'required'],
            [['idProveedorGastos', 'idTipoGastos', 'idMedioPagoGastos', 'idBancos', 'idCuentaBancariaLocal', 'digitosTarjeta'], 'integer'],
            [['fechaDocumento', 'fechaPago', 'fechaRegistro', 'fechaAplicada','subtotal',
'descuento',
'impuesto'], 'safe'],
            [['numeroDocumentoTransferencias', 'numeroCheque', 'usuarioAplica'], 'string', 'max' => 50],
            [['numeroAutorizacionTarjeta'], 'string', 'max' => 20],
            [['numeroDocumento'], 'string', 'max' => 50],
            [['detalleGasto'], 'string', 'max' => 100],
            [['estadoGasto'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idGastos' => 'Id Gastos',
            'idProveedorGastos' => 'Proveedor Gastos',
            'idTipoGastos' => 'Tipo Gastos',
            'idMedioPagoGastos' => 'Medio Pago Gastos',
            'idBancos' => 'Bancos',
            'idCuentaBancariaLocal' => 'Cuenta Bancaria Local',
            'numeroDocumentoTransferencias' => 'N° Documento Transferencias',
            'numeroCheque' => 'Numero Cheque',
            'digitosTarjeta' => 'Digitos Tarjeta',
            'numeroAutorizacionTarjeta' => 'N° Autorizacion Tarjeta',
            'numeroDocumento' => 'N° Documento (Factura)',
            'subtotal' => 'Subtotal',
            'descuento' => 'Descuento',
            'impuesto' => 'Impuesto Venta',
            'montoGasto' => 'Total',
            'fechaDocumento' => 'Fecha Documento',
            'fechaPago' => 'Fecha Pago',
            'detalleGasto' => 'Detalle Gasto',
            'fechaRegistro' => 'Fecha Registro',
            'usuarioRegistra' => 'Usuario Registra',
            'fechaAplicada' => 'Fecha de  Aplicación',
            'usuarioAplica' => 'Usuario Aplica',
            'estadoGasto' => 'Estado Gasto',
        ];
    }

      public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_proveedores_gastos.nombreEmpresa', 'tbl_medio_pago_gastos.descripcionMedioPagoGastos']);
    }

       public function afterFind()
      {
            $this->fechaRegistro = date('d-m-Y', strtotime( $this->fechaRegistro));
            $this->fechaAplicada = date('d-m-Y', strtotime( $this->fechaAplicada));
            $this->fechaDocumento = date('d-m-Y', strtotime( $this->fechaDocumento));
            $this->fechaPago = date('d-m-Y', strtotime( $this->fechaPago));
           /* if ($this->fecha_documento == '01-01-1970') {
                 $this->fecha_documento = '';
             }*/
            return parent::afterFind();

      }

      public function beforeSave($insert)
      {
              $this->fechaDocumento  = date('Y-m-d', strtotime( $this->fechaDocumento ));
              $this->fechaPago = date('Y-m-d', strtotime( $this->fechaPago));
              $this->fechaAplicada = date('Y-m-d', strtotime( $this->fechaAplicada));
              return parent::beforeSave($insert);
      }

    //funcion que busca los parametros que solo tienen como activo en ProveedorGastos
    public function getProveedorGastos()
    {
        return ProveedoresGastos::find()
        ->where(['=','estado','activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('nombreEmpresa ASC')
        ->all();
    }


    //funcion que busca los parametros que solo tienen como activo en TioGastos
    public function getTioGastos()
    {
        return TiposGastos::find()
        ->where(['=','estadoTipoGastos','activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('descripcionTipoGastos ASC')
        ->all();
    }

     //funcion que busca los parametros que solo tienen como activo en MedioPagoGastos
    public function getMedioPagoGastos()
    {
        return MedioPagoGastos::find()
        //->where(['=','estadoTipoGastos','activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('descripcionMedioPagoGastos ASC')
        ->all();
    }

    //funcion que busca los parametros que solo tienen como activo en MedioPagoGastos
    public function getEntidadFinanciera()
    {
        return EntidadesFinancieras::find()
        //->where(['=','estadoTipoGastos','activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('descripcion ASC')
        ->all();
    }


    public function getCuentaBancaria()
    {
        return CuentasBancarias::find()
        //->where(['=','estadoTipoGastos','activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('numero_cuenta ASC')
        ->all();
    }

}
