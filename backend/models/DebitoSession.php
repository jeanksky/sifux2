<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;

class DebitoSession
{
	public static function getIDproveedor() {
		return Yii::$app->session->get('deb_idproveedor');
	}
	public static function setIDproveedor($deb_idproveedor) {
		return Yii::$app->session->set('deb_idproveedor', $deb_idproveedor);
	}
	//----------------------------------------------------------
	public static function getProveedor() {
		return Yii::$app->session->get('deb_proveedor');
	}
	public static function setProveedor($deb_proveedor) {
		return Yii::$app->session->set('deb_proveedor', $deb_proveedor);
	}
	//----------------------------------------------------------
	public static function getTramiteupdate() {
		return Yii::$app->session->get('idTramite_update');
	}
	public static function setTramiteupdate($idTramite_update) {
		return Yii::$app->session->set('idTramite_update', $idTramite_update);
	}
	//----------------------------------------------------------
	public static function getIDproveedor_tramite_upd() {
		return Yii::$app->session->get('deb_idproveedor_tramite_upd');
	}
	public static function setIDproveedor_tramite_upd($deb_idproveedor_tramite_upd) {
		return Yii::$app->session->set('deb_idproveedor_tramite_upd', $deb_idproveedor_tramite_upd);
	}
}
