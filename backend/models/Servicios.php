<?php

namespace backend\models;

use Yii;
use backend\models\Familia;

/**
 * This is the model class for table "tbl_producto_servicios".
 *
 * @property integer $codProdServicio
 * @property string $tipo
 * @property integer $numFacturaProv
 * @property string $nombreProductoServicio
 * @property integer $cantidadInventario
 * @property integer $cantidadMinima
 * @property integer $codFamilia
 * @property double $precioCompra
 * @property integer $porcentUnidad
 * @property double $precioVenta
 * @property string $exlmpuesto
 * @property double $precioVentaImpuesto
 * @property double $anularDescuento
 * @property double $precioMinServicio
 *
 * @property TblDetalleFacturas[] $tblDetalleFacturas
 * @property TblFamilia $codFamilia0
 */
class Servicios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_producto_servicios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProdServicio', 'nombreProductoServicio'], 'unique'],
           // [['tipo', 'numFacturaProv', 'nombreProductoServicio', 'cantidadInventario', 'cantidadMinima', 'codFamilia', 'precioCompra', 'porcentUnidad', 'precioVenta', 'exlmpuesto', 'precioVentaImpuesto', 'anularDescuento', 'precioMinServicio'], 'required'],
            [['tipo', 'nombreProductoServicio', 'codFamilia','precioMinServicio', 'codProdServicio'], 'required'],
            [['numFacturaProv', 'cantidadInventario', 'cantidadMinima', 'codFamilia', 'porcentUnidad'], 'integer'],
            [['precioCompra', 'precioVenta', 'precioVentaImpuesto', 'anularDescuento', 'precioMinServicio'/*, 'codProdServicio'*/], 'number'],
            [['tipo'], 'string', 'max' => 10],
            ['codProdServicio', 'match', 'pattern' => '/^[1-9]\w*$/i'],
            [['nombreProductoServicio'], 'string', 'max' => 50],
            [['exlmpuesto'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codProdServicio' => 'Código',
            //'tipo' => 'Tipo',
            //'numFacturaProv' => 'Número de factura',
            'nombreProductoServicio' => 'Nombre de servicio',
            //'cantidadInventario' => 'Cantidad inventario',
            //'cantidadMinima' => 'Cantidad mínima',
            'codFamilia' => 'Código de categoria',
            //'precioCompra' => 'Precio de compra',
            //'porcentUnidad' => '% utilidad',
            //'precioVenta' => 'Precio de venta',
            'exlmpuesto' => 'Exento de impuestos',
            //'precioVentaImpuesto' => 'Precio venta + impuesto',
            //'anularDescuento' => 'Anular descuento',
            'precioMinServicio' => 'Precio mínimo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblDetalleFacturas()
    {
        return $this->hasMany(TblDetalleFacturas::className(), ['codProdServicio' => 'codProdServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodFamilia0()
    {
        return $this->hasOne(Familia::className(), ['codFamilia' => 'codFamilia']);
    }

    public function getNombreFamilia()
    {
        return Familia::find()
        ->where(['=','estado','1'])
        ->andwhere(['=','tipo','Servicio'])
        ->orderBy('descripcion ASC')
        ->asArray()
        ->all();
    }

    // public function beforeSave($insert) {

    //         $this->precioMinServicio = str_replace(",", ".", $this->precioMinServicio);
    //         return  parent::beforeSave($insert);

    // }

}
