<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_movi_cobrar".
 *
 * @property integer $idMov
 * @property string $fecmov
 * @property string $tipmov
 * @property string $concepto
 * @property double $monto_anterior
 * @property double $monto_movimiento
 * @property double $saldo_pendiente
 * @property string $usuario
 * @property integer $idCabeza_Factura
 * @property string $idCliente
 */
class MovimientoCobrar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_movi_cobrar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monto_movimiento', 'concepto'], 'required'],
            [['fecmov', 'idMetPago'], 'safe'],
            [['monto_anterior', 'monto_movimiento', 'saldo_pendiente'], 'number'],
            [['idCabeza_Factura'], 'integer'],
            [['tipmov'], 'string', 'max' => 20],
            [['concepto'], 'string', 'max' => 1000],
            [['usuario'], 'string', 'max' => 40],
            [['idCliente'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMov' => 'Id Mov',
            'fecmov' => 'Fecmov',
            'tipmov' => 'Tipmov',
            'concepto' => 'Concepto',
            'monto_anterior' => 'Monto Anterior',
            'monto_movimiento' => 'Monto Movimiento',
            'saldo_pendiente' => 'Saldo Pendiente',
            'usuario' => 'Usuario',
            'idCabeza_Factura' => 'Id Cabeza  Factura',
            'idCliente' => 'Id Cliente',
        ];
    }

    public function beforeSave($insert)
    {
            $this->fecmov = date('Y-m-d H:i:s', strtotime($this->fecmov));
            return parent::beforeSave($insert);
    }

    public function afterFind()
    {

        $this->fecmov = date('d-m-Y h:i:s A', strtotime( $this->fecmov));

        return parent::afterFind();

    }
}
