<?php

namespace backend\models;

use Yii;

use common\models\User;
use backend\models\RolOperacion; //Uso el modelo RolOperacion para obtener las relaciones y ver a que tipo de ususario le asigno los tipos de permisos
use backend\models\Operacion; //Uso el modelo Operacion para obtener los tipos de permisos
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Funcionario;
use yii\data\Pagination;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $nombre
 * @property string $codigo
 * @property string $rol_id
 *
 * @property TblDerechosUsuarios[] $tblDerechosUsuarios
 */
class UsuariosDerechos extends \yii\db\ActiveRecord
{
    public $operaciones; //Declaro una sentencia para almacenar las operaciones (los tipos de permisos)
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['operaciones', 'safe'], //Para los permisos
            [['username', 'auth_key', 'password_hash', 'email'/*, 'created_at', 'updated_at'*/], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['nombre'], 'string', 'max' => 50],
            [['codigo', 'tipo_usuario'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Usuario',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'tipo_usuario' => 'Tipo de usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
   /* public function getTblDerechosUsuarios()
    {
        return $this->hasMany(TblDerechosUsuarios::className(), ['idUsuario' => 'id']);
    }*/

    /*
    DELETE FROM rol_operacion
    WHERE rol_id = 38
    AND operacion_id NOT IN (
      SELECT operacion_id
      FROM (
        SELECT operacion_id
        FROM rol_operacion
        WHERE operacion_id > 11
        AND operacion_id < 16
      ) foo
    )
    */

    //-------------Derechos de usuario--------------------------------
    //--------------------------------------------------------------------

   /*El método afterSave, se ejecuta luego de que el modelo
   (ya sea en una inserción o actualización) ha sido guardado en nuestra tabla.*/
    public function afterSave($insert, $changedAttributes){

    \Yii::$app->db->createCommand()->delete('rol_operacion', 'rol_id = '.(int) $this->id . ' AND operacion_id NOT IN ( select operacion_id from ( select operacion_id from rol_operacion where operacion_id > '.(int) 11 .' AND operacion_id < ' .(int) 16 . ') foo) ')->execute();
  //  \Yii::$app->db->createCommand()->delete('rol_operacion', 'rol_id = '.(int) $this->id)->execute();

    /*Lo que estamos haciendo, es borrar todos los registros de la tabla rol_operacion (conocida como tabla pivot)
    que tengan como rol_id la id que corresponde al modelo siendo guardado.*/
    /*Es decir, con cada modificación, se borrarán todos los registros de operaciones asignadas a ese rol,
    y serán insertados los nuevos registros correspondientes.*/
    foreach ($this->operaciones as $id) {
        $ro = new RolOperacion();
        $ro->rol_id = $this->id;
        $ro->operacion_id = $id;
        $ro->save();
        }
    }
    /*Ya que se trata de una tabla pivot que sólo contiene claves foráneas sin ningún dato adicional,
    es seguro realizar el borrado y posterior inserción.*/


    /*El método getRolOperaciones, es el método de relación entre el modelo Rol y el modelo RolOperacion.*/
    public function getRolOperaciones()
    {
        return $this->hasMany(RolOperacion::className(), ['rol_id' => 'id']);
    }


    /*Este método señala la relación existente entre Rol y Operacion. Sin embargo, esa relación no es directa,
    sino que se realiza a través de la tabla pivot rol_operacion.*/
    public function getOperacionesPermitidas()
    {
        return $this->hasMany(Operacion::className(), ['id' => 'operacion_id'])
            ->viaTable('rol_operacion', ['rol_id' => 'id']);
            /*Expresa que existe una relación uno a muchos entre rol y operacion (marcada por $this->hasMany)
            y para obtener todas los registros de operacion que corresponden a un rol, se mapea la id de rol
            a las operacion_id correspondientes, pero pasando a través de la tabla pivot ‘rol_operacion’.*/
    }

    /*Este método, getOperacionesPermitidasList, simplemente devuelve como arreglo el resultado
    obtenido en el método anterior.*/
    public function getOperacionesPermitidasList()
    {
        return $this->getOperacionesPermitidas()->asArray();
    }

    public function getTipoF()
    {
        return $this->hasOne(Funcionario::className(), ['tipo_usuario' => 'tipo_usuario']);

    }

    protected static function getTipo(){

    return ArrayHelper::map(UsuariosDerechos::find()
                // ->where(['=','tipo_usuario','Administrador'])
                // ->andwhere(['=','tipo_usuario','Operador'])
                ->asArray()
                ->all(),'tipo_usuario','tipo_usuario');

 }

        public static function  get_status(){

              $cat = Funcionario::find()->all();

              $cat = ArrayHelper::map($cat, 'tipo_usuario', 'tipo_usuario');

              return $cat;

        }


  // public function afterFind()
  //      {

  //       \Yii::$app->db->createCommand("SELECT * FROM USER  LIMIT 10")->execute();
  //       return parent::afterFind();
  //    }

/*
    public function getTipoNoRepetido()
    {
        return UsuariosDerechos::find()
        ->where(['=','estado','Activo'])
        ->andwhere(['=','tipo','Producto'])
        ->orderBy('descripcion ASC')
        ->all();
    }*/


}
