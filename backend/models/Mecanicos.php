<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_mecanicos".
 *
 * @property integer $idMecanico
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 * @property integer $porcentComision
 *
 * @property TblOrdenServicio[] $tblOrdenServicios
 */
class Mecanicos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_mecanicos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', /*'direccion',*/ 'telefono', 'porcentComision'], 'required'],
            [['porcentComision'], 'integer'],
            [['nombre'], 'string', 'max' => 80],
            [['direccion'], 'string', 'max' => 130],
            [['telefono'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           // 'idMecanico' => 'Id',
            'nombre' => 'Nombre',
            'direccion' => 'Dirección',
            'telefono' => 'Teléfono',
            'porcentComision' => '% Comisión',
            'codigo' => 'Código',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblOrdenServicios()
    {
        return $this->hasMany(TblOrdenServicio::className(), ['idMecanico' => 'idMecanico']);
    }

    public static function getNombreMecanicos()
      {
          return Mecanicos::find()
          ->orderBy('idMecanico ASC')
          ->asArray()
          ->all();
      }
}
