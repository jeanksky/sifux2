<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_moneda".
 *
 * @property integer $idTipo_moneda
 * @property string $abreviatura
 * @property string $descripcion
 * @property string $simbolo
 */
class Moneda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_moneda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abreviatura', 'descripcion', 'simbolo'], 'required'],
            [['abreviatura'], 'string', 'max' => 15],
            [['descripcion'], 'string', 'max' => 40],
            [['simbolo'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipo_moneda' => 'Id Tipo Moneda',
            'abreviatura' => 'Abreviatura',
            'descripcion' => 'Descripcion',
            'simbolo' => 'Simbolo',
        ];
    }
}
