<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recepcion_hacienda_gastos".
 *
 * @property integer $id
 * @property integer $idGastos
 * @property integer $id_factun
 * @property string $tipo_documento
 * @property string $resolucion
 * @property string $detalle
 * @property string $respuesta_hacienda
 * @property string $fecha
 * @property string $clave_recepcion
 * @property resource $xml_recepcion
 * @property resource $xml_respuesta
 */
class RecepcionHaciendaGastos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recepcion_hacienda_gastos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idGastos', 'id_factun', 'tipo_documento', 'resolucion', 'detalle', 'respuesta_hacienda', 'fecha'], 'required'],
            [['idGastos', 'id_factun'], 'integer'],
            [['fecha'], 'safe'],
            [['xml_recepcion', 'xml_respuesta'], 'string'],
            [['tipo_documento'], 'string', 'max' => 30],
            [['resolucion'], 'string', 'max' => 25],
            [['detalle'], 'string', 'max' => 210],
            [['respuesta_hacienda'], 'string', 'max' => 2000],
            [['clave_recepcion'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idGastos' => 'Id Gastos',
            'id_factun' => 'Id Factun',
            'tipo_documento' => 'Tipo Documento',
            'resolucion' => 'Resolucion',
            'detalle' => 'Detalle',
            'respuesta_hacienda' => 'Respuesta Hacienda',
            'fecha' => 'Fecha',
            'clave_recepcion' => 'Clave Recepcion',
            'xml_recepcion' => 'Xml Recepcion',
            'xml_respuesta' => 'Xml Respuesta',
        ];
    }
}
