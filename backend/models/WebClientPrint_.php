<?php

namespace backend\models;
use Exception;
use ZipArchive;
use Yii;
use backend\models\Empresa;

// Ajuste WebClientPrint
WebClientPrint::$licenseOwner = 'nelux Professional Services - 1 WebApp Lic - 1 WebServer Lic';
WebClientPrint::$licenseKey = '4232B0C64DF0EF8F1042A092A8545F2D5278B6EB';

//IMPORTANT SETTINGS:
//===================
//Establecer URL ABSOLUTO en el archivo WebClientPrint.php
//WebClientPrint::$webClientPrintAbsoluteUrl = Utils::getRoot().'/nelux/apps_web/lutux v1.2.0/backend/models/WebClientPrint.php';
WebClientPrint::$webClientPrintAbsoluteUrl = Utils::getRoot().'/lutux/backend/models/WebClientPrint.php';
//WebClientPrint::$webClientPrintAbsoluteUrl = Utils::getRoot().'/backend/models/WebClientPrint.php';//para la web
$empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
$cod_empresa = $empresa->cod_empresa;
//WebClientPrint::$webClientPrintAbsoluteUrl = Utils::getRoot().'/lu/'.$cod_empresa.'/backend/models/WebClientPrint.php';//para cloud

//Establecer la carpeta wcpcache RELATIVE al archivo WebClientPrint.php
//¡Es necesario el permiso FILE WRITE en esta carpeta!
WebClientPrint::$wcpCacheFolder = 'wcpcache/';
//===================

// Limpiar caché incorporado
// NOTA: eliminarlo si implementa su propio sistema de caché
//WebClientPrint::cacheClean(30); //en minutos

// Solicitud de proceso
$urlParts = parse_url($_SERVER['REQUEST_URI']);
if (isset($urlParts['query'])){
    if (Utils::strContains($urlParts['query'], WebClientPrint::WCP)){
        WebClientPrint::processRequest($urlParts['query']);
    }
}


/**
 * WebClientPrint proporciona funciones para registrar la solución "WebClientPrint para PHP"
 * Script en páginas web PHP, así como para procesar solicitudes de clientes y administrar la
 * memoria caché interna.
 *
 * @author Neodynamic <http://neodynamic.com/support>
 * @copyright (c) 2016, Neodynamic SRL
 * @license http://neodynamic.com/eula Neodynamic EULA
 */
class WebClientPrint {

    const VERSION = '2.0.2016.800';
    const CLIENT_PRINT_JOB = "clientPrint";
    const WCP = 'WEB_CLIENT_PRINT';

    const WCP_CACHE_WCPP_INSTALLED = 'WCPP_INSTALLED';
    const WCP_CACHE_WCPP_VER = 'WCPP_VER';
    const WCP_CACHE_PRINTERS = 'PRINTERS';

    /**
     * Obtiene o establece el propietario de la licencia
     * @var string
     */
    static $licenseOwner = '';
    /**
     * Obtiene o establece la clave de licencia
     * @var string
     */
    static $licenseKey = '';
    /**
     * Obtiene o establece la URL ABSOLUTE en el archivo WebClientPrint.php
     * @var string
     */
    static $webClientPrintAbsoluteUrl = '';
    /**
     * Obtiene o establece la URL de la carpeta wcpcache RELATIVE al archivo WebClientPrint.php.
     * ¡Es necesario el permiso FILE WRITE en esta carpeta!
     * @var string
     */
    static $wcpCacheFolder = '';

    /**
     * Adds a new entry to the built-in file system cache.
     * @param string $sid The user's session id
     * @param string $key The cache entry key
     * @param string $val The data value to put in the cache
     * @throws Exception
     */
    public static function cacheAdd($sid, $key, $val){
        if (Utils::isNullOrEmptyString(self::$wcpCacheFolder)){
            throw new Exception('WebClientPrint wcpCacheFolder is missing, please specify it.');
        }
        if (Utils::isNullOrEmptyString($sid)){
            throw new Exception('WebClientPrint FileName cache is missing, please specify it.');
        }
        $cacheFileName = (Utils::strEndsWith(self::$wcpCacheFolder, '/')?self::$wcpCacheFolder:self::$wcpCacheFolder.'/').$sid.'.wcpcache';
        $dataWCPP_VER = '';
        $dataPRINTERS = '';

        if(file_exists($cacheFileName)){
            $cache_info = parse_ini_file($cacheFileName);

            $dataWCPP_VER = $cache_info[self::WCP_CACHE_WCPP_VER];
            $dataPRINTERS = $cache_info[self::WCP_CACHE_PRINTERS];
        }

        if ($key === self::WCP_CACHE_WCPP_VER){
            $dataWCPP_VER = self::WCP_CACHE_WCPP_VER.'='.'"'.$val.'"';
            $dataPRINTERS = self::WCP_CACHE_PRINTERS.'='.'"'.$dataPRINTERS.'"';
        } else if ($key === self::WCP_CACHE_PRINTERS){
            $dataWCPP_VER = self::WCP_CACHE_WCPP_VER.'='.'"'.$dataWCPP_VER.'"';
            $dataPRINTERS = self::WCP_CACHE_PRINTERS.'='.'"'.$val.'"';
        }

        $data = $dataWCPP_VER.chr(13).chr(10).$dataPRINTERS;
        $handle = fopen($cacheFileName, 'w') or die('Cannot open file:  '.$cacheFileName);
        fwrite($handle, $data);
        fclose($handle);

    }

    /**
     * Obtiene un valor de la caché del sistema de archivos incorporado basado en el sid & key especificado
     * @param String $ sid ID de sesión del usuario
     * @param String $ key La clave de entrada de caché
     * @return String Devuelve el valor de la caché para el sid & key especificado si se encuentra; O una cadena vacía de lo contrario.
     * @throws Exception
     */
    public static function cacheGet($sid, $key){
        if (Utils::isNullOrEmptyString(self::$wcpCacheFolder)){
            throw new Exception('WebClientPrint wcpCacheFolder falta, especifíquelo.');
        }
        if (Utils::isNullOrEmptyString($sid)){
            throw new Exception('WebClientPrint FileName caché falta, por favor, especifique.');
        }
        $cacheFileName = (Utils::strEndsWith(self::$wcpCacheFolder, '/')?self::$wcpCacheFolder:self::$wcpCacheFolder.'/').$sid.'.wcpcache';
        if(file_exists($cacheFileName)){
            $cache_info = parse_ini_file($cacheFileName, FALSE, INI_SCANNER_RAW);

            if($key===self::WCP_CACHE_WCPP_VER || $key===self::WCP_CACHE_WCPP_INSTALLED){
                return $cache_info[self::WCP_CACHE_WCPP_VER];
            }else if($key===self::WCP_CACHE_PRINTERS){
                return $cache_info[self::WCP_CACHE_PRINTERS];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }

    /**
     * Cleans the built-in file system cache
     * @param type $minutes The number of minutes after any files on the cache will be removed.
     */
    public static function cacheClean($minutes){
        if (!Utils::isNullOrEmptyString(self::$wcpCacheFolder)){
            $cacheDir = (Utils::strEndsWith(self::$wcpCacheFolder, '/')?self::$wcpCacheFolder:self::$wcpCacheFolder.'/');
            if ($handle = opendir($cacheDir)) {
                 while (false !== ($file = readdir($handle))) {
                    if ($file!='.' && $file!='..' && (time()-filectime($cacheDir.$file)) > (60*$minutes)) {
                        unlink($cacheDir.$file);
                    }
                 }
                 closedir($handle);
            }
        }
    }

    /**
     * Devuelve el código de script para detectar si WCPP está instalado en la máquina cliente.
     *
     * El código de la secuencia de comandos de detección WCPP termina con un estado de "éxito" o "error".
     * Puede manejar ambas situaciones creando dos funciones javascript que deben ser
     * wcppDetectOnSuccess () y wcppDetectOnFailure ().
     * Estas dos funciones serán invocadas automáticamente por el código de la secuencia de comandos WCPP-detection.
     *
     * El script de detección WCPP utiliza una variable de tiempo de retardo que por defecto es 10000 ms (10 seg).
     * Puede cambiarlo creando una variable global javascript cuyo nombre debe ser wcppPingDelay_ms.
     * Por ejemplo, para utilizar 5 segundos en lugar de 10, debe agregar esto a su secuencia de comandos:
     *
     * var wcppPingDelay_ms = 5000;
     *
     * @return String Una etiqueta [script] que enlaza con el código de la secuencia de comandos WCPP-detection.
     * @throws Exception
     */
    public static function createWcppDetectionScript(){

        if (Utils::isNullOrEmptyString(self::$webClientPrintAbsoluteUrl)){
            throw new Exception('Falta la URL absoluta de WebClientPrint, especifíquela.');
        }

        $buffer = '<script type="text/javascript">';
        if(Utils::isIE6to9() || Utils::isIE10orGreater()){
            $buffer .= 'var wcppPingNow=false;';
        } else {
            $buffer .= 'var wcppPingNow=true;';
        }
        $buffer .= '</script>';

        $wcpHandler = self::$webClientPrintAbsoluteUrl.'?'.self::WCP.'&d='.session_id();
        $buffer .= '<script src="'.$wcpHandler.'" type="text/javascript"></script>';

        if(Utils::isIE6to9() || Utils::isIE10orGreater()){
            $crlf = chr(13).chr(10);
            $buffer .= $crlf.$crlf;
            $buffer .= '<!--[if WCPP]>'.$crlf;
            $buffer .= '<script type="text/javascript">'.$crlf;
            $buffer .= '$(document).ready(function(){jsWCPP.ping();});'.$crlf;
            $buffer .= '</script>'.$crlf;
            $buffer .= '<![endif]-->'.$crlf;
            $buffer .= '<!--[if !WCPP]>'.$crlf;
            $buffer .= '<script type="text/javascript">'.$crlf;
            $buffer .= '$(document).ready(function(){wcppDetectOnFailure();});'.$crlf;
            $buffer .= '</script>'.$crlf;
            $buffer .= '<![endif]-->'.$crlf;
         }

         return $buffer;
    }

    /**
     * Returns a string containing a HTML meta tag for the WCPP-detection procedure.
     *
     * The meta tag X-UA-Compatible is generated for Internet Explorer (IE) 10 or greater to emulate IE9.
     * If this meta tag is not generated, then IE10 or greater will display some unwanted dialog box to
     * the user when the WCPP-detection script is executed.
     *
     * @return string A string containing a HTML meta tag for the WCPP-detection procedure.
     */
    public static function getWcppDetectionMetaTag(){
         return Utils::isIE10orGreater()?'<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />':'';
    }

    /**
     * Returns a [script] tag linking to the WebClientPrint script code by using
     * the specified URL for the client print job generation.
     *
     * @param string $clientPrintJobUrl The URL to the php file that creates ClientPrintJob objects.
     * @return string A [script] tag linking to the WebClientPrint script code by using the specified URL for the client print job generation.
     * @throws Exception
     */
    public static function createScript($clientPrintJobUrl){
        if (Utils::isNullOrEmptyString(self::$webClientPrintAbsoluteUrl)){
            throw new Exception('Falta la URL absoluta de WebClientPrint, especifíquela.');
        }
        $wcpHandler = self::$webClientPrintAbsoluteUrl.'?';
        $wcpHandler .= self::WCP;
        $wcpHandler .= '&';
        $wcpHandler .= self::VERSION;
        $wcpHandler .= '&';
        $wcpHandler .= microtime(true);
        $wcpHandler .= '&u=';
        $wcpHandler .= base64_encode($clientPrintJobUrl);
        return '<script src="'.$wcpHandler.'" type="text/javascript"></script>';
        //return $clientPrintJobUrl;
    }

    public static function processRequest($data){

        $PING = 'wcppping';
        $SID = 'sid';
        $HAS_WCPP = 'wcppInstalled';

        $GEN_WCP_SCRIPT_URL = 'u';
        $GEN_DETECT_WCPP_SCRIPT = 'd';
        $WCP_SCRIPT_AXD_GET_PRINTERS = 'getPrinters';
        $WCPP_SET_PRINTERS = 'printers';
        $WCP_SCRIPT_AXD_GET_WCPPVERSION = 'getWcppVersion';
        $WCPP_SET_VERSION = 'wcppVer';

        $wcpUrl=self::$webClientPrintAbsoluteUrl;

        parse_str($data, $qs);

        if(isset($qs[$SID])){
            if(isset($qs[$PING])){
                if (isset($qs[$WCPP_SET_VERSION])){
                    self::cacheAdd($qs[$SID], self::WCP_CACHE_WCPP_VER, $qs[$WCPP_SET_VERSION]);
                }else{
                    self::cacheAdd($qs[$SID], self::WCP_CACHE_WCPP_VER,'1.0.0.0');
                }
            }else if(isset($qs[$WCPP_SET_PRINTERS])){
                self::cacheAdd($qs[$SID], self::WCP_CACHE_PRINTERS, strlen($qs[$WCPP_SET_PRINTERS]) > 0 ? base64_decode($qs[$WCPP_SET_PRINTERS]) : '');
            }else if(strpos($data, $WCP_SCRIPT_AXD_GET_PRINTERS)>0){
                ob_start();
                ob_clean();
                header('Content-type: text/plain');
                echo self::cacheGet($qs[$SID], self::WCP_CACHE_PRINTERS);
            }else if(isset($qs[$WCPP_SET_VERSION])){
                if(strlen($qs[$WCPP_SET_VERSION]) > 0){
                    self::cacheAdd($qs[$SID], self::WCP_CACHE_WCPP_VER, $qs[$WCPP_SET_VERSION]);
                }
            }else if(strpos($data, $WCP_SCRIPT_AXD_GET_WCPPVERSION)>0){
                ob_start();
                ob_clean();
                header('Content-type: text/plain');
                echo self::cacheGet($qs[$SID], self::WCP_CACHE_WCPP_VER);
            }else{
                ob_start();
                ob_clean();
                header('Content-type: text/plain');
                echo self::cacheGet($qs[$SID], self::WCP_CACHE_WCPP_INSTALLED);
            }
        }else if(isset($qs[$GEN_DETECT_WCPP_SCRIPT])){

            $curSID = $qs[$GEN_DETECT_WCPP_SCRIPT];
            $onSuccessScript = 'wcppDetectOnSuccess(data);';
            $onFailureScript = 'wcppDetectOnFailure();';
            $dynamicIframeId = 'i'.substr(uniqid(), 0, 3);
            $absoluteWcpAxd = $wcpUrl.'?'.self::WCP.'&'.$SID.'='.$curSID;

            $s1 = 'dmFyIGpzV0NQUD0oZnVuY3Rpb24oKXt2YXIgc2V0WFhYLU5FTy1IVE1MLUlELVhYWD1mdW5jdGlvbigpe2lmKHdpbmRvdy5jaHJvbWUpeyQoJyNYWFgtTkVPLUhUTUwtSUQtWFhYJykuYXR0cignaHJlZicsJ3dlYmNsaWVudHByaW50OicrYXJndW1lbnRzWzBdKTt2YXIgYT0kKCdhI1hYWC1ORU8tSFRNTC1JRC1YWFgnKVswXTt2YXIgZXZPYmo9ZG9jdW1lbnQuY3JlYXRlRXZlbnQoJ01vdXNlRXZlbnRzJyk7ZXZPYmouaW5pdEV2ZW50KCdjbGljaycsdHJ1ZSx0cnVlKTthLmRpc3BhdGNoRXZlbnQoZXZPYmopfWVsc2V7JCgnI1hYWC1ORU8tSFRNTC1JRC1YWFgnKS5hdHRyKCdzcmMnLCd3ZWJjbGllbnRwcmludDonK2FyZ3VtZW50c1swXSl9fTtyZXR1cm57aW5pdDpmdW5jdGlvbigpe2lmKHdpbmRvdy5jaHJvbWUpeyQoJzxhIC8+Jyx7aWQ6J1hYWC1ORU8tSFRNTC1JRC1YWFgnfSkuYXBwZW5kVG8oJ2JvZHknKX1lbHNleyQoJzxpZnJhbWUgLz4nLHtuYW1lOidYWFgtTkVPLUhUTUwtSUQtWFhYJyxpZDonWFhYLU5FTy1IVE1MLUlELVhYWCcsd2lkdGg6JzEnLGhlaWdodDonMScsc3R5bGU6J3Zpc2liaWxpdHk6aGlkZGVuO3Bvc2l0aW9uOmFic29sdXRlJ30pLmFwcGVuZFRvKCdib2R5Jyl9fSxwaW5nOmZ1bmN0aW9uKCl7c2V0WFhYLU5FTy1IVE1MLUlELVhYWCgnWFhYLU5FTy1QSU5HLVVSTC1YWFgnKyhhcmd1bWVudHMubGVuZ3RoPT0xPycmJythcmd1bWVudHNbMF06JycpKTt2YXIgZGVsYXlfbXM9KHR5cGVvZiB3Y3BwUGluZ0RlbGF5X21zPT09J3VuZGVmaW5lZCcpPzEwMDAwOndjcHBQaW5nRGVsYXlfbXM7c2V0VGltZW91dChmdW5jdGlvbigpeyQuZ2V0KCdYWFgtTkVPLVVTRVItSEFTLVdDUFAtWFhYJyxmdW5jdGlvbihkYXRhKXtpZihkYXRhIT0nZicpe1hYWC1ORU8tT04tU1VDQ0VTUy1TQ1JJUFQtWFhYfWVsc2V7WFhYLU5FTy1PTi1GQUlMVVJFLVNDUklQVC1YWFh9fSl9LGRlbGF5X21zKX19fSkoKTsgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtqc1dDUFAuaW5pdCgpO2lmKHdjcHBQaW5nTm93KWpzV0NQUC5waW5nKCk7fSk7';
            $s2 = base64_decode($s1);
            $s2 = str_replace('XXX-NEO-HTML-ID-XXX', $dynamicIframeId, $s2);
            $s2 = str_replace('XXX-NEO-PING-URL-XXX', $absoluteWcpAxd.'&'.$PING, $s2);
            $s2 = str_replace('XXX-NEO-USER-HAS-WCPP-XXX', $absoluteWcpAxd, $s2);
            $s2 = str_replace('XXX-NEO-ON-SUCCESS-SCRIPT-XXX', $onSuccessScript, $s2);
            $s2 = str_replace('XXX-NEO-ON-FAILURE-SCRIPT-XXX', $onFailureScript, $s2);

            ob_start();
            ob_clean();
            header('Content-type: application/javascript');
            echo $s2;

        }else if(isset($qs[$GEN_WCP_SCRIPT_URL])){

            $clientPrintJobUrl = base64_decode($qs[$GEN_WCP_SCRIPT_URL]);
            if (strpos($clientPrintJobUrl, '?')>0){
                $clientPrintJobUrl .= '&';
            }else{
                $clientPrintJobUrl .= '?';
            }
            $clientPrintJobUrl .= self::CLIENT_PRINT_JOB;
            $absoluteWcpAxd = $wcpUrl;
            $wcppGetPrintersParam = '-getPrinters:'.$absoluteWcpAxd.'?'.self::WCP.'&'.$SID.'=';
            $wcpHandlerGetPrinters = $absoluteWcpAxd.'?'.self::WCP.'&'.$WCP_SCRIPT_AXD_GET_PRINTERS.'&'.$SID.'=';
            $wcppGetWcppVerParam = '-getWcppVersion:'.$absoluteWcpAxd.'?'.self::WCP.'&'.$SID.'=';
            $wcpHandlerGetWcppVer = $absoluteWcpAxd.'?'.self::WCP.'&'.$WCP_SCRIPT_AXD_GET_WCPPVERSION.'&'.$SID.'=';

            $s1 = 'dmFyIGpzV2ViQ2xpZW50UHJpbnQ9KGZ1bmN0aW9uKCl7dmFyIHNldEE9ZnVuY3Rpb24oKXt2YXIgZV9pZD0naWRfJytuZXcgRGF0ZSgpLmdldFRpbWUoKTtpZih3aW5kb3cuY2hyb21lKXskKCdib2R5JykuYXBwZW5kKCc8YSBpZD0iJytlX2lkKyciPjwvYT4nKTskKCcjJytlX2lkKS5hdHRyKCdocmVmJywnd2ViY2xpZW50cHJpbnQ6Jythcmd1bWVudHNbMF0pO3ZhciBhPSQoJ2EjJytlX2lkKVswXTt2YXIgZXZPYmo9ZG9jdW1lbnQuY3JlYXRlRXZlbnQoJ01vdXNlRXZlbnRzJyk7ZXZPYmouaW5pdEV2ZW50KCdjbGljaycsdHJ1ZSx0cnVlKTthLmRpc3BhdGNoRXZlbnQoZXZPYmopfWVsc2V7JCgnYm9keScpLmFwcGVuZCgnPGlmcmFtZSBuYW1lPSInK2VfaWQrJyIgaWQ9IicrZV9pZCsnIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBzdHlsZT0idmlzaWJpbGl0eTpoaWRkZW47cG9zaXRpb246YWJzb2x1dGUiIC8+Jyk7JCgnIycrZV9pZCkuYXR0cignc3JjJywnd2ViY2xpZW50cHJpbnQ6Jythcmd1bWVudHNbMF0pfXNldFRpbWVvdXQoZnVuY3Rpb24oKXskKCcjJytlX2lkKS5yZW1vdmUoKX0sNTAwMCl9O3JldHVybntwcmludDpmdW5jdGlvbigpe3NldEEoJ1VSTF9QUklOVF9KT0InKyhhcmd1bWVudHMubGVuZ3RoPT0xPycmJythcmd1bWVudHNbMF06JycpKX0sZ2V0UHJpbnRlcnM6ZnVuY3Rpb24oKXtzZXRBKCdVUkxfV0NQX0FYRF9XSVRIX0dFVF9QUklOVEVSU19DT01NQU5EJyskKCcjc2lkJykudmFsKCkpO3ZhciBkZWxheV9tcz0odHlwZW9mIHdjcHBHZXRQcmludGVyc0RlbGF5X21zPT09J3VuZGVmaW5lZCcpPzEwMDAwOndjcHBHZXRQcmludGVyc0RlbGF5X21zO3NldFRpbWVvdXQoZnVuY3Rpb24oKXskLmdldCgnVVJMX1dDUF9BWERfR0VUX1BSSU5URVJTJyskKCcjc2lkJykudmFsKCksZnVuY3Rpb24oZGF0YSl7aWYoZGF0YS5sZW5ndGg+MCl7d2NwR2V0UHJpbnRlcnNPblN1Y2Nlc3MoZGF0YSl9ZWxzZXt3Y3BHZXRQcmludGVyc09uRmFpbHVyZSgpfX0pfSxkZWxheV9tcyl9LGdldFdjcHBWZXI6ZnVuY3Rpb24oKXtzZXRBKCdVUkxfV0NQX0FYRF9XSVRIX0dFVF9XQ1BQVkVSU0lPTl9DT01NQU5EJyskKCcjc2lkJykudmFsKCkpO3ZhciBkZWxheV9tcz0odHlwZW9mIHdjcHBHZXRWZXJEZWxheV9tcz09PSd1bmRlZmluZWQnKT8xMDAwMDp3Y3BwR2V0VmVyRGVsYXlfbXM7c2V0VGltZW91dChmdW5jdGlvbigpeyQuZ2V0KCdVUkxfV0NQX0FYRF9HRVRfV0NQUFZFUlNJT04nKyQoJyNzaWQnKS52YWwoKSxmdW5jdGlvbihkYXRhKXtpZihkYXRhLmxlbmd0aD4wKXt3Y3BHZXRXY3BwVmVyT25TdWNjZXNzKGRhdGEpfWVsc2V7d2NwR2V0V2NwcFZlck9uRmFpbHVyZSgpfX0pfSxkZWxheV9tcyl9LHNlbmQ6ZnVuY3Rpb24oKXtzZXRBLmFwcGx5KHRoaXMsYXJndW1lbnRzKX19fSkoKTs=';
            $s2 = base64_decode($s1);
            $s2 = str_replace('URL_PRINT_JOB', $clientPrintJobUrl, $s2);
            $s2 = str_replace('URL_WCP_AXD_WITH_GET_PRINTERS_COMMAND', $wcppGetPrintersParam, $s2);
            $s2 = str_replace('URL_WCP_AXD_GET_PRINTERS', $wcpHandlerGetPrinters, $s2);
            $s2 = str_replace('URL_WCP_AXD_WITH_GET_WCPPVERSION_COMMAND', $wcppGetWcppVerParam, $s2);
            $s2 = str_replace('URL_WCP_AXD_GET_WCPPVERSION', $wcpHandlerGetWcppVer, $s2);

            ob_start();
            ob_clean();
            header('Content-type: application/javascript');
            echo $s2;
        }

    }

}

/**
 * La clase base para todo tipo de impresoras soportadas en el lado del cliente.
 */
abstract class ClientPrinter{

    public $printerId;
    public function serialize(){

    }
}

/**
 * Representa la impresora predeterminada instalada en la máquina cliente.
 */
class DefaultPrinter extends ClientPrinter{
    public function __construct() {
        $this->printerId = chr(0);
    }

    public function serialize() {
        return $this->printerId;
    }
}

/**
 * Representa una impresora instalada en la máquina cliente con un controlador de SO asociado.
 */
class InstalledPrinter extends ClientPrinter{

    /**
     * Obtiene o establece el nombre de la impresora instalada en la máquina cliente. El valor predeterminado es una cadena vacía.
     * @var string
     */
    public $printerName = '';

    /**
     * Crea una instancia de la clase InstalledPrinter con el nombre de impresora especificado.
     * @param String $ printerName Nombre de la impresora instalada en la máquina cliente.
     */
    public function __construct($printerName) {
        $this->printerId = chr(1);
        $this->printerName = $printerName;
    }


    public function serialize() {

        if (Utils::isNullOrEmptyString($this->printerName)){
             throw new Exception("El nombre de impresora especificado es nulo o vacío.");
        }

        return $this->printerId.$this->printerName;
    }
}

/**
 * Representa una impresora que está conectada a través de un puerto paralelo en la máquina cliente.
 */
class ParallelPortPrinter extends ClientPrinter{

    /**
     * Obtiene o establece el nombre del puerto paralelo, por ejemplo LPT1. El valor predeterminado es "LPT1"
     * @var string
     */
    public $portName = "LPT1";

    /**
     * Crea una instancia de la clase ParallelPortPrinter con el nombre de puerto especificado.
     * @param String $ portName El nombre del puerto paralelo, por ejemplo LPT1.
     */
    public function __construct($portName) {
        $this->printerId = chr(2);
        $this->portName = $portName;
    }

    public function serialize() {

        if (Utils::isNullOrEmptyString($this->portName)){
             throw new Exception("El nombre del puerto paralelo especificado es nulo o vacío.");
        }

        return $this->printerId.$this->portName;
    }
}

/**
 * Representa una impresora que está conectada a través de un puerto serie en la máquina cliente.
 */
class SerialPortPrinter extends ClientPrinter{

    /**
     * Obtiene o establece el nombre del puerto serie, por ejemplo COM1. El valor predeterminado es "COM1"
     * @var string
     */
    public $portName = "COM1";
    /**
     * Obtiene o establece la velocidad en baudios del puerto serie en bits por segundo. El valor predeterminado es 9600
     * @var integer
     */
    public $baudRate = 9600;
    /**
     * Obtiene o establece el protocolo de comprobación de paridad del puerto serie. El valor predeterminado es NONE = 0
     * NONE = 0, ODD = 1, EVEN = 2, MARK = 3, SPACE = 4
     * @var integer
     */
    public $parity = SerialPortParity::NONE;
    /**
     * Obtiene o establece el número estándar del puerto serie de stopbits por byte. El valor predeterminado es ONE = 1
     * ONE = 1, TWO = 2, ONE_POINT_FIVE = 3
     * @var integer
     */
    public $stopBits = SerialPortStopBits::ONE;
    /**
     * Obtiene o establece la longitud estándar del puerto serie de bits de datos por byte. El valor predeterminado es 8
     * @var integer
     */
    public $dataBits = 8;
    /**
     * Obtiene o establece el protocolo de handshaking para la transmisión de datos en serie. El valor predeterminado es XON_XOFF = 1
     * NONE = 0, REQUEST_TO_SEND = 2, REQUEST_TO_SEND_XON_XOFF = 3, XON_XOFF = 1
     * @var integer
     */
    public $flowControl = SerialPortHandshake::XON_XOFF;

    /**
     * Crea una instancia de la clase Serial Port Printer con la información especificada.
     * @param String $ portName El nombre del puerto serie, por ejemplo COM1.
     * @param Integer $ baudRate La velocidad en baudios del puerto serie en bits por segundo.
     * @param Integer $ parity El protocolo de comprobación de paridad del puerto serie.
     * @param Integer $ stopBits El número de serie estándar de stopbits por byte.
     * @param Integer $ stopBits El número de serie estándar de stopbits por byte.
     * @param Integer $ flowControl Protocolo de enlace para la transmisión en serie de datos.
     */
    public function __construct($portName, $baudRate, $parity, $stopBits, $dataBits, $flowControl) {
        $this->printerId = chr(3);
        $this->portName = $portName;
        $this->baudRate = $baudRate;
        $this->parity = $parity;
        $this->stopBits = $stopBits;
        $this->dataBits = $dataBits;
        $this->flowControl = $flowControl;
    }

    public function serialize() {

        if (Utils::isNullOrEmptyString($this->portName)){
             throw new Exception("The specified serial port name is null or empty.");
        }

        return $this->printerId.$this->portName.Utils::SER_SEP.$this->baudRate.Utils::SER_SEP.$this->dataBits.Utils::SER_SEP.$this->flowControl.Utils::SER_SEP.$this->parity.Utils::SER_SEP.$this->stopBits;
    }
}

/**
 * Representa una impresora IP / Ethernet de red que se puede alcanzar desde la máquina cliente.
 */
class NetworkPrinter extends ClientPrinter{

    /**
     * Representa una impresora IP / Ethernet de red que se puede alcanzar desde la máquina cliente.
     * @var string
     */
    public $dnsName = "";
    /**
     * Obtiene o establece la dirección de Protocolo de Internet (IP) asignada a la impresora. El valor predeterminado es una cadena vacía
     * @var string
     */
    public $ipAddress = "";
    /**
     * Obtiene o establece el número de puerto asignado a la impresora. El valor predeterminado es 0
     * @var integer
     */
    public $port = 0;

    /**
     * Crea una instancia de la clase NetworkPrinter con el nombre DNS especificado o la dirección IP y el número de puerto.
     * @param String $ dns Nombre El nombre DNS asignado a la impresora
     * @param String $ ipAddress La dirección IP (Protocolo de Internet) asignada a la impresora.
     * @param Integer $ port El número de puerto asignado a la impresora.
     */
    public function __construct($dnsName, $ipAddress, $port) {
        $this->printerId = chr(4);
        $this->dnsName = $dnsName;
        $this->ipAddress = $ipAddress;
        $this->port = $port;
    }

    public function serialize() {

        if (Utils::isNullOrEmptyString($this->dnsName) && Utils::isNullOrEmptyString($this->ipAddress)){
             throw new Exception("The specified network printer settings is not valid. You must specify the DNS Printer Name or its IP address.");
        }

        return $this->printerId.$this->dnsName.Utils::SER_SEP.$this->ipAddress.Utils::SER_SEP.$this->port;
    }
}

/**
 *  Representa una impresora que será seleccionada por el usuario en la máquina cliente. Se le pedirá al usuario que muestre un diálogo de impresión.
 */
class UserSelectedPrinter extends ClientPrinter{
    public function __construct() {
        $this->printerId = chr(5);
    }

    public function serialize() {
        return $this->printerId;
    }
}

/**
 * Especifica el bit de paridad para la configuración del puerto serie.
 */
class SerialPortParity{
    const NONE = 0;
    const ODD = 1;
    const EVEN = 2;
    const MARK = 3;
    const SPACE = 4;
}

/**
 * Especifica el número de bits de parada utilizados para los ajustes del puerto serie.
 */
class SerialPortStopBits{
    const NONE = 0;
    const ONE = 1;
    const TWO = 2;
    const ONE_POINT_FIVE = 3;
}

/**
 * Specifies the control protocol used in establishing a serial port communication.
 */
class SerialPortHandshake{
    const NONE = 0;
    const REQUEST_TO_SEND = 2;
    const REQUEST_TO_SEND_XON_XOFF = 3;
    const XON_XOFF = 1;
}

/**
 * Especifica el protocolo de control utilizado para establecer una comunicación de puerto serie.
 */
class PrintFile{

    /**
     * Obtiene o establece la ruta del archivo en el lado del servidor que se imprimirá en el lado del cliente.
     * @var string
     */
    public $filePath = '';
    /**
     * Obtiene o establece el nombre de archivo que se creará en el lado del cliente.
     * Debe incluir la extensión de archivo como .pdf, .txt, .doc, .xls, etc.
     * @var string
     */
    public $fileName = '';
    /**
     * Obtiene o establece el contenido binario del archivo en el lado del servidor que se imprimirá en el lado del cliente.
     * @var string
     */
    public $fileBinaryContent = '';

    const PREFIX = 'wcpPF:';
    const SEP = '|';

    /**
     *
     * @param string $filePath The path of the file at the server side that will be printed at the client side.
     * @param string $fileName The file name that will be created at the client side. It must include the file extension like .pdf, .txt, .doc, .xls, etc.
     * @param string $fileBinaryContent The binary content of the file at the server side that will be printed at the client side.
     */
    public function __construct($filePath, $fileName, $fileBinaryContent) {
        $this->filePath = $filePath;
        $this->fileName = $fileName;
        $this->fileBinaryContent = $fileBinaryContent;

    }

    public function serialize() {
        $file = str_replace('\\', 'BACKSLASHCHAR',$this->fileName );
        return self::PREFIX.$file.self::SEP.$this->getFileContent();
    }

    public function getFileContent(){
        $content = $this->fileBinaryContent;
        if(!Utils::isNullOrEmptyString($this->filePath)){
            $handle = fopen($this->filePath, 'rb');
            $content = fread($handle, filesize($this->filePath));
            fclose($handle);
        }
        return $content;
    }
}

/**
 * Algunas funciones de utilidad utilizadas por WebClientPrint para la solución de PHP.
 */
class Utils{
    const SER_SEP = "|";

    static function isNullOrEmptyString($s){
        return (!isset($s) || trim($s)==='');
    }

    static function formatHexValues($s){

        $buffer = '';

        $l = strlen($s);
        $i = 0;

        while ($i < $l)
        {
            if ($s[$i] == '0')
            {
                if ($i + 1 < $l && ($s[$i] == '0' && $s[$i + 1] == 'x'))
                {
                    if ($i + 2 < $l &&
                        (($s[$i + 2] >= '0' && $s[$i + 2] <= '9') || ($s[$i + 2] >= 'a' && $s[$i + 2] <= 'f') || ($s[$i + 2] >= 'A' && $s[$i + 2] <= 'F')))
                    {
                        if ($i + 3 < $l &&
                           (($s[$i + 3] >= '0' && $s[$i + 3] <= '9') || ($s[$i + 3] >= 'a' && $s[$i + 3] <= 'f') || ($s[$i + 3] >= 'A' && $s[$i + 3] <= 'F')))
                        {
                            try{
                                $buffer .= chr(substr($s, $i, 4));
                                $i += 4;
                                continue;

                            } catch (Exception $ex) {
                                throw new Exception("Invalid hex notation in the specified printer commands at index: ".$i);
                            }


                        }
                        else
                        {
                            try{

                                $buffer .= chr(substr($s, $i, 3));
                                $i += 3;
                                continue;

                            } catch (Exception $ex) {
                                throw new ArgumentException("Invalid hex notation in the specified printer commands at index: ".$i);
                            }
                        }
                    }
                }
            }

            $buffer .= substr($s, $i, 1);

            $i++;
        }

        return $buffer;

    }

    public static function intToArray($i){
        return pack("L", $i);
    }

    public static function getRoot(){
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $protocol = static::strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
        return $protocol."://".$_SERVER['SERVER_NAME'].$port;
    }

    public static function strleft($s1, $s2) {
    return substr($s1, 0, strpos($s1, $s2));
    }

    public static function strContains($s1, $s2){
        return (strpos($s1, $s2) !== false);
    }

    public static function strEndsWith($s1, $s2)
    {
        return substr($s1, -strlen($s2)) === $s2;
    }

    public static function getUA(){
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public static function isIE6to9(){
        $ua = static::getUA();
        return ((static::strContains($ua,"MSIE 6.") ||
                 static::strContains($ua,"MSIE 7.") ||
                 static::strContains($ua,"MSIE 8.") ||
                 static::strContains($ua,"MSIE 9.")) &&
                 static::strContains($ua,"Opera") === false);
    }

    public static function isIE5orOlder(){
        $ua = static::getUA();
        return ((static::strContains($ua,"MSIE 5.") ||
                 static::strContains($ua,"MSIE 4.") ||
                 static::strContains($ua,"MSIE 3.") ||
                 static::strContains($ua,"MSIE 2.")) &&
                 static::strContains($ua,"Opera") === false);
    }

    public static function isIE10orGreater(){
        $ua = static::getUA();
        return (static::strContains($ua,"MSIE") || static::strContains($ua,"Trident/")) && static::isIE6to9() === false && static::isIE5orOlder() === false && static::strContains($ua,"Opera") === false;
    }

}

/**
 * Specifies information about the print job to be processed at the client side.
 */
class ClientPrintJob{

    /**
     * Obtiene o establece el objeto ClientPrinter. El valor predeterminado es NULL.
     * El objeto ClientPrinter hace referencia al tipo de impresora que la máquina cliente ha conectado o puede alcanzar.
     * - Utilice un objeto DefaultPrinter para utilizar la impresora predeterminada instalada en la máquina cliente.
     * - Utilice un objeto InstalledPrinter para utilizar una impresora instalada en la máquina cliente con un controlador de Windows asociado.
     * - Utilice un objeto ParallelPortPrinter para utilizar una impresora que esté conectada a través de un puerto paralelo en la máquina cliente.
     * - Utilice un objeto SerialPortPrinter para utilizar una impresora que esté conectada a través de un puerto serie en la máquina cliente.
     * - Utilice un objeto de impresora de red para utilizar una impresora IP / Ethernet de red que pueda ser alcanzada desde la máquina cliente.
     * @var ClientPrinter
     */
    public $clientPrinter = null;
    /**
     * Obtiene o establece los comandos de la impresora en formato de texto sin formato. El valor predeterminado es una cadena vacía.
     * @var string
     */
    public $printerCommands = '';
    /**
     * Obtiene o establece si los comandos de impresora tienen caracteres expresados en notación hexadecimal. El valor predeterminado es false.
     * La cadena establecida en la propiedad $ printerCommands puede contener caracteres expresados en notación hexadecimal.
     * Muchos lenguajes de impresora tienen comandos que están representados por caracteres no imprimibles y para expresar estos
     * comandos en una cadena podría requerir muchas concatenaciones y por lo tanto no ser tan legible.
     * Mediante el uso de notación hexadecimal, puede hacerlo simple y elegante. Aquí hay un ejemplo: si necesita codificar ASCII 27 (escape),
     * Entonces usted puede representarlo como 0x27.
     * @var boolean
     */
    public $formatHexValues = false;
    /**
     * Obtiene o establece el objeto PrintFile que se va a imprimir en el lado del cliente. El valor predeterminado es NULL.
     * @var PrintFile
     */
    public $printFile = null;
    /**
     * Obtiene o establece una matriz de PrintFile objetos para imprimir en el lado del cliente. El valor predeterminado es NULL.
     * @var array
     */
    public $printFileGroup = null;


    /**
     * Envía este objeto ClientPrintJob al cliente para su procesamiento posterior.
     * El objeto ClientPrintJob será procesado por el WCPP instalado en la máquina cliente.
     * @return String Una cadena que representa un objeto ClientPrintJob.
     */
    public function sendToClient(){

        header('Content-type: application/octet-stream');

        $cpjHeader = chr(99).chr(112).chr(106).chr(2);

        $buffer = '';

        if (!Utils::isNullOrEmptyString($this->printerCommands)){
            if($this->formatHexValues){
                $buffer = Utils::formatHexValues ($this->printerCommands);
            } else {
                $buffer = $this->printerCommands;
            }
        } else if (isset ($this->printFile)){
            $buffer = $this->printFile->serialize();
        } else if (isset ($this->printFileGroup)){
            $buffer = 'wcpPFG:';
            $zip = new ZipArchive;
            $cacheFileName = (Utils::strEndsWith(WebClientPrint::$wcpCacheFolder, '/')?WebClientPrint::$wcpCacheFolder:WebClientPrint::$wcpCacheFolder.'/').'PFG'.uniqid().'.zip';
            $res = $zip->open($cacheFileName, ZipArchive::CREATE);
            if ($res === TRUE) {
                foreach ($this->printFileGroup as $printFile) {
                    $zip->addFromString($printFile->fileName, $printFile->getFileContent());
                }
                $zip->close();
                $handle = fopen($cacheFileName, 'rb');
                $buffer .= fread($handle, filesize($cacheFileName));
                fclose($handle);
                unlink($cacheFileName);
            } else {
                $buffer='Error al crear PrintFileGroup. No se puede crear un archivo zip.';
            }
        }

        $arrIdx1 = Utils::intToArray(strlen($buffer));

        if (!isset($this->clientPrinter)){
            $this->clientPrinter = new UserSelectedPrinter();
        }

        $buffer .= $this->clientPrinter->serialize();

        $arrIdx2 = Utils::intToArray(strlen($buffer));

        $lo = '';
        if(Utils::isNullOrEmptyString(WebClientPrint::$licenseOwner)){
            $lo = substr(uniqid(), 0, 8);
        }  else {
            $lo = WebClientPrint::$licenseOwner;
        }
        $lk = '';
        if(Utils::isNullOrEmptyString(WebClientPrint::$licenseKey)){
            $lk = substr(uniqid(), 0, 8);
        }  else {
            $lk = WebClientPrint::$licenseKey;
        }
        $buffer .= $lo.chr(124).$lk;

        return $cpjHeader.$arrIdx1.$arrIdx2.$buffer;
    }

}
