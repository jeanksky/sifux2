<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_recibos_detalle".
 *
 * @property integer $idDetallerecibo
 * @property integer $idRecibo
 * @property integer $idDocumento
 * @property string $tipo
 */
class RecibosDetalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_recibos_detalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRecibo', 'idDocumento'], 'integer'],
            [['tipo'], 'string', 'max' => 2],
            [['referencia'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDetallerecibo' => 'Id Detallerecibo',
            'idRecibo' => 'Id Recibo',
            'idDocumento' => 'Id Documento',
            'tipo' => 'Tipo',
        ];
    }
}
