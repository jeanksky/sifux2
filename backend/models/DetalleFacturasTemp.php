<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_detalle_facturas_temp".
 *
 * @property string $usuario
 * @property string $cod
 * @property string $desc
 * @property string $precio
 * @property integer $cantidad
 * @property string $dcto
 * @property string $iva
 * @property string $fecha_hora
 */
class DetalleFacturasTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_detalle_facturas_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['precio', 'dcto', 'iva', 'cantidad'], 'number'],
            [['fecha_hora', 'cantidad'], 'safe'],
            [['usuario'], 'string', 'max' => 50],
            [['cod', 'desc'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usuario' => 'Usuario',
            'cod' => 'Cod',
            'desc' => 'Desc',
            'precio' => 'Precio',
            'cantidad' => 'Cantidad',
            'dcto' => 'Dcto',
            'iva' => 'Iva',
            'fecha_hora' => 'Fecha Hora',
        ];
    }
}
