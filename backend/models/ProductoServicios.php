<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_producto_servicios".
 *
 * @property integer $codProdServicio
 * @property string $tipo
 * @property integer $numFacturaProv
 * @property string $nombreProductoServicio
 * @property integer $cantidadInventario
 * @property integer $cantidadMinima
 * @property integer $codFamilia
 * @property double $precioCompra
 * @property integer $porcentUnidad
 * @property double $precioVenta
 * @property string $exlmpuesto
 * @property double $precioVentaImpuesto
 * @property double $anularDescuento
 * @property double $precioMinServicio
 *
 * @property TblDetalleFacturas[] $tblDetalleFacturas
 * @property TblFamilia $codFamilia0
 */
class ProductoServicios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_producto_servicios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProdServicio'/*,'nombreProductoServicio'*/], 'unique'],
            [['tipo', 'codProdServicio',/*'numFacturaProv',*/ 'nombreProductoServicio',/* 'cantidadInventario','cantidad_dias_produccion',*/ 'cantidadMinima', 'codFamilia', 'precioCompra', 'porcentUnidad', 'precioVenta', 'exlmpuesto', 'precioVentaImpuesto', 'anularDescuento','idUnidadMedida'/*, 'precioMinServicio'*/], 'required'],
            [['numFacturaProv', 'cantidadMinima', 'codFamilia', 'cantidad_dias_produccion'], 'integer'],
            [['precioCompra', 'cantidadInventario', 'precioVenta', 'precioVentaImpuesto', 'precioMinServicio','porcentUnidad'], 'number'],
            [['tipo'], 'string', 'max' => 10],
            [['ubicacion'], 'string', 'max' => 50],
            [['nombreProductoServicio'], 'string', 'max' => 500],
            [['codProdServicio'], 'string', 'max' => 20],
            [['exlmpuesto', 'anularDescuento'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codProdServicio' => 'Código',
            'tipo' => 'Tipo',
            'numFacturaProv' => 'Número de factura',
            'nombreProductoServicio' => 'Descripción del producto',
            'cantidadInventario' => 'Cantidad inventario',
            'cantidadMinima' => 'Cant mínima',
            'codFamilia' => 'Código de categoria',
            'precioCompra' => 'Precio de compra',
            'porcentUnidad' => '% utilidad',
            'precioVenta' => 'Precio de venta',
            'cantidad_dias_produccion' => 'Cant. de días rendimiento',
            'exlmpuesto' => 'Exento de impuestos',
            'precioVentaImpuesto' => 'Precio venta + impuesto',
            'anularDescuento' => 'Anular descuento',
            'precioMinServicio' => 'Precio mínimo',
            'ubicacion' => 'Ubicación',
            'idUnidadMedida'=>'Unidad Medida',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblDetalleFacturas()
    {
        return $this->hasMany(TblDetalleFacturas::className(), ['codProdServicio' => 'codProdServicio']);
    }

    //antes de guardar
    public function beforeSave($insert)
    {
      $this->codProdServicio = strtoupper($this->codProdServicio);
      return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodFamilia0()
    {
        return $this->hasOne(TblFamilia::className(), ['codFamilia' => 'codFamilia']);
    }

    public function getNombreFamilia()
    {
        return Familia::find()
        ->where(['=','estado','1'])
        ->andwhere(['=','tipo','Producto'])
        ->orderBy('codFamilia ASC')
        ->all();
    }

//funcion que busca los parametros que solo tienen como activo en UnidadMedida
    public function getUnidadMedida()
    {
        return UnidadMedidaProducto::find()
        //->where(['=','estadoTipoGastos','activo'])
        //->andwhere(['=','tipo','Producto'])
        ->orderBy('descripcionUnidadMedida ASC')
        ->all();
    }

    public function getfullName()
        {
                return $this->codProdServicio.' '.$this->nombreProductoServicio;
        }
}
