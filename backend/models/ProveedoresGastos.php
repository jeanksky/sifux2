<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_proveedores_gastos".
 *
 * @property integer $idProveedorGastos
 * @property string $tipoEntidad
 * @property string $identificacion
 * @property string $nombreEmpresa
 * @property string $telefono
 * @property string $sitioWeb
 * @property string $email
 * @property string $estado
 */
class ProveedoresGastos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_proveedores_gastos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipoEntidad', 'identificacion', 'nombreEmpresa', 'estado'], 'required'],
            [['tipoEntidad'], 'string', 'max' => 9],
            [['identificacion', 'sitioWeb'], 'string', 'max' => 20],
            [['nombreEmpresa'], 'string', 'max' => 80],
            [['telefono', 'estado'], 'string', 'max' => 10],
            [['email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProveedorGastos' => 'Id Proveedor Gastos',
            'tipoEntidad' => 'Tipo Entidad',
            'identificacion' => 'Identificacion',
            'nombreEmpresa' => 'Nombre Empresa',
            'telefono' => 'Telefono',
            'sitioWeb' => 'Sitio Web',
            'email' => 'Email',
            'estado' => 'Estado',
        ];
    }
}
