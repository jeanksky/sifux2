<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_encabezado_factura".
 *
 * @property integer $idCabeza_Factura
 * @property string $fecha_inicio
 * @property string $fecha_final
 * @property string $fecha_vencimiento
 * @property string $idCliente
 * @property integer $idOrdenServicio
 * @property double $porc_descuento
 * @property double $iva
 * @property double $total_a_pagar
 * @property string $estadoFactura
 * @property string $tipoFacturacion
 * @property integer $codigoVendedor
 * @property double $subtotal
 * @property string $observacion
 *
 * @property TblDetalleFacturas[] $tblDetalleFacturas
 * @property TblHistCancelFacturaCr[] $tblHistCancelFacturaCrs
 * @property TblMedioPago[] $tblMedioPagos
 */
class MovimientoCredito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_encabezado_factura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_final', 'fecha_vencimiento'], 'safe'],
            [['idOrdenServicio', 'codigoVendedor'], 'integer'],
            [['porc_descuento', 'iva', 'total_a_pagar', 'subtotal'], 'number'],
            [['estadoFactura'], 'required'],
            [['idCliente'], 'string', 'max' => 80],
            [['estadoFactura'], 'string', 'max' => 12],
            [['tipoFacturacion'], 'string', 'max' => 10],
            [['observacion'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCabeza_Factura' => 'Id Cabeza  Factura',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_final' => 'Fecha Final',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'idCliente' => 'Id Cliente',
            'idOrdenServicio' => 'Id Orden Servicio',
            'porc_descuento' => 'Porc Descuento',
            'iva' => 'Iva',
            'total_a_pagar' => 'Total A Pagar',
            'estadoFactura' => 'Estado Factura',
            'tipoFacturacion' => 'Tipo Facturacion',
            'codigoVendedor' => 'Codigo Vendedor',
            'subtotal' => 'Subtotal',
            'observacion' => 'Observacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblDetalleFacturas()
    {
        return $this->hasMany(TblDetalleFacturas::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblHistCancelFacturaCrs()
    {
        return $this->hasMany(TblHistCancelFacturaCr::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblMedioPagos()
    {
        return $this->hasMany(TblMedioPago::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    public function getMovimiento_de_dias($fecha_i,$fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);
        return $dias;
    }
}
