<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_historial_envio_email".
 *
 * @property integer $idHistorial
 * @property string $tipo_documento
 * @property integer $idDocumento
 * @property string $fecha_envio
 * @property string $email
 */
class HistorialEnvioEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_historial_envio_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDocumento'], 'integer'],
            [['fecha_envio'], 'safe'],
            [['tipo_documento'], 'string', 'max' => 3],
            [['email'], 'string', 'max' => 500],
            [['usuario'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idHistorial' => 'Id Historial',
            'tipo_documento' => 'Tipo Documento',
            'idDocumento' => 'Id Documento',
            'fecha_envio' => 'Fecha Envio',
            'email' => 'Email',
        ];
    }
}
