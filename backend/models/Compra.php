<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;
use backend\models\ProductoServicios;//Prueba
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;


	class Compra
	{

		public static function getContenidoCompra() {
			if(is_string(Yii::$app->session->get('compra'))){
				return JSON::decode(Yii::$app->session->get('compra'), true);
			}
			else
				return Yii::$app->session->get('compra');
		}

		public static function setContenidoCompra($compra) {
			return Yii::$app->session->set('compra', JSON::encode($compra));
		}
		//----------------------------------------------------------
		/*public static function getContenidoCheque() {
			if(is_string(Yii::$app->session->get('cheque')))
				return CJSON::decode(Yii::$app->session->get('cheque'), true);
			else
				return Yii::$app->session->get('cheque');
		}

		public static function setContenidoCheque($cheque) {
			return Yii::$app->session->set('cheque', CJSON::encode($cheque));
		}*/
		//----------------------------------------------------------
		/*public static function getProveedor() {
			return Yii::$app->session->get('proveedor');
		}

		public static function setProveedor($proveedor) {
			return Yii::$app->session->set('proveedor', $proveedor);
		}*/
		//----------------------------------------------------------
		public static function getCabezaFactura() {//es auxiliar
			return Yii::$app->session->get('cabfauxiliar');
		}
		public static function setCabezaFactura($cabfauxiliar) {
			return Yii::$app->session->set('cabfauxiliar', $cabfauxiliar);
		}
		//----------------------------------------------------------
		public static function getVendedor() {
			return Yii::$app->session->get('vendedor');
		}
		public static function setVendedor($vendedor) {
			return Yii::$app->session->set('vendedor', $vendedor);
		}
		//----------------------------------------------------------
		public static function getCliente() {
			return Yii::$app->session->get('cliente');
		}

		public static function setCliente($cliente) {
			return Yii::$app->session->set('cliente', $cliente);
		}
		//----------------------------------------------------------
		public static function getEstadoFactura() {
			return Yii::$app->session->get('estado');
		}

		public static function setEstadoFactura($estado) {
			return Yii::$app->session->set('estado', $estado);
		}
		//----------------------------------------------------------
		public static function getFechaInicial() {
			return Yii::$app->session->get('fecha');
		}

		public static function setFechaInicial($fecha) {
			return Yii::$app->session->set('fecha', $fecha);
		}
		//----------------------------------------------------------
		public static function getPago() {
			return Yii::$app->session->get('pagoc');
		}

		public static function setPago($pagoc) {
			return Yii::$app->session->set('pagoc', $pagoc);
		}

		//----------------------------------------------------------
		public static function getAgenteComision() {
			return Yii::$app->session->get('ageComi');
		}

		public static function setAgenteComision($ageComi) {
			return Yii::$app->session->set('ageComi', $ageComi);
		}

		//----------------------------------------------------------
				public static function getOT() {
					return Yii::$app->session->get('modoot');
				}

				public static function setOT($modoot) {
					return Yii::$app->session->set('modoot', $modoot);
				}

	 //----------------------------------------------------------
		public static function getMedio() {
			return Yii::$app->session->get('medio');
		}

		public static function setMedio($medio) {
			return Yii::$app->session->set('medio', $medio);
		}
		//----------------------------------------------------------
		public static function getComproba() {
			return Yii::$app->session->get('comprobacion');
		}

		public static function setComproba($comprobacion) {
			return Yii::$app->session->set('comprobacion', $comprobacion);
		}

		//----------------------------------------------------------
		public static function getObserva() {
			return Yii::$app->session->get('observa');
		}

		public static function setObserva($observa) {
			return Yii::$app->session->set('observa', $observa);
		}
		//----------------------------------------------------------
		/*public static function getDoc() {
			return Yii::$app->session->get('doc');
		}

		public static function setDoc($doc) {
			return Yii::$app->session->set('doc', $doc);
		}*/
		//----------------------------------------------------------
		public static function getServicio_porc_ign() {
			return Yii::$app->session->get('porc_ign');
		}

		public static function setServicio_porc_ign($porc_ign) {
			return Yii::$app->session->set('porc_ign', $porc_ign);
		}
		//----------------------------------------------------------
		public static function getTotal($par) {
			$subtotal=$total=$iva=$dcto=$dcto_imp=$descuento=0;
			$prod_con_impuesto = 0;
			$prod_sin_impuesto = 0;
			$selec_des = '<select class="form-control" id="sel1">
							<option value="" selected disabled>Seleccione %</option>
							<option value="">0%</option>
					        <option value="1">1%</option>
					        <option value="2">2%</option>
					        <option value="3">3%</option>
					        <option value="4">4%</option>
					        <option value="5">5%</option>
					        <option value="6">6%</option>
					        <option value="7">7%</option>
					        <option value="8">8%</option>
					        <option value="9">9%</option>
					        <option value="10">10%</option>
					      </select>';
			$editardescuento = Html::a('', null, [
                                'class' => 'glyphicon glyphicon-retweet',
                                'id' => 'editardescuento',
                                'data-toggle' => 'popover',
                                'data-placement' => 'left',
                                //'data-trigger'=>'hover',
                                'data-html'=>'true',
                                'data-content'=>$selec_des,
                                'title' => Yii::t('app', 'Editar descuento'),
                                ]);
			if(@$compra=Compra::getContenidoCompra())
			foreach($compra as $product)  {

				//obtengo el subtotal
				$subtotal += @$product['cantidad']*@$product['precio'];

				//obtengo descuento de los productos que no tienen impuestos
				$dcto += $product['iva'] != 0 ? 0 : ((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//obtengo descuento de los productos que tienen impuestos
				$dcto_imp += $product['iva'] == 0 ? 0 : ((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//suma de ambos descuento
				$descuento = $dcto + $dcto_imp;
				//obtengo la suma del precio + cantidad de los productos sin impuesto
				$prod_sin_impuesto += $product['iva'] != 0 ? 0 : @$product['cantidad']*$product['precio'];
				//obtengo la suma del precio + cantidad de los productos con impuesto
				$prod_con_impuesto += $product['iva'] == 0 ? 0 : @$product['cantidad']*$product['precio'];

				//Para obtener el impuesto resto el descuento de los productos con descuento con los productos con impuestos
				//luego los multiplico por el 0.13 que es el 13% del impuesto de venta
				$iva = ($prod_con_impuesto - $dcto_imp) * Yii::$app->params['impuesto'];
				//tp es el total de productos con y sin impuesto
				$tp = $prod_sin_impuesto + $prod_con_impuesto;
				//el total a pagar resto los dos descuentos al tp para luego sumarlo al iva
				$total = ($tp-($dcto+$dcto_imp))+$iva;

		}

		if($par==true){
			return array(
									'subtotal'=>$subtotal,
									'dcto'=>$descuento,
									'iva'=>$iva,
									'total'=>$total
									);
		}
		else {
			$servicio_ignorado = Compra::getServicio_porc_ign();
			$servicio = ($subtotal - $descuento)*(Yii::$app->params['porciento_servicio']/100);
			if (Yii::$app->params['servicioRestaurante']==true && $servicio_ignorado != 'checked') {
				return  '
				<div class="resumen">
					<h4>SERVICIO 10% (EX):<span style="float:right">'.number_format($servicio,2).'</span></h4>
					 <h3>RESUMEN</h3><hr>
					 <h4>SUBTOTAL:<span style="float:right">'.number_format($subtotal+$servicio,2).'</span></h4>
					 <h4>TOTAL DESCUENTO:<span style="float:right">'.number_format($descuento,2).'</span></h4>
					 <h4>TOTAL IV:<span style="float:right">'.number_format($iva,2).'</span></h4>
					 <h4>TOTAL A PAGAR:<span style="float:right">'.number_format($total+$servicio,2).'</span></h4>
				</div>';
			} else {
				return  '
				<div class="resumen">
					 <h3>RESUMEN</h3><hr>
					 <h4>SUBTOTAL:<span style="float:right">'.number_format($subtotal,2).'</span></h4>
					 <h4>TOTAL DESCUENTO:<span style="float:right">'.number_format($descuento,2).'</span></h4>
					 <h4>TOTAL IV:<span style="float:right">'.number_format($iva,2).'</span></h4>
					 <h4>TOTAL A PAGAR:<span style="float:right">'.number_format($total,2).'</span></h4>
				</div>';
			}
		}

		}

	//----------------------------------------------------------
		public static function getVendedornull() {
			return Yii::$app->session->get('venull');
		}

		public static function setVendedornull($venull) {
			return Yii::$app->session->set('venull', $venull);
		}

		//---------------------------------O.T
		public static function getNombre_ot() {
			return Yii::$app->session->get('nombre_ot');
		}

		public static function setNombre_ot($nombre_ot) {
			return Yii::$app->session->set('nombre_ot', $nombre_ot);
		}
		//---------------------------------O.T
		public static function getPlaca_ot() {
			return Yii::$app->session->get('placa_ot');
		}

		public static function setPlaca_ot($placa_ot) {
			return Yii::$app->session->set('placa_ot', $placa_ot);
		}
		//---------------------------------O.T
		public static function getMarca_ot() {
			return Yii::$app->session->get('marcas_ot');
		}

		public static function setMarca_ot($marcas_ot) {
			return Yii::$app->session->set('marcas_ot', $marcas_ot);
		}
		//---------------------------------O.T
		public static function getModelo_ot() {
			return Yii::$app->session->get('modelo_ot');
		}

		public static function setModelo_ot($modelo_ot) {
			return Yii::$app->session->set('modelo_ot', $modelo_ot);
		}
		//---------------------------------O.T
		public static function getVersion_ot() {
			return Yii::$app->session->get('version_ot');
		}

		public static function setVersion_ot($version_ot) {
			return Yii::$app->session->set('version_ot', $version_ot);
		}
		//---------------------------------O.T
		public static function getAno_ot() {
			return Yii::$app->session->get('ano_ot');
		}

		public static function setAno_ot($ano_ot) {
			return Yii::$app->session->set('ano_ot', $ano_ot);
		}
		//---------------------------------O.T
		public static function getMotor_ot() {
			return Yii::$app->session->get('motor_ot');
		}

		public static function setMotor_ot($motor_ot) {
			return Yii::$app->session->set('motor_ot', $motor_ot);
		}
		//---------------------------------O.T
		public static function getCilindrada_ot() {
			return Yii::$app->session->get('cilindrada_ot');
		}

		public static function setCilindrada_ot($cilindrada_ot) {
			return Yii::$app->session->set('cilindrada_ot', $cilindrada_ot);
		}

		//---------------------------------O.T
		public static function getCombustible_ot() {
			return Yii::$app->session->get('combustible_ot');
		}

		public static function setCombustible_ot($combustible_ot) {
			return Yii::$app->session->set('combustible_ot', $combustible_ot);
		}

		//---------------------------------O.T
		public static function getTransmision_ot() {
			return Yii::$app->session->get('transmision_ot');
		}

		public static function setTransmision_ot($transmision_ot) {
			return Yii::$app->session->set('transmision_ot', $transmision_ot);
		}
	}
