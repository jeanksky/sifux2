<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_producto_proveedores".
 *
 * @property integer $idProd_prov
 * @property string $codProdServicio
 * @property integer $codProveedores
 * @property string $codigo_proveedor
 */
class ProductoProveedores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_producto_proveedores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProveedores'], 'integer'],
            [['codProdServicio'], 'string', 'max' => 20],
            [['codigo_proveedor'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProd_prov' => 'Id Prod Prov',
            'codProdServicio' => 'Cod Prod Servicio',
            'codProveedores' => 'Cod Proveedores',
            'codigo_proveedor' => 'Codigo Proveedor',
        ];
    }
}
