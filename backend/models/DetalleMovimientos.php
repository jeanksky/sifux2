<?php

namespace backend\models;

use backend\models\ProductoServicios;
use backend\models\Movimientos;
use Yii;

/**
 * This is the model class for table "tbl_detalle_movimientos".
 *
 * @property integer $idDetalleMovimiento
 * @property integer $idMovimiento
 * @property string $codProdServicio
 * @property integer $cantidad
 * @property string $observaciones
 */
class DetalleMovimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_detalle_movimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idMovimiento', 'codProdServicio', 'cantidad', 'cantidadinv'/*, 'observaciones','estado'*/ ], 'required'],
            [['idMovimiento'], 'integer'],
            [['cantidad', 'cantidadinv'], 'number'],
            // [[''], 'string', 'max' => 11],
            // [['observaciones'], 'string', 'max' => 1000],
            // [['estado'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDetalleMovimiento' => 'Id Detalle Movimiento',
            'idMovimiento' => 'Id Movimiento',
            'codProdServicio' => 'Cod Prod Servicio',
            'cantidad' => 'Cantidad',
            // 'observaciones' => 'Observaciones',
            // 'estado' => 'Estado',
        ];
    }

       public function getIdMovimiento()
    {
        return $this->hasOne(Movimientos::className(), ['idMovimiento' => 'idMovimiento']);
    }

    public function getCodProdServicio()
    {
        return $this->hasOne(ProductoServicios::className(), ['codProdServicio' => 'codProdServicio']);
    }
}
