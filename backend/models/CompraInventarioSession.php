<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;
//use backend\models\Compra;//Prueba

	class CompraInventarioSession
	{
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//---------------------CREATE-------------------------------------------------------
//----------------------------------------------------------------------------------
		public static function getContenidoProducto() {
			if(is_string(Yii::$app->session->get('prodcom'))){
				return JSON::decode(Yii::$app->session->get('prodcom'), true);
			}
			else
				return Yii::$app->session->get('prodcom');
		}

		public static function setContenidoProducto($prodcom) {
			return Yii::$app->session->set('prodcom', JSON::encode($prodcom));
		}

		//------------//Estas 2 funciones son unicas para obtener la secion de encabezado
		public static function getFormapago() {
			return Yii::$app->session->get('cab');
		}

		public static function setFormapago($cab) {
			return Yii::$app->session->set('cab', $cab);
		}
		//------------//Estas 2 funciones son unicas para obtener la fecha de registro
		public static function getFechadocumento() {
			return Yii::$app->session->get('fechado');
		}

		public static function setFechadocumento($fechado) {
			return Yii::$app->session->set('fechado', $fechado);
		}
		//------------//Estas 2 funciones son unicas para obtener la fecha de registro
		public static function getFecharegistro() {
			return Yii::$app->session->get('fechare');
		}

		public static function setFecharegistro($fechare) {
			return Yii::$app->session->set('fechare', $fechare);
		}
		//------------//Estas 2 funciones son unicas para obtener la fecha de vencimiento
		public static function getFechavencimiento() {
			return Yii::$app->session->get('fechavei');
		}

		public static function setFechavencimiento($fechavei) {
			return Yii::$app->session->set('fechavei', $fechavei);
		}
		//------------//Estas 2 funciones son unicas para obtener el proveedor
		public static function getProveedor() {
			return Yii::$app->session->get('provee');
		}

		public static function setProveedor($provee) {
			return Yii::$app->session->set('provee', $provee);
		}
		//------------//Estas 2 funciones son unicas para obtener el no factura electronico
		public static function getNofactura() {
			return Yii::$app->session->get('nofa');
		}

		public static function setNofactura($nofa) {
			return Yii::$app->session->set('nofa', $nofa);
		}

		//------------//Estas 2 funciones son unicas para obtener el no interno de factura de proveedor
		public static function getInterno_prov() {
			return Yii::$app->session->get('noinpro');
		}

		public static function setInterno_prov($noinpro) {
			return Yii::$app->session->set('noinpro', $noinpro);
		}

		//------------//Estas 2 funciones son unicas para obtener el subtotal
		public static function getSubtotal() {
			return Yii::$app->session->get('subtotal');
		}

		public static function setSubtotal($subtotal) {
			return Yii::$app->session->set('subtotal', $subtotal);
		}
		//------------//Estas 2 funciones son unicas para obtener el descuento
		public static function getDescuento() {
			return Yii::$app->session->get('descuento');
		}

		public static function setDescuento($descuento) {
			return Yii::$app->session->set('descuento', $descuento);
		}
		//------------//Estas 2 funciones son unicas para obtener el impuesto
		public static function getImpuesto() {
			return Yii::$app->session->get('impuesto');
		}

		public static function setImpuesto($impuesto) {
			return Yii::$app->session->set('impuesto', $impuesto);
		}
		//------------//Estas 2 funciones son unicas para obtener total
		public static function getTotal() {
			return Yii::$app->session->get('totalco');
		}

		public static function setTotal($totalco) {
			return Yii::$app->session->set('totalco', $totalco);
		}

		//------------Estas funciones son auxiliares me
		//------------permiten agregar un producto a la modal que alimenta la tabla de compra cuando se
		//------------ingresa un nuevo producto a base de datos
		public static function getProductoAir() {
			return Yii::$app->session->get('p_air');
		}

		public static function setProductoAir($p_air) {
			return Yii::$app->session->set('p_air', $p_air);
		}
		//--------------------------------------------------------------------
		public static function getPrecioAir() {
			return Yii::$app->session->get('precio_air');
		}

		public static function setPrecioAir($precio_air) {
			return Yii::$app->session->set('precio_air', $precio_air);
		}
		//--------------------------------------------------------------------
		public static function getUtilidadAir() {
			return Yii::$app->session->get('util_air');
		}

		public static function setUtilidadAir($util_air) {
			return Yii::$app->session->set('util_air', $util_air);
		}
		//--------------------------------------------------------------------
		public static function getVentadAir() {
			return Yii::$app->session->get('vent_air');
		}

		public static function setVentaAir($vent_air) {
			return Yii::$app->session->set('vent_air', $vent_air);
		}
		//--------------------------------------------------------------------
		public static function getVentaImdAir() {
			return Yii::$app->session->get('ventI_air');
		}

		public static function setVentaImAir($ventI_air) {
			return Yii::$app->session->set('ventI_air', $ventI_air);
		}

		//funcion para que me consulte el descuento que lo necesito cada ves que ingreso el primero
    public static function getDescuento_consecutivo()
    {
    	return Yii::$app->session->get('desc_cons');
    }
		//funcion para que me almacena el descuento que lo necesito cada ves que ingreso el primero
		public static function setDescuento_consecutivo($desc_cons)
    {
    	return Yii::$app->session->set('desc_cons', $desc_cons);
    }

		//funcion para que me consulte la orden de compra que tengo asignada de forma temporal
    public static function getOrdenCompra()
    {
    	return Yii::$app->session->get('ord_compr');
    }
		//funcion para que me almacena de forma temporal la orden de compra que tengo que registrar como compra
		public static function setOrdenCompra($ord_compr)
    {
    	return Yii::$app->session->set('ord_compr', $ord_compr);
    }
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//---------------------UPDATE-------------------------------------------------------
//----------------------------------------------------------------------------------

		public static function getContenidoProductoup() {
			if(is_string(Yii::$app->session->get('prodcom_u'))){
				return JSON::decode(Yii::$app->session->get('prodcom_u'), true);
			}
			else
				return Yii::$app->session->get('prodcom_u');
		}

		public static function setContenidoProductoup($prodcom_u) {
			return Yii::$app->session->set('prodcom_u', JSON::encode($prodcom_u));
		}

		//------------//Estas 2 funciones son unicas para obtener la secion de encabezado
		public static function getFormapagoup() {
			return Yii::$app->session->get('cab_u');
		}

		public static function setFormapagoup($cab_u) {
			return Yii::$app->session->set('cab_u', $cab_u);
		}
		//------------//Estas 2 funciones son unicas para obtener la fecha de registro
		public static function getFechadocumentoup() {
			return Yii::$app->session->get('fechado_u');
		}

		public static function setFechadocumentoup($fechado_u) {
			return Yii::$app->session->set('fechado_u', $fechado_u);
		}
		//------------//Estas 2 funciones son unicas para obtener la fecha de registro
		public static function getFecharegistroup() {
			return Yii::$app->session->get('fechare_u');
		}

		public static function setFecharegistroup($fechare_u) {
			return Yii::$app->session->set('fechare_u', $fechare_u);
		}
		//------------//Estas 2 funciones son unicas para obtener la fecha de vencimiento
		public static function getFechavencimientoup() {
			return Yii::$app->session->get('fechavei_u');
		}

		public static function setFechavencimientoup($fechavei_u) {
			return Yii::$app->session->set('fechavei_u', $fechavei_u);
		}
		//------------//Estas 2 funciones son unicas para obtener el proveedor
		public static function getProveedorup() {
			return Yii::$app->session->get('provee_u');
		}

		public static function setProveedorup($provee_u) {
			return Yii::$app->session->set('provee_u', $provee_u);
		}
		//------------//Estas 2 funciones son unicas para obtener el numer de factura del proveedor
		public static function getNofacturaup() {
			return Yii::$app->session->get('nofa_u');
		}

		public static function setNofacturaup($nofa_u) {
			return Yii::$app->session->set('nofa_u', $nofa_u);
		}
		//------------//Estas 2 funciones son unicas para obtener el no interno de factura de proveedor
		public static function getInterno_provup() {
			return Yii::$app->session->get('noinpro_u');
		}

		public static function setInterno_provup($noinpro_u) {
			return Yii::$app->session->set('noinpro_u', $noinpro_u);
		}
		//------------//Estas 2 funciones son unicas para obtener el subtotal
		public static function getSubtotalup() {
			return Yii::$app->session->get('subtotal_u');
		}

		public static function setSubtotalup($subtotal_u) {
			return Yii::$app->session->set('subtotal_u', $subtotal_u);
		}
		//------------//Estas 2 funciones son unicas para obtener el descuento
		public static function getDescuentoup() {
			return Yii::$app->session->get('descuento_u');
		}

		public static function setDescuentoup($descuento_u) {
			return Yii::$app->session->set('descuento_u', $descuento_u);
		}
		//------------//Estas 2 funciones son unicas para obtener el impuesto
		public static function getImpuestoup() {
			return Yii::$app->session->get('impuesto_u');
		}

		public static function setImpuestoup($impuesto_u) {
			return Yii::$app->session->set('impuesto_u', $impuesto_u);
		}
		//------------//Estas 2 funciones son unicas para obtener total
		public static function getTotalup() {
			return Yii::$app->session->get('impuesto_u');
		}

		public static function setTotalup($impuesto_u) {
			return Yii::$app->session->set('impuesto_u', $impuesto_u);
		}
		//------------Estas funciones son auxiliares me
		//------------permiten agregar un producto a la modal que alimenta la tabla de compra cuando se
		//------------ingresa un nuevo producto a base de datos
		public static function getProductoAirup() {
			return Yii::$app->session->get('p_air_u');
		}

		public static function setProductoAirup($p_air_u) {
			return Yii::$app->session->set('p_air_u', $p_air_u);
		}
		//--------------------------------------------------------------------
		public static function getPrecioAirup() {
			return Yii::$app->session->get('precio_air_u');
		}

		public static function setPrecioAirup($precio_air_u) {
			return Yii::$app->session->set('precio_air_u', $precio_air_u);
		}
		//--------------------------------------------------------------------
		public static function getUtilidadAirup() {
			return Yii::$app->session->get('util_air_u');
		}

		public static function setUtilidadAirup($util_air_u) {
			return Yii::$app->session->set('util_air_u', $util_air_u);
		}
		//--------------------------------------------------------------------
		public static function getVentadAirup() {
			return Yii::$app->session->get('util_air_u');
		}

		public static function setVentaAirup($util_air_u) {
			return Yii::$app->session->set('util_air_u', $util_air_u);
		}
		//--------------------------------------------------------------------
		public static function getVentaImdAirup() {
			return Yii::$app->session->get('ventI_air_u');
		}

		public static function setVentaImAirup($ventI_air_u) {
			return Yii::$app->session->set('ventI_air_u', $ventI_air_u);
		}

		public static function getactualizando(){
			return Yii::$app->session->get('estadoac');
		}

		public static function setactualizando($estadoac) {
			return Yii::$app->session->set('estadoac', $estadoac);
		}

		//--------------------------------------------------------------------------
		//--------SESSION PARA CONFIRMAR QUE EXISTEN DATOS CARGADOS PARA RECEPCION
		public static function getRecepcion_doc(){
			return Yii::$app->session->get('redoc');
		}

		public static function setRecepcion_doc($redoc) {
			return Yii::$app->session->set('redoc', $redoc);
		}

		//--------SESSION PARA ALMACENAR EL CONSECUTIVO ELECTRONICO
		public static function getConsecutivo_electr(){
			return Yii::$app->session->get('consec_electr');
		}

		public static function setConsecutivo_electr($consec_electr) {
			return Yii::$app->session->set('consec_electr', $consec_electr);
		}

		//--------SESSION PARA EL TIPO DOCUMENTO ELECTRONICO
		public static function getTipo_doc_electr(){
			return Yii::$app->session->get('ti_do_el');
		}

		public static function setTipo_doc_electr($ti_do_el) {
			return Yii::$app->session->set('ti_do_el', $ti_do_el);
		}

		//--------SESSION PARA LA FECHA DEL DOCUMENTO ELECTRONICO
		public static function getFecha_doc_electr(){
			return Yii::$app->session->get('fe_do_el');
		}

		public static function setFecha_doc_electr($fe_do_el) {
			return Yii::$app->session->set('fe_do_el', $fe_do_el);
		}

		//--------SESSION PARA LOS DATOS QUE SE OCUPAN EN LA RECEPCION DE documentos
		public static function getDetalle_recepcion(){
			return Yii::$app->session->get('det_re_ha');
		}

		public static function setDetalle_recepcion($det_re_ha) {
			return Yii::$app->session->set('det_re_ha', $det_re_ha);
		}

		//--------SESSION PARA ALMACENAR EL XML DE RECEPCION
		public static function getCarga_xml(){
			return Yii::$app->session->get('carga_xml_re');
		}

		public static function setCarga_xml($carga_xml_re) {
			return Yii::$app->session->set('carga_xml_re', $carga_xml_re);
		}

		//--------SESSION PARA ALMACENAR EL EMISOR DE RECEPCION
		public static function getEmisor_recepcion(){
			return Yii::$app->session->get('emisor_re');
		}

		public static function setEmisor_recepcion($emisor_re) {
			return Yii::$app->session->set('emisor_re', $emisor_re);
		}

		//--------SESSION PARA ALMACENAR EL ID EMISOR DE RECEPCION
		public static function getId_emisor_recepcion(){
			return Yii::$app->session->get('id_emisor_re');
		}

		public static function setId_emisor_recepcion($id_emisor_re) {
			return Yii::$app->session->set('id_emisor_re', $id_emisor_re);
		}

		//Sessiones para almacenar campos especificos del resumen de la factura
		public static function getSubtotal_recepcion(){ return Yii::$app->session->get('sub_rec'); }
		public static function setSubtotal_recepcion($sub_rec) { return Yii::$app->session->set('sub_rec', $sub_rec); }

		public static function getDescuento_recepcion(){ return Yii::$app->session->get('desc_rec'); }
		public static function setDescuento_recepcion($desc_rec) { return Yii::$app->session->set('desc_rec', $desc_rec); }

		public static function getImpuesto_recepcion(){ return Yii::$app->session->get('imp_rec'); }
		public static function setImpuesto_recepcion($imp_rec) { return Yii::$app->session->set('imp_rec', $imp_rec); }

		public static function getMonto_pagar_recepcion(){ return Yii::$app->session->get('mopa_rec'); }
		public static function setMonto_pagar_recepcion($mopa_rec) { return Yii::$app->session->set('mopa_rec', $mopa_rec); }

		//--------------------SESSION PARA ALMACENAR Referencia
		public static function getReferencia_recepcion(){ return Yii::$app->session->get('ref_rec'); }
		public static function setReferencia_recepcion($ref_rec) { return Yii::$app->session->set('ref_rec', $ref_rec); }
		//--------------------------------------------------------------------

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//---------------------ETIQUETASS-------------------------------------------------------
//----------------------------------------------------------------------------------
		/*public static function getContenidoProductoetiq() {
			if(is_string(Yii::$app->session->get('etiqueta'))){
				return JSON::decode(Yii::$app->session->get('etiqueta'), true);
			}
			else
				return Yii::$app->session->get('etiqueta');
		}

		public static function setContenidoProductoetiq($etiqueta) {
			return Yii::$app->session->set('etiqueta', JSON::encode($etiqueta));
		}*/

	}
