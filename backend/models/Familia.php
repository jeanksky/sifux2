<?php

namespace backend\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "tbl_familia".
 *
 * @property integer $codFamilia
 * @property string $descripcion
 * @property string $estado
 *
 * @property TblProductoServicios[] $tblProductoServicios
 */
class Familia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_familia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion', 'estado'], 'required'],
            [['descripcion'], 'string', 'max' => 40],
            [['descripcion'], 'unique'],
            [['estado'], 'string', 'max' => 10],
            [['tipo'], 'string', 'max' => 10],
            // [['estado'], 'default', 'value' => function ($model, $attribute) {
            // return ($attribute === 'Activo' ? '1' : '0'); }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codFamilia' => 'Código',
            'descripcion' => 'Categoría',
            'estado' => 'Estado',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilia()
    {
        return $this->hasMany(ProductoServicios::className(), ['codFamilia' => 'codFamilia']);
    }

     public static function getNombreFamilia()
    {
        return Familia::find()
        ->where(['=','estado','Activo'])
        ->andwhere(['=','tipo','Servicio'])
        ->orderBy('descripcion ASC')
        ->asArray()
        ->all();
    }

     public function beforeSave($insert)
    {
        if (($this->estado === 'Activo')) {

          $this->estado = '1';
          return parent::beforeSave($insert);

        } else {
            $this->estado = '0';
            return parent::beforeSave($insert);

        }
    }

    public function afterFind()
        {
            if (($this->estado === '1')) {

            $this->estado = 'Activo';
            return parent::afterFind();

        } else {

            $this->estado = 'Inactivo';
            return parent::afterFind();

        }


        }

}
