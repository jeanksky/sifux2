<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_proveedores".
 *
 * @property integer $codProveedores
 * @property string $nombreEmpresa
 * @property string $cedula
 * @property string $telefono
 * @property string $nombreContacto
 * @property string $observaciones
 * @property integer $diasCredito
 * @property string $telefono2
 * @property string $telefono3
 * @property integer $idMoneda
 * @property string $credi_saldo
 * @property string $notific_cntc_pagos
 * @property string $notific_cntc_pedidos
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_proveedores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreEmpresa','tipoIdentidad','cedula', 'telefono', 'nombreContacto', 'notific_cntc_pagos', 'notific_cntc_pedidos','idMoneda'/*, 'observaciones'*/], 'required'],
            [['nombreEmpresa'], 'string', 'max' => 40],
            [['tipoIdentidad'], 'string', 'max' => 9],
            [['cedula'], 'string', 'max' => 20],
            [['diasCredito','idMoneda'], 'integer'],
            [['credi_saldo'], 'number'],
            [['telefono','telefono2','telefono3'], 'string', 'max' => 10],
            [['nombreContacto'], 'string', 'max' => 80],
            [['observaciones'], 'string', 'max' => 130],
            [['notific_cntc_pagos', 'notific_cntc_pedidos'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codProveedores' => 'Código',
            'nombreEmpresa' => 'Nombre de empresa',
            'tipoIdentidad' => 'Tipo entidad*',
            'cedula' => 'Cedula de empresa',
            'telefono' => 'Teléfono',
            'idMoneda' => 'Moneda',
            'notific_cntc_pagos' => 'Notificación contacto pagos',
            'notific_cntc_pedidos' => 'Notificación contacto pedidos',
            'telefono2' => '2do Teléfono',
            'telefono3' => '3er Teléfono',
            'nombreContacto' => 'Nombre de contacto',
            'diasCredito' => 'Días de Crédito',
            'observaciones' => 'Observaciones',
            'credi_saldo' => 'Saldo proveedor'
        ];
    }
}
