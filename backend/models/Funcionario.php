<?php

namespace backend\models;

use Yii;

/*use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\SignupForm;
use backend\models\ContactForm;*/

/**
 * This is the model class for table "tbl_funcionario".
 *
 * @property integer $idFuncionario
 * @property string $identificacion
 * @property string $nombre
 * @property string $apellido1
 * @property string $apellido2
 * @property string $genero
 * @property string $tipoFuncionario
 * @property string $usuario
 * @property string $email
 * @property string $tipo_usuario
 * @property string $estado
 */
class Funcionario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_funcionario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* return [
            [['identificacion', 'nombre', 'apellido1', 'genero', 'tipoFuncionario', 'username', 'email', 'tipo_usuario', 'estado'], 'required'],
            [['identificacion', 'username'], 'string', 'max' => 20],
            [['nombre'], 'string', 'max' => 30],
            [['apellido1', 'apellido2'], 'string', 'max' => 15],
            [['genero'], 'string', 'max' => 10],
            [['tipoFuncionario', 'tipo_usuario', 'estado'], 'string', 'max' => 13],
            [['email'], 'string', 'max' => 50]
        ];*/
        return [
            [['identificacion', 'nombre', 'apellido1', 'genero','telefono', 'tipoFuncionario', 'username', 'email' , 'tipo_usuario', 'estado'], 'required'],
            [['identificacion'], 'string', 'max' => 20],
            [['identificacion','email'], 'unique'],
            [['nombre'], 'string', 'max' => 30],
            [['apellido1', 'apellido2'], 'string', 'max' => 15],
            [['genero','telefono'], 'string', 'max' => 10],
            [['tipoFuncionario', 'tipo_usuario', 'estado'], 'string', 'max' => 13],

            [['username'], 'filter', 'filter' => 'trim'],
            [['username'], 'unique'],
            [['username'], 'string', 'min' => 4, 'max' => 255],
            [['email'], 'filter', 'filter' => 'trim'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 130],

           /* ['password', 'required'],
            ['password', 'string', 'min' => 4]*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFuncionario' => 'Código',
            'identificacion' => 'Identificación',
            'nombre' => 'Nombre',
            'apellido1' => 'Primer apellido',
            'apellido2' => 'Segundo apellido',
            'genero' => 'Género',
            'tipoFuncionario' => 'Puesto de funcionario',
            'username' => 'Usuario',
            'email' => 'E-mail',
            'telefono' => 'Teléfono',
            'tipo_usuario' => 'Tipo de usuario',
            'estado' => 'Estado',  
          //  'password' => 'Password',  
        ];
    }

    // public function getfullName()
    //     {
    //             $model1 = new Funcionario();
    //             if ($model1->load(Yii::$app->request->post())){
    //                 return $model1->nombre.' '.$model1->apellido1.' '.$model1->apellido2;
    //             }
    //             if ($this->load(Yii::$app->request->post())){
    //                 return $this->nombre.' '.$this->apellido1.' '.$this->apellido2;
    //             }        
    //     }
        
    public function getUsername()
        {
               /* $model2 = new Funcionario();
                if ($model2->load(Yii::$app->request->post())){
                    return $model2->username;
                }*/
                if ($this->load(Yii::$app->request->post())){
                    return $this->username;
                    //return Yii::$app->request->post('username');
                }
                
        }
    public function getEmail()
        {
               /* $model3 = new Funcionario();
                if ($model3->load(Yii::$app->request->post())){
                    return $model3->email;
                }*/
                if ($this->load(Yii::$app->request->post())){
                    return $this->email;
                    //return Yii::$app->request->post('email');
                }
        }
   /* public function getCodigo()
        {
                $model4 = new Funcionario();
                if ($model4->load(Yii::$app->request->post())){
                    return $model4->idFuncionario;
                }
                
        }*/
    public function getTipoUsuario()
        {
              /*  $model5 = new Funcionario();
                if ($model5->load(Yii::$app->request->post())){
                    return $model5->tipo_usuario;
                }*/
                if ($this->load(Yii::$app->request->post())){
                    return $this->tipo_usuario;
                    //return Yii::$app->request->post('tipo_usuario');
                }
                
        }
  /*  public function getPass()
        {
                $model6 = new Funcionario();
                if ($model6->load(Yii::$app->request->post())){
                    return $model6->tipo_usuario;
                }
                
        }*/
    public function getEstado()
        {
               /* $model7 = new Funcionario();
                if ($model7->load(Yii::$app->request->post())){
                    return $model7->estado;
                }*/
                if ($this->load(Yii::$app->request->post())){
                    return $this->estado;
                    //return Yii::$app->request->post('estado');
                }
                
        }
    public function getName(){
        if ($this->load(Yii::$app->request->post())){
            return $this->nombre.' '.$this->apellido1.' '.$this->apellido2;
        }
    }

      public function getFullName()
        {
                return $this->nombre.' '.$this->apellido1.' '.$this->apellido2;
        }  
}
