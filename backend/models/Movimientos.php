<?php

namespace backend\models;

use yii\helpers\ArrayHelper;
use Yii;
use backend\models\ProductoServicios;
use backend\models\TipoMovimientos;
use backend\models\DetalleMovimientos;
/**
 * This is the model class for table "tbl_movimientos".
 *
 * @property integer $idMovimiento
 * @property string $documentoRef
 * @property string $observaciones
 * @property integer $codProdServicio
 * @property integer $cantidad
 * @property string $estado
 */
class Movimientos extends \yii\db\ActiveRecord
{

     public $password;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_movimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha','codigoTipo','documentoRef', 'observaciones','estado'/*, 'codProdServicio', 'cantidad', */], 'required'],
            // [['codProdServicio', 'cantidad'], 'integer'],
            [['documentoRef','observaciones'], 'string', 'max' => 1000],
            [['estado'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMovimiento' => 'No. Mov',
            'codigoTipo' => 'Tipo',
            'documentoRef' => 'Doc. Referencia',
             'observaciones' => 'Observaciones',
            'codProdServicio' => 'Producto',
            'cantidad' => 'Cantidad',
            'estado' => 'Estado',
            'fecha' => 'Fecha',

        ];
    }

    public function getIdMovimiento()
    {
        return $this->hasMany(DetalleMovimientos::className(), ['idMovimiento' => 'idMovimiento']);
    }

    public function getcodProdServicio0()
    {
        return $this->hasOne(ProductoServicios::className(), ['codProdServicio' => 'codProdServicio']);
    }

    public function getcodigoTipo0()
    {
        return $this->hasOne(TipoMovimientos::className(), ['codigoTipo' => 'codigoTipo']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
          return $this->fecha = date('Y-m-d', strtotime( $this->fecha));
         
        } else {
            return  $this->fecha = '0000-00-00'; 
             
        }
    }

    public function afterFind()
      {
      $this->fecha = date('d-m-Y', strtotime( $this->fecha));
       parent::afterFind();
      }

      public function getCantidadProductos()
      {
        
      }


    //   public function init()
    // {
    //     parent::init();

    //     $this->codProdServicio = [
    //         [
               
    //             'codProdServicio' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio', 'codProdServicio'),

    //             'cantidad'  => 1
    //         ],
    //     ];
    // }
}
