<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_entidades_financieras".
 *
 * @property integer $idEntidad_financiera
 * @property string $abreviatura
 * @property string $descripcion
 * @property string $telefono1
 * @property string $telefono2
 */
class EntidadesFinancieras extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_entidades_financieras';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abreviatura', 'descripcion'], 'required'],
            [['abreviatura', 'descripcion'], 'string', 'max' => 80],
            [['telefono1', 'telefono2'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEntidad_financiera' => 'Id Entidad Financiera',
            'abreviatura' => 'Abreviatura',
            'descripcion' => 'Descripcion',
            'telefono1' => 'Telefono1',
            'telefono2' => 'Telefono2',
        ];
    }
}
