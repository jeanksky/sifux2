<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_categoria_gastos".
 *
 * @property integer $idCategoriaGastos
 * @property string $descripcionCategoriaGastos
 * @property string $estadoCategoriaGastos
 */
class CategoriaGastos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_categoria_gastos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcionCategoriaGastos', 'estadoCategoriaGastos'], 'required'],
            [['descripcionCategoriaGastos'], 'string', 'max' => 30],
            [['estadoCategoriaGastos'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCategoriaGastos' => 'Id Categoria Gastos',
            'descripcionCategoriaGastos' => 'Descripcion Categoria Gastos',
            'estadoCategoriaGastos' => 'Estado Categoria Gastos',
        ];
    }
}
