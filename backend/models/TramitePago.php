<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_tramite_pago".
 *
 * @property integer $idTramite_pago
 * @property integer $idProveedor
 * @property string $cantidad_saldo_documento
 * @property string $monto_tramite_pago
 * @property string $porcentaje_descuento
 * @property string $monto_pagar
 * @property string $recibo_cancelacion
 * @property string $email_proveedor
 * @property string $prioridad_pago
 * @property string $usuario_registra
 * @property string $fecha_registra
 * @property string $usuario_cancela
 * @property string $fecha_cancela
 * @property string $usuario_aplica
 * @property string $fecha_aplica
 */
class TramitePago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tramite_pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProveedor', 'monto_saldo_pago', 'monto_tramite_pagar', /*'recibo_cancelacion', */'prioridad_pago'], 'required'],
            [['idProveedor'], 'integer'],
            [['cantidad_monto_documento', 'monto_saldo_pago', 'porcentaje_descuento', 'monto_tramite_pagar'], 'number'],
            [['fecha_registra', 'fecha_cancela', 'fecha_aplica', 'email_proveedor'], 'safe'],
            [['recibo_cancelacion', 'email_proveedor', 'estado_tramite'], 'string', 'max' => 50],
            [['detalle','detalle_reverso','detalle_anulacion','documento_banco','observaciones'], 'string', 'max' => 120],
            [['prioridad_pago'], 'string', 'max' => 10],
            [['usuario_registra', 'usuario_cancela', 'usuario_aplica'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTramite_pago' => 'Id Tramite Pago',
            'idProveedor' => 'Id Proveedor',
            'cantidad_monto_documento' => 'Cantidad Saldo Documento',
            'monto_saldo_pago' => 'Monto Tramite Pago',
            'porcentaje_descuento' => 'Porcentaje Descuento',
            'monto_tramite_pagar' => 'Monto Pagar',
            'recibo_cancelacion' => 'Recibo Cancelacion',
            'email_proveedor' => 'Email Proveedor',
            'prioridad_pago' => 'Prioridad Pago',
            'usuario_registra' => 'Usuario Registra',
            'fecha_registra' => 'Fecha Registra',
            'usuario_cancela' => 'Usuario Cancela',
            'fecha_cancela' => 'Fecha Cancela',
            'usuario_aplica' => 'Usuario Aplica',
            'fecha_aplica' => 'Fecha Aplica',
        ];
    }
}
