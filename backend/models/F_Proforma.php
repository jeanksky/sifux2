<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;
use backend\models\ProductoServicios;//Prueba


	class F_Proforma
	{

	//Session para crear proforma
		public static function getOrdenServicio() {//es auxiliar
			return Yii::$app->session->get('orden');
		}
		public static function setOrdenServicio($orden) {
			return Yii::$app->session->set('orden', $orden);
		}

		public static function getContenidoCompra() {
			if(is_string(Yii::$app->session->get('compra_prof'))){
				return JSON::decode(Yii::$app->session->get('compra_prof'), true);
			}
			else
				return Yii::$app->session->get('compra_prof');
		}

		public static function setContenidoCompra($compra_prof) {
			return Yii::$app->session->set('compra_prof', JSON::encode($compra_prof));
		}
		//------------------------------------------------------------------------------------
		/*public static function getCabezaFactura() {//es auxiliar
			return Yii::$app->session->get('cabfauxiliar');
		}
		public static function setCabezaFactura($cabfauxiliar) {
			return Yii::$app->session->set('cabfauxiliar', $cabfauxiliar);
		}*/
		//----------------------------------------------------------
		public static function getVendedor() {
			return Yii::$app->session->get('vendedor_prof');
		}
		public static function setVendedor($vendedor_prof) {
			return Yii::$app->session->set('vendedor_prof', $vendedor_prof);
		}
		//----------------------------------------------------------
		public static function getCliente() {
			return Yii::$app->session->get('cliente_prof');
		}

		public static function setCliente($cliente_prof) {
			return Yii::$app->session->set('cliente_prof', $cliente_prof);
		}
		//----------------------------------------------------------
		public static function getEstadoFactura() {
			return Yii::$app->session->get('estado_prof');
		}

		public static function setEstadoFactura($estado_prof) {
			return Yii::$app->session->set('estado_prof', $estado_prof);
		}
		//----------------------------------------------------------
		public static function getFecha() {
			return Yii::$app->session->get('fecha_prof');
		}

		public static function setFecha($fecha_prof) {
			return Yii::$app->session->set('fecha_prof', $fecha_prof);
		}
		//----------------------------------------------------------
		public static function getObserva() {
			return Yii::$app->session->get('observa_prof');
		}

		public static function setObserva($observa_prof) {
			return Yii::$app->session->set('observa_prof', $observa_prof);
		}
		//----------------------------------------------------------
		public static function getTotal($par=false) {
			$subtotal=$total=$iva=$dcto=$dcto_imp=$descuento=0;
			$prod_con_impuesto = 0;
			$prod_sin_impuesto = 0;
			if(@$compra=F_Proforma::getContenidoCompra())
			foreach($compra as $product)  {
				//obtengo el subtotal
				$subtotal += @$product['cantidad']*@$product['precio'];

				//obtengo descuento de los productos que no tienen impuestos
				$dcto += $product['iva'] != 0 ? 0 : ((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//obtengo descuento de los productos que tienen impuestos
				$dcto_imp += $product['iva'] == 0 ? 0 : ((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//suma de ambos descuento
				$descuento = $dcto + $dcto_imp;
				//obtengo la suma del precio + cantidad de los productos sin impuesto
				$prod_sin_impuesto += $product['iva'] != 0 ? 0 : @$product['cantidad']*$product['precio'];
				//obtengo la suma del precio + cantidad de los productos con impuesto
				$prod_con_impuesto += $product['iva'] == 0 ? 0 : @$product['cantidad']*$product['precio'];

				//Para obtener el impuesto resto el descuento de los productos con descuento con los productos con impuestos
				//luego los multiplico por el 0.13 que es el 13% del impuesto de venta
				$iva = ($prod_con_impuesto - $dcto_imp) * 0.13;
				//tp es el total de productos con y sin impuesto
				$tp = $prod_sin_impuesto + $prod_con_impuesto;
				//el total a pagar resto los dos descuentos al tp para luego sumarlo al iva
				$total = ($tp-($dcto+$dcto_imp))+$iva;
		}
		if($par==true) return array(
								'subtotal'=>$subtotal,
								'dcto'=>$descuento,
								'iva'=>$iva,
								'total'=>$total
								);
		else
		return  '
		<div class="resumen">
			 <h3>RESUMEN</h3><hr>
			 <h4>SUBTOTAL:<span style="float:right">'.number_format($subtotal,2).'</span></h4>
			 <h4>TOTAL DESCUENTO:<span style="float:right">'.number_format($descuento,2).'</span></h4>
			 <h4>TOTAL IV:<span style="float:right">'.number_format($iva,2).'</span></h4>
			 <h4>TOTAL A PAGAR:<span style="float:right">'.number_format($total,2).'</span></h4>
		</div>';
		}
	//--------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------
	//Session para actualiar proforma
		public static function getContenidoComprau() {
			if(is_string(Yii::$app->session->get('compra_profu'))){
				return JSON::decode(Yii::$app->session->get('compra_profu'), true);
			}
			else
				return Yii::$app->session->get('compra_profu');
		}

		public static function setContenidoComprau($compra_profu) {
			return Yii::$app->session->set('compra_profu', JSON::encode($compra_profu));
		}
		//------------------------------------------------------------------------------------
		public static function getCabezaFactura() {//es auxiliar
			return Yii::$app->session->get('cabpruxiliar');
		}
		public static function setCabezaFactura($cabpruxiliar) {
			return Yii::$app->session->set('cabpruxiliar', $cabpruxiliar);
		}
		//----------------------------------------------------------
		public static function getVendedoru() {
			return Yii::$app->session->get('vendedor_profu');
		}
		public static function setVendedoru($vendedor_profu) {
			return Yii::$app->session->set('vendedor_profu', $vendedor_profu);
		}
		//----------------------------------------------------------
		public static function getClienteu() {
			return Yii::$app->session->get('cliente_profu');
		}

		public static function setClienteu($cliente_profu) {
			return Yii::$app->session->set('cliente_profu', $cliente_profu);
		}
		//----------------------------------------------------------
		public static function getEstadoFacturau() {
			return Yii::$app->session->get('estado_profu');
		}

		public static function setEstadoFacturau($estado_profu) {
			return Yii::$app->session->set('estado_profu', $estado_profu);
		}
		//----------------------------------------------------------
		public static function getFechau() {
			return Yii::$app->session->get('fecha_profu');
		}

		public static function setFechau($fecha_profu) {
			return Yii::$app->session->set('fecha_profu', $fecha_profu);
		}
		//----------------------------------------------------------
		public static function getTotalu($par=false) {
			$subtotal=$total=$iva=$dcto=$dcto_imp=$descuento=0;
			$prod_con_impuesto = 0;
			$prod_sin_impuesto = 0;
			if(@$comprau=F_Proforma::getContenidoComprau())
			foreach($comprau as $product){
				//obtengo el subtotal
				$subtotal += @$product['cantidad']*@$product['precio'];

				//deparo con pre_descuento la forma de ontener el descuento dependiendo si es producto de base de datos o producto temporal
				//esto ocurre solo para update
				$pre_dcto = @$product['temporal']=='x' ? $product['dcto'] : $product['dcto']*@$product['cantidad'];
				//obtengo descuento de los productos que no tienen impuestos
				$dcto += $product['iva'] != 0 ? 0 : $pre_dcto;//
				//$dcto += $product['iva'] != 0 ? 0 : ((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//obtengo descuento de los productos que tienen impuestos
				$dcto_imp += $product['iva'] == 0 ? 0 : $pre_dcto;//((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//$dcto_imp += $product['iva'] == 0 ? 0 : ((@$product['precio']*@$product['dcto'])/100)*@$product['cantidad'];
				//suma de ambos descuento
				$descuento = $dcto + $dcto_imp;
				//obtengo la suma del precio + cantidad de los productos sin impuesto
				$prod_sin_impuesto += $product['iva'] != 0 ? 0 : @$product['cantidad']*$product['precio'];
				//obtengo la suma del precio + cantidad de los productos con impuesto
				$prod_con_impuesto += $product['iva'] == 0 ? 0 : @$product['cantidad']*$product['precio'];

				//Para obtener el impuesto resto el descuento de los productos con descuento con los productos con impuestos
				//luego los multiplico por el 0.13 que es el 13% del impuesto de venta
				$iva = ($prod_con_impuesto - $dcto_imp) * Yii::$app->params['impuesto'];
				//tp es el total de productos con y sin impuesto
				$tp = $prod_sin_impuesto + $prod_con_impuesto;
				//el total a pagar resto los dos descuentos al tp para luego sumarlo al iva
				$total = ($tp-($dcto+$dcto_imp))+$iva;

		}
		if($par==true) {
			return array(
				'subtotal'=>number_format(($subtotal),2),
				'dcto'=>number_format(($descuento),2),
				'iva'=>number_format(($iva),2),
				'total'=>$total
										);}
		else{
			$idCab = F_Proforma::getCabezaFactura(); //desde auxiliar del archivo Compra
			/*$subtotal_ = number_format($subtotal,2);
			$dcto_ = number_format($descuento,2);
			$iva_ = number_format($iva,2);
			$total_ = number_format($total,2);*/
			$sql = "UPDATE tbl_proforma SET subtotal = :subtotal, porc_descuento = :descuento, iva = :iva, total_a_pagar = :total WHERE idCabeza_Factura = :idCab";
		    $command = \Yii::$app->db->createCommand($sql);
		    $command->bindParam(":subtotal", $subtotal);
		    $command->bindParam(":descuento", $descuento);
		    $command->bindParam(":iva", $iva);
		    $command->bindParam(":total", $total);
		    $command->bindValue(":idCab", $idCab);
		    $command->execute();
				return  '
				<div class="resumen">
					 <h3>RESUMEN</h3><hr>
					 <h4>SUBTOTAL:<span style="float:right">'.number_format($subtotal,2).'</span></h4>
					 <h4>TOTAL DESCUENTO:<span style="float:right">'.number_format($descuento,2).'</span></h4>
					 <h4>TOTAL IV:<span style="float:right">'.number_format($iva,2).'</span></h4>
					 <h4>TOTAL A PAGAR:<span style="float:right">'.number_format($total,2).'</span></h4>
				</div>';

			}

		}

		//----------------------------------------------------------
			public static function getVendedornull() {
				return Yii::$app->session->get('venullp');
			}

			public static function setVendedornull($venullp) {
				return Yii::$app->session->set('venullp', $venullp);
			}

	}
