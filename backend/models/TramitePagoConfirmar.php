<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_tramite_pago".
 *
 * @property integer $idTramite_pago
 * @property integer $idProveedor
 * @property string $cantidad_monto_documento
 * @property string $monto_saldo_pago
 * @property string $porcentaje_descuento
 * @property string $monto_tramite_pagar
 * @property string $recibo_cancelacion
 * @property string $email_proveedor
 * @property string $prioridad_pago
 * @property string $usuario_registra
 * @property string $fecha_registra
 * @property string $usuario_cancela
 * @property string $fecha_cancela
 * @property string $usuario_aplica
 * @property string $fecha_aplica
 * @property string $detalle
 * @property string $estado_tramite
 */
class TramitePagoConfirmar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tramite_pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProveedor', 'monto_saldo_pago', 'monto_tramite_pagar', 'prioridad_pago'], 'required'],
            [['idProveedor'], 'integer'],
            [['cantidad_monto_documento', 'monto_saldo_pago', 'porcentaje_descuento', 'monto_tramite_pagar'], 'number'],
            [['fecha_registra', 'fecha_cancela', 'fecha_aplica'], 'safe'],
            [['recibo_cancelacion', 'email_proveedor', 'estado_tramite'], 'string', 'max' => 50],
            [['prioridad_pago'], 'string', 'max' => 10],
            [['usuario_registra', 'usuario_cancela', 'usuario_aplica'], 'string', 'max' => 20],
            [['detalle','detalle_reverso','detalle_anulacion','documento_banco','observaciones'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTramite_pago' => 'Id Trámite',
            'idProveedor' => 'Id Proveedor',
            'cantidad_monto_documento' => 'Monto',
            'monto_saldo_pago' => 'Monto Saldo Pago',
            'porcentaje_descuento' => 'Porcentaje Descuento',
            'monto_tramite_pagar' => 'Monto Tramite Pagar',
            'recibo_cancelacion' => 'Recibo Cancelacion',
            'email_proveedor' => 'Email Proveedor',
            'prioridad_pago' => 'Prioridad Pago',
            'usuario_registra' => 'Usuario Registra',
            'fecha_registra' => 'Fecha',
            'usuario_cancela' => 'Usuario Cancela',
            'fecha_cancela' => 'Fecha Cancela',
            'usuario_aplica' => 'Usuario Aplica',
            'fecha_aplica' => 'Fecha Aplica',
            'detalle' => 'Detalle',
            'estado_tramite' => 'Estado Tramite',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_proveedores.nombreEmpresa']);
    }

    public function afterFind(){
      $this->fecha_registra = date('d-m-Y ** H:i:s', strtotime( $this->fecha_registra));
      return parent::afterFind();
    }
}
