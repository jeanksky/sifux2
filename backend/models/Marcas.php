<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_marcas".
 *
 * @property string $codMarcas
 */
class Marcas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_marcas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_marca','slug'], 'required'],
            [['nombre_marca','slug'], 'unique'],
            [['nombre_marca','slug'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre_marca' => 'Nombre','slug'=>'Slug',
        ];
    }
}
