<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_agentes_comisiones".
 *
 * @property integer $idAgenteComision
 * @property string $cedula
 * @property string $nombre
 * @property string $telefono
 * @property string $direccion
 * @property double $porcentComision
 */
class AgentesComisiones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_agentes_comisiones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipoIdentidad','cedula', 'nombre', 'telefono', 'porcentComision','estadoAgenteComision'], 'required'],
            [['cedula'], 'unique'],
            [['porcentComision'], 'number'],
            [['cedula'], 'string', 'max' => 20],
            [['nombre'], 'string', 'max' => 80],
            [['telefono'], 'string', 'max' => 10],
            [['direccion'], 'string', 'max' => 130]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idAgenteComision' => 'Id Agente Comision',
            'tipoIdentidad'=> ' Tipo Identidad',
            'cedula' => 'Cedula',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'porcentComision' => 'Porcentaje Comision',
            'estadoAgenteComision' => 'estado',
        ];
    }
}
