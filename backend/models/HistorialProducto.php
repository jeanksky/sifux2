<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_historial_producto".
 *
 * @property integer $id_historial
 * @property string $codProdServicio
 * @property string $fecha
 * @property string $origen
 * @property integer $id_documento
 * @property integer $cant_ant
 * @property string $tipo
 * @property integer $cant_mov
 * @property integer $cant_new
 */
class HistorialProducto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_historial_producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProdServicio','fecha', 'origen', 'id_documento', 'cant_ant', 'tipo', 'cant_mov', 'cant_new', 'usuario'], 'safe'],
            [['id_documento', 'cant_ant', 'cant_mov', 'cant_new', 'usuario'], 'integer'],
            [['codProdServicio'], 'string', 'max' => 20],
            [['origen'], 'string', 'max' => 5],
            [['tipo'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_historial' => 'Id Historial',
            'codProdServicio' => 'Cod Prod Servicio',
            'fecha' => 'Fecha',
            'origen' => 'Origen',
            'id_documento' => 'Id Documento',
            'cant_ant' => 'Cant Ant',
            'tipo' => 'Tipo',
            'cant_mov' => 'Cant Mov',
            'cant_new' => 'Cant New',
        ];
    }
}
