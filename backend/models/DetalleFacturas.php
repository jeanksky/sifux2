<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_detalle_facturas".
 *
 * @property integer $idDetalleFactura
 * @property integer $idCabeza_factura
 * @property integer $codProdServicio
 * @property integer $cantidad
 * @property double $precio_unitario
 * @property double $precio_por_cantidad
 *
 * @property TblEncabezadoFactura $idCabezaFactura
 * @property TblProductoServicios $codProdServicio0
 */
class DetalleFacturas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_detalle_facturas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'idCabeza_factura','codProdServicio', 'cantidad', 'precio_unitario', 'precio_por_cantidad'*/ ], 'required'],
            [['idCabeza_factura', /*'codProdServicio',*/ ], 'integer'],
            [['precio_unitario', 'precio_por_cantidad','cantidad'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDetalleFactura' => 'Id Detalle Factura',
            'idCabeza_factura' => 'Id Cabeza Factura',
            'codProdServicio' => 'Cod Prod Servicio',
            'cantidad' => 'Cantidad',
            'precio_unitario' => 'Precio Unitario',
            'precio_por_cantidad' => 'Precio Por Cantidad',
            'descuento_producto' => 'DESC%',
            'subtotal_iva' => 'IV%',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCabezaFactura()
    {
        return $this->hasOne(TblEncabezadoFactura::className(), ['idCabeza_Factura' => 'idCabeza_factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodProdServicio0()
    {
        return $this->hasOne(TblProductoServicios::className(), ['codProdServicio' => 'codProdServicio']);
    }
}
