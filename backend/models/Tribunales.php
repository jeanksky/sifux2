<?php
namespace backend\models;

use common\models\Cn_db_tse;

class Tribunales extends Cn_db_tse
{
    public function __construct()
    {
        parent::__construct();
    }

    //estas funciones me obtienen las personas fisicas o juridicas
    public function obtener_nombre_juridico($id)
    {
      $sql = 'SELECT * FROM juridica WHERE id like "' . $id. '%"';
      $result = $this->_db->query($sql);
      $nombre_completo = $result->fetch_assoc();
      if (@$nombre_completo['nombre']) {
        return $nombre_completo['nombre'];
      } else{
        return 'Sin resultado';
      }
    }

    public function obtener_nombre_completo($id)
    {
        $sql = 'SELECT * FROM personas WHERE id = "' . $id. '"';
        $result = $this->_db->query($sql);
        $nombre_completo = $result->fetch_assoc();
        if (@$nombre_completo['nombre']) {
          return $nombre_completo['nombre'] . ' ' . $nombre_completo['apellido1'] . ' ' . $nombre_completo['apellido2'];
        } else{
          return 'Sin resultado';
        }

    }

    public function obtener_nombre_completo_array($id)
    {
        $sql = 'SELECT * FROM personas WHERE id = "' . $id. '"';
        $result = $this->_db->query($sql);
        $nombre_completo = $result->fetch_assoc();
        if (@$nombre_completo['nombre']) {
          return [$nombre_completo['nombre'] , $nombre_completo['apellido1'] , $nombre_completo['apellido2']];
        } else{
          return 'Sin resultado';
        }

    }

    //esta funcion me permite obtener la licencia mensual en la que está sujeto el sistema.
    public function obtener_genero($id)
    {
      $sql = 'SELECT sexo FROM personas WHERE id = "' . $id . '"';
      $result = $this->_db->query($sql);
      $sexo = $result->fetch_assoc();
      if ($sexo['sexo'] == '1') {
        return 'Masculino';
      }else {
        return 'Femenino';
      }
    }

    //obtener la lista de cantones
    public function obtener_cantones($id)
    {
      $sql = 'SELECT MAX(id_canton) AS id_canton, nombre_canton FROM lugares WHERE id_provincia = '.(int)$id.' GROUP BY nombre_canton ORDER BY id_canton';
      $result = $this->_db->query($sql);
      $cantones = $result->fetch_all(MYSQLI_ASSOC);
      return $cantones;
    }

    //obtener la lista de distritos
    public function obtener_distritos($provincia, $canton)
    {
      $sql = 'SELECT MAX(id_distrito) AS id_distrito, nombre_distrito FROM lugares WHERE id_provincia = '.(int)$provincia.' AND id_canton = '.(int)$canton.' GROUP BY nombre_distrito ORDER BY id_distrito';
      $result = $this->_db->query($sql);
      $distritos = $result->fetch_all(MYSQLI_ASSOC);
      return $distritos;
    }

    //obtener la lista de distritos
    public function obtener_barrios($provincia, $canton, $distrito)
    {
      $sql = 'SELECT MAX(id_barrio) AS id_barrio, nombre_barrio FROM lugares WHERE id_provincia = '.(int)$provincia.' AND id_canton = '.(int)$canton.' AND id_distrito = '.(int)$distrito.' GROUP BY nombre_barrio ORDER BY id_barrio';
      $result = $this->_db->query($sql);
      $barrios = $result->fetch_all(MYSQLI_ASSOC);
      return $barrios;
    }
}
