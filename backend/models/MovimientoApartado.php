<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_movi_adelanto".
 *
 * @property integer $idMov
 * @property integer $idCabeza_Factura
 * @property integer $idCliente
 * @property string $fecmov
 * @property string $concepto
 * @property string $monto_anterior
 * @property string $monto_movimiento
 * @property string $saldo_pendiente
 * @property string $usuario
 * @property integer $idMetPago
 */
class MovimientoApartado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_movi_apartado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCabeza_Factura', 'idCliente', 'idMetPago'], 'integer'],
            [['fecmov'], 'safe'],
            [['monto_anterior', 'monto_movimiento', 'saldo_pendiente'], 'number'],
            [['concepto'], 'string', 'max' => 200],
            [['usuario'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMov' => 'Id Mov',
            'idCabeza_Factura' => 'Id Cabeza  Factura',
            'idCliente' => 'Id Cliente',
            'fecmov' => 'Fecmov',
            'concepto' => 'Concepto',
            'monto_anterior' => 'Monto Anterior',
            'monto_movimiento' => 'Monto Movimiento',
            'saldo_pendiente' => 'Saldo Pendiente',
            'usuario' => 'Usuario',
            'idMetPago' => 'Id Met Pago',
        ];
    }
}
