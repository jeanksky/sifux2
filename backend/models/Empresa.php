<?php

namespace backend\models;
use yii\web\UploadedFile;
use yii\helpers\Url;
use backend\models\Empresa;
//use backend\models\Utils;
use Yii;

/**
 * This is the model class for table "tbl_empresa".
 *
 * @property integer $idEmpresa
 * @property string $nombre
 * @property string $direccion
 * @property string $localidad
 * @property string $estado
 * @property string $telefono
 * @property string $fax
 * @property string $email
 * @property string $sitioWeb
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_empresa';
    }

    /**
     * @inheritdoc
     */
    public $file;
    public $file_et;

    public function rules()
    {
        return [
            [['nombre', 'direccion', 'localidad', 'estado', 'telefono', /*'fax',*/ 'email', 'sitioWeb', 'ced_juridica', 'email_gerencia', 'email_proveeduria','email_contabilidad','email_notificaciones'], 'required'],
            [['nombre', 'sitioWeb'], 'string', 'max' => 50],
            [['direccion', 'localidad', 'logo'], 'string', 'max' => 130],
            [['estado', 'email', 'email_gerencia', 'email_proveeduria','email_contabilidad','email_notificaciones'], 'string', 'max' => 60],
            [['telefono', 'fax'], 'string', 'max' => 10],
            [['file', 'file_et', 'consecutivo_fe', 'consecutivo_te'], 'safe'],
            // [['file'], 'file', 'extensions'=>'jpg, gif, png'],
            // checks if "primaryImage" is a valid image with proper size
            [['file','file_et'], 'image', 'extensions'=>'jpg, gif, png',
                'checkExtensionByMimeType'=>false,
                // 'minWidth' => 100, 'maxWidth' => 1000,
                // 'minHeight' => 100, 'maxHeight' => 1000,
            ],

            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEmpresa' => 'Id Empresa',
            'nombre' => 'Nombre',
            'direccion' => 'Dirección',
            'localidad' => 'Localidad',
            'estado' => 'Estado',
            'telefono' => 'Teléfono',
            'fax' => 'Fax',
            'email' => 'Email general',
            'email_gerencia' => 'Email gerencia',
            'email_proveeduria' => 'Email proveeduría',
            'email_contabilidad' => 'Email contabilidad',
            'email_notificaciones'=>'Email notificaciones',
            'sitioWeb' => 'Sitio Web',
            'file' => 'Adjuntar imagen',
            'file_et' => 'Adjuntar imagen de etiqueta',
            'ced_juridica' => 'Cédula Jurídica',
        ];
    }

     public function getEmpresa()
        {
           // $fullname = new Funcionario();

           $sql = "SELECT nombre FROM tbl_empresa WHERE idEmpresa = :idE";
           $command = \Yii::$app->db->createCommand($sql);
           $command->bindValue(":idE",1);
           $result = $command->queryOne();

            return $result['nombre'];
        }

    public function getImageurl($tipo)
        {
        $sql = "SELECT logo FROM tbl_empresa WHERE idEmpresa = :idE";
        $command = \Yii::$app->db->createCommand($sql);
           $command->bindValue(":idE",1);
           $result = $command->queryOne();
           if ($tipo=='html') {
            return Url::base(true).'/'.$result['logo'];
          } else {
            return \Yii::$app->request->BaseUrl.'/'.$result['logo'];
          }
        }

        public function getFavicon()
        {

            return \Yii::$app->request->BaseUrl.'/uploads/favicon.ico';

        }

        public function getWCP()
        {
          return '/sifux2/backend/models/WebClientPrintController.php';
          $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
          $cod_empresa = $empresa->cod_empresa;
          if (Yii::$app->params['tipo_sistema'] == 'lutux') {
            return '/lu/'.$cod_empresa.'/backend/models/WebClientPrintController.php';//para cloud
          }else {
            return '/si/'.$cod_empresa.'/backend/models/WebClientPrintController.php';//para cloud
          }
        }

}
