<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;

	class Compra_view
	{
		public static function getContenidoFactura() {
			if(is_string(Yii::$app->session->get('compr'))){
				return JSON::decode(Yii::$app->session->get('compr'), true);
			}
			else
				return Yii::$app->session->get('compr');
		}

		public static function setContenidoFactura($compr) {
			return Yii::$app->session->set('compr', JSON::encode($compr));
		}
		//----------------------------------------------------------
		public static function getCabezaFactura() {//es auxiliar
			return Yii::$app->session->get('facturadia');
		}
		public static function setCabezaFactura($facturadia) {
			return Yii::$app->session->set('facturadia', $facturadia);
		}

		//----------------------------------------------------------
		public static function getTotalm($par=false) {
			$subtotal=$total=$iva=$dcto=0;
			if(@$comprau=Compra_view::getContenidoFactura())
			foreach($comprau as $product){
				$subtotal+=@$product['cantidad']*@$product['precio'];
				$dcto+=(@$product['dcto'])*@$product['cantidad'];

				$temporal = $subtotal-$dcto;
				$subtotal_ = 0;
				$temporal_ = 0;

				if ($product['iva']!=0.00) {
					$subtotal_+=@$product['cantidad']*@$product['precio'];

					$iva += (float)($subtotal_*(@$product['iva']/100));
				}
				//Resultado del total a pagar
				$total = ($temporal)+$iva;

		}
		if($par==true) {
			return array(
				'subtotal'=>number_format(($subtotal),2),
				'dcto'=>number_format(($dcto),2),
				'iva'=>number_format(($iva),2),
				'total'=>$total
										);}
		else{
			return  '  <div class="resumen">

			   <h3 align="right">RESUMEN</h3>
			   <table class="" align="right">
			    <tbody>
			        <tr>
			             <td class="detail-view">
			                <table>
			                	<tr>
			                        <th align="left">SUBTOTAL: </th>
			                        <td align="right">'.number_format($subtotal,2).'</td>
			                    </tr>
							    <tr>
			                        <th align="left">TOTAL DESCUENTO: &nbsp;</th>
			                        <td align="right">'.number_format($dcto,2).'</td>
			                    </tr>
			                    <tr>
			                        <th align="left">TOTAL IV: </th>
			                        <td align="right"><span>'.number_format($iva,2).'</span></td>
			                    </tr>
								 <tr>
			                        <th align="left">TOTAL A PAGAR: </th>
			                        <td align="right"><span><strong>'.number_format($total,2).'</strong></span></td>
			                    </tr>
			                </table>
			            </td>
			        </tr>
			    </tbody>
				</table>
			    </div>';

			}

		}
		//almaceno parcialmente en session los medios de pago que se estan procesando en caja
				public static function getContenidoMedioPago() {
					if(is_string(Yii::$app->session->get('mp_caja'))){
						return JSON::decode(Yii::$app->session->get('mp_caja'), true);
					}
					else {
						return Yii::$app->session->get('mp_caja');
					}
				}

				public static function setContenidoMedioPago($mp_caja) {
					return Yii::$app->session->set('mp_caja', JSON::encode($mp_caja));
				}
	}
