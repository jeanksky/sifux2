<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_medio_pago".
 *
 * @property integer $idMetPago
 * @property integer $idCabeza_factura
 * @property integer $idForma_Pago
 * @property string $tipo_del_medio
 * @property string $comprobacion
 * @property string $cuenta_deposito
 * @property string $estado
 * @property string $fecha_deposito
 *
 * @property TblEncabezadoFactura $idCabezaFactura
 */
class MedioPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_medio_pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCabeza_factura'], 'required'],
            [['idCabeza_factura', 'idForma_Pago'], 'integer'],
            [['fecha_deposito', 'monto'], 'safe'],
            [['tipo_del_medio', 'comprobacion'], 'string', 'max' => 80],
            [['cuenta_deposito'], 'string', 'max' => 100],
            [['estado'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMetPago' => 'Id Met Pago',
            'idCabeza_factura' => 'Id Cabeza Factura',
            'idForma_Pago' => 'Id Forma  Pago',
            'tipo_del_medio' => 'Tipo Del Medio',
            'comprobacion' => 'Comprobacion',
            'cuenta_deposito' => 'Cuenta Deposito',
            'estado' => 'Estado',
            'monto' => 'Monto',
            'fecha_deposito' => 'Fecha Deposito',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCabezaFactura()
    {
        return $this->hasOne(TblEncabezadoFactura::className(), ['idCabeza_Factura' => 'idCabeza_factura']);
    }
}
