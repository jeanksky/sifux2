<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_cuentas_bancarias".
 *
 * @property string $idBancos
 * @property integer $idTipo_moneda
 * @property integer $idEntidad_financiera
 * @property string $numero_cuenta
 * @property string $descripcion
 * @property string $tipo_cuenta
 * @property string $cuenta_cliente
 * @property string $saldo_libros
 * @property string $saldo_bancos
 */
class CuentasBancarias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cuentas_bancarias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTipo_moneda', 'idEntidad_financiera', 'numero_cuenta', 'descripcion', 'tipo_cuenta', 'cuenta_cliente', 'saldo_libros','saldo_bancos'], 'required'],
            [['idTipo_moneda', 'idEntidad_financiera'], 'integer'],
            [['saldo_libros', 'saldo_bancos'], 'number'],
            [['numero_cuenta'], 'string', 'max' => 40],
            [['descripcion'], 'string', 'max' => 30],
            [['tipo_cuenta'], 'string', 'max' => 10],
            [['cuenta_cliente'], 'string', 'max' => 17, 'min' => 17]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipo_moneda' => 'Tipo Moneda',
            'idEntidad_financiera' => 'Entidad Financiera',
            'numero_cuenta' => 'Número Cuenta',
            'descripcion' => 'Descripción',
            'tipo_cuenta' => 'Tipo Cuenta',
            'cuenta_cliente' => 'Cuenta Cliente',
            'saldo_libros' => 'Saldo Libros',
            'saldo_bancos' => 'Saldo Bancos',
        ];
    }

    //atributos ajenos para enlaces con el módulo local
    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_moneda.descripcion', 'tbl_entidades_financieras.descripcion']);
    }
}
