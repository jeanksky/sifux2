<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_marcas_producto".
 *
 * @property integer $id_marcas
 * @property string $nombre_marca
 * @property string $estado
 */
class MarcasProducto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_marcas_producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_marca'], 'unique'],
            [['nombre_marca','estado'], 'required'],
            [['nombre_marca'], 'string', 'max' => 1000],
            [['estado'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_marcas' => 'Id Marcas',
            'nombre_marca' => 'Nombre Marca',
            'estado' => 'Estado',
        ];
    }
}
