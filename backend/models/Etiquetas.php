<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_etiquetas".
 *
 * @property string $codigo
 * @property string $nombre
 * @property string $fecha_reg
 * @property string $ubi
 * @property double $precio
 * @property integer $cantidad
 * @property string $estado
 */
class Etiquetas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_etiquetas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_reg'], 'safe'],
            [['precio'], 'number'],
            [['cantidad'], 'integer'],
            [['codigo'], 'string', 'max' => 100],
            [['ubi'], 'string', 'max' => 40],
            [['nombre'], 'string', 'max' => 500],
            [['estado'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'fecha_reg' => 'Fecha Reg',
            'ubi' => 'Ubi',
            'precio' => 'Precio',
            'cantidad' => 'Cantidad',
            'estado' => 'Estado',
        ];
    }

    public function afterFind()
    {

        $this->fecha_reg = date('d-m-Y', strtotime( $this->fecha_reg));
        if ($this->fecha_reg!='') {
            $this->fecha_reg = date('d-m-Y', strtotime( $this->fecha_reg));
        }

        return parent::afterFind();

    }
}
