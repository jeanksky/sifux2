<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_clientes_ot".
 *
 * @property integer $idOT
 * @property integer $idCliente
 * @property integer $dias_ot
 * @property string $montoTotalEjecutado_ot
 * @property string $estado_ot
 * @property string $autorizacion_ot
 */
class Ot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_clientes_ot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCliente', 'dias_ot'], 'integer'],
            [['montoTotalEjecutado_ot'], 'number'],
            [['estado_ot'], 'string', 'max' => 10],
            [['autorizacion_ot'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idOT' => 'Id Ot',
            'idCliente' => 'Id Cliente',
            'dias_ot' => 'Dias Ot',
            'montoTotalEjecutado_ot' => 'Monto Total Ejecutado Ot',
            'estado_ot' => 'Estado Ot',
            'autorizacion_ot' => 'Autorizacion Ot vencidos',
        ];
    }
}
