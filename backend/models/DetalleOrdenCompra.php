<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_detalle_orden_compra".
 *
 * @property integer $idDetaOrdCom
 * @property integer $idOrdenCompra
 * @property string $codProdServicio
 * @property integer $idProd_prov
 * @property integer $cantidad_sugerida
 * @property integer $cantidad_pedir
 */
class DetalleOrdenCompra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_detalle_orden_compra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrdenCompra', 'idProd_prov', 'cantidad_sugerida', 'cantidad_pedir'], 'integer'],
            [['codProdServicio'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDetaOrdCom' => 'Id Deta Ord Com',
            'idOrdenCompra' => 'Id Orden Compra',
            'codProdServicio' => 'Cod Prod Servicio',
            'idProd_prov' => 'Id Prod Prov',
            'cantidad_sugerida' => 'Cantidad Sugerida',
            'cantidad_pedir' => 'Cantidad Pedir',
        ];
    }
}
