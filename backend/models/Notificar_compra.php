<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Notificar_compra extends Model
{
  public $archivo;
  public $resolucion;
  public $detalle;
  public $tipo_documento;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['archivo','tipo_documento','resolucion','detalle'], 'safe'],
            [['archivo', 'tipo_documento', 'resolucion', 'detalle'], 'required'],
            [['detalle'], 'string', 'max' => 200]
        ];
    }
}
