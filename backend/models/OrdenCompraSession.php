<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;

class OrdenCompraSession
{
	public static function getIDproveedor() {
		return Yii::$app->session->get('OCP_idproveedor');
	}
	public static function setIDproveedor($OCP_idproveedor) {
		return Yii::$app->session->set('OCP_idproveedor', $OCP_idproveedor);
	}
	//----------------------------------------------------------
	public static function getProveedor() {
		return Yii::$app->session->get('OCP_proveedor');
	}
	public static function setProveedor($OCP_proveedor) {
		return Yii::$app->session->set('OCP_proveedor', $OCP_proveedor);
	}
	//----------------------------------------------------------
	public static function getDiasrendimiento() {
		return Yii::$app->session->get('dias_rendimiento');
	}
	public static function setDiasrendimiento($dias_rendimiento) {
		return Yii::$app->session->set('dias_rendimiento', $dias_rendimiento);
	}
	//----------------------------------------------------------
	public static function getFechasrendimiento() {
		return Yii::$app->session->get('fechas_rendimiento');
	}
	public static function setFechasrendimiento($fechas_rendimiento) {
		return Yii::$app->session->set('fechas_rendimiento', $fechas_rendimiento);
	}
	//----------------------------------------------------------
}
