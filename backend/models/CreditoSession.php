<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;

class CreditoSession 
{	
	//-------------------------------------------------------------------------------------------------------
	//--------------------------------- CANCELACIÓN ---------------------------------------------------
	//-------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------
		public static function getIDcliente() {
			return Yii::$app->session->get('cred_idcliente');
		}
		public static function setIDcliente($cred_idcliente) {
			return Yii::$app->session->set('cred_idcliente', $cred_idcliente);
		}
	//----------------------------------------------------------
		public static function getCliente() {
			return Yii::$app->session->get('cred_cliente');
		}
		public static function setCliente($cred_cliente) {
			return Yii::$app->session->set('cred_cliente', $cred_cliente);
		}
	//----------------------------------------------------------
		public static function getAprobado() {
			return Yii::$app->session->get('cred_aprobado');
		}
		public static function setAprobado($cred_aprobado) {
			return Yii::$app->session->set('cred_aprobado', $cred_aprobado);
		}
	//----------------------------------------------------------
		public static function getEjecutado() {
			return Yii::$app->session->get('cred_ejecutado');
		}
		public static function setEjecutado($cred_ejecutado) {
			return Yii::$app->session->set('cred_ejecutado', $cred_ejecutado);
		}
	//----------------------------------------------------------
		public static function getDisponible() {
			return Yii::$app->session->get('cred_disponible');
		}
		public static function setDisponible($cred_disponible) {
			return Yii::$app->session->set('cred_disponible', $cred_disponible);
		}


//-------------MODULO CANCELACION---------------------------------------------------------
//----------------------------------------------------------------------------------------
		public static function getFechafactura(){
			return Yii::$app->session->get('fe_factur_cred');
		}
		public static function setFechafactura($fe_factur_cred) {
			return Yii::$app->session->set('fe_factur_cred', $fe_factur_cred);
		}
	//-----------------------------------------------------------------------------------
		public static function getFactura(){
			return Yii::$app->session->get('factur_cred');
		}
		public static function setFactura($factur_cred) {
			return Yii::$app->session->set('factur_cred', $factur_cred);
		}
	//-----------------------------------------------------------------------------------
		
		public static function getMontofactura() {
			return Yii::$app->session->get('saldo_fc');
		}
		public static function setMontofactura($saldo_fc) {
			return Yii::$app->session->set('saldo_fc', $saldo_fc);
		}

	//-----------------Notas de credito pendientes-----------------------------------------	
		public static function getNotasCreditoPendientes() {
			if(is_string(Yii::$app->session->get('creditopendiente'))){
				return JSON::decode(Yii::$app->session->get('creditopendiente'), true);
			}
			else
				return Yii::$app->session->get('creditopendiente');
		}

		public static function setNotasCreditoPendientes($creditopendiente) {
			return Yii::$app->session->set('creditopendiente', JSON::encode($creditopendiente));
		}

	//-----------------Notas de credito para varias facturas-----------------------------------------	
		public static function getNotasCreditoVariasFacturas() {
			if(is_string(Yii::$app->session->get('creditopendiente_varios'))){
				return JSON::decode(Yii::$app->session->get('creditopendiente_varios'), true);
			}
			else
				return Yii::$app->session->get('creditopendiente_varios');
		}

		public static function setNotasCreditoVariasFacturas($creditopendiente_varios) {
			return Yii::$app->session->set('creditopendiente_varios', JSON::encode($creditopendiente_varios));
		}

	//---Monto de notas de credito aplicadas a un recibo para aplicar a varias facturas facturas-----
		public static function getMontoNC() {
			return Yii::$app->session->get('monto_nc');
		}
		public static function setMontoNC($monto_nc) {
			return Yii::$app->session->set('monto_nc', $monto_nc);
		}

	//------------------------------REFERENCIAS-------------------------------------
		public static function getReferencia() {
			return Yii::$app->session->get('referencia');
		}
		public static function setReferencia($referencia) {
			return Yii::$app->session->set('referencia', $referencia);
		}

	//-------------------------------------------------------------------------------------------------------
	//--------------------------------- RECIBOS GENERADOS ---------------------------------------------------
	//-------------------------------------------------------------------------------------------------------

		public static function getRecibo(){
			return Yii::$app->session->get('recibo');
		}
		public static function setRecibo($recibo) {
			return Yii::$app->session->set('recibo', $recibo);
		}
	//-----------------------------------------------------------------------------------
		public static function getIdrecibobuscar(){
			return Yii::$app->session->get('recibobusca');
		}
		public static function setIdrecibobuscar($recibobusca) {
			return Yii::$app->session->set('recibobusca', $recibobusca);
		}
	//-----------------------------------------------------------------------------------
		public static function getBanderacargar(){
			return Yii::$app->session->get('banderacarg');
		}
		public static function setBanderacargar($banderacarg) {
			return Yii::$app->session->set('banderacarg', $banderacarg);
		}

	//-------------------------------------------------------------------------------------------------------
	//--------------------------------- FACTURAS CANCELADAS -------------------------------------------------
	//-------------------------------------------------------------------------------------------------------
		public static function getFechadesde(){
			return Yii::$app->session->get('fedes');
		}
		public static function setFechadesde($fedes) {
			return Yii::$app->session->set('fedes', $fedes);
		}
	//-----------------------------------------------------------------------------------
		public static function getFechaasta(){
			return Yii::$app->session->get('feast');
		}
		public static function setFechaasta($feast) {
			return Yii::$app->session->set('feast', $feast);
		}
	//-----------------------------------------------------------------------------------
		public static function getFacturaCancelada(){//RECIBOS DE FACTURAS CANCELADAS
			return Yii::$app->session->get('facturacancelada');
		}
		public static function setFacturaCancelada($facturacancelada) {
			return Yii::$app->session->set('facturacancelada', $facturacancelada);
		}

}