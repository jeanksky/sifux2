<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;
//use backend\models\Compra;//Prueba

	class ListaFacturaVehicular
	{
		public static function getContenidoFacturaVe() {
			if(is_string(Yii::$app->session->get('factve'))){
				return JSON::decode(Yii::$app->session->get('factve'), true);
			}
			else
				return Yii::$app->session->get('factve');
		}

		public static function setContenidoFacturaVe($factve) {
			return Yii::$app->session->set('factve', JSON::encode($factve));
		}
		
		//----------------------------------------------------------//Estas 2 funciones son unicas para obtener el total desde el index de cabeza-caja
		/*public static function getTotalauxiliar() {
			return Yii::$app->session->get('totalu');
		}

		public static function setTotalauxiliar($totalu) {
			return Yii::$app->session->set('totalu', $totalu);
		}
		*/
	
	}
