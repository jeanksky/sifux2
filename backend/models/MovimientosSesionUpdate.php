<?php
namespace backend\models;

use Yii;
use yii\web\Session;
use yii\helpers\Json;


	class MovimientosSesionUpdate 
	{

		public static function getContenidoMovimiento() {
			if(is_string(Yii::$app->session->get('movimientou'))){
				return JSON::decode(Yii::$app->session->get('movimientou'), true);
			}
			else
				return Yii::$app->session->get('movimientou');
		}

		public static function setContenidoMovimiento($movimientou) {
			return Yii::$app->session->set('movimientou', JSON::encode($movimientou));
		}
		//----------------------------------------------------------		
		
		public static function getFecha() {
			return Yii::$app->session->get('fechamu');
		}
		public static function setFecha($fechamu) {
			return Yii::$app->session->set('fechamu', $fechamu);
		}
		//----------------------------------------------------------
		public static function getTipo() {
			return Yii::$app->session->get('tipou');
		}
		public static function setTipo($tipou) {
			return Yii::$app->session->set('tipou', $tipou);
		}
		//----------------------------------------------------------
		public static function getReferencia() {
			return Yii::$app->session->get('referenciau');
		}

		public static function setReferencia($referenciau) {
			return Yii::$app->session->set('referenciau', $referenciau);
		}
		//----------------------------------------------------------
		public static function getObservaciones() {
			return Yii::$app->session->get('observacionesu');
		}

		public static function setObservaciones($observacionesu) {
			return Yii::$app->session->set('observacionesu', $observacionesu);
		}
		//----------------------------------------------------------
		public static function getEstado() {
			return Yii::$app->session->get('estadou');
		}

		public static function setEstado($estadou) {
			return Yii::$app->session->set('estadou', $estadou);
		}
		//----------------------------------------------------------
		public static function getUsuario() {
			return Yii::$app->session->get('usuariou');
		}

		public static function setUsuario($usuariou) {
			return Yii::$app->session->set('usuariou', $usuariou);
		}
		//----------------------------------------------------------
		public static function getId_movimiento() {
			return Yii::$app->session->get('idm');
		}

		public static function setId_movimiento($idm) {
			return Yii::$app->session->set('idm', $idm);
		}

		}