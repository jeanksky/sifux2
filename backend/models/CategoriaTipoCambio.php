<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_categoria_tipo_cambio".
 *
 * @property integer $idCategoria
 * @property string $categoria
 * @property integer $idTipo_moneda
 * @property integer $idEntidad_financiera
 */
class CategoriaTipoCambio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_categoria_tipo_cambio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria', 'idTipo_moneda', 'idEntidad_financiera'], 'required'],
            [['idTipo_moneda', 'idEntidad_financiera'], 'integer'],
            [['categoria'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCategoria' => 'Id Categoria',
            'categoria' => 'Categoria',
            'idTipo_moneda' => 'Id Tipo Moneda',
            'idEntidad_financiera' => 'Id Entidad Financiera',
        ];
    }
}
