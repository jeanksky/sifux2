<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_clientes_apartados".
 *
 * @property integer $idApartado
 * @property integer $idCliente
 * @property integer $dias_apartado
 * @property string $estado
 */
class ClientesApartados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_clientes_apartados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCliente', 'dias_apartado'], 'integer'],
            [['montoMaximoApartado'], 'number'],
            [['estado'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idApartado' => 'Id Apartado',
            'idCliente' => 'Id Cliente',
            'dias_apartado' => 'Dias de Apartado',
            'estado' => 'Permiso de apartado',
            'montoMaximoApartado' => 'Mónto máximo de apartado'
        ];
    }
}
