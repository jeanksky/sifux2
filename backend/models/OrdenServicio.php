<?php

namespace backend\models;

use Yii;

use backend\models\DetalleOrdServicio;
use backend\models\Servicios;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_orden_servicio".
 *
 * @property integer $idOrdenServicio
 * @property string $fecha
 * @property integer $montoServicio
 * @property integer $idCliente
 * @property integer $placa
 * @property integer $idMecanico
 * @property string $radio
 * @property string $alfombra
 * @property string $herramienta
 * @property string $repuesto
 * @property integer $cantOdometro
 * @property string $cantCombustible
 * @property string $observaciones
 *
 * @property TblDetalleOrdServicio[] $tblDetalleOrdServicios
 * @property TblEncabezadoFactura[] $tblEncabezadoFacturas
 * @property TblClientes $idCliente0
 * @property TblMecanicos $idMecanico0
 * @property TblVehiculos $placa0
 */
class OrdenServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $famili; //Declaro una sentencia para almacenar las familias
    public $servi; //Declaro una sentencia para almacenar los servicios de la familia

    public static function tableName()
    {
        return 'tbl_orden_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['famili', 'safe'], //Para traer las familias
            ['servi', 'safe'], //Para traer las familias
           // ['res', 'safe'], //Para traer las familias
            [['fecha'], 'safe'],
            [[/*'montoServicio',*/ 'idCliente', 'idMecanico', 'cantOdometro', 'estado'], 'integer'],
            [['montoServicio','famili','servi'/*,'idCliente'*/, 'placa', 'idMecanico', 'radio', 'alfombra', 'herramienta', 'repuesto', 'cantOdometro', 'cantCombustible', /*'observaciones', 'observacionesServicio'*/], 'required'],
            [['radio', 'alfombra', 'herramienta', 'repuesto'], 'string', 'max' => 2],
            [['cantCombustible'], 'string', 'max' => 6],
            [['placa'], 'string', 'max' => 10],
            [['observaciones', 'observacionesServicio'], 'string', 'max' => 150],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idOrdenServicio' => 'Orden de Servicio',
            'fecha' => 'Fecha',
            'montoServicio' => 'Monto',
            'idCliente' => 'Propietario',
            'placa' => 'Placa',
            // 'anio' => 'Año',
            // 'modelo' => 'Modelo',
            // 'marca' => 'Marca',
            'idMecanico' => 'Mecánico',
            // 'telefono' => 'Teléfono',
            // 'email' => 'E-mail',
            'radio' => 'Radio',
            'alfombra' => 'Alfombra',
            'herramienta' => 'Herramienta',
            'repuesto' => 'Repuesto',
            'cantOdometro' => 'Cant Odometro',
            'cantCombustible' => 'Cant Combustible',
            'observaciones' => 'Observaciones',
            'observacionesServicio' => 'Observaciones de Servicio',
            'famili' => 'Familia',
            'servi' => 'Servicio',

        ];
    }

    public function afterSave($insert, $changedAttributes){

    \Yii::$app->db->createCommand()->delete('tbl_detalle_ord_servicio', 'idOrdenServicio = '.(int) $this->idOrdenServicio)->execute();
    /*Lo que estamos haciendo, es borrar todos los registros de la tabla tbl_detalle_ord_servicio (conocida como tabla pivot)
    que tengan como idOrdenServicio la id que corresponde al modelo siendo guardado.*/
    /*Es decir, con cada modificación, se borrarán todos los registros de operaciones asignadas a ese rol,
    y serán insertados los nuevos registros correspondientes.*/
    foreach ($this->servi as $id) {
        $ro = new DetalleOrdServicio();
        $ro->idOrdenServicio = $this->idOrdenServicio;
        $idF = Servicios::find()->where(['codProdServicio'=>$id])->one();
        $ro->familia = $idF->codFamilia;
        $ro->producto = $id;
        $ro->save();
        }


        //no almacenar duplicados foreach
        //http://www.yiiframeworkenespanol.org/foro/showthread.php?tid=736
        //http://es.wpcures.com/10216/eliminar-duplicados-en-un-bucle-foreach-closed
        //http://www.forosdelweb.com/f18/duplicado-no-deseado-foreach-adentro-foreach-arrays-952956/

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblDetalleOrdServicios()
    {                                                          //id local             id detalleordenservicio
        return $this->hasMany(DetalleOrdServicio::className(), ['idOrdenServicio' => 'idOrdenServicio'])
        ->viaTable('tbl_orden_servicio', ['idOrdenServicio' => 'idOrdenServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblEncabezadoFacturas()
    {
        return $this->hasMany(EncabezadoFactura::className(), ['idOrdenServicio' => 'idOrdenServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente0()
    {
        return $this->hasOne(Clientes::className(), ['idCliente' => 'idCliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMecanico0()
    {
        return $this->hasOne(Mecanicos::className(), ['idMecanico' => 'idMecanico']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaca0()
    {
        return $this->hasOne(Vehiculos::className(), ['placa' => 'placa']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
          return $this->fecha = date('Y-m-d', strtotime( $this->fecha));

        } else {
            return  $this->fecha = '0000-00-00';

        }
    }

    public function afterFind()
      {
      $this->fecha = date('d-m-Y', strtotime( $this->fecha));
       parent::afterFind();
      }

    public function hola()
    {
      return 4;
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_clientes.nombreCompleto']);
    }

}
