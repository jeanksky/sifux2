<?php

namespace backend\models;
use backend\models\CuentasBancarias;

use Yii;

/**
 * This is the model class for table "tbl_movimiento_libros".
 *
 * @property integer $idMovimiento_libro
 * @property integer $idCuenta_bancaria
 * @property integer $idTipoMoneda
 * @property integer $idTipodocumento
 * @property integer $numero_documento
 * @property string $fecha_documento
 * @property string $monto
 * @property string $tasa_cambio
 * @property string $monto_moneda_local
 * @property string $beneficiario
 * @property string $detalle
 * @property string $fecha_registro
 * @property string $usuario_registra
 */
class MovimientoLibros extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_movimiento_libros';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCuenta_bancaria', 'idTipoMoneda', 'beneficiario','idTipodocumento', 'numero_documento', 'fecha_documento', 'detalle', 'monto', 'fecha_registro', 'usuario_registra'], 'required'],
            //[['numero_documento'], 'unique', 'targetAttribute' => ['numero_documento', 'idCuenta_bancaria' ], 'message'=>'Este {attribute} ya ha sido registrado con este número de cuenta.'],
            //['numero_documento', 'unico_documento_banco'],
            [['idCuenta_bancaria', 'idTipoMoneda', 'idTipodocumento', 'numero_documento'], 'integer'],
            [['fecha_documento', 'fecha_registro'], 'safe'],
            ['monto',function ($attribute, $params) {
              if($this->monto == 0){
                  $this->addError('monto','Monto tiene que ser mayor a 0');
              }
}],
            [['monto', 'tasa_cambio', 'monto_moneda_local'], 'number'],
            [['beneficiario', 'usuario_registra'], 'string', 'max' => 50],
            [['detalle'], 'string', 'max' => 150]
        ];
    }

    public function checkIsArray(){
         if($this->monto == 0){
             $this->addError('monto','Monto tiene que ser mayor a 0');
         }
    }

    public function unico_documento_banco($attribute,$params){
          $cuenta_escrita = CuentasBancarias::find()->where(['idBancos'=>$this->idCuenta_bancaria])->one();
          $movimientos = $this::find()->all();
          foreach ($movimientos as $key => $movimiento) {
            $cuenta_consultada = CuentasBancarias::find()->where(['idBancos'=>$movimiento['idCuenta_bancaria']])->one();
            if ($movimiento['numero_documento'] == $this->numero_documento && $cuenta_consultada->idEntidad_financiera == $cuenta_escrita->idEntidad_financiera)
            {
              $this->addError('numero_documento', "Repetido para este banco");
            }
          }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMovimiento_libro' => 'Id Movimiento Libro',
            'idCuenta_bancaria' => 'Cuenta Bancaria',//ID
            'idTipoMoneda' => 'Tipo Moneda',
            'idTipodocumento' => 'Tipo Documento',
            'numero_documento' => 'Número Documento',
            'fecha_documento' => 'Fecha Documento',
            'monto' => 'Monto',
            'tasa_cambio' => 'Tasa de cambio',
            'monto_moneda_local' => 'Monto en moneda local',
            'beneficiario' => 'Beneficiario',
            'detalle' => 'Detalle del movimiento',
            'fecha_registro' => 'Fecha de registro',
            'usuario_registra' => 'Usuario que Registra',
        ];
    }

    public function beforeSave($insert)
    {
            $this->attributes = array_map('strtoupper',$this->attributes);
            $this->fecha_registro = date('Y-m-d', strtotime( $this->fecha_registro));
            $this->fecha_documento = date('Y-m-d', strtotime( $this->fecha_documento));
            if ($this->fecha_documento == '1970-01-01') {
                 $this->fechaVencimiento = '';
             }
            return parent::beforeSave($insert);
    }

   /* public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_proveedores.nombreEmpresa']);
    }*/


    public function afterFind()
    {
          $this->fecha_registro = date('d-m-Y', strtotime( $this->fecha_registro));
          $this->fecha_documento = date('d-m-Y', strtotime( $this->fecha_documento));
          if ($this->fecha_documento == '01-01-1970') {
               $this->fecha_documento = '';
           }
          return parent::afterFind();

    }

    //atributos ajenos para enlaces con el módulo local
    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_moneda.descripcion', 'tbl_cuentas_bancarias.numero_cuenta', 'tbl_tipo_documento.descripcion']);
    }
}
