<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_detalle_ord_servicio".
 *
 * @property integer $idDetaOrden
 * @property integer $idOrdenServicio
 * @property string $familia
 * @property string $producto
 *
 * @property TblOrdenServicio $idOrdenServicio0
 */
class DetalleOrdServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_detalle_ord_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrdenServicio', /*'familia', 'producto'*/], 'required'],
            [['idOrdenServicio'], 'integer'],
            [['familia', 'producto'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //'idDetaOrden' => 'Id Deta Orden',
            'idOrdenServicio' => 'Id Orden Servicio',
            'familia' => 'Familia',
            'producto' => 'Producto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrdenServicio0()
    {
        return $this->hasOne(OrdenServicio::className(), ['idOrdenServicio' => 'idOrdenServicio']);
    }
}
