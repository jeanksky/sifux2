<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ProductoServicios;

/**
 * ProductoServiciosSearch represents the model behind the search form about `backend\models\ProductoServicios`.
 */
class ProductoServiciosSearch extends ProductoServicios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProdServicio'], 'string'],
            [['numFacturaProv', 'cantidadInventario', 'cantidadMinima', 'codFamilia', 'porcentUnidad'], 'integer'],
            [['tipo', 'nombreProductoServicio', 'exlmpuesto','anularDescuento','ubicacion'], 'safe'],
            [['precioCompra', 'precioVenta', 'precioVentaImpuesto',  'precioMinServicio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductoServicios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codProdServicio' => $this->codProdServicio,
            'numFacturaProv' => $this->numFacturaProv,
            'cantidadInventario' => $this->cantidadInventario,
            'cantidadMinima' => $this->cantidadMinima,
            'codFamilia' => $this->codFamilia,
            'precioCompra' => $this->precioCompra,
            'porcentUnidad' => $this->porcentUnidad,
            'precioVenta' => $this->precioVenta,
            'precioVentaImpuesto' => $this->precioVentaImpuesto,
            'anularDescuento' => $this->anularDescuento,
            'precioMinServicio' => $this->precioMinServicio,
            'ubicacion' => $this->ubicacion,
        ]);

        $query->andFilterWhere(['like', 'tipo', $this->tipo='Producto'])
            ->andFilterWhere(['like', 'nombreProductoServicio', $this->nombreProductoServicio])
            ->andFilterWhere(['like', 'exlmpuesto', $this->exlmpuesto]);

        return $dataProvider;
    }
}
