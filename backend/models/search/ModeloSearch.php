<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Modelo;

/**
 * ModeloSearch represents the model behind the search form about `backend\models\Modelo`.
 */
class ModeloSearch extends Modelo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_modelo', 'slug'], 'safe'],
            [['tbl_marcas.nombre_marca'], 'safe'], //para traer datos de la tabla tbl_marcas
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Modelo::find();

        //para comunicar tbl_modelo con tbl_marcas
        $query->leftJoin([
        'tbl_marcas'
      ], 'tbl_marcas.codMarca = tbl_modelo.codMarca');
        //------------------------------------------------

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //para comunicar tbl_vehiculos con tbl_clientes
        $dataProvider->setSort([
            'attributes'=>[
                'codModelo',
                'codMarca',
                'nombre_modelo',
                'slug',
                'tbl_marcas.nombre_marca'
            ]
        ]); //-----------------------------------------

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'nombre_modelo', $this->nombre_modelo])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'tbl_marcas.nombre_marca', $this->getAttribute('tbl_marcas.nombre_marca')]); //para busquedas de la tabla tbl_marcas;

        return $dataProvider;
    }
}
