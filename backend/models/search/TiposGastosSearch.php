<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TiposGastos;

/**
 * TiposGastosSearch represents the model behind the search form about `backend\models\TiposGastos`.
 */
class TiposGastosSearch extends TiposGastos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTipoGastos', 'idCategoriaGasto'], 'integer'],
            [['descripcionTipoGastos', 'estadoTipoGastos'], 'safe'],
            [['tbl_categoria_gastos.descripcionCategoriaGastos'], 'safe'], //para traer datos de la tabla tbl_categoria_gastos
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TiposGastos::find();

    //para comunicar tbl_tipos_gastos con tbl_categoria_gastos
        $query->leftJoin([
        'tbl_categoria_gastos'
      ], 'tbl_categoria_gastos.idCategoriaGastos = tbl_tipos_gastos.idCategoriaGasto');
        //------------------------------------------------

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idTipoGastos' => $this->idTipoGastos,
            'idCategoriaGasto' => $this->idCategoriaGasto,
        ]);

        $query->andFilterWhere(['like', 'descripcionTipoGastos', $this->descripcionTipoGastos])
            ->andFilterWhere(['like', 'estadoTipoGastos', $this->estadoTipoGastos])
            ->andFilterWhere(['like', 'tbl_categoria_gastos.descripcionCategoriaGastos', $this->getAttribute('tbl_categoria_gastos.descripcionCategoriaGastos')]); //para busquedas de la tabla tbl_categoria_gastos;

        return $dataProvider;
    }
}
