<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Servicios;

/**
 * ServiciosSearch represents the model behind the search form about `backend\models\Servicios`.
 */
class ServiciosSearch extends Servicios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProdServicio', 'numFacturaProv', 'cantidadInventario', 'cantidadMinima', 'codFamilia', 'porcentUnidad'], 'integer'],
            [['tipo', 'nombreProductoServicio', 'exlmpuesto'], 'safe'],
            [['precioCompra', 'precioVenta', 'precioVentaImpuesto', 'anularDescuento', 'precioMinServicio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Servicios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codProdServicio' => $this->codProdServicio,
            'numFacturaProv' => $this->numFacturaProv,
            'cantidadInventario' => $this->cantidadInventario,
            'cantidadMinima' => $this->cantidadMinima,
            'codFamilia' => $this->codFamilia,
            'precioCompra' => $this->precioCompra,
            'porcentUnidad' => $this->porcentUnidad,
            'precioVenta' => $this->precioVenta,
            'precioVentaImpuesto' => $this->precioVentaImpuesto,
            'anularDescuento' => $this->anularDescuento,
            'precioMinServicio' => $this->precioMinServicio,
        ]);

        $query->andFilterWhere(['like', 'tipo', $this->tipo='Servicio'])
            ->andFilterWhere(['like', 'nombreProductoServicio', $this->nombreProductoServicio])
            ->andFilterWhere(['like', 'exlmpuesto', $this->exlmpuesto]);

        return $dataProvider;
    }
}
