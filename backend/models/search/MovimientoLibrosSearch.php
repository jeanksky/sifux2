<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MovimientoLibros;

/**
 * MovimientoLibrosSearch represents the model behind the search form about `backend\models\MovimientoLibros`.
 */
class MovimientoLibrosSearch extends MovimientoLibros
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idMovimiento_libro', 'idCuenta_bancaria', 'idTipoMoneda', 'idTipodocumento', 'numero_documento'], 'integer'],
            [['fecha_documento', 'beneficiario', 'detalle', 'fecha_registro', 'usuario_registra'], 'safe'],
            [['monto', 'tasa_cambio', 'monto_moneda_local'], 'number'],
            [['tbl_cuentas_bancarias.numero_cuenta', 'tbl_tipo_documento.descripcion','tbl_moneda.descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MovimientoLibros::find();

        //para comunicar tbl_movimiento_libros con tbl_moneda
        $query->leftJoin([
        'tbl_moneda'
        ], 'tbl_moneda.idTipo_moneda = tbl_movimiento_libros.idTipoMoneda');

        //para comunicar tbl_movimiento_libros con tbl_tipo_documento
        $query->leftJoin([
        'tbl_tipo_documento'
        ], 'tbl_tipo_documento.idTipo_documento = tbl_movimiento_libros.idTipodocumento');
        //para comunicar tbl_movimiento_libros con tbl_cuentas_bancarias
        $query->leftJoin([
        'tbl_cuentas_bancarias'
        ], 'tbl_cuentas_bancarias.idBancos = tbl_movimiento_libros.idCuenta_bancaria');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['idMovimiento_libro'=>SORT_DESC]]//Me trae los datos de forma desendiente
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idMovimiento_libro' => $this->idMovimiento_libro,
            'idCuenta_bancaria' => $this->idCuenta_bancaria,
            'idTipoMoneda' => $this->idTipoMoneda,
            'idTipodocumento' => $this->idTipodocumento,
            'numero_documento' => $this->numero_documento,
            'fecha_documento' => $this->fecha_documento,
            'monto' => $this->monto,
            'tasa_cambio' => $this->tasa_cambio,
            'monto_moneda_local' => $this->monto_moneda_local,
            'fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'beneficiario', $this->beneficiario])
            ->andFilterWhere(['like', 'detalle', $this->detalle])
            ->andFilterWhere(['like', 'tbl_moneda.descripcion', $this->getAttribute('tbl_moneda.descripcion')])
            ->andFilterWhere(['like', 'tbl_cuentas_bancarias.numero_cuenta', $this->getAttribute('tbl_cuentas_bancarias.numero_cuenta')])
            ->andFilterWhere(['like', 'tbl_tipo_documento.descripcion', $this->getAttribute('tbl_tipo_documento.descripcion')])
            ->andFilterWhere(['like', 'usuario_registra', $this->usuario_registra]);

        return $dataProvider;
    }
}
