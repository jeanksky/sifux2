<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\OrdenServicio;

/**
 * OrdenServicioSearch represents the model behind the search form about `backend\models\OrdenServicio`.
 */
class OrdenServicioSearch extends OrdenServicio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrdenServicio', 'montoServicio', 'idCliente', 'placa', 'idMecanico', 'cantOdometro'], 'integer'],
            [['fecha', 'radio', 'alfombra', 'herramienta', 'repuesto', 'cantCombustible', 'observaciones'], 'safe'],
            [['tbl_clientes.nombreCompleto'], 'safe'], //para traer datos de la tabla tbl_clientes
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenServicio::find()->where(['=','estado',1]);

        //para comunicar tbl_orden_servicio con tbl_clientes
        $query->leftJoin([
        'tbl_clientes'
        ], 'tbl_clientes.idCliente = tbl_orden_servicio.idCliente');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['idOrdenServicio'=>SORT_DESC]]//Me trae los datos de forma desendiente
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idOrdenServicio' => $this->idOrdenServicio,
            'fecha' => $this->fecha,
            'montoServicio' => $this->montoServicio,
            'idCliente' => $this->idCliente,
            'placa' => $this->placa,
            'idMecanico' => $this->idMecanico,
            'cantOdometro' => $this->cantOdometro,
        ]);

        $query->andFilterWhere(['like', 'radio', $this->radio])
            ->andFilterWhere(['like', 'alfombra', $this->alfombra])
            ->andFilterWhere(['like', 'herramienta', $this->herramienta])
            ->andFilterWhere(['like', 'repuesto', $this->repuesto])
            ->andFilterWhere(['like', 'cantCombustible', $this->cantCombustible])
            ->andFilterWhere(['like', 'tbl_clientes.nombreCompleto', $this->getAttribute('tbl_clientes.nombreCompleto')])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
