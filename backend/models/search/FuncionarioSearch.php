<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Funcionario;

/**
 * FuncionarioSearch represents the model behind the search form about `backend\models\Funcionario`.
 */

class FuncionarioSearch extends Funcionario
{

public $fullName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idFuncionario'], 'integer'],
            [['identificacion', 'nombre', 'apellido1', 'apellido2', 'genero', 'tipoFuncionario', 'username', 'email', 'tipo_usuario', 'estado'], 'safe'],
            [['fullName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Funcionario::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
                'attributes' => [

                   'idFuncionario',
                    'fullName' => [
                        'asc' => ['nombre' => SORT_ASC, 'apellido1' => SORT_ASC, 'apellido2' => SORT_ASC],
                        'desc' => ['nombre' => SORT_DESC, 'apellido1' => SORT_DESC, 'apellido2' => SORT_DESC],
                        'default' => SORT_ASC
                    ],
                    'username' => [
                        'asc' => ['username' => SORT_ASC],
                        'desc' => ['username' => SORT_DESC],
                    ],
                    'tipoFuncionario' => [
                        'asc' => ['tipoFuncionario' => SORT_ASC],
                        'desc' => ['tipoFuncionario' => SORT_DESC],
                    ],
                    'tipo_usuario' => [
                        'asc' => ['tipo_usuario' => SORT_ASC],
                        'desc' => ['tipo_usuario' => SORT_DESC],
                        
                    ]
                   
                ]
            ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idFuncionario' => $this->idFuncionario,
        ]);

        $query->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andWhere('nombre LIKE "%' . $this->fullName . '%" ' .
                        'OR apellido1 LIKE "%' . $this->fullName . '%"'.'OR apellido2 LIKE "%' . $this->fullName . '%"'
                    )
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido1', $this->apellido1])
            ->andFilterWhere(['like', 'apellido2', $this->apellido2])
            ->andFilterWhere(['like', 'genero', $this->genero])
            ->andFilterWhere(['like', 'tipoFuncionario', $this->tipoFuncionario])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'tipo_usuario', $this->tipo_usuario])
            ->andFilterWhere(['like', 'estado', $this->estado]);

        return $dataProvider;
    }
}
