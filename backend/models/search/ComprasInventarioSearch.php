<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ComprasInventario;

/**
 * ComprasInventarioSearch represents the model behind the search form about `backend\models\ComprasInventario`.
 */
class ComprasInventarioSearch extends ComprasInventario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCompra', 'numFactura', 'idProveedor'], 'integer'],
            [['fechaRegistro', 'fechaVencimiento', 'formaPago', 'estado', 'estado_pago', 'n_interno_prov'], 'safe'],
            [['subTotal', 'descuento', 'impuesto', 'total'], 'number'],
            [['tbl_proveedores.nombreEmpresa'], 'safe'],//para traer datos de la tabla tbl_clientes
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ComprasInventario::find();

        //para comunicar tbl_compras con tbl_proveedores
        $query->leftJoin([
        'tbl_proveedores'
        ], 'tbl_proveedores.codProveedores = tbl_compras.idProveedor');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['idCompra'=>SORT_DESC]]//Me trae los datos de forma desendiente
        ]);



        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCompra' => $this->idCompra,
            'numFactura' => $this->numFactura,
            'fechaRegistro' => $this->fechaRegistro,
            'fechaVencimiento' => $this->fechaVencimiento,
            'idProveedor' => $this->idProveedor,
            'subTotal' => $this->subTotal,
            'descuento' => $this->descuento,
            'impuesto' => $this->impuesto,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'formaPago', $this->formaPago])
        ->andFilterWhere(['like', 'numFactura', $this->numFactura])
        ->andFilterWhere(['like', 'n_interno_prov', $this->n_interno_prov])
        ->andFilterWhere(['like', 'estado_pago', $this->estado_pago])
        ->andFilterWhere(['like', 'estado', $this->estado])
        ->andFilterWhere(['like', 'tbl_proveedores.nombreEmpresa', $this->getAttribute('tbl_proveedores.nombreEmpresa')]);

        return $dataProvider;
    }
}
