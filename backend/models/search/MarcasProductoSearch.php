<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MarcasProducto;

/**
 * MarcasProductoSearch represents the model behind the search form about `backend\models\MarcasProducto`.
 */
class MarcasProductoSearch extends MarcasProducto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_marcas'], 'integer'],
            [['nombre_marca', 'estado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MarcasProducto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id_marcas'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_marcas' => $this->id_marcas,
        ]);

        $query->andFilterWhere(['like', 'nombre_marca', $this->nombre_marca])
            ->andFilterWhere(['like', 'estado', $this->estado]);

        return $dataProvider;
    }
}
