<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\OrdenCompraProv;

/**
 * OrdenCompraProvSearch represents the model behind the search form about `backend\models\OrdenCompraProv`.
 */
class OrdenCompraProvSearch extends OrdenCompraProv
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrdenCompra', 'idCompra', 'idProveedor', 'idContacto_pedido', 'id_transporte'], 'integer'],
            [['fecha_registro', 'fecha_ingreso_mercaderia', 'prioridad', 'observaciones', 'estado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenCompraProv::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idOrdenCompra' => $this->idOrdenCompra,
            'idCompra' => $this->idCompra,
            'idProveedor' => $this->idProveedor,
            'fecha_registro' => $this->fecha_registro,
            'fecha_ingreso_mercaderia' => $this->fecha_ingreso_mercaderia,
            'idContacto_pedido' => $this->idContacto_pedido,
            'id_transporte' => $this->id_transporte,
        ]);

        $query->andFilterWhere(['like', 'prioridad', $this->prioridad])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'estado', $this->estado]);

        return $dataProvider;
    }
}
