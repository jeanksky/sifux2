<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\EncabezadoCaja;

/**
 * EncabezadoCajaSearch represents the model behind the search form about `backend\models\EncabezadoCaja`.
 */
class EncabezadoCajaSearch extends EncabezadoCaja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCabeza_Factura', 'idOrdenServicio', 'codigoVendedor'], 'integer'],
            [['fecha_inicio', 'fecha_final', 'idCliente', 'estadoFactura', 'tipoFacturacion'], 'safe'],
            [['porc_descuento', 'iva', 'total_a_pagar', 'subtotal'], 'number'],
            [['tbl_clientes.nombreCompleto'], 'safe'], //para traer datos de la tabla tbl_clientes
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EncabezadoCaja::find()->where(['<>','estadoFactura','Pendiente'])->andWhere("estadoFactura NOT IN ('Apartado','Crédito','Anulada','Cancelada')", []);

        //para comunicar tbl_encabezado_factura con tbl_clientes
        $query->leftJoin([
        'tbl_clientes'
        ], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['idCabeza_Factura'=>SORT_DESC]]//Me trae los datos de forma desendiente
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCabeza_Factura' => $this->idCabeza_Factura,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_final' => $this->fecha_final,
            'idOrdenServicio' => $this->idOrdenServicio,
            'porc_descuento' => $this->porc_descuento,
            'iva' => $this->iva,
            'total_a_pagar' => $this->total_a_pagar,
            'codigoVendedor' => $this->codigoVendedor,
            'subtotal' => $this->subtotal,
        ]);

        $query->andFilterWhere(['like', 'idCliente', $this->idCliente])
            ->andFilterWhere(['like', 'estadoFactura', $this->estadoFactura])
            ->andFilterWhere(['like', 'tbl_clientes.nombreCompleto', $this->getAttribute('tbl_clientes.nombreCompleto')])
            ->andFilterWhere(['like', 'tipoFacturacion', $this->tipoFacturacion]);

        return $dataProvider;
    }
}
