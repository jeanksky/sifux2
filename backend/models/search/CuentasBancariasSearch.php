<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CuentasBancarias;

/**
 * CuentasBancariasSearch represents the model behind the search form about `backend\models\CuentasBancarias`.
 */
class CuentasBancariasSearch extends CuentasBancarias
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idBancos', 'idTipo_moneda', 'idEntidad_financiera'], 'integer'],
            [['numero_cuenta', 'descripcion', 'tipo_cuenta', 'cuenta_cliente'], 'safe'],
            [['saldo_libros', 'saldo_bancos'], 'number'],
            [['tbl_moneda.descripcion', 'tbl_entidades_financieras.descripcion'], 'safe'], //para traer datos de la tabla tbl_moneda y tbl_entidades_financieras
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuentasBancarias::find();

        //para comunicar tbl_cuentas_bancarias con tbl_moneda
        $query->leftJoin([
        'tbl_moneda'
        ], 'tbl_moneda.idTipo_moneda = tbl_cuentas_bancarias.idTipo_moneda');

        //para comunicar tbl_cuentas_bancarias con tbl_entidades_financieras
        $query->leftJoin([
        'tbl_entidades_financieras'
        ], 'tbl_entidades_financieras.idEntidad_financiera = tbl_cuentas_bancarias.idEntidad_financiera');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idBancos' => $this->idBancos,
            'idTipo_moneda' => $this->idTipo_moneda,
            'idEntidad_financiera' => $this->idEntidad_financiera,
            'saldo_libros' => $this->saldo_libros,
            'saldo_bancos' => $this->saldo_bancos,
        ]);

        $query->andFilterWhere(['like', 'numero_cuenta', $this->numero_cuenta])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'tipo_cuenta', $this->tipo_cuenta])
            ->andFilterWhere(['like', 'cuenta_cliente', $this->cuenta_cliente])
            ->andFilterWhere(['like', 'tbl_moneda.descripcion', $this->getAttribute('tbl_moneda.descripcion')])
            ->andFilterWhere(['like', 'tbl_entidades_financieras.descripcion', $this->getAttribute('tbl_entidades_financieras.descripcion')]);

        return $dataProvider;
    }
}
