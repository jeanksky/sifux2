<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DerechosUsuarios;

/**
 * DerechosUsuariosSearch represents the model behind the search form about `backend\models\DerechosUsuarios`.
 */
class DerechosUsuariosSearch extends DerechosUsuarios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDerechoUsuario', 'idUsuario'], 'integer'],
            [['caja', 'recepcion', 'mecanica', 'Otros'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DerechosUsuarios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idDerechoUsuario' => $this->idDerechoUsuario,
            'idUsuario' => $this->idUsuario,
        ]);

        $query->andFilterWhere(['like', 'caja', $this->caja])
            ->andFilterWhere(['like', 'recepcion', $this->recepcion])
            ->andFilterWhere(['like', 'mecanica', $this->mecanica])
            ->andFilterWhere(['like', 'Otros', $this->Otros]);

        return $dataProvider;
    }
}
