<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ProveedoresGastos;

/**
 * ProveedoresGastosSearch represents the model behind the search form about `backend\models\ProveedoresGastos`.
 */
class ProveedoresGastosSearch extends ProveedoresGastos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProveedorGastos'], 'integer'],
            [['tipoEntidad', 'identificacion', 'nombreEmpresa', 'telefono', 'sitioWeb', 'email', 'estado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProveedoresGastos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idProveedorGastos' => $this->idProveedorGastos,
        ]);

        $query->andFilterWhere(['like', 'tipoEntidad', $this->tipoEntidad])
            ->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'nombreEmpresa', $this->nombreEmpresa])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'sitioWeb', $this->sitioWeb])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'estado', $this->estado]);

        return $dataProvider;
    }
}
