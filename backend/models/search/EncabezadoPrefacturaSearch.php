<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\EncabezadoPrefactura;

/**
 * EncabezadoPrefacturaSearch represents the model behind the search form about `backend\models\EncabezadoPrefactura`.
 */
class EncabezadoPrefacturaSearch extends EncabezadoPrefactura
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'idCabeza_Factura', 'idCliente',*/ 'idOrdenServicio', 'porc_descuento', 'iva'], 'integer'],
            [['fecha_inicio', 'fecha_final', 'estadoFactura'], 'safe'],
            [['total_a_pagar'], 'number'],
            [['idCabeza_Factura', 'idCliente', 'tbl_clientes.nombreCompleto','tbl_orden_servicio.idMecanico', 'tbl_factura_ot.idCliente'], 'safe'], //para traer datos de la tabla tbl_clientes y tbl_mecanicos
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EncabezadoPrefactura::find()->where(['=','estadoFactura','Pendiente'])->orderBy(['idCabeza_Factura' => SORT_DESC]);


        //para comunicar tbl_encabezado_factura con tbl_clientes
        $query->leftJoin([
        'tbl_factura_ot'
        ], 'tbl_factura_ot.idCabeza_Factura = tbl_encabezado_factura.idCabeza_Factura');

        //para comunicar tbl_encabezado_factura con tbl_clientes
        $query->leftJoin([
        'tbl_clientes'
        ], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente');

        //para comunicar tbl_orden_servicio con tbl_clientes
        $query->leftJoin([
        'tbl_orden_servicio'
        ], 'tbl_orden_servicio.idOrdenServicio = tbl_encabezado_factura.idOrdenServicio');
        //------------------------------------------------

        $dataProvider = new ActiveDataProvider([
            'query' => $query//,
            //'sort'=> ['defaultOrder' => ['idCabeza_Factura'=>SORT_ASC]]//Me trae los datos de forma desendiente
        ]);

        //para comunicar tbl_encabezado_factura con tbl_clientes y con tbl_orden_servicio
        $dataProvider->setSort([
            'attributes'=>[
                'tbl_encabezado_factura.idCabeza_Factura',
                'fecha_inicio',
                'idOrdenServicio',
                'porc_descuento' ,
                'iva',
                'total_a_pagar',
                'tbl_encabezado_factura.idCliente',
                'tbl_factura_ot.idCliente',
                'tbl_clientes.nombreCompleto',
                'tbl_orden_servicio.idMecanico',
                'estadoFactura',
            ]
        ]); //-----------------------------------------

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tbl_encabezado_factura.idCabeza_Factura' => $this->idCabeza_Factura,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_final' => $this->fecha_final,
            'tbl_encabezado_factura.idCliente' => $this->idCliente,
            'idOrdenServicio' => $this->idOrdenServicio,
            'porc_descuento' => $this->porc_descuento,
            'iva' => $this->iva,
            'total_a_pagar' => $this->total_a_pagar,
        ]);

        $query->andFilterWhere(['like', 'estadoFactura', $this->estadoFactura])
        //->andFilterWhere(['like', 'idCabeza_Factura', $this->idCabeza_Factura])
        ->andFilterWhere(['like', 'fecha_inicio', $this->fecha_inicio])
        ->andFilterWhere(['like', 'tbl_encabezado_factura.idCabeza_Factura', $this->idCabeza_Factura])
        ->andFilterWhere(['=', 'tbl_factura_ot.idCliente', $this->getAttribute('tbl_factura_ot.idCliente')])
        ->andFilterWhere(['like', 'tbl_clientes.nombreCompleto', $this->getAttribute('tbl_clientes.nombreCompleto')])
        ->andFilterWhere(['like', 'tbl_encabezado_factura.idCliente', $this->getAttribute('tbl_encabezado_factura.idCliente')])
        ->andFilterWhere(['like', 'tbl_orden_servicio.idMecanico', $this->getAttribute('tbl_orden_servicio.idMecanico')]); //para busquedas de la tabla tbl_clientes

        return $dataProvider;
    }
}
