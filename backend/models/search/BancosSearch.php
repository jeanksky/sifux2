<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Bancos;

/**
 * BancosSearch represents the model behind the search form about `backend\models\Bancos`.
 */
class BancosSearch extends Bancos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idBanco'], 'integer'],
            [['nombreBanco'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bancos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idBanco' => $this->idBanco,
        ]);

        $query->andFilterWhere(['like', 'nombreBanco', $this->nombreBanco]);

        return $dataProvider;
    }
}
