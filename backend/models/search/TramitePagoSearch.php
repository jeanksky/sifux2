<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TramitePago;

/**
 * TramitePagoSearch represents the model behind the search form about `backend\models\TramitePago`.
 */
class TramitePagoSearch extends TramitePago
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTramite_pago', 'idProveedor'], 'integer'],
            [['cantidad_monto_documento', 'monto_saldo_pago', 'porcentaje_descuento', 'monto_tramite_pagar'], 'number'],
            [['recibo_cancelacion', 'email_proveedor', 'prioridad_pago', 'usuario_registra', 'fecha_registra', 'usuario_cancela', 'fecha_cancela', 'usuario_aplica', 'fecha_aplica'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TramitePago::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idTramite_pago' => $this->idTramite_pago,
            'idProveedor' => $this->idProveedor,
            'cantidad_monto_documento' => $this->cantidad_monto_documento,
            'monto_saldo_pago' => $this->monto_saldo_pago,
            'porcentaje_descuento' => $this->porcentaje_descuento,
            'monto_tramite_pagar' => $this->monto_tramite_pagar,
            'fecha_registra' => $this->fecha_registra,
            'fecha_cancela' => $this->fecha_cancela,
            'fecha_aplica' => $this->fecha_aplica,
        ]);

        $query->andFilterWhere(['like', 'recibo_cancelacion', $this->recibo_cancelacion])
            ->andFilterWhere(['like', 'email_proveedor', $this->email_proveedor])
            ->andFilterWhere(['like', 'prioridad_pago', $this->prioridad_pago])
            ->andFilterWhere(['like', 'usuario_registra', $this->usuario_registra])
            ->andFilterWhere(['like', 'usuario_cancela', $this->usuario_cancela])
            ->andFilterWhere(['like', 'usuario_aplica', $this->usuario_aplica]);

        return $dataProvider;
    }
}
