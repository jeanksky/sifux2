<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Gastos;

/**
 * GastosSearch represents the model behind the search form about `backend\models\Gastos`.
 */
class GastosSearch extends Gastos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idGastos', 'idProveedorGastos', 'idTipoGastos', 'idMedioPagoGastos', 'idBancos', 'idCuentaBancariaLocal', 'digitosTarjeta'], 'integer'],
            [['numeroDocumentoTransferencias', 'numeroCheque', 'numeroAutorizacionTarjeta', 'numeroDocumento', 'montoGasto', 'fechaDocumento', 'fechaPago', 'detalleGasto', 'fechaRegistro', 'usuarioRegistra', 'fechaAplicada', 'usuarioAplica', 'estadoGasto'], 'safe'],
            [['tbl_proveedores_gastos.nombreEmpresa','tbl_medio_pago_gastos.descripcionMedioPagoGastos'], 'safe'], //para traer datos de la tabla tbl_proveedores_gastos
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //Busca solo los que tenga estado aplicado
        $query = Gastos::find()->where(['=','estadoGasto','Aplicado']); // Pendiente - Aplicado

        //para comunicar tbl_tipos_gastos con tbl_categoria_gastos
        $query->leftJoin([
        'tbl_proveedores_gastos'
      ], 'tbl_proveedores_gastos.idProveedorGastos = tbl_gastos.idProveedorGastos');

        //para comunicar tbl_tipos_gastos con tbl_categoria_gastos
        $query->leftJoin([
        'tbl_medio_pago_gastos'
      ], 'tbl_medio_pago_gastos.idMedioPagoGastos = tbl_gastos.idMedioPagoGastos');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['idGastos'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idGastos' => $this->idGastos,
            'idProveedorGastos' => $this->idProveedorGastos,
            'idTipoGastos' => $this->idTipoGastos,
            'idMedioPagoGastos' => $this->idMedioPagoGastos,
            'idBancos' => $this->idBancos,
            'idCuentaBancariaLocal' => $this->idCuentaBancariaLocal,
            'digitosTarjeta' => $this->digitosTarjeta,
            'fechaDocumento' => $this->fechaDocumento,
            'fechaPago' => $this->fechaPago,
            'fechaRegistro' => $this->fechaRegistro,
            'fechaAplicada' => $this->fechaAplicada,
        ]);

        $query->andFilterWhere(['like', 'numeroDocumentoTransferencias', $this->numeroDocumentoTransferencias])
            ->andFilterWhere(['like', 'numeroCheque', $this->numeroCheque])
            ->andFilterWhere(['like', 'numeroAutorizacionTarjeta', $this->numeroAutorizacionTarjeta])
            ->andFilterWhere(['like', 'numeroDocumento', $this->numeroDocumento])
            ->andFilterWhere(['like', 'detalleGasto', $this->detalleGasto])
            ->andFilterWhere(['like', 'usuarioRegistra', $this->usuarioRegistra])
            ->andFilterWhere(['like', 'usuarioAplica', $this->usuarioAplica])
            ->andFilterWhere(['like', 'estadoGasto', $this->estadoGasto])
            ->andFilterWhere(['like', 'tbl_proveedores_gastos.idProveedorGastos', $this->getAttribute('tbl_proveedores_gastos.idProveedorGastos')])
            ->andFilterWhere(['like', 'tbl_medio_pago_gastos.idMedioPagoGastos', $this->getAttribute('tbl_medio_pago_gastos.idMedioPagoGastos')]); //para busquedas de la tabla tbl_categoria_gastos;

        return $dataProvider;
    }
}
