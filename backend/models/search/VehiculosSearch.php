<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Vehiculos;

/**
 * VehiculosSearch represents the model behind the search form about `backend\models\Vehiculos`.
 */
class VehiculosSearch extends Vehiculos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCliente', 'anio'], 'integer'],
            [['placa','modelo', 'marca', 'combustible', 'motor', 'cilindrada_motor', 'version'], 'safe'],
            [['tbl_clientes.nombreCompleto', 'tbl_marcas.nombre_marca', 'tbl_modelo.nombre_modelo'], 'safe'], //para traer datos de la tabla tbl_clientes
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vehiculos::find();

        //para comunicar tbl_vehiculos con tbl_clientes
        $query->leftJoin([
        'tbl_clientes'
        ], 'tbl_clientes.idCliente = tbl_vehiculos.idCliente');
        //------------------------------------------------
        //para comunicar tbl_vehiculos con tbl_marcas
        $query->leftJoin([
        'tbl_marcas'
      ], 'tbl_marcas.codMarca = tbl_vehiculos.marca');
        //------------------------------------------------
        //para comunicar tbl_vehiculos con tbl_modelo
        $query->leftJoin([
        'tbl_modelo'
      ], 'tbl_modelo.codModelo = tbl_vehiculos.modelo');
        //------------------------------------------------

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //para comunicar tbl_vehiculos con tbl_clientes
        $dataProvider->setSort([
            'attributes'=>[
                'placa',
                'marca',
                'modelo',
                'anio',
                'tbl_clientes.nombreCompleto','tbl_marcas.nombre_marca','tbl_modelo.nombre_modelo'
            ]
        ]); //-----------------------------------------

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'placa' => $this->placa,
            'idCliente' => $this->idCliente,
            'anio' => $this->anio,
        ]);

        $query->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'modelo', $this->modelo])
            ->andFilterWhere(['like', 'marca', $this->marca])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'motor', $this->motor])
            ->andFilterWhere(['like', 'cilindrada_motor', $this->cilindrada_motor])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'tbl_clientes.nombreCompleto', $this->getAttribute('tbl_clientes.nombreCompleto')])//para busquedas de la tabla tbl_clientes
            ->andFilterWhere(['like', 'tbl_marcas.nombre_marca', $this->getAttribute('tbl_marcas.nombre_marca')])//para busquedas de la tabla tbl_marcas
            ->andFilterWhere(['like', 'tbl_modelo.nombre_modelo', $this->getAttribute('tbl_modelo.nombre_modelo')]);//para busquedas de la tabla tbl_modelo

        return $dataProvider;
    }
}
