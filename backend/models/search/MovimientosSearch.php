<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Movimientos;

/**
 * MovimientosSearch represents the model behind the search form about `backend\models\Movimientos`.
 */
class MovimientosSearch extends Movimientos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idMovimiento'], 'integer'],
            [['codigoTipo'], 'integer'],
            [['documentoRef'], 'safe'],
            [['observaciones'], 'safe'],
            [['estado'], 'safe'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Movimientos::find()->orderBy(['idMovimiento' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['idMovimiento'=>SORT_ASC]],//Me trae los datos de forma desendiente
        ]);


        $dataProvider->setSort([
            'attributes'=>[
            'idMovimiento',
            'observaciones',
            'documentoRef','estado',

                    /*'fecha' => [
                        'asc' => ['fecha' => SORT_ASC],
                        'desc' => ['fecha' => SORT_DESC],
                    ],*/
                    /*'codigoTipo' => [
                        'asc' => ['codigoTipo' => SORT_ASC],
                        'desc' => ['codigoTipo' => SORT_DESC],

                    ]*/

                ]
            ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idMovimiento' => $this->idMovimiento,
            // 'codProdServicio' => $this->codProdServicio,
            // 'cantidad' => $this->cantidad,
        ]);

        $query->andFilterWhere(['like', 'codigoTipo', $this->codigoTipo])
            ->andFilterWhere(['like', 'fecha', $this->fecha])
            ->andFilterWhere(['like', 'documentoRef', $this->documentoRef])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'estado', $this->estado]);

        return $dataProvider;
    }
}
