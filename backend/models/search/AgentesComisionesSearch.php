<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AgentesComisiones;

/**
 * AgentesComisionesSearch represents the model behind the search form about `backend\models\AgentesComisiones`.
 */
class AgentesComisionesSearch extends AgentesComisiones
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idAgenteComision'], 'integer'],
            [['cedula', 'nombre', 'telefono', 'direccion','estadoAgenteComision'], 'safe'],
            [['tipoIdentidad'],'string'],
            [['porcentComision'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgentesComisiones::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tipoIdentidad' => $this->tipoIdentidad,
            'idAgenteComision' => $this->idAgenteComision,
            'porcentComision' => $this->porcentComision,
            'estadoAgenteComision' => $this->estadoAgenteComision,
        ]);

        $query->andFilterWhere(['like', 'cedula', $this->cedula])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'estadoAgenteComision', $this->estadoAgenteComision])
             ->andFilterWhere(['like', 'tipoIdentidad', $this->tipoIdentidad]);

        return $dataProvider;
    }
}
