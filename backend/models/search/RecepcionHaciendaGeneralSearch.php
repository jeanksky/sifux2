<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RecepcionHaciendaGeneral;

/**
 * RecepcionHaciendaGeneralSearch represents the model behind the search form about `backend\models\RecepcionHaciendaGeneral`.
 */
class RecepcionHaciendaGeneralSearch extends RecepcionHaciendaGeneral
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reha_id', 'reha_factun_id'], 'integer'],
            [['reha_tipo_documento', 'reha_resolucion', 'reha_detalle', 'reha_respuesta_hacienda', 'reha_fecha_hora', 'reha_clave_recepcion', 'reha_xml_recepcion', 'reha_xml_respuesta'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RecepcionHaciendaGeneral::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'reha_id' => $this->reha_id,
            'reha_factun_id' => $this->reha_factun_id,
            'reha_fecha_hora' => $this->reha_fecha_hora,
        ]);

        $query->andFilterWhere(['like', 'reha_tipo_documento', $this->reha_tipo_documento])
            ->andFilterWhere(['like', 'reha_resolucion', $this->reha_resolucion])
            ->andFilterWhere(['like', 'reha_detalle', $this->reha_detalle])
            ->andFilterWhere(['like', 'reha_respuesta_hacienda', $this->reha_respuesta_hacienda])
            ->andFilterWhere(['like', 'reha_clave_recepcion', $this->reha_clave_recepcion])
            ->andFilterWhere(['like', 'reha_xml_recepcion', $this->reha_xml_recepcion])
            ->andFilterWhere(['like', 'reha_xml_respuesta', $this->reha_xml_respuesta]);

        return $dataProvider;
    }
}
