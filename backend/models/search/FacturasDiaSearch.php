<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\FacturasDia;

/**
 * FacturasDiaSearch represents the model behind the search form about `backend\models\FacturasDia`.
 */
class FacturasDiaSearch extends FacturasDia
{
    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['idCabeza_Factura', 'idCliente' , 'idOrdenServicio', 'codigoVendedor'], 'integer'],
            [['fecha_inicio', 'fecha_final', 'estadoFactura', 'tipoFacturacion'], 'safe'],
            [['porc_descuento', 'iva', 'total_a_pagar', 'subtotal'], 'number'],
            [['tbl_clientes.nombreCompleto'], 'safe'],//para traer datos de la tabla tbl_clientes
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = FacturasDia::find()->where(['=','estadoFactura','Cancelada']) ->andWhere(['=','fecha_final', date('Y-m-d')]);
        //me consulta la facturas con estado Cancelada, Crédito, Anulada nada más
        $query = FacturasDia::find()->where("estadoFactura IN (:estado , :estado2, :estado3)", [":estado"=>"Cancelada", ":estado2"=>"Crédito", ":estado3"=>"Anulada"])->andWhere("fecha_emision = :fecha", [":fecha"=>date('Y-m-d')]);

        //para comunicar encabezado_factura con tbl_clientes
        $query->leftJoin([
        'tbl_clientes'
        ], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['idCabeza_Factura'=>SORT_DESC]]//Me trae los datos de forma desendiente
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCabeza_Factura' => $this->idCabeza_Factura,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_final' => $this->fecha_final,
            'idOrdenServicio' => $this->idOrdenServicio,
            'porc_descuento' => $this->porc_descuento,
            'idCliente' => $this->idCliente,
            'iva' => $this->iva,
            'total_a_pagar' => $this->total_a_pagar,
            'codigoVendedor' => $this->codigoVendedor,
            'subtotal' => $this->subtotal,
        ]);

        $query->andFilterWhere(['like', 'estadoFactura', $this->estadoFactura])
            ->andFilterWhere(['like', 'idCliente', $this->idCliente])
            ->andFilterWhere(['like', 'tbl_clientes.nombreCompleto', $this->att()])
            ->andFilterWhere(['like', 'tipoFacturacion', $this->tipoFacturacion]);

        return $dataProvider;
    }

    public function att(){
        $att = $this->getAttribute('tbl_clientes.nombreCompleto');
        return $att;
    }
}
