<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Clientes;

/**
 * ClientesSearch represents the model behind the search form about `backend\models\Clientes`.
 */
class ClientesSearch extends Clientes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCliente'], 'integer'],
            [['tipoIdentidad', 'nombreCompleto', 'identificacion', 'genero', 'fechNacimiento', 'telefono', 'fax', 'celular', 'email', 'direccion', 'credito', 'documentoGarantia', 'diasCredito', 'autorizacion', 'estadoCuenta'], 'safe'],
            [['montoMaximoCredito', 'montoTotalEjecutado'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clientes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCliente' => $this->idCliente,
            'fechNacimiento' => $this->fechNacimiento,
            'montoMaximoCredito' => $this->montoMaximoCredito,
            'montoTotalEjecutado' => $this->montoTotalEjecutado,
        ]);

        $query->andFilterWhere(['like', 'tipoIdentidad', $this->tipoIdentidad])
            ->andFilterWhere(['like', 'nombreCompleto', $this->nombreCompleto])
            ->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'genero', $this->genero])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'celular', $this->celular])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'credito', $this->credito])
            ->andFilterWhere(['like', 'documentoGarantia', $this->documentoGarantia])
            ->andFilterWhere(['like', 'diasCredito', $this->diasCredito])
            ->andFilterWhere(['like', 'autorizacion', $this->autorizacion])
            ->andFilterWhere(['like', 'estadoCuenta', $this->estadoCuenta]);

        return $dataProvider;
    }
}
