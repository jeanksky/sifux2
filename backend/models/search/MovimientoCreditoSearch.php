<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MovimientoCredito;

/**
 * MovimientoCreditoSearch represents the model behind the search form about `backend\models\MovimientoCredito`.
 */
class MovimientoCreditoSearch extends MovimientoCredito
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCabeza_Factura', 'idOrdenServicio', 'codigoVendedor'], 'integer'],
            [['fecha_inicio', 'fecha_final', 'fecha_vencimiento', 'idCliente', 'estadoFactura', 'tipoFacturacion', 'observacion'], 'safe'],
            [['porc_descuento', 'iva', 'total_a_pagar', 'subtotal'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MovimientoCredito::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCabeza_Factura' => $this->idCabeza_Factura,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_final' => $this->fecha_final,
            'fecha_vencimiento' => $this->fecha_vencimiento,
            'idOrdenServicio' => $this->idOrdenServicio,
            'porc_descuento' => $this->porc_descuento,
            'iva' => $this->iva,
            'total_a_pagar' => $this->total_a_pagar,
            'codigoVendedor' => $this->codigoVendedor,
            'subtotal' => $this->subtotal,
        ]);

        $query->andFilterWhere(['like', 'idCliente', $this->idCliente])
            ->andFilterWhere(['like', 'estadoFactura', $this->estadoFactura])
            ->andFilterWhere(['like', 'tipoFacturacion', $this->tipoFacturacion])
            ->andFilterWhere(['like', 'observacion', $this->observacion]);

        return $dataProvider;
    }
}
