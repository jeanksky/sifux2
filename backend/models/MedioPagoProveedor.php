<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_medio_pago_proveedor".
 *
 * @property integer $idMedio_pago
 * @property integer $idTramite_pago
 * @property string $medio_pago
 * @property integer $idBanco
 * @property integer $idTipo_documento
 * @property string $numero_cheque
 * @property string $numero_recibo_cancelacion
 * @property string $nombre_agente
 * @property string $fecha_documento
 * @property string $cuenta_bancaria_proveedor
 */
class MedioPagoProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_medio_pago_proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTramite_pago', 'medio_pago'], 'required'],
            [['idTramite_pago', 'idBanco', 'idTipo_documento', 'cuenta_bancaria_proveedor', 'cuenta_bancaria_local', 'idContacto_pago'], 'integer'],
            [['fecha_documento'], 'safe'],
            [['medio_pago'], 'string', 'max' => 40],
            [['numero_cheque', 'numero_recibo_cancelacion'], 'string', 'max' => 50],
            [['nombre_agente'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMedio_pago' => 'Id Medio Pago',
            'idTramite_pago' => 'Id Tramite Pago',
            'medio_pago' => 'Medio Pago',
            'idBanco' => 'Id Banco',
            'idTipo_documento' => 'Id Tipo Documento',
            'numero_cheque' => 'Numero Cheque',
            'numero_recibo_cancelacion' => 'Numero Recibo Cancelacion',
            'nombre_agente' => 'Nombre Agente',
            'fecha_documento' => 'Fecha Documento',
            'cuenta_bancaria_proveedor' => 'Cuenta Bancaria Proveedor',
            'cuenta_bancaria_local'=>'Cuenta bancaria local',
        ];
    }
}
