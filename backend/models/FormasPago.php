<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_formas_pago".
 *
 * @property string $id_forma
 * @property string $desc_forma
 */
class FormasPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_formas_pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['desc_forma'], 'required'],
            [['desc_forma'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_forma' => 'Id Forma',
            'desc_forma' => 'Desc Forma',
        ];
    }
}
