<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_detalle_orden_compra_temp".
 *
 * @property integer $idDetaOrdComTempo
 * @property integer $idProveedores
 * @property integer $codProdServicio
 * @property integer $idProd_prov
 * @property integer $cantidad_sujerida
 * @property integer $cantidad_pedir
 */
class DetalleOrdenCompraTemporal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_detalle_orden_compra_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['codProdServicio'], 'string', 'max' => 20],
            [['idProveedores', 'idProd_prov', 'cantidad_sugerida', 'cantidad_pedir'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDetaOrdComTempo' => 'Id Deta Ord Com Tempo',
            'idProveedores' => 'Id Proveedores',
            'codProdServicio' => 'Cod Prod Servicio',
            'idProd_prov' => 'Id Prod Prov',
            'cantidad_sujerida' => 'Cantidad Sujerida',
            'cantidad_pedir' => 'Cantidad Pedir',
        ];
    }
}
