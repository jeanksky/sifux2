<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_cuentas_bancarias_proveedor".
 *
 * @property integer $id
 * @property integer $CodProveedores
 * @property string $entidad_bancaria
 * @property string $descripcion
 * @property string $cuenta_bancaria
 */
class CuentasBancariasProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cuentas_bancarias_proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodProveedores','entidad_bancaria','descripcion','cuenta_bancaria'], 'required'],
            [['CodProveedores'], 'integer'],
            [['entidad_bancaria', 'descripcion', 'cuenta_bancaria'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'CodProveedores' => 'Cod Proveedores',
            'entidad_bancaria' => 'Entidad Bancaria',
            'descripcion' => 'Descripcion',
            'cuenta_bancaria' => 'Cuenta Bancaria',
        ];
    }
}
