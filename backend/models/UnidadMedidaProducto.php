<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_unidad_medida_producto".
 *
 * @property integer $idUnidadMedida
 * @property string $descripcionUnidadMedida
 * @property string $simbolo
 */
class UnidadMedidaProducto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_unidad_medida_producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcionUnidadMedida'], 'required'],
            [['descripcionUnidadMedida'], 'string', 'max' => 100],
            [['simbolo'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUnidadMedida' => 'Id Unidad Medida',
            'descripcionUnidadMedida' => 'Descripcion Unidad Medida',
            'simbolo' => 'Simbolo',
        ];
    }
}
