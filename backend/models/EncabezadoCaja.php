<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use backend\models\Empresa;//
use backend\models\Factun;
use backend\models\EncabezadoPrefactura;
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
use backend\models\Clientes;
use backend\models\FormasPago;
//require("mpdf/mpdf.php");
use mPDF;
/**
 * This is the model class for table "tbl_encabezado_factura".
 *
 * @property integer $idCabeza_Factura
 * @property string $fecha_inicio
 * @property string $fecha_final
 * @property string $idCliente
 * @property integer $idOrdenServicio
 * @property double $porc_descuento
 * @property double $iva
 * @property double $total_a_pagar
 * @property string $estadoFactura
 * @property string $tipoFacturacion
 * @property integer $codigoVendedor
 * @property double $subtotal
 *
 * @property TblDetalleFacturas[] $tblDetalleFacturas
 * @property TblHistCancelFacturaCr[] $tblHistCancelFacturaCrs
 * @property TblMedioPago[] $tblMedioPagos
 */
class EncabezadoCaja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_encabezado_factura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_final'], 'safe'],
            [['idOrdenServicio', 'codigoVendedor'], 'integer'],
            [['porc_descuento', 'iva', 'total_a_pagar', 'subtotal'], 'number'],
            [['estadoFactura'], 'required'],
            [['idCliente'], 'string', 'max' => 80],
            [['estadoFactura'], 'string', 'max' => 12],
            [['tipoFacturacion'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCabeza_Factura' => 'No. Factura',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_final' => 'Fecha Final',
            'idCliente' => 'Id Cliente',
            'idOrdenServicio' => 'Id Orden Servicio',
            'porc_descuento' => 'Descuento',
            'iva' => 'Iva',
            'total_a_pagar' => 'Total',
            'estadoFactura' => 'Estado Factura',
            'tipoFacturacion' => 'Tipo Facturacion',
            'codigoVendedor' => 'Codigo Vendedor',
            'subtotal' => 'Subtotal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblDetalleFacturas()
    {
        return $this->hasMany(TblDetalleFacturas::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblHistCancelFacturaCrs()
    {
        return $this->hasMany(TblHistCancelFacturaCr::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblMedioPagos()
    {
        return $this->hasMany(TblMedioPago::className(), ['idCabeza_factura' => 'idCabeza_Factura']);
    }

    public function afterFind() // para obtener la fecha con la estructura
    {

        $this->fecha_inicio = date('d-m-Y', strtotime( $this->fecha_inicio));

        if ($this->fecha_final!='') {
            $this->fecha_final = date('d-m-Y', strtotime( $this->fecha_final));
        }

        if ($this->fecha_vencimiento!='') {
            $this->fecha_vencimiento = date('d-m-Y', strtotime( $this->fecha_vencimiento));
        }

        return parent::afterFind();


    }// fin de la funcion ordenar fecha

    public function attributes()
    {
        return array_merge(parent::attributes(), ['tbl_clientes.nombreCompleto']);
    }

    public function _pdf($id, $stylesheet, $pdf_html, $archivo_pdf)
    {
      $pdf = new mPDF();
      $pdf->title = 'Factura No. '.$id;
      //$pdf->SetHtmlHeader($comprobante_electronico);
      $pdf->WriteHTML($stylesheet,1);
      $pdf->WriteHTML($pdf_html,2);
      //$pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());

      $pdf->Output($archivo_pdf,'');
    }

    public function _pdf_nc($id, $stylesheet, $pdf_html, $archivo_pdf)
    {
      $pdf = new mPDF();
      $pdf->title = 'Nota de cŕedito No. '.$id;
      //$pdf->SetHtmlHeader($comprobante_electronico);
      $pdf->WriteHTML($stylesheet,1);
      $pdf->WriteHTML($pdf_html,2);
      //$pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());

      $pdf->Output($archivo_pdf,'');
    }

    public function _pdf_show($id, $stylesheet, $pdf_html, $archivo_pdf)
    {
      $pdf = new mPDF();
      $pdf->title = 'Factura No. '.$id;
      $pdf->WriteHTML($stylesheet,1);
      $pdf->WriteHTML($pdf_html,2);
      $pdf->Output($archivo_pdf,'I');
    }

    public function _pdf_nc_show($id, $stylesheet, $pdf_html, $archivo_pdf)
    {
      $pdf = new mPDF();
      $pdf->title = 'Nota de cŕedito No. '.$id;
      $pdf->WriteHTML($stylesheet,1);
      $pdf->WriteHTML($pdf_html,2);
      $pdf->Output($archivo_pdf,'I');
    }

}
