<?php

namespace backend\models;
use Yii;
use backend\models\Empresa;
//clase para conectar con factun
class Factun{
  public function ConeccionFactun_con_pan($api_dir, $json_data, $medio)//coneccion con parametros
  {
    $empresa = Empresa::findOne(1);
    $user = $empresa->user_factun;
    $pass = $empresa->pass_factun;
    $token_factun = $empresa->token_factun;
    $data_string = json_encode($json_data);
    $ch = curl_init($api_dir);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $medio);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic '. base64_encode("{$user}:{$pass}"),
        'FactunToken: '.$token_factun,
        'Content-Type: application/json; charset=utf-8',
        'Content-Length: ' . strlen($data_string))
    );

    $json = curl_exec($ch);
    return $json;
  }

  public function ConeccionFactun_sin_pan($api_dir, $medio)//coneccion sin parametros
  {
    $empresa = Empresa::findOne(1);
    $user = $empresa->user_factun;
    $pass = $empresa->pass_factun;
    $token_factun = $empresa->token_factun;
    $data_json = http_build_query(array());
    $ch = curl_init($api_dir);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $medio);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic '. base64_encode("{$user}:{$pass}"),
        'Content-Type: application/json',
        'FactunToken: '.$token_factun,
        'Content-Length: 0')
    );

    return curl_exec($ch);
  }
}

 ?>
