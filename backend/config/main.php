<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    //'bootstrap' => ['log', 'megaMenu'],
    'modules' => [
            'backup' => [
                'class' => 'spanjeta\modules\backup\Module',
            ],
    ],
    'language'=>'es',
    'sourceLanguage'=>'es',
    'components' => [
    //can name it whatever you want as it is custom
        'urlManagerFrontend' => [
                'class' => 'yii\web\urlManager',
                'baseUrl' => '/frontend/web/',//i.e. $_SERVER['DOCUMENT_ROOT'] .'/yiiapp/web/'
                'enablePrettyUrl' => true, //urls amigables
                'showScriptName' => false, //eliminar index.php
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,//me mantiene logeado
            'authTimeout' => 1296000, // auth expire, dos semanas en segundos
        ],
        'session' => [
            /*'class' => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 7 * 24 * 60 * 60],
            'timeout' => 3600*4, //session expire
            'useCookies' => true,*/
            'class' => 'yii\web\DbSession',
            'timeout' => 60*60*24*14, // 2 weeks
            'sessionTable' => 'tbl_session_sys',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*'response' => [
    			'formatters' => [
    				'pdf' => [
    					'class' => 'robregonm\pdf\PdfResponseFormatter',
    					'mode' => '', // Optional
    					'format' => 'A4',  // Optional but recommended. http://mpdf1.com/manual/index.php?tid=184
    					'defaultFontSize' => 0, // Optional
    					'defaultFont' => '', // Optional
    					'marginLeft' => 15, // Optional
    					'marginRight' => 15, // Optional
    					'marginTop' => 16, // Optional
    					'marginBottom' => 16, // Optional
    					'marginHeader' => 9, // Optional
    					'marginFooter' => 9, // Optional
    					'orientation' => 'Landscape', // optional. This value will be ignored if format is a string value.
    					'options' => [
    						// mPDF Variables
    						// 'fontdata' => [
    							// ... some fonts. http://mpdf1.com/manual/index.php?tid=454
    						// ]
    					]
    				],
    			]
    		],*/
        /*'megaMenu'=> [
            'class' => '\extpoint\megamenu\MegaMenu',
            'items' => [
                // sitemap
            ],
        ],*/
        /*'thumbler'=> [
            'class' => 'alexBond\thumbler\Thumbler',
            'sourcePath' => '/path/to/source/files',
            'thumbsPath' => '/path/to/resize/cache',
        ],*/
    ],
    /*Parametro generales utilizables para el sistema desde cualquier parte*/
    'params' => $params,
    'params' => [/*MODULOS PARAMETRIZADAS*/
                  'tipo_sistema' => 'sifux',//sifux - lutux
                  'tipo_empresa' => 'juridico',//fisico, juridico, dimex o nite
                  'icon-framework' => 'fa',  // Font Awesome Icon framework
                  'url_api' => 'dev.api.factun.com',
                  'apartados' => true,
                  'comisiones' => true, //para usar Modulo de Comisiones   / Yii::$app->params['parametro'];
                  'ot' => false,//Es para activar las ordenes de trabajo
                  'caja_multiple' => false,
                  'servicioRestaurante' => false,
                  /*VARIABLES PARAMETRIZADAS*/
                  'porciento_servicio' => 10,
                  'porciento_impuesto' => 13,
                  'impuesto' => 0.13,
                  'reverso_impuesto' => 1.13,
                  'limite_monto_venta_MIN' => 2500000,//PARA REPORTES D-151 VENTA CLIENTE
                  'limite_monto_comision_MIN' => 50000,//PARA REPORTES D-151 COMISION AGENTE
                  'limite_monto_compra_MIN' => 2500000,//PARA REPORTES D-151  COMPRA PROVEEDORES
                ],

    // 'backuprestore' => [
    //     'class' => '\oe\backuprestore\Module',
    //     //'layout' => '@admin-views/layouts/main', or what ever layout you use

    // ],

];
