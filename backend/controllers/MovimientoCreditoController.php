<?php

namespace backend\controllers;

use Yii;
use backend\models\MovimientoCredito;
use backend\models\search\MovimientoCreditoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Clientes;
use backend\models\FacturasDia;
use backend\models\MovimientoCobrar;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\CreditoSession;
use backend\models\NumberToLetterConverter;
use backend\models\Empresa;
use backend\models\NcPendientes;
use yii\helpers\Html;
use yii\helpers\Json;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use yii\helpers\Url;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use kartik\widgets\Select2;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use backend\models\EncabezadoCaja;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\MovimientoLibros;
use backend\models\MedioPago;
use yii\filters\AccessControl;
/**
 * MovimientoCreditoController implements the CRUD actions for MovimientoCredito model.
 */
class MovimientoCreditoController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'only' => [ 'index','view','create','update','delete',
            'addcliente','calcu_nuevo_saldo',
            'limpiar','busqueda_cliente','facturafecha',
            'inyectardatosamanipular','agrega_nota_credito', 'agregar_abono',
            'actualiza_modal_credpen','actualizar_nota_credito','deleteitem_cr_pen',
            'aplicarnotasdecredito_fact_grupal','desaplicarnotasdecredito_fact_grupal',
            'obtenereferenciacancelacion', 'consulta_modal_movi',
            'obtener_facturas_marcadas',
            'obtener_facturas_marcadas_regreso',
            'cancelacionexitosa','inyectarrecibo','consulta_recibo',
            'consultar_todos_recibos',
            'notificar_estado_cuenta','imprimir_estado_cuenta','enviar_estado_cuenta',
            'filtro_facturas_canceladas','inyectarfactura','aplicarnotascredito','cancelarvariasfacturas'
          ],
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete',
                'addcliente','calcu_nuevo_saldo',
                'limpiar','busqueda_cliente','facturafecha',
                'inyectardatosamanipular','agrega_nota_credito', 'agregar_abono',
                'actualiza_modal_credpen','actualizar_nota_credito','deleteitem_cr_pen',
                'aplicarnotasdecredito_fact_grupal','desaplicarnotasdecredito_fact_grupal',
                'obtenereferenciacancelacion', 'consulta_modal_movi',
                'obtener_facturas_marcadas',
                'obtener_facturas_marcadas_regreso',
                'cancelacionexitosa','inyectarrecibo','consulta_recibo',
                'consultar_todos_recibos',
                'notificar_estado_cuenta','imprimir_estado_cuenta','enviar_estado_cuenta',
                'filtro_facturas_canceladas','inyectarfactura','aplicarnotascredito','cancelarvariasfacturas'
              ],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all MovimientoCredito models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MovimientoCreditoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MovimientoCredito model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MovimientoCredito model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MovimientoCredito();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MovimientoCredito model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MovimientoCredito model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MovimientoCredito model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MovimientoCredito the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MovimientoCredito::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddcliente()
    {
        if(Yii::$app->request->isAjax){
            $codCliente = Yii::$app->request->post('codCliente');
            if ($modelclient = Clientes::find()->where(['=','idCliente', $codCliente])->one()){
                if($factura = FacturasDia::find()->where(['=','idCliente', $modelclient->idCliente])/*->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])*/->one()){
                    $this->cargardatos($modelclient->idCliente);
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El Cliente se ha cargado al movimiento.');

            } else {
                Yii::$app->getSession()->setFlash('info', '<span class="glyphicon glyphicon-flag"></span> <strong>Atención!</strong> No se registran facturas para este cliente.');
            }
                        Yii::$app->response->redirect(array('movimiento-credito/index'));
            }else{
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> El ID digitado no pertenece a ningun cliente registrado.');
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            }
        }
    }

    public function cargardatos($idCliente){
        $modelclient = Clientes::find()->where(['=','idCliente', $idCliente])->one();
        $montodisponible = $modelclient->montoMaximoCredito - $modelclient->montoTotalEjecutado;
        CreditoSession::setIDcliente($modelclient->idCliente);
        CreditoSession::setCliente($modelclient->nombreCompleto);//Agrego el nombre del cliente
        /*CreditoSession::setAprobado($modelclient->montoMaximoCredito);
        CreditoSession::setEjecutado($modelclient->montoTotalEjecutado);
        CreditoSession::setDisponible($montodisponible);*/
        CreditoSession::setFactura(null);
        CreditoSession::setMontofactura(null);

        CreditoSession::setRecibo(null);
        CreditoSession::setIdrecibobuscar(null);
        CreditoSession::setBanderacargar(null);

        CreditoSession::setFechadesde(null);
        CreditoSession::setFechaasta(null);

        CreditoSession::setFacturaCancelada(null);
    }

    public function actionLimpiar(){
        CreditoSession::setIDcliente(null);
        CreditoSession::setCliente(null);
        /*CreditoSession::setAprobado(null);
        CreditoSession::setEjecutado(null);
        CreditoSession::setDisponible(null);*/
        CreditoSession::setNotasCreditoPendientes(array());
        CreditoSession::setFactura(null);
        CreditoSession::setMontofactura(null);
        CreditoSession::setFechafactura(null);
        CreditoSession::setNotasCreditoVariasFacturas(array());
        CreditoSession::setMontoNC(null);
        CreditoSession::setReferencia(null);

        CreditoSession::setRecibo(null);
        CreditoSession::setIdrecibobuscar(null);
        CreditoSession::setBanderacargar(null);

        CreditoSession::setFechadesde(null);
        CreditoSession::setFechaasta(null);

        CreditoSession::setFacturaCancelada(null);

        Yii::$app->response->redirect(array('movimiento-credito/index'));
    }

    //me busca los clientes registrados
        public function actionBusqueda_cliente(){
         if(Yii::$app->request->isAjax){
                    $consultaBusqueda  = Yii::$app->request->post('valorBusqueda');

                    //Filtro anti-XSS
                    $caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
                    $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
                    $consultaBusqueda = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda);

                    $mensaje = "";

                    //Comprueba si $consultaBusqueda está seteado
                    if (isset($consultaBusqueda)) {
                        $consultaBusquedalimpia = "%".$consultaBusqueda."%";
                        $credito = 'Si';
                        //Selecciona todo de la tabla idCliente
                        //donde el nombre sea igual a $consultaBusqueda,
                        //o el apellido sea igual a $consultaBusqueda,
                        //o $consultaBusqueda sea igual a nombre + (espacio) + apellido
                        $sql = "SELECT * FROM tbl_clientes
                        WHERE idCliente LIKE :like_
                        OR nombreCompleto LIKE :like_
                        OR CONCAT(idCliente,' ',nombreCompleto)  LIKE :like_
                        AND credito = :credito
                        LIMIT 15";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":credito", $credito);
                        $command->bindParam(":like_", $consultaBusquedalimpia);
                        $consulta = $command->queryAll();
                        //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                        //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                            //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                            //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                            //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle


                            foreach($consulta as $resultados){
                                $idCliente = $resultados['idCliente'];
                                $nombreCompleto = $resultados['nombreCompleto'];
                                //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                                $accion = Html::a('', '', [
                                    'class' => 'glyphicon glyphicon-plus',
                                    'onclick'=>'agrega_cliente("'.$idCliente.'")',
                                    'data-dismiss'=>'modal',
                                    'title' => Yii::t('app', 'Cargar cliente')
                                ]);
                                //Output

                         $mensaje .='<tbody><tr>
                                      <td width="50" align="center">'.$idCliente.'</td>
                                      <td width="300">'.$nombreCompleto.'</td>
                                      <td width="60" align="center">'.$accion.'</td>
                                    </tr></tbody>';

                            }



                    //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
                }
                echo $mensaje;
            }//fin ajax
        }//fin de funcion de busqueda de productos

        //Para consicionar a que fecha se va a elegir la factura
        public function actionFacturafecha(){
            if(Yii::$app->request->isAjax){
                    $tipofecha  = Yii::$app->request->post('tipofecha');
                    if ($tipofecha == 'todas') {
                        CreditoSession::setFechafactura(null);
                    } else { CreditoSession::setFechafactura($tipofecha); }
            }
        }

        //ingresa en session los datos de la factura de credito del cliente a manipular
        public function actionInyectardatosamanipular(){
            if(Yii::$app->request->isAjax){
              $model_m_cobrar = new MovimientoCobrar();
              $factura  = Yii::$app->request->post('factura');
              $total_a_pagar  = Yii::$app->request->post('total_a_pagar');
              $idcli = CreditoSession::getIDcliente();
              CreditoSession::setFactura(null);
              CreditoSession::setMontofactura(null);
              CreditoSession::setFactura($factura);
              CreditoSession::setMontofactura($total_a_pagar);
              $factura_credito = FacturasDia::find()->where(['=','idCliente', $idcli])->andWhere('idCabeza_Factura = :idCabeza_Factura',[':idCabeza_Factura'=>$factura])->one();
              $suma_monto_movimiento = CreditoSession::getFactura() ? $model_m_cobrar->find()->where(['idCabeza_Factura'=>CreditoSession::getFactura()])->sum('monto_movimiento') : 0;
    					$nuevo_saldo = CreditoSession::getMontofactura() - $suma_monto_movimiento;
              $suma_monto_nc = CreditoSession::getFactura() ? $model_m_cobrar->find()->where(['idCabeza_Factura'=>CreditoSession::getFactura()])->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])->sum('monto_movimiento') : 0;
              $btn_movimiento_factura = '';
              if ($factura_credito->factun_id > 0) {
                $btn_movimiento_factura = Html::a('<i class="fa fa-trash" aria-hidden="true"></i> Movimientos a esta factura', ['#'], [
												'class'=>'btn btn-default',
												'id' => 'activity-nota_credito_rev',
												'data-toggle' => 'modal',
												//'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
												'data-target' => '#modalNotacredito',
												'data-url' => Url::to(['facturas-dia/movimientos', 'id' => $factura]),
												'data-pjax' => '0',
												'title' => Yii::t('app', 'Revocar factura y consultar movimiento')]);
              }

              //lista de movimientos
              $lista_movi = '';
              if (@$idCabeza_Factura = CreditoSession::getFactura()) {
                $movimientofactura = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->orderBy(['idMov' => SORT_DESC])->all();
                $lista_movi = '<table class="items table table-striped">';
                if($movimientofactura) {
                $lista_movi .= '<tbody>';
                  foreach($movimientofactura as $position => $movi) {
                    $consultar = Html::a('', ['', 'id' => $position], [
                                            'class'=>'glyphicon glyphicon-search',
                                            'id' => 'activity-index-link-agregacredpen',
                                            'data-toggle' => 'modal',
                                            'onclick'=>'javascript:consulta_modal_movi("'.$movi['idMov'].'")',
                                            'data-target' => '#modalconsultamovi',
                                            //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                            'data-pjax' => '0',
                                            'title' => Yii::t('app', 'Mostrar movimiento de crédito') ]);
                    $imprimir = Html::a('<span class=""></span>', ['movimiento-credito/index', 'id'=>$movi['idMov']], [
                                            'class'=>'fa fa-print',
                                            'id' => 'activity-index-link-report',
                                            //'target'=>'_blank',
                                            'data-toggle' => 'titulo',
                                            'onclick' => 'jsWebClientPrint.print()',
                                            'data-pjax' => '0',
                                            'title' => Yii::t('app', 'Imprimir movimiento'),
                                            ]);
                    $accion_mov = $consultar . ' ' . $imprimir;

                  $lista_movi .= '<tr>
                                    <td><font face="arial" size=2>'.$movi['idMov'].'</font></td>
                                    <td><font face="arial" size=1>'.$movi['fecmov'].'</font></td>
                                    <td><font face="arial" size=2>'.$movi['tipmov'].'</font></td>
                                    <td align="right"><font face="arial" size=2>'.number_format($movi['monto_movimiento'],2).'</font></td>
                                    <td class="actions button-column">'.$accion_mov.'</td></tr>';
                  }
                $lista_movi .= '</tbody>';
                }
                $lista_movi .= '</table>';
              }

              $arr = array('btn_movimiento_factura'=>$btn_movimiento_factura,
              'saldo_de_la_factura'=>$nuevo_saldo,
              'saldo_de_la_factura_masc'=>number_format($nuevo_saldo,2),
              'suma_monto_nc'=>number_format($suma_monto_nc,2), 'movi_credit'=>$lista_movi);
              echo json_encode($arr);
            }
        }

        public function actionCalcu_nuevo_saldo()
        {
          if(Yii::$app->request->isAjax){
              $nuevosaldo  = Yii::$app->request->post('nuevosaldo');
              echo number_format($nuevosaldo,2);
            }
        }

        //Agrega nota de credito pendiente en session
        public function actionAgrega_nota_credito(){
            if(Yii::$app->request->isAjax){
                $concepto  = Yii::$app->request->post('concepto');
                $monto_nc  = Yii::$app->request->post('monto_nc');
                //$idCliente = CreditoSession::getIDcliente();
                $idCliente = Yii::$app->request->post('idCliente');

                $modelncpend = new NcPendientes();
                //Abrimos el contenido de notas de credito
              /*  $notascr_pen = CreditoSession::getNotasCreditoPendientes();
                $_POST['concepto'] = $concepto;
                $_POST['monto_nc'] = $monto_nc;
                $_POST['fecha'] = date("d-m-Y h:i:s",time());
                $_POST['idCliente'] = $idCliente;
                $notascr_pen[] = $_POST;
                CreditoSession::setNotasCreditoPendientes($notascr_pen); */
                $modelncpend->concepto = $concepto;
                $modelncpend->monto = floatval($monto_nc);
                $modelncpend->fecha = date("Y-m-d H:i:s",time());
                $modelncpend->idCliente = $idCliente;
                $modelncpend->save();
            }
        }

        //muestra el formulario que me va actualizar de la nota de credito
        public function actionActualiza_modal_credpen(){
            if(Yii::$app->request->isAjax){
                $position = Yii::$app->request->post('position');
                $modelncpend = NcPendientes::find()->where(['idNCPendiente'=>$position])->one();
                //$notascr_pen = CreditoSession::getNotasCreditoPendientes();
                //if($notascr_pen)
                //foreach ($notascr_pen as $positionall => $notascr)
                //{
                   // if ($positionall==$position) {
                        //echo $producto[$positionall]['codProdServicio'];
                       /* $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$producto[$positionall]['codProdServicio']])->one();
                        $movicob = new MovimientoCobrar();*/

                        //$form = ActiveForm::begin([ 'options' => ['name'=>'caja'], 'action'=>['/compras-inventario/actualiza_modalprod_listo'] ]);
                        $concepto = $modelncpend->concepto; //$notascr_pen[$positionall]['concepto']
                        $monto = $modelncpend->monto;//$notascr_pen[$positionall]['monto_nc']
                        $formulario = '

                            <label>Cliente:</label><br>'.
                            Html::input('text', '', ' '.CreditoSession::getIDcliente() , ['size' => '3', 'class' => '', 'id' => 'idCliente', 'placeholder' =>' ID Cliente', 'readonly' => true, 'onchange' => '']).
                            ' '.Html::input('text', '', ' '.CreditoSession::getCliente() , ['class' => '', 'id' => 'Cliente', 'placeholder' =>' Cliente', 'readonly' => true, 'onchange' => '']).'
                            <br><br><label>Concepto:</label><br>
                            <textarea class="form-control" rows="5" id="concepto_u" >'.$concepto.'</textarea>
                            <br><label>Monto: </label>'.
                            Html::input('text', '',$monto , ['size' => '8', 'class' => 'form-control', 'id' => 'monto_nc_u', 'placeholder' =>' Monto', 'readonly' => false, 'onchange' => '', "onkeypress"=>"return isNumberDe(event)"]).'
                            <input type="hidden" name="position" id="position_u" value="'.$modelncpend->idNCPendiente.'">';

                        echo $formulario;
                        //ActiveForm::end();//El final del activefrom debe de estar despues del echo para que funcione el submit
                    //}//fin if
                //}
            } else echo 'No tenemos resultados';
        }

        //Agrega nota de credito pendiente en session
        public function actionActualizar_nota_credito(){
            if(Yii::$app->request->isAjax){
                $concepto  = Yii::$app->request->post('concepto');
                $monto_nc  = floatval(Yii::$app->request->post('monto_nc'));
                $position_vista  = intval(Yii::$app->request->post('position'));

                $sql = "UPDATE tbl_nc_pendientes SET monto = :monto_nc, concepto = :concepto WHERE idNCPendiente = :idNCPendiente";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":monto_nc", $monto_nc);
                    $command->bindParam(":concepto", $concepto);
                    $command->bindValue(":idNCPendiente", $position_vista);
                    $command->execute();
                /*$notascr_pen = CreditoSession::getNotasCreditoPendientes();
                if ($notascr_pen)
                    foreach ($notascr_pen as $position => $notascr){
                        if ($position == $position_vista) {
                            $notascr_pen[$position]['concepto'] = $concepto;
                            $notascr_pen[$position]['monto_nc'] = $monto_nc;
                        }
                        CreditoSession::setNotasCreditoPendientes($notascr_pen);
                    }*/
               /* $idCliente = CreditoSession::getIDcliente();

                //Abrimos el contenido de notas de credito
                $notascr_pen = CreditoSession::getNotasCreditoPendientes();

                $_POST['concepto'] = $concepto;
                $_POST['monto_nc'] = $monto_nc;
                $_POST['fecha'] = date("d-m-Y h:i:s",time());
                $_POST['idCliente'] = $idCliente;
                $notascr_pen[] = $_POST;

                CreditoSession::setNotasCreditoPendientes($notascr_pen); */
            }
        }

        //elimina nota de credito seleccionada pendiente en session
        public function actionDeleteitem_cr_pen($id)
        {
            \Yii::$app->db->createCommand()->delete('tbl_nc_pendientes', 'idNCPendiente = '.(int) $id)->execute();
            //Descodificamos el array JSON
           /* $notascr_pen = JSON::decode(Yii::$app->session->get('creditopendiente'), true);
            //Eliminamos el atributo pasado por parámetro
            unset($notascr_pen[$id]);
            //Volvemos a codificar y guardar el contenido
            Yii::$app->session->set('creditopendiente', JSON::encode($notascr_pen));
            */
            Yii::$app->response->redirect(array('movimiento-credito/index'));
        }

        //esta fincion aplica las notas de credito a una factura y las almacena en base de datos
        public function actionAplicarnotasdecredito($id)
        {
            $idCabeza_Factura = CreditoSession::getFactura();
            $bandera = 'No cancelada';
            //Con este if compruebo que haya una factura cargada para aplicar la nota de credito
            if($idCabeza_Factura){
                $modelfactura = MovimientoCredito::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one(); //Obtengo la factura
                $saldo_de_la_factura = $modelfactura->total_a_pagar; //extraigo el total a pagar de la factura
                //$notascr_pen = CreditoSession::getNotasCreditoPendientes(); //Obtengo las notas de credito cargadas en session
                //Obtengo las notas de credito cargadas en base de datos
                $notascr_pen = NcPendientes::find()->where(['idCliente'=>$id])->orderBy(['idNCPendiente' => SORT_DESC])->all();
                if($notascr_pen) { //Compruebo que hallan notas cargadas

                $monto = 0; //asigno el monto en 0
                foreach($notascr_pen as $not_pen) { //con esto sumo lo que hay en el total de montos de notas de credito
                    $monto += $not_pen['monto']; //para luego compararlo con el monto de la factura
                    }
                $model_m_co = new MovimientoCobrar(); //con estos procesos puedo obtener el saldo pendiente de esta factura
                $suma_monto_movimiento = $model_m_co->find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->sum('monto_movimiento');
                $saldo_fac = CreditoSession::getMontofactura() - $suma_monto_movimiento;
                //al comparar valido que las notas no superen el monto de la factura
                //lo interesante es que necesito hacer una comparacion muy exacta cuando se trata de montos iguales, por eso uso number_format()
                //como se ve a continuacion ya que en algun momento dado despues de varios procesos por abono o NC puede pasar solo por montos iguales
                //por el contrario el monto siempre será mayor al saldo de la factura
                if ( (number_format($saldo_fac,2) == number_format($monto,2)) || ($saldo_fac > $monto) ) {

                    //una ves confirmo que las notas no superan el monto de la factura buelvo a recorrer las notas
                    //para aplicarlas cada una a la factura
                    foreach($notascr_pen as $model_m_cobrar => $notascr) { //Recorro cada una de la notas cargadas
                        $model_m_cobrar = new MovimientoCobrar(); //obtengo la instancia MovimientoCobrar por donde voy a almacenar las notas de credito

                        //Obtengo la suma del monto_movimiento que es el monto que se ha aplicado historicamente
                        $command = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_cobrar WHERE idCabeza_Factura = " . $idCabeza_Factura);
                        $suma_monto_movimiento = $command->queryScalar(); //ingreso la suma del monto en una variable


                        //$suma_monto_movimiento = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->sum('monto_movimiento');
                        //Asigno la nota de credito en los campos que corresponden a la tabla de la base de datos
                        $monto_anterior = $saldo_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                        $saldo_pendiente = $monto_anterior - $notascr['monto']; //le resto el monto de la nota de credito con el monto anterior para saber el monto pendiente
                        $model_m_cobrar->fecmov = $notascr['fecha'];
                        $model_m_cobrar->tipmov = 'NC';
                        $model_m_cobrar->concepto = $notascr['concepto'];
                        $model_m_cobrar->monto_anterior = $monto_anterior;
                        $model_m_cobrar->monto_movimiento = $notascr['monto'];
                        $model_m_cobrar->saldo_pendiente = $saldo_pendiente;
                        $model_m_cobrar->usuario = Yii::$app->user->identity->username;
                        $model_m_cobrar->idCabeza_Factura = $idCabeza_Factura;
                        $model_m_cobrar->idCliente = CreditoSession::getCliente();
                        $model_m_cobrar->idCli = CreditoSession::getIDcliente();
                        if($model_m_cobrar->save()) //Guardo la nota de credito
                        {
                            //obtenemos datos del cliente
                            $modelcliente = Clientes::find()->where(['idCliente'=>$modelfactura->idCliente])->one();
                            $nuevacantidad = $modelcliente->montoTotalEjecutado - $notascr['monto'];
                            //Creo el nuevo recibo por NC
                            $recibo = new Recibos();
                            $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
                            $recibo->monto = $notascr['monto'];
                            $recibo->cliente = $modelfactura->idCliente;
                            $recibo->usuario = Yii::$app->user->identity->username;
                            $recibo->tipoRecibo = 'NC';
                            $recibo->saldo_cuenta = $nuevacantidad;
                            //Creo el detalle del recibo
                            if ($recibo->save()) {
                                $rec_deta = new RecibosDetalle();//instancia para almacenar la nota de credito
                                $rec_deta->idRecibo = $recibo->idRecibo;
                                $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                                $rec_deta->tipo = 'NC';
                                $rec_deta->referencia = 'Factura: '. $idCabeza_Factura . ' - ' . $notascr['concepto'];
                                $rec_deta->fecha = date("Y-m-d H:i:s",time());
                                $rec_deta->monto = $notascr['monto'];
                                $rec_deta->save();
                            }
                            //actualizamos el nuevo monto ejecutado del cliente luego del movimiento
                            $idCliente = $modelfactura->idCliente;
                            $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :nuevacantidad WHERE idCliente = :idCliente";
                            $command = \Yii::$app->db->createCommand($sql);
                            $command->bindParam(":nuevacantidad", $nuevacantidad);
                            $command->bindValue(":idCliente", $idCliente);
                            $command->execute();
                            $sql_s = "UPDATE tbl_encabezado_factura SET credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idCabeza_Factura";
                                $command_s = \Yii::$app->db->createCommand($sql_s);
                                $command_s->bindParam(":credi_saldo", $saldo_pendiente);
                                $command_s->bindValue(":idCabeza_Factura", $idCabeza_Factura);
                                $command_s->execute();
                            //eliminamos la NC pendiente en BD
                            \Yii::$app->db->createCommand()->delete('tbl_nc_pendientes', 'idNCPendiente = '.(int) $notascr['idNCPendiente'])->execute();
                            $bandera = 'Aplicada';
                        }


                    }//fin foreash
                    //vuelvo a condicionar pero esta ves si el monto es igual al saldo para así cancelar la factura
                    if (number_format($monto,2) == number_format($saldo_fac,2)) {
                        $estadoFactura = 'Cancelada';
                        $fecha_final = date("Y-m-d");
                        $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estadoFactura, fecha_final = :fecha_final, credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idCabeza_Factura";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":estadoFactura", $estadoFactura);
                        $command->bindValue(":fecha_final", $fecha_final);
                        $command->bindParam(":credi_saldo", $saldo_pendiente);
                        $command->bindValue(":idCabeza_Factura", $idCabeza_Factura);
                        $command->execute();
                        CreditoSession::setFactura(null);
                        CreditoSession::setMontofactura(null);
                        $bandera = 'Cancelada';
                    }
                    CreditoSession::setNotasCreditoPendientes(array()); //borro las notas de credito que estan en session
                    if ($bandera == 'Cancelada') {
                          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Esta Factura ha sido cancelada.');
                        } elseif ($bandera == 'Aplicada') {
                           Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Notas de credito aplicadas correctamente.');
                        } else {
                          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Datos incorrectos.');
                        }
                        Yii::$app->response->redirect(array('movimiento-credito/index'));
            } else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Las notas de crédito superan el saldo pendiente de la factura.');
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            }
                } else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> No hay notas de credito pendientes de aplicar.');
                    Yii::$app->response->redirect(array('movimiento-credito/index'));
                }
            } else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Seleccione primero una factura para aplicar las notas de crédito.');
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            }

        }

        //esta funcion carga las notas de credito y el monto de estas sin almacenar en base de datos
        //(en un estado de espera por si hay alguna otra nota antes de aplicarla a la lista de facturas
        //que se van a cancelar)
        public function actionAplicarnotasdecredito_fact_grupal()
        {
            $idCliente = $_GET['idCl'];
            $totlcanc = floatval($_GET['totlcanc']);
            $notascr_pen = NcPendientes::find()->where(['idCliente'=>$idCliente])->orderBy(['idNCPendiente' => SORT_DESC])->all();
                //Compruebo y obtengo las notas de credito cargadas en session y si hay facturas a cancelar
                if(($notascr_pen) && ($totlcanc)) { //Compruebo que hallan notas cargadas
                    $suma_x_notas = 0;
                    $nc_vf = CreditoSession::getNotasCreditoVariasFacturas();
                    foreach($notascr_pen as $notas){//Recorro las notas de credito
                        $suma_x_notas += $notas['monto'];
                    }
                    //Comprobamos que el monto de las notas de credito no superen el monto total de las facturas
                    if ((number_format($totlcanc,2) == number_format($suma_x_notas,2)) || ($totlcanc > $suma_x_notas)) {
                        foreach($notascr_pen as $notas){//Recorro las notas de credito
                            $_POST['fecha_'] = $notas['fecha'];
                            $_POST['concepto_'] = $notas['concepto'];
                            $_POST['monto_nc_'] = $notas['monto'];

                            $nc_vf[] = $_POST;
                            \Yii::$app->db->createCommand()->delete('tbl_nc_pendientes', 'idNCPendiente = '.(int) $notas['idNCPendiente'])->execute();
                        }



                        CreditoSession::setNotasCreditoVariasFacturas($nc_vf);
                        $suma_nc_total_fact_can = CreditoSession::getMontoNC() ? CreditoSession::getMontoNC() : 0;

                        CreditoSession::setMontoNC($suma_x_notas + $suma_nc_total_fact_can);
                        CreditoSession::setNotasCreditoPendientes(array());
                        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Notas de crédito cargadas.');
                        Yii::$app->response->redirect(array('movimiento-credito/index'));
                    } else {//fin if para comprovar que el monto de nc no sea mayor al saldo
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> El monto de notas de crédito superan al monto por cancelar.');
                        Yii::$app->response->redirect(array('movimiento-credito/index'));
                    }

                } else {//fin if $notascr_pen

                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> No hay notas de crédito pendientes de aplicar o no hay ninguna factura en lista para cancelar.');
                    Yii::$app->response->redirect(array('movimiento-credito/index'));
                }

        }

        //esta funcion devuelve las notas de credito para editarlas eliminando tambien el monto
        public function actionDesaplicarnotasdecredito_fact_grupal()
        {
            $idCliente = $_GET['idCl'];
            $sum_nc = floatval($_GET['sum_nc']);
                //Compruebo y obtengo las notas de credito cargadas en session y si hay facturas a cancelar
                if((@$notascr_pen = CreditoSession::getNotasCreditoVariasFacturas()) && ($sum_nc)) { //Compruebo que hallan notas cargadas

                    //$nc_p = CreditoSession::getNotasCreditoPendientes();

                    foreach($notascr_pen as $modelncpendi => $notas){//Recorro las notas de credito

                        $modelncpendi = new NcPendientes();
                        $modelncpendi->idCliente = $idCliente;
                        $modelncpendi->fecha = date("Y-m-d H:i:s",time());
                        $modelncpendi->concepto = $notas['concepto_'];
                        $modelncpendi->monto = floatval($notas['monto_nc_']);
                        if ($modelncpendi->save()) {
                            CreditoSession::setMontoNC(null);
                            CreditoSession::setNotasCreditoVariasFacturas(array());
                            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Notas de crédito devueltas correctamente.');
                            Yii::$app->response->redirect(array('movimiento-credito/index'));
                        }else{
                            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Se perdió.');
                            Yii::$app->response->redirect(array('movimiento-credito/index'));
                        }

                       /* $_POST['fecha'] = date("d-m-Y h:i:s",time());
                        $_POST['concepto'] = $notas['concepto_'];
                        $_POST['monto_nc'] = $notas['monto_nc_'];
                        $nc_p[] = $_POST;*/
                    }

                        //CreditoSession::setNotasCreditoPendientes($nc_p);
                        //$suma_nc_total_fact_can = CreditoSession::getMontoNC() ? CreditoSession::getMontoNC() : 0;


                } else {//fin if $notascr_pen

                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> No hay notas de crédito cargadas a devolver.');
                    Yii::$app->response->redirect(array('movimiento-credito/index'));
                }

        }

        //me carga en session la referencia
        public function actionObtenereferenciacancelacion(){
            if(Yii::$app->request->isAjax){
                $referenciacance  = Yii::$app->request->post('referenciacance');
                CreditoSession::setReferencia($referenciacance);
            }
        }

        //Esta funcion permite cancelar todas las facturas listadas en "Facturas a Cancelar" tomando
        //en cuenta las notas de credito que se le cargaron generando asi el recibo respectivo
        //tanto de la o las notas como la cancelacion de las facturas
        public function actionCancelarvariasfacturas(){
            if(Yii::$app->request->isAjax){
            $idCliente = intval(Yii::$app->request->post('idCl'));//intval($_GET['idCl']);
            $totlcanc = floatval(Yii::$app->request->post('totlcanc'));//floatval($_GET['totlcanc']);
            $totlnc = floatval(Yii::$app->request->post('totlnc'));//floatval($_GET['totlnc']);
            $suma_saldo_total_can = floatval(Yii::$app->request->post('suma_saldo_total_can'));//floatval($_GET['suma_saldo_total_can']);
            $tipo_pago = Yii::$app->request->post('tipo');
            $ddl_entidad = Yii::$app->request->post('ddl_entidad');
            $tipoCuentaBancaria = Yii::$app->request->post('tipoCuentaBancaria');
            $fechadepo = Yii::$app->request->post('fechadepo');
            $comprobacion = Yii::$app->request->post('comprobacion');

            $regresar_monto_cliente = $totlnc + $totlcanc;
            $notascr_pen_fa = CreditoSession::getNotasCreditoVariasFacturas();
            if ($suma_saldo_total_can) {
                # code...

            //Obtengo datos del cliente
            $modelclient = Clientes::find()->where(['=','idCliente', $idCliente])->one();
            $nuevoMontoTotalEjecutado = $modelclient->montoTotalEjecutado - $regresar_monto_cliente;
            //Creo el nuevo recibo
            $recibo = new Recibos();
            $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
            $recibo->monto = $totlcanc;
            $recibo->cliente = $idCliente;
            $recibo->usuario = Yii::$app->user->identity->username;
            $recibo->tipoRecibo = 'CF';//Cancelacion de factura
            $recibo->saldo_cuenta = $nuevoMontoTotalEjecutado;
            if ($recibo->save()) {
                //Ingreso al recibo las notas de credito que se les hicieron a las facturas
                    if ($notascr_pen_fa) {//compruebo que hayan notas de credito
                    foreach($notascr_pen_fa as $model_m_cobrar => $notascr) {

                        $model_m_cobrar = new MovimientoCobrar(); //obtengo la instancia MovimientoCobrar por donde voy a almacenar las notas de credito
                        $model_m_cobrar->fecmov = $notascr['fecha_'];
                        $model_m_cobrar->tipmov = 'NC';
                        $model_m_cobrar->concepto = $notascr['concepto_'];
                        //$model_m_cobrar->monto_anterior = $monto_anterior;
                        $model_m_cobrar->monto_movimiento = $notascr['monto_nc_'];
                        //$model_m_cobrar->saldo_pendiente = $saldo_pendiente;
                        $model_m_cobrar->usuario = Yii::$app->user->identity->username;
                        //$model_m_cobrar->idCabeza_Factura = $idCabeza_Factura;
                        $model_m_cobrar->idCliente = CreditoSession::getCliente();
                        $model_m_cobrar->idCli = CreditoSession::getIDcliente();
                        if($model_m_cobrar->save()) //Guardo la nota de credito
                        {
                            $rec_deta = new RecibosDetalle();
                            $rec_deta->idRecibo = $recibo->idRecibo;
                            $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                            $rec_deta->tipo = 'NC';
                            $rec_deta->referencia = CreditoSession::getReferencia() ? CreditoSession::getReferencia() : 'CANCELACIÓN';
                            $rec_deta->fecha = date("Y-m-d H:i:s",time());
                            $rec_deta->save();
                        }
                    }//fin foreach de notas
                }//fin comprovacion de notas de credito
                //luego ingreso las facturas al mismo recibo
                $facturas = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("credi_atencion = :credi_atencion", [":credi_atencion"=>'1'])->all();
                foreach ($facturas as $model_fact_rec_deta => $factura) {
                    //ingresamos el detalle de recibo
                    $model_fact_rec_deta = new RecibosDetalle();
                    $model_fact_rec_deta->idRecibo = $recibo->idRecibo;
                    $model_fact_rec_deta->idDocumento = $factura['idCabeza_Factura'];
                    $model_fact_rec_deta->tipo = 'FA';
                    $model_fact_rec_deta->referencia = CreditoSession::getReferencia() ? CreditoSession::getReferencia() : 'CANCELACIÓN';
                    $model_fact_rec_deta->fecha = date("Y-m-d H:i:s",time());
                    $model_fact_rec_deta->monto = $factura['credi_saldo'];
                    if ($model_fact_rec_deta->save()) {
                        //actualizamos tbl_encabezado_factura
                        $credi_atencion = '';
                        $estadoFactura = 'Cancelada';
                        $credi_saldo = 0.00;
                        $fecha_final = date("Y-m-d");
                        $idCabeza_Factura = $factura['idCabeza_Factura'];
                        $sql = "UPDATE tbl_encabezado_factura SET credi_atencion = :credi_atencion, credi_saldo = :credi_saldo, estadoFactura = :estadoFactura, fecha_final = :fecha_final WHERE idCabeza_Factura = :idCabeza_Factura";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":credi_atencion", $credi_atencion);
                        $command->bindParam(":credi_saldo", $credi_saldo);
                        $command->bindValue(":fecha_final", $fecha_final);
                        $command->bindParam(":estadoFactura", $estadoFactura);
                        $command->bindValue(":idCabeza_Factura", $idCabeza_Factura);
                        $command->execute();

                    }//fin $model_fact_rec_deta->save()
                }//fin foreach
                //agragamos el medio de pago
                $tbl_medio_pago = new MedioPago();//$recibo->
                $tbl_medio_pago->idCabeza_factura = $recibo->idRecibo;
                $tbl_medio_pago->idForma_Pago = $tipo_pago;
                $tbl_medio_pago->tipo_del_medio = $tipo_pago != 1 ? $ddl_entidad : '';
                $tbl_medio_pago->comprobacion = $tipo_pago != 1 ? $comprobacion : '';
                $tbl_medio_pago->cuenta_deposito = $tipo_pago != 1 ? $tipoCuentaBancaria : '';
                $tbl_medio_pago->fecha_deposito = date("Y-m-d");
                $tbl_medio_pago->estado = 'Cancelación';
                $tbl_medio_pago->monto = $totlcanc;
                $tbl_medio_pago->save();
                if ($tipo_pago == 4) {//Si el pago es por deposito bancario se ingresa aqui para dirigir a la funcion que creara un movimento en libro
                  $idEntidad_financiera = $ddl_entidad;
                  $numero_cuenta = $tipoCuentaBancaria;
                  $fechadepo = date('Y-m-d', strtotime( $fechadepo ));
                  $modelcuentabancaria = CuentasBancarias::find()->where(['idEntidad_financiera'=>$idEntidad_financiera])->andWhere("numero_cuenta = :numero_cuenta", [":numero_cuenta"=>$numero_cuenta])->one();

                  $this->crearmovimientolibro($modelcuentabancaria->idBancos, $comprobacion, $idCabeza_Factura, $totlcanc, $fechadepo);
                  $movimiento = 'si';
                }
            //Actualizamos cliente
            $sql2 = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
            $command2 = \Yii::$app->db->createCommand($sql2);
            $command2->bindParam(":montoTotalEjecutado", $nuevoMontoTotalEjecutado);
            $command2->bindValue(":idCliente", $idCliente);
            $command2->execute();

            CreditoSession::setMontoNC(null);
            CreditoSession::setReferencia(null);
            CreditoSession::setNotasCreditoVariasFacturas(array());
            CreditoSession::setFactura(null);
            CreditoSession::setMontofactura(null);
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Factura(s) Cancelada(s) correctamente.');
            //Yii::$app->response->registerScript('imprimir/imprimir_recibo', 'jQuery("a").attr("target","_blank")', ['idRecibo' => $recibo->idRecibo]);
            //echo $this->render('imprimir/imprimir_facturas', ['idRecibo' => $recibo->idRecibo]);
            //Yii::$app->response->redirect(array('movimiento-credito/index'));
            //Yii::$app->response->redirect(array('movimiento-credito/imprimir_facturas', 'idRecibo'=>$recibo->idRecibo));
            Yii::$app->response->redirect(array('movimiento-credito/index','idRecibof'=>$recibo->idRecibo));
            //echo $recibo->idRecibo;
            }// fin if guargar recibo
            else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> No se ha procesado la acción, problemas al almacenar recibos.');
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            }
            }//fin if de comprobacion de total a cancelar
            else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> No se ha procesado la acción, No hay un total a cancelar.');
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            }
        }//fin funcion ajax
        }//fin funsion

        public function actionConsulta_modal_movi(){
            if(Yii::$app->request->isAjax){
                $idMov  = Yii::$app->request->post('idMov');
                $movimiento = MovimientoCobrar::find()->where(['idMov'=>$idMov])->one();
                $facturaAplicada = $movimiento->idCabeza_Factura == '' ? 'Varias' : $movimiento->idCabeza_Factura;
                $mostrarmovimiento = '
                <label>Factura aplicada: </label> '.$facturaAplicada.'<br>
                <label>ID Movimiento: </label> '.$movimiento->idMov.'<br>
                <label>Fecha / hora: </label> '.$movimiento->fecmov.'<br>
                <label>Tipo de movimiento: </label> '.$movimiento->tipmov.'<br>
                <label>Concepto: </label> '.$movimiento->concepto.'<br>
                <label>Monto anterior: </label> ₡'.number_format($movimiento->monto_anterior,2).'<br>
                <label>Monto ejecutado: </label> ₡'.number_format($movimiento->monto_movimiento,2).'<br>
                <label>Usuario que registró: </label> '.$movimiento->usuario.'<br>
                ';
                echo $mostrarmovimiento;
            }
        }



        //esta funcion me crea los abonos y me crea una nuevo movimiento en libro para los abonos por deposito bancario
        public function actionAgregar_abono(){
            if(Yii::$app->request->isAjax){
                $pago  = Yii::$app->request->post('pago');
                $referencia  = Yii::$app->request->post('referencia');
                $tipo_pago_abono = Yii::$app->request->post('tipo_pago_abono');
                $idCabeza_Factura = CreditoSession::getFactura();
                $movimiento = 'no';


                //Con este if compruebo que haya una factura cargada para aplicar la nota de credito
                if($idCabeza_Factura){
                    $modelfactura = MovimientoCredito::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one(); //Obtengo la factura
                    $saldo_de_la_factura = $modelfactura->total_a_pagar; //extraigo el total a pagar de la factura


                        $model_m_cobrar = new MovimientoCobrar(); //obtengo la instancia MovimientoCobrar por donde voy a almacenar las notas de credito

                        //Obtengo la suma del monto_movimiento que es el monto que se ha aplicado historicamente
                        $command = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_cobrar WHERE idCabeza_Factura = " . $idCabeza_Factura);
                        $suma_monto_movimiento = $command->queryScalar(); //ingreso la suma del monto en una variable


                        //$suma_monto_movimiento = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->sum('monto_movimiento');
                        //Asigno la nota de credito en los campos que corresponden a la tabla de la base de datos

                        $monto_anterior = $saldo_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                        $saldo_pendiente = $monto_anterior - $pago; //le resto el monto de la nota de credito con el monto anterior para saber el monto pendiente
                        //al comparar valido que las notas no superen el monto de la factura
                        //lo interesante es que necesito hacer una comparacion muy exacta cuando se trata de montos iguales, por eso uso number_format()
                        //como se ve a continuacion ya que en algun momento dado despues de varios procesos por abono o NC solo por montos iguales puede pasar
                        //por el contrario el monto siempre será mayor al saldo de la factura
                        if ((number_format($pago,2) == number_format($monto_anterior,2)) || ($pago < $monto_anterior)) {
                        $bandera_cancelada = false;
                        $idImprimir = 0;
                        $model_m_cobrar->fecmov = date("Y-m-d H:i:s",time());
                        $model_m_cobrar->tipmov = 'AB';
                        $model_m_cobrar->concepto = $referencia;
                        $model_m_cobrar->monto_anterior = $monto_anterior;
                        $model_m_cobrar->monto_movimiento = $pago;
                        $model_m_cobrar->saldo_pendiente = $saldo_pendiente;
                        $model_m_cobrar->usuario = Yii::$app->user->identity->username;
                        $model_m_cobrar->idCabeza_Factura = $idCabeza_Factura;
                        $model_m_cobrar->idCliente = CreditoSession::getCliente();
                        $model_m_cobrar->idCli = CreditoSession::getIDcliente();
                        if($model_m_cobrar->save()) //Guardo la nota de credito
                        {
                            $modelcliente = Clientes::find()->where(['idCliente'=>$modelfactura->idCliente])->one();
                            $nuevacantidad = $modelcliente->montoTotalEjecutado - $pago;
                            //Creo el nuevo recibo por NC
                            $recibo = new Recibos();
                            $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
                            $recibo->monto = $pago;
                            $recibo->cliente = $modelfactura->idCliente;
                            $recibo->usuario = Yii::$app->user->identity->username;
                            $recibo->tipoRecibo = 'AB';//tipo abono
                            $recibo->saldo_cuenta = $nuevacantidad;//saldo de la cuenta hasta ahora
                            //Creo el detalle del recibo
                            if ($recibo->save()) {
                                $idImprimir = $recibo->idRecibo;
                                $rec_deta = new RecibosDetalle();//instancia para almacenar el recibo del abono
                                $rec_deta->idRecibo = $recibo->idRecibo;
                                $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                                $rec_deta->tipo = 'AB';
                                $rec_deta->referencia = 'Factura: '. $idCabeza_Factura . ' - ' . $referencia;
                                $rec_deta->fecha = date("Y-m-d H:i:s",time());
                                $rec_deta->monto = $pago;
                                $rec_deta->save();
                            }
                            //actualizamos el nuevo monto ejecutado del cliente luego del movimiento
                            $idCliente = $modelfactura->idCliente;
                            $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :nuevacantidad WHERE idCliente = :idCliente";
                            $command = \Yii::$app->db->createCommand($sql);
                            $command->bindParam(":nuevacantidad", $nuevacantidad);
                            $command->bindValue(":idCliente", $idCliente);
                            $command->execute();

                            $sql_s = "UPDATE tbl_encabezado_factura SET credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idCabeza_Factura";
                                $command_s = \Yii::$app->db->createCommand($sql_s);
                                $command_s->bindParam(":credi_saldo", $saldo_pendiente);
                                $command_s->bindValue(":idCabeza_Factura", $idCabeza_Factura);
                                $command_s->execute();
                            if(number_format($pago,2) == number_format($monto_anterior,2)){
                                $estadoFactura = 'Cancelada';
                                $fecha_final = date("Y-m-d");
                                $sql2 = "UPDATE tbl_encabezado_factura SET estadoFactura = :estadoFactura, fecha_final = :fecha_final, credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idCabeza_Factura";
                                $command2 = \Yii::$app->db->createCommand($sql2);
                                $command2->bindParam(":estadoFactura", $estadoFactura);
                                $command2->bindParam(":credi_saldo", $saldo_pendiente);
                                $command2->bindValue(":idCabeza_Factura", $idCabeza_Factura);
                                $command2->bindValue(":fecha_final", $fecha_final);
                                $command2->execute();
                                CreditoSession::setFactura(null);
                                CreditoSession::setMontofactura(null);
                                $bandera_cancelada = true;

                            }
                            //$this->actionImprimir_movimiento_credito($idCabeza_Factura);
                        }

                        $fechadepo = date('Y-m-d');
                        if ($tipo_pago_abono == 4) {//Si el pago es por deposito bancario se ingresa aqui para dirigir a la funcion que creara un movimento en libro
                          $idEntidad_financiera = Yii::$app->request->post('ddl_entidad');
                          $numero_cuenta = Yii::$app->request->post('tipoCuentaBancaria');
                          $fechadepo = Yii::$app->request->post('fechadepo');
                          $comprobacion = Yii::$app->request->post('comprobacion');
                          $modelcuentabancaria = CuentasBancarias::find()->where(['idEntidad_financiera'=>$idEntidad_financiera])->andWhere("numero_cuenta = :numero_cuenta", [":numero_cuenta"=>$numero_cuenta])->one();

                          $this->crearmovimientolibro($modelcuentabancaria->idBancos, $comprobacion, $idCabeza_Factura, $pago, $fechadepo);
                          $movimiento = 'si';
                        }
                          $tipo_del_medio = Yii::$app->request->post('ddl_entidad');
                          $comprobacion = Yii::$app->request->post('comprobacion');
                          $cuenta = $tipo_pago_abono==2 ? '' : Yii::$app->request->post('tipoCuentaBancaria');

                          $estadopago = 'Abono';
                          /*$tbl_medio_pago = "INSERT INTO tbl_medio_pago (idCabeza_factura, idForma_Pago, tipo_del_medio, comprobacion, cuenta_deposito, estado, fecha_deposito)
                                   VALUES (:idCabeza_factura, :idForma_Pago, :tipo_del_medio, :comprobacion, :cuenta_deposito, :estado, :fecha_deposito)";
                          $medio_pago = \Yii::$app->db->createCommand($tbl_medio_pago);
                          $medio_pago->bindParam(":idCabeza_factura", $idCabeza_Factura);
                          $medio_pago->bindParam(":idForma_Pago", $tipo_pago_abono);
                          $medio_pago->bindParam(":tipo_del_medio", $tipo_del_medio);
                          $medio_pago->bindParam(":comprobacion", $comprobacion);
                          $medio_pago->bindParam(":cuenta_deposito", $cuenta);
                          $medio_pago->bindParam(":fecha_deposito", $fechadepo);
                          $medio_pago->bindParam(":estado", $estadopago);
                          //$medio_pago->bindValue(":id", $id);
                          $medio_pago->execute();*/
                          $tbl_medio_pago = new MedioPago();
                          $tbl_medio_pago->idCabeza_factura = $idCabeza_Factura;
                          $tbl_medio_pago->idForma_Pago = $tipo_pago_abono;
                          $tbl_medio_pago->tipo_del_medio = $tipo_pago_abono != 1 ? $tipo_del_medio : '';
                          $tbl_medio_pago->comprobacion = $tipo_pago_abono != 1 ? $comprobacion : '';
                          $tbl_medio_pago->cuenta_deposito = $tipo_pago_abono != 1 ? $cuenta : '';
                          $tbl_medio_pago->fecha_deposito = date('Y-m-d');
                          $tbl_medio_pago->estado = $estadopago;
                          $tbl_medio_pago->monto = $pago;
                          if ($tbl_medio_pago->save()) {//actualizamos el movumiento por cobrar (abono) agregandole el medio de pago
                            $m_cobrar = $model_m_cobrar->find()->where(['idMov'=>$model_m_cobrar->idMov])->one();
                            $m_cobrar->idMetPago = $tbl_medio_pago->idMetPago;
                            $m_cobrar->save();
                          }


                        $not_adicional = $movimiento=='no' ? '' : 'También se ha creado un nuevo movimiento en libro.';
                        if ($bandera_cancelada == true) {
                            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Esta Factura ha sido cancelada. '.$not_adicional);
                            Yii::$app->response->redirect(array('movimiento-credito/index','idRecibof'=>$idImprimir));
                        } else {
                          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Pago por abono aplicado correctamente a la factura. '.$not_adicional);
                          Yii::$app->response->redirect(array('movimiento-credito/index','idRecibo'=>$idImprimir));
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> No se ha procesado el pago porque el monto digitado supera el saldo pendiente de pagar.');
                        Yii::$app->response->redirect(array('movimiento-credito/index'));
                    }

                } else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Seleccione primero una factura para aplicar un abono.');
                    Yii::$app->response->redirect(array('movimiento-credito/index'));
                }
            }
        }

        //Esta funcion me imprime los abonos y notas de crédito
        public function actionImprimir_movimiento_credito($id){
            return $this->render('imprimir/imprimir_movimiento_credito', ['id' => $id]);
            //https://github.com/mike42/escpos-php/issues/14
            //https://github.com/mike42/escpos-php
            //https://social.technet.microsoft.com/Forums/es-ES/ed907f67-f5db-40f4-b309-c6cf8e7ca4ff/agregar-impresora-de-red-por-puerto-local?forum=wspfes
            //net use lpt1: \\nombrepc\impresora
/*
            $MovimientoCobrar = MovimientoCobrar::find()->where(['idMov'=>$id])->one();//busco el movimiento a cobrar
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//busco la empresa para el encabezado
            $impresora = 'TSP100'; //Selecciona la impresora que se va a usar
            $tipo_movimiento = $MovimientoCobrar->tipmov == 'NC' ? 'NOTA DE CREDITO' : 'ABONO'; //Identofico si es un AB o NC


//Texto impreción diecta del movimiento (AB/NC)-------------------------------------------------------------------------
            $connector = new WindowsPrintConnector("smb://Dell300914/".$impresora);//smb://Dell300914/TSP100
            $printer = new Printer($connector);
            try {
            //Encabezado----------------------
                $printer -> text( "\n\n" . $empresa->nombre . "\n");
                $printer -> text( $empresa->direccion . "\n");
                $printer -> text( $empresa->telefono.' / '. $empresa->fax . "\n\n\n" );
            //Contenido-----------------------
                $printer -> text( $tipo_movimiento . "\n");
                $printer -> text( '# '. $MovimientoCobrar->idMov . "\n\n");
                $printer -> text( "Cliente:\n". $MovimientoCobrar->idCliente . "\n\n\n");
                $printer -> text( "Por Concepto de:\n". $MovimientoCobrar->concepto . "\n\n");
                $printer -> text( "------------------------------------------------\n");
                $printer -> text( "Aplicada al Documento:".$MovimientoCobrar->idCabeza_Factura."\n");
                $printer -> text( "------------------------------------------------\n");
                $printer -> text( "MONTO:".number_format($MovimientoCobrar->monto_movimiento,2)."\n\n\n");
                $printer -> text( NumberToLetterConverter::ValorEnLetras($MovimientoCobrar->monto_movimiento,"COLONES")."\n\n\n\n\n\n");
                $printer -> text( "_______________________\nAutoriza\n\n\n\n\n");
                $printer -> text( "Us-Reg:".$MovimientoCobrar->usuario."\n\n\n\n\n\n\n\n\n");
//fin de texto de movimiento------------------------------------------------------------------------------------------

            $printer -> feed(2);//Esto me permite cortar la factura en la base del papel
            $printer -> cut();
            $printer -> close();//cierro la operación
            } catch (Exception $e) {
                echo "Couldn't print to this printer";
            } finally {

            }
            echo "<script lenguaje=\"JavaScript\">window.close();</script>";*/
            //Yii::$app->response->redirect(array('movimiento-credito/index'));
           /* $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Factura</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$encabezado.'<br><br>'.$content.'
            </body>

            </html>';

        echo $imprimir;*/
        }

        //actualizo tbl_encabezado_factura para que todas las credi_atencion que estan seleccionadas con 1 sean las
        //que me permitan ser escojidas como las que iran a ser canceladas, con esta opcion las clasifico y las obtengo
        public function actionObtener_facturas_marcadas(){
            if(Yii::$app->request->isAjax){
                $checkboxValues  = Yii::$app->request->post('checkboxValues');
                $array = explode(",", $checkboxValues);

                $longitud = count($array);
                for($i=0; $i<$longitud; $i++) {
                    $credi_atencion = '1';

                    $sql = "UPDATE tbl_encabezado_factura SET credi_atencion = :credi_atencion WHERE idCabeza_Factura = :idCabeza_Factura";

                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":credi_atencion", $credi_atencion);
                        $command->bindValue(":idCabeza_Factura", $array[$i]);
                        $command->execute();
                }
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            }
        }

        //con esta otra funcion puedo editar las facturas cuyo credi_atencio esta en 1 al cliente seleccionado pasarlas en 0
        //para que ya no esten en la lista de cancelacion, de esta forma se extluyen al proceso de cancelacion
        public function actionObtener_facturas_marcadas_regreso(){
            if(Yii::$app->request->isAjax){
                $checkboxValues  = Yii::$app->request->post('checkboxValues');
                $array = explode(",", $checkboxValues);

                $longitud = count($array);
                for($i=0; $i<$longitud; $i++) {
                    $credi_atencion = '';

                    $sql = "UPDATE tbl_encabezado_factura SET credi_atencion = :credi_atencion WHERE idCabeza_Factura = :idCabeza_Factura";

                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":credi_atencion", $credi_atencion);
                        $command->bindValue(":idCabeza_Factura", $array[$i]);
                        $command->execute();
                }
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            }
        }

        public function actionCancelacionexitosa(){

            Yii::$app->response->redirect(array('movimiento-credito/index'));
        }




//-------------------------------------------------------------------------------------------------------
//--------------------------------- RECIBOS GENERADOS ---------------------------------------------------
//-------------------------------------------------------------------------------------------------------


        //ingresa en session los datos del recibo del cliente a manipular
        public function actionInyectarrecibo(){
            if(Yii::$app->request->isAjax){
                    $idRecibo  = Yii::$app->request->post('idRecibo');
                    $detalles = RecibosDetalle::find()->where(['idRecibo'=>$idRecibo])->orderBy(['idDetallerecibo' => SORT_DESC])->all();
          					$detalle_html = '<table class="items table table-striped">';
          					if($detalles) {
          						$detalle_html .= '<tbody>';
          						foreach($detalles as $position => $detalle) {
          							$mostrar = '';
          							$fecha = '';
          							$documento = '';
          							$monto = 0.00;
          							if ($detalle['tipo']=='FA') {//Muestra la factura en una modal
          								$modelfa = MovimientoCredito::find()->where(['idCabeza_Factura'=>$detalle['idDocumento']])->one();
          								$documento = $modelfa->idCabeza_Factura;
          								$monto = $detalle['monto'];
          								$mostrar = Html::a('', ['#'], [
          	                                'class'=>'glyphicon glyphicon-search',
          	                                'id' => 'activity-index-link-factura-2',
          	                                'data-toggle' => 'modal',
          	                                'data-target' => '#modalFactura2',
          	                                'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $detalle['idDocumento']]),
          	                                'data-pjax' => '0',
          	                                'title' => Yii::t('app', 'Procede a mostrar la factura')]);
          							} else
          							if (($detalle['tipo']=='AB') || ($detalle['tipo']=='NC')) {//muestra el movimento en una modal
          								$modelmovi = MovimientoCobrar::find()->where(['idMov'=>$detalle['idDocumento']])->one();
          								$documento = $modelmovi->idMov;
          								$monto = $modelmovi->monto_movimiento;
          								$mostrar = Html::a('', ['', 'id' => $position], [
          		                        'class'=>'glyphicon glyphicon-search',
          		                        'id' => 'activity-index-link-agrega-docu',
          		                        'data-toggle' => 'modal',
          		                        'onclick'=>'javascript:consulta_modal_movimiento("'.$detalle['idDocumento'].'")',
          		                        'data-target' => '#modalconsultadocu',
          		                        //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
          		                        'data-pjax' => '0',
          		                        'title' => Yii::t('app', 'Procede a mostrar el movimiento') ]);
          							}
          								$detalle_html .= '
                          <tr>
                            <td><font face="arial" size=2>'.$documento.'</font></td>
                            <td align="right"><font face="arial" size=2>'.$detalle['tipo'].'</font></td>
                            <td align="right"><font face="arial" size=2>'.date('d-m-Y h:i:s A', strtotime( $detalle['fecha'] )).'</font></td>
                            <td align="right"><font face="arial" size=2>'.number_format($monto,2).'</font></td>
                            <td><font face="arial" size=2>'.$detalle['referencia'].'</font></td>
                            <td class="actions button-column">'.$mostrar.'</td>
                          </tr>';
          							}
          						$detalle_html .= '</tbody>';
          					}
          					$detalle_html .='</table>';
                    echo $detalle_html;
            }
        }

        //Para buscar
        public function actionConsulta_recibo(){
            if(Yii::$app->request->isAjax){
                    $idRecibo_buscar = Yii::$app->request->post('idRecibo_buscar');
                    $idCliente = Yii::$app->request->post('idCliente');
                    $recibos = Recibos::find()->where(['cliente'=>$idCliente])->andWhere("idRecibo = :idRecibo", [":idRecibo"=>$idRecibo_buscar])->one();
                    if ($recibos) {

                    $recibos_html = '<table class="items table table-striped" id="tabla_recibos">';
              	      $recibos_html .= '<tbody>';
              		    $consultar = Html::a('', ['', 'id' => $recibos->idRecibo], [
              		                        'class'=>'glyphicon glyphicon-search',
              		                        'id' => 'activity-index-link-agregacredpen',
              		                        'data-toggle' => 'modal',
              		                        'onclick'=>'javascript:consulta_modal_recibo("'.$recibos->idRecibo.'")',
              		                        'data-target' => '#modalconsultamovi',
              		                        //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
              		                        'data-pjax' => '0',
              		                        'title' => Yii::t('app', 'Mostrar datos del recibo') ]);
              				$imprimir = Html::a('<span class=""></span>', ['movimiento-credito/index', 'idRecibo'=>$recibos->idRecibo], [
              		                        'class'=>'fa fa-print',
              		                        'id' => 'activity-index-link-report',
              		                        //'target'=>'_blank',
              		                        'data-toggle' => 'titulo',
              		                        //'data-url' => Url::to(['create']),
              		                        'data-pjax' => '0',
              		                        'title' => Yii::t('app', 'Imprimir Recibo'),
              		                        ]);
              				$accion_mov = $consultar . ' ' . $imprimir;
              		        $recibos_html .= '
                          <tr style = "cursor:pointer;" onclick="marcarecibos(this, '.$recibos->idRecibo.')">
                            <td><font face="arial" size=2>'.$recibos->idRecibo.'</font></td>
                            <td align="right"><font face="arial" size=2>'.number_format( $recibos->monto,2 ).'</font></td>
                            <td><font face="arial" size=2>'.date('d-m-Y h:i:s A', strtotime( $recibos->fechaRecibo )).'</font></td>
                            <td><font face="arial" size=2>'.$recibos->usuario.'</font></td>
                            <td class="actions button-column">'.$imprimir.'</td>
                          </tr>';

                          $recibos_html .= '</tbody>';
                	        $recibos_html .= '</table>';
                          echo $recibos_html;

              	    	}

                    //CreditoSession::setIdrecibobuscar($idRecibo_buscar);
                    // code...
                    else {
                      echo "<center><h3>No se encuentran resultados</h3></center>";
                    }
                  }
        }

        //Para lstar todas las facturas
        public function actionConsultar_todos_recibos(){
            if(Yii::$app->request->isAjax){
                    $idCliente  = Yii::$app->request->post('idCliente');
                    $recibos = Recibos::find()->where(['cliente'=>$idCliente])->orderBy(['idRecibo' => SORT_DESC])->all();
                    $recibos_html = '<table class="items table table-striped" id="tabla_recibos">';
              	        $recibos_html .= '<tbody>';
              	        if ($recibos) {
              		        foreach($recibos as $position => $recibo) {
              		        $consultar = Html::a('', ['', 'id' => $position], [
              		                        'class'=>'glyphicon glyphicon-search',
              		                        'id' => 'activity-index-link-agregacredpen',
              		                        'data-toggle' => 'modal',
              		                        'onclick'=>'javascript:consulta_modal_recibo("'.$recibo['idRecibo'].'")',
              		                        'data-target' => '#modalconsultamovi',
              		                        //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
              		                        'data-pjax' => '0',
              		                        'title' => Yii::t('app', 'Mostrar datos del recibo') ]);
              				$imprimir = Html::a('<span class=""></span>', ['movimiento-credito/index', 'idRecibo'=>$recibo['idRecibo']], [
              		                        'class'=>'fa fa-print',
              		                        'id' => 'activity-index-link-report',
              		                        //'target'=>'_blank',
              		                        'data-toggle' => 'titulo',
              		                        //'data-url' => Url::to(['create']),
              		                        'data-pjax' => '0',
              		                        'title' => Yii::t('app', 'Imprimir Recibo'),
              		                        ]);
              				$accion_mov = $consultar . ' ' . $imprimir;
              		        $recibos_html .= '
                          <tr style = "cursor:pointer;" onclick="marcarecibos(this, '.$recibo['idRecibo'].')">
                            <td><font face="arial" size=2>'.$recibo['idRecibo'].'</font></td>
                            <td align="right"><font face="arial" size=2>'.number_format( $recibo['monto'],2 ).'</font></td>
                            <td><font face="arial" size=2>'.date('d-m-Y h:i:s A', strtotime( $recibo['fechaRecibo'] )).'</font></td>
                            <td><font face="arial" size=2>'.$recibo['usuario'].'</font></td>
                            <td class="actions button-column">'.$imprimir.'</td>
                          </tr>';
              		    	}
              	    	}
              	        $recibos_html .= '</tbody>';
              	        $recibos_html .= '</table>';
                        echo $recibos_html;
                    /*CreditoSession::setBanderacargar($banderacargar);
                    CreditoSession::setIdrecibobuscar(null);*/
            }
        }

        //Para imprimir facturas canceladas en el mivimiento credito
        public function actionImprimir_facturas($idRecibof)
        {
            echo $this->render('imprimir/imprimir_facturas', ['idRecibof' => $idRecibof]);
            //echo "<script lenguaje=\"JavaScript\">window.close();</script>";
        }

        //Para imprimir el recibo
        public function actionImprimir_recibo(){
            $idRecibo = intval($_GET['idRecibo']);
            echo $this->render('imprimir/imprimir_recibo', ['idRecibo' => $idRecibo]);
            //echo "<script lenguaje=\"JavaScript\">window.close();</script>";


           /* $recibos = Recibos::find()->where(['=','idRecibo', $idRecibo])->one();
            $modelclient = Clientes::find()->where(['=','idCliente', $recibos->cliente])->one();
            $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $detalles_doc = RecibosDetalle::find()->where(['idRecibo'=>$idRecibo])->orderBy(['idDetallerecibo' => SORT_DESC])->all();
            $content = '';
            $impresora = 'TSP100';*/


//-------------------IMPRECIÓN DE RECIBO Y NC -----------------------------------------------------
            //http://stackoverflow.com/questions/25973046/printing-to-pos-printer-from-php
         /*   try {
            $connector = new WindowsPrintConnector("smb://Dell300914/".$impresora);
            $printer = new Printer($connector);
            //Encabezado--------------------------------------------
            $printer -> text( "\n\n" . $empresa->nombre . "\n");
            $printer -> text( $empresa->direccion . "\n");
            $printer -> text( $empresa->telefono.' / '. $empresa->fax . "\n\n\n" );

            if ($recibos->tipoRecibo == 'CF') {
                //inicializamos las varibles glovales que se requieren para generar montos totales por facturas o NC
                //como tabien los diferentes tbody que vamos a ocupar para mostrar el conjunto de facturas y NC
                $tbodyfacturas = $tbodynotascredito = '';
                $montoFA = $montoNC = 0;
                    foreach ($detalles_doc as $position => $detalle) {//Recorremos todo el detelle de dicho recibo
                    //Si el detalle consta de Facturas lo recorremos con esta condicion
                        if ($detalle['tipo']=='FA') {
                            //obtenemos solo datos de facturas
                            $modelfa = MovimientoCredito::find()->where(['idCabeza_Factura'=>$detalle['idDocumento']])->one();
                            //$tbodyfacturas .= $modelfa->idCabeza_Factura."         ".date('d-m-Y', strtotime( $detalle['fecha'] ))."   ".number_format($detalle['monto'],2)."\n";
                            $tbodyfacturas .= new item($modelfa->idCabeza_Factura."         ".date('d-m-Y', strtotime( $detalle['fecha'] )), number_format($detalle['monto'],2));
                            $montoFA += $detalle['monto'];//sumamos el monto total de las facturas recorridas

                        } //De lo contrario recorreriamos las notas de credito que se le aplicaron a la(s) factura(s) recorrida(s) anteriormente
                        else if ($detalle['tipo']=='NC') {
                            //obtenemos solo datos del movimiento por cobrar NC
                            $modelmovi = MovimientoCobrar::find()->where(['idMov'=>$detalle['idDocumento']])->one();
                            //$tbodynotascredito .= $modelmovi->idMov."         ".date('d-m-Y', strtotime( $detalle['fecha'] ))."   ".number_format($modelmovi->monto_movimiento,2)."\n";
                            $tbodynotascredito .= new item($modelmovi->idMov."         ".date('d-m-Y', strtotime( $detalle['fecha'] )), number_format($modelmovi->monto_movimiento,2));
                            $montoNC += $modelmovi->monto_movimiento;//sumamos el monto total de las NC
                        }
                    }//fin foeach

                //Este es el contenido de los recibos por CANCELACION (donde van facturas y NC)
                $printer -> text('RECIBO DE DINERO'. "\n" . '# '. $recibos->idRecibo. "\n\n\n\n");
                $printer -> text( date('d-m-Y h:i:s', strtotime( $recibos->fechaRecibo )). "\n\n\n");
                $printer -> text('Hemos recibido de'. "\n" . $modelclient->nombreCompleto. "\n\n\n");
                $printer -> text('La suma de'. "\n" . NumberToLetterConverter::ValorEnLetras($recibos->monto,"COLONES") . "\n\n\n");
                $printer -> text( 'Por Concepto de: ' . "\n" . 'CANCELACION DE FACTURAS' . "\n\n\n");
                $printer -> text( "----------------------------------\nFacturas Canceladas\n----------------------------------\n\n" );
                $printer -> text( 'DOCUMENTO      FECHA          MONTO' . "\n" . $tbodyfacturas . "\n---------------------------------------------");
                $printer -> text( new item("",'(+) '.number_format($montoFA,2). "\n\n\n" ));
                $printer -> text( "----------------------------------\nNotas de Credito Aplicadas\n----------------------------------\n\n" );
                $printer -> text( 'DOCUMENTO      FECHA          MONTO' . "\n" . $tbodynotascredito . "\n---------------------------------------------");
                $printer -> text( new item("",'(-) '.number_format($montoNC,2). "\n\n\n\n" ));
                $printer -> text( 'MONTO: '.number_format($recibos->monto,2). "\n\n\n\n" );
                $printer -> text( 'Saldo tota de la cuenta: '.number_format($recibos->saldo_cuenta,2). "\n\n\n\n\n");
                $printer -> text( '_______________________' . "\n" . 'Autoriza' . "\n\n\n\n\n");
                $printer -> text( 'Us-Reg: '.$recibos->usuario . "\n\n\n\n\n\n\n\n\n");

            } else {
                $diferencia_tipo = $recibos->tipoRecibo == 'NC' ? 'NOTA DE CREDITO' : 'RECIBO DE DINERO';
                //$concepto = $recibos->tipoRecibo == 'NC' ? 'NOTA DE CRÉDITO' : 'ABONO A FACTURA';
                $muestra = $recibos->tipoRecibo == 'NC' ? 'Al cliente:' : 'Hemos recibido de:';
                $numerorecibo = $recibos->tipoRecibo == 'NC' ? '' : '# '.$recibos->idRecibo;
                $detalle_doc_indi = RecibosDetalle::find()->where(['idRecibo'=>$idRecibo])->one();
                $modelmovi = MovimientoCobrar::find()->where([ 'idMov'=>$detalle_doc_indi->idDocumento ])->one();
                //$modelmovi->
                $factura = MovimientoCredito::find()->where(['idCabeza_Factura'=>$modelmovi->idCabeza_Factura])->one();
                $printer -> text( $diferencia_tipo . "\n" . $numerorecibo . "\n\n\n" );
                $printer -> text( date('d-m-Y h:i:s', strtotime( $recibos->fechaRecibo )) . "\n\n" );
                $printer -> text( $muestra . "\n" . $modelclient->nombreCompleto . "\n\n" );
                $printer -> text( "La suma de:\n" . NumberToLetterConverter::ValorEnLetras($recibos->monto,"COLONES") . "\n\n" );
                $printer -> text( "Por Concepto de:\n" . $modelmovi->concepto . "\n\n\n" );
                $printer -> text( "+-------------------------------+\n| Factura que se le aplico      |\n+-------------------------------+\n" );
                $printer -> text( "DOCUMENTO        FECHA             MONTO\n" );
                $printer -> text( $factura->idCabeza_Factura.'            '.$factura->fecha_inicio.'       '.number_format($factura->total_a_pagar,2)."\n" );
                $printer -> text( "---------------------------------------------\n\n" );
                $printer -> text( "+-----------------------+\n| Movimiento #: " . $modelmovi->idMov. "\n+-----------------------+\n\n" );
                $printer -> text( "Saldo Anterior: " . number_format($modelmovi->monto_anterior,2) . "\n" );
                $printer -> text( "Monto Aplicado: " . number_format($modelmovi->monto_movimiento,2) . "\n" );
                $printer -> text( "Saldo Documento: " . number_format($modelmovi->saldo_pendiente,2) . "\n\n\n" );
                $printer -> text( "MONTO: " . number_format($recibos->monto,2) . "\n\n" );
                $printer -> text( "Saldo total de la cuenta: " . number_format($recibos->saldo_cuenta,2) . "\n\n\n\n\n\n" );
                $printer -> text( "_______________________\nAutoriza\n\n\n\n\n" );
                $printer -> text( "Us-Reg:" . $recibos->usuario . "\n\n\n\n\n\n\n\n\n\n" );

            }//fin else
            //Fin recibos cancelaciones y NC

            $printer -> feed(2);//Esto me permite cortar la factura en la base del papel
            $printer -> cut();
            $printer -> close();
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> No se logra identificar la impresora');
                Yii::$app->response->redirect(array('movimiento-credito/index'));
            } finally {
                //Yii::$app->response->redirect(array('movimiento-credito/index'));
            }
            echo "<script lenguaje=\"JavaScript\">window.close();</script>";
            //Yii::$app->response->redirect(array('movimiento-credito/index'));
        */

        }//fin funcion actionImprimir_recibo()

//-------------------------------------------------------------------------------------------------------
//--------------------------------- ESTADO DE CUENTA ----------------------------------------------------
//-------------------------------------------------------------------------------------------------------
        //Para actualizar notificaciones del cliente si se envian o no el estado de cuenta
        public function actionNotificar_estado_cuenta(){
            if(Yii::$app->request->isAjax){
                $notif  = Yii::$app->request->post('notif');
                $idCliente  = Yii::$app->request->post('idCliente');

                $sql = "UPDATE tbl_clientes SET notificaciones = :notificaciones WHERE idCliente = :idCliente";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":notificaciones", $notif);
                $command->bindValue(":idCliente", $idCliente);
                $command->execute();
            }
        }

        //Para imprimir el estado de cuenta
        public function actionImprimir_estado_cuenta(){
            $idCliente = intval($_GET['idCliente']);
            $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $cliente = Clientes::find()->where(['idCliente'=>$idCliente])->one();
            $encabezado = '
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table>
                            <tbody>
                            <tr>
                            <td>
                            <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                            </td>
                            <td style="text-align: left;">
                            <strong>'.$empresa->nombre.'</strong><br>
                            <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                            <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                            <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                            <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                            <strong>Dirección:</strong> '.$empresa->email.'
                            </td>
                            <tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">ESTADO DE CUENTA <br><small>Al '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>';
            $content = '
                <p><font size=1><strong>Cliente:</strong></font> '.$cliente->nombreCompleto.'</p>
                <p>
                    <font size=1><strong>ID:</strong> '.$cliente->idCliente.'</font>&nbsp;&nbsp;&nbsp;&nbsp;
                    <font size=1><strong>Teléfono:</strong> '.$cliente->telefono.'</font>&nbsp;&nbsp;&nbsp;&nbsp;
                    <font size=1><strong>Correo:</strong> '.$cliente->email.'</font>
                </p>
                <p><font size=1><strong>Dirección:</strong> '.$cliente->direccion.'</font></p>
                '.$this->renderAjax('estado_cuenta_reporte',['idCliente'=>$idCliente]).'
            ';
            $imprimir = '
                        <!DOCTYPE html>
                        <html>
                        <head>
                            <meta charset="UTF-8">
                            <title>Factura</title>
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                            <script type="text/javascript">
                                function imprimir() {
                                    if (window.print) {
                                        window.print();
                                        setTimeout("self.close()", 50 );
                                    } else {
                                        alert("La función de impresion no esta soportada por su navegador.");
                                    }
                                }
                            </script>
                            <style type="text/css">
                                footer{
                                  display: none;
                                }
                            </style>
                        </head>
                        <body onload="imprimir();">

                        '.$encabezado.$content.'
                        </body>

                        </html>';

                    echo $imprimir;

        }


        //Para enviar el estado de cuenta al correo
        public function actionEnviar_estado_cuenta(){
            $idCliente = intval($_GET['idCliente']);
            $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $cliente = Clientes::find()->where(['idCliente'=>$idCliente])->one();

            if ($cliente->notificaciones == 'true') {
            $estado_cuenta_correo = '
                <font size=3><strong>Cliente:</strong></font> '.$cliente->nombreCompleto.'<br>

                    <font size=3><strong>ID:</strong> '.$cliente->idCliente.'</font>&nbsp;&nbsp;&nbsp;&nbsp;
                    <font size=3><strong>Teléfono:</strong> '.$cliente->telefono.'</font>&nbsp;&nbsp;&nbsp;&nbsp;
                    <font size=3><strong>Correo:</strong> '.$cliente->email.'</font>
                <br>
                <font size=3><strong>Dirección:</strong> '.$cliente->direccion.'</font><br><br>
                '.$this->renderAjax('estado_cuenta_correo',['idCliente'=>$idCliente]).'
            ';
            $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
            $content = '
            <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
            <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
            <br>
            <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
              <tbody><tr>
                <td align="center" valign="top">
                  <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                    <tbody><tr>
                        <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                          <table>
                            <tbody><tr>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                                &nbsp;&nbsp;&nbsp;&nbsp;

                              </td>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                       <br>
                        <table>
                            <tbody>
                            <tr>
                            <td>
                            <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                            </td>
                            <td style="text-align: left;">
                            <strong>'.$empresa->nombre.'</strong><br>
                            <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                            <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                            <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                            <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                            <strong>Dirección:</strong> '.$empresa->email.'
                            </td>
                            <tr>
                            </tbody>
                        </table>
                        <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">ESTADO DE CUENTA <br><small>Al '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>

                        '.$estado_cuenta_correo.'


            <br><br><br>

                      </td>
                    </tr>
                    <tr>
                      <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                        Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                  </tbody></table>
                </td>
              </tr>
            </tbody></table>
            <br></div></div><div class="adL">

            </div></div></div>





                ';
            //Creamos el mensaje que será enviado a la cuenta de correo del usuario
            $subject = "Estado de Cuenta ".$empresa->nombre;


            //Enviamos el correo
            Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
            ->setTo($cliente->email)//para
            ->setFrom($empresa->email_contabilidad)//de
            ->setSubject($subject)
            ->setHtmlBody($content)
            ->send();
            //echo $content;
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El estado de cuenta fue enviado correctamente.');
            Yii::$app->response->redirect(array('movimiento-credito/index'));
        }
        else {
            Yii::$app->response->redirect(array('movimiento-credito/index'));
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> El Cliente no tiene aprobado las notifiaciones de estado de cuenta.');
        }
        }

//-------------------------------------------------------------------------------------------------------
//--------------------------------- FACTURAS CANCELADAS -------------------------------------------------
//--------------------------------------------------------------------------------------------------------

//ingresa en session los datos de las fechas a filtrar
        public function actionFiltro_facturas_canceladas(){
            if(Yii::$app->request->isAjax){
                    $fedes  = Yii::$app->request->post('fedes');//desde
                    $feast  = Yii::$app->request->post('feast');//hasta
                    $idCliente = Yii::$app->request->post('idCliente');
                    if ($fedes==false) {
                        $fedes = $feast;
                    }
                    if ($feast==false) {
                        $feast = $fedes;
                    }
                    $fedes = date('Y-m-d', strtotime( $fedes ));
                    $feast = date('Y-m-d', strtotime( $feast ));
                    $facturasCanceladas = MovimientoCredito::find()->where(['=','idCliente', $idCliente])
                    ->andWhere("estadoFactura = :estadoFactura", [':estadoFactura'=>'Cancelada'])
                    ->andWhere("fecha_inicio BETWEEN :fecha_uno AND :fecha_dos",
                    [':fecha_uno'=>$fedes, ':fecha_dos'=>$feast])
                    ->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
                    $contador = 0;
              			$movimiento_html = '<table class="items table table-striped" id="tabla_facturas_canceladas">';
              	        	$movimiento_html .= '<tbody>';
              	        		foreach($facturasCanceladas as $position => $factura) {
                              $contador += 1;
              	        			$fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
              	        			$fecha_final = date('d-m-Y', strtotime( $factura['fecha_final'] ));
              	        			$mostrarFactura = Html::a('', ['#'], [
              	                                'class'=>'glyphicon glyphicon-search',
              	                                'id' => 'activity-index-link-facturas_canceladas',
              	                                'data-toggle' => 'modal',
              	                                'data-target' => '#modalFacturacancel',
              	                                'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
              	                                'data-pjax' => '0',
              	                                'title' => Yii::t('app', 'Procede a mostrar la factura')]);
              	        			$movimiento_html .=
                               '<tr tabindex="0" style="cursor:pointer; color:#231BB8;" onclick="obtener_fact_canc(this, '.$factura['idCabeza_Factura'].')" onkeypress="$(document).keypress(function(event){ if(event.which == 13) obtener_fact_canc(this, '.$factura['idCabeza_Factura'].') })">
              	        		 		    <td align="left" width="350" ><font size=4 >'.$factura['idCabeza_Factura'] .' | '. $factura['fe'].'</font></td>
              	                		<td align="center"  ><font size=4>'.$fecha_inicio.'</font></td>
              	                		<td align="center" ><font size=4>'.$fecha_final.'</font></td>
              	                		<td align="right" ><font size=4>'.number_format($factura['total_a_pagar'],2).'</font></td>
              	                		<td class="actions button-column">'.$mostrarFactura.'</td>
                                </tr>';
              	        		}
              	        	$movimiento_html .= '</tbody>';
              	        $movimiento_html .= '</table>';
                        if ($contador > 0) {
                          echo $movimiento_html;
                        } else {
                          echo '<center><h2>No se encontraron resultados</h2></center>';
                        }

            }
        }

//ingresa en session los datos de la factura a consultar notas y recibos
        public function actionInyectarfactura(){
            if(Yii::$app->request->isAjax){
                    $idCabeza_Factura  = Yii::$app->request->post('idCabeza_Factura');
                    $movimientos_nc = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])->orderBy(['idMov' => SORT_DESC])->all();
        							$movimientos_ab = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'AB'])->orderBy(['idMov' => SORT_DESC])->all();
                      //Abonos
                      $abono_html = '<table class="items table table-striped">';
        							foreach ($movimientos_ab as $position => $movimiento) {
        								$muestraRecibo = Html::a('', ['', 'id' => $position], [
        		                        'class'=>'glyphicon glyphicon-search',
        		                        'id' => 'activity-index-link-recibo',
        		                        'data-toggle' => 'modal',
        		                        'onclick'=>'javascript:consulta_modal_recibo("'.$movimiento['idMov'].'")',
        		                        'data-target' => '#modalconsultarecibo',
        		                        //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
        		                        'data-pjax' => '0',
        		                        'title' => Yii::t('app', 'Procede a mostrar el movimiento') ]);

        								$reDet = RecibosDetalle::find()->where(['idDocumento'=>$movimiento['idMov']])->one();
        								$imprimeRecibo = Html::a('<span class=""></span>', ['movimiento-credito/index', 'idRecibo'=>$reDet->idRecibo], [
        		                        'class'=>'fa fa-print',
        		                        'id' => 'activity-index-link-report',
        		                        //'target'=>'_blank',
        		                        'data-toggle' => 'titulo',
        		                        //'data-url' => Url::to(['create']),
        		                        'data-pjax' => '0',
        		                        'title' => Yii::t('app', 'Imprimir Recibo'),
        		                        ]);

        								$abono_html .= '
                        <tr>
                          <td>'.$movimiento['idMov'].'</td>
                          <td align="right">'.date('Y-m-d | h:i:s A', strtotime($reDet->fecha)).'</td>
                          <td align="right">'.number_format($reDet->monto,2).'</td>
                          <td class="actions button-column">'.$muestraRecibo . ' '. $imprimeRecibo.'</td>
                        </tr>';
        							}

        							$abono_html .= '</table>';

                      //notas de credito
                      $nc_html = '<table class="items table table-striped">';

        							foreach ($movimientos_nc as $position => $movimiento) {
        								$muestraRecibo = Html::a('', ['', 'id' => $position], [
        		                        'class'=>'glyphicon glyphicon-search',
        		                        'id' => 'activity-index-link-recibo',
        		                        'data-toggle' => 'modal',
        		                        'onclick'=>'javascript:consulta_modal_recibo("'.$movimiento['idMov'].'")',
        		                        'data-target' => '#modalconsultarecibo',
        		                        //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
        		                        'data-pjax' => '0',
        		                        'title' => Yii::t('app', 'Procede a mostrar el movimiento') ]);

        								$reDet_ = RecibosDetalle::find()->where(['idDocumento'=>$movimiento['idMov']])->one();
        								$imprimeNC = Html::a('<span class=""></span>', ['movimiento-credito/index', 'id'=>$movimiento['idMov']], [
        						                                'class'=>'fa fa-print',
        						                                'id' => 'activity-index-link-report',
        						                                //'target'=>'_blank',
        						                                'data-toggle' => 'titulo',
        						                                //'data-url' => Url::to(['create']),
        						                                'data-pjax' => '0',
        						                                'title' => Yii::t('app', 'Imprimir nota de crédito'),
        						                                ]);

        								$nc_html .= '<tr>
                                      <td>'.$movimiento['idMov'].'</td>
                                      <td align="right">'.date('d-m-Y | h:i:s A', strtotime($reDet_->fecha)).'</td>
                                      <td align="right">'.number_format($reDet_->monto,2).'</td>
                                      <td class="actions button-column">'.$muestraRecibo . ' '. $imprimeNC.'</td>
                                    </tr>';
        							}

        							$nc_html .= '</table>';
                      $arr = array('recibos_detalle_div'=>$abono_html,
                      'nc_detalle_div'=>$nc_html
                      );
                      echo json_encode($arr);
        						}
        }

        public function crearmovimientolibro($idCuenta_bancaria, $comprobacion, $idCabeza_Factura, $monto, $fechadepo)
        {
          $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
          $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
          $facturas_caja = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
          //habro una instancia para obtener el tipo de moneda de la cuenta con la estoy usando
          $moneda = Moneda::find()->where(['idTipo_moneda'=>$modelcuentabancaria->idTipo_moneda])->one();
          //habro una instancia para obtener el tipo de cambio y compararla con la moneda que ya tengo de la cuenta
          $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
          //abrimos una intancia de movimiento libros donde se procese a crear un movimiento automatizado
          $model_movi_libros = new MovimientoLibros();
          $model_movi_libros->idCuenta_bancaria = $idCuenta_bancaria;
          $model_movi_libros->idTipoMoneda = $modelcuentabancaria->idTipo_moneda;
          $model_movi_libros->idTipodocumento = 2;
          $model_movi_libros->idMedio_pago = null;
          $model_movi_libros->numero_documento = (int) $comprobacion;
          $model_movi_libros->fecha_documento = date('Y-m-d', strtotime( $fechadepo ));//$model_mediopago->fecha_documento;
          $model_movi_libros->monto = $monto;
          $model_movi_libros->tasa_cambio = null;
          $model_movi_libros->monto_moneda_local = 0.00;
          //con esto compruebo si hay que hacer tipo cambio.
          if ($moneda->monedalocal!='x') {
              $model_movi_libros->monto = $monto / $tipocambio->valor_tipo_cambio;
              $model_movi_libros->tasa_cambio = $tipocambio->valor_tipo_cambio;
              $model_movi_libros->monto_moneda_local = $monto;
          }
          $model_movi_libros->beneficiario = $empresa->nombre;
          $model_movi_libros->detalle = 'PAGO DE FACTURA CON TRANSFERENCIA BANCARIA';
          $model_movi_libros->fecha_registro = date("Y-m-d");
          $model_movi_libros->usuario_registra = Yii::$app->user->identity->nombre;
          //guardamos el movimiento en libros, si esto es asi...
          if ($model_movi_libros->save()) {
              //...Debemos modificar el monto de cuentas bancarias correspondiendo al movimiento, en ese caso pago a proveedor
              ////habrimos instancia para guardar en cuentas bancarias correspondioente al movimiento en libros
              //$modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
              //como el tipo de movimiento es debito el saldo en libros de cuenta bancaaria es igual a su misma cantidad menos el monto del nuevo movimiento en debito
              $modelcuentabancaria->saldo_libros += $model_movi_libros->monto;

              //guardamos actualizacion en cuenta bancaria
              if ($modelcuentabancaria->save()) {//y modificamos el saldo en libros para el control de saldo por movimiento
                  $libros = MovimientoLibros::find()->where(['idMovimiento_libro'=>$model_movi_libros->idMovimiento_libro])->one();
                  $libros->saldo = $modelcuentabancaria->saldo_libros;//porque es hasta aqui que el saldo en libros lleva este monto
                  $libros->save();
              }
            }//fin $model_movi_libros->save
        }

}



//clase para agrupar numeros en fatura
class item
{
    private $nombre;
    private $precio;
    private $signo;

    public function __construct($nombre = '', $precio = '', $signo = false)
    {
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> signo = $signo;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 28;
        if ($this -> signo) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> nombre, $leftCols) ;

        $sign = ($this -> signo ? '$ ' : '');
        $right = str_pad($sign . $this -> precio, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}
