<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\AccessHelpers;

class BaseController extends Controller {

    public function beforeAction($action)
    {
        $tipo_usuario = "Administrador";
        if (!parent::beforeAction($action)) {
            return false;
        }

        $operacion = str_replace("/", "-", Yii::$app->controller->route);

        $permitirSiempre = [
        'site-captcha', 'site-signup', 'site-index', 'site-error', 'site-about', 'site-contact', 'site-login', 'site-logout', 'version-enviarnotificacion',
        'empresa-deleteimg','empresa-activar_factun','empresa-guardar_datos_factun',
        'empresa-crear_carpeta',
        'vehiculos-modelo',
        'funcionario-buscar_funcionario',
        'clientes-compruebe_repetido', 'clientes-compruebe_repetido_mail', 'clientes-buscar_cliente', 'clientes-obtener_canton', 'clientes-obtener_distrito', 'clientes-obtener_barrio',
        /*producto-servicios*/
        'producto-servicios-index2', 'producto-servicios-asignar_proveedor', 'producto-servicios-elimina_proveedor', 'producto-servicios-actualiza_modal_proveedor',
        'producto-servicios-actualiza_proveedor', 'producto-servicios-historial_producto', 'producto-servicios-asignar_marca', 'producto-servicios-elimina_marca',
        'producto-servicios-actualiza_modal_marca','producto-servicios-actualiza_marca', 'producto-servicios-busqueda_producto',
        'proveedores-agrega_contacto_pedido', 'proveedores-elimina_contacto_pedido', 'proveedores-actualiza_modal_contacto_pedido', 'proveedores-actualiza_contacto_pedido',
        'proveedores-agrega_contacto_pagos', 'proveedores-actualiza_modal_contacto_pago', 'proveedores-actualiza_contacto_pagos', 'proveedores-elimina_contacto_pagos',
        'proveedores-asignar_cuenta_bancaria', 'proveedores-elimina_cuenta_bancaria',
        'proveedores-actualiza_modal_cuenta_bancaria', 'proveedores-actualizar_cuenta_bancaria',
        /*orden-servicio*/
        'orden-servicio-cliente','orden-servicio-serviciosc', 'orden-servicio-serviciosu', 'orden-servicio-placa',
        'orden-servicio-detallevehiculo', 'orden-servicio-detallepropietario', 'orden-servicio-serviciosmontoc',
        'orden-servicio-serviciosmontou', 'orden-servicio-cambiaperiodo','orden-servicio-create_proforma',
        'orden-servicio-report', 'orden-servicio-imprimirorden',
        /*compras-inventario*/
        'compras-inventario-cancelaru','compras-inventario-sumafecha','compras-inventario-desactivarsession', 'compras-inventario-run_producto_input',
        'compras-inventario-desactivarsessionu', 'compras-inventario-obtenerprecioventa', 'compras-inventario-obtenerutilidadimpu',
        'compras-inventario-obtenerventauti','compras-inventario-agrega_modalprod','compras-inventario-agrega_modalprod_update','compras-inventario-view',
        'compras-inventario-actualiza_modalprod_listo','compras-inventario-actualiza_modalprod_listo_update','compras-inventario-actualiza_modalprod',
        'compras-inventario-actualiza_modalprod_update','compras-inventario-deleteitem','compras-inventario-deleteitem_update',
        'compras-inventario-run_producto', 'compras-inventario-run_producto_air', 'compras-inventario-agrega_prod_inv', 'compras-inventario-convertir_dolar',
        'compras-inventario-agrega_prod_inv_update', 'compras-inventario-busqueda_producto', 'compras-inventario-cargar_compra', 'compras-inventario-cargar_compra_bd',
        'compras-inventario-guardarcompra', 'compras-inventario-aplicarcompra', 'compras-inventario-report', 'compras-inventario-etiquetas',
        'compras-inventario-run_producto_etiqueta', 'compras-inventario-run_agrega_etiqueta', 'compras-inventario-deleteetiqueta', 'compras-inventario-renviar_xml_not',
        'compras-inventario-actualiza_modaletiq', 'compras-inventario-agrega_compra_etiqueta', 'compras-inventario-etiquetas_marcadas',
        'compras-inventario-obtener_productos_marcados_create', 'compras-inventario-obtener_productos_marcados_update', 'compras-inventario-etiquetas_compra',
        'compras-inventario-actualiza_modaletiq_listo', 'compras-inventario-obtener_etiquetas_marcadas', 'compras-inventario-eliminar_etiquetas_marcadas',
        'compras-inventario-obtenermontoflete', 'compras-inventario-obtenermontoflete_u', 'compras-inventario-enviar_xml_not', 'compras-inventario-pdf_resolucion',
        /*Transporte*/
        'transporte-view',
        /*Ordenes de compra a proveedores*/
        'orden-compra-prov-view', 'orden-compra-prov-create', 'orden-compra-prov-update', 'orden-compra-prov-delete', 'orden-compra-prov-limpiar',
        'orden-compra-prov-busqueda_proveedor', 'orden-compra-prov-addproveedor', 'orden-compra-prov-cargardatos', 'orden-compra-prov-actualizar_producto_modal',
        'orden-compra-prov-agrega_producto_pedido', 'orden-compra-prov-obtener_productos_pedidos_regreso', 'orden-compra-prov-orden_compra_productos_disponibles',
        'orden-compra-prov-orden_compra_productos_disponibles_rango_fechas', 'orden-compra-prov-orden_compra_productos_pedido', 'orden-compra-prov-estadistica_producto', 'orden-compra-prov-buscar_estadistica_producto',
        'orden-compra-prov-imprimir_orden_compra', 'orden-compra-prov-obtenerordencompra', 'orden-compra-prov-lista_orden_compra',
        'orden-compra-prov-findModel', 'orden-compra-prov-enviar_compra_correo', 'orden-compra-prov-enviar_orden_compra',
        /*Prefacturas*/
        'cabeza-prefactura-descripcion', 'cabeza-prefactura-activar_btn_facturacion',
        /*Tramite de pago a proveedores*/
        'tramite-pago-view','tramite-pago-vista_factura','tramite-pago-create','tramite-pago-update','tramite-pago-delete',
        'tramite-pago-addproveedor','tramite-pago-limpiar','tramite-pago-busqueda_proveedor','tramite-pago-obtener_facturas_marcadas',
        'tramite-pago-obtener_facturas_marcadas_regreso','tramite-pago-agrega_factura_lista','tramite-pago-tbl_uno','tramite-pago-tbl_dos',
        'tramite-pago-mov_facturafecha','tramite-pago-agrega_nota_credito','tramite-pago-actualiza_nota_credito','tramite-pago-elimina_nota_credito',
        'tramite-pago-tbl_nc','tramite-pago-imprimir_movimientos','tramite-pago-tbl_nd','tramite-pago-agrega_nota_debito',
        'tramite-pago-actualiza_nota_debito','tramite-pago-elimina_nota_debito','tramite-pago-tbl_ab','tramite-pago-agrega_abono',
        'tramite-pago-generar_pago','tramite-pago-agrega_nota_credito_tramite','tramite-pago-actualiza_nota_credito_tramite',
        'tramite-pago-elimina_nota_credito_tramite','tramite-pago-tbl_nc_tramite','tramite-pago-montopagar','tramite-pago-valida_numero_cheque',
        'tramite-pago-valida_numero_comprovante_trans', 'tramite-pago-actualizar_pago','tramite-pago-listatramite', 'tramite-pago-obtenerfacturasdetramite','tramite-pago-imprimir',
        'tramite-pago-enviar_tramite', 'tramite-pago-enviar_tramite_correo',
        'tramite-pago-eliminar_tramite','tramite-pago-buscar_factura_cancelada','tramite-pago-vista_movimientos',
        /*cuentas-bancarias*/
        'cuentas-bancarias-view','cuentas-bancarias-create','cuentas-bancarias-update','cuentas-bancarias-delete', 'cuentas-bancarias-agrega_moneda',
        'cuentas-bancarias-actualiza_modal_moneda','cuentas-bancarias-actualizar_moneda','cuentas-bancarias-elimina_moneda','cuentas-bancarias-local_moneda',
        'cuentas-bancarias-agrega_entidad_fi','cuentas-bancarias-actualiza_modal_entidad','cuentas-bancarias-actualizar_entidad',
        'cuentas-bancarias-elimina_entidad','cuentas-bancarias-categoria_tipo_cambio','cuentas-bancarias-agrega_categoria','cuentas-bancarias-actualiza_modal_categoria',
        'cuentas-bancarias-actualizar_categoria','cuentas-bancarias-elimina_categoria','cuentas-bancarias-tipo_cambio',
        'cuentas-bancarias-agrega_tcambio','cuentas-bancarias-actualiza_modal_tcambio','cuentas-bancarias-actualizar_tcambio',
        'cuentas-bancarias-elimina_tcambio','cuentas-bancarias-valorcambiousar','cuentas-bancarias-agrega_documento',
        'cuentas-bancarias-actualiza_modal_documento','cuentas-bancarias-actualizar_documento','cuentas-bancarias-elimina_documento',
        /*Movimiento en libros*/
        'movimiento-libros-obtenermontomonedalocal', 'movimiento-libros-view','movimiento-libros-create','movimiento-libros-consulta_repetido',
        'movimiento-libros-update','movimiento-libros-delete','movimiento-libros-obtenercuenta','movimiento-libros-obtenertipodocumento',
        /*Confirmacion de tramite de pago*/
        //'tramite-pago-confirmar-view','tramite-pago-confirmar-create','tramite-pago-confirmar-update','tramite-pago-confirmar-delete',
        'tramite-pago-confirmar-listatramitecancelar','tramite-pago-confirmar-listatramiteconfirmar',
        'tramite-pago-confirmar-buscar_tramite_cancelar','tramite-pago-confirmar-buscar_tramite_confirmar',
        'tramite-pago-confirmar-reasignar_prioridad_tramites','tramite-pago-confirmar-inyectardatostramite',
        'tramite-pago-confirmar-tabla_facturas','tramite-pago-confirmar-reversar_tramite',
        'tramite-pago-confirmar-culminar_tramite_primero','tramite-pago-confirmar-modal_pagar_tramite',
        'tramite-pago-confirmar-valida_documento_banco','tramite-pago-confirmar-cancelar_tramite_pendiente',
        'tramite-pago-confirmar-devolver_tramite','tramite-pago-confirmar-aplicar_tramite_pendiente',
        'tramite-pago-confirmar-anular_tramite', 'tramite-pago-nc_tramite',
        /*Movimientos de inventario*/
        'movimientos-view','movimientos-tipo','movimientos-delete-tipo', 'movimientos-tipou', 'movimientos-delete-tipou', 'movimientos-referencia',
        'movimientos-delete-referencia', 'movimientos-referenciau', 'movimientos-delete-referenciau', 'movimientos-observaciones',
        'movimientos-delete-observaciones', 'movimientos-observacionesu', 'movimientos-delete-observacionesu', 'movimientos-cantidad',
        'movimientos-estado', 'movimientos-delete-estado', 'movimientos-delete-estadou', 'movimientos-additemi', 'movimientos-additemu',
        'movimientos-borrar-todo', 'movimientos-borrar-todou', 'movimientos-deleteall', 'movimientos-deleteallu', 'movimientos-aplicarmovimiento',
        'movimientos-complete', 'movimientos-deleteitem', 'movimientos-deleteitemu', 'movimientos-report', 'movimientos-busqueda_producto',
        /*Proforma*/
        'proforma-view', 'proforma-vendedor2', 'proforma-deletevendedor', 'proforma-cliente2',
        'proforma-clientenew', 'proforma-deletecliente', 'proforma-preciounitario', 'proforma-precioganancia',
        'proforma-precioiva', 'proforma-additem_temporal', 'proforma-moditem_temporal', 'proforma-additem_temporal_update',
        'proforma-moditem_temporal_update', 'proforma-additem', 'proforma-additemu', 'proforma-complete',
        'proforma-prefactura', 'proforma-addobservacion', 'proforma-addobservacion-u', 'proforma-deleteitem',
        'proforma-deleteitemu', 'proforma-deletevendedor-u', 'proforma-updatedescuento', 'proforma-updatedescuentou',
        'proforma-vendedor2-u', 'proforma-updatecantidad', 'proforma-updatecantidadu', 'proforma-getpreciototal',
        'proforma-getpreciototalu', 'proforma-regresarborrar', 'proforma-deleteall', 'proforma-deletecliente-u',
        'proforma-cliente2-u', 'proforma-clientenew-u', 'proforma-regresarborrar-u', 'proforma-report',
        'proforma-enviar_correo_proforma', 'proforma-busqueda_producto',
        /*Pre factura*/
        'cabeza-prefactura-view','cabeza-prefactura-regresarborrar-u','cabeza-prefactura-tabla',
        'cabeza-prefactura-regresarborrar','cabeza-prefactura-regresarborrar-caja','cabeza-prefactura-activar_btn_facturacion',
        'cabeza-prefactura-cliente2','cabeza-prefactura-cliente2-u','cabeza-prefactura-clientenew',
        'cabeza-prefactura-sincliente','cabeza-prefactura-sincliente_u','cabeza-prefactura-clientenew-u',
        'cabeza-prefactura-deletecliente','cabeza-prefactura-deletecliente-u','cabeza-prefactura-vendedor2',
        'cabeza-prefactura-vendedor2-u','cabeza-prefactura-deletevendedor','cabeza-prefactura-deletevendedor-u',
        'cabeza-prefactura-addpago','cabeza-prefactura-addpago-u','cabeza-prefactura-addmedio','cabeza-prefactura-addmedio-u',
        'cabeza-prefactura-addcom','cabeza-prefactura-addcom-u','cabeza-prefactura-addcuenta-u',
        'cabeza-prefactura-addfechadepo-u','cabeza-prefactura-addobservacion','cabeza-prefactura-addobservacion-u',
        'cabeza-prefactura-compruebatipopago','cabeza-prefactura-deletepago','cabeza-prefactura-deletepago-u',
        'cabeza-prefactura-additem','cabeza-prefactura-additemu','cabeza-prefactura-detalle',
        'cabeza-prefactura-detalle_update','cabeza-prefactura-deleteitem','cabeza-prefactura-deleteitemu',
        'cabeza-prefactura-updatecantidad','cabeza-prefactura-updatecantidadu','cabeza-prefactura-updatedescuento',
        'cabeza-prefactura-updatedescuentou','cabeza-prefactura-addpro','cabeza-prefactura-getpreciototal',
        'cabeza-prefactura-getpreciototalu','cabeza-prefactura-caja', 'cabeza-prefactura-habilitar_ot',
        'cabeza-prefactura-deleteall','cabeza-prefactura-complete','cabeza-prefactura-completerminado',
        'cabeza-prefactura-complete-u','cabeza-prefactura-cuentas','cabeza-prefactura-cancelar-factura',
        'cabeza-prefactura-busqueda_producto','cabeza-prefactura-addagentecomision','cabeza-prefactura-deleteagente',
        'cabeza-prefactura-addagentecomision-u','cabeza-prefactura-deleteagente-u', 'cabeza-prefactura-ignorar', 'cabeza-prefactura-ignorar_u',
        'cabeza-prefactura-datos_ot', 'cabeza-prefactura-datos_otu', 'cabeza-prefactura-report', 'cabeza-prefactura-modificar_linea_cr', 'cabeza-prefactura-modificar_linea_up',
        'cabeza-prefactura-crear_apartado', 'cabeza-prefactura-apartados', 'cabeza-prefactura-mov_apartados', 'cabeza-prefactura-agregar_mov_apartado',
        'cabeza-prefactura-imprimir_abono_apartado', 'cabeza-prefactura-condicion_venta', 'cabeza-prefactura-aplicardescuentogeneral_cr', 'cabeza-prefactura-aplicardescuentogeneral_up',
        'cabeza-prefactura-agregar_monto_pago', 'cabeza-prefactura-obtener_pago', 'cabeza-prefactura-completando_facturacion',
        /*Caja y facturacion*/
        'cabeza-caja-view', 'cabeza-caja-create', 'cabeza-caja-condicion_venta', 'cabeza-caja-lista_pendiente',
        'cabeza-caja-update', 'cabeza-caja-addpago', 'cabeza-caja-datospago','cabeza-caja-delete_medio_pago',
        'cabeza-caja-footer', 'cabeza-caja-obtener_correo', 'cabeza-caja-agregar_monto_pago', 'cabeza-caja-consulta_estado_hacienda',
        'cabeza-caja-ticket', 'cabeza-caja-orden', 'cabeza-caja-report', 'cabeza-caja-factura_electronica',
        /*Facturas del día*/
        'facturas-dia-view','facturas-dia-busqueda_factura','facturas-dia-busqueda_factura_simple','facturas-dia-report',
        'facturas-dia-ticket','facturas-dia-footer', 'facturas-dia-enviar_correo_factura_movimiento',
        'facturas-dia-xml','facturas-dia-pdf','facturas-dia-movimientos','facturas-dia-agrega_movimiento',
        'facturas-dia-agrega_nota', 'cabeza-caja-confirmar_facturas_hacienda',
        //'facturas-dia-enviar_correo_movimiento',
        /*Cuentas por cobrar*/
        'movimiento-credito-view','movimiento-credito-create','movimiento-credito-update','movimiento-credito-delete',
        'movimiento-credito-addcliente','movimiento-credito-cargardatos','movimiento-credito-calcu_nuevo_saldo',
        'movimiento-credito-limpiar','movimiento-credito-busqueda_cliente','movimiento-credito-facturafecha',
        'movimiento-credito-inyectardatosamanipular','movimiento-credito-agrega_nota_credito', 'movimiento-credito-agregar_abono',
        'movimiento-credito-actualiza_modal_credpen','movimiento-credito-actualizar_nota_credito','movimiento-credito-deleteitem_cr_pen',
        'movimiento-credito-aplicarnotasdecredito_fact_grupal','movimiento-credito-desaplicarnotasdecredito_fact_grupal',
        'movimiento-credito-obtenereferenciacancelacion', 'movimiento-credito-consulta_modal_movi',
        'movimiento-credito-imprimir_movimiento_credito','movimiento-credito-obtener_facturas_marcadas',
        'movimiento-credito-obtener_facturas_marcadas_regreso', 'movimiento-credito-movimientos',
        'movimiento-credito-cancelacionexitosa','movimiento-credito-inyectarrecibo','movimiento-credito-consulta_recibo',
        'movimiento-credito-consultar_todos_recibos','movimiento-credito-imprimir_facturas','movimiento-credito-imprimir_recibo',
        'movimiento-credito-notificar_estado_cuenta','movimiento-credito-imprimir_estado_cuenta','movimiento-credito-enviar_estado_cuenta',
        'movimiento-credito-filtro_facturas_canceladas','movimiento-credito-inyectarfactura',
        /*Categoria gastos*/
        'categoria-gastos-view',
        /*Tipos de gastos*/
        'tipos-gastos-view',
        /*Proveedores gastos*/
        'proveedores-gastos-view',
        /*Registro de gastos*/
        'gastos-view','gastos-create','gastos-update','gastos-delete', 'gastos-enviar_xml_not', 'gastos-renviar_xml_not',
        'gastos-addmedio-u','gastos-obtener_numero_cuenta','gastos-aplicar_gasto_seleccionado', 'gastos-pdf_resolucion',
        /*Recepcion general hacienda*/
        'recepcion-hacienda-general-get_file','recepcion-hacienda-general-elimina_recepcion','recepcion-hacienda-general-enviar_xml_not','recepcion-hacienda-general-pdf_resolucion','recepcion-hacienda-general-view',
        'recepcion-hacienda-general-renviar_xml_not',
        /*Reportes*/
        'reporte-index','reporte-rbancos_m_movimiento_libros','reporte-ibancos_m_movimiento_libros',
        'reporte-ibancos_m_cuentas_bancarias','reporte-rcaja_m_venta_detalle','reporte-icaja_m_venta_detalle', 'reporte-rcaja_m_desechadas','reporte-icaja_m_desechadas',
        'reporte-rcaja_m_venta_balance_caja','reporte-icaja_m_venta_balance_caja','reporte-rcaja_m_venta_gra_ex',
        'reporte-icaja_m_venta_gra_ex','reporte-rcaja_m_venta_cuen_cobrar','reporte-icaja_m_venta_cuen_cobrar','reporte-rcaja_m_venta_abonos',
        'reporte-icaja_m_venta_abonos','reporte-rcaja_m_venta_ndebito','reporte-icaja_m_venta_ndebito',
        'reporte-rcaja_m_venta_ncredito','reporte-icaja_m_venta_ncredito','reporte-rcaja_m_d151',
        'reporte-icaja_m_d151','reporte-rcaja_m_pagos_x_caja','reporte-icaja_m_pagos_x_caja','reporte-rcaja_m_resumen_comisiones','reporte-icaja_m_resumen_comisiones','reporte-rcaja_m_detalle_comisiones','reporte-icaja_m_detalle_comisiones',
        'reporte-rcaja_m_apartado_lista', 'reporte-icaja_m_apartado_lista', 'reporte-rcaja_m_apartado_cancelado', 'reporte-icaja_m_apartado_cancelado',
        'reporte-rcompras_m_orden_compra_x_proveedor','reporte-icompras_m_orden_compra_x_proveedor','reporte-rcompras_m_resumen_compras',
        'reporte-icompras_m_resumen_compras','reporte-rcompras_m_compras_gra_ex','reporte-icompras_m_compras_gra_ex',
        'reporte-rcompras_m_detalle_compras','reporte-icompras_m_detalle_compras',
        'reporte-iinventario_m_costo','reporte-rinventario_m_costo_categoria','reporte-iinventario_m_costo_categoria',
        'reporte-rinventario_m_x_categoria','reporte-iinventario_m_x_categoria','reporte-rinventario_m_x_debajo_minimo',
        'reporte-iinventario_m_x_debajo_minimo','reporte-rprovee_m_resumen_cuenta_pagar','reporte-iprovee_m_resumen_cuenta_pagar',
        'reporte-rprovee_m_notas_debito','reporte-iprovee_m_notas_debito','reporte-rprovee_m_notas_credito',
        'reporte-iprovee_m_notas_credito','reporte-rprovee_m_d151','reporte-iprovee_m_d151',
        'reporte-rfuncionario_m_resu_ventas','reporte-ifuncionario_m_resu_ventas','reporte-rfuncionario_m_resu_notas_credito',
        'reporte-ifuncionario_m_resu_notas_credito','reporte-iorden_servicio_m_pendiente','reporte-rorden_servicio_m_resumen_meca',
        'reporte-iorden_servicio_m_resumen_meca','reporte-rorden_servicio_m_detalle_meca','reporte-iorden_servicio_m_detalle_meca',
        'reporte-icaja_m_ot_resumen', 'reporte-rcaja_m_ot_detalle', 'reporte-icaja_m_ot_detalle' , 'reporte-rprovee_m_tramites' ,'reporte-iprovee_m_tramites',
        'reporte-rinventario_m_producto_no_vendido','reporte-iinventario_m_producto_no_vendido','reporte-rcaja_m_d151_detalle','reporte-icaja_m_d151_detalle',
        'reporte-rprovee_m_d151_detalle','reporte-iprovee_m_d151_detalle', 'reporte-rcaja_m_recepcion_hacienda_ge',
        /*este metodo se usa para cargar los clientes de un periodo fiscal en el reporte d151 detalle*/
        'reporte-cargar-cliente-periodo', 'reporte-icaja_m_recepcion_hacienda_ge',
        /*este metodo se usa para cargar los provvedores de un periodo fiscal en el reporte d151 detalle de proveedores*/
        'reporte-cargar-proveedor-periodo',
        /*metodos para exportar a excel*/
        'reporte-rcaja_m_d151_detalle_export',
        /*devoluciones producto*/
        'devoluciones-producto-view','devoluciones-producto-create','devoluciones-producto-update','devoluciones-producto-delete','devoluciones-producto-cargar-factura','devoluciones-producto-devolver_producto','devoluciones-producto-cargar_todos','devoluciones-producto-descartar_producto',
        'devoluciones-producto-aplicar_devolucion','devoluciones-producto-re_aplicar_devolucion','devoluciones-producto-enviar_correo_factura_movimiento','devoluciones-producto-pdf','devoluciones-producto-refrescar','devoluciones-producto-imprimir_movimiento_credito',
        'devoluciones-producto-imprimir_devoluciones', 'devoluciones-producto-consultar_estados_movimiento',
        /*bancos*/
        'bancos-index','bancos-view','bancos-create','bancos-update','bancos-delete',
        /*marcas-produco*/
        'marcas-producto-view','marcas-producto-create','marcas-producto-update','marcas-producto-delete','marcas-producto-actualiza_marca',
      ];

        if (in_array($operacion, $permitirSiempre)) {
            return true;
        }
       // $admin = (isset(Yii::app()->user->tipo_usuario) and Yii::app()->user->tipo_usuario == 'Administrador') ? true : false ;
        if (!AccessHelpers::getAcceso($operacion)) {
            echo $this->render('/site/nopermitido');
            return false;
        }

        return true;
    }

}
