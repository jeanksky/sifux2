<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\FacturasDia;
use backend\models\Factun;
use backend\models\EncabezadoCaja;
use backend\models\search\FacturasDiaSearch;
use yii\web\Controller;
use backend\models\Compra_view;//factura para la vista
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Empresa;
use backend\models\Clientes;//para obtener el cliente
use backend\models\Funcionario;
use backend\models\FormasPago;//
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
use backend\models\MovimientoCobrar;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\UnidadMedidaProducto;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\EncabezadoPrefactura;
use backend\models\HistorialEnvioEmail;
//use kartik\mpdf\Pdf;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;
use yii\filters\AccessControl;

require("mpdf/mpdf.php");
//use mPDF;
/**
 * FacturasDiaController implements the CRUD actions for FacturasDia model.
 */
class FacturasDiaController extends BaseController
{

    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'only' => ['index','view','create','update','delete',
                        'busqueda_factura','busqueda_factura_simple','report',
                        'footer', 'enviar_correo_factura_movimiento',
                        'xml','pdf','movimientos','agrega_movimiento',
                        'agrega_nota'],
              'rules' => [
                [
                  'actions' => ['index','view','create','update','delete',
                              'busqueda_factura','busqueda_factura_simple','report',
                              'footer', 'enviar_correo_factura_movimiento',
                              'xml','pdf','movimientos','agrega_movimiento',
                              'agrega_nota'],
                  'allow' => true,
                  'roles' => ['@'],
                ]
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FacturasDia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacturasDiaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FacturasDia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FacturasDia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FacturasDia();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FacturasDia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FacturasDia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FacturasDia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FacturasDia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FacturasDia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //descargar xml de la factura
    public function actionXml($clave_fe, $tipo, $docum)
    {
      //$model = FacturasDia::findOne($idFactura);
        if ($clave_fe > 0) {

          $consultar_factun = new Factun();
          if ($docum == 'NC') {
            $documento = MovimientoCobrar::find()->Where(['clave_mov'=>$clave_fe])->One();
            $clave = $documento->clave_mov;
            if ($tipo=='doc') {
              $api_xml = $documento->xml_mov;
              $docum = 'NC_';
            } else {
              $api_xml = $documento->xml_resp;
              $docum .= 'RESP_';
            }
          } else {
            $documento = FacturasDia::find()->Where(['clave_fe'=>$clave_fe])->One();
            $clave = $documento->clave_fe;
            if ($tipo=='doc') {
              $api_xml = $documento->xml_fe;
              $docum = 'FE_';
            } else {
              $api_xml = $documento->xml_resp;
              $docum .= 'RESP_';
            }
          }




            $result_xml = $api_xml;//consultamos el xml
            $archivo_xml = $docum.$clave.".xml";//creamos el nombre del archivo xml, y como de aqui se obtiene no se le dice en que directorio esta
            $archivo = fopen($archivo_xml, "a+");//creamos el archivo xml y con permiso de lectura y escritura
            fwrite($archivo, $result_xml);//escribimos los datos xml en el archivo
            fclose($archivo);//cerramos el archivo
            //lo siguiente es para imprimir el archivo en el navegador
            header("Content-type: application/octet-stream");
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=\"$archivo_xml\"\n");//imprimimos el archivo
            readfile($archivo_xml);//se lee el archivo para mostrarlo
            unlink($archivo_xml);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Se está descargando el XML.');
            return $this->redirect(['index']);
        } else {
          Yii::$app->getSession()->setFlash('error', '<span class="fa fa-times-circle"></span> <strong>Error!</strong> No se encuentra el XML.');
          return $this->redirect(['index']);
        }
    }

    //funcion para imprimir el Pdf
    public function actionPdf($idFactura, $id_factun_mov)
    {
      $consultar_factun = new Factun();
      $model = FacturasDia::findOne($idFactura);
      $numero_factura = '';
      $estado_hacienda = '';
      if ($model->factun_id > -1) {
        $clave = $model->clave_fe;
        $numero_factura = $model->fe;
        $estado_hacienda = $model->estado_hacienda;
      }

      $pdf_html = $this->pdf_html($model, $clave, $numero_factura, $id_factun_mov);
      $archivo_pdf = 'Factura No. '.$model->idCabeza_Factura.'.pdf';
      $stylesheet = file_get_contents('css/style_print.css');
      $caja_pdf = new EncabezadoCaja();
      $caja_pdf->_pdf_show($model->idCabeza_Factura, $stylesheet, $pdf_html, $archivo_pdf);

      //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Se está descargando el PDF.');
      //return $this->redirect(['index']);
    }

    //me busca las facturas en base de datos
    public function actionBusqueda_factura(){
        if(Yii::$app->request->isAjax){
            $consultaBusqueda_cod = strval(Yii::$app->request->post('valorBusqueda_cod'));
            $consultaBusqueda_fec = strval(Yii::$app->request->post('valorBusqueda_fec'));
            $consultaBusqueda_fec = strval(date('Y-m-d', strtotime( $consultaBusqueda_fec )));
            $consultaBusqueda_cli = strval(Yii::$app->request->post('valorBusqueda_cli'));
            $consultaBusqueda_est = strval(Yii::$app->request->post('valorBusqueda_est'));
             //Filtro anti-XSS
            $caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
            $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
            $consultaBusqueda_cod = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cod);
            $consultaBusqueda_fec = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_fec);
            $consultaBusqueda_cli = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cli);
            $consultaBusqueda_est = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_est);

            $mensaje = "";
            $consultaBusqueda_cod_ = $consultaBusqueda_fec_ = $consultaBusqueda_cli_ = $consultaBusqueda_est_ = "";
            //Comprueba si $consultaBusqueda está seteado
                    if (isset($consultaBusqueda_cod) && isset($consultaBusqueda_fec) && isset($consultaBusqueda_cli) && isset($consultaBusqueda_est)) {
                        //$consultaBusquedalimpia = "%".$consultaBusqueda_cod."%".$consultaBusqueda_fec."%".$consultaBusqueda_cli."%".$consultaBusqueda_est."%";

                        /*$consultaBusquedalimpia_des =
                        $consultaBusquedalimpia_ubi = */
                        $consultaBusqueda_cod_ = "%".$consultaBusqueda_cod./*"%".$consultaBusqueda_fec.*/"%".$consultaBusqueda_cli."%".$consultaBusqueda_est."%";

                        $consultaBusqueda_cli_ = "%".$consultaBusqueda_cod./*"%".$consultaBusqueda_fec.*/"%".$consultaBusqueda_cli."%".$consultaBusqueda_est."%";
                        $consultaBusqueda_est_ = "%".$consultaBusqueda_cod./*"%".$consultaBusqueda_fec.*/"%".$consultaBusqueda_cli."%".$consultaBusqueda_est."%";
                        if ($consultaBusqueda_cod=="" && $consultaBusqueda_cli=="" && $consultaBusqueda_est == "" ) {
                            $consultaBusqueda_cod_ = $consultaBusqueda_cli_ = $consultaBusqueda_est_ = "";
                            $consultaBusqueda_fec_ = "%".$consultaBusqueda_fec."%";
                        }

                        //consulta a la base de datos que debuelve las facturas de clientes ya registrados
                        $sql = "SELECT idCabeza_Factura, factun_id, clave_fe, estado_hacienda, fe, fecha_inicio, f.idCliente AS cliente, estadoFactura
                        FROM tbl_encabezado_factura f INNER JOIN tbl_clientes c ON f.idCliente = c.idCliente
                        WHERE fecha_emision LIKE :like_fecha
                        OR estadoFactura LIKE :like_estado
                        OR idCabeza_Factura LIKE :like_factura
                        OR c.nombreCompleto LIKE :like_cliente
                        ORDER BY idCabeza_Factura DESC
                        LIMIT 250";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindValue(":like_factura", $consultaBusqueda_cod_);
                        //$command->bindValue(":likelim", $consultaBusquedalimpia);
                        $command->bindValue(":like_fecha", $consultaBusqueda_fec_);
                        $command->bindValue(":like_cliente", $consultaBusqueda_cli_);
                        $command->bindParam(":like_estado", $consultaBusqueda_est_);
                        $consulta = $command->queryAll();
                        //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                        //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                            //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                            //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                            //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle

                            //recorre la consulta
                            foreach($consulta as $resultados){
                               $cliente_mostrar = "";
                                if($cliente = Clientes::find()->where(['idCliente'=>$resultados['cliente']])->one())
                                {
                                    $cliente_mostrar = Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$resultados['cliente']);
                                }else{
                                    if ($resultados['cliente']=="")
                                    $cliente_mostrar = 'Cliente no asignado';
                                    else
                                    $cliente_mostrar = $resultados['cliente'];
                                }
                                $crea_movimiento = $xml = '';
                                if ($resultados['factun_id'] > -1) {
                                  $crea_movimiento = Html::a('', ['#'], [
                                  'class'=>'fa fa-file-text-o fa-2x',
                                  'id' => 'activity-nota_credito',
                                  'data-toggle' => 'modal',
                                  'data-target' => '#modalNotacredito',
                                  'data-url' => Url::to(['facturas-dia/movimientos', 'id' => $resultados['idCabeza_Factura']]),
                                  'data-pjax' => '0',
                                  'title' => Yii::t('app', 'Consulta los movimientos')]);
                                  $clave_fe = $resultados["clave_fe"];
                                  $xml = Html::a('<span class=""></span>', ['facturas-dia/xml', 'clave_fe'=>$clave_fe, 'tipo'=>'doc', 'docum'=>'FA'], [
                                    'class'=>'fa fa-file-code-o fa-2x',
                                    'id' => 'xml',
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Descargar xml'),
                                  ]);
                                  $xml_res = Html::a('<span class=""></span>', ['facturas-dia/xml', 'clave_fe'=>$clave_fe, 'tipo'=>'res', 'docum'=>'FA'], [
                                                        'class'=>'fa fa-code fa-2x',
                                                        'id' => 'xml_res',
                                                        'data-pjax' => '0',
                                                        'title' => Yii::t('app', 'Descargar xml respuesta de hacienda'),
                                                      ]);
                                } else {
                                  $crea_movimiento = Html::a('', ['#'], [
                                          'class'=>'fa fa-file-text-o fa-2x',
                                          'id' => 'activity-nota_credito',
                                          'data-toggle' => 'modal',
                                          //'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
                                          'data-target' => '#modalNotacredito',
                                          'data-url' => Url::to(['facturas-dia/movimientos', 'id' => $resultados['idCabeza_Factura']]),
                                          'data-pjax' => '0',
                                          'title' => Yii::t('app', 'Consulta y creacion de notas de credito (SOLO PARA REGIMEN SIMPLIFICADO - 25514-H)')]);
                                }
                                $mostrarFactura = Html::a('', ['#'], [
                                'class'=>'fa fa-eye fa-2x',
                                'id' => 'activity-index-link-factura',
                                'data-toggle' => 'modal',
                                'onclick'=>'javascript:agrega_footer('.$resultados['idCabeza_Factura'].')',
                                'data-target' => '#modalFactura',
                                'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $resultados['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Procede a mostrar la factura')]).
                                '&nbsp;&nbsp;'.$crea_movimiento.
                                '&nbsp;&nbsp;'.$xml.'&nbsp;&nbsp;'.$xml_res.
                                '&nbsp;&nbsp;'.Html::a('<span class=""></span>', ['facturas-dia/pdf', 'idFactura'=>$resultados['idCabeza_Factura'], 'id_factun_mov'=>''], [
                                'class'=>'fa fa-file-pdf-o fa-2x',
                                'id' => 'pdf',
                                'target'=>'_blank',
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Convertir a PDF'),
                                ]);


                                $idCabeza_Factura = $resultados['idCabeza_Factura'];
                                $fecha_inicio = date('d-m-Y', strtotime( $resultados['fecha_inicio'] ));
                                $estadoFactura = $resultados['estadoFactura'];
                                //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';

                                //Output
                                $suma_monto_nc = MovimientoCobrar::find()
                                ->where(['idCabeza_Factura'=>$idCabeza_Factura])
                                ->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])
                                ->sum('monto_movimiento');
                                $nc = '';
                                $nc = $str_d = $str_i = '';
                                if ($suma_monto_nc > 0) {
                                  $nc = ' <span class="label label-warning" style="font-size:12pt;">Nota de Crédito</span> ';
                                  //$str_i = '<strike>';
                                  //$str_d = '</strike>';
                                }
                          //consulta estado de FE
                          $estado_hacienda = '';
                          if ($resultados['factun_id'] > 0) {
                            if ($resultados['estado_hacienda']=='01') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-success" style="font-size:12pt;">Aceptado por Hacienda</span>';
                            elseif ($resultados['estado_hacienda']=='02') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-primary" style="font-size:12pt;">Recibida por Hacienda</span>';
                            elseif ($resultados['estado_hacienda']=='03') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-danger" style="font-size:12pt;">Rechazada por Hacienda</span>';
                            elseif ($resultados['estado_hacienda']=='04') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-info" style="font-size:12pt;">Enviado a Hacienda</span>';
                            elseif ($resultados['estado_hacienda']=='05') {
                              $estado_hacienda = $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-warning" style="font-size:12pt;">EN ESPERA</span>';
                            }
                          } else {
                            $estado_hacienda = ' &nbsp;&nbsp;&nbsp;<span class="label label-default" style="font-size:12pt;">NO ES FACTURA ELECTRÓNICA</span>';
                          }

                         $mensaje .='<tbody><tr>
                                       <td width="45%">&nbsp&nbsp'.$str_i.$idCabeza_Factura.$str_d.' - '.$estado_hacienda.$nc.'</td>
                                       <td width="10%">'.$str_i.$fecha_inicio.$str_d.'</td>
                                       <td width="25%" align="left">'.$str_i.$cliente_mostrar.$str_d.'</td>
                                       <td width="5%" align="center">'.$str_i.$estadoFactura.$str_d.'</td>
                                       <td width="15%" align="right">'.$mostrarFactura.'</td>
                                    </tr></tbody>';

                            }



                    //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
                }
                echo $mensaje;//muestro la lista de facturas en una tabla
        }
    }

    //esta busqueda obtiene los clientes que no son registrados
    public function actionBusqueda_factura_simple(){
        if(Yii::$app->request->isAjax){
            $consultaBusqueda_cod = strval(Yii::$app->request->post('valorBusqueda_cod'));
            $consultaBusqueda_fec = strval(Yii::$app->request->post('valorBusqueda_fec'));
            $consultaBusqueda_fec = strval(date('Y-m-d', strtotime( $consultaBusqueda_fec )));
            $consultaBusqueda_cli = strval(Yii::$app->request->post('valorBusqueda_cli'));
            $consultaBusqueda_est = strval(Yii::$app->request->post('valorBusqueda_est'));
             //Filtro anti-XSS
            $caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
            $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
            $consultaBusqueda_cod = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cod);
            $consultaBusqueda_fec = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_fec);
            $consultaBusqueda_cli = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cli);
            $consultaBusqueda_est = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_est);

            $mensaje = "";
            $consultaBusqueda_cod_ = $consultaBusqueda_fec_ = $consultaBusqueda_cli_ = $consultaBusqueda_est_ = "";
            //Comprueba si $consultaBusqueda está seteado
                    if (isset($consultaBusqueda_cod) && isset($consultaBusqueda_fec) && isset($consultaBusqueda_cli) && isset($consultaBusqueda_est)) {
                        $consultaBusqueda_cod_ = "%".$consultaBusqueda_cod."%".$consultaBusqueda_cli."%".$consultaBusqueda_est."%";

                        $consultaBusqueda_cli_ = "%".$consultaBusqueda_cod."%".$consultaBusqueda_cli."%".$consultaBusqueda_est."%";
                        $consultaBusqueda_est_ = "%".$consultaBusqueda_cod."%".$consultaBusqueda_cli."%".$consultaBusqueda_est."%";
                        if ($consultaBusqueda_cod=="" && $consultaBusqueda_cli=="" && $consultaBusqueda_est == "" ) {
                            $consultaBusqueda_cod_ = $consultaBusqueda_cli_ = $consultaBusqueda_est_ = "";
                            $consultaBusqueda_fec_ = "%".$consultaBusqueda_fec."%";
                        }

                        //Selecciona todo de la tabla mmv001
                        //donde el nombre sea igual a $consultaBusqueda,
                        //o el apellido sea igual a $consultaBusqueda,
                        //o $consultaBusqueda sea igual a nombre + (espacio) + apellido
                        $sql = "SELECT idCabeza_Factura, factun_id, clave_fe, fecha_inicio, estado_hacienda, fe, idCliente AS cliente, estadoFactura
                        FROM tbl_encabezado_factura
                        WHERE fecha_emision LIKE :like_fecha
                        OR estadoFactura LIKE :like_estado
                        OR idCabeza_Factura LIKE :like_factura
                        OR idCliente LIKE :like_cliente
                        ORDER BY idCabeza_Factura DESC
                        LIMIT 250 ";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindValue(":like_factura", $consultaBusqueda_cod_);
                        //$command->bindValue(":likelim", $consultaBusquedalimpia);
                        $command->bindValue(":like_fecha", $consultaBusqueda_fec_);
                        $command->bindValue(":like_cliente", $consultaBusqueda_cli_);
                        $command->bindParam(":like_estado", $consultaBusqueda_est_);
                        $consulta = $command->queryAll();
                        //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                            foreach($consulta as $resultados){
                               $cliente_mostrar = ""; $filtro = false;
                                if($cliente = Clientes::find()->where(['idCliente'=>$resultados['cliente']])->one())
                                {
                                    $cliente_mostrar = Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$resultados['cliente']);
                                }else{
                                    $filtro = true;
                                    if ($resultados['cliente']=="")
                                    $cliente_mostrar = 'Cliente no asignado';
                                    else
                                    $cliente_mostrar = $resultados['cliente'];
                                }
                                $crea_movimiento = $xml = '';
                                if ($resultados['factun_id'] > 0) {
                                  $crea_movimiento = Html::a('', ['#'], [
                                  'class'=>'fa fa-file-text-o fa-2x',
                                  'id' => 'activity-nota_credito',
                                  'data-toggle' => 'modal',
                                  'data-target' => '#modalNotacredito',
                                  'data-url' => Url::to(['facturas-dia/movimientos', 'id' => $resultados['idCabeza_Factura']]),
                                  'data-pjax' => '0',
                                  'title' => Yii::t('app', 'Consulta y creacion de un movimiento')]);
                                  $xml = Html::a('<span class=""></span>', ['facturas-dia/xml', 'clave_fe'=>$resultados['clave_fe'], 'tipo'=>'doc', 'docum'=>'FA'], [
                                  'class'=>'fa fa-file-code-o fa-2x',
                                  'id' => 'xml',
                                  'data-pjax' => '0',
                                  'title' => Yii::t('app', 'Descargar xml'),
                                ]);
                                } else {
                                  $crea_movimiento = Html::a('', ['#'], [
                                          'class'=>'fa fa-file-text-o fa-2x',
                                          'id' => 'activity-nota_credito',
                                          'data-toggle' => 'modal',
                                          //'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
                                          'data-target' => '#modalNotacredito',
                                          'data-url' => Url::to(['facturas-dia/movimientos', 'id' => $resultados['idCabeza_Factura']]),
                                          'data-pjax' => '0',
                                          'title' => Yii::t('app', 'Consulta y creacion de notas de credito (SOLO PARA REGIMEN SIMPLIFICADO - 25514-H)')]);
                                }
                                $mostrarFactura = Html::a('', ['#'], [
                                'class'=>'fa fa-eye fa-2x',
                                'id' => 'activity-index-link-factura',
                                'data-toggle' => 'modal',
                                'onclick'=>'javascript:agrega_footer('.$resultados['idCabeza_Factura'].')',
                                'data-target' => '#modalFactura',
                                'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $resultados['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Procede a mostrar la factura')]).
                                '&nbsp;&nbsp;'.$crea_movimiento.
                                '&nbsp;&nbsp;'.$xml.
                                '&nbsp;&nbsp;'.Html::a('<span class=""></span>', ['facturas-dia/pdf', 'idFactura'=>$resultados['idCabeza_Factura'], 'id_factun_mov'=>''], [
                                'class'=>'fa fa-file-pdf-o fa-2x',
                                'id' => 'pdf',
                                'target'=>'_blank',
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Convertir a PDF'),
                                ]);


                                $idCabeza_Factura = $resultados['idCabeza_Factura'];
                                $fecha_inicio = date('d-m-Y', strtotime( $resultados['fecha_inicio'] ));
                                $estadoFactura = $resultados['estadoFactura'];

                                //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';

                                //Output
                                $suma_monto_nc = MovimientoCobrar::find()
                                ->where(['idCabeza_Factura'=>$idCabeza_Factura])
                                ->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])
                                ->sum('monto_movimiento');
                                $nc = $str_d = $str_i = '';
                                if ($suma_monto_nc > 0) {
                                  $nc = ' <span class="label label-warning" style="font-size:12pt;">NC</span> ';
                                  $str_i = '<strike>';
                                  $str_d = '</strike>';
                                }

                                //consulta estado de FE
                                $estado_hacienda = '';
                                if ($resultados['factun_id'] > 0) {
                                  if ($resultados['estado_hacienda']=='01') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-success" style="font-size:12pt;">Aceptado por Hacienda</span>';
                                  elseif ($resultados['estado_hacienda']=='02') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-primary" style="font-size:12pt;">Recibida por Hacienda</span>';
                                  elseif ($resultados['estado_hacienda']=='03') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-danger" style="font-size:12pt;">Rechazada por Hacienda</span>';
                                  elseif ($resultados['estado_hacienda']=='04') $estado_hacienda = $resultados['fe'].' &nbsp;&nbsp;&nbsp;<span class="label label-info" style="font-size:12pt;">Enviado a Hacienda</span>';
                                  elseif ($resultados['estado_hacienda']=='05') {
                                    $estado_hacienda = $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-warning" style="font-size:12pt;">EN ESPERA</span>';
                                  }
                                } else {
                                  $estado_hacienda = ' &nbsp;&nbsp;&nbsp;<span class="label label-default" style="font-size:12pt;">NO ES FACTURA ELECTRÓNICA</span>';
                                }
                          if ($filtro == true) {
                            $mensaje .='<tbody><tr>
                                         <td width="45%">&nbsp&nbsp'.$str_i.$idCabeza_Factura.$str_d.' - '.$estado_hacienda.$nc.'</td>
                                         <td width="10%">'.$str_i.$fecha_inicio.$str_d.'</td>
                                         <td width="30%" align="left">'.$str_i.$cliente_mostrar.$str_d.'</td>
                                         <td width="5%" align="center">'.$str_i.$estadoFactura.$str_d.'</td>
                                         <td width="10%" align="right">'.$mostrarFactura.'</td>
                                       </tr></tbody>';
                          }

                            }
                }
                echo $mensaje;//muestro la lista de faturas en una tabla
        }
    }

    //accion que me imprime para hoja normal de carta
    public function actionReport() {
        $id = Compra_view::getCabezaFactura();
        $factura = $this->findModel($id);


        $content = $this->renderAjax('report',['model' => $factura ]);
       /* $content = $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);*/

       $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Factura</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";

                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';

        echo $imprimir;

        Compra_view::setCabezaFactura(null);
    }//fin actionReport

    //accion para imprimir por tickets
    public function actionTicket($id)
    {
        return $this->render('ticket', ['id' => $id]);
        /*$empresaimagen = new Empresa();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $id = Compra_view::getCabezaFactura();
        //coneccion con la impresora
        $equipo = 'Dell300914';
        $impresora = 'TSP100';
        $connector = new WindowsPrintConnector("smb://".$equipo."/".$impresora);
        $printer = new Printer($connector);


        //Encabezado empresa--------------------------------------------
        //$logo = EscposImage::load($empresaimagen->getImageurl('html'), false);
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer -> text( "\n\n" . $empresa->nombre . "\n");
        $printer -> text( $empresa->localidad.' - '.$empresa->direccion . "\n" );
        $printer -> text( $empresa->sitioWeb.' / '.$empresa->email . "\n" );
        $printer -> text( $empresa->telefono.' / '. $empresa->fax . "\n\n\n" );
        $printer -> selectPrintMode();
        $printer -> feed();
        //Encabezado factura--------------------------------------------
        $modelcaja = $this->findModel($id);//vuelvo a llamar al modelo actual pero esta ves con las modificaciones que se aplicaron
        $printer -> text( 'Factura No: ' . $id ."\n");
        $printer -> text( 'Fecha: ' . $modelcaja->fecha_inicio ."\n\n");
        $printer -> text( "CLIENTE:------------------------------\n");
        if (@$cliente = Clientes::find()->where(['idCliente'=>$modelcaja->idCliente])->one()) {
            $printer -> text( "Nombre: ".$cliente->nombreCompleto . "\n");
            $printer -> text( "Telefono: ".$cliente->telefono . "\n");
            $printer -> text( "Direccion: ".$cliente->direccion . "\n");
        } else {
            $printer -> text( $modelcaja->idCliente . "\n");
        }
        $printer -> text( "--------------------------------------\n\n");

        $printer -> text( 'Estado de la factura: ' . $modelcaja->estadoFactura ."\n");

        //$fecha_final = $modelcaja->fecha_final=='' ? 'Pendiente' : $modelcaja->fecha_final ;
        //$printer -> text( 'Fecha Final: ' . $fecha_final ."\n");

        if(@$modelP = FormasPago::find()->where(['id_forma'=>$modelcaja->tipoFacturacion])->one()){
            if ($modelcaja->estadoFactura!='Crédito') {
                $printer -> text( 'Tipo de pago: ' . $modelP->desc_forma ."\n");
            } else {
                    $printer -> text( 'Fecha Vencimiento: ' . $modelcaja->fecha_vencimiento ."\n");
                }
            }

        $printer -> text( 'Observacion: ' . $modelcaja->observacion ."\n\n\n");

        //Detalle de factura--------------------------------------------
        $products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $modelcaja->idCabeza_Factura])->all();
        $detallefactura = '';
        foreach($products as $product) {
            $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
            $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
            $detallefactura .= new item($product['cantidad']. ' ' . substr($descrip,0,22), number_format(($product['precio_unitario']*$product['cantidad']),2));
        }
        //$printer -> text( "\n\n\n" );
        //Cierre de impresión
        $printer -> text( $detallefactura ."---------------------------------------------\n" );
        $printer -> text(new item("                 SUBTOTAL:", number_format($modelcaja->subtotal,2)));
        $printer -> text(new item("                 TOTAL DESCUENTO:", number_format($modelcaja->porc_descuento,2)));
        $printer -> text(new item("                 TOTAL IV:", number_format($modelcaja->iva,2)));
        $printer -> text(new item("                 TOTAL A PAGAR:",number_format($modelcaja->total_a_pagar,2))."\n\n");

        $modelF = Funcionario::find()->where(['idFuncionario'=>$modelcaja->codigoVendedor])->one();
        $printer -> text( "Facturado por: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2 . "\n\n\n\n");

        $printer -> feed(2);//Esto me permite cortar la factura en la base del papel
        $printer -> cut();
        $printer -> close();
        Compra_view::setCabezaFactura(null);
        Yii::$app->response->redirect(array('facturas-dia/index'));*/
    }//fin actionTicket

    //esta funsion me imprime el footer con los valores que necesito darle a los botones
    public function actionFooter(){
        if(Yii::$app->request->isAjax){
            $id = strval(Yii::$app->request->post('id'));//obtengo el valor de la factura para poder enviarlo en la accion del boton
            $model = $this->findModel($id);
            @$modelC = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
            $email = @$modelC->email ? $modelC->email : '';
            $footer = '<div class="col-xs-3">'.Html::input('text', '', $email, ['size' => '30', 'id' => 'email_cliente', 'class' => 'form-control input-sm', 'placeholder' =>' Dirección email']).'</div>
                  <div class="col-xs-2">'.
                  Html::a('<i class="fa fa-envelope" aria-hidden="true"></i> Enviar por email', null, [
                    'class'=>'btn',
                    'data-toggle'=>'tooltip',
										'onclick' => 'javascript:enviar_factura_correo('.$id.')',
                    'title'=>'Esta opción permite enviar la factura al cliente, si es un cliente temporal no registrado asegúrese de escribir el email antes de usar esta opción'
                ]).'<div id="notificacion_envio_correo"></div></div>';
            $footer .= '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir completo', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Imprime factura completa'
                ]).Html::a('<i class="fa fa-print"></i> Imprimir por Ticket', ['/facturas-dia/ticket', 'id'=>$id], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Imprime factura por Ticket'
                ]);
            echo $footer;
        }
    }//fin action footer

    //enviar factura al correo desde una llamada a la funcion
    public function actionEnviar_correo_factura_movimiento($tipo, $id, $email, $id_factun_mov)
    {
      $consultar_factun = new Factun();
        $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $model = $this->findModel($id);
            //sleep(1);
            if (@$model) {
              $id_factun = $model->factun_id;
              $encabezado = '<table>
                  <tbody>
                  <tr>
                  <td>
                  <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                  </td>
                  <td style="text-align: left;">
                  <strong>'.$empresa->nombre.'</strong><br>
                  <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                  <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                  <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                  <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                  <strong>Dirección:</strong> '.$empresa->email.'
                  </td>
                  <tr>
                  </tbody>
              </table>';
              $informacion_factura_cancelada = $this->renderAjax('view',['model' => $model]);
              $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
              $opcion_movimiento = $tipo=='FA' ? '' : '<h4 style="text-align:right; color: #FF0040; font-family: Segoe UI, sans-serif;">FACTURA REVOCADA CON NOTA DE CRÉDITO</h4>';
              $content = '
              <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO">
                <div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
                  <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
                    <br>
                    <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
                      <tbody><tr>
                        <td align="center" valign="top">
                          <table width="80%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                            <tbody>
                              <tr>
                                <td align="right" style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">

                                </td>
                              </tr>
                              <tr>
                                <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                                 <br>
                                  <br><br>
                                  '.$encabezado.$opcion_movimiento.'
                                  <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">ENVÍO DE FACTURA # '.$id.'<br><small> '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>
                                  '.$informacion_factura_cancelada.'
                                  <br><br><br><br><br><br><br><br><br>
                                </td>
                              </tr>
                              <tr>
                                <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                                  Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                    </table>
                  <br><br>
                  </div>
                </div>
              </div>';

              //CONSULTAR
              $clave = $numero_factura = $estado_hacienda = '';
              $stylesheet = file_get_contents('css/style_print.css');
              if ($id_factun == -1) {//sin factura electronica  |||||||||||||||||||||||||||||||||||||||||||||||||
                $pdf_html = $this->pdf_html($model, '', '', '');
                //$comprobante_electronico = 'No cuenta con comprobante electrónico';
                $archivo_pdf = 'Factura No. '.$id.'.pdf';
                /*$pdf = new mPDF();
                $pdf->title = 'Factura No. '.$id;
                //$pdf->SetHtmlHeader($comprobante_electronico);
                $pdf->WriteHTML($stylesheet,1);
                $pdf->WriteHTML($pdf_html,2);
                //$pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());
                $pdf->Output($archivo_pdf,'');*/
                $caja_pdf = new EncabezadoCaja();
                $caja_pdf->_pdf($id, $stylesheet, $pdf_html, $archivo_pdf);
                //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                $subject = "Nueva factura por compra a ".$empresa->nombre;
                //Enviamos el correo
                Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                ->setTo($email)//para
                ->setFrom($empresa->email_contabilidad)//de
                ->setSubject($subject)
                ->setHtmlBody($content)
                ->attach($archivo_pdf)//->attach($adjunto[2])//se agragan todos los adjuntos
                ->send();
                $histrial_email = new HistorialEnvioEmail();
                $histrial_email->tipo_documento = 'FA';
                $histrial_email->idDocumento = $id;
                $histrial_email->fecha_envio = date('Y-m-d H:i:s',time());
                $histrial_email->email = $email;
                $histrial_email->usuario = Yii::$app->user->identity->username;
                $histrial_email->save();
                unlink($archivo_pdf);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
                echo '<br><center><span class="label label-success" style="font-size:15pt;">Factura enviada correctamente.</span></center>';
              } else { //con factura electronica  |||||||||||||||||||||||||||||||||||||||||||||||||
                //$api_consultar = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/'.$id_factun;
                //$api_xml = @$id_factun_mov ? 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/XML/'.$id_factun_mov : 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/XML/'.$id_factun;
                //obtengo los datos de la factura desde factun
                //$result_factura = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_consultar, 'GET'));
                $clave = $model->clave_fe;
                $numero_factura = $model->fe;
                $estado_hacienda = $model->estado_hacienda;
                $pdf_html = $this->pdf_html($model, $clave, $numero_factura, $id_factun_mov);
                if ($estado_hacienda=='01') {
                  //$comprobante_electronico = 'Comprobante electrónico: ' . $numero_factura;
                  $archivo_xml = $tipo=='FA' ? 'FE_'.$clave.".xml" : $tipo.'_'.$clave.".xml";//creamos el nombre del archivo xml, y como de aqui se obtiene no se le dice en que directorio esta
                  $archivo_xml_resp = $tipo=='FA' ? 'FE_RESPUESTA_'.$clave.".xml" : $tipo.'_RESPUESTA_'.$clave.".xml";
                  $archivo_pdf = $tipo=='FA' ? $clave.'.pdf' : $tipo.'_'.$clave.'.pdf';//creamos el nombre del arhivo pdf, ...
                  //creamos el xml en el servidor (estará temporalmente)
                  $archivo = fopen($archivo_xml, "a+");//creamos el archivo xml y con permiso de lectura y escritura
                  fwrite($archivo, $model->xml_fe);//escribimos los datos xml en el archivo
                  fclose($archivo);//cerramos el archivo
                  //creamos el archivo xml de respuesta que esta en bd
                  $archivo2 = fopen($archivo_xml_resp, "a+");//creamos el archivo xml y con permiso de lectura y escritura
                  fwrite($archivo2, $model->xml_resp);//escribimos los datos xml en el archivo
                  fclose($archivo2);//cerramos el archivo
                  //creamos el Pdf en el servidor (estará temporalmente)
                  /*$pdf = new mPDF();
                  $pdf->title = 'Factura No. '.$id;
                  //$pdf->SetHtmlHeader($comprobante_electronico);
                  $pdf->WriteHTML($stylesheet,1);
                  $pdf->WriteHTML($pdf_html,2);
                  //$pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());
                  $pdf->Output($archivo_pdf,'');*/
                  //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                  $caja_pdf = new EncabezadoCaja();
                  $caja_pdf->_pdf($id, $stylesheet, $pdf_html, $archivo_pdf);
                  $subject = $tipo=='FA' ? "Nueva factura por compra a ".$empresa->nombre : 'Nueva '.$tipo.' de '.$empresa->nombre;
                  //Enviamos el correo
                  Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                  ->setTo($email)//para
                  ->setFrom($empresa->email_contabilidad)//de
                  ->setSubject($subject)
                  ->setHtmlBody($content)
                  ->attach($archivo_xml)->attach($archivo_xml_resp)->attach($archivo_pdf)//se agragan todos los adjuntos
                  ->send();
                  $histrial_email = new HistorialEnvioEmail();
                  $histrial_email->tipo_documento = 'FA';
                  $histrial_email->idDocumento = $id;
                  $histrial_email->fecha_envio = date('Y-m-d H:i:s',time());
                  $histrial_email->email = $email;
                  $histrial_email->usuario = Yii::$app->user->identity->username;
                  $histrial_email->save();
                  unlink($archivo_xml);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
                  unlink($archivo_xml_resp);
                  unlink($archivo_pdf);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
                  echo '<br><center><span class="label label-success" style="font-size:15pt;">Factura enviada correctamente.</span></center>';
                }//fin consulta si esta aprovado por hacienda
                else {
                  echo '<br><center><span class="label label-danger" style="font-size:15pt;">No puedes enviar facturas al correo sin aprobación de Hacienda.</span></center>';
                }
              }

            } //fin @$model
            //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> La cotización fué enviada correctamente.');
            //$this->redirect(['update', 'id' => $id]);
    }// fin de envio_email

    //html del pdf
    public function pdf_html($model, $clave, $numero_factura, $id_factun_mov)
    {
      $empresaimagen = new Empresa();
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $model->idCabeza_Factura])->all();
      $detalle_factura = '';
      if($products) {
        foreach($products as $position => $product) {
          $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
          $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
          $detalle_factura .= '<tr>
            <td class="no">'.$product['cantidad'].'</td>
            <td class="cod" style="font-size: 0.8em;">'.$product['codProdServicio'].'</td>
            <td class="desc" style="font-size: 0.8em;"><strong>'.$descrip.'</strong></td>
            <td class="unit" style="font-size: 0.8em;">'.number_format($product['precio_unitario'], 2).'</td>
            <td class="qty" style="font-size: 0.8em;">'.number_format($product['descuento_producto'],2).'</td>
            <td class="unit" style="font-size: 0.8em;">'.number_format($product['subtotal_iva'],2).'</td>
            <td class="total">'.number_format(($product['precio_unitario']*$product['cantidad']),2).'</td>
          </tr>';
        }
      }

      $info_cliente = $info_invoice = $tipo_documento = '';
      if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
          $tipo_documento = 'FE';
          $info_cliente = '
          <h2 class="name">'.$cliente->nombreCompleto.'<br>ID: '.$cliente->identificacion.'</h2>
          <div class="address">'.$cliente->direccion.'</div>
          <div class="email">'.$cliente->celular.' / '.$cliente->telefono.'<br><a href="mailto:'.$cliente->email.'">'.$cliente->email.'</a></div>';
      } else{
        $tipo_documento = 'TE';
          $info_cliente = '<h2 class="name">'.$model->idCliente.'</h2>';
      }

      $tipo_pago = $feven = '';
      if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
          $tipo_pago = $modelP->desc_forma;
          if ($model->estadoFactura!='Crédito') {
            $feven = '';
          }
          else {
            $feven = 'Fecha de vencimiento: '.$model->fecha_vencimiento;
          }
      }
      $observacion = '';
      //comprovamos que si el documento es un movimiento NC o ND
      if ($id_factun_mov > 0) {
        $consultar_factun = new Factun();
        $api_cosult = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Consultar/'.$id_factun_mov;
        $result_conslt = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_cosult, 'PUT'));
        $numero_mov = $clave_mov =  '';
        foreach ($result_conslt->data as $key => $value) {
          //con esto obtenemos los datos de la factura
          if ($key == 'clave') { $clave_mov = $value; }
          if ($key == 'numero_documento') { $numero_mov = $value; }//con esto obtenemos el numero de mpvimiento de hacienda para atribuircelo al archivo
        }
        $movimiento = MovimientoCobrar::find()->where(['factun_id_mov'=>$id_factun_mov])->one();
        if ($movimiento->tipmov='NC') {
          $tipo_doc = 'NCE';
          $tipo_documento = 'ID nota de crédito. '.$movimiento->idMov;
        } else {
          $tipo_doc = 'NDE';
          $tipo_documento = 'ID nota de débito. '.$movimiento->idMov;
        }
        $info_invoice = '
        <h1 style="color: #0087C3;">'.$tipo_documento.'</h1>
        <h2 class="name" style="color: #57B223;">'.$tipo_doc.': '.$numero_mov.'</h2>
        <div class="date">Fecha del movimiento: '.$movimiento->fecmov.'</div>
        <div style="font-size: 0.8em;  color: #FF0000;">Clave: '.$clave_mov.'</div>
        <h3 style="font-size: 1.0em; color: #0087C3;">Aplicada a la factura: '.$numero_factura.'</h3>';
        $observacion = '<div>Concepto:</div>
        <div class="notice">'.$movimiento->concepto.'</div>';
      } else {
        if ($model->factun_id == -1) {
          $info_invoice = '
          <h3 style="color: #0087C3;">Número interno. '.$model->idCabeza_Factura.'</h3>
          <div class="date">Fecha de emisión: '.$model->fecha_inicio.'</div>
          <div class="date">'.$feven.'</div>
          <h3 style="font-size: 1.0em; color: #0087C3;">Condición Venta: '.$model->estadoFactura.' | Tipo de Pago:	'.$tipo_pago.'</h3>';
        } else {
          $info_invoice = '
          <h3 style="color: #0087C3;">Número interno. '.$model->idCabeza_Factura.'</h3>
          <h2 class="name" style="color: #57B223;">'.$tipo_documento.': '.$numero_factura.'</h2>
          <div class="date">Fecha de emisión: '.$model->fecha_inicio.'</div>
          <div class="date">'.$feven.'</div>
          <div style="font-size: 0.8em;  color: #FF0000;">Clave: '.$clave.'</div>
          <h3 style="font-size: 1.0em; color: #0087C3;">Condición Venta: '.$model->estadoFactura.' | Tipo de Pago:	'.$tipo_pago.'</h3>';
        }
        $observacion = '<div>Observación:</div>
        <div class="notice">'.$model->observacion.'</div>';
      }



      $footer_pdf = $model->factun_id == -1 ? 'Autorizado mediante resolución 11-97 del 05/09/1997 de la D.G.T.D' : 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';
      $servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
      if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
        $servicio_restaurante = '<tr>
                         <td colspan="3"></td>
                         <td colspan="3">SERVICIO 10%: </td>
                         <td>'.number_format($servicio,2).'</td>
                     </tr>';
        $subtotal = $model->subtotal+$servicio;
        $total_a_pagar = $model->total_a_pagar+$servicio;
      } else {
        $servicio_restaurante = '';
        $subtotal = $model->subtotal;
        $total_a_pagar = $model->total_a_pagar;
      }
      return '<header class="clearfix">
            <div id="company">
              <table>
              <tr>
              <td>
              <img src="'.$empresaimagen->getImageurl('pdf').'" height="10%">
              </td>
              <td>
                <div id="company">
                  <h2 class="name">'.$empresa->nombre.' - ID: '.$empresa->ced_juridica.'</h2>
                  <div>'.$empresa->localidad.' - '.$empresa->direccion.'</div>
                  <div>'.$empresa->telefono.' / '.$empresa->fax.'</div>
                  <div><a href="mailto:'.$empresa->sitioWeb.'">'.$empresa->sitioWeb.'</a> | <a href="mailto:'.$empresa->email.'">'.$empresa->email.'</a></div>
                </div>
              </td>
              </tr>
              </table>
            </div>
      </header>
      <main>
        <div id="details" class="clearfix">
          <table class="table_info">
            <tr>
              <th class="derecha">
                <div id="client">
                  <div class="to">Factura a:</div>
                  '.$info_cliente.'
                </div>
              </th>
              <th class="izquierda">
                <div id="invoice">

                  '.$info_invoice.'

                </div>
              </th>
            </tr>
          </table>
        </div>
        <table border="0" cellspacing="0" cellpadding="0" class="table_detalle">
          <thead>
            <tr>
              <th class="no">C.</th>
              <th class="cod">COD</th>
              <th class="desc">DESCRIPCIÓN</th>
              <th class="unit">PREC UNIT</th>
              <th class="qty">DESCTO</th>
              <th class="unit">IV %</th>
              <th class="total">TOTAL</th>
            </tr>
          </thead>
          <tbody>
            '.$detalle_factura.'
          </tbody>
          <tfoot>
            '.$servicio_restaurante.'
            <tr>
              <td colspan="3"></td>
              <td colspan="3">SUBTOTAL</td>
              <td>'.number_format($subtotal,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3">TOTAL DESCUENTO</td>
              <td>'.number_format($model->porc_descuento,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3">TOTAL IV</td>
              <td>'.number_format($model->iva,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3"><strong>TOTAL A PAGAR</strong></td>
              <td><strong>'.number_format($total_a_pagar,2).'</strong></td>
            </tr>
          </tfoot>
        </table>
        <div id="thanks">¡Gracias por su preferencia!</div>
        <div id="notices">
          '.$observacion.'
        </div>
      </main>
      <footer>
        '.$footer_pdf.'
      </footer>';
    }

    //obtengo el html con informacion del movimiento
    public function actionMovimientos($id)
    {
      $model = $this->findModel($id);
      echo $this->renderAjax('movimientos', ['model' => $model]);
    }

    //Agrega nota de credito o debito
    public function actionAgrega_nota()
    {
      if(Yii::$app->request->isAjax){
        $cliente = $identificacion = $tipo_identificacion = $email = '';
          $tipo = Yii::$app->request->post('tipo');
          $concepto  = Yii::$app->request->post('concepto');
          $monto  = Yii::$app->request->post('monto');
          $idCabeza_Factura = Yii::$app->request->post('idCa');
          $bandera = 'No cancelada'; $diasCredito = '';
          $model = $this->findModel($idCabeza_Factura);
          $cliente_model = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
          if (@$cliente_model) {
            $cliente = $cliente_model->nombreCompleto;
            $identificacion = $cliente_model->identificacion;
            $tipo_identificacion = $cliente_model->tipoIdentidad == 'Física' ? '01' : '02';
            $email = $cliente_model->email;
            $diasCredito = strval($cliente_model->diasCredito);
          } else {
            $cliente = $model->idCliente;
          }
            //$modelfactura = MovimientoCredito::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one(); //Obtengo la factura
            $saldo_de_la_factura = $model->total_a_pagar; //extraigo el total a pagar de la factura

            $model_m_cobrar = new MovimientoCobrar(); //con estos procesos puedo obtener el saldo pendiente de esta factura
            $suma_monto_movimiento = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->sum('monto_movimiento');
            $saldo_fac = $model->total_a_pagar - $suma_monto_movimiento;
            //al comparar valido que las notas no superen el monto de la factura
            //lo interesante es que necesito hacer una comparacion muy exacta cuando se trata de montos iguales, por eso uso number_format()
            //como se ve a continuacion ya que en algun momento dado despues de varios procesos por abono o NC puede pasar solo por montos iguales
            //por el contrario el monto siempre será mayor al saldo de la factura
            if ($monto > $saldo_fac) {
                $notif = '<br><center><span class="label label-danger" style="font-size:15pt;">Error; Las notas de crédito superan el saldo pendiente de la factura.</span></center>';
                $arr = array('notif'=>$notif, 'id_factun'=>'', 'email'=>'');
                echo json_encode($arr);
            } else if ( (number_format($saldo_fac,2) == number_format($monto,2)) || ($saldo_fac > $monto) ) {
                //una ves confirmo que las notas no superan el monto de la factura buelvo a recorrer las notas
                //para aplicarlas cada una a la factura
                //ENVIAR MOVIMIENTO
                $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
                $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
                $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
                $condicion_venta = '01'; //01 = Contado, 02 = Crédito, 03 = Consignación, 04 = Apartado, 05 = Arrendamiento con opción de compra, 06 = Arrendamiento en función financiera, 99 = Otros
                $tipo_pago = '01';
                $fecha_documento = date("Y-m-d");
                $numero_original = strval($idCabeza_Factura);
                $plazo = '';



                 //Asigno la nota de credito en los campos que corresponden a la tabla de la base de datos
                    $monto_anterior = $saldo_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                    $saldo_pendiente = $tipo=='nc' ? $monto_anterior - $monto : $monto_anterior + $monto; //le resto o sumo el monto del movimeinto (NC o ND) con el monto anterior para saber el monto pendiente
                    $model_m_cobrar->factun_id_mov = $result_movimiento->id;
                    $model_m_cobrar->fecmov = date("Y-m-d H:i:s",time());
                    $model_m_cobrar->tipmov = $tipo=='nc' ? 'NC' : 'ND';
                    $model_m_cobrar->concepto = $concepto;
                    $model_m_cobrar->monto_anterior = $monto_anterior;
                    $model_m_cobrar->monto_movimiento = $monto;
                    $model_m_cobrar->saldo_pendiente = $saldo_pendiente;
                    $model_m_cobrar->usuario = Yii::$app->user->identity->username;
                    $model_m_cobrar->idCabeza_Factura = $idCabeza_Factura;
                    $model_m_cobrar->idCliente = $cliente;
                    if ($model_m_cobrar->save()) {//Guardo la nota de credito
                      //Creo el nuevo recibo por NC siempre y cuando la factura estuviera acfreditada
                        $recibo = new Recibos();
                        $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
                        $recibo->monto = $monto;
                        $recibo->cliente = $model->idCliente;
                        $recibo->usuario = Yii::$app->user->identity->username;
                        $recibo->tipoRecibo = 'NC';
                        $recibo->saldo_cuenta = 0.00;
                        //Creo el detalle del recibo
                        if ($recibo->save()) {
                            $rec_deta = new RecibosDetalle();//instancia para almacenar la nota de credito
                            $rec_deta->idRecibo = $recibo->idRecibo;
                            $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                            $rec_deta->tipo = 'NC';
                            $rec_deta->referencia = 'Factura: '. $idCabeza_Factura . ' - ' . $concepto;
                            $rec_deta->fecha = date("Y-m-d H:i:s",time());
                            $rec_deta->monto = $monto;
                            $rec_deta->save();
                          }



                    }
                  //vuelvo a condicionar pero esta ves si el monto es igual al saldo para así cancelar la factura
                  if ($saldo_fac == $monto) {
                      $bandera = 'Revocada';
                  }
                  $notif = '';
                  if ($bandera == 'Revocada') {
                        $notif = '<br><center><span class="label label-success" style="font-size:15pt;">Esta factura ha sido revocada y cancelada con este movimiento.</span></center>';
                      } else {
                         $notif = '<br><center><span class="label label-success" style="font-size:15pt;">Movimiento aplicado correctamente.</span></center>';
                      }
                      $arr = array('notif'=>$notif, 'id_factun'=>'', 'email'=>'');
                      echo json_encode($arr);

            }
      }
    }

    //Agrega nota de credito o debito electronico
    public function actionAgrega_movimiento(){
        if(Yii::$app->request->isAjax){
          $cliente = $identificacion = $tipo_identificacion = $email = '';
            $tipo = Yii::$app->request->post('tipo');
            $concepto  = Yii::$app->request->post('concepto');
            //$monto  = Yii::$app->request->post('monto');
            $idCabeza_Factura = Yii::$app->request->post('idCa');
            $bandera = 'No cancelada'; $diasCredito = '';
            $model = $this->findModel($idCabeza_Factura);
            $cliente_model = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
            if (@$cliente_model) {
              $cliente = $cliente_model->nombreCompleto;
              $identificacion = $cliente_model->identificacion;
              $tipo_identificacion = $cliente_model->tipoIdentidad == 'Física' ? '01' : '02';
              $email = $cliente_model->email;
              $diasCredito = strval($cliente_model->diasCredito);
            } else {
              $cliente = $model->idCliente;
            }
              //$modelfactura = MovimientoCredito::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one(); //Obtengo la factura
              $saldo_de_la_factura = $model->total_a_pagar; //extraigo el total a pagar de la factura

              $model_m_cobrar = new MovimientoCobrar(); //con estos procesos puedo obtener el saldo pendiente de esta factura
              $suma_monto_movimiento = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->sum('monto_movimiento');
              $saldo_fac = $model->total_a_pagar - $suma_monto_movimiento;
              //al comparar valido que las notas no superen el monto de la factura
              //lo interesante es que necesito hacer una comparacion muy exacta cuando se trata de montos iguales, por eso uso number_format()
              //como se ve a continuacion ya que en algun momento dado despues de varios procesos por abono o NC puede pasar solo por montos iguales
              //por el contrario el monto siempre será mayor al saldo de la factura
              if ( (number_format($saldo_fac,2) == number_format($model->total_a_pagar,2)) || ($saldo_fac > $model->total_a_pagar) ) {
                  //una ves confirmo que las notas no superan el monto de la factura buelvo a recorrer las notas
                  //para aplicarlas cada una a la factura
                  //ENVIAR MOVIMIENTO
                  $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
                  $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
                  $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
                  $condicion_venta = '01'; //01 = Contado, 02 = Crédito, 03 = Consignación, 04 = Apartado, 05 = Arrendamiento con opción de compra, 06 = Arrendamiento en función financiera, 99 = Otros
                  $tipo_pago = '01';
                  $fecha_documento = date("Y-m-d");
                  $numero_original = strval($idCabeza_Factura);
                  $plazo = '';
                  $tipo_documento = $tipo=='nc' ? '03' : '02';
                  if ($model->tipoFacturacion == 1) {//efectivo
                    $condicion_venta = '01'; $tipo_pago = '01';
                  } elseif($model->tipoFacturacion == 2){//deposito
                    $condicion_venta = '01'; $tipo_pago = '04';
                  } elseif ($model->tipoFacturacion == 3) {//tarjeta
                    $condicion_venta = '01'; $tipo_pago = '02';
                  } elseif ($model->tipoFacturacion == 5) {//cheque
                    $condicion_venta = '01'; $tipo_pago = '03';
                  } elseif ($model->tipoFacturacion == 4) {//Credito
                    $condicion_venta = '02';
                    $plazo = $diasCredito;
                  } //crédito
                  $json_movimiento = [
                    "fecha_documento" => strval($fecha_documento),
                    "numero_original" => strval($numero_original),
                    "tipo_documento" => strval($tipo_documento), //02= nota de debito, 03= nota de credito
                    "condicion_venta" => strval($condicion_venta),
                    "tipo_pago" => strval($tipo_pago),
                    "plazo" => $plazo,
                    "sucursal_id" => intval($empresa->sucursal_id),
                    "compania_id" => intval($empresa->compania_id),
                    "moneda" => strval($moneda_local->abreviatura),
                    "referencia_id" => intval($model->factun_id),
                    "tipo_cambio" => floatval($tipocambio->valor_tipo_cambio),
                    "identificacion" => $identificacion,
                    "tipo_identificacion" => $tipo_identificacion,
                    "nombre_cliente" => strval($cliente),
                    "email" => $email,
                    "razon_de_nota" => strval($concepto)
                  ];

                  $consultar_factun = new Factun();
                  $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Crear';
                  $result_movimiento = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir, $json_movimiento, 'POST'));
                  $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$model->idCabeza_Factura])->all();
                  $array_detalle[] = null; $codigo = 'ORD_SERV'; $descripcion = 'ORD_SERV'; $unidad = 'Sp'; $servicio = true;//datos del detalle
                  foreach ($detalle_venta as $key => $value) {
                    $c = $key+1;//contador
                    $moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
                    if ($producto = ProductoServicios::find()->where(['codProdServicio'=>$value->codProdServicio])->one()) {
                      $unid_med = UnidadMedidaProducto::find()->where(['idUnidadMedida'=>$producto->idUnidadMedida])->one();
                      $codigo = $producto->codProdServicio;
                      $descripcion = $producto->nombreProductoServicio;
                      $unidad = $unid_med->simbolo;
                      $servicio = $unidad == 'Sp' ? true : false;
                    }
                    $porcentaje_descuento = (int) round(($value->descuento_producto/$value->cantidad) * 100 / $value->precio_unitario,0);
                    $array_detalle[$key] = [//creamos el array para montar todos los productos para el arreglo de detalle
                                    'numero_linea'=>$c,
                                    'cantidad' => $value->cantidad,
                                    'tipo_codigo' => '01',
                                    'codigo' => $codigo,
                                    'descripcion' => $descripcion,
                                    'unidad' => $unidad,
                                    'moneda' => $moneda->abreviatura,
                                    'tipo_impuesto' => '01',
                                    'porcentaje_impuesto' => $value->subtotal_iva,
                                    'precio_unitario' => $value->precio_unitario,
                                    'porcentaje_descuento' => $porcentaje_descuento,
                                    'servicio' => $servicio,
                                   ];
                  }//fin de foreach
                  $json_detalle = [
                      'documento_id' => $result_movimiento->id,
                      'detalles' => $array_detalle,
                    ];
                  $api_dir2 = 'https://'.Yii::$app->params['url_api'].'/api/v1/Detalle/Crear';
                  $result_detalle = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir2, $json_detalle, 'POST'));
                  //EJECUTAR NOTA
                  $api_dir3 = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Facturar/'.$result_detalle->id;
                  $facturar = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir3, 'PUT'));
                  //compruebo respuesta de factun
                  if ($result_movimiento->id > 0) {
                    //buscamos los datos electronicos para almacenarlos en el movimiento
                    $consultar_factun = new Factun();
                    $api_cosult = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Consultar/'.$result_movimiento->id;
                    $result_conslt = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_cosult, 'PUT'));
                    $numero_mov = $clave_mov =  $estado_hacienda = $estado_detalle = '';
                    foreach ($result_conslt->data as $key => $value) {
                      //con esto obtenemos los datos de la factura
                      if ($key == 'clave') { $clave_mov = $value; }
                      if ($key == 'numero_documento') { $numero_mov = $value; }//con esto obtenemos el numero de mpvimiento de hacienda para atribuircelo al archivo
                      if ($key == 'estado_hacienda') { $estado_hacienda = $value; }
                      if ($key == 'detalle_mensaje') { $estado_detalle = $value; }
                    }
                    $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/XML/'.$result_movimiento->id;
                    $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
                   //Asigno la nota de credito en los campos que corresponden a la tabla de la base de datos
                      $monto_anterior = $saldo_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                      $saldo_pendiente = $tipo=='nc' ? $monto_anterior - $model->total_a_pagar : $monto_anterior + $model->total_a_pagar; //le resto o sumo el monto del movimeinto (NC o ND) con el monto anterior para saber el monto pendiente
                      $model_m_cobrar->factun_id_mov = $result_movimiento->id;
                      $model_m_cobrar->fecmov = date("Y-m-d H:i:s",time());
                      $model_m_cobrar->tipmov = $tipo=='nc' ? 'NC' : 'ND';
                      $model_m_cobrar->concepto = $concepto;
                      $model_m_cobrar->monto_anterior = $monto_anterior;
                      $model_m_cobrar->monto_movimiento = $model->total_a_pagar;
                      $model_m_cobrar->saldo_pendiente = $saldo_pendiente;
                      $model_m_cobrar->usuario = Yii::$app->user->identity->username;
                      $model_m_cobrar->idCabeza_Factura = $idCabeza_Factura;
                      $model_m_cobrar->idCliente = $cliente;
                      $model_m_cobrar->idCli = (int)$cliente;
                      $model_m_cobrar->mov_el = $numero_mov;
                      $model_m_cobrar->clave_mov = $clave_mov;
                      $model_m_cobrar->xml_mov = $result_xml;
                      $model_m_cobrar->estado_hacienda = $estado_hacienda;
                      $model_m_cobrar->estado_detalle = $estado_detalle;

                      if ($model_m_cobrar->save()) {//Guardo la nota de credito
                        //Creo el nuevo recibo por NC siempre y cuando la factura estuviera acfreditada

                          $modelcliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
                          if (@$modelcliente) {//Para Clientes registrados
                          $nuevacantidad = $modelcliente->montoTotalEjecutado - $model->total_a_pagar;
                          $recibo = new Recibos();
                          $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
                          $recibo->monto = $model->total_a_pagar;
                          $recibo->cliente = $model->idCliente;
                          $recibo->usuario = Yii::$app->user->identity->username;
                          $recibo->tipoRecibo = 'NC';
                          $recibo->saldo_cuenta = $nuevacantidad;
                          //Creo el detalle del recibo
                          if ($recibo->save()) {
                              $rec_deta = new RecibosDetalle();//instancia para almacenar la nota de credito
                              $rec_deta->idRecibo = $recibo->idRecibo;
                              $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                              $rec_deta->tipo = 'NC';
                              $rec_deta->referencia = 'Factura: '. $idCabeza_Factura . ' - ' . $concepto;
                              $rec_deta->fecha = date("Y-m-d H:i:s",time());
                              $rec_deta->monto = $model->total_a_pagar;
                              $rec_deta->save();
                              $modelcliente->montoTotalEjecutado = $nuevacantidad;
                              if ($model->estadoFactura == 'Crédito') {
                                $modelcliente->save();
                              }
                            }
                          }//fin comprovacion de clientes
                          else { //Para clientes no registrados
                            $suma_monto_nc = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])->sum('monto_movimiento');
                            $suma_monto_nd = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'ND'])->sum('monto_movimiento');
                            $saldo_fac = $model->total_a_pagar - $suma_monto_nc + $suma_monto_nd;
                            $recibo = new Recibos();
                            $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
                            $recibo->monto = $model->total_a_pagar;
                            $recibo->cliente = $model->idCliente;
                            $recibo->usuario = Yii::$app->user->identity->username;
                            $recibo->tipoRecibo = 'NC';
                            $recibo->saldo_cuenta = $saldo_fac;
                            //Creo el detalle del recibo
                            if ($recibo->save()) {
                                $rec_deta = new RecibosDetalle();//instancia para almacenar la nota de credito
                                $rec_deta->idRecibo = $recibo->idRecibo;
                                $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                                $rec_deta->tipo = 'NC';
                                $rec_deta->referencia = 'Factura: '. $idCabeza_Factura . ' - ' . $concepto;
                                $rec_deta->fecha = date("Y-m-d H:i:s",time());
                                $rec_deta->monto = $model->total_a_pagar;
                                $rec_deta->save();
                              }
                          }
                      }
                    //vuelvo a condicionar pero esta ves si el monto es igual al saldo para así cancelar la factura
                    if (number_format($model->total_a_pagar,2) == number_format($monto_anterior,2)) {
                        $estadoFactura = 'Cancelada';
                        $fecha_final = date("Y-m-d");
                        $sql = "UPDATE tbl_encabezado_factura SET fecha_final = :fecha_final, estadoFactura = :estadoFactura, credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idCabeza_Factura";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":fecha_final", $fecha_final);
                        $command->bindParam(":estadoFactura", $estadoFactura);
                        $command->bindParam(":credi_saldo", $saldo_pendiente);
                        $command->bindValue(":idCabeza_Factura", $idCabeza_Factura);
                        $command->execute();
                        $bandera = 'Cancelada';
                    }
                    $notif = '';
                    if ($bandera == 'Cancelada') {
                          $notif = '<br><center><span class="label label-success" style="font-size:15pt;">Esta factura ha sido revocada y cancelada con este movimiento.</span></center>';
                        } else {
                           $notif = '<br><center><span class="label label-success" style="font-size:15pt;">Movimiento aplicado correctamente.</span></center>';
                        }
                        $arr = array('notif'=>$notif, 'id_factun'=>$result_movimiento->id, 'email'=>$email);
                        echo json_encode($arr);
                  } else {
                    $notif = '<br><center><span class="label label-danger" style="font-size:15pt;">Error; '.$result_movimiento->exito.' No hay comunicación con factun.</span></center>';
                    $arr = array('notif'=>$notif, 'id_factun'=>'', 'email'=>$email);
                    echo json_encode($arr);
                    }
              } else {
                  echo '<br><center><span class="label label-danger" style="font-size:15pt;">Error; Las notas de crédito superan el saldo pendiente de la factura.</span></center>';
              }
        }
    }

}//fin clase

/*
//clase para agrupar numeros en fatura
class item
{
    private $nombre;
    private $precio;
    private $signo;

    public function __construct($nombre = '', $precio = '', $signo = false)
    {
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> signo = $signo;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 35;
        if ($this -> signo) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> nombre, $leftCols) ;

        $sign = ($this -> signo ? '$ ' : '');
        $right = str_pad($sign . $this -> precio, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}*/
