<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
//use backend\models\UsuariosDerechos;
use backend\models\Funcionario;
use backend\models\search\FuncionarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Tribunales;
use backend\models\SignupForm;
//---------------------para la modal de eliminar con permiso de admin
use yii\web\Response;
use common\models\DeletePass; //para confirmar eliminacion de clientes
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;

/*use backend\models\ContactForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;*/

/*use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;

use common\models\AccessHelpers;
use common\models\LoginForm;*/

/**
 * FuncionarioController implements the CRUD actions for Funcionario model.
 */
class FuncionarioController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete','buscar_funcionario'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete','buscar_funcionario'],
            'allow' => true,
            'roles' => ['@'],
          ]
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

    /**
     * Lists all Funcionario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FuncionarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single Funcionario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      $us = new DeletePass();
      if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
      {
          Yii::$app->response->format = Response::FORMAT_JSON;
          return ActiveForm::validate($us);
      }
      if ($us->load(Yii::$app->request->post())) {
          if ($us->validate())
          {
              $us->username = null;
              $us->password = null;
          }
          else
          {
              $us->getErrors();
          }
      }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'us'=>$us,
        ]);
    }

    /**
     * Creates a new Funcionario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Funcionario();
        $model_User = new SignupForm();

                if ($model->load(Yii::$app->request->post()) && $model->save() ) {
                //    if ($model_User->load(Yii::$app->request->post())) {
                      //  if (/*$user = */$model_User->signup()) {
                           // if (Yii::$app->getUser()->login($user)) {}
                                $model_User->signupU();
                                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El funcionario ha sido registrado correctamente.');
                                return $this->redirect(['create', 'model' => $model]);
                                //return $this->redirect(['view', 'id' => $model->idFuncionario]);
                            //}
                      // }
                  //  }
                }
                else {
                    return $this->render('create', [
                        'model' => $model,
                        'model_User' =>$model_User,
                    ]);
                }
      /*  $model = new Funcionario();
        $model_User = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idFuncionario]);
        }
        else {
            return $this->render('create', [
                'model' => $model,
                'model_User' =>$model_User,
            ]);
        }*/
    }

    /**
     * Updates an existing Funcionario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_User = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El funcionario ha sido actualizado correctamente.');
            return $this->redirect(['update', 'id' => $model->idFuncionario]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'model_User' =>$model_User,
            ]);
        }
    }

    /**
     * Deletes an existing Funcionario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
      if(Yii::$app->request->post())
      {

        $id = Html::encode($_POST["idFuncionario"]);

        if($id)
        {
          $this->findModel($id)->delete();
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El funcionario se ha eliminado correctamente.');
          return $this->redirect(['index']);
        }
      }
    }

    /**
     * Finds the Funcionario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Funcionario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Funcionario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //funcion que me permite buscar un cliente y obtener sus datos
    public function actionBuscar_funcionario()
    {
      $tribunales = new Tribunales();
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('id');
        $array_nombre = $tribunales->obtener_nombre_completo_array($id);
        $arr = array( 'nombre'=>$array_nombre[0], 'apellido1'=>$array_nombre[1], 'apellido2'=>$array_nombre[2],
                      'genero'=>$tribunales->obtener_genero($id) );
        echo json_encode($arr);
      }

    }
}
