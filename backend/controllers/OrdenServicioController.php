<?php

namespace backend\controllers;

use Yii;
use backend\models\OrdenServicio;
use backend\models\search\OrdenServicioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Clientes;
use backend\models\F_Proforma;
use backend\models\Vehiculos;
use yii\helpers\Url;
use backend\models\Familia;
use backend\models\Servicios;
use backend\models\DetalleOrdServicio;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use backend\models\EncabezadoPrefactura;
use backend\models\DetalleFacturas;
use backend\models\Proforma;
use backend\models\DetalleProformas;
use kartik\mpdf\Pdf;
use backend\models\Empresa;
use yii\web\Response;
use common\models\DeletePass; //para confirmar eliminacion de cllientes
use backend\models\Marcas;
use backend\models\Modelo;
use backend\models\FacturaOt;
use backend\models\Ot;
use yii\filters\AccessControl;

/**
 * OrdenServicioController implements the CRUD actions for OrdenServicio model.
 */
class OrdenServicioController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete','primererpropietario',
                'cliente','serviciosc', 'serviciosu', 'placa',
                'detallevehiculo', 'detallepropietario', 'serviciosmontoc',
                'serviciosmontou', 'cambiaperiodo','create_proforma',
                'report', 'imprimirorden'
              ],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all OrdenServicio models.
     * @return mixed
     */
    public function actionIndex()
    {
        F_Proforma::setOrdenServicio(null);
        $searchModel = new OrdenServicioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El cliente se ha eliminado correctamente.');
                return $this->redirect(['index']);
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single OrdenServicio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $submit = false)
    {

         $model = $this->findModel($id);
       // $familias = Familia::find()->all();
        $familias = Familia::findAll(['tipo'=>'Servicio']);
        //$servicios = Servicios::findAll(['tipo'=>'Servicio']);
        //$modelDetalleOrdServicio = new DetalleOrdServicio();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $model->famili = \yii\helpers\ArrayHelper::getColumn(
        /*Notemos también que todo esto ocurre dentro del método getColumn de ArrayHelper. Este método,
        devuelve los valores de una columna específica de un arreglo. En este caso, ‘familia’.*/
            $model->getTblDetalleOrdServicios()->asArray()->all(),
            'familia'
        );
/*return $this->render('update', [
                'model' => $model,
                'familias' => $familias,
               // 'servicios' => $servicios,
            ]);*/

        return $this->renderAjax('view', [
            'model' => $model,
            //'familias' => $familias,
        ]);
    }

    public function actionImprimirorden($id, $submit = false)
    {
         $model = $this->findModel($id);
        $familias = Familia::findAll(['tipo'=>'Servicio']);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $model->famili = \yii\helpers\ArrayHelper::getColumn(
        /*Notemos también que todo esto ocurre dentro del método getColumn de ArrayHelper. Este método,
        devuelve los valores de una columna específica de un arreglo. En este caso, ‘familia’.*/
            $model->getTblDetalleOrdServicios()->asArray()->all(),
            'familia'
        );
        return $this->renderAjax('view', [
            'model' => $model,
            'familias' => $familias,
        ]);
    }
    /**
     * Creates a new OrdenServicio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate_proforma(){

        F_Proforma::setOrdenServicio('Si');
        Yii::$app->response->redirect(array('orden-servicio/create'));
    }

    public function actionCreate()
    {
        $model = new OrdenServicio();

        //Este objeto me trae las familias filtradas por tipo 'Servicio'
        $familias = Familia::findAll(['tipo'=>'Servicio'/*,'estado'=>'Activo'*/]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          $cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
          $cliente_ot = Yii::$app->request->post('cliente_ot');
            if ( F_Proforma::GetOrdenServicio()=='Si') {
                $proforma = new Proforma();
                $proforma->fecha_inicio = $model->fecha;
                $proforma->fecha_final = "";
                $proforma->idCliente = $model->idCliente != "" ? $model->idCliente : 0;
                $proforma->idOrdenServicio = $model->idOrdenServicio;
                $proforma->porc_descuento = 0;
                $proforma->iva = $model->montoServicio * Yii::$app->params['porciento_impuesto'] / 100;
                $proforma->total_a_pagar = $model->montoServicio + ($model->montoServicio * Yii::$app->params['porciento_impuesto'] / 100);
                $proforma->estadoFactura = "Pendiente";
                if ($proforma->save()) {
                    $detalle_proforma = new DetalleProformas;
                    $detalle_proforma->idCabeza_factura = $proforma->idCabeza_Factura;
                    $detalle_proforma->cantidad = 1;
                    $detalle_proforma->codProdServicio = 0;
                    $detalle_proforma->descuento_producto = 0;
                    $detalle_proforma->subtotal_iva = $cliente->iv == true ? 0.00 : Yii::$app->params['porciento_impuesto'];
                    $detalle_proforma->precio_unitario = $model->montoServicio;
                    $detalle_proforma->precio_por_cantidad = $model->montoServicio;
                    $detalle_proforma->save();
                    F_Proforma::setOrdenServicio(null);
                }
            } else {
            F_Proforma::setOrdenServicio(null);
            $porc = '5';
            $placa = $model->placa;
            $sql = "UPDATE tbl_vehiculos SET periodo_en_manten = :porc WHERE placa = :placa";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":porc", $porc);
                    $command->bindValue(":placa", $placa);
                    $command->execute();
            $prefactura = new EncabezadoPrefactura();
            $prefactura->fecha_inicio = $model->fecha;
            $prefactura->fecha_final = "";
            $prefactura->idCliente = $model->idCliente != "" ? $model->idCliente : 0;
            $prefactura->idOrdenServicio = $model->idOrdenServicio;
            $prefactura->porc_descuento = 0;
            $prefactura->iva = $model->montoServicio * Yii::$app->params['porciento_impuesto'] / 100;
            $prefactura->total_a_pagar = $model->montoServicio + ($model->montoServicio * Yii::$app->params['porciento_impuesto'] / 100);
            $prefactura->estadoFactura = "Pendiente";
            $prefactura->tipoFacturacion = 1;
            if ($prefactura->save()) {
              if ($cliente_ot > 0) {
                $this->ot_incluido($prefactura->idCabeza_Factura, $cliente_ot, $model->placa, 'pr');
              }
                $detalleprefactura = new DetalleFacturas;
                $detalleprefactura->idCabeza_factura = $prefactura->idCabeza_Factura;
                $detalleprefactura->cantidad = 1;
                $detalleprefactura->codProdServicio = 0;
                $detalleprefactura->descuento_producto = 0;
                $detalleprefactura->subtotal_iva = $cliente->iv == true ? 0.00 : Yii::$app->params['porciento_impuesto'];
                $detalleprefactura->precio_unitario = $model->montoServicio;
                $detalleprefactura->precio_por_cantidad = $model->montoServicio;
                $detalleprefactura->save();
            }
        }
            //return $this->redirect(['view', 'id' => $model->idOrdenServicio]);
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> La orden de servicio ha sido creada correctamente. Puede actualizarla en cualquier momento antes de ser facturado');
            return $this->redirect(['update', 'id' => $model->idOrdenServicio]);

        }else {
            return $this->render('create', [
                'model' => $model,
                'familias' => $familias,
                //'servicios' => $servicios,
            ]);
        }
    }

    //asigna OT a la prefactura
    public function ot_incluido($factura, $cliente_ot, $placa, $mod)
    {
      $vehiculo = Vehiculos::findOne($placa);
      $factura_ot = new FacturaOt();
      $factura_ot->idCabeza_Factura = $factura;
      $factura_ot->idCliente = $cliente_ot;
      $factura_ot->placa = $placa;
      $factura_ot->codMarca = $vehiculo->marca;
      $factura_ot->codModelo = $vehiculo->modelo;
      $factura_ot->codVersion = $vehiculo->version;
      $factura_ot->ano = $vehiculo->anio;
      $factura_ot->motor = $vehiculo->motor;
      $factura_ot->cilindrada = $vehiculo->cilindrada_motor;
      $factura_ot->combustible = $vehiculo->combustible;
      $factura_ot->transmision = '';
      $factura_ot->save();
      if ($mod=='pr') {
        $fact_obj = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$factura])->one();
        if (@$cliente_ot = Ot::find()->where(['idCliente'=>$cliente_ot])->one()) {
          $monto_ejecutado = $cliente_ot->montoTotalEjecutado_ot;
          $cliente_ot->montoTotalEjecutado_ot = $cliente_ot->montoTotalEjecutado_ot + $fact_obj->total_a_pagar;
          $cliente_ot->save();
        }
      }
    }

    //Función para cambiar el periodo de mantenimiento del vehiculo en el taller.
    public function actionCambiaperiodo(){

        if(Yii::$app->request->isAjax){
            $periodo_en_manten = Yii::$app->request->post('periodo_en_manten');
            $placa = Yii::$app->request->post('placa');
            $sql = "UPDATE tbl_vehiculos SET periodo_en_manten = :porc WHERE placa = :placa";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":porc", $periodo_en_manten);
                    $command->bindValue(":placa", $placa);
                    $command->execute();
            //echo @$periodo_en_manten;
        }
    }

    //Esta función es una accion que me trae los servicios correspondientes a la familia
    //que asigne para la accion Create
    public function actionServiciosc(){
        $model = new OrdenServicio();
        if(Yii::$app->request->isAjax){
            $ids = Yii::$app->request->post('idF');//obtengo por post los datos del valor "idF" de la familia
            $sql = "SELECT descripcion FROM tbl_familia WHERE codFamilia = :ids";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":ids", $ids);
            $result = $command->queryOne();
                        $espacio = '&nbsp&nbsp&nbsp&nbsp';
                        $servicios = Servicios::findAll([ 'codFamilia' => $ids]);
                        //$servicios = Servicios::find()->All();
                        $T_se = \yii\helpers\ArrayHelper::map($servicios, 'codProdServicio', /*'nombreProductoServicio',*/
                                function($element) {
                                    $impuesto = Yii::$app->params['porciento_impuesto'] / 100;
                                    $precio = @$element['precioMinServicio'];
                                    $precioimpuesto = ($precio*$impuesto)+$precio;
                                    return $element['nombreProductoServicio'].' <br> ( ₡'.$precioimpuesto .' ivi )';
                                });
                        echo '<div id = "'.$ids.'" style="border: 2px solid #CCC2C7;"><br><center><h4>' .$result['descripcion'] . '</h4></center>' .
                                                                                Html::activeCheckboxList($model, 'servi', $T_se, ['unselect'=>NULL,
                                                                               'separator' => $espacio,
                                                                               //'options'=>['id'=>'dicSer', 'onchange'=>'javascript:sumontoservicio()'],
                                                                               'item' => function($index, $label, $name, $checked, $value) {
                                                                                    $cla = Yii::$app->request->post('idF');
                                                                                    $label='<br>'.$label;
                                                                                    //$return = '<label class="modal-radio" id="dicSer">';
                                                                                    //$return .= '<input type="checkbox" name=" ' . $name . '" value="' . $value . '"  id="person1"></input>';
                                                                                   // $return .= '<span>' . ucwords($label) . '  </span>';
                                                                                   // $return .= '</label>';
                                                                                   // return $return;
                                                                                    //https://github.com/yiisoft/yii2/issues/6527
                                                                                    //Abrimos el contenido

                                                                                    return  '<label class="modal-radio" id="dicSer" >' . Html::checkbox($name, $checked, ['class'=>'checked'.$cla.'_','id'=>'vaSer'.$cla.'_', 'onClick'=>'if (this.checked) sumarMontSer('.$value.'); else restarMontSer('.$value.')' , 'label' => $label, 'value' => $value/*, 'data-bind' => 'checked:fieldName'*/]) . '</label>';
                                                                                 }
                                                                               ]) . '<br></div><br>';
        }

    }

    //Función cuya acción me debuelve el precio del servicio que se selecciona o deselecciona.
    public function actionServiciosmontou(){

        if(Yii::$app->request->isAjax){
            $idV = Yii::$app->request->post('idV');
            $total = floatval(str_replace(",", "", Yii::$app->request->post('total')));
            $operacion = Yii::$app->request->post('operacion');
            $sql = "SELECT precioMinServicio FROM tbl_producto_servicios WHERE codProdServicio = :idV";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":idV", $idV);
            $result = $command->queryOne();
            if ($operacion=='s') {
              $total += @$result['precioMinServicio'];
            } else {
              $total -= @$result['precioMinServicio'];
            }
            $total_mas_iv = $total + ($total*Yii::$app->params['porciento_impuesto'] / 100);
            $arr = array('total'=>number_format($total,2), 'total_mas_iv'=>number_format($total_mas_iv,2));
            echo json_encode($arr);
        }
    }

    //Función cuya acción me debuelve el precio más IV del servicio que se selecciona o deselecciona.
    public function actionServiciosmontoc(){

        if(Yii::$app->request->isAjax){
            $idV = Yii::$app->request->post('idV');
            $sql = "SELECT precioMinServicio FROM tbl_producto_servicios WHERE codProdServicio = :idV";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":idV", $idV);
            $result = $command->queryOne();


            echo @$result['precioMinServicio'] + (@$result['precioMinServicio'] * Yii::$app->params['porciento_impuesto'] / 100);
        }
    }

  /*  public function actionCreate($submit = false)
    {
        //$ids = Yii::$app->request->post('ids');

        $model = new OrdenServicio();

        $familias = Familia::findAll(['tipo'=>'Servicio','estado'=>'Activo']);

        $servicios = Servicios::findAll([ 'codFamilia' => $ids ]); //$model->estado === 0 ? 'Inactivo' : 'Activo',



        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->idOrdenServicio]);
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
          }
        }
        return $this->render('create', [
                'model' => $model,
                'familias' => $familias,
                'servicios' => $servicios,
            ]);
    }*/

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       // $familias = Familia::find()->all();
        $familias = Familia::findAll(['tipo'=>'Servicio']);
        //$servicios = Servicios::findAll(['tipo'=>'Servicio']);
        //$modelDetalleOrdServicio = new DetalleOrdServicio();

        $model->famili = \yii\helpers\ArrayHelper::getColumn(
        /*Notemos también que todo esto ocurre dentro del método getColumn de ArrayHelper. Este método,
        devuelve los valores de una columna específica de un arreglo. En este caso, ‘familia’.*/
            $model->getTblDetalleOrdServicios()->asArray()->all(),
            'familia'
        );

         $model->servi = \yii\helpers\ArrayHelper::map(DetalleOrdServicio::findAll(['idOrdenServicio' => $id]),'producto','producto');

        if ($model->load(Yii::$app->request->post())) {
          $model->montoServicio = floatval(str_replace(",", "", $model->montoServicio));
            /*Lo que estamos haciendo aquí, luego de cargar el modelo con los parámetros
            recibidos via post, es comprobar si se encuentra presente: $_POST['OrdenServicio']['famili'] y
            $_POST['OrdenServicio']['servi'] */
            if (!isset($_POST['OrdenServicio']['famili']) && !isset($_POST['OrdenServicio']['servi'])) {
                /*El motivo de ello, es que esta variable no estará presente cuando no se haya marcado ninguna operación.
                Si no se ha seleccionado ninguna operación, hacemos: $model->famili = []; y $model->servi = [];*/
                $model->famili = [];
                $model->servi = [];
            }
            if ($model->save()) {
                //Lo siguiente me permite agregar el monto de los servicios al detalle de la factura.

                //actualizamos el detalle de proforma o de prefactura, dependiendo si el estado es 0 o no
                $CabeFact = $model->estado == 0 ? Proforma::find()->where(['idOrdenServicio'=>$model->idOrdenServicio])->one() : EncabezadoPrefactura::find()->where(['idOrdenServicio'=>$model->idOrdenServicio])->one();
                //Ahora ejecutamos la actualizacion del cliente de la factura por si esta el caso de que la orden presenta un nuevo cliente
                $idCabeza_factura = $CabeFact->idCabeza_Factura;
                $sql = $model->estado == 0 ? "UPDATE tbl_detalle_proforma SET precio_unitario = :precio_unitario WHERE idCabeza_factura = :idCabeza_factura AND codProdServicio = :idOrdenServicio" : "UPDATE tbl_detalle_facturas SET precio_unitario = :precio_unitario WHERE idCabeza_factura = :idCabeza_factura AND codProdServicio = :idOrdenServicio";
                    $monto = $model->montoServicio;//Tengo que poner el monto de servicio en una variable porque dentro de bindParam no no acepta.
                    //con esto edita el detalle ya sea de la proforma o de la prefactura
                    $idOrdenServicio = '0';
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":precio_unitario", $monto);
                    $command->bindValue(":idCabeza_factura", $idCabeza_factura);
                    $command->bindValue(":idOrdenServicio", $idOrdenServicio);
                    $command->execute();
                //con esto edita el cliente del encabezado de la prefactura o proforma
                $idCliente = $model->idCliente;
                $idOrdenServicio = $model->idOrdenServicio;
                $sql2 = $model->estado == 0 ? "UPDATE tbl_proforma SET idCliente = :idCliente WHERE idOrdenServicio = :idOrdenServicio" : "UPDATE tbl_encabezado_factura SET idCliente = :idCliente WHERE idOrdenServicio = :idOrdenServicio" ;
                $command = \Yii::$app->db->createCommand($sql2);
                $command->bindValue(":idCliente", $idCliente);
                $command->bindValue(":idOrdenServicio", $idOrdenServicio);
                $command->execute();
                //regresa al origen de la orden
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> La orden de servicio ha sido actualizada correctamente. Puede actualizarla en cualquier momento antes de ser facturado');
                return $this->redirect(['update', 'id' => $model->idOrdenServicio]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'familias' => $familias,
               // 'servicios' => $servicios,
            ]);
        }
    }

    //Esta función es una accion que me trae los servicios correspondientes a la familia
    //que asigne para la accion Update
    public function actionServiciosu(){ //http://yii2enespanol.com/2015/05/02/select-dependientes-en-yii-2/
        $model = new OrdenServicio();
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $ids = Yii::$app->request->post('idF');
            $sql = "SELECT descripcion FROM tbl_familia WHERE codFamilia = :ids";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":ids", $ids);
            $result = $command->queryOne();
                $espacio = '&nbsp&nbsp&nbsp&nbsp';
                //$ids = Yii::$app->request->isAjax ? Yii::$app->request->post('idF') : null;index.php?r=orden-servicio%2F
                $servicios = Servicios::findAll([ 'codFamilia' => $ids]);
                $T_se = \yii\helpers\ArrayHelper::map($servicios, 'codProdServicio', /*'nombreProductoServicio',*/
                    function($element) {
                                    $impuesto = Yii::$app->params['porciento_impuesto'] / 100;
                                    $precio = @$element['precioMinServicio'];
                                    $precioimpuesto = ($precio*$impuesto)+$precio;
                                    return $element['nombreProductoServicio'].' <br> ( ₡'.$precioimpuesto .' ivi )';
                                });
                //http://www.yiiframework.com/doc-2.0/guide-helper-html.html
                $model->servi = \yii\helpers\ArrayHelper::map(DetalleOrdServicio::findAll(['idOrdenServicio' => $id]),'producto','producto');
                echo '<div id = "'.$ids.'" style="border: 2px solid #CCC2C7;"><br><center><h4>' .$result['descripcion'] . '</h4></center>' .
                                                                                Html::activeCheckboxList($model, 'servi', $T_se, ['unselect'=>NULL,
                                                                               'separator' => $espacio,
                                                                               'item' => function($index, $label, $name, $checked, $value) {
                                                                                    $cla = Yii::$app->request->post('idF');
                                                                                    $label='<br>'.$label;
                                                                                    return  '<label class="modal-radio" id="dicSer" >' . Html::checkbox($name, $checked, ['class'=>'checked'.$cla.'_', 'id'=>'vaSer', 'onClick'=>'if (this.checked) sumarMontSer('.$value.'); else restarMontSer('.$value.')' , 'label' => $label, 'value' => $value]) . '</label>';
                                                                                    //return  '<label class="modal-radio" id="dicSer">' . Html::checkbox($name, $checked, ['label' => $label, 'value' => $value, 'data-bind' => 'checked:fieldName']) . '</label>';
                                                                                 }
                                                                               ]) . '<br></div><br>';
        }
    }

    //Función cuya acción me devuelve el detalle del vehiculo
    public function actionDetallevehiculo(){
        if(Yii::$app->request->isAjax){
            $v_placa = Yii::$app->request->post('v_placa');
            //$marca = Vehiculos::findAll(array('placa'=>$v_placa));
            //$marca_ = \yii\helpers\ArrayHelper::map(Vehiculos::findAll(['placa' => $v_placa]),'marca','marca');
            $sql = "SELECT marca , modelo , anio, idCliente FROM tbl_vehiculos WHERE placa = :placa";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":placa", $v_placa);
            $result = $command->queryOne();
            $marca = Marcas::findOne($result['marca']);
            $modelo = Modelo::findOne($result['modelo']);
            $resultado = '<label>Marca: </label>'.' '.$marca->nombre_marca.' <br>
                          <label>Modelo: </label>'.' '.$modelo->nombre_modelo.' <br>
                          <label>Año: </label>'.' '.$result['anio'];

            $arr = array( 'idCliente'=>$result['idCliente'],
                          'resultados'=>$resultado );
            echo json_encode($arr);
        }
    }

    //Función cuya acción me devuelve el detalle del propietario
    public function actionDetallepropietario(){
        if(Yii::$app->request->isAjax){
            $v_cliente = Yii::$app->request->post('v_cliente');
            $sql = "SELECT celular , email, nombreCompleto FROM tbl_clientes WHERE idCliente = :idCliente";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindValue(":idCliente", $v_cliente);
            $result = $command->queryOne();
            $resultado = '<h1><small>'.$result['nombreCompleto'].'</small></h1>
                          <label>Teléfono celular: </label>'.' '.$result['celular'].' <br>
                          <label>Email: </label>'.' '.$result['email'];
            echo $resultado;
        }
    }

    /**
     * Updates an existing OrdenServicio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
  /*  public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $familias = Familia::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idOrdenServicio]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'familias' => $familias,
            ]);
        }
    }*/
    /**
     * Deletes an existing OrdenServicio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
      if(Yii::$app->request->post())
      {
        $id = Html::encode($_POST["idOrdenServicio"]);
        $prefactura = EncabezadoPrefactura::find()->where(['idOrdenServicio'=>$id])->one();
        $prefactura->idOrdenServicio = null;
        if ($prefactura->save()) {
          \Yii::$app->db->createCommand()->delete('tbl_detalle_facturas', 'idCabeza_factura = '.$prefactura->idCabeza_Factura.' AND codProdServicio = "0"' )->execute();
          $this->findModel($id)->delete();
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> La orden de servicio se ha eliminado correctamente.');
        } else {
          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> La orden de servicio no se eliminó.');
        }

        return $this->redirect(['index']);
      }
    }

    /**
     * Finds the OrdenServicio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenServicio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenServicio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCliente() {
     $out = [];
     if (isset($_POST['depdrop_parents'])) {
         $parents = $_POST['depdrop_parents'];
         if ($parents != null) {
             $idCliente = $parents[0];
             $out = $this->getCliente($idCliente);
             echo Json::encode(['output'=>$out, 'selected'=>'']);
             return;
         }
     }
     echo Json::encode(['output'=>'', 'selected'=>'']);
 }

    protected function getCliente($idCliente){
     $datas = Vehiculos::find()->where(['idCliente'=>$idCliente])->all();
     return $this->MapData($datas,'placa','placa');

 }

    protected function MapData($datas,$fieldId,$fieldName){
     $obj = [];
     foreach ($datas as $key => $value) {
         array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}]);
     }
     return $obj;
 }

  public function actionPlaca() {
     $out = [];
     if (isset($_POST['depdrop_parents'])) {
         $parents = $_POST['depdrop_parents'];
         if ($parents != null) {
             $placa = $parents[0];
             $out = $this->getPlaca($placa);
             echo Json::encode(['output'=>$out, 'selected'=>'']);
             return;
         }
     }
     echo Json::encode(['output'=>'', 'selected'=>'']);
 }


 protected function getPlaca($placa){
     $datas = Vehiculos::findAll(array('placa'=>$placa));
     $idCli = \yii\helpers\ArrayHelper::map($datas,'idCliente','idCliente');
     $datas2 = Clientes::find()->where(['idCliente'=>$idCli])->all();
     return $this->MapData($datas2,'idCliente','nombreCompleto');
 }

 public function actionPrimererpropietario(){
     if(Yii::$app->request->isAjax){
        $v_placa = Yii::$app->request->post('v_placa');
        $vehiculo = Vehiculos::find()->where(['placa'=>$v_placa])->one();
        return $vehiculo->idCliente;
     }
 }

public function actionReport($id){
    $empresaimagen = new Empresa();
    $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
    $familias = Familia::findAll(['tipo'=>'Servicio']);

        //$id = Compra_view::getCabezaFactura();
        $content = '
        <div class="panel panel-default">
            <div class="panel-body">
              <span style="float:left;" size="4" >
                <strong>'.$empresa->nombre.'</strong><br>
                <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'
              </span>
              <span style="float:right;" size="5" ><strong>Orden de Servicio N°: '.$id. '<br>Fecha:</strong> '.date('d-m-Y').'</span>
            </div>
        </div>
        ';

        $content .= $this->renderAjax('imprimirorden',['model' => $this->findModel($id)]);
       /* $content = $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);*/

       $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Orden de Servicio</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';

        echo $imprimir;
}

 //   public function actionMarca() {
 //     $out = [];
 //     if (isset($_POST['depdrop_parents'])) {
 //         $parents = $_POST['depdrop_parents'];
 //         if ($parents != null) {
 //             $placa = $parents[0];
 //             $out = $this->getMarca($placa);
 //             echo Json::encode(['output'=>$out, 'selected'=>'']);
 //             return;
 //         }
 //     }
 //     echo Json::encode(['output'=>'', 'selected'=>'']);
 // }

 //    protected function getMarca($placa){
 //     $datas = Vehiculos::find()->where(['placa'=>$placa])->all();
 //     return $this->MapData($datas,'marca','marca');

 // }

 //    public function actionModelo() {
 //     $out = [];
 //     if (isset($_POST['depdrop_parents'])) {
 //         $parents = $_POST['depdrop_parents'];
 //         if ($parents != null) {
 //             $placa = $parents[0];
 //             $out = $this->getModelo($placa);
 //             echo Json::encode(['output'=>$out, 'selected'=>'']);
 //             return;
 //         }
 //     }
 //     echo Json::encode(['output'=>'', 'selected'=>'']);
 // }

 //    protected function getModelo($placa){
 //     $datas = Vehiculos::find()->where(['placa'=>$placa])->all();
 //     return $this->MapData($datas,'modelo','modelo');

 // }

 //    public function actionAnio() {
 //     $out = [];
 //     if (isset($_POST['depdrop_parents'])) {
 //         $parents = $_POST['depdrop_parents'];
 //         if ($parents != null) {
 //             $placa = $parents[0];
 //             $out = $this->getAnio($placa);
 //             echo Json::encode(['output'=>$out, 'selected'=>'']);
 //             return;
 //         }
 //     }
 //     echo Json::encode(['output'=>'', 'selected'=>'']);
 // }

 //    protected function getAnio($placa){
 //     $datas = Vehiculos::find()->where(['placa'=>$placa])->all();
 //     return $this->MapData($datas,'anio','anio');

 // }


 //    public function actionTelefono() {
 //     $out = [];
 //     if (isset($_POST['depdrop_parents'])) {
 //         $parents = $_POST['depdrop_parents'];
 //         if ($parents != null) {
 //             $idCliente = $parents[0];
 //             $out = $this->getTelefono($idCliente);
 //             echo Json::encode(['output'=>$out, 'selected'=>'']);
 //             return;
 //         }
 //     }
 //     echo Json::encode(['output'=>'', 'selected'=>'']);
 // }

 //    protected function getTelefono($idCliente){
 //     $datas = Clientes::find()->where(['idCliente'=>$idCliente])->all();
 //     return $this->MapData($datas,'telefono','telefono');

 // }


 //     public function actionEmail() {
 //     $out = [];
 //     if (isset($_POST['depdrop_parents'])) {
 //         $parents = $_POST['depdrop_parents'];
 //         if ($parents != null) {
 //             $idCliente = $parents[0];
 //             $out = $this->getEmail($idCliente);
 //             echo Json::encode(['output'=>$out, 'selected'=>'']);
 //             return;
 //         }
 //     }
 //     echo Json::encode(['output'=>'', 'selected'=>'']);
 // }

 //    protected function getEmail($idCliente){
 //     $datas = Clientes::find()->where(['idCliente'=>$idCliente])->all();
 //     return $this->MapData($datas,'email','email');

 // }


}
