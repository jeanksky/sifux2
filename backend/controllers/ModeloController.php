<?php

namespace backend\controllers;

use Yii;
use backend\models\Clientes;
use backend\models\Modelo;
use backend\models\Marcas;
use backend\models\Vehiculos;
use kartik\widgets\DepDrop;
use yii\helpers\Json;
use yii\helpers\Url;


use backend\models\search\ModeloSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ModeloController implements the CRUD actions for Modelo model.
 */
class ModeloController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'only' => ['index','view','create','update','delete','lists','get-modelo','subcat'],
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete','lists','get-modelo','subcat'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all Modelo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModeloSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Modelo model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Modelo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Modelo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El modelo ha sido registrado correctamente.');
            return $this->redirect(['create', 'model' => $model]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Modelo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El modelo ha sido actualizado correctamente.');
            return $this->redirect(['update','id' => $model->codModelo]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Modelo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $fvehiculo = Vehiculos::find()->where(['modelo'=>$id])->all();
        if(!$fvehiculo){
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El Modelo se ha eliminado correctamente.');
            return $this->redirect(['index']);
        }
        else {
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> Imposible eliminar este modelo. Tiene registros asociados.');
            return $this->redirect(['index']);
        }
    }


//Dependent Drop Down Lists

      public function actionLists($id) {

     $countPosts = Modelo::find()
          ->where(['codMarca' => $id])
          ->count();

     $posts = Modelo::find()
          ->where(['codMarca' => $id])
          ->orderBy('id DESC')
          ->all();

     if($countPosts > 0) {
          foreach($posts as $post){
               echo "<option value='".$post->codModelo."'>".$post->codModelo."</option>";
          }
     }
     else{
          echo "<option>-</option>";
     }
}


//

        public function actionGetModelo() {
             $out = [];
             if (isset($_POST['depdrop_parents'])) {
                 $parents = $_POST['depdrop_parents'];
                 if ($parents != null) {
                     $codMarca = $parents[0];
                     $out = $this->getModelo($codMarca);
                     echo Json::encode(['output'=>$out, 'selected'=>'']);
                     return;
                 }
             }
             echo Json::encode(['output'=>'', 'selected'=>'']);
         }

    // THE CONTROLLER
    public function actionSubcat() {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $codMarca = $parents[0];
            $out = Modelo::getSubCatList($codMarca);
            // the getSubCatList function will query the database based on the
            // cat_id and return an array like below:
            // [
            //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
            //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
            // ]
            echo Json::encode(['output'=>$out, 'selected'=>'']);
            return;
        }
    }
    echo Json::encode(['output'=>'', 'selected'=>'']);
}

    /**
     * Finds the Modelo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Modelo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Modelo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
