<?php

namespace backend\controllers;

use Yii;
use backend\models\CategoriaGastos;
use backend\models\search\CategoriaGastosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\TiposGastos;
use yii\filters\AccessControl;

/**
 * CategoriaGastosController implements the CRUD actions for CategoriaGastos model.
 */
class CategoriaGastosController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'only' => ['index','view','create','update','delete'],
              'rules' => [
                [
                  'actions' => ['index','view','create','update','delete'],
                  'allow' => true,
                  'roles' => ['@'],
                ]
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoriaGastos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriaGastosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CategoriaGastos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategoriaGastos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CategoriaGastos();
        $model->estadoCategoriaGastos='activo';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Categoria Gastos
ha sido registrado correctamente.');
            //return $this->redirect(['view', 'id' => $model->idCategoriaGastos]);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CategoriaGastos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Categoria Gastos ha sido actualizado correctamente.');
            //return $this->redirect(['view', 'id' => $model->idCategoriaGastos]);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CategoriaGastos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //variable que busca que si en Tipos de Gastos esta asociado la categoria para que no la eliminen.
        $CompTipoGasto = TiposGastos::find()->where(['idCategoriaGasto'=>$id])->one();
        if(@$CompTipoGasto){
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> Imposible eliminar Categoria Gastos. Tiene registros asociados.');
            return $this->redirect(['index']);
        }//fin del if
        else{
             $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> Categoria Gastos se ha eliminado correctamente.');
            return $this->redirect(['index']);
        }//fin del else
    }

    /**
     * Finds the CategoriaGastos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CategoriaGastos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CategoriaGastos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
