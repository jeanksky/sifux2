<?php

namespace backend\controllers;

use Yii;
use backend\models\Proforma;
use backend\models\DetalleProformas;
use backend\models\DetalleOrdServicio;
use backend\models\search\ProformaSearch;
use backend\models\Funcionario;
use backend\models\ProductoCatalogo;
use backend\models\ProductoProveedores;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\F_Proforma;
use backend\models\ProductoServicios;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Empresa;
use kartik\mpdf\Pdf;
use backend\models\Clientes;
use backend\models\EncabezadoPrefactura;
use backend\models\DetalleFacturas;
use backend\models\OrdenServicio;
use backend\models\Tribunales;
use yii\filters\AccessControl;
//use mPDF;

/**
 * ProformaController implements the CRUD actions for Proforma model.
 */
class ProformaController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete',
                'vendedor2', 'deletevendedor', 'cliente2',
                'clientenew', 'deletecliente', 'preciounitario', 'precioganancia',
                'precioiva', 'additem_temporal', 'moditem_temporal', 'additem_temporal_update',
                'moditem_temporal_update', 'additem', 'additemu', 'complete',
                'prefactura', 'addobservacion', 'addobservacion-u', 'deleteitem',
                'deleteitemu', 'deletevendedor-u', 'updatedescuento', 'updatedescuentou',
                'vendedor2-u', 'updatecantidad', 'updatecantidadu', 'getpreciototal',
                'getpreciototalu', 'regresarborrar', 'deleteall', 'deletecliente-u',
                'cliente2-u', 'clientenew-u', 'regresarborrar-u', 'report',
                'enviar_correo_proforma', 'busqueda_producto'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all Proforma models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProformaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proforma model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proforma model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $vendedornull = F_Proforma::getVendedornull();

      if ($vendedornull!='1') {

         $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
         if ($funcionario) {
             F_Proforma::setVendedor($funcionario->idFuncionario);

         } else {

         }

          F_Proforma::setVendedornull(null);
      }
        return $this->render('create');
    }

    /**
     * Updates an existing Proforma model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //Definimos c como null para comprobación
                $c=null;
                //Baseamos el contenido que puede quedar de otro detalle
                F_Proforma::setContenidoComprau(null);
                F_Proforma::setCabezaFactura(null);
                //Abrimos el contenido
                $compra = F_Proforma::getContenidoComprau(); /////
                F_Proforma::setCabezaFactura($id);

                //Me obtiene los datos del detalle del encabezado correspondiente
                $query = new yii\db\Query();
                $data = $query->select(['codProdServicio','cantidad','checkiva','precio_unitario','precio_por_cantidad',
                'descuento_producto', 'subtotal_iva', 'temporal', 'descrip_temporal', 'precio_unit_prov_temporal',
                'porc_ganancias_temporal','precio_unit_gan_temporal','precio_total_ganacia_mod','precio_mas_iva_total_temporal_mod'])
                    ->from('tbl_detalle_proforma')
                    ->where(['=','idCabeza_factura', $model->idCabeza_Factura])
                    ->distinct()
                    ->all();
                $nivel = 'Presiona para mostrar la orden de servicio, verifique que esté al 100% antes de facturar o acreditar';
                //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
                foreach($data as $row){
                    $codProdServicio = $row['codProdServicio'];
                    $cantidad = $row['cantidad'];
                    $precio_unitario = $row['precio_unitario'];
                    $descuento = $row['descuento_producto'];
                    $iva = $row['subtotal_iva'];
                   if ($codProdServicio>='') {
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        if($c==null)
                        {   //cargo las variables de la tabla
                            //$orden = Html::a('Orden de Servicio', '../web/index.php?r=orden-servicio%2Fview&id='.$model->idOrdenServicio);
                            //Modal con datos: https://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
                            $orden = Html::a('Orden de Servicio', ['#'], [
                                'id' => 'activity-index-link-update',
                                'data-toggle' => 'modal',//'data-placement'=>'left',
                                'data-trigger'=>'hover', 'data-content'=>$nivel,
                                'data-target' => '#modalOrden',
                                'data-url' => Url::to(['orden-servicio/view', 'id' => $model->idOrdenServicio]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Orden de Servicio correspondiente'),
                                ]);
                            $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();

                            $descripcion = '';

                            if ($modelProd) {
                                $descripcion = $codProdServicio != "0" ? $modelProd->nombreProductoServicio : $orden;
                            } else {
                                $descripcion = $codProdServicio == "0" ? $orden : 'Este producto ya no existe en invetario';
                            }


                            $_POST['cantidad']= $codProdServicio != "0" ? $cantidad : 1;//por una extraña razón la cantidad de orden de servicio se incrementa al agregar un producto, por eso le agrego forzadamente cantidad 1 todas las veces.
                            $_POST['cod']= $codProdServicio;
                            $_POST['desc']= $row['temporal']=='x' ? $row['descrip_temporal'] : $descripcion;
                            $_POST['precio']= $precio_unitario;
                            //$_POST['dcto']= ?;//recordar que esto se cambia
                            $_POST['dcto'] = $descuento;
                            //Iva en forma manual, se puede cargar de la DB si se desea
                            $_POST['iva'] = $iva;
                            if ($row['temporal']=='x') {
                              $_POST['checkiva'] = $row['checkiva'];
                              $_POST['temporal'] = $row['temporal'];
                              $_POST['precio_unit_prov_temporal'] = $row['precio_unit_prov_temporal'];
                              $_POST['porc_ganancias_temporal'] = $row['porc_ganancias_temporal'];
                              $_POST['precio_unit_gan_temporal'] = $row['precio_unit_gan_temporal'];
                              $_POST['precio_total_ganacia_mod'] = $row['precio_total_ganacia_mod'];
                              $_POST['precio_mas_iva_total_temporal_mod'] = $row['precio_mas_iva_total_temporal_mod'];
                            }

                            $compra[] = $_POST;
                            //echo $model->nombreProductoServicio;
                        }
                        //Guardamos el contenido
                        F_Proforma::setContenidoComprau($compra);
                    }
                }

            return $this->render('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing Proforma model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Proforma model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proforma the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proforma::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //Esta funcion me amacena los datos del vendedor en session F_Proforma
    public function actionVendedor2(){
        if(Yii::$app->request->isAjax){
            $idFuncionario = Yii::$app->request->post('idFuncionario');
            F_Proforma::setVendedor((int)$idFuncionario);
            Yii::$app->response->redirect(array('proforma/create'));
        }
    }

    //Esta funcion me elimina los datos del vendedor almacenados en session F_Proforma
    public function actionDeletevendedor()
    {
        F_Proforma::setVendedor(null);//Para borrar vendedor
        F_Proforma::setVendedornull('1');
        Yii::$app->response->redirect(array('proforma/create'));
    }

    //esta funcion me agrega clientes que ya existen para session F_Proforma
    public function actionCliente2(){
        if(Yii::$app->request->isAjax){
            $idCliente = Yii::$app->request->post('idCliente');
            $producto_venta = F_Proforma::getContenidoCompra();//obtenemos todos los productos apartados
            if (@$producto_venta){
              foreach ($producto_venta as $position => $product){//recorremos cada producto
                //buscamos el producto en turno en inventario
                $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$producto_venta[$position]['cod']])->one();
                //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
                if (@$producto_serv) {
                  $producto_venta[$position]['iva'] = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                  F_Proforma::setContenidoCompra($producto_venta);//guardamos en sessión
                }
              }
            }

            //si el cliente seleccionado esta en base de datos obtenemos un objeto
            if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$idCliente])->one())
            { //comprovamos si es exento de impuesto
              if ($cliente_ob->iv == true) {//si es un cliente exento vuelve a recorrer los productos
                if (@$producto_venta){
                  foreach ($producto_venta as $position => $product){
                    $producto_venta[$position]['iva'] = '0.00';//para asignarle un valor 0.00 exento a todos los productos
                    F_Proforma::setContenidoCompra($producto_venta);//volvemos a guardar
                  }
                }
              }
            }
            F_Proforma::setCliente((int)$idCliente);
            Yii::$app->response->redirect(array('proforma/create'));
        }
    }



    //Esta funcion me agrega clientes nuevos para session F_Proforma
    public function actionClientenew(){
      $tribunales = new Tribunales();
        if (Yii::$app->request->post('new-cliente')) {
          $producto_venta = F_Proforma::getContenidoCompra();//obtenemos todos los productos apartados
          if(@$producto_venta){
            foreach ($producto_venta as $position => $product){//recorremos cada producto
              //buscamos el producto en turno en inventario
              $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$producto_venta[$position]['cod']])->one();
              //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
              if (@$producto_serv) {
                $producto_venta[$position]['iva'] = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                F_Proforma::setContenidoCompra($producto_venta);//guardamos en sessión
              }
            }
          }

          $idCliente = Yii::$app->request->post('new-cliente');
          $modelclient = Clientes::find()->where(['identificacion'=>$idCliente])->one();
          if (@$modelclient) {
            if ($modelclient->iv == true) {
            //si es un cliente exento vuelve a recorrer los productos
              if(@$producto_venta)
              foreach ($producto_venta as $position => $product){
                $producto_venta[$position]['iva'] = '0.00';//para asignarle un valor 0.00 exento a todos los productos
                F_Proforma::setContenidoCompra($producto_venta);//volvemos a guardar
              }
            }
            F_Proforma::setCliente($modelclient->idCliente);
          } else {
            $nombre_tribunal = $tribunales->obtener_nombre_completo($idCliente);
            if ($nombre_tribunal != 'Sin resultado') {
              F_Proforma::setCliente($nombre_tribunal);
            } else {
              $nombre_tribunal_juridico = $tribunales->obtener_nombre_juridico($idCliente);
              if ($nombre_tribunal_juridico != 'Sin resultado') {
                F_Proforma::setCliente($nombre_tribunal_juridico);
              }else {
                F_Proforma::setCliente($idCliente);
              }
            }
          }
          Yii::$app->response->redirect(array('proforma/create'));
        } else {
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ningun cliente, el espacio estaba vacío.');
            Yii::$app->response->redirect(array('proforma/create'));
        }

    }

    //Esta funcion me elimina los datos de cliente almacenados en session compra
    public function actionDeletecliente()
    {
        //Para borrar cliente
        F_Proforma::setCliente(null);
        Yii::$app->response->redirect(array('proforma/create'));
    }

    //esta funcion ma permite calcular el precio de ganancia (menos descuento si hay) y el precio con iva desde los varores ingresados de
    public function actionPreciounitario()//cantidad - precio unitario y porcentage ganancia (descuento si hay)
    {
      if(Yii::$app->request->isAjax){//recibimos los valores por ajax para manipularlos en tiempo real
        $precio_unit_prov_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_prov_temporal')));//obtenemos precio unitario del proveedor
        $precio_unit_prov_temporal =  $precio_unit_prov_temporal == '' ? 0 : $precio_unit_prov_temporal; //condicionamos que el precio sea 0 si no existe todavia
        $cantidad_temporal = Yii::$app->request->post('cantidad_temporal');//obtenemos la cantidad
        $cantidad_temporal =  $cantidad_temporal == '' ? 0 : $cantidad_temporal; //condicionamos que la cantidad sea 0 si no exite todavia
        $porc_ganancias_temporal = Yii::$app->request->post('porc_ganancias_temporal'); //obtenemos el cantidad de la ganancia temporal
        $porc_ganancias_temporal =  $porc_ganancias_temporal == '' ? 0 : $porc_ganancias_temporal; //condicionamos que el porcentage sea 0 si no existe todavia
        $descuento = Yii::$app->request->post('descuento'); //obtenemos cantidad de descuento
        $descuento =  $descuento == '' ? 0 : $descuento; //condicionamos que el descuento sea 0 si no existe todavia
        //$precio_unit_prov_temporal = $precio_unit_prov_temporal == '' ? 0 : $precio_unit_prov_temporal;
        //$cantidad_temporal = $cantidad_temporal == '' ? 0 : $cantidad_temporal;
        $impuesto = Yii::$app->params['porciento_impuesto']; //impuesto
        $iva = $impuesto / 100; //porcentage de impuesto
        //la ganancia de obtiene del monto con cantidad por el porcentage de ganancia
        $ganancia = ($cantidad_temporal * $precio_unit_prov_temporal) * ($porc_ganancias_temporal / 100);
        //este se suma al monto iniciar que fue multiplicado por la cantidad
        $descuento_total = (($cantidad_temporal *  $precio_unit_prov_temporal) + $ganancia) * $descuento / 100;//salida
        //la cantidad total final solo le restamos el descuento
        $precio_total_ganancia_temporal = ( ($cantidad_temporal * $precio_unit_prov_temporal) + $ganancia ) - $descuento_total;//salida
        //a la cantidad con impuesto solo le sumamos al monto total de ganancia por a ese mismo valor por el porcentaje de impuesto
        $precio_mas_iva_total_temporal = $precio_total_ganancia_temporal + ($precio_total_ganancia_temporal * $iva);//salida
        //para obtener el precio unitario mas ganancia solo vasta con dividir la cantidad
        $precio_unit_gan_temporal = $precio_total_ganancia_temporal/$cantidad_temporal;//salida
        //se termina incluyendo el impuesto
        $precio_mas_iva_temporal = $precio_unit_gan_temporal + ($precio_unit_gan_temporal * $iva);//salida

        $arr = array('precio_unit_gan_temporal'=>number_format($precio_unit_gan_temporal,2),
                    'precio_mas_iva_temporal' => number_format($precio_mas_iva_temporal,2),
                    'monto_descuento' => number_format($descuento_total,2),
                    'precio_total_ganacia' => number_format($precio_total_ganancia_temporal,2),
                    'precio_mas_iva_total_temporal' => number_format($precio_mas_iva_total_temporal,2));
        echo json_encode($arr);
      }
    }

    //esta funcion me permite obtener el margen de ganancia y el precio mas iva (y el monto de descuento si es requerido)
    public function actionPrecioganancia()
    {
      if(Yii::$app->request->isAjax){//obtengo los valores por ajax
        $precio_unit_gan_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_gan_temporal')));//obtengo el precio de las ganancias sin iva
        $cantidad_temporal = Yii::$app->request->post('cantidad_temporal');//obtengo la cantidad
        $precio_unit_prov_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_prov_temporal')));//obtengo el precio unitario del proveedor
        $descuento = Yii::$app->request->post('descuento');//obtengo el descuento si se es considerado

        $cantidad_temporal = $cantidad_temporal == '' ? 0 : $cantidad_temporal;//obtengo cantidad
        //Monto * (100 + porcentaje) / 100
        $impuesto = Yii::$app->params['porciento_impuesto'];//predetermino el impuesto
        $iva = $impuesto / 100;//obtengo el % del descuento
        //aqui me condiciona que el descuento se exclulle si este viene incluido de lo contrario se ingresa normal el precio_unitario_gan_temporal
        $precio_unit_gan = $descuento == '' ? $precio_unit_gan_temporal : $precio_unit_gan_temporal * 100  / ( 100 - $descuento );
        $utilidad_individual = $precio_unit_gan - $precio_unit_prov_temporal;
        $subtotal_utilidad_indiv = $utilidad_individual / $precio_unit_prov_temporal;
        $porc_margen_temporal = $subtotal_utilidad_indiv * 100;//almacena el porcentage de ganancia

        $monto_descuento_individual = $descuento=='' ? 0 : $precio_unit_gan * $descuento / 100;//este no se muestra
        $monto_descuento_grupal = $monto_descuento_individual * $cantidad_temporal;//

        $precio_total_ganancia_temporal = $precio_unit_gan_temporal * $cantidad_temporal;//total ganacias es precio unit gan que en teoria ya viene con el descuento incluido por la cantidad

        $precio_mas_iva_temporal = $precio_unit_gan_temporal + ($precio_unit_gan_temporal * $iva);//($precio_unit_gan_temporal - $monto_descuento_individual) + ( ($precio_unit_gan_temporal - $monto_descuento_individual) * $iva );
        $precio_mas_iva_total_temporal = $precio_mas_iva_temporal*$cantidad_temporal;//

        $arr = array('porc_ganancias_temporal'=>number_format($porc_margen_temporal,2),
                     'precio_mas_iva_temporal' => number_format($precio_mas_iva_temporal,2),
                     'monto_descuento' => number_format($monto_descuento_grupal,2),
                     'precio_total_ganacia' => number_format($precio_total_ganancia_temporal,2),
                     'precio_mas_iva_total_temporal' => number_format($precio_mas_iva_total_temporal,2));
        echo json_encode($arr);
      }

    }

    public function actionPrecioiva()
    {
      if(Yii::$app->request->isAjax){
        $precio_mas_iva_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_temporal')));
        $cantidad_temporal = Yii::$app->request->post('cantidad_temporal');
        $precio_unit_prov_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_prov_temporal')));
        $descuento = Yii::$app->request->post('descuento');

        $cantidad_temporal = $cantidad_temporal == '' ? 0 : $cantidad_temporal;
        $descuento_individual = $descuento=='' ? 0 : $precio_unit_prov_temporal * $descuento / 100;//este no se muestra
        //$precio_unit_gan_temporal = $precio_unit_prov_temporal + $descuento_individual;//($precio_mas_iva_temporal/1.13) + 95.71;
        /* no se ocupa el impuesto porque ya estaá aplicado en $precio_mas_iva_temporal
        $impuesto = Yii::$app->params['porciento_impuesto'];
        $iva = $impuesto / 100;
        */
        /*$utilidad_individual = $precio_unit_gan_temporal - $precio_unit_prov_temporal;
        $subtotal_utilidad_indiv = $utilidad_individual / $precio_unit_prov_temporal;
        $porc_margen_temporal = $subtotal_utilidad_indiv * 100;//almacena el porcentage de ganancia
*/

        //salida precio unitario
        $precio_unit_gan_temporal = $precio_mas_iva_temporal/Yii::$app->params['reverso_impuesto'];
        //salida precio total mas ganancia
        $precio_total_ganancia_temporal = $precio_unit_gan_temporal * $cantidad_temporal;//total ganacias es precio unit gan que en teoria ya viene con el descuento incluido por la cantidad
        //salida precio total ganacia
        $precio_mas_iva_total_temporal = $precio_mas_iva_temporal * $cantidad_temporal;//

        //aqui me condiciona que el descuento se exclulle si este viene incluido de lo contrario se ingresa normal el precio_unitario_gan_temporal
        $precio_unit_gan = $descuento == '' ? $precio_unit_gan_temporal : $precio_unit_gan_temporal * 100  / ( 100 - $descuento );
        //obtengo la diferencia de del precio unitario de ganancia (sin el descuento) menos el precio unitario del proveedor
        $utilidad_individual = $precio_unit_gan - $precio_unit_prov_temporal;
        //el resultado lo dividimos por el precio unitario del proveedor temporal para obtener la utilidad
        $subtotal_utilidad_indiv = $utilidad_individual / $precio_unit_prov_temporal;
        //Salida margen % ganancia es igual a la utilidad por cien
        $porc_margen_temporal = $subtotal_utilidad_indiv * 100;//almacena el porcentage de ganancia

        $monto_descuento_individual = $descuento=='' ? 0 : $precio_unit_gan * $descuento / 100;//este no se muestra
        $monto_descuento_grupal = $monto_descuento_individual * $cantidad_temporal;//$descuento=='' ? 0 : $precio_total_ganancia_temporal * $descuento / 100;//este se muestra//
        //$monto_descuento_grupal = $cantidad_temporal*(($precio_unit_prov_temporal*$descuento)/100);

        //$precio_total_ganancia_temporal = $precio_total_ganancia_temporal - $monto_descuento_grupal;

        $arr = array('porc_ganancias_temporal'=>number_format($porc_margen_temporal,2),
                     'precio_unit_gan_temporal' => number_format($precio_unit_gan_temporal,2),
                     'monto_descuento' => number_format($monto_descuento_grupal,2),
                     'precio_total_ganacia' => number_format($precio_total_ganancia_temporal,2),
                     'precio_mas_iva_total_temporal' => number_format($precio_mas_iva_total_temporal,2));
        echo json_encode($arr);
      }
    }

    //agrega un producto temporal
    public function actionAdditem_temporal()
    {
      //Definimos c como null para comprobación
      $c=null;
      //Abrimos el contenido
      $compra = F_Proforma::getContenidoCompra();
      if(Yii::$app->request->isAjax){
        $descuento = Yii::$app->request->post('descuento');
        $codigo_temporal = Yii::$app->request->post('codigo_temporal');
        $descripcion_temporal = Yii::$app->request->post('descripcion_temporal');
        $checkiva = Yii::$app->request->post('checkiva');
        $cantidad_temporal = floatval(str_replace(",", "", Yii::$app->request->post('cantidad_temporal')));
        $precio_unit_prov_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_prov_temporal')));
        $porc_ganancias_temporal = floatval(str_replace(",", "", Yii::$app->request->post('porc_ganancias_temporal')));
        $precio_unit_gan_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_gan_temporal')));
        //$precio_mas_iva_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_temporal')));
        //$descuento_mod = floatval(str_replace(",", "", Yii::$app->request->post('descuento_hi')));
        $precio_total_ganacia_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_total_ganacia_hi')));
        $precio_mas_iva_total_temporal_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_total_temporal_hi')));
        //Consultamos si el existe producto para aumentar la cantidad
        if($compra)
         foreach ($compra as $position => $product)
            {
                if($product['cod']==$codigo_temporal)
                {
                $compra[$position]['cantidad']+=$cantidad_temporal;
                $c=1;
                }
            }

            if($c==null)
            {
                $_POST['cantidad']= $cantidad_temporal;
                $_POST['cod']= $codigo_temporal;
                $_POST['desc']= $descripcion_temporal;
                $_POST['precio']= $descuento == '' ? $precio_unit_gan_temporal : $precio_unit_gan_temporal * 100  / ( 100 - $descuento );//recordar que esto se cambia
                //$_POST['dcto']= ?;//recordar que esto se cambia
                $_POST['dcto'] = $descuento=='' ? '0.00' : $descuento;
                //Iva en forma manual, se puede cargar de la DB si se desea
                $_POST['iva'] = $checkiva != 'si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                $_POST['checkiva'] = $checkiva;
                /*...Estos son exclusivos para productos temporales...*/
                $_POST['temporal'] = 'x';
                $_POST['precio_unit_prov_temporal'] = $precio_unit_prov_temporal;
                $_POST['porc_ganancias_temporal'] = $porc_ganancias_temporal;
                $_POST['precio_unit_gan_temporal'] = $precio_unit_gan_temporal;
                //$_POST['precio_mas_iva_temporal'] = $precio_mas_iva_temporal;
                //$_POST['descuento_mod'] = $descuento_mod;
                $_POST['precio_total_ganacia_mod'] = $precio_total_ganacia_mod;
                $_POST['precio_mas_iva_total_temporal_mod'] = $precio_mas_iva_total_temporal_mod;
                /*.................................................*/
                $compra[] = $_POST;
                //echo $model->nombreProductoServicio;
            }
            //Guardamos el contenido
            F_Proforma::setContenidoCompra($compra);
            Yii::$app->response->redirect(array('proforma/create'));
      }//fin ajax
    }

    //actualiza el producto temporal
    public function actionModitem_temporal()
    {
      //Definimos c como null para comprobación
      $c=null;
      //Abrimos el contenido
      $compra = F_Proforma::getContenidoCompra();
      if(Yii::$app->request->isAjax){
        $codigo_id = Yii::$app->request->post('codigo_id');
        $descuento = Yii::$app->request->post('descuento');
        $codigo_temporal = Yii::$app->request->post('codigo_temporal');
        $descripcion_temporal = Yii::$app->request->post('descripcion_temporal');
        $cantidad_temporal = floatval(str_replace(",", "", Yii::$app->request->post('cantidad_temporal')));
        $precio_unit_prov_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_prov_temporal')));
        $porc_ganancias_temporal = floatval(str_replace(",", "", Yii::$app->request->post('porc_ganancias_temporal')));
        $precio_unit_gan_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_gan_temporal')));
        $precio_total_ganacia_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_total_ganacia_hi')));
        $precio_mas_iva_total_temporal_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_total_temporal_hi')));
        $checkiva_ = Yii::$app->request->post('checkiva_');
        if ($compra)
        foreach ($compra as $position => $comp) {
          if($position==$codigo_id)
          {
            $compra[$position]['cantidad'] = $cantidad_temporal;
            $compra[$position]['cod'] = $codigo_temporal;
            $compra[$position]['desc'] = $descripcion_temporal;
            $compra[$position]['precio'] = $descuento == '' ? $precio_unit_gan_temporal : $precio_unit_gan_temporal * 100  / ( 100 - $descuento );//recordar que esto se cambia
            $compra[$position]['dcto'] = $descuento=='' ? '0.00' : $descuento;
            $compra[$position]['iva'] = $checkiva_ != 'si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
            $compra[$position]['checkiva'] = $checkiva_;
            $compra[$position]['temporal'] = 'x';
            $compra[$position]['precio_unit_prov_temporal'] = $precio_unit_prov_temporal;
            $compra[$position]['porc_ganancias_temporal'] = $porc_ganancias_temporal;
            $compra[$position]['precio_unit_gan_temporal'] = $precio_unit_gan_temporal;
            $compra[$position]['precio_total_ganacia_mod'] = $precio_total_ganacia_mod;
            $compra[$position]['precio_mas_iva_total_temporal_mod'] = $precio_mas_iva_total_temporal_mod;
          }
          //Guardamos el contenido
          F_Proforma::setContenidoCompra($compra);
          Yii::$app->response->redirect(array('proforma/create'));
        }
        }
      }

    //esta funcion me permitirá ingresar PRODUCTOS TEMPORALES a base de datos para update
    	function actionAdditem_temporal_update() {
        $detallePro = new DetalleProformas();
        //Definimos c como null para comprobación
        $c=null;
        //Abrimos el contenido
        $compra = F_Proforma::getContenidoComprau();

        if(Yii::$app->request->isAjax){
            //obtenemos los datos por post
            $descuento = Yii::$app->request->post('descuento');
            $codProdServicio = Yii::$app->request->post('codigo_temporal');
            $descripcion_temporal = Yii::$app->request->post('descripcion_temporal');
            $cantidad = floatval(str_replace(",", "", Yii::$app->request->post('cantidad_temporal')));
            $checkiva = Yii::$app->request->post('checkiva');
            $precio_unit_prov_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_prov_temporal')));
            $porc_ganancias_temporal = floatval(str_replace(",", "", Yii::$app->request->post('porc_ganancias_temporal')));
            $precio_unit_gan_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_gan_temporal')));
            //$precio_mas_iva_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_temporal')));
            //$descuento_mod = floatval(str_replace(",", "", Yii::$app->request->post('descuento_hi')));
            $precio_total_ganacia_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_total_ganacia_hi')));
            $precio_mas_iva_total_temporal_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_total_temporal_hi')));
            $cabezafactura = Yii::$app->request->post('idCabeza');

                   //Consultamos si el existe producto para aumentar la cantidad en tbl_detalle_facturas
                    $sql = "SELECT * FROM tbl_detalle_proforma WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $codProdServicio);
                    $command->bindValue(":idCabeza_factura", $cabezafactura);
                    $consulta = $command->queryOne();
                    if($compra)
                    if($consulta['codProdServicio']==$codProdServicio && $consulta['idCabeza_factura']==$cabezafactura)
                        {
                           //actualizamos en tbl_detalle_facturas en la nueva cantidad
                           $cantidadnueva = $consulta['cantidad'];//cantidad de tbl_detalle_facturas
                           $cantidadnueva+=$cantidad;
                           $detalle_prof = DetalleProformas::find()->where(['=','codProdServicio', $codProdServicio])->andWhere("idCabeza_factura = :idCabeza_factura", [":idCabeza_factura"=>$cabezafactura])->one();
                           $detalle_prof->cantidad = $cantidadnueva;
                           $detalle_prof->save();
                          $c=1;
                        }

                //Si no existe producto repetido, se ingresa normalmente
                if($c==null)
                {
                    $detallePro->idCabeza_factura = $cabezafactura;
                    $detallePro->codProdServicio = $codProdServicio;
                    $detallePro->cantidad = $cantidad;
                    $precio_unitario = $descuento == '' ? $precio_unit_gan_temporal : $precio_unit_gan_temporal * 100  / ( 100 - $descuento );
                    $detallePro->precio_unitario = $precio_unitario;//$precio_unit_prov_temporal;
                    $detallePro->precio_por_cantidad = round(($precio_unitario*$cantidad),2);
                    $descuento_producto = $descuento == '' ? 0.00 : round($cantidad*(($precio_unitario*$descuento)/100),2);
                    $detallePro->descuento_producto = $descuento_producto;
                    $detallePro->subtotal_iva = $checkiva != 'si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                    $detallePro->descrip_temporal = $descripcion_temporal;
                    //------------solo para producto temporal
                    $detallePro->temporal = 'x';
                    $detallePro->checkiva = $checkiva;
                    $detallePro->precio_unit_prov_temporal = $precio_unit_prov_temporal;
                    $detallePro->porc_ganancias_temporal = $porc_ganancias_temporal;
                    $detallePro->precio_unit_gan_temporal = $precio_unit_gan_temporal;
                    $detallePro->precio_total_ganacia_mod = $precio_total_ganacia_mod;
                    $detallePro->precio_mas_iva_total_temporal_mod = $precio_mas_iva_total_temporal_mod;
                    //---------fin solo producto temporal
                    $detallePro->save();

                }
                //Guardamos el contenido
                F_Proforma::setContenidoComprau($compra);
                Yii::$app->response->redirect(array('proforma/update', 'id'=>$cabezafactura));
        }
    	}

    //funcion para actualizar los item de los productos temporale en update
    public function actionModitem_temporal_update()
    {
      $detallePro = new DetalleProformas();
      //Definimos c como null para comprobación
      $c=null;
      //Abrimos el contenido
      //$compra = F_Proforma::getContenidoComprau();

      if(Yii::$app->request->isAjax){
        //obtenemos los datos por post
        $descuento = Yii::$app->request->post('descuento');
        $codProdServicio = Yii::$app->request->post('codigo_temporal');
        $descripcion_temporal = Yii::$app->request->post('descripcion_temporal');
        $checkiva = Yii::$app->request->post('checkiva_');
        $cantidad = floatval(str_replace(",", "", Yii::$app->request->post('cantidad_temporal')));
        $precio_unit_prov_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_prov_temporal')));
        $porc_ganancias_temporal = floatval(str_replace(",", "", Yii::$app->request->post('porc_ganancias_temporal')));
        $precio_unit_gan_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_unit_gan_temporal')));
        //$precio_mas_iva_temporal = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_temporal')));
        //$descuento_mod = floatval(str_replace(",", "", Yii::$app->request->post('descuento_hi')));
        $precio_total_ganacia_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_total_ganacia_hi')));
        $precio_mas_iva_total_temporal_mod = floatval(str_replace(",", "", Yii::$app->request->post('precio_mas_iva_total_temporal_hi')));
        $cabezafactura = Yii::$app->request->post('idCabeza');

        $detallePro = DetalleProformas::find()->where(['=','codProdServicio', $codProdServicio])->andWhere("idCabeza_factura = :idCabeza_factura", [":idCabeza_factura"=>$cabezafactura])->one();
        //Si no existe producto repetido, se ingresa normalmente
        if($c==null)
        {
            $detallePro->idCabeza_factura = $cabezafactura;
            $detallePro->codProdServicio = $codProdServicio;
            $detallePro->cantidad = $cantidad;
            $precio_unitario = $descuento == '' ? $precio_unit_gan_temporal : $precio_unit_gan_temporal * 100  / ( 100 - $descuento );
            $detallePro->precio_unitario = $precio_unitario;//$precio_unit_prov_temporal;
            $detallePro->precio_por_cantidad = round(($precio_unitario*$cantidad),2);
            $descuento_producto = $descuento == '' ? 0.00 : round($cantidad*(($precio_unitario*$descuento)/100),2);
            $detallePro->descuento_producto = $descuento_producto;
            $detallePro->subtotal_iva = $checkiva != 'si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
            $detallePro->descrip_temporal = $descripcion_temporal;
            //------------solo para producto temporal
            $detallePro->temporal = 'x';
            $detallePro->checkiva = $checkiva;
            $detallePro->precio_unit_prov_temporal = $precio_unit_prov_temporal;
            $detallePro->porc_ganancias_temporal = $porc_ganancias_temporal;
            $detallePro->precio_unit_gan_temporal = $precio_unit_gan_temporal;
            $detallePro->precio_total_ganacia_mod = $precio_total_ganacia_mod;
            $detallePro->precio_mas_iva_total_temporal_mod = $precio_mas_iva_total_temporal_mod;
            //---------fin solo producto temporal
            $detallePro->save();

        }
        //Guardamos el contenido
        //F_Proforma::setContenidoComprau($compra);
        Yii::$app->response->redirect(array('proforma/update', 'id'=>$cabezafactura));
      }
    }

    //Esta funcion me agrega los datos del detalle en session compra
    //Agregar datos para la proforma (create)
    public function actionAdditem()
    {
        //Definimos c como null para comprobación
        $c=null;
        //Abrimos el contenido
        $compra = F_Proforma::getContenidoCompra();

        if(Yii::$app->request->isAjax){
            $codProdServicio = Yii::$app->request->post('codProdServicio');
            $cantidad = Yii::$app->request->post('cantidad');
            $descuento = Yii::$app->request->post('descuento');

            //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
            $sql = "SELECT cantidadInventario, cantidadMinima FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindParam(":codProdServicio", $codProdServicio);
            $idproducto = $command->queryOne();

            //$nuevacantidad = $idproducto['cantidadInventario'] - $cantidad; //Aqui la alamcenamos la nueva cantidad
           /*if ($nuevacantidad >= $idproducto['cantidadMinima']){
                    echo 'cantidad_minima';
                }*/

                //actualizamos en tbl_producto_servicios el la nueva cantidad
               /* $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                $command2 = \Yii::$app->db->createCommand($sql2);
                $command2->bindParam(":cantidad", $nuevacantidad);
                $command2->bindParam(":codProdServicio", $codProdServicio);
                $command2->execute();*/

                //Consultamos si el existe producto para aumentar la cantidad
                if($compra)
                 foreach ($compra as $position => $product)
                    {
                        if($product['cod']==$codProdServicio)
                        {
                        $compra[$position]['cantidad']+=$cantidad;
                        $c=1;
                        }
                    }
                //Si no existe producto repetido, se asigna lo mandado por POST
                $model_ = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                if($c==null)
                {
                  $model = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                  $iv = $model->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                  if(@$cliente = F_Proforma::getCliente()){
                    if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$cliente])->one())
                    {
                        $iv = $cliente_ob->iv == true ? '0.00' : $iv;
                    }
                  }
                    $model = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                    $_POST['cod']= $model->codProdServicio;
                    $_POST['desc']= $model->nombreProductoServicio;
                    $_POST['precio']= $model->precioVenta;//recordar que esto se cambia
                    //$_POST['dcto']= ?;//recordar que esto se cambia
                    $_POST['dcto'] = $model->anularDescuento!='Si' ? $descuento : '0.00';
                    //Iva en forma manual, se puede cargar de la DB si se desea
                    $_POST['iva'] = $iv;
                    $_POST['temporal'] = '';
                    $compra[] = $_POST;
                    //echo $model->nombreProductoServicio;
                }
                //Guardamos el contenido
                F_Proforma::setContenidoCompra($compra);

                /*if ( $model_->cantidadMinima >= $model_->cantidadInventario) {
                    Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$model_->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                    Yii::$app->response->redirect(array('proforma/create'));
                }*/
                if ( ($idproducto['cantidadInventario'] == 0/*$idproducto['cantidadMinima']*/) ) {
                    Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> Tenga presente que no hay más cantidad en stock para este producto.');
                    Yii::$app->response->redirect(array('proforma/create'));
                } else if ( $model_->cantidadMinima >= $model_->cantidadInventario ) {
                    Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> Considere que '.$model_->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima.');
                    Yii::$app->response->redirect(array('proforma/create'));
                }
                Yii::$app->response->redirect(array('proforma/create'));
            }

        //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> no se obtubo el dato');
        //Yii::$app->response->redirect(array('proforma/create'));
        //return $this->render('create', ['time' => date('H:i:s')]);
    }

    //Esta funcion me agrega los datos del detalle en session compra_update
            //Agregar datos para la prefactura (update manual)
            public function actionAdditemu()
            {
                $detallePro = new DetalleProformas();
                //Definimos c como null para comprobación
                $c=null;
                //Abrimos el contenido
                $compra = F_Proforma::getContenidoComprau();

                if(Yii::$app->request->isAjax){
                    //obtenemos los datos por post
                    $codProdServicio = Yii::$app->request->post('codProdServicio');
                    $cantidad = Yii::$app->request->post('cantidad');
                    $descuento = Yii::$app->request->post('descuento');
                    $cabezafactura = Yii::$app->request->post('idCabeza');

                        //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                        $sql_ = "SELECT cantidadInventario, cantidadMinima FROM tbl_producto_servicios WHERE codProdServicio = :codProd";
                        $command_ = \Yii::$app->db->createCommand($sql_);
                        $command_->bindParam(":codProd", $codProdServicio);
                        $idproducto = $command_->queryOne();
                        //$nuevacantidad = $idproducto['cantidadInventario'] - $cantidad; //Aqui la alamcenamos la nueva cantidad

                        //actualizamos en tbl_producto_servicios en la nueva cantidad
                        /*$sql2_ = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                        $command2_ = \Yii::$app->db->createCommand($sql2_);
                        $command2_->bindParam(":cantidad", $nuevacantidad);
                        $command2_->bindParam(":codProdServicio", $codProdServicio);
                        $command2_->execute();*/

                           //Consultamos si el existe producto para aumentar la cantidad en tbl_detalle_facturas
                            $sql = "SELECT * FROM tbl_detalle_proforma WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                            $command = \Yii::$app->db->createCommand($sql);
                            $command->bindParam(":codProdServicio", $codProdServicio);
                            $command->bindValue(":idCabeza_factura", $cabezafactura);
                            $consulta = $command->queryOne();
                            if($compra)
                            if($consulta['codProdServicio']==$codProdServicio && $consulta['idCabeza_factura']==$cabezafactura)
                                {
                                       /* $compra[$position]['cantidad']+=$cantidad;
                                       $c=1;*/
                                       //actualizamos en tbl_detalle_facturas en la nueva cantidad
                                       $cantidadnueva = $consulta['cantidad'];//cantidad de tbl_detalle_facturas
                                       $cantidadnueva+=$cantidad;
                                       $sql2 = "UPDATE tbl_detalle_proforma SET cantidad = :cantidad WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                                       $command2 = \Yii::$app->db->createCommand($sql2);
                                       $command2->bindParam(":cantidad", $cantidadnueva);
                                       $command2->bindParam(":codProdServicio", $codProdServicio);
                                       $command2->bindValue(":idCabeza_factura", $cabezafactura);
                                       $command2->execute();
                                    $c=1;
                                }

                       // }
                        //Si no existe producto repetido, se ingresa normalmente
                        $model_ = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                        if($c==null)
                        {
                            $resultdescuento = $model_->anularDescuento!='Si' ? $descuento : '0';
                            $subtotal_iva = $model_->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0';
                            $cabeFa = Proforma::find()->where(['idCabeza_Factura'=>$cabezafactura])->one();
                              if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$cabeFa->idCliente])->one())
                              {
                                  $subtotal_iva = $cliente_ob->iv == true ? '0.00' : $subtotal_iva;
                              }
                            $precio_por_cantidad = $cantidad*$model_->precioVenta;
                            $descuento_producto = $precio_por_cantidad*($resultdescuento/100);

                            $detallePro->idCabeza_factura = $cabezafactura;
                            $detallePro->codProdServicio = $model_->codProdServicio;
                            $detallePro->cantidad = $cantidad;
                            $detallePro->precio_unitario = $model_->precioVenta;
                            $detallePro->precio_por_cantidad = $precio_por_cantidad;
                            $detallePro->descuento_producto = $descuento_producto;
                            $detallePro->subtotal_iva = $subtotal_iva;

                            $detallePro->save();

                        }
                        //Guardamos el contenido
                        F_Proforma::setContenidoComprau($compra);
                       if ($idproducto['cantidadInventario'] == 0) {
                            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> Tenga presente que no hay más cantidad en stock para este producto.');
                            Yii::$app->response->redirect(array('proforma/update', 'id'=>$cabezafactura));

                        }else if ($model_->cantidadMinima >= $model_->cantidadInventario){
                            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> Considere que '.$model_->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima.');
                            Yii::$app->response->redirect(array('proforma/update', 'id'=>$cabezafactura));
                        }
                    Yii::$app->response->redirect(array('proforma/update', 'id'=>$cabezafactura));
                }

            }

    public function actionComplete(){
    if(($estadoF = F_Proforma::getEstadoFactura()) && ($FechaCompraIni = F_Proforma::getFecha()) && ($vendedor = F_Proforma::getVendedor()) && ($cliente = F_Proforma::getCliente()) && ($productos = F_Proforma::getContenidoCompra()))
    {
        $CabCompra = new Proforma();
        $resumen = F_Proforma::getTotal(true);
        $CabCompra->fecha_inicio = date('Y-m-d');
        $CabCompra->idCliente = $cliente;
        $CabCompra->porc_descuento = $resumen['dcto'];
        $CabCompra->iva = $resumen['iva'];
        $CabCompra->total_a_pagar = $resumen['total'];
        $CabCompra->estadoFactura = $estadoF;
        $CabCompra->tipoFacturacion = 'Ninguno';
        $CabCompra->codigoVendedor = $vendedor;
        $CabCompra->observaciones = F_Proforma::getObserva() ? F_Proforma::getObserva() : 'Ninguna';
        $CabCompra->subtotal = $resumen['subtotal'];

      //  if ($CabCompra->validate())
        //{   #guardamos cabecera de factura
            if ($CabCompra->save()) {
            // guardamos detalle factura
                foreach($productos as $position => $product)
                {
                    $position = new DetalleProformas();
                    $position->idCabeza_factura = $CabCompra->idCabeza_Factura;
                    $position->codProdServicio = $product['cod'];
                    $position->cantidad = $product['cantidad'];
                    $position->precio_unitario = $product['precio'];
                    $position->precio_por_cantidad = round(($product['precio']*$product['cantidad']),2);
                    $position->descuento_producto = round(/*$product['cantidad']**/(($product['precio']*$product['dcto'])/100),2);
                    $position->subtotal_iva = $product['iva'];
                    if ($product['temporal']=='x') {
                      $position->descrip_temporal = $product['desc'];
                      $position->temporal = $product['temporal'];
                      $position->checkiva = $product['checkiva'];
                      $position->precio_unit_prov_temporal = round($product['precio_unit_prov_temporal'],2);
                      $position->porc_ganancias_temporal = round($product['porc_ganancias_temporal'],2);
                      $position->precio_unit_gan_temporal = round($product['precio_unit_gan_temporal'],2);
                      $position->precio_total_ganacia_mod = round($product['precio_total_ganacia_mod'],2);
                      $position->precio_mas_iva_total_temporal_mod = round($product['precio_mas_iva_total_temporal_mod'],2);
                    }
                    $position->save();
                }
                $this->borrarTodo();
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Proforma almacenada correctamente.');
                Yii::$app->response->redirect(array('proforma/update', 'id'=>$CabCompra->idCabeza_Factura));
            }
      //  }
        else {
            $error='';
            foreach ($CabCompra->getErrors() as $position => $er)
            $error.=$er[0].'<br/>';
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>'.$CabCompra->estadoFactura.'Disculpa, no se efectuó el proceso de proforma.');
            Yii::$app->response->redirect(array('proforma/create'));
        }
    }
  }//fin action complete

    //funcion para generar una prefactura
    public function actionPrefactura($id)
    {
      $model = $this->findModel($id);
    /*  if ($model->idOrdenServicio) {

        Yii::$app->getSession()->setFlash('error ', ' <span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> NO se pudo generar la prefactura.');
        Yii::$app->response->redirect(array('proforma/update', 'id'=>$id));
      } else {*/
      $prefactura = new EncabezadoPrefactura();
      $prefactura->fecha_inicio = date('Y-m-d');
      $prefactura->idCliente = $model->idCliente;
      $prefactura->idOrdenServicio = '';
      $prefactura->porc_descuento = $model->porc_descuento;
      $prefactura->iva = $model->iva;
      $prefactura->total_a_pagar = $model->total_a_pagar;
      $prefactura->estadoFactura = 'Pendiente';
      $prefactura->tipoFacturacion = $model->tipoFacturacion;
      $prefactura->codigoVendedor = $model->codigoVendedor;
      $prefactura->subtotal = $model->subtotal;
      $prefactura->observacion = $model->observaciones;

      if ($prefactura->save()) {
        $detalle_prof = DetalleProformas::find()->where(['=','idCabeza_factura', $model->idCabeza_Factura])->all();
        foreach($detalle_prof as $position => $product){
          $inventario = ProductoServicios::find()->where(['codProdServicio'=>$product->codProdServicio])->one();
          //debemos de limitar a que sean solo productos en base de datos y los productos que sean mayores que 0
          if (($product->temporal!='x' and @$inventario->cantidadInventario > 0) || $product->codProdServicio=='0') {
          $position = new DetalleFacturas();//creamos nueva instancia del detalle de la factura
          //luego asignamos valores a las variables correspondientes al detalle de la factura
          $position->idCabeza_factura = $prefactura->idCabeza_Factura;
          $cantidadInventariov = 0;//asignamos un valor en cero de cantidad de inventario que vamos a ir llenando con forme tenemos el permiso
          //hemos dejamo de utimo a la cantidad porque no vamos a poner lo que viene en proforma sino lo que avastese el inventario
            if ($product->codProdServicio=='0') {//
              //si le codigo de producto servicio es 0 consiste en una orden de sericio, por lo tanto entra en este paso
                //creo una instancia de la orden de servicio que voy a modificar para que este activa
                $orden_servicio = OrdenServicio::find()->where(['idOrdenServicio' => $model->idOrdenServicio])->one();
                $orde_ser_new = new OrdenServicio();
                $orde_ser_new->fecha = date('Y-m-d');
                $orde_ser_new->montoServicio = $orden_servicio->montoServicio;
                $orde_ser_new->idCliente = $orden_servicio->idCliente;
                $orde_ser_new->placa = $orden_servicio->placa;
                $orde_ser_new->idMecanico = $orden_servicio->idMecanico;
                $orde_ser_new->radio = 'Si';
                $orde_ser_new->alfombra = 'Si';
                $orde_ser_new->herramienta = 'Si';
                $orde_ser_new->repuesto = 'Si';
                $orde_ser_new->cantOdometro = 0;
                $orde_ser_new->cantCombustible = '1/2';
                $orde_ser_new->observaciones = 'Sin observaciones';
                $orde_ser_new->observacionesServicio = 'Orden de servicio lista para facturar';
                $orde_ser_new->estado = 1;
                $orde_ser_new->famili = \yii\helpers\ArrayHelper::getColumn($orden_servicio->getTblDetalleOrdServicios()->asArray()->all(),
                    'familia');
                $orde_ser_new->servi = \yii\helpers\ArrayHelper::map(DetalleOrdServicio::findAll(['idOrdenServicio' => $model->idOrdenServicio]),'producto','producto');
                if ($orde_ser_new->save()) {
                  $prefactura = EncabezadoPrefactura::findOne($prefactura->idCabeza_Factura);
                  $cliente = Clientes::find()->where(['idCliente'=>$prefactura->idCliente])->one();
                  $prefactura->idOrdenServicio = $orde_ser_new->idOrdenServicio;
                  $prefactura->save();
                  $position->codProdServicio = 0;
                  $position->precio_unitario = $orden_servicio->montoServicio;
                  $position->precio_por_cantidad = $orden_servicio->montoServicio;
                  $position->descuento_producto = 0.00;
                  $position->subtotal_iva = $cliente->iv == true ? 0.00 : Yii::$app->params['porciento_impuesto'];
                }
            } else {
              //aqui solo entra producto
              $position->codProdServicio = $product->codProdServicio;
              $position->precio_unitario = $product->precio_unitario;
              $position->precio_por_cantidad = $product->precio_por_cantidad;
              $position->descuento_producto = $product->descuento_producto;
              $position->subtotal_iva = $product->subtotal_iva;
              $contador = $inventario->cantidadInventario; //llenamos un contador con toda la cantidad de inventario
                for ($i = $product->cantidad; $i > 0; $i--) { //recorremos en un ciclo limitado a la cantidad que trae este producto en la proforma
                    $cantidadInventariov += $contador == 0 ? 0 : 1; //empesamos a llenar la variable con el contador 1x1 solo si el contador no ha llegado a 0
                    $contador -= $contador == 0 ? 0 : 1; //por cada vuelta en el ciclo voy restando el valor del contador, si este llegó a 0 ya no se va a restar mas
                    //esto me permite mantener el control de inventario y que no tenga valores negativos
                }
                //$cantidadInventariov sería lo que avastece inventario
                $inventario->cantidadInventario -= $cantidadInventariov;
                $inventario->save();//y actualizamos cantidad en inventario hasta donde este lo abastece
            }
          //y como es lo que avastese entonces eso es lo que se agrega en la prefactura
          $position->cantidad += $cantidadInventariov;
          $position->save();//y luego guardamos la prefactura
          } else {
            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Atención!</strong> Algunos productos no se encuentra en inventario o están agotados.');
          }
        }
        $this->historial_producto($prefactura->idCabeza_Factura, 'RES');
        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Pre-factura creada correctamente (Si la pro-forma contaba con productos temporales estos no se añadieron a la pre-factura).<br> Si observa que hay menos cantidad en algunos productos compruebe si fue que el inventario ya no abastecía. <br>El sistema solo puede reservar los productos que abastece el inventario.');
        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$prefactura->idCabeza_Factura));
      } else {
        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> NO se pudo generar la prefactura.');
        Yii::$app->response->redirect(array('proforma/update', 'id'=>$id));
      }
    //}
    }

    //Esta funcion me agrega la observacion en session F_Proforma
            public function actionAddobservacion(){
                if(Yii::$app->request->isAjax){
                    $observacion = Yii::$app->request->post('observacion');
                    F_Proforma::setObserva($observacion);
                }
            }

    //Esta funcion actualiza la observacion en base de datos
            public function actionAddobservacionU(){
                if(Yii::$app->request->isAjax){
                    $observacion = Yii::$app->request->post('observacion');
                    $idCabeza = Yii::$app->request->post('idCabeza');
                    $sql = "UPDATE tbl_proforma SET observaciones = :observaciones WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":observaciones", $observacion);
                        $command->bindParam(":id", $idCabeza);
                        $command->execute();
                }
            }

    //Esta funsion me elimina la linea que selecciono eliminar en el detalle de la proforma - create
    public function actionDeleteitem()
    {
        $id = intval($_GET['id']);

        //Descodificamos el array JSON
        $compra = JSON::decode(Yii::$app->session->get('compra_prof'), true);
        //Eliminamos el atributo pasado por parámetro
        unset($compra[$id]);
        //Volvemos a codificar y guardar el contenido
        Yii::$app->session->set('compra_prof', JSON::encode($compra));

        Yii::$app->response->redirect(array('proforma/create'));

    }

    //Esta funsion me elimina la linea que selecciono eliminar en el detalle de la prefactura - update
            public function actionDeleteitemu()
            {
                $cod = $_GET['id0'];
                $id = intval($_GET['id1']);
                $idCabeza = intval($_GET['id2']);
                $can = intval($_GET['can']);

                //Descodificamos el array JSON
                $compra = JSON::decode(Yii::$app->session->get('compra'), true);
                //Eliminamos el atributo pasado por parámetro
                unset($compra[$id]);
                //Volvemos a codificar y guardar el contenido
                Yii::$app->session->set('compra', JSON::encode($compra));
                //Compra_update::setContenidoComprau(null);
                //Eliminamos de la base de datos
                \Yii::$app->db->createCommand()->delete('tbl_detalle_proforma', 'idCabeza_factura = '.$idCabeza.' AND codProdServicio = "'.$cod.'"' )->execute();
                //Regresamos a la prefactura
                Yii::$app->response->redirect(array( 'proforma/update', 'id' => $idCabeza ));
            }

    public function actionDeletevendedorU($id)
    {
        $sql = "UPDATE tbl_proforma SET codigoVendedor = null WHERE idCabeza_Factura = :id";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":id", $id);
                $command->execute();
        F_Proforma::setVendedoru('');//Para borrar vendedor al actualizar
        Yii::$app->response->redirect(array('proforma/update','id' => $id));
    }

    //función para actualizar DESCUENTO desde la edicion de cantidad en la tabla de CREATE
    public function actionUpdatedescuento()
    {
        //Abrimos el contenido
        $compra = F_Proforma::getContenidoCompra();
        $value = null;
        foreach($_GET as $key => $value)
            {
                if(substr($key, 0, 9) == 'cantidad_')
                {

                    if (!is_numeric($value) || $value <= 0 || $value == ' ')
                        {
                            throw new \yii\base\Exception('Cantidad Invalida');
                        }
                    else
                        {
                            $position = explode('_', $key);
                            $position = $position[1];

                            $cantidad = null; $cod = null;
                            //Si existe cantidad
                            if(isset($compra[$position]['cantidad']))
                            {
                                $cod = $compra[$position]['cod'];
                                $cantidad = $compra[$position]['cantidad'];
                            }
                            //echo number_format($compra[$position]['cantidad']*(($compra[$position]['precio']*$compra[$position]['dcto'])/100),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                            //echo $value*$precio;

                                $compra[$position]['cantidad'] = $value;
                                echo number_format($compra[$position]['cantidad']*(($compra[$position]['precio']*$compra[$position]['dcto'])/100),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                                if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario) {
                                    if ($compra[$position]['temporal']!='x') {
                                      Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                                    }
                                    Yii::$app->response->redirect(array('proforma/create'));
                                    return F_Proforma::setContenidoCompra($compra);
                                }
                                else { return F_Proforma::setContenidoCompra($compra); }


                               //Guardamos el contenido
                            //return Compra::setContenidoCompra($compra);
                        }
                }  $value = null;
            }

    }

    //función para actualizar DESCUENTO desde la edicion de cantidad en la tabla de prefactura UPDATE
    public function actionUpdatedescuentou()
    {
        //Abrimos el contenido
        $compra = F_Proforma::getContenidoComprau();
        $cab = F_Proforma::getCabezaFactura();

        foreach($_GET as $key => $value)
            {
                if(substr($key, 0, 9) == 'cantidad_')
                {
                    if (!is_numeric($value) || $value <= 0 || $value == ' ')
                        {
                            throw new \yii\base\Exception('Cantidad Invalida');
                        }
                    else
                        {
                            $position = explode('_', $key);
                            $position = $position[1];
                            $cantidad = null; $cod = null; $c_producto = null;
                            //Si existe cantidad
                            if(isset($compra[$position]['cantidad']))
                            {
                                $cod = $compra[$position]['cod'];
                                $cantidad = $compra[$position]['cantidad'];
                            }

                                $compra[$position]['cantidad'] = $value;
                                echo number_format($compra[$position]['cantidad']*(($compra[$position]['dcto'])),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                                if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario) {
                                    Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                                    Yii::$app->response->redirect(array('proforma/update', 'id'=>$cab));
                                    return F_Proforma::setContenidoComprau($compra);
                                }
                                else { return F_Proforma::setContenidoComprau($compra); }

                        }
                } $value = null;
            }

    }

    //Esta funcion me amacena los datos del vendedor en session compra_update
            public function actionVendedor2U(){
                $id = F_Proforma::getCabezaFactura();
                if(Yii::$app->request->isAjax){
                    $idFuncionario = Yii::$app->request->post('idFuncionario');
                    $sql = "UPDATE tbl_proforma SET codigoVendedor = :idFuncionario WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":idFuncionario", $idFuncionario);
                        $command->bindParam(":id", $id);
                        $command->execute();
                    F_Proforma::setVendedoru((int)$idFuncionario);
                    Yii::$app->response->redirect(array('proforma/update','id' => $id));
                }
            }

    //funcion para devolver el dato TOTAL modificando la cantidad para la tabla CREATE en proforma
    public function actionUpdatecantidad()
    {
        //Abrimos el contenido
        $compra = F_Proforma::getContenidoCompra();

        foreach($_GET as $key => $value)
            {
                if(substr($key, 0, 9) == 'cantidad_')
                {

                    if (!is_numeric($value) || $value <= 0 || $value == ' ')
                        {
                            throw new \yii\base\Exception('Cantidad Invalida');
                        }
                    else
                        {
                            $position = explode('_', $key);
                            $position = $position[1];

                            //Si existe cantidad
                            if(isset($compra[$position]['cantidad'])){

                                $cod = $compra[$position]['cod'];
                                $cantidad = $compra[$position]['cantidad'];
                                //$compra[$position]['cantidad'] = $value;
                            }


                                $compra[$position]['cantidad'] = $value;
                                $precio=$compra[$position]['precio']*$compra[$position]['cantidad'];
                                echo number_format(($precio),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                //Guardamos el contenido
                                return F_Proforma::setContenidoCompra($compra);

                        }
                } $value = null;
            }

    }

    //funcion para devolver el dato TOTAL modificando la cantidad para la tabla UPDATE en prefactura y en la base de datos
            public function actionUpdatecantidadu()
            {
                //Abrimos el contenido
                $compra = F_Proforma::getContenidoComprau();
                $cab = F_Proforma::getCabezaFactura();

                foreach($_GET as $key => $value)
                    {
                        if(substr($key, 0, 9) == 'cantidad_')
                        {
                            if (!is_numeric($value) || $value <= 0 || $value == ' ')
                                {
                                    throw new \yii\base\Exception('Cantidad Invalida');
                                }
                            else
                                {
                                    $position = explode('_', $key);
                                    $position = $position[1];

                                    $cantidad = null; $cod = null;

                                    //Si existe cantidad
                                    if(isset($compra[$position]['cantidad']))
                                    {
                                        $cod = $compra[$position]['cod'];
                                        $cantidad = $compra[$position]['cantidad'];
                                    }

                                        $compra[$position]['cantidad'] = $value;
                                        //$precio=$compra[$position]['precio']-(($compra[$position]['precio']*$compra[$position]['dcto'])/100);
                                        $precio=$compra[$position]['precio']*$compra[$position]['cantidad'];
                                        //$model = Productos::model()->findByPk($compra[$position]['id_producto']);
                                         //$resultado=ProductoProveedor::getProductoProveedor($compra[$position]['id_producto']);
                                        //echo number_format(($value*$precio),2);
                                        echo number_format(($precio),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                        //Guardamos cantidad en base de datos
                                        $sql = "SELECT codProdServicio FROM tbl_producto_servicios WHERE nombreProductoServicio = :producto";
                                        $command = \Yii::$app->db->createCommand($sql);
                                        $command->bindParam(":producto", $compra[$position]['desc']);
                                        $idproducto = $command->queryOne();

                                        if ($idproducto['codProdServicio']!='0') {
                                            $sql2 = "UPDATE tbl_detalle_proforma SET cantidad = :cantidad WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                                            $command2 = \Yii::$app->db->createCommand($sql2);
                                            $command2->bindParam(":cantidad", $compra[$position]['cantidad']);
                                            $command2->bindParam(":codProdServicio", $idproducto['codProdServicio']);
                                            $command2->bindValue(":idCabeza_factura", $cab);
                                            $command2->execute();
                                        }
                                        //Guardamos el contenido en session
                                        return F_Proforma::setContenidoComprau($compra);

                                }
                        }
                    }

            }

    public function actionGetpreciototal() {
    //Imprimimos el resumen
    echo F_Proforma::getTotal();
    }

    public function actionGetpreciototalu() {
    //Imprimimos el resumen
    echo F_Proforma::getTotalu();
    }

    //función para borrar datos en session compra y regresar al index
    public function actionRegresarborrar()
    {
        $this->borrarTodo();
        Yii::$app->response->redirect(array('proforma/index'));
    }

    public function actionDeleteall()
    {
        $this->borrarTodo();
        Yii::$app->response->redirect(array('proforma/create'));
    }

    private function borrarTodo()
    {
      F_Proforma::setContenidoCompra(array());
      F_Proforma::setFecha(null);
      F_Proforma::setVendedor(null);
      F_Proforma::setCliente(null);
      F_Proforma::setEstadoFactura(null);
      F_Proforma::setVendedornull(null);
      F_Proforma::setObserva(null);
    }

    private function borrarTodoU()
    {
      F_Proforma::setContenidoComprau(array());
      F_Proforma::setFechau(null);
      F_Proforma::setVendedoru(null);
      F_Proforma::setClienteu(null);
      F_Proforma::setEstadoFacturau(null);
      F_Proforma::setCabezaFactura(null);
      F_Proforma::setObserva(null);
    }

    public function actionDeleteclienteU($id)
    {
        $sql = "UPDATE tbl_proforma SET idCliente = '' WHERE idCabeza_Factura = :id";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":id", $id);
                $command->execute();
        //Para borrar cliente
        F_Proforma::setClienteu(null);
        Yii::$app->response->redirect(array('proforma/update','id' => $id));
    }

    //esta funcion me agrega clientes que ya existen para session compra_udate
    public function actionCliente2U(){
        $id = F_Proforma::getCabezaFactura();
        if(Yii::$app->request->isAjax){
            $idCliente = Yii::$app->request->post('idCliente');
            $detalle_venta = DetalleProformas::find()->where(['idCabeza_factura'=>$id])->all();
            if (@$detalle_venta)
            foreach ($detalle_venta as $position => $product){//recorremos cada producto
              $position = DetalleProformas::findOne($product->idDetalleFactura);
              //buscamos el producto en turno en inventario
              $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$product->codProdServicio])->one();
              //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
              if (@$producto_serv) {
                $position->subtotal_iva = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : 0.00;
                $position->save();//guardamos en sessión
              }
            }
            if (@$modelclient = Clientes::find()->where(['idCliente'=>$idCliente])->one()) {
              if ($modelclient->iv == true) {//si es un cliente exento vuelve a recorrer los productos
                if (@$detalle_venta)
                foreach ($detalle_venta as $position => $product){
                  $position = DetalleProformas::findOne($product->idDetalleFactura);
                  $position->subtotal_iva = 0.00;
                  $position->save();
                }
              }
            }
            $sql = "UPDATE tbl_proforma SET idCliente = :idCliente WHERE idCabeza_Factura = :id";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":idCliente", $idCliente);
                $command->bindParam(":id", $id);
                $command->execute();
            F_Proforma::setClienteu((int)$idCliente);
            Yii::$app->response->redirect(array('proforma/update','id' => $id));
            //echo $result ['nombreCompleto'];
        }
    }

    //Esta funcion me agrega clientes nuevos para session compra_update
    public function actionClientenewU(){
      $tribunales = new Tribunales();
        $id = F_Proforma::getCabezaFactura();
        $detalle_venta = DetalleProformas::find()->where(['idCabeza_factura'=>$id])->all();
        if (@$detalle_venta){
          foreach ($detalle_venta as $position => $product){//recorremos cada producto
            $position = DetalleProformas::findOne($product->idDetalleFactura);
            //buscamos el producto en turno en inventario
            $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$product->codProdServicio])->one();
            //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
            if (@$producto_serv) {
              $position->subtotal_iva = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : 0.00;
              $position->save();//guardamos en sessión
            }
          }
        }

        if (Yii::$app->request->post('new-cliente')) {
            $idCliente = Yii::$app->request->post('new-cliente');
            $modelclient = Clientes::find()->where(['identificacion'=>$idCliente])->one();
            if (@$modelclient) {
              //verificamos que el cliente no sea exento de iv
              if ($modelclient->iv == true) {//si es un cliente exento vuelve a recorrer los productos
                if (@$detalle_venta)
                foreach ($detalle_venta as $position => $product){
                  $position = DetalleProformas::findOne($product->idDetalleFactura);
                  $position->subtotal_iva = 0.00;
                  $position->save();
                }
              }
              $idCliente = $modelclient->idCliente;
            } else {
              $nombre_tribunal = $tribunales->obtener_nombre_completo($idCliente);
              $nombre_tribunal_juridico = $tribunales->obtener_nombre_juridico($idCliente);
              if ($nombre_tribunal != 'Sin resultado') {
                $idCliente = $nombre_tribunal;
              } elseif ($nombre_tribunal_juridico != 'Sin resultado') {
                $idCliente = $nombre_tribunal_juridico;
              }
            }
            $sql = "UPDATE tbl_proforma SET idCliente = :idCliente WHERE idCabeza_Factura = :id";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":idCliente", $idCliente);
                $command->bindParam(":id", $id);
                $command->execute();
            F_Proforma::setClienteu($idCliente);
            Yii::$app->response->redirect(array('proforma/update','id' => $id));
        }else {
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ningun cliente, el espacio estaba vacío.');
            Yii::$app->response->redirect(array('proforma/update','id' => $id));
        }

    }

    public function actionRegresarborrarU()
    {
    //Para borrar todo
    $this->borrarTodoU();
    Yii::$app->response->redirect(array('proforma/index'));
    }

    public function actionReport($id) {

        $empresaimagen = new Empresa();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();

        //$id = F_Proforma::getCabezaFactura();
        $content = '
        <div class="panel panel-default">
            <div class="panel-body">
                <table>
                    <tbody>
                    <tr>
                    <td>
                    <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                    </td>
                    <td style="text-align: left;">
                    <strong>'.$empresa->nombre.'</strong><br>
                    <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                    <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                    <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                    <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                    <strong>Dirección:</strong> '.$empresa->email.'
                    </td>
                    <tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Proforma N°: '.$id. '<br>Fecha: '.date('d-m-Y').'</h4>';

        $content .= $this->renderAjax('view',['model' => $this->findModel($id)]);
       /* $content = $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);*/

       $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Proforma</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';

        echo $imprimir;
    }

    //enviar proforma al correo desde una llamada a la funcion
    public function actionEnviar_correo_proforma()
    {
      if(Yii::$app->request->isAjax){
        $id = intval(Yii::$app->request->post('id'));
        $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $model = $this->findModel($id);
            $email = Yii::$app->request->post('email_cliente');
            $bandera = 'verde';
            if (@$model) {
                $cliente = Clientes::find()->where(['idCliente'=>intval($model->idCliente)])->one();
                if (@$cliente) {
                  if ($cliente->email=='') {
                    echo '<center><span class="label label-danger" style="font-size:15pt;">Este cliente no cuenta con una direccion de correo, ingrese una dirección antes de enviar la cotización.</span></center><br>';
                    /*Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente no cuenta con una direccion de correo, ingrese una dirección antes de enviar la cotización.');
                    return $this->redirect(['update', 'id' => $id]);*/
                    $bandera = 'roja';
                  } else {
                    $email = @$email ? $email : $cliente->email;
                  }
                } else {
                  if ($email == '') {
                    echo '<center><span class="label label-danger" style="font-size:15pt;">Se requiere una dirección de correo válida, verifique que la escribió correctamente.</span></center><br>';
                    /*Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Se requiere una dirección de correo válida, verifique que la escribió correctamente.');
                    return $this->redirect(['update', 'id' => $id]);*/
                    $bandera = 'roja';
                  }
                }
                if ($bandera == 'verde') {
                $informacion_proforma = $this->renderAjax('view',['model' => $model]);
                $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
                $content = '
                <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
                <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
                <br>
                <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
                  <tbody><tr>
                    <td align="center" valign="top">
                      <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                        <tbody><tr>
                            <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                              <table>
                                <tbody><tr>
                                  <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                                    &nbsp;&nbsp;&nbsp;&nbsp;

                                  </td>
                                  <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                                  </td>
                                </tr>
                              </tbody></table>
                            </td>
                        </tr>
                        <tr>
                          <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                           <br>
                            <table>
                                <tbody>
                                <tr>
                                <td>
                                <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                                </td>
                                <td style="text-align: left;">
                                <strong>'.$empresa->nombre.'</strong><br>
                                <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                                <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                                <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                                <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                                <strong>Dirección:</strong> '.$empresa->email.'
                                </td>
                                <tr>
                                </tbody>
                            </table>
                            <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">ENVÍO DE COTIZACIÓN # '.$id.'<br><small> '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>

                            '.$informacion_proforma.'


                <br><br><br>

                          </td>
                        </tr>
                        <tr>
                          <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                            Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                          </td>
                        </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody></table>
                <br></div></div><div class="adL">

                </div></div></div> ';
                //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                $subject = "Proforma de ".$empresa->nombre;

                //Enviamos el correo
                Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                ->setTo($email)//para
                ->setFrom($empresa->email_notificaciones)//de
                ->setSubject($subject)
                ->setHtmlBody($content)
                ->send();
                echo '<center><span class="label label-success" style="font-size:15pt;">Cotización enviada correctamente.</span></center><br>';
                }
            } //fin @$model
            //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> La cotización fué enviada correctamente.');
            //$this->redirect(['update', 'id' => $id]);
          }
    }// fin de envio_email

    //me busca los productos en inventario

  //me busca los productos en inventario
    public function actionBusqueda_producto(){
     if(Yii::$app->request->isAjax){
       $consultaBusqueda_cod = Yii::$app->request->post('valorBusqueda_cod');
       $consultaBusqueda_des = Yii::$app->request->post('valorBusqueda_des');
       $consultaBusqueda_cat = Yii::$app->request->post('valorBusqueda_cat');
       $consultaBusqueda_cpr = Yii::$app->request->post('valorBusqueda_cpr');
       $consultaBusqueda_ubi = Yii::$app->request->post('valorBusqueda_ubi');
       $consultaBusqueda_fam = Yii::$app->request->post('valorBusqueda_fam');

       //Filtro anti-XSS
       $caracteres_malos = array("<", ">", "\"", "'", "<", ">", "'");
       $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
       $consultaBusqueda_cod = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cod);
       $consultaBusqueda_des = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_des);
       $consultaBusqueda_cat = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cat);
       $consultaBusqueda_cpr = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cpr);
       //$consultaBusqueda_ubi = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_ubi);

       $mensaje = "";

       //Comprueba si $consultaBusqueda está seteado
       if (isset($consultaBusqueda_cod) && isset($consultaBusqueda_des) && isset($consultaBusqueda_fam) && isset($consultaBusqueda_cat) && isset($consultaBusqueda_cpr) && isset($consultaBusqueda_ubi)) {
           $consultaBusquedalimpia = "%".$consultaBusqueda_cod."%".$consultaBusqueda_des."%".$consultaBusqueda_ubi."%";
           /*$consultaBusquedalimpia_des =
           $consultaBusquedalimpia_ubi = */
           $tipo = 'Producto';
           $consultaBusqueda_cod = '%'.$consultaBusqueda_cod.'%';
           $consultaBusqueda_des = '%'.$consultaBusqueda_des.'%';
           //$consultaBusqueda_fam = '%'.$consultaBusqueda_fam.'%';
           $consultaBusqueda_cat = '%'.$consultaBusqueda_cat.'%';
           $consultaBusqueda_cpr = '%'.$consultaBusqueda_cpr.'%';
           $consultaBusqueda_ubi = '%'.$consultaBusqueda_ubi.'%';
           $sql = "SELECT tps.codProdServicio, tps.codFamilia, tps.nombreProductoServicio, tps.cantidadInventario, tps.ubicacion, tps.precioVentaImpuesto
           FROM tbl_producto_servicios tps INNER JOIN tbl_familia  tf
           ON tps.codFamilia = tf.codFamilia
           WHERE tps.codProdServicio IN "."(";
           $sql .= " SELECT pr.`codProdServicio`
             FROM `tbl_producto_catalogo` pr_c RIGHT JOIN tbl_producto_servicios pr
             ON pr_c.`codProdServicio` = pr.`codProdServicio`
             LEFT JOIN `tbl_producto_proveedores` pr_p
             ON pr.`codProdServicio` = pr_p.codProdServicio
             WHERE pr.tipo = 'Producto'";
             if($consultaBusqueda_cod != '%%'){
               $sql .= " AND pr.codProdServicio LIKE :codProdServicio ";
             }
             if($consultaBusqueda_des != '%%'){
               $sql .= " AND pr.nombreProductoServicio LIKE :nombreProductoServicio ";
             }
             if($consultaBusqueda_ubi != '%%'){
               $sql .= " AND pr.ubicacion LIKE :ubicacion ";
             }
             if($consultaBusqueda_cat != '%%'){
               $sql .= " AND pr_c.`codigo` LIKE :codigo ";
             }/*else{
               $sql .= " AND pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo ";
             }*/
               //$sql .= " AND IF ( :codigo <> '%%', pr_c.`codigo` LIKE :codigo, pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo) ";
             if($consultaBusqueda_cpr != '%%'){
               $sql .= " AND pr_p.`codigo_proveedor` LIKE  :codigo_prov ";
             }
             $sql .= ")";
             if($consultaBusqueda_fam == ''){
               $sql .= " AND tps.codFamilia LIKE '%%' ";
             }else{
               $sql .= " AND tps.codFamilia = :codFamilia ";
             }
                 $sql .= " LIMIT 50";
       $command = \Yii::$app->db->createCommand($sql);
       if($consultaBusqueda_cod != '%%'){ $command->bindParam(":codProdServicio", $consultaBusqueda_cod); }
       if($consultaBusqueda_des != '%%'){ $command->bindParam(":nombreProductoServicio", $consultaBusqueda_des); }
          //$command->bindParam(":codFamilia_", $consultaBusqueda_fam);
       if($consultaBusqueda_fam != '')  { $command->bindParam(":codFamilia", $consultaBusqueda_fam); }
       if($consultaBusqueda_cpr != '%%'){ $command->bindParam(":codigo_prov", $consultaBusqueda_cpr); }
       if($consultaBusqueda_cat != '%%'){$command->bindParam(":codigo", $consultaBusqueda_cat); }
       if($consultaBusqueda_ubi != '%%'){ $command->bindParam(":ubicacion", $consultaBusqueda_ubi); }
       $consulta = $command->queryAll();
                    //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                    //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                        //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                        //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                        //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle
                        /*$consulta = ProductoServicios::find()->where(['=','tipo', $tipo])
                        ->andFilterWhere(['like', 'codProdServicio', $consultaBusqueda_cod ])
                        ->andFilterWhere(['like', 'nombreProductoServicio', $consultaBusqueda_des ])
                        //->andFilterWhere(['like', 'codigo_proveedor', $_ ])//proveedor
                        //->andFilterWhere(['like', 'codigo', $_ ])//catalogo
                        ->andFilterWhere(['like', 'ubicacion', $consultaBusqueda_ubi ])
                        ->orderBy(['nombreProductoServicio' => SORT_DESC])->limit(50)->all();*/
                        $p = 0;
                        foreach($consulta as $resultados){
                            $p += 1;
                            $codProdServicio = $resultados['codProdServicio'];
                            $nombreProductoServicio = $resultados['nombreProductoServicio'];
                            $ubicacion = $resultados['ubicacion'];
                            $cantidadInventario = $resultados['cantidadInventario'];
                            $presiopro = '₡ '.number_format($resultados['precioVentaImpuesto'],2);
                            //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                            $accion = Html::a('', '', [
                                'class' => 'glyphicon glyphicon-plus',
                                'data-datac' => $codProdServicio,
                                'onclick'=>'javascript:agrega_producto($(this).data("datac"))',
                                'data-dismiss'=>'modal',
                                'title' => Yii::t('app', 'Cargar producto'),
                                /*'data' => [
                                    'confirm' => '¿Seguro que quieres agregar?',
                                    'method' => 'post',
                                ],*/
                            ]);
                            //Output
                      $catalogo = '';
                      $codigo_proveedor = '';
                     $marcas_asociadas = ProductoCatalogo::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                     foreach ($marcas_asociadas as $marcas) { $catalogo .= $marcas->codigo.' * ';}
                     $codigos_proveedores = ProductoProveedores::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                     foreach ($codigos_proveedores as $cod_prov) { $codigo_proveedor .= $cod_prov->codigo_proveedor.' * ';}

                     $codpro = "'".$codProdServicio."'";
                     $mensaje .='
                     <tbody tabindex="0" onkeypress="$(document).keypress(function(event){ if(event.which == 13) agrega_producto('.$codpro.') })" >
                                <tr style="cursor:pointer" id="'.$codpro.'" onclick="javascript:agrega_producto('.$codpro.'); this.onclick = null;">
                                  <td id="start" width="150">'.$codProdServicio.'</td>
                                  <td width="450">'.$nombreProductoServicio.'</td>
                                  <td width="100" align="center">'.$cantidadInventario.'</td>
                                  <td width="150" align="center">'.$catalogo.'</td>
                                  <td width="150" align="center">'.$codigo_proveedor.'</td>
                                  <td width="50" align="center">'.$ubicacion.'</td>
                                  <td width="140" align="right">'.$presiopro.'</td>
                                  <td width="60" align="right"></td>
                                  </tr>
                                </tbody>';


                           /* $mensaje .= '
                            <p>
                            <strong>CÓDIGO:</strong> ' . $codProdServicio . '<br>
                            <strong>DESCRIPCIÓN:</strong> ' . $nombreProductoServicio . '<br>
                            <strong>CANT INVENT:</strong> ' . $cantidadInventario . '<br>
                            </p>';*/

                        }



                //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
            }
            echo '<table class="row_selected" >'.$mensaje.'</table>';
        }
    }//fin de funcion de busqueda de productos

        //llena el historial del producto deacuerdo a cada movimiento de ingreso del producto a la ventas
        public function historial_producto($idFa, $tipo)
        {
          //ingresar producto en historial de producto
          $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$idFa])->all();
          foreach ($detalle_venta as $key => $prod) {
            if ($prod->codProdServicio != '0') {
                $inventario = ProductoServicios::find()->where(['codProdServicio'=>$prod->codProdServicio])->one();
                if (@$inventario) {
                $codProdServicio = $inventario->codProdServicio;
                $fecha = date('Y-m-d H:i:s',time());
                $origen = 'Prefactura';
                $id_documento = $prod->idCabeza_factura;
                $cant_mov = $prod->cantidad;
                if ($tipo=='RES') {
                  $cant_ant = $inventario->cantidadInventario + $cant_mov;
                  $cant_new =  $inventario->cantidadInventario;
                } else {
                  $cant_ant = $inventario->cantidadInventario;
                  $cant_new =  $inventario->cantidadInventario + $cant_mov;
                }
                $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
                $usuario = $funcionario->idFuncionario;
                $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
                $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
                $position_h_p->bindParam(":codProdServicio", $codProdServicio);
                $position_h_p->bindParam(":fecha", $fecha);
                $position_h_p->bindParam(":origen", $origen);
                $position_h_p->bindParam(":id_documento", $id_documento);
                $position_h_p->bindValue(":cant_ant", $cant_ant);
                $position_h_p->bindParam(":tipo", $tipo);
                $position_h_p->bindValue(":cant_mov", $cant_mov);
                $position_h_p->bindValue(":cant_new", $cant_new);
                $position_h_p->bindParam(":usuario", $usuario);
                $position_h_p->execute();
              }
            }
          }
        }
}
