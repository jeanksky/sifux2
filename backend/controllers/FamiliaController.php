<?php

namespace backend\controllers;

use Yii;
use backend\models\Familia;
use backend\models\search\FamiliaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ProductoServicios;

use yii\helpers\Html;
//---------------------para la modal
use yii\web\Response;
use common\models\DeletePass; //para confirmar eliminacion de clientes
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;

/**
 * FamiliaController implements the CRUD actions for Familia model.
 */
class FamiliaController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete'],
            'allow' => true,
            'roles' => ['@'],
          ]
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

    /**
     * Lists all Familia models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $searchModel = new FamiliaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/
    public function actionIndex()
    {
        $searchModel = new FamiliaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single Familia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'us'=>$us,
        ]);
    }

    /**
     * Creates a new Familia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Familia();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La familia ha sido registrada correctamente.');
            return $this->redirect(['create', 'model' => $model]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Familia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La familia ha sido actualizada correctamente.');
            return $this->redirect(['update', 'id' => $model->codFamilia]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Familia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
    public function actionDelete()
    { //http://www.yiiframework.com/forum/index.php/topic/48455-solucionadomandar-mensaje-al-intentar-eliminar-un-registro-en-el-grid/

        if(Yii::$app->request->post())
        {

            $codFamilia = Html::encode($_POST["codFamilia"]);

            if((int) $codFamilia)
            {
                //busco donde la familia sea igual a un dato que tenga el producto
                $ps = ProductoServicios::find()->where(['codFamilia'=>$codFamilia])->all();
                //comparo que si no existe que entre a eliminar, de lo contrario me diga que si hay datos asociados y no se puede eliminar
                if(!$ps){
                Familia::deleteAll("codFamilia=:codFamilia", [":codFamilia" => $codFamilia]);
                     if(!isset($_GET['ajax']))  {
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> La Familia se ha eliminado correctamente.');
                    return $this->redirect(['index']);
                }

                }else{
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> Imposible eliminar Familia. Tiene registros asociados.');
                return $this->redirect(['index']);
                  }
            }
            else
            {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> La Familia no se ha podido eliminar.');
                return $this->redirect(['index']);
            }
        }
        else
        {
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Familia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Familia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Familia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
