<?php

namespace backend\controllers;

use Yii;
use backend\models\RecepcionHaciendaGeneral;
use backend\models\search\RecepcionHaciendaGeneralSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Empresa;
use yii\web\UploadedFile;
use backend\models\Xml_file;
use backend\models\Factun;
use backend\models\CompraInventarioSession;
use backend\models\ComprasInventario;
use yii\helpers\Html;
use yii\filters\AccessControl;
use backend\models\MovimientoCredito;
require("mpdf/mpdf.php");
/**
 * RecepcionHaciendaGeneralController implements the CRUD actions for RecepcionHaciendaGeneral model.
 */
class RecepcionHaciendaGeneralController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'only' => ['index','get_file','elimina_recepcion','enviar_xml_not','pdf_resolucion','view', 'renviar_xml_not'],
              'rules' => [
                [
                  'actions' => ['index','get_file','elimina_recepcion','enviar_xml_not','pdf_resolucion','view', 'renviar_xml_not'],
                  'allow' => true,
                  'roles' => ['@'],
                ]
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RecepcionHaciendaGeneral models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecepcionHaciendaGeneralSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGet_file()
    {
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $model = new Xml_file(); $alert = '';
      if ($model->load(Yii::$app->request->post())) {
          $model->archivo = UploadedFile::getInstance($model, 'archivo');
          $save_file = null;
          $file = '';
          if($model->archivo){
            $imagepath = 'uploads/';
            $file = $imagepath.$model->archivo->name;
            $extencion = substr($file, -4);
            if ($extencion == '.xml' || $extencion == '.XML') {
              $save_file = 1;
            } else {
              Yii::$app->getSession()->setFlash('error','<span class="glyphicon glyphicon-remove"></span> Se encontro un problema. Este archivo no es XML, asegúrese de que la exención sea <strong>.xml</strong>');
              Yii::$app->response->redirect(['recepcion-hacienda-general/index']);
            }
          }
          if($save_file == 1){
            $model->archivo->saveAs($file);
            $xml = file_get_contents($file);
            $carga_xml = simplexml_load_string( $xml );
            $content = json_decode(json_encode($carga_xml),TRUE);
            //var_dump($content);
            $clave = $tipo_docu = $moneda = '';
            $emisor = $fecha = $impuesto = $monto = '';
            $ident_emisor = $consecutivo = '';
            $receptor = $ident_receptor = ''; $diasvencidas = null;
            $tiempo_resolucion = '<span class="label label-success">Está a tiempo de aplicar la resolución.</span>';
            //consulta del xml
            foreach ($content as $key => $value) {
              if ($key == 'Clave') {
                $clave = $value;
              }
              if ($key == 'NumeroConsecutivo') {
                $consecutivo = $value;
                $tipo_docu = substr($value, 8, -10);
                CompraInventarioSession::setConsecutivo_electr($consecutivo);
                CompraInventarioSession::setTipo_doc_electr($tipo_docu);
                if ($tipo_docu == '01') { $tipo_docu = 'una Factura electrónica'; }
                elseif($tipo_docu == '02') { $tipo_docu = 'una Nota de débito electrónica'; }
                elseif($tipo_docu == '03') { $tipo_docu = 'una Nota de crédito electrónica'; }
                elseif($tipo_docu == '04') { $tipo_docu = 'un Tiquete Electrónico'; }
              }
              if ($key == 'FechaEmision') {
                $fecha = $value;//sernalynett@hotmail.com
                CompraInventarioSession::setFecha_doc_electr(date('Y-m-d H:i:s', strtotime( $fecha )));
                $mv = new MovimientoCredito();
	        			$dias_despues = $mv->getMovimiento_de_dias(date('Y-m-d H:i:s'), date('Y-m-d H:i:s', strtotime( $fecha )));
                if ($dias_despues > 30) {
                  $tiempo_resolucion = '<span class="label label-danger">Ya venció el tiempo de resolución.</span>';
                }
              }
              if ($key == 'Emisor') {
                $emisor = $value['Nombre'];
                $ident_emisor = $value['Identificacion']['Numero'];
                CompraInventarioSession::setEmisor_recepcion($emisor);
                CompraInventarioSession::setId_emisor_recepcion($ident_emisor);
              }
              if ($key == 'Receptor') {
                $receptor = $value['Nombre'];
                $ident_receptor = @$value['Identificacion']['Numero'];
                if ($ident_receptor == '') {
                  $alert .= ' Este documento no cuenta con tu identificación (Física / Jurídica / DIMEX / NITE)';
                }
              }
              if ($key == 'ResumenFactura') {
                $moneda = $value['CodigoMoneda'];
                $totalventa = @$value['TotalVenta'] ? $value['TotalVenta'] : '0.00';
                $totaldescuento = @$value['TotalDescuentos'] ? $value['TotalDescuentos'] : '0.00';
                $impuesto = @$value['TotalImpuesto'] ? $value['TotalImpuesto'] : '0.00';
                $monto = @$value['TotalComprobante'] ? $value['TotalComprobante'] : '0.00';
                CompraInventarioSession::setSubtotal_recepcion($totalventa);
                CompraInventarioSession::setDescuento_recepcion($totaldescuento);
                CompraInventarioSession::setImpuesto_recepcion($impuesto);
                CompraInventarioSession::setMonto_pagar_recepcion($monto);
              }
              if ($key == 'InformacionReferencia') {
                CompraInventarioSession::setReferencia_recepcion($value['Numero']);
              }
            }
            /*$DOM = new DOMDocument('1.0', 'utf-8');
            $DOM->loadXML($xml);
            $clave = $DOM->getElementsByTagName('clave');*/
            unlink($file);
            if ($alert == '') {
              if ($empresa->ced_juridica == $ident_receptor) {
                $tus_datos = '<h3>Tus datos</h3>
                Receptor: ' . $receptor . '
                <br>ID: ' . $ident_receptor;
              } else {
                $tus_datos = '<div class="well" style="background-color: #F79C88;"><h3>No son tus datos <small>(Si lo envias Hacienda lo va a rechazar)</small></h3>
                Receptor: ' . $receptor . '
                <br>ID: ' . $ident_receptor.'</div>';
              }
              $detalle_recepcion = '<h3><span class="glyphicon glyphicon-ok-sign"></span> Se ha cargado '.$tipo_docu.'<span style="float:right">'.Html::a('', ['elimina_recepcion'], ['class'=>'fa fa-remove', 'data-toggle' => 'titulo', 'title' => 'Cancelar operación' ]).'</span></h3>
              Emitida por: ' . $emisor . '
              <br>ID: ' . $ident_emisor . '
              <br>Consecutivo: '. $consecutivo .'
              <br>Clave: '. $clave .'
              <br>Emitida el: '.date('d-m-Y', strtotime( $fecha )).' a las '.date('h:i:s A', strtotime( $fecha )).' '.$tiempo_resolucion.'
              <br>Moneda: '.$moneda.'
              <br>Impuesto: '.number_format($impuesto,2).'
              <br>Por un monto de: '.number_format($monto,2).'<br>'.$tus_datos;
              CompraInventarioSession::setRecepcion_doc(true);
              CompraInventarioSession::setCarga_xml($xml);
              CompraInventarioSession::setDetalle_recepcion($detalle_recepcion);
              Yii::$app->response->redirect(['recepcion-hacienda-general/index']);
            } else {
              Yii::$app->getSession()
              ->setFlash('error',
              '<span class="glyphicon glyphicon-remove"></span> Se encontro un problema.'. $alert );
              Yii::$app->response->redirect(['recepcion-hacienda-general/index']);
            }
          }
      }
    }

    public function actionElimina_recepcion()
    {
      CompraInventarioSession::setTipo_doc_electr(null);
      CompraInventarioSession::setFecha_doc_electr(null);
      CompraInventarioSession::setConsecutivo_electr(null);
      CompraInventarioSession::setRecepcion_doc(null);
      CompraInventarioSession::setCarga_xml(null);
      CompraInventarioSession::setDetalle_recepcion(null);
      CompraInventarioSession::setSubtotal_recepcion(null);
      CompraInventarioSession::setDescuento_recepcion(null);
      CompraInventarioSession::setImpuesto_recepcion(null);
      CompraInventarioSession::setMonto_pagar_recepcion(null);
      CompraInventarioSession::setEmisor_recepcion(null);
      CompraInventarioSession::setId_emisor_recepcion(null);
      CompraInventarioSession::setReferencia_recepcion(null);
      Yii::$app->response->redirect(['recepcion-hacienda-general/index']);
    }

    //funcion que me permite enviar xml a hacienda para confirmar resolución de compra
    public function actionEnviar_xml_not()
    {
      if(Yii::$app->request->isAjax){
      $resolucion = Yii::$app->request->post('resolucion');
      $detalle = Yii::$app->request->post('detalle');
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $xml = CompraInventarioSession::getCarga_xml();
        $base64_xml = base64_encode($xml);
        $json_recepcion = [
          "resolucion" => $resolucion,
          "detalle" => substr($detalle, 0, 80),
          "xml" => $base64_xml
        ];
        $consultar_factun = new Factun();
        $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Procesar';
        $result_movimiento = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir, $json_recepcion, 'POST'));
        $mensaje = $detalle_mensaje = ''; $clave_recepcion = ''; $fecha_recepcion = ''; $mensajes_error = '';
        foreach ($result_movimiento->data as $key => $value) {
                  if ($key == 'estado_recepcion') { $mensaje = $value; }
                  if ($key == 'clave_recepcion') { $clave_recepcion = $value; }
                  if ($key == 'fecha_resolucion') { $fecha_recepcion = $value; }
                }
        foreach ($result_movimiento->mensajes as $key => $value) {
                  //if ($key == 'mensaje') { $detalle_mensaje = $value; }
                  if ($key!='codigo_mensaje'&&$key!='mensaje'&&$key!='detalle_mensaje') {
                    $mensajes_error .= '<br> > '.$value;
                  }
                }

        if ($result_movimiento->id > 0) {
          if ($mensaje == '01' || $mensaje == '03') {//siempre y cuando el estado sea 03 o 01 se extrae el xml
            $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/XML/'.$clave_recepcion;
            $api_xml_r = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/RespuestaXML/'.$clave_recepcion;
            $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
            $result_xml_r = $consultar_factun->ConeccionFactun_sin_pan($api_xml_r, 'GET');
          } else {
            $result_xml = '';
            $result_xml_r = '';
          }

          $id_factun = $result_movimiento->id;
          $tipo_documento = CompraInventarioSession::getTipo_doc_electr();
          if ($tipo_documento == '01') { $tipo_documento = 'Factura electrónica'; }
          elseif($tipo_documento == '02') { $tipo_documento = 'Nota de débito electrónica'; }
          elseif($tipo_documento == '03') { $tipo_documento = 'Nota de crédito electrónica'; }
          elseif($tipo_documento == '04') { $tipo_documento = 'Tiquete Electrónico'; }
          if ($resolucion == 1) { $resolucion = 'Aceptado'; }
          else if ($resolucion == 2) { $resolucion = 'Parcialmente aceptado'; }
          else { $resolucion = 'Rechazado'; }
          //$fecha_recepcion = date("Y-m-d H:i:s",time());
          $fecha_doc = CompraInventarioSession::getFecha_doc_electr();
          //$mensaje = $mensaje.' '.$detalle_mensaje;
          $consecutivo = CompraInventarioSession::getConsecutivo_electr();
          $nombre_emisor = CompraInventarioSession::getEmisor_recepcion();
          $cedula_emisor = CompraInventarioSession::getId_emisor_recepcion();
          $subtotal = (double)CompraInventarioSession::getSubtotal_recepcion();
          $descuento = (double)CompraInventarioSession::getDescuento_recepcion();
          $impuesto = (double)CompraInventarioSession::getImpuesto_recepcion();
          $total = (double)CompraInventarioSession::getMonto_pagar_recepcion();
          $referencia = CompraInventarioSession::getReferencia_recepcion();
          $usuario_registra = Yii::$app->user->identity->username;
          $sql = "INSERT INTO tbl_recepcion_hacienda
          (reha_factun_id, reha_cedula_emisor, reha_nombre_emisor, reha_tipo_documento, reha_resolucion, reha_detalle, reha_respuesta_hacienda,
          reha_fecha_hora, reha_fecha_hora_documento, reha_clave_recepcion, reha_xml_recepcion, reha_xml_respuesta, reha_consecutivo_electronico,
          reha_subtotal, reha_descuento, reha_impuesto, reha_total, reha_referencia, reha_xml_documento, reha_usuario_registra)
          VALUES (:id_factun, :cedula_emisor, :nombre_emisor, :tipo_documento, :resolucion, :detalle, :respuesta_hacienda, :fecha, :fecha_doc, :clave_recepcion,
          :xml_recepcion, :xml_respuesta, :consecutivo, :subtotal, :descuento, :impuesto, :total, :referencia, :xml_documento, :usuario_registra)";
          $command = \Yii::$app->db->createCommand($sql);
          $command->bindParam(":id_factun", $id_factun);
          $command->bindParam(":cedula_emisor", $cedula_emisor);
          $command->bindParam(":nombre_emisor", $nombre_emisor);
          $command->bindParam(":tipo_documento", $tipo_documento);
          $command->bindParam(":resolucion", $resolucion);
          $command->bindParam(":detalle", $detalle);
          $command->bindParam(":respuesta_hacienda", $mensaje);
          $command->bindParam(":clave_recepcion", $clave_recepcion);
          $command->bindParam(":fecha", $fecha_recepcion);
          $command->bindParam(":fecha_doc", $fecha_doc);
          $command->bindParam(":xml_recepcion", $result_xml);
          $command->bindParam(":xml_respuesta", $result_xml_r);
          $command->bindParam(":consecutivo", $consecutivo);
          $command->bindParam(":subtotal", $subtotal);
          $command->bindParam(":descuento", $descuento);
          $command->bindParam(":impuesto", $impuesto);
          $command->bindParam(":total", $total);
          $command->bindParam(":referencia", $referencia);
          $command->bindParam(":xml_documento", $xml);
          $command->bindParam(":usuario_registra", $usuario_registra);
          //$command2->bindValue(":id", $id);
          if ($command->execute()){
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span><strong>Listo: </strong>Proceso enviado a Hacienda correcamente.');
            Yii::$app->response->redirect(['recepcion-hacienda-general/elimina_recepcion']);
          }
        } else {
          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error:</strong> No se pudo procesar el envío, esto se puede deber a que exista un parámetro equívoco en el XML. <br>'.$mensajes_error);
          Yii::$app->response->redirect(['recepcion-hacienda-general/index']);
        }
      }
    }

    public function actionRenviar_xml_not()
    {
      $recepcion_hacienda_general = RecepcionHaciendaGeneral::find()->where('reha_respuesta_hacienda IN ("NO_ENVIADO", "05")',[])->all();
      foreach ($recepcion_hacienda_general as $key_recep => $recepcion) {
        $consultar_factun = new Factun();
        $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Reenviar/'.$recepcion->reha_clave_recepcion;
        $result_movimiento = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir, 'POST'));
        $mensaje = $fecha_recepcion = '';
        foreach ($result_movimiento->data as $key => $value) {
                  if ($key == 'estado_recepcion') { $mensaje = $value; }
                  if ($key == 'fecha_resolucion') { $fecha_recepcion = $value; }
                }
        $key_recep = RecepcionHaciendaGeneral::find()->where(['reha_clave_recepcion'=>$recepcion->reha_clave_recepcion])->one();
        $key_recep->reha_respuesta_hacienda = $mensaje;
        $key_recep->reha_fecha_hora = $fecha_recepcion;
        $key_recep->save();
      }
      $recepcion_hacienda_general = RecepcionHaciendaGeneral::find()->where('reha_respuesta_hacienda NOT IN ("01", "NO_ENVIADO", "03", "05", "Procesada correctamente ")',[])->all();
      foreach ($recepcion_hacienda_general as $key_recep => $recepcion) {
        $consultar_factun = new Factun();
        $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Consultar/'.$recepcion->reha_clave_recepcion;
        $result_movimiento = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir, 'PUT'));
        $mensaje = $fecha_recepcion = '';
        foreach ($result_movimiento->data as $key => $value) {
                  if ($key == 'estado_hacienda') { $mensaje = $value; }
                }
        $key_recep = RecepcionHaciendaGeneral::find()->where(['reha_clave_recepcion'=>$recepcion->reha_clave_recepcion])->one();
        $key_recep->reha_respuesta_hacienda = $mensaje;
        if ($mensaje == '01' || $mensaje == '03') {
          $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/XML/'.$recepcion->reha_clave_recepcion;
          $api_xml_r = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/RespuestaXML/'.$recepcion->reha_clave_recepcion;
          $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
          $result_xml_r = $consultar_factun->ConeccionFactun_sin_pan($api_xml_r, 'GET');
          $key_recep->reha_xml_recepcion = $result_xml;
          $key_recep->reha_xml_respuesta = $result_xml_r;
        }
        $key_recep->save();
      }
      echo "string";
    }

    //funcion para imprimir el Pdf
    public function actionPdf_resolucion($id)
    {
      $consultar_factun = new Factun();
      $model = RecepcionHaciendaGeneral::findOne($id);
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $tipo_mov = '';
      $num_mov_cons = '';
      if ($model->reha_tipo_documento == 'Nota de crédito electrónica') {
        $tipo_mov = '&nbsp;&nbsp;&nbsp;NCE Aplicada a: ';
        $num_mov_cons = substr($model->reha_referencia, 21, -9);
      }
      if ($model->reha_tipo_documento == 'Nota de débito electrónica') {
        $tipo_mov = '&nbsp;&nbsp;&nbsp;NDE Aplicada a: ';
        $num_mov_cons = substr($model->reha_referencia, 21, -9);
      }
      $pdf_html = '<h3 align="center">COMPROBANTE ELECTRÓNICO DE RECEPCIÓN DE DOCUMENTOS<br>DEL MINISTERIO DE HACIENDA</h3><br>';
      $pdf_html .= '<table>
                      <tr>
                        <td>Clave:</td>
                        <td>'.$model->reha_clave_recepcion.'</td>
                      </tr>
                      <tr>
                        <td>Número Cédula Emisor:</td>
                        <td>'.$model->reha_cedula_emisor.'</td>
                      </tr>
                      <tr>
                        <td>Nombre Emisor:</td>
                        <td>'.$model->reha_nombre_emisor.'</td>
                      </tr>
                      <tr>
                        <td>Fecha y hora Emisión:</td>
                        <td>'.date('d-m-Y h:i:s A', strtotime($model->reha_fecha_hora_documento)).'</td>
                      </tr>
                      <tr>
                        <td>Total Factura:</td>
                        <td>'.number_format($model->reha_total,2).'</td>
                      </tr>
                      <tr>
                        <td><br>Número Cédula Receptor:</td>
                        <td><br>'.$empresa->ced_juridica.'</td>
                      </tr>
                      <tr>
                        <td>Fecha y hora Recepción:</td>
                        <td>'.date('d-m-Y h:i:s A', strtotime($model->reha_fecha_hora)).'</td>
                      </tr>
                    </table>';
      $pdf_html .= '<br><h4>Detalle de confirmación ('.$model->reha_tipo_documento.')</h4>';
      $pdf_html .= '<table>
                      <tr>
                        <td>No. documento:</td>
                        <td>'.$model->reha_consecutivo_electronico.'</td>
                        <td>'.$tipo_mov.'</td>
                        <td>'.$num_mov_cons.'</td>
                      </tr>
                      <tr>
                        <td>Mensaje:</td>
                        <td>'.$model->reha_resolucion.'</td>
                      </tr>
                      <tr>
                        <td>Detalle Mensaje:</td>
                        <td>'.$model->reha_detalle.'</td>
                      </tr>
                    </table><br><br>
                    <p align="center">"Autorizada mediante resolución N° DGT-R-48-2016 del 07-10-2016"</p>';
      $archivo_pdf = 'Recepción Gasto ID. '.$model->reha_id.'.pdf';
      $stylesheet = file_get_contents('css/style_print.css');
      $caja_pdf = new ComprasInventario();
      $caja_pdf->_pdf_show_re($model->reha_id, $stylesheet, $pdf_html, $archivo_pdf);

      //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Se está descargando el PDF.');
      //return $this->redirect(['index']);
    }

    /**
     * Displays a single RecepcionHaciendaGeneral model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($reha_fecha_hora, $reha_nombre_emisor, $reha_consecutivo_electronico)
    {
        return $this->renderAjax('view', [
            'reha_fecha_hora' => $reha_fecha_hora,
            'reha_nombre_emisor'=>$reha_nombre_emisor,
            'reha_consecutivo_electronico'=>$reha_consecutivo_electronico
        ]);
    }

    /**
     * Creates a new RecepcionHaciendaGeneral model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RecepcionHaciendaGeneral();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->reha_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RecepcionHaciendaGeneral model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->reha_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RecepcionHaciendaGeneral model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RecepcionHaciendaGeneral model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RecepcionHaciendaGeneral the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RecepcionHaciendaGeneral::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
