<?php

namespace backend\controllers;

use Yii;
use backend\models\Version;
use backend\models\search\VersionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Empresa;//
use backend\models\Vehiculos;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * VersionController implements the CRUD actions for Version model.
 */
class VersionController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'only' => ['index','view','create','update','delete','enviarnotificacion'],
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete','enviarnotificacion'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all Version models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VersionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Version model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Version model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Version();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La versión ha sido registrado correctamente.');
            return $this->redirect(['create', 'model' => $model]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Version model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La versión ha sido actualizada correctamente.');
            return $this->redirect(['update', 'id' => $model->codVersion]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Version model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $fvehiculo = Vehiculos::find()->where(['version'=>$id])->all();
        if(!$fvehiculo){
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> La versión se ha eliminado correctamente.');
            return $this->redirect(['index']);
        }
        else {
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> Imposible eliminar esta versión. Tiene registros asociados.');
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Version model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Version the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Version::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //esta funcion envia notificaciones del usuario correspondiente a un problema con el sistema
    public function actionEnviarnotificacion(){
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        if(Yii::$app->request->isAjax){
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $usuario_n = Yii::$app->request->post('usuario_n');
            $about = Yii::$app->request->post('about');
            $asunto_n = Yii::$app->request->post('asunto_n');
            $prioridad_n = Yii::$app->request->post('prioridad_n');

            $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
            $content = '
            <!---->

            <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
            <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">

            <br>
            <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
              <tbody><tr>
                <td align="center" valign="top">
                  <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                    <tbody><tr>
                        <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                          <table>
                            <tbody><tr>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                                &nbsp;&nbsp;&nbsp;&nbsp;

                              </td>
                              <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                       <br>
                        <p style="color:#0e4595;font-family: Segoe UI;font-size:22px;line-height:1.5;margin:0 0 25px;text-align:center!important" align="center"><strong>Problema técnico de LUTUX/SIFUX</strong> </p><br>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left"> Estimado(a) funsionario(a) de SOPORTE TÉCNICO DE NELUX, hay una nueva notificación por problema técnico del '.$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y').'.</p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Los siguientes datos son proporcionados por un usuario del sistema:</p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Empresa: <strong>'.$empresa->nombre.' - '.$empresa->cod_empresa.'</strong></p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Telefono de la empresa: <strong>'.$empresa->telefono.'</strong></p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Usuario que notifica: <strong>'.$usuario_n.'</strong></p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Prioridad: <strong>'.$prioridad_n.'</strong></p>
            <p  class="text-justify" style="color:#0e4595;font-family: Segoe UI;font-size:16px;line-height:1.5;margin:0 30px 25px;text-align:justify!important" align="left">Mensaje: <strong>'.$about.'</strong></p>

            <br><br><br><br>

                      </td>
                    </tr>
                  </tbody></table>
                </td>
              </tr>
              <tr>
                <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                  Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
              </tr>
            </tbody></table>
            <br></div></div><div class="adL">

            </div></div></div>


            <!---->
            ';


           /* F_Proforma::setCliente((int)$idCliente);
            Yii::$app->response->redirect(array('proforma/create'));*/

            //Enviamos el correo
            Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
            ->setTo('soporte@neluxps.com')
            //->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
            ->setFrom($empresa->email)
            ->setSubject($asunto_n.' de LUTUX/SIFUX de '.$empresa->nombre.' Prioridad del problema: '.$prioridad_n)
            ->setHtmlBody($content)
            ->send();


            //Vaciar el campo del formulario

                //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Su notificación fue enviada, pronto estaremos en contacto con usted.');
                //return $this->redirect(['site/index']);
                echo 'Mensaje enviado correctamente';

        }
    }
}
