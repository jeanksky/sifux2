<?php

namespace backend\controllers;

use Yii;
use backend\models\Proveedores;
use backend\models\search\ProveedoresSearch;
use backend\models\ContactosPedidos;
use backend\models\ContactosPagos;
use backend\models\CuentasBancariasProveedor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html; //para el nuevo eliminar
//---------------------para la modal
use yii\web\Response;
use common\models\DeletePass; //para confirmar eliminacion de clientes
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;

/**
 * ProveedoresController implements the CRUD actions for Proveedores model.
 */
class ProveedoresController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete',
                'agrega_contacto_pedido', 'elimina_contacto_pedido', 'actualiza_modal_contacto_pedido', 'actualiza_contacto_pedido',
                'agrega_contacto_pagos', 'actualiza_modal_contacto_pago', 'actualiza_contacto_pagos', 'elimina_contacto_pagos',
                'asignar_cuenta_bancaria', 'elimina_cuenta_bancaria',
                'actualiza_modal_cuenta_bancaria', 'actualizar_cuenta_bancaria'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all Proveedores models.
     * @return mixed
     */
   /* public function actionIndex()
    {
        $searchModel = new ProveedoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/
    public function actionIndex()
    {
        $searchModel = new ProveedoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single Proveedores model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'us'=>$us,
        ]);
    }

    /**
     * Creates a new Proveedores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Proveedores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El proveedor ha sido registrado correctamente.');
            return $this->redirect(['create', 'model' => $model]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Proveedores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El proveedor ha sido actualizado correctamente.');
            return $this->redirect(['update', 'id' => $model->codProveedores]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Proveedores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
    public function actionDelete()
    {
        $msg = null;
        if(Yii::$app->request->post())
        {
            $codProveedores = Html::encode($_POST["codProveedores"]);
            if((int) $codProveedores)
            {
                if(Proveedores::deleteAll("codProveedores=:codProveedores", [":codProveedores" => $codProveedores]))
                {
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El proveedor se ha eliminado correctamente.');
                    return $this->redirect(['index']);
                }
                else
                {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> El proveedor no se ha podido eliminar.');
                    return $this->redirect(['index']);
                }
            }
            else
            {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> El proveedor no se ha podido eliminar.');
                return $this->redirect(['index']);
            }
        }
        else
        {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Proveedores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proveedores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proveedores::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //permite agregar un ontacto pedido
    public function actionAgrega_contacto_pedido(){
        $pedido = new ContactosPedidos();
        if(Yii::$app->request->isAjax){
            $codProveedores = Yii::$app->request->post('codProveedores');
            $nombre = strval(Yii::$app->request->post('nombre'));
            $telefono_ofic = strval(Yii::$app->request->post('telefono_ofic'));
            $extension = strval(Yii::$app->request->post('extension'));
            $celular = strval(Yii::$app->request->post('celular'));
            $email = strval(Yii::$app->request->post('email'));

            $pedido->codProveedores = $codProveedores;
            $pedido->nombre = $nombre;
            $pedido->telefono_ofic = $telefono_ofic;
            $pedido->extension = $extension;
            $pedido->celular = $celular;
            $pedido->email = $email;
            $pedido->save();
        }
    }

    //muestra el formulario que me va actualizar de la nota de credito
        public function actionActualiza_modal_contacto_pedido(){
            if(Yii::$app->request->isAjax){
                $position = Yii::$app->request->post('position');
                $contacto_pedido = ContactosPedidos::find()->where(['idContacto_pedido'=>$position])->one();

                        $formulario = '

                            <label>Nombre:</label><br>'.
                            Html::input('text', '', $contacto_pedido->nombre, ['size' => '8', 'class' => 'form-control', 'id' => 'nombre_', 'placeholder' =>' Nombre', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]).
                            '<br><label>Teléfono de Oficina:</label>
                            '.\yii\widgets\MaskedInput::widget([
                                    'name' => 'telefono_ofic_',
                                    'id' => 'telefono_ofic_',
                                    'value' => $contacto_pedido->telefono_ofic,
                                    'mask' => '9999-99-99',
                                ]).'
                            <br><label>Extensión: </label>'.
                              \yii\widgets\MaskedInput::widget([
                                "name" => "extension_",
                                "id" => "extension_",
                                'value' => $contacto_pedido->extension,
                                "mask" => "9999-99-99",
                            ]).'
                              <br><label>Celular: </label>'.
                              \yii\widgets\MaskedInput::widget([
                                'name' => 'celular_',
                                'id' => 'celular_',
                                'value' => $contacto_pedido->celular,
                                'mask' => '9999-99-99',
                            ]).'
                              <br><label>Email: </label>'.
                              \yii\widgets\MaskedInput::widget([
                                'name' => 'email_',
                                'id' => 'email_',
                                'value' => $contacto_pedido->email,
                                'clientOptions' => ['alias' =>  'email'],
                            ]).'
                            <input type="hidden" name="position" id="position_u" value="'.$contacto_pedido->idContacto_pedido.'">';

                        echo $formulario;
                        //ActiveForm::end();//El final del activefrom debe de estar despues del echo para que funcione el submit
                    //}//fin if
                //}
            } else echo 'No tenemos resultados';
        }

    //Actualiza el contacto de pedidos en la BD
    public function actionActualiza_contacto_pedido(){
        if(Yii::$app->request->isAjax){
            $codProveedores = Yii::$app->request->post('codProveedores');
            $nombre = strval(Yii::$app->request->post('nombre'));
            $telefono_ofic = strval(Yii::$app->request->post('telefono_ofic'));
            $extension = strval(Yii::$app->request->post('extension'));
            $celular = strval(Yii::$app->request->post('celular'));
            $email = strval(Yii::$app->request->post('email'));
            $idContacto_pedido = Yii::$app->request->post('idContacto_pedido');

            $sql = "UPDATE tbl_contactos_pedidos SET nombre = :nombre, telefono_ofic = :telefono_ofic, extension = :extension, celular = :celular, email = :email WHERE idContacto_pedido = :idContacto_pedido";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":nombre", $nombre);
                    $command->bindParam(":telefono_ofic", $telefono_ofic);
                    $command->bindParam(":extension", $extension);
                    $command->bindParam(":celular", $celular);
                    $command->bindParam(":email", $email);
                    $command->bindValue(":idContacto_pedido", $idContacto_pedido);
                    $command->execute();
        }
    }
    //elimina un  ontacto de pedido
    public function actionElimina_contacto_pedido(){
        if(Yii::$app->request->isAjax){
            $idContacto_pedido = Yii::$app->request->post('idContacto_pedido');
            \Yii::$app->db->createCommand()->delete('tbl_contactos_pedidos', 'idContacto_pedido = '.(int) $idContacto_pedido)->execute();
        }
    }

    //permite agregar un contacto de pago
    public function actionAgrega_contacto_pagos(){
        $pagos = new ContactosPagos();
        if(Yii::$app->request->isAjax){
            $codProveedores = Yii::$app->request->post('codProveedores');
            $nombre = strval(Yii::$app->request->post('nombre'));
            $telefono_ofic = strval(Yii::$app->request->post('telefono_ofic'));
            $extension = strval(Yii::$app->request->post('extension'));
            $celular = strval(Yii::$app->request->post('celular'));
            $email = strval(Yii::$app->request->post('email'));

            $pagos->codProveedores = $codProveedores;
            $pagos->nombre = $nombre;
            $pagos->telefono_ofic = $telefono_ofic;
            $pagos->extension = $extension;
            $pagos->celular = $celular;
            $pagos->email = $email;
            $pagos->save();
        }
    }

    //muestra el formulario que me va actualizar del contacto de pagos
    public function actionActualiza_modal_contacto_pago(){
        if(Yii::$app->request->isAjax){
            $position = Yii::$app->request->post('position');
            $contacto_pago = ContactosPagos::find()->where(['idContacto_pago'=>$position])->one();

                    $formulario = '

                        <label>Nombre:</label><br>'.
                        Html::input('text', '', $contacto_pago->nombre, ['size' => '8', 'class' => 'form-control', 'id' => '_nombre_', 'placeholder' =>' Nombre', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]).
                        '<br><label>Teléfono de Oficina:</label>
                        '.\yii\widgets\MaskedInput::widget([
                                'name' => '_telefono_ofic_',
                                'id' => '_telefono_ofic_',
                                'value' => $contacto_pago->telefono_ofic,
                                'mask' => '9999-99-99',
                            ]).'
                        <br><label>Extensión: </label>'.
                          \yii\widgets\MaskedInput::widget([
                            "name" => "_extension_",
                            "id" => "_extension_",
                            'value' => $contacto_pago->extension,
                            "mask" => "9999-99-99",
                        ]).'
                          <br><label>Celular: </label>'.
                          \yii\widgets\MaskedInput::widget([
                            'name' => '_celular_',
                            'id' => '_celular_',
                            'value' => $contacto_pago->celular,
                            'mask' => '9999-99-99',
                        ]).'
                          <br><label>Email: </label>'.
                          \yii\widgets\MaskedInput::widget([
                            'name' => '_email_',
                            'id' => '_email_',
                            'value' => $contacto_pago->email,
                            'clientOptions' => ['alias' =>  'email'],
                        ]).'
                        <input type="hidden" name="position" id="_position_u" value="'.$contacto_pago->idContacto_pago.'">';

                    echo $formulario;
                    //ActiveForm::end();//El final del activefrom debe de estar despues del echo para que funcione el submit
                //}//fin if
            //}
        } else echo 'No tenemos resultados';
    }

    //Actualiza el contacto de pedidos en la BD
    public function actionActualiza_contacto_pagos(){
        if(Yii::$app->request->isAjax){
            $codProveedores = Yii::$app->request->post('codProveedores');
            $nombre = strval(Yii::$app->request->post('nombre'));
            $telefono_ofic = strval(Yii::$app->request->post('telefono_ofic'));
            $extension = strval(Yii::$app->request->post('extension'));
            $celular = strval(Yii::$app->request->post('celular'));
            $email = strval(Yii::$app->request->post('email'));
            $idContacto_pago = Yii::$app->request->post('idContacto_pago');

            $sql = "UPDATE tbl_contactos_pagos SET nombre = :nombre, telefono_ofic = :telefono_ofic, extension = :extension, celular = :celular, email = :email WHERE idContacto_pago = :idContacto_pago";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":nombre", $nombre);
                    $command->bindParam(":telefono_ofic", $telefono_ofic);
                    $command->bindParam(":extension", $extension);
                    $command->bindParam(":celular", $celular);
                    $command->bindParam(":email", $email);
                    $command->bindValue(":idContacto_pago", $idContacto_pago);
                    $command->execute();
        }
    }
    //elimina un contacto de pedido
    public function actionElimina_contacto_pagos(){
        if(Yii::$app->request->isAjax){
            $idContacto_pago = Yii::$app->request->post('idContacto_pago');
            \Yii::$app->db->createCommand()->delete('tbl_contactos_pagos', 'idContacto_pago = '.(int) $idContacto_pago)->execute();
        }
    }

    //permite agregar una cuenta bancaria a proveedor
    public function actionAsignar_cuenta_bancaria(){

        $cuenta = new CuentasBancariasProveedor();
        if(Yii::$app->request->isAjax){
            $codProveedores = Yii::$app->request->post('codProveedores');
            $entidadbancaria = strval(Yii::$app->request->post('entidadbancaria'));
            $descripcion = strval(Yii::$app->request->post('descripcion'));
            $cuentab = strval(Yii::$app->request->post('cuentab'));

            $cuenta->CodProveedores = $codProveedores;
            $cuenta->entidad_bancaria = $entidadbancaria;
            $cuenta->descripcion = $descripcion;
            $cuenta->cuenta_bancaria = $cuentab;
            $cuenta->save();
        }
    }

    //muestra el formulario que me va actualizar la cuenta bancaria del proveedor
    public function actionActualiza_modal_cuenta_bancaria(){
        if(Yii::$app->request->isAjax){
            $position = Yii::$app->request->post('position');
            $cuenta = CuentasBancariasProveedor::find()->where(['id'=>$position])->one();

                    $formulario = '

                        <label>Entidad bancaria:</label><br>'.
                        Html::input('text', '', $cuenta->entidad_bancaria, ['size' => '8', 'class' => 'form-control', 'id' => '_entidadbancaria', 'placeholder' =>' Nombre', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]).
                        '<br><label>Descripción:</label>
                        '.Html::input('text', '', $cuenta->descripcion, ['size' => '8', 'class' => 'form-control', 'id' => '_descripcion', 'placeholder' =>' Descripción', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]).
                        '<br><label>Cuenta bancaria: </label>
                        '.Html::input('text', '', $cuenta->cuenta_bancaria, ['size' => '8', 'class' => 'form-control', 'id' => '_cuentab', 'placeholder' =>' Cuenta bancaria', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]).
                        '<input type="hidden" name="position" id="_position_u_" value="'.$cuenta->id.'">';

                    echo $formulario;
                    //ActiveForm::end();//El final del activefrom debe de estar despues del echo para que funcione el submit
                //}//fin if
            //}
        } else echo 'No tenemos resultados';
    }

    //Actualiza el contacto de pedidos en la BD
    public function actionActualizar_cuenta_bancaria(){
        if(Yii::$app->request->isAjax){
            $codProveedores = Yii::$app->request->post('codProveedores');
            $entidadbancaria = strval(Yii::$app->request->post('entidadbancaria'));
            $descripcion = strval(Yii::$app->request->post('descripcion'));
            $cuentab = strval(Yii::$app->request->post('cuentab'));
            $id = Yii::$app->request->post('id');
            $cuenta = CuentasBancariasProveedor::find()->where(['id'=>$id])->one();
            $cuenta->CodProveedores = $codProveedores;
            $cuenta->entidad_bancaria = $entidadbancaria;
            $cuenta->descripcion = $descripcion;
            $cuenta->cuenta_bancaria = $cuentab;
            $cuenta->save();
        }
    }

    //elimina una cuenta bancaria de cproveedor
    public function actionElimina_cuenta_bancaria(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            \Yii::$app->db->createCommand()->delete('tbl_cuentas_bancarias_proveedor', 'id = '.(int) $id)->execute();
        }
    }
}
