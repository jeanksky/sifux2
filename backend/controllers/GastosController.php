<?php

namespace backend\controllers;

use Yii;
use backend\models\Gastos;
use backend\models\Funcionario;
use backend\models\search\GastosSearch;
use backend\models\CuentasBancarias;
use backend\models\Empresa;
use backend\models\EncabezadoCaja;
use backend\models\ProveedoresGastos;
use backend\models\RecepcionHaciendaGastos;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\MovimientoLibros;
use backend\models\ComprasInventario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use backend\models\Notificar_compra;
use backend\models\Factun;
use yii\filters\AccessControl;
require("mpdf/mpdf.php");
/**
 * GastosController implements the CRUD actions for Gastos model.
 */
class GastosController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete','addmedio-u','obtener_numero_cuenta','aplicar_gasto_seleccionado','enviar_xml_not','pdf_resolucion', 'renviar_xml_not'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete','addmedio-u','obtener_numero_cuenta','aplicar_gasto_seleccionado','enviar_xml_not','pdf_resolucion', 'renviar_xml_not'],
            'allow' => true,
            'roles' => ['@'],
          ]
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

    /**
     * Lists all Gastos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GastosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gastos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gastos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {
        $model = new Gastos();

        if ( Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false ) {
            //return $this->redirect(['view', 'id' => $model->idMovimiento_libro]);
            if ($model->validate()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
          $model->subtotal = floatval(str_replace(",", "", $model->subtotal));
          $model->descuento = floatval(str_replace(",", "", $model->descuento));
          $model->impuesto = floatval(str_replace(",", "", $model->impuesto));
          $model->montoGasto = floatval(str_replace(",", "", $model->montoGasto));
          $model->estadoGasto = 'Pendiente';
          $model->fechaRegistro = date('Y-m-d');
          $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
          $model->usuarioRegistra = $funcionario->idFuncionario;
          if ($model->save()) {
            $model->refresh();
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br>Gasto registrado correctamente.');
            //return $this->redirect(['update', 'id' => $model->idBancos]);
            return $this->redirect(['index']);
          }
          else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
          }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Gastos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $submit = false)
    {
        $model = $this->findModel($id);


        if ( Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false ) {
            //return $this->redirect(['view', 'id' => $model->idMovimiento_libro]);
            if ($model->validate()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
          $model->subtotal = floatval(str_replace(",", "", $model->subtotal));
          $model->descuento = floatval(str_replace(",", "", $model->descuento));
          $model->impuesto = floatval(str_replace(",", "", $model->impuesto));
          $model->montoGasto = floatval(str_replace(",", "", $model->montoGasto));
          $model->estadoGasto = 'Pendiente';
          $model->fechaRegistro = date('Y-m-d');
          $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
          $model->usuarioRegistra = $funcionario->idFuncionario;
          if ($model->save()) {
            $model->refresh();
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br>Gasto actualizado correctamente.');
            //return $this->redirect(['update', 'id' => $model->idBancos]);
            return $this->redirect(['index']);
          }
          else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
          }
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->idGastos]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }*/
    }

    /**
     * Deletes an existing Gastos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gastos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gastos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gastos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function actionAddmedioU(){
                if(Yii::$app->request->isAjax){
                    $idEntidad_financiera = Yii::$app->request->post('idEntidad_financiera');
                    $datos = '<option value="">Seleccionar</option>';
                    $datas2 = CuentasBancarias::find()->where(['idEntidad_financiera'=>$idEntidad_financiera])->all();
                    foreach ($datas2 as $key => $cuenta) {
                      $datos .= '<option value="'.$cuenta["idBancos"].'">'.$cuenta["numero_cuenta"] .' - '. $cuenta["descripcion"].'</option>';
                    }
                      echo $datos;
                }
            }// fin actionAddmedioU

            public function actionObtener_numero_cuenta()
            {
              if(Yii::$app->request->isAjax){
               $idBancos = Yii::$app->request->post('idBancos');
               $cuenta = CuentasBancarias::find()->where(['idBancos'=>$idBancos])->one();
               echo $cuenta->numero_cuenta;
             }
            }

            public function actionAplicar_gasto_seleccionado(){
                   if(Yii::$app->request->isAjax){
                    $checkboxValues  = Yii::$app->request->post('checkboxValues');
                    $array = explode(",", $checkboxValues);
                    $longitud = count($array);
                       if ($checkboxValues) {
                             foreach ($array as &$key){//recorro las etiquetas que he seleccionado
                               $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
                               $gasto = Gastos::find()->where(['=','idGastos', $key])->one();
                               $tipo_pago = $gasto->idMedioPagoGastos;
                               $fechaAplicada = date('Y-m-d');
                               $usuarioAplica = $funcionario->idFuncionario;
                               $estadoGasto = 'Aplicado';
                               $idGastos = $key;
                               $sql = "UPDATE tbl_gastos SET fechaAplicada = :fechaAplicada, usuarioAplica = :usuarioAplica, estadoGasto = :estadoGasto
                               WHERE idGastos = :idGastos";
                               $command = \Yii::$app->db->createCommand($sql);
                               $command->bindParam(":fechaAplicada", $fechaAplicada);
                               $command->bindParam(":usuarioAplica", $usuarioAplica);
                               $command->bindParam(":estadoGasto", $estadoGasto);
                               $command->bindValue(":idGastos", $idGastos);
                               if ($command->execute()) {
                                 if ( $tipo_pago == 2 ) {
                                   $fechaPago = date('Y-m-d', strtotime( $gasto->fechaPago ));
                                   $proveedor = ProveedoresGastos::find()->where(['=','idProveedorGastos', $gasto->idProveedorGastos])->one();
                                   $this->crearmovimientolibro($gasto->idCuentaBancariaLocal, $gasto->numeroDocumentoTransferencias, $gasto->montoGasto, $fechaPago, $gasto->detalleGasto, $proveedor->nombreEmpresa);
                                 }
                               }
                             }

                           Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> Se han aplicado los gastos correctamente.');
                           Yii::$app->response->redirect(array('gastos/index'));
                       } else {
                         Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove"></span> <strong>Error!</strong> Disculpa, no se encontró ningún gasto marcado para aplicar.');
                         Yii::$app->response->redirect(array('gastos/index'));
                       }
                   }
               }
            public function crearmovimientolibro($idCuenta_bancaria, $comprobacion, $monto, $fechadepo, $detalleGasto, $nombreEmpresa)
               {
                 //$empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
                 $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
                 // $facturas_caja = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
                 //habro una instancia para obtener el tipo de moneda de la cuenta con la estoy usando
                 // $moneda = Moneda::find()->where(['idTipo_moneda'=>$modelcuentabancaria->idTipo_moneda])->one();
                 //habro una instancia para obtener el tipo de cambio y compararla con la moneda que ya tengo de la cuenta
                 // $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
                 //abrimos una intancia de movimiento libros donde se procese a crear un movimiento automatizado
                 $model_movi_libros = new MovimientoLibros();
                 $model_movi_libros->idCuenta_bancaria = $idCuenta_bancaria;
                 $model_movi_libros->idTipoMoneda = $modelcuentabancaria->idTipo_moneda;
                 $model_movi_libros->idTipodocumento = 1;//transferencia bancaria
                 $model_movi_libros->idMedio_pago = null;
                 $model_movi_libros->numero_documento = (int)$comprobacion;
                 $model_movi_libros->fecha_documento = $fechadepo;//$model_mediopago->fecha_documento;
                 $model_movi_libros->monto = $monto;
                 $model_movi_libros->tasa_cambio = null;
                 $model_movi_libros->monto_moneda_local = 0.00;
                 //con esto compruebo si hay que hacer tipo cambio.
                 /*if ($moneda->monedalocal!='x') {
                     $model_movi_libros->monto = $facturas_caja->total_a_pagar / $tipocambio->valor_tipo_cambio;
                     $model_movi_libros->tasa_cambio = $tipocambio->valor_tipo_cambio;
                     $model_movi_libros->monto_moneda_local = $facturas_caja->total_a_pagar;
                 }*/
                 $model_movi_libros->beneficiario = $nombreEmpresa;
                 $model_movi_libros->detalle = $detalleGasto;
                 $model_movi_libros->fecha_registro = date("Y-m-d");
                 $model_movi_libros->usuario_registra = Yii::$app->user->identity->nombre;
                 //guardamos el movimiento en libros, si esto es asi...
                 if ($model_movi_libros->save()) {
                     //...Debemos modificar el monto de cuentas bancarias correspondiendo al movimiento, en ese caso pago a proveedor
                     ////habrimos instancia para guardar en cuentas bancarias correspondioente al movimiento en libros
                     //$modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
                     //como el tipo de movimiento es debito el saldo en libros de cuenta bancaaria es igual a su misma cantidad menos el monto del nuevo movimiento en debito
                     $modelcuentabancaria->saldo_libros -= $model_movi_libros->monto;

                     //guardamos actualizacion en cuenta bancaria
                     if ($modelcuentabancaria->save()) {//y modificamos el saldo en libros para el control de saldo por movimiento
                         $libros = MovimientoLibros::find()->where(['idMovimiento_libro'=>$model_movi_libros->idMovimiento_libro])->one();
                         $libros->saldo = $modelcuentabancaria->saldo_libros;//porque es hasta aqui que el saldo en libros lleva este monto
                         $libros->save();
                     }
                   }//fin $model_movi_libros->save
               }

   //funcion que me permite enviar xml a hacienda para confirmar resolución de compra
  public function actionEnviar_xml_not()
   {
     $model = new Notificar_compra();
     $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
       if ($model->load(Yii::$app->request->post())) {
         $id = Yii::$app->request->post('idGastos');
         $model->archivo = UploadedFile::getInstance($model, 'archivo');
         $save_file = '';
         $file = '';
         if($model->archivo){
             $imagepath = 'uploads/';
             $file = $imagepath.$model->archivo->name;
             $save_file = 1;
         }
         if($save_file){
                 $model->archivo->saveAs($file);
             }
       }

       $xml = file_get_contents($file);
       $base64_xml = base64_encode($xml);
       unlink($file);
       $json_recepcion = [
         "resolucion" => $model->resolucion,
         "detalle" => substr($model->detalle, 0, 80),
         "xml" => $base64_xml
       ];
       $consultar_factun = new Factun();
       $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Procesar';
       $result_movimiento = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir, $json_recepcion, 'POST'));
       $mensaje = $detalle_mensaje = ''; $clave_recepcion = ''; $fecha_recepcion = ''; $mensajes_error = '';
       foreach ($result_movimiento->data as $key => $value) {
                 if ($key == 'estado_recepcion') { $mensaje = $value; }
                 if ($key == 'clave_recepcion') { $clave_recepcion = $value; }
                 if ($key == 'fecha_resolucion') { $fecha_recepcion = $value; }
               }
       foreach ($result_movimiento->mensajes as $key => $value) {
                 //if ($key == '') { $detalle_mensaje = $value; }
                 if ($key!='codigo_mensaje'&&$key!='mensaje'&&$key!='detalle_mensaje') {
                   $mensajes_error .= '<br> > '.$value;
                 }
               }
       if ($result_movimiento->id > 0) {
         if ($mensaje == '01' || $mensaje == '03') {//siempre y cuando el estado sea 03 o 01 se extrae el xml
           $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/v2/Recepcion/XML/'.$clave_recepcion;
           $api_xml_r = 'https://'.Yii::$app->params['url_api'].'/api/v2/Recepcion/RespuestaXML/'.$clave_recepcion;
           $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
           $result_xml_r = $consultar_factun->ConeccionFactun_sin_pan($api_xml_r, 'GET');
         } else {
           $result_xml = '';
           $result_xml_r = '';
         }
         $idGastos = $id;
         $id_factun = $result_movimiento->id;
         $tipo_documento = $model->tipo_documento;
         $resolucion = $model->resolucion;
         if ($model->resolucion == 1) { $resolucion = 'Aceptado'; }
         else if ($model->resolucion == 2) { $resolucion = 'Parcialmente aceptado'; }
         else { $resolucion = 'Rechazado'; }
         $detalle = $model->detalle;
         //$fecha = date("Y-m-d H:i:s",time());
         //$mensaje = $mensaje.' '.$detalle_mensaje;
         $usuario_registra = Yii::$app->user->identity->username;
         $sql = "INSERT INTO recepcion_hacienda_gastos (idGastos, id_factun, tipo_documento, resolucion, detalle, respuesta_hacienda, fecha, clave_recepcion, xml_recepcion, xml_respuesta, usuario_registra)
                  VALUES (:idGastos, :id_factun, :tipo_documento, :resolucion, :detalle, :respuesta_hacienda, :fecha, :clave_recepcion, :xml_recepcion, :xml_respuesta, :usuario_registra)";
         $command = \Yii::$app->db->createCommand($sql);
         $command->bindParam(":idGastos", $idGastos);
         $command->bindParam(":id_factun", $id_factun);
         $command->bindParam(":tipo_documento", $tipo_documento);
         $command->bindParam(":resolucion", $resolucion);
         $command->bindParam(":detalle", $detalle);
         $command->bindParam(":respuesta_hacienda", $mensaje);
         $command->bindParam(":clave_recepcion", $clave_recepcion);
         $command->bindParam(":fecha", $fecha_recepcion);
         $command->bindParam(":xml_recepcion", $result_xml);
         $command->bindParam(":xml_respuesta", $result_xml_r);
         $command->bindParam(":usuario_registra", $usuario_registra);
         //$command2->bindValue(":id", $id);
         if ($command->execute()){
           Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span><strong>Listo: </strong>Proceso enviado a Hacienda correcamente.');
           Yii::$app->response->redirect(array( 'gastos/index' ));
         }
       } else {
         Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error:</strong> No se pudo procesar el envío, esto se puede deber a que exista un parámetro equívoco en el XML. <br>'.$mensajes_error);
         Yii::$app->response->redirect(array( 'gastos/index' ));
       }

   }

   //para reenviar XML de recepcion por gastos en forma automatizada
   public function actionRenviar_xml_not()
   {
     $recepcion_hacienda_general = RecepcionHaciendaGastos::find()->where('respuesta_hacienda IN ("NO_ENVIADO", "05")',[])->all();
     foreach ($recepcion_hacienda_general as $key_recep => $recepcion) {
       $consultar_factun = new Factun();
       $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Reenviar/'.$recepcion->clave_recepcion;
       $result_movimiento = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir, 'POST'));
       $mensaje = $fecha_recepcion = '';
       foreach ($result_movimiento->data as $key => $value) {
                 if ($key == 'estado_recepcion') { $mensaje = $value; }
                 if ($key == 'fecha_resolucion') { $fecha_recepcion = $value; }
               }
       $key_recep = RecepcionHaciendaGastos::find()->where(['clave_recepcion'=>$recepcion->clave_recepcion])->one();
       $key_recep->respuesta_hacienda = $mensaje;
       $key_recep->fecha = $fecha_recepcion;
       $key_recep->save();
     }
     $recepcion_hacienda_general = RecepcionHaciendaGastos::find()->where('respuesta_hacienda NOT IN ("01", "NO_ENVIADO", "03", "05", "Procesada correctamente ")',[])->all();
     foreach ($recepcion_hacienda_general as $key_recep => $recepcion) {
       $consultar_factun = new Factun();
       $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Consultar/'.$recepcion->clave_recepcion;
       $result_movimiento = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir, 'PUT'));
       $mensaje = $fecha_recepcion = '';
       foreach ($result_movimiento->data as $key => $value) {
                 if ($key == 'estado_hacienda') { $mensaje = $value; }
               }
       $key_recep = RecepcionHaciendaGastos::find()->where(['clave_recepcion'=>$recepcion->clave_recepcion])->one();
       $key_recep->respuesta_hacienda = $mensaje;
       if ($mensaje == '01' || $mensaje == '03') {
         $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/XML/'.$recepcion->clave_recepcion;
         $api_xml_r = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/RespuestaXML/'.$recepcion->clave_recepcion;
         $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
         $result_xml_r = $consultar_factun->ConeccionFactun_sin_pan($api_xml_r, 'GET');
         $key_recep->xml_recepcion = $result_xml;
         $key_recep->xml_respuesta = $result_xml_r;
       }
       $key_recep->save();
     }
     echo "string";
   }

   //funcion para imprimir el Pdf
   public function actionPdf_resolucion($id)
   {
     $consultar_factun = new Factun();
     $model = RecepcionHaciendaGastos::findOne($id);
     $gastos = Gastos::findOne($model->idGastos);
     $proveedor = ProveedoresGastos::findOne($gastos->idProveedorGastos);
     $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
     $tipo_mov = '';
     $num_mov_cons = '';
     if ($model->tipo_documento == 'Nota de crédito electronica') {
       $tipo_mov = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NCE Receptor: ';
       $num_mov_cons = '';
     }
     if ($model->tipo_documento == 'Nota de débito electronica') {
       $tipo_mov = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NDE Receptor: ';
       $num_mov_cons = '';
     }
     $pdf_html = '<h3 align="center">COMPROBANTE ELECTRÓNICO DE RECEPCIÓN DE DOCUMENTOS<br>DEL MINISTERIO DE HACIENDA</h3><br>';
     $pdf_html .= '<table>
                     <tr>
                       <td>Clave:</td>
                       <td>'.$model->clave_recepcion.'</td>
                     </tr>
                     <tr>
                       <td>Número Cédula Emisor:</td>
                       <td>'.$proveedor->identificacion.'</td>
                     </tr>
                     <tr>
                       <td>Fecha Emisor:</td>
                       <td>'.date('d-m-Y h:i:s A', strtotime($model->fecha)).'</td>
                     </tr>
                     <tr>
                       <td>Total Factura:</td>
                       <td>'.number_format($gastos->montoGasto,2).'</td>
                     </tr>
                     <tr>
                       <td>Número Cédula Receptor:</td>
                       <td>'.$empresa->ced_juridica.'</td>
                     </tr>
                   </table>';
     $pdf_html .= '<h4>Detalle de confirmación</h4>';
     $pdf_html .= '<table>
                     <tr>
                       <td>FE Receptor:</td>
                       <td>'.$gastos->numeroDocumento.'</td>
                       <td>'.$tipo_mov.'</td>
                       <td>'.$num_mov_cons.'</td>
                     </tr>
                     <tr>
                       <td>Mensaje:</td>
                       <td>'.$model->resolucion.'</td>
                     </tr>
                     <tr>
                       <td>Detalle Mensaje:</td>
                       <td>'.$model->detalle.'</td>
                     </tr>
                   </table><br><br>
                   <p align="center">"Autorizada mediante resolución N° DGT-R-48-2016 del 07-10-2016"</p>';
     $archivo_pdf = 'Recepción Gasto ID. '.$model->id.'.pdf';
     $stylesheet = file_get_contents('css/style_print.css');
     $caja_pdf = new ComprasInventario();
     $caja_pdf->_pdf_show_re($model->id, $stylesheet, $pdf_html, $archivo_pdf);

     //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Se está descargando el PDF.');
     //return $this->redirect(['index']);
   }
}
