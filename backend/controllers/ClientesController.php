<?php

namespace backend\controllers;

use Yii;
use backend\models\Clientes;
use backend\models\search\ClientesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use backend\models\SignupForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use backend\models\OrdenServicio;
use backend\models\Tribunales;
use backend\models\Ot;
use backend\models\ClientesApartados;
//use common\models\AccessHelpers;
//use common\models\User; //para confirmar eliminacion de cllientes
use common\models\DeletePass; //para confirmar eliminacion de cllientes
use yii\filters\AccessControl;

/**
 * ClientesController implements the CRUD actions for Clientes model.
 */
class ClientesController extends BaseController
{
    private $condition = false;


    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'only' => ['index','view','create','update','delete','restar-stock','clientes-compruebe_repetido', 'clientes-compruebe_repetido_mail', 'clientes-buscar_cliente', 'clientes-obtener_canton', 'clientes-obtener_distrito', 'clientes-obtener_barrio'],
              'rules' => [
                [
                  'actions' => ['index','view','create','update','delete','restar-stock','clientes-compruebe_repetido', 'clientes-compruebe_repetido_mail', 'clientes-buscar_cliente', 'clientes-obtener_canton', 'clientes-obtener_distrito', 'clientes-obtener_barrio'],
                  'allow' => true,
                  'roles' => ['@'],
                ]
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clientes models.
     * @return mixed
     */
  /*  public function actionIndex()
    {
        $searchModel = new ClientesSearch();
        $us = new DeletePass();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);

    }*/
    /* public function actionIndex()
    {
        $searchModel = new ClientesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $this->condition === false ) {
            //return $this->goBack();
            $us->load(Yii::$app->request->post());
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El cliente se ha eliminado correctamente.');
            return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
        } else
        {
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
        }
    }*/

    public function actionIndex()
    {
        $searchModel = new ClientesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
       // $msg = null;//variable si uso
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El cliente se ha eliminado correctamente.');
                return $this->redirect(['index']);
                $us->username = null;
                $us->password = null;
                //return $this->render('index');
            }
            else
            {
                $us->getErrors();
                //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> El cliente NO se ha eliminado.');
                //return $this->render('index');
               // Yii::$app->response->format = Response::FORMAT_JSON;
               // return ActiveForm::validate($us);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single Clientes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'us'=>$us,
        ]);
    }


    /**
     * Creates a new Clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clientes();
        $model_User = new SignupForm();
        $model_ot = new Ot();
        $model_cl_apartado = new ClientesApartados();

        if ($model->load(Yii::$app->request->post())) {
          if ($model->save()) {
            if ($model->email!='') {
              $model_User->signupC();
            }
            if (Yii::$app->params['ot'] == true && $model_ot->load(Yii::$app->request->post())) {
              $model_ot->idCliente = $model->idCliente;
              $model_ot->save();
            }
            if (Yii::$app->params['apartados'] == true && $model_cl_apartado->load(Yii::$app->request->post())) {
              $model_cl_apartado->idCliente = $model->idCliente;
              $model_cl_apartado->save();
            }

            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El cliente ha sido registrado correctamente.');
            return $this->redirect(['create', 'model' => $model]);
          }
        } else {
            return $this->render('create', [
                'model' => $model,
                'model_ot' => $model_ot,
                'model_cl_apartado' => $model_cl_apartado
            ]);
        }
    }

   /* public function actionCreate($submit = false)
        {
            $model = new Clientes();
            $model_User = new SignupForm();

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    $model_User->signup2();
                    $model->refresh();
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return $this->redirect(['index']);
             //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El cliente ha sido registrado correctamente.');

                } else {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        } */

    /**
     * Updates an existing Clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model_ot = Ot::find()->where(['idCliente'=>$id])->one();
        $model_cl_apartado = ClientesApartados::find()->where(['idCliente'=>$id])->one();
        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
          if (Yii::$app->params['ot'] == true && $model_ot->load(Yii::$app->request->post())) {
            if (Yii::$app->params['ot']==true && $model->load(Yii::$app->request->post())) {
              $model_ot->save();
            }
          }
          if (Yii::$app->params['apartados'] == true && $model_cl_apartado->load(Yii::$app->request->post())) {
            $model_cl_apartado->save();
          }
            $correoC = $model->email;
            $nombreC = $model->nombreCompleto;
            $identificacion = $model->identificacion;
            $sql = "UPDATE user SET nombre = :nombreC, email = :correoC WHERE username = :identificacion";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":nombreC", $nombreC);
                $command->bindParam(":correoC", $correoC);
                $command->bindParam(":identificacion", $identificacion);
                $command->execute();
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El cliente ha sido actualizado correctamente.');
            return $this->redirect(['update', 'id' => $model->idCliente]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'model_ot' => $model_ot,
                'model_cl_apartado' => $model_cl_apartado
            ]);
        }
    }

  /*  public function actionUpdate($id, $submit = false)
        {
            $model = $this->findModel($id);

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    $model->refresh();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $this->redirect(['index']);
                   //return [
                   //     'message' => '¡Éxito!',
                   // ];
                } else {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        } */

    /**
     * Deletes an existing Clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   /* public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
   /* public function actionDelete($id)
    {
        $us = new DeletePass();
        $model = $this->findModel($id)->delete();
        if ($us->load(Yii::$app->request->post()) && $us->login()) {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }
        return $this->redirect(['index']);
    }*/

    public function actionDelete()
    {
        $msg = null;
        if(Yii::$app->request->post())
        {
            $idCliente = Html::encode($_POST["idCliente"]);
            if((int) $idCliente)
            {
                $ordenS = OrdenServicio::find()->where(['idCliente'=>$idCliente])->all();
                if(!$ordenS)
                {
                    Clientes::deleteAll("idCliente=:idCliente", [":idCliente" => $idCliente]);
                    Ot::deleteAll("idCliente=:idCliente", [":idCliente" => $idCliente]);
                    ClientesApartados::deleteAll("idCliente=:idCliente", [":idCliente" => $idCliente]);
                    if(!isset($_GET['ajax']))  {
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El cliente se ha eliminado correctamente.');
                    return $this->redirect(['index']);
                    }
                }
                else
                {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> Imposible eliminar el cliente, tiene datos asociados.');
                    return $this->redirect(['index']);
                }

            }
            else
            {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> El cliente no se ha podido eliminar.');
                return $this->redirect(['index']);
            }
        }
        else
        {
            return $this->redirect(['index']);
        }
    }


    /**
     * Finds the Clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clientes::findOne($id)) !== null) {
           // $elimCli = $model->idCliente;
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //funcion que me permite buscar un cliente y obtener sus datos
    public function actionBuscar_cliente()
    {
      $tribunales = new Tribunales();
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('id');
        $entidad = Yii::$app->request->post('entidad');
        if ($entidad == 'Jurídica') {
          $nombreCompleto = $tribunales->obtener_nombre_juridico($id);;
          $genero = '';
        } else {
          $nombreCompleto = $tribunales->obtener_nombre_completo($id);
          $genero = $tribunales->obtener_genero($id);
        }
        $arr = array( 'nombreCompleto'=>$nombreCompleto,
        'genero'=>$genero );
        echo json_encode($arr);
      }

    }

    //funcion para obtener los cantones respectivos de la provincia elegida
    public function actionObtener_canton()
    {
      $tribunales = new Tribunales();
      if(Yii::$app->request->isAjax){
        $provincia = Yii::$app->request->post('provincia');
        $datos = '<option value="">Seleccionar</option>';
        $cantones = $tribunales->obtener_cantones($provincia);
        foreach ($cantones as $key => $canton) {
          $datos .= '<option value="'.$canton["id_canton"].'">'.$canton["nombre_canton"].'</option>';
        }
          echo $datos;
      }
    }

    //funcion para obtener los distrito respectivos del canton elegido
    public function actionObtener_distrito()
    {
      $tribunales = new Tribunales();
      if(Yii::$app->request->isAjax){
        $provincia = Yii::$app->request->post('provincia');
        $canton = Yii::$app->request->post('canton');
        $datos = '<option value="">Seleccionar</option>';
        $distritos = $tribunales->obtener_distritos($provincia, $canton);
        foreach ($distritos as $key => $distrito) {
          $datos .= '<option value="'.$distrito["id_distrito"].'">'.$distrito["nombre_distrito"].'</option>';
        }
          echo $datos;
      }
    }

    //funcion para obtener los barrios respectivos del distrito elegido
    public function actionObtener_barrio()
    {
      $tribunales = new Tribunales();
      if(Yii::$app->request->isAjax){
        $provincia = Yii::$app->request->post('provincia');
        $canton = Yii::$app->request->post('canton');
        $distrito = Yii::$app->request->post('distrito');
        $datos = '<option value="">Seleccionar</option>';
        $barrios = $tribunales->obtener_barrios($provincia, $canton, $distrito);
        foreach ($barrios as $key => $barrio) {
          $datos .= '<option value="'.$barrio["id_barrio"].'">'.$barrio["nombre_barrio"].'</option>';
        }
          echo $datos;
      }
    }

    public function actionRestarStock(){
       if(Yii::app()->user->isGuest)
          return;
       // le queda menos de un segundo de vida de sesion
      $model = new StockEntry();
       $model->reduceStockCausedByUser(Yii::app()->user->id);
       if(!$model->save()){ // ha muerto la sesion en medio de esto.
            throw new Exception("Su sesión ha caducado.");
       }
    }

    //esta funcion me comprueba que la cedula del cliente no sea comprueba_repetido
    public function actionCompruebe_repetido()
    {
      if(Yii::$app->request->isAjax){
        $ident = Yii::$app->request->post('ident');
        $cliente = Clientes::find()->where(['=','identificacion', $ident])->one();
        if (@$cliente) {
          echo "repetido";
        } else {
          echo "no repetido";
        }
      }
    }

    //esta funcion me comprueba que la cedula del cliente no sea comprueba_repetido
    public function actionCompruebe_repetido_mail()
    {
      if(Yii::$app->request->isAjax){
        $email = Yii::$app->request->post('email');
        $cliente = Clientes::find()->where(['=','email', $email])->one();
        if (@$cliente) {
          echo "repetido";
        } else {
          echo "no repetido";
        }
      }
    }

}
