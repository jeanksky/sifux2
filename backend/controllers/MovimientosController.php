<?php

namespace backend\controllers;

use Yii;
use backend\models\TipoMovimientos;
use backend\models\Movimientos;
use backend\models\DetalleMovimientos;
use backend\models\MovimientosSesion;
use backend\models\MovimientosSesionUpdate;
use backend\models\search\MovimientosSearch;
use backend\models\ProductoCatalogo;
use backend\models\ProductoProveedores;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use backend\models\ProductoServicios;
use backend\models\Funcionario;
use yii\helpers\Html;
use backend\models\Empresa;
use yii\filters\AccessControl;

/**
 * MovimientosController implements the CRUD actions for Movimientos model.
 */
class MovimientosController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete',
                'tipo','delete-tipo', 'tipou', 'delete-tipou', 'referencia',
                'delete-referencia', 'referenciau', 'delete-referenciau', 'observaciones',
                'delete-observaciones', 'observacionesu', 'delete-observacionesu', 'cantidad',
                'estado', 'delete-estado', 'delete-estadou', 'additemi', 'additemu',
                'borrar-todo', 'borrar-todou', 'deleteall', 'deleteallu', 'aplicarmovimiento',
                'complete', 'deleteitem', 'deleteitemu', 'report', 'busqueda_producto'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all Movimientos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MovimientosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Movimientos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Movimientos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Movimientos();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El movimiento a inventario ha sido realizado correctamente.');
    //         return $this->redirect(['create', 'model' => $model]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }
    public function actionCreate()
     {
         return $this->render('create');
     }
    /**
     * Updates an existing Movimientos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El movimiento a inventario ha sido aplicado correctamente.');
    //         return $this->redirect(['create', 'model' => $model]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }


    //función para la vista de actualizar

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //Definimos c como null para comprobación
                $c=null;
                //Baseamos el contenido que puede quedar de otro detalle
                MovimientosSesionUpdate::setContenidoMovimiento(null);
                //Abrimos el contenido
                $compra = MovimientosSesionUpdate::getContenidoMovimiento(); /////

                //Me obtiene los datos del detalle del encabezado correspondiente
                $query = new yii\db\Query();
                $data = $query->select(['codProdServicio','cantidad','cantidadinv','cantidadinvant'/*,'observaciones','estado'*/])
                    ->from('tbl_detalle_movimientos')
                    ->where(['=','idMovimiento', $model->idMovimiento])
                    ->distinct()
                    ->all();
                //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
                foreach($data as $row){
                    $codProdServicio = $row['codProdServicio'];
                    $cantidad = $row['cantidad'];
                    $cantidadinv = $row['cantidadinv'];
                    $cantidadinvant = $row['cantidadinvant'];//
                    // $observaciones = $row['observaciones'];
                    // $estado = $row['estado'];
                   if ($codProdServicio!='') {
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        if($c==null)
                        {   //cargo las variables de la tabla
                            $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();

                            $_POST['cod']= $codProdServicio;
                            $_POST['cant']= $cantidad;
                            $_POST['can_ant']=  $cantidadinvant;
                            $_POST['cantinv']= $cantidadinv;
                            $_POST['desc']= $modelProd ? $modelProd->nombreProductoServicio : 'Este producto ya no existe en invetario';
                            $_POST['prec']= $modelProd ? $modelProd->precioVentaImpuesto : 0;
                            // $_POST['obs']= $observaciones;
                            // $_POST['est']= $estado;

                            $compra[] = $_POST;
                            //echo $model->nombreProductoServicio;
                        }
                        //Guardamos el contenido
                        MovimientosSesionUpdate::setContenidoMovimiento($compra);
                    }
                }

            return $this->render('update', [
                'model' => $model,
            ]);
    }



    /**
     * Deletes an existing Movimientos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Movimientos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Movimientos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Movimientos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     //Esta funcion me amacena los datos del tipo en session movimientos
            public function actionTipo(){
                if(Yii::$app->request->isAjax){
                    $idTipo = Yii::$app->request->post('idTipo');
                    MovimientosSesion::setTipo((int)$idTipo);
                    Yii::$app->response->redirect(array('movimientos/create'));
                }
            }

    //Esta funcion me elimina los datos del Tipo almacenados en session movimientos
            public function actionDeleteTipo()
            {
                MovimientosSesion::setTipo(null);//Para borrar Tipo
                Yii::$app->response->redirect(array('movimientos/create'));
            }
//Esta funcion me amacena los datos del tipo en session movimientos update
            public function actionTipou(){
                if(Yii::$app->request->isAjax){
                    $idTipo = Yii::$app->request->post('idTipo');
                    MovimientosSesionUpdate::setTipo((int)$idTipo);

                    $idMo = MovimientosSesionUpdate::getId_movimiento();
                    Yii::$app->response->redirect(array( 'movimientos/update', 'id' => $idMo ));
                    // Yii::$app->response->redirect(array('movimientos/create'));
                }
            }
    //Esta funcion me elimina los datos del Tipo almacenados en session movimientos update
            public function actionDeleteTipou()
            {
                MovimientosSesionUpdate::setTipo(null);//Para borrar Tipo
                 $idMo = MovimientosSesionUpdate::getId_movimiento();
                Yii::$app->response->redirect(array( 'movimientos/update', 'id' => $idMo ));
                // Yii::$app->response->redirect(array('movimientos/create'));
            }

     //Esta funcion me agrega nuevas referencias  para session de movimientos
            public function actionReferencia(){
                if (Yii::$app->request->post('new-referencia')) {
                    $idReferencia = Yii::$app->request->post('new-referencia');
                    MovimientosSesion::setReferencia($idReferencia);
                    Yii::$app->response->redirect(array('movimientos/create'));
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ninguna referencia, el espacio estaba vacío.');
                    Yii::$app->response->redirect(array('movimientos/create'));
                }

            }
    //Esta funcion me elimina los datos de doc.referencia almacenados en session movimientos
         public function actionDeleteReferencia()
            {
                MovimientosSesion::setReferencia(null);//Para borrar Tipo
                Yii::$app->response->redirect(array('movimientos/create'));
            }
//Esta funcion me agrega nuevas referencias  para session de movimientos update
            public function actionReferenciau(){
               $id = MovimientosSesionUpdate::getId_movimiento();
                if (Yii::$app->request->post('new-referencia')) {
                    $referencia = Yii::$app->request->post('new-referencia');
                    $sql = "UPDATE tbl_movimientos SET documentoRef = :referencia WHERE idMovimiento = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":referencia", $referencia);
                        $command->bindParam(":id", $id);
                        $command->execute();
                    MovimientosSesionUpdate::setObservaciones($referencia);
                    Yii::$app->response->redirect(array('movimientos/update','id' => $id));
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ningun documento de referencia, el espacio no debe estar vacío.');
                    Yii::$app->response->redirect(array('movimientos/update','id' => $id));
                }

            }
    //Esta funcion me elimina los datos de doc.referencia almacenados en session movimientos update
         public function actionDeleteReferenciau($id)
            {
              $sql = "UPDATE tbl_movimientos SET documentoRef = '' WHERE idMovimiento = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":id", $id);
                        $command->execute();
                MovimientosSesionUpdate::setReferencia('');//Para borrar vendedor al actualizar
                Yii::$app->response->redirect(array('movimientos/update','id' => $id));
            }

      //Esta funcion me agrega nuevas observaciones  para session de movimientos
            public function actionObservaciones(){
                if (Yii::$app->request->post('new-observaciones')) {
                    $idObservaciones = Yii::$app->request->post('new-observaciones');
                    MovimientosSesion::setObservaciones($idObservaciones);
                    Yii::$app->response->redirect(array('movimientos/create'));
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ninguna observación, el espacio esta vacío.');
                    Yii::$app->response->redirect(array('movimientos/create'));
                }

            }

    //Esta funcion me elimina los datos de observaciones almacenados en session movimientos
            public function actionDeleteObservaciones()
            {
                MovimientosSesion::setObservaciones(null);
                Yii::$app->response->redirect(array('movimientos/create'));
            }

         //Esta funcion me agrega nuevas observaciones  para session de movimientos
            public function actionObservacionesu(){
              $id = MovimientosSesionUpdate::getId_movimiento();
                if (Yii::$app->request->post('new-observaciones')) {
                    $observaciones = Yii::$app->request->post('new-observaciones');
                    $sql = "UPDATE tbl_movimientos SET observaciones = :observaciones WHERE idMovimiento = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":observaciones", $observaciones);
                        $command->bindParam(":id", $id);
                        $command->execute();
                    MovimientosSesionUpdate::setObservaciones($observaciones);
                    Yii::$app->response->redirect(array('movimientos/update','id' => $id));
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ninguna observación, el espacio no debe estar vacío.');
                    Yii::$app->response->redirect(array('movimientos/update','id' => $id));
                }

            }

    //Esta funcion me elimina los datos de observaciones almacenados en session movimientos update
            public function actionDeleteObservacionesu($id)
            {
              $sql = "UPDATE tbl_movimientos SET observaciones = '' WHERE idMovimiento = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":id", $id);
                        $command->execute();
                MovimientosSesionUpdate::setObservaciones('');//Para borrar vendedor al actualizar
                Yii::$app->response->redirect(array('movimientos/update','id' => $id));
            }

    //Esta funcion que agrega nueva Cantidad  para session de movimientos
            public function actionCantidad(){
                if (Yii::$app->request->post('new-cantidad')) {
                    $idCantidad = Yii::$app->request->post('new-cantidad');
                    MovimientosSesion::setCliente($idCantidad);
                    Yii::$app->response->redirect(array('movimientos/create'));
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ninguna cantidad, el espacio estaba vacío.');
                    Yii::$app->response->redirect(array('movimientos/create'));
                }

            }

    //Esta funcion me agrega nuevas estado  para session de movimientos
            public function actionEstado(){
                if (Yii::$app->request->post('estado')) {
                    $estado = Yii::$app->request->post('estado');
                    MovimientosSesion::setEstado($estado);
                    Yii::$app->response->redirect(array('movimientos/create'));
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ningun estado, el espacio esta vacío.');
                    Yii::$app->response->redirect(array('movimientos/create'));
                }

            }

    //Esta funcion me elimina los datos de estado almacenados en session movimientos
            public function actionDeleteEstado()
            {
                MovimientosSesion::setEstado(null);
                Yii::$app->response->redirect(array('movimientos/create'));
            }
    //Esta funcion me elimina los datos de estado almacenados en session movimientos update
            public function actionDeleteEstadou()
            {
                MovimientosSesionUpdate::setEstado(null);//Para borrar Tipo
                Yii::$app->response->redirect(array('movimientos/create'));
            }

           //Esta funcion me agrega los datos del detalle en session compra
            //Agregar datos para la prefactura (create)
            public function actionAdditemi()
            {
                $CabMovi = new Movimientos();
                //Definimos c como null para comprobación
                $c=null;
                //Abrimos el contenido
                $movimientos = MovimientosSesion::getContenidoMovimiento();

                if(Yii::$app->request->isAjax){
                    // $estado = Yii::$app->request->post('estado');
                    // $observaciones = Yii::$app->request->post('observaciones');
                    if (Yii::$app->request->post('cantidad')==null) {
                         Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br>El espacio de cantidad no puede estar vacío');
                            Yii::$app->response->redirect(array('movimientos/create'));
                    }else{
                    $cantidad = Yii::$app->request->post('cantidad');
                    $producto = Yii::$app->request->post('producto');

                    //if(MovimientosSesion::getEstado()=='Aplicado')
                    //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                    $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $producto);
                    $idproducto = $command->queryOne();

                    // $CabMovi->codigoTipo = $codigoTipo;
                    $nuevacantidad = null;
                    $tipo = MovimientosSesion::getTipo();

                    $sql3 = "SELECT acciones FROM tbl_tipo_movimientos WHERE codigoTipo = :codigoTipo";
                    $command3 = \Yii::$app->db->createCommand($sql3);
                    $command3->bindParam(":codigoTipo", $tipo);
                    $idAcciones = $command3->queryOne();


                    if($idAcciones['acciones']=='R'){
                    $nuevacantidad = $idproducto['cantidadInventario'] - $cantidad; //Aqui la alamcenamos la nueva cantidad
                   }else{
                    $nuevacantidad = $idproducto['cantidadInventario'] + $cantidad; //Aqui la alamcenamos la nueva cantidad
                   }
                   if ($nuevacantidad < 0) {
                     Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> La cantidad en inventario no puede quedar menor que 0');
                     Yii::$app->response->redirect(array('movimientos/create'));
                   } else {
                     if (MovimientosSesion::getEstado()=='Aplicado') {
                      //actualizamos en tbl_producto_servicios el la nueva cantidad
                          $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                          $command2 = \Yii::$app->db->createCommand($sql2);
                          $command2->bindParam(":cantidad", $nuevacantidad);
                          $command2->bindParam(":codProdServicio", $producto);
                          $command2->execute();
                         }
                          //Si no existe producto repetido, se asigna lo mandado por POST
                          if($c==null)
                          {
                              $modelp = ProductoServicios::find()->where(['codProdServicio'=>$producto])->one();

                              $_POST['cod']= $modelp->codProdServicio;
                              $_POST['can_ant']= $modelp->cantidadInventario;
                              $_POST['cant']= $cantidad;
                              $_POST['cantinv']= $nuevacantidad;
                              $_POST['desc']= $modelp->nombreProductoServicio;
                              $_POST['prec']= $modelp->precioVentaImpuesto * $cantidad;
                              // $_POST['obs']= $observaciones;
                              // $_POST['est']= $estado;

                              $movimientos[] = $_POST;
                              //echo $model->nombreProductoServicio;
                          }
                          //Guardamos el contenido
                          MovimientosSesion::setContenidoMovimiento($movimientos);

                   }

                    //
                }
            }
                //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> no se obtubo el dato');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                //return $this->render('create', ['time' => date('H:i:s')]);
            }

            // public function actionDeleteitem($id)
            // {
            //     $id = (int) $id;
            //     //Descodificamos el array JSON
            //     $movimiento = JSON::decode(Yii::$app->session->get('movimiento'), true);
            //     //Eliminamos el atributo pasado por parámetro
            //     unset($movimiento[$id]);
            //     //Volvemos a codificar y guardar el contenido
            //     Yii::$app->session->set('movimiento', JSON::encode($movimiento));
            //     Yii::$app->response->redirect(array('movimientos/create'));
            // }

            public function actionAdditemu()
            {
                $detalleMov = new DetalleMovimientos();
                //Definimos c como null para comprobación
                $c=null;
                //Abrimos el contenido
                $compra = MovimientosSesionUpdate::getContenidoMovimiento();

                if(Yii::$app->request->isAjax){
                    $idMovimiento = Yii::$app->request->post('idMovimiento');
                    if (Yii::$app->request->post('cantidad')==null) {
                         Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br>El espacio de cantidad no puede estar vacío');
                            Yii::$app->response->redirect(array('movimientos/update','id' => $idMovimiento));
                    }else{
                    //obtenemos los datos por post
                    $codProdServicio = Yii::$app->request->post('producto');
                    $cantidad = Yii::$app->request->post('cantidad');
                    // $observaciones = Yii::$app->request->post('observaciones');
                    // $estado = Yii::$app->request->post('estado');
                    $nuevacantidad = null;

                        //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                        $sql_ = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProd";
                        $command_ = \Yii::$app->db->createCommand($sql_);
                        $command_->bindParam(":codProd", $codProdServicio);
                        $idproducto = $command_->queryOne();

                        $tipo = MovimientosSesionUpdate::getTipo();

                        $sql3 = "SELECT acciones FROM tbl_tipo_movimientos WHERE codigoTipo = :codigoTipo";
                        $command3 = \Yii::$app->db->createCommand($sql3);
                        $command3->bindParam(":codigoTipo", $tipo);
                        $idAcciones = $command3->queryOne();


                        if($idAcciones['acciones']=='R'){
                            $nuevacantidad = $idproducto['cantidadInventario'] - $cantidad; //Aqui la alamcenamos la nueva cantidad
                        }else{
                            $nuevacantidad = $idproducto['cantidadInventario'] + $cantidad; //Aqui la alamcenamos la nueva cantidad
                        }
                        //$nuevacantidad = $idproducto['cantidadInventario'] - $cantidad; //Aqui la alamcenamos la nueva cantidad

                       if ($nuevacantidad < 0) {
                         Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> La cantidad en inventario no puede quedar menor que 0');
                         Yii::$app->response->redirect(array('movimientos/update', 'id'=>$idMovimiento));
                       } else {

                            //actualizamos en tbl_producto_servicios en la nueva cantidad
                          /*  $sql2_ = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                            $command2_ = \Yii::$app->db->createCommand($sql2_);
                            $command2_->bindParam(":cantidad", $nuevacantidad);
                            $command2_->bindParam(":codProdServicio", $codProdServicio);
                            $command2_->execute();*/

                               //Consultamos si el existe producto para aumentar la cantidad en tbl_detalle_factura
                                $sql = "SELECT * FROM tbl_detalle_movimientos WHERE codProdServicio = :codProdServicio AND idMovimiento = :idMovimiento";
                                $command = \Yii::$app->db->createCommand($sql);
                                $command->bindParam(":codProdServicio", $codProdServicio);
                                $command->bindValue(":idMovimiento", $idMovimiento);
                                $consulta = $command->queryOne();
                                if($compra)
                                if($consulta['codProdServicio']==$codProdServicio && $consulta['idMovimiento']==$idMovimiento)
                                    {
                                           /* $compra[$position]['cantidad']+=$cantidad;
                                           $c=1;*/
                                           //actualizamos en tbl_detalle_movimientos en la nueva cantidad
                                          /* $cantidadnueva = $consulta['cantidad'];//cantidad de tbl_detalle_facturas
                                           $cantidadnueva+=$cantidad;
                                           $sql2 = "UPDATE tbl_detalle_movimientos SET cantidad = :cantidad, cantidadinv = :cantidadinv WHERE codProdServicio = :codProdServicio AND idMovimiento = :idMovimiento";
                                           $command2 = \Yii::$app->db->createCommand($sql2);
                                           $command2->bindParam(":cantidad", $cantidadnueva);
                                           $command2->bindParam(":cantidadinv", $nuevacantidad);
                                           $command2->bindParam(":codProdServicio", $codProdServicio);
                                           $command2->bindValue(":idMovimiento", $idMovimiento);
                                           $command2->execute();*/
                                        $c=1;
                                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Este producto ya está incluido, si desea cambiar el valor eliminelo y vuelva agregarlo.');
                                        Yii::$app->response->redirect(array('movimientos/update','id' => $idMovimiento));
                                    }//fin if $consulta['codProdServicio']==$codProdServicio && $consulta['idMovimiento']==$idMovimiento

                           // }
                            //Si no existe producto repetido, se ingresa normalmente
                            if($c==null)
                            {
                                $model_ = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();

                                $detalleMov->idMovimiento = $idMovimiento;
                                $detalleMov->codProdServicio = $model_->codProdServicio;
                                $detalleMov->cantidad = $cantidad;
                                $detalleMov->cantidadinv = $nuevacantidad;
                                $detalleMov->cantidadinvant = $model_->cantidadInventario;
                                // $detalleMov->observaciones = $observaciones;
                                // $detalleMov->estado = $estado;

                                $detalleMov->save();

                            }//fin if $c==null
                            //Guardamos el contenido
                            MovimientosSesionUpdate::setContenidoMovimiento($compra);
                        }

                    }
                //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> no se obtubo el dato');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                //Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $cabezafactura));
                }//fin consulta ajax
            }

             //Esta funcion me permite borrar todo lo que estan en session compra
    private function borrarTodo()
    {
      MovimientosSesion::setContenidoMovimiento(array());
      MovimientosSesion::setTipo(null);
      MovimientosSesion::setReferencia(null);
      MovimientosSesion::setObservaciones(null);
      MovimientosSesion::setEstado(null);
    }

             //Esta funcion me permite borrar todo lo que estan en session compra
    private function borrarTodou()
    {
      MovimientosSesionUpdate::setContenidoMovimiento(array());
      MovimientosSesionUpdate::setTipo(null);
      MovimientosSesionUpdate::setReferencia(null);
      MovimientosSesionUpdate::setObservaciones(null);
      MovimientosSesionUpdate::setEstado(null);
    }

    public function actionDeleteall()
            {
            //Para borrar todo
            $this->borrarTodo();
            Yii::$app->response->redirect(array('movimientos/create'));
            }


             public function actionDeleteallu()
            {
            //Para borrar todo
            $this->borrarTodou();
            Yii::$app->response->redirect(array('movimientos/index'));
            }

            public function actionAplicarmovimiento(){
                if (($estado = MovimientosSesionUpdate::getEstado()) && ( $idMo = MovimientosSesionUpdate::getId_movimiento()) && ($codigoTipo = MovimientosSesionUpdate::getTipo())) {
                    $nuevoestado = "Aplicado";
                    $sql = "UPDATE tbl_movimientos SET estado = :estado WHERE idMovimiento = :idMovimiento";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":estado", $nuevoestado);
                    $command->bindValue(":idMovimiento", $idMo);
                    $command->execute();

                    $sql2 = "SELECT acciones FROM tbl_tipo_movimientos WHERE codigoTipo = :codigoTipo";
                    $command2 = \Yii::$app->db->createCommand($sql2);
                    $command2->bindParam(":codigoTipo", $codigoTipo);
                    $accion = $command2->queryOne();

                    if ($productos = MovimientosSesionUpdate::getContenidoMovimiento()) {
                        foreach($productos as $position => $product)
                            {
                                $sql3 = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                                $command3 = \Yii::$app->db->createCommand($sql3);
                                $command3->bindParam(":codProdServicio", $product['cod']);
                                $cantidad = $command3->queryOne();

                                $nuevacantidad = $accion['acciones']=='R' ? $cantidad['cantidadInventario'] - $product['cant']: $cantidad['cantidadInventario'] + $product['cant'];

                                $sql4 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidadInventario WHERE codProdServicio = :codProdServicio";
                                $position = \Yii::$app->db->createCommand($sql4);
                                $position->bindParam(":cantidadInventario", $nuevacantidad);
                                $position->bindValue(":codProdServicio", $product['cod']);
                                $position->execute();

                                //-----------------------------
                                $tipo = $accion['acciones']=='R' ? 'RES' : 'SUM' ;
                                $this->historial_producto_in($product['cod'], $idMo, $product['cant'], $tipo);
                                //----------------------------

                                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El movimiento se ha aplicado correctamente.');
                    Yii::$app->response->redirect(array( 'movimientos/update', 'id' => $idMo ));
                            }
                    }


                }
            }

            //llena el historial del producto deacuerdo a cada movimiento de ingreso del producto a la ventas
            public function historial_producto_in($codProdServicio, $id_documento, $cant_mov, $tipo)
            {
              //ingresar producto en historial de producto
                $inventario = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                //$codProdServicio = $inventario->codProdServicio;
                $fecha = date('Y-m-d H:i:s',time());
                $origen = 'Movimiento';
                if ($tipo=='RES') {
                  $cant_ant = $inventario->cantidadInventario + $cant_mov;
                  $cant_new = $inventario->cantidadInventario;
                } else {
                  $cant_ant = $inventario->cantidadInventario - $cant_mov;
                  $cant_new =  $inventario->cantidadInventario;
                }

                $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
                $usuario = $funcionario->idFuncionario;
                $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
                $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
                $position_h_p->bindParam(":codProdServicio", $codProdServicio);
                $position_h_p->bindParam(":fecha", $fecha);
                $position_h_p->bindParam(":origen", $origen);
                $position_h_p->bindParam(":id_documento", $id_documento);
                $position_h_p->bindValue(":cant_ant", $cant_ant);
                $position_h_p->bindParam(":tipo", $tipo);
                $position_h_p->bindValue(":cant_mov", $cant_mov);
                $position_h_p->bindValue(":cant_new", $cant_new);
                $position_h_p->bindParam(":usuario", $usuario);
                $position_h_p->execute();

            }

            public function actionComplete(){
                if(($usuario = MovimientosSesion::getUsuario()) && ($fecha = MovimientosSesion::getFecha()) && ($codigoTipo = MovimientosSesion::getTipo()) && ($documentoRef = MovimientosSesion::getReferencia()) && ($observaciones = MovimientosSesion::getObservaciones()) && ($estado = MovimientosSesion::getEstado()) && ($productos = MovimientosSesion::getContenidoMovimiento()))
                {
                    $CabMovi = new Movimientos();

                    $CabMovi->fecha = date('Y-m-d');
                    $CabMovi->codigoTipo = $codigoTipo;
                    $CabMovi->documentoRef = $documentoRef;
                    $CabMovi->observaciones = $observaciones;
                    $CabMovi->estado = $estado;
                    $CabMovi->usuario = $usuario;
                  //  if ($CabMovi->validate())
                    //{   #guardamos cabecera de movimiento
                        if ($CabMovi->save()) {
                        // guardamos detalle movimiento
                            foreach($productos as $position => $product)
                            {
                                $position = new DetalleMovimientos();
                                $position->idMovimiento = $CabMovi->idMovimiento;
                                $position->codProdServicio = strval($product['cod']);
                                $position->cantidadinvant = $product['can_ant'];//
                                $position->cantidad = $product['cant'];
                                $position->cantidadinv = $product['cantinv'];
                                // $position->observaciones = $product['obs'];
                                // $position->estado = $product['est'];

                                $position->save();
                            }
                            $this->borrarTodo();
                            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El movimiento ha sido almacenado correctamente.');
                            //Yii::$app->response->redirect(array('movimientos/create'));
                            Yii::$app->response->redirect(array('movimientos/update','id' => $CabMovi->idMovimiento));
                        }
                  //  }
                    else {
                        $error='';
                        foreach ($CabMovi->getErrors() as $position => $er)
                        $error.=$er[0].'<br/>';
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>El movimiento no pudo ser registrado.');
                        Yii::$app->response->redirect(array('movimientos/create'));
                    }

                }
                //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Prefactura almacenada correctamente.');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }

             public function actionDeleteitem()
            {
                $cod = $_GET['id0'];
                $id = intval($_GET['id']);
                // $idMovimiento = intval($_GET['id2']);
                $can = intval($_GET['can']);

                //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                    $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $cod);
                    $idproducto = $command->queryOne();

                   // $nuevacantidad = $idproducto['cantidadInventario'] + $can; //Aqui la alamcenamos la nueva cantidad

               /* if ($cod>0) {
                    //Actualizamos la cantidad al producto que no será tomado en cuenta
                    $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                    $command2 = \Yii::$app->db->createCommand($sql2);
                    $command2->bindParam(":cantidad", $nuevacantidad);
                    $command2->bindParam(":codProdServicio", $cod);
                    $command2->execute();
                }*/

                //Descodificamos el array JSON
                $movimiento = JSON::decode(Yii::$app->session->get('movimiento'), true);
                //Eliminamos el atributo pasado por parámetro
                unset($movimiento[$id]);
                //Volvemos a codificar y guardar el contenido
                Yii::$app->session->set('movimiento', JSON::encode($movimiento));
                //Compra_update::setContenidoComprau(null);
                //Eliminamos de la base de datos
                // \Yii::$app->db->createCommand()->delete('tbl_detalle_movimientos', 'idMovimiento = '.$idMovimiento.' AND codProdServicio = '.$cod )->execute();
                //Regresamos a la prefactura
                Yii::$app->response->redirect(array( 'movimientos/create'));
            }


            public function actionDeleteitemu()
            {
                $cod = $_GET['id0'];
                $id = intval($_GET['id']);
                $idMovimiento = intval($_GET['id2']);
                $can = intval($_GET['can']);

                //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                    $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $cod);
                    $idproducto = $command->queryOne();

                    //$nuevacantidad = $idproducto['cantidadInventario'] + $can; //Aqui la alamcenamos la nueva cantidad


              /*  if ($cod>0) {
                    //Actualizamos la cantidad al producto que no será tomado en cuenta
                    $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                    $command2 = \Yii::$app->db->createCommand($sql2);
                    $command2->bindParam(":cantidad", $nuevacantidad);
                    $command2->bindParam(":codProdServicio", $cod);
                    $command2->execute();
                }*/

                //Descodificamos el array JSON
                // $compra = JSON::decode(Yii::$app->session->get('compra'), true);
                //Eliminamos el atributo pasado por parámetro
                // unset($compra[$id]);
                //Volvemos a codificar y guardar el contenido
                // Yii::$app->session->set('compra', JSON::encode($compra));
                //Compra_update::setContenidoComprau(null);
                //Eliminamos de la base de datos
                \Yii::$app->db->createCommand()->delete('tbl_detalle_movimientos', 'idMovimiento = '.$idMovimiento.' AND codProdServicio = "'.$cod.'"' )->execute();
                //Regresamos a la prefactura
                Yii::$app->response->redirect(array( 'movimientos/update', 'id' => $idMovimiento ));
            }

        public function actionReport($id) {

        $empresaimagen = new Empresa();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $movimiento = $this->findModel($id);
        //$id = F_Proforma::getCabezaFactura();
        $content = '
        <div class="panel panel-default">
            <div class="panel-body">
                <table>
                    <tbody>
                    <tr>
                    <td>
                    <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                    </td>
                    <td style="text-align: left;">
                    <strong>'.$empresa->nombre.'</strong><br>
                    <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                    <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                    <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                    <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                    <strong>Dirección:</strong> '.$empresa->email.'
                    </td>
                    <tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Movimiento N°: '.$id. '<br>Fecha: '.$movimiento->fecha.'</h4>';

        $content .= $this->renderAjax('view',['model' => $movimiento]);

        $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Movimiento de inventario</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';

        echo $imprimir;
        }

            //me busca los productos en inventario
        public function actionBusqueda_producto(){
             if(Yii::$app->request->isAjax){
               $consultaBusqueda_cod = Yii::$app->request->post('valorBusqueda_cod');
               $consultaBusqueda_des = Yii::$app->request->post('valorBusqueda_des');
               $consultaBusqueda_cat = Yii::$app->request->post('valorBusqueda_cat');
               $consultaBusqueda_cpr = Yii::$app->request->post('valorBusqueda_cpr');
               $consultaBusqueda_ubi = Yii::$app->request->post('valorBusqueda_ubi');
               $consultaBusqueda_fam = Yii::$app->request->post('valorBusqueda_fam');

               //Filtro anti-XSS
               $caracteres_malos = array("<", ">", "\"", "'", "<", ">", "'");
               $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
               $consultaBusqueda_cod = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cod);
               $consultaBusqueda_des = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_des);
               $consultaBusqueda_cat = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cat);
               $consultaBusqueda_cpr = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cpr);
               //$consultaBusqueda_ubi = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_ubi);

               $mensaje = "";

               //Comprueba si $consultaBusqueda está seteado
               if (isset($consultaBusqueda_cod) && isset($consultaBusqueda_des) && isset($consultaBusqueda_fam) && isset($consultaBusqueda_cat) && isset($consultaBusqueda_cpr) && isset($consultaBusqueda_ubi)) {
                   $consultaBusquedalimpia = "%".$consultaBusqueda_cod."%".$consultaBusqueda_des."%".$consultaBusqueda_ubi."%";
                   /*$consultaBusquedalimpia_des =
                   $consultaBusquedalimpia_ubi = */
                   $tipo = 'Producto';
                   $consultaBusqueda_cod = '%'.$consultaBusqueda_cod.'%';
                   $consultaBusqueda_des = '%'.$consultaBusqueda_des.'%';
                   //$consultaBusqueda_fam = '%'.$consultaBusqueda_fam.'%';
                   $consultaBusqueda_cat = '%'.$consultaBusqueda_cat.'%';
                   $consultaBusqueda_cpr = '%'.$consultaBusqueda_cpr.'%';
                   $consultaBusqueda_ubi = '%'.$consultaBusqueda_ubi.'%';
                   $sql = "SELECT tps.codProdServicio, tps.codFamilia, tps.nombreProductoServicio, tps.cantidadInventario, tps.ubicacion, tps.precioVentaImpuesto
                   FROM tbl_producto_servicios tps INNER JOIN tbl_familia  tf
                   ON tps.codFamilia = tf.codFamilia
                   WHERE tps.codProdServicio IN "."(";
                   $sql .= " SELECT pr.`codProdServicio`
                     FROM `tbl_producto_catalogo` pr_c RIGHT JOIN tbl_producto_servicios pr
                     ON pr_c.`codProdServicio` = pr.`codProdServicio`
                     LEFT JOIN `tbl_producto_proveedores` pr_p
                     ON pr.`codProdServicio` = pr_p.codProdServicio
                     WHERE pr.tipo = 'Producto'";
                     if($consultaBusqueda_cod != '%%'){
                       $sql .= " AND pr.codProdServicio LIKE :codProdServicio ";
                     }
                     if($consultaBusqueda_des != '%%'){
                       $sql .= " AND pr.nombreProductoServicio LIKE :nombreProductoServicio ";
                     }
                     if($consultaBusqueda_ubi != '%%'){
                       $sql .= " AND pr.ubicacion LIKE :ubicacion ";
                     }
                     if($consultaBusqueda_cat != '%%'){
                       $sql .= " AND pr_c.`codigo` LIKE :codigo ";
                     }/*else{
                       $sql .= " AND pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo ";
                     }*/
                       //$sql .= " AND IF ( :codigo <> '%%', pr_c.`codigo` LIKE :codigo, pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo) ";
                     if($consultaBusqueda_cpr != '%%'){
                       $sql .= " AND pr_p.`codigo_proveedor` LIKE  :codigo_prov ";
                     }
                     $sql .= ")";
                     if($consultaBusqueda_fam == ''){
                       $sql .= " AND tps.codFamilia LIKE '%%' ";
                     }else{
                       $sql .= " AND tps.codFamilia = :codFamilia ";
                     }
                         $sql .= " LIMIT 50";
               $command = \Yii::$app->db->createCommand($sql);
               if($consultaBusqueda_cod != '%%'){ $command->bindParam(":codProdServicio", $consultaBusqueda_cod); }
               if($consultaBusqueda_des != '%%'){ $command->bindParam(":nombreProductoServicio", $consultaBusqueda_des); }
                  //$command->bindParam(":codFamilia_", $consultaBusqueda_fam);
               if($consultaBusqueda_fam != '')  { $command->bindParam(":codFamilia", $consultaBusqueda_fam); }
               if($consultaBusqueda_cpr != '%%'){ $command->bindParam(":codigo_prov", $consultaBusqueda_cpr); }
               if($consultaBusqueda_cat != '%%'){$command->bindParam(":codigo", $consultaBusqueda_cat); }
               if($consultaBusqueda_ubi != '%%'){ $command->bindParam(":ubicacion", $consultaBusqueda_ubi); }
               $consulta = $command->queryAll();
                            //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                            //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                                //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                                //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                                //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle


                                foreach($consulta as $resultados){
                                    $codProdServicio = $resultados['codProdServicio'];
                                    $nombreProductoServicio = $resultados['nombreProductoServicio'];
                                    $ubicacion = $resultados['ubicacion'];
                                    $cantidadInventario = $resultados['cantidadInventario'];
                                    $presiopro = '₡ '.number_format($resultados['precioVentaImpuesto'],2);
                                    //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                                    $accion = Html::a('', '', [
                                        'class' => 'glyphicon glyphicon-plus',
                                        'onclick'=>'agrega_productoi("'.$codProdServicio.'")',
                                        'data-dismiss'=>'modal',
                                        'title' => Yii::t('app', 'Cargar producto'),
                                        /*'data' => [
                                            'confirm' => '¿Seguro que quieres agregar?',
                                            'method' => 'post',
                                        ],*/
                                    ]);
                              //Output
                              $catalogo = '';
                              $codigo_proveedor = '';
                             $marcas_asociadas = ProductoCatalogo::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                             foreach ($marcas_asociadas as $marcas) { $catalogo .= $marcas->codigo.' * ';}
                             $codigos_proveedores = ProductoProveedores::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                             foreach ($codigos_proveedores as $cod_prov) { $codigo_proveedor .= $cod_prov->codigo_proveedor.' * ';}

                             $codpro = "'".$codProdServicio."'";
                             $mensaje .='
                             <tbody tabindex="0" onkeypress="$(document).keypress(function(event){ if(event.which == 13) agrega_productoi('.$codpro.') })" >
                                        <tr style="cursor:pointer" id="'.$codpro.'" onclick="javascript:agrega_productoi('.$codpro.'); this.onclick = null;">
                                          <td id="start" width="150">'.$codProdServicio.'</td>
                                          <td width="450">'.$nombreProductoServicio.'</td>
                                          <td width="100" align="center">'.$cantidadInventario.'</td>
                                          <td width="150" align="center">'.$catalogo.'</td>
                                          <td width="150" align="center">'.$codigo_proveedor.'</td>
                                          <td width="50" align="center">'.$ubicacion.'</td>
                                          <td width="140" align="right">'.$presiopro.'</td>
                                          <td width="60" align="right"></td>
                                          </tr>
                                        </tbody>

                                         <div class="table-pagination pull-right">
                                         </div>
                                         ';


                                   /* $mensaje .= '
                                    <p>
                                    <strong>CÓDIGO:</strong> ' . $codProdServicio . '<br>
                                    <strong>DESCRIPCIÓN:</strong> ' . $nombreProductoServicio . '<br>
                                    <strong>CANT INVENT:</strong> ' . $cantidadInventario . '<br>
                                    </p>';*/

                                }



                        //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
                    }
                    echo $mensaje;
                }
            }


}
