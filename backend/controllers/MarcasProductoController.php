<?php

namespace backend\controllers;

use Yii;
use backend\models\MarcasProducto;
use backend\models\ProductoCatalogo;
use backend\models\search\MarcasProductoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MarcasProductoController implements the CRUD actions for MarcasProducto model.
 */
class MarcasProductoController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete','actualiza_marca'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete','actualiza_marca'],
            'allow' => true,
            'roles' => ['@'],
          ]
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

    /**
     * Lists all MarcasProducto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MarcasProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MarcasProducto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MarcasProducto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MarcasProducto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MarcasProducto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_marcas]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MarcasProducto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $catalogo = ProductoCatalogo::find()->where(['id_marcas'=>$id])->one();
        if (@$catalogo) {
          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ban-circle"></span> <strong>Error!</strong><br><br> La marca no se eliminó porque se encuentra asociado a un producto.');
        } else {
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> La marca ha sido eliminado correctamente.');
          $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the MarcasProducto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MarcasProducto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MarcasProducto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionActualiza_marca($value, $id, $colum)
    {
      $model = $this->findModel($id);
      if ($colum=='nombre_marca') {
        $model->nombre_marca = $value;
      }else {
        $model->estado = $value;
      }
      if ($model->save()) {
        return '<br><center><div class="alert alert-success">Actualizado correctamente!!</div></center>';
      }else {
        return '<br><center><div class="alert alert-danger">Error, se encuentran incongruencias!!</div></center>';
      }
    }
}
