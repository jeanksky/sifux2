<?php

namespace backend\controllers;

use Yii;
use backend\models\ProductoServicios;
use backend\models\DetalleOrdenCompra;
use backend\models\DetalleOrdenCompraTemporal;
use backend\models\search\ProductoServiciosSearch;
use backend\models\Proveedores;
use backend\models\OrdenCompraProv;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use backend\models\ProductoProveedores;
use backend\models\ProductoCatalogo;
use yii\helpers\Html;
use yii\helpers\Url;
//---------------------para la modal
use yii\web\Response;
use common\models\DeletePass; //para confirmar eliminacion de clientes
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;

/**
 * ProductoServiciosController implements the CRUD actions for ProductoServicios model.
 */
class ProductoServiciosController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete',
                'index2', 'asignar_proveedor', 'elimina_proveedor', 'actualiza_modal_proveedor',
                'actualiza_proveedor', 'historial_producto', 'asignar_marca', 'elimina_marca',
                'actualiza_modal_marca','actualiza_marca', 'busqueda_producto'
              ],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all ProductoServicios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductoServiciosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    public function actionIndex2()
    {

        $searchModel = new ProductoServiciosSearch();
        //$searchModel  = \Yii::createObject(ProductoServiciosSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider = $searchModel->search($_GET);

        return $this->renderAjax('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //me busca los productos en inventario
    public function actionBusqueda_producto(){
     if(Yii::$app->request->isAjax){
                $consultaBusqueda_cod = Yii::$app->request->post('valorBusqueda_cod');
                $consultaBusqueda_des = Yii::$app->request->post('valorBusqueda_des');
                $consultaBusqueda_cat = Yii::$app->request->post('valorBusqueda_cat');
                $consultaBusqueda_cpr = Yii::$app->request->post('valorBusqueda_cpr');
                $consultaBusqueda_ubi = Yii::$app->request->post('valorBusqueda_ubi');
                $consultaBusqueda_fam = Yii::$app->request->post('valorBusqueda_fam');

                //Filtro anti-XSS
                $caracteres_malos = array("<", ">", "\"", "'", "<", ">", "'");
                $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
                $consultaBusqueda_cod = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cod);
                $consultaBusqueda_des = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_des);
                $consultaBusqueda_cat = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cat);
                $consultaBusqueda_cpr = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cpr);
                //$consultaBusqueda_ubi = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_ubi);

                $mensaje = "";

                //Comprueba si $consultaBusqueda está seteado
                if (isset($consultaBusqueda_cod) && isset($consultaBusqueda_des) && isset($consultaBusqueda_fam) && isset($consultaBusqueda_cat) && isset($consultaBusqueda_cpr) && isset($consultaBusqueda_ubi)) {
                    $consultaBusquedalimpia = "%".$consultaBusqueda_cod."%".$consultaBusqueda_des."%".$consultaBusqueda_ubi."%";
                    /*$consultaBusquedalimpia_des =
                    $consultaBusquedalimpia_ubi = */
                    $tipo = 'Producto';
                    $consultaBusqueda_cod = '%'.$consultaBusqueda_cod.'%';
                    $consultaBusqueda_des = '%'.$consultaBusqueda_des.'%';
                    //$consultaBusqueda_fam = '%'.$consultaBusqueda_fam.'%';
                    $consultaBusqueda_cat = '%'.$consultaBusqueda_cat.'%';
                    $consultaBusqueda_cpr = '%'.$consultaBusqueda_cpr.'%';
                    $consultaBusqueda_ubi = '%'.$consultaBusqueda_ubi.'%';
                    $sql = "SELECT tps.codProdServicio, tps.codFamilia, tps.nombreProductoServicio, tps.cantidadInventario, tps.ubicacion, tps.precioVentaImpuesto
                    FROM tbl_producto_servicios tps INNER JOIN tbl_familia  tf
                    ON tps.codFamilia = tf.codFamilia
                    WHERE tps.codProdServicio IN "."(";
                    $sql .= " SELECT pr.`codProdServicio`
                      FROM `tbl_producto_catalogo` pr_c RIGHT JOIN tbl_producto_servicios pr
                      ON pr_c.`codProdServicio` = pr.`codProdServicio`
                      LEFT JOIN `tbl_producto_proveedores` pr_p
                      ON pr.`codProdServicio` = pr_p.codProdServicio
                      WHERE pr.tipo = 'Producto'";
                      if($consultaBusqueda_cod != '%%'){
                        $sql .= " AND pr.codProdServicio LIKE :codProdServicio ";
                      }
                      if($consultaBusqueda_des != '%%'){
                        $sql .= " AND pr.nombreProductoServicio LIKE :nombreProductoServicio ";
                      }
                      if($consultaBusqueda_ubi != '%%'){
                        $sql .= " AND pr.ubicacion LIKE :ubicacion ";
                      }
                      if($consultaBusqueda_cat != '%%'){
                        $sql .= " AND pr_c.`codigo` LIKE :codigo ";
                      }/*else{
                        $sql .= " AND pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo ";
                      }*/
                        //$sql .= " AND IF ( :codigo <> '%%', pr_c.`codigo` LIKE :codigo, pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo) ";
                      if($consultaBusqueda_cpr != '%%'){
                        $sql .= " AND pr_p.`codigo_proveedor` LIKE  :codigo_prov ";
                      }
                      $sql .= ")";
                      if($consultaBusqueda_fam == ''){
                        $sql .= " AND tps.codFamilia LIKE '%%' ";
                      }else{
                        $sql .= " AND tps.codFamilia = :codFamilia ";
                      }
                          $sql .= " LIMIT 50";
                $command = \Yii::$app->db->createCommand($sql);
                if($consultaBusqueda_cod != '%%'){ $command->bindParam(":codProdServicio", $consultaBusqueda_cod); }
                if($consultaBusqueda_des != '%%'){ $command->bindParam(":nombreProductoServicio", $consultaBusqueda_des); }
                   //$command->bindParam(":codFamilia_", $consultaBusqueda_fam);
                if($consultaBusqueda_fam != '')  { $command->bindParam(":codFamilia", $consultaBusqueda_fam); }
                if($consultaBusqueda_cpr != '%%'){ $command->bindParam(":codigo_prov", $consultaBusqueda_cpr); }
                if($consultaBusqueda_cat != '%%'){$command->bindParam(":codigo", $consultaBusqueda_cat); }
                if($consultaBusqueda_ubi != '%%'){ $command->bindParam(":ubicacion", $consultaBusqueda_ubi); }
                $consulta = $command->queryAll();
                        $p = 0;
                        foreach($consulta as $resultados){
                            $p += 1;
                            $codProdServicio = $resultados['codProdServicio'];
                            $nombreProductoServicio = $resultados['nombreProductoServicio'];
                            $ubicacion = $resultados['ubicacion'];
                            $cantidadInventario = $resultados['cantidadInventario'];
                            $presiopro = '₡ '.number_format($resultados['precioVentaImpuesto'],2);
                            //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                            $accion = Html::a('<span class="fa fa-eye"></span>', ['view', 'id'=>$resultados['codProdServicio']], ['class' => ''])
                            .' '.Html::a('<span class="fa fa-pencil"></span>', ['update', 'id'=>$resultados['codProdServicio']], ['class' => ''])
                            .' '. Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                'id' => 'activity-index-link-delete',
                                'class' => 'activity-delete-link',
                                'title' => Yii::t('app', 'Eliminar Producto '.$nombreProductoServicio),
                                'data-toggle' => 'modal',
                                'data-target' => '#modalDelete',
                                'onclick' => 'javascript:enviaResultado("'.$codProdServicio.'")',
                                'href' => Url::to('#'),
                                'data-pjax' => '0',
                                ]);
                            //Output
                      $catalogo = '';
                      $codigo_proveedor = '';
                     $marcas_asociadas = ProductoCatalogo::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                     foreach ($marcas_asociadas as $marcas) { $catalogo .= $marcas->codigo.' * ';}
                     $codigos_proveedores = ProductoProveedores::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                     foreach ($codigos_proveedores as $cod_prov) {//recorro lo productos con proveedores para ver su estado
                       if ($cod_prov->estado == 'Pedido') {//se eligen los productos pedidos
                         $proveedor = Proveedores::findOne($cod_prov->codProveedores);//se les obtiene el proveedor y la orden de compra
                         $orden_compra = OrdenCompraProv::find()->where(['estado'=>'Generada'])->andWhere('idProveedor = :idProveedor',[':idProveedor'=>$cod_prov->codProveedores])->one();
                         if (@$orden_compra) {//si presenta un pedido informa en la consulta que el producto esta pedido por si este no se encuentra en el inventario
                           $f_pedido = date('d-m-Y', strtotime( $orden_compra->fecha_registro ));
                           $codigo_proveedor .= '<a href="#" title="Este producto ya fue pedido al proveedor '.$proveedor->nombreEmpresa.' el día '.$f_pedido.'.">'.$cod_prov->codigo_proveedor.' * </a>';
                         } else {//si el producto se encuentra e estado pedido pero no esta asociado a ninguna compra se informa que este producto debe ser reportado
                           $codigo_proveedor .= '<a href="#" style="color:#ffa800;" title="Reporte este código, porque se encuentra en un estado PEDIDO pero no esta asociado a ninguna orden de compra.">'.$cod_prov->codigo_proveedor.' * </a>';;
                         }

                       } else {
                         $codigo_proveedor .= $cod_prov->codigo_proveedor.' * ';
                       }
                     }
                     $codpro = "'".$codProdServicio."'";
                     $mensaje .='
                     <tbody tabindex="0" onkeypress="$(document).keypress(function(event){ if(event.which == 13) agrega_producto('.$codpro.') })" >
                                <tr style="cursor:pointer" id="'.$codpro.'" onclick="javascript:agrega_producto('.$codpro.'); this.onclick = null;">
                                  <td id="start" width="150">'.$codProdServicio.'</td>
                                  <td width="450">'.$nombreProductoServicio.'</td>
                                  <td width="100" align="center">'.$cantidadInventario.'</td>
                                  <td width="150" align="center">'.$catalogo.'</td>
                                  <td width="150" align="center">'.$codigo_proveedor.'</td>
                                  <td width="50" align="center">'.$ubicacion.'</td>
                                  <td width="140" align="right">'.$presiopro.'</td>
                                  <td width="60" align="right">'.$accion.'</td>
                                  </tr>
                                </tbody>';
                        }
            }
            echo '<table class="table table-hover" style="width:100%;">'.$mensaje.'</table>';
        }
    }//fin de funcion de busqueda de productos

    /**
     * Displays a single ProductoServicios model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      $us = new DeletePass();
      if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
      {
          Yii::$app->response->format = Response::FORMAT_JSON;
          return ActiveForm::validate($us);
      }
      if ($us->load(Yii::$app->request->post())) {
          if ($us->validate())
          {
              $us->username = null;
              $us->password = null;
          }
          else
          {
              $us->getErrors();
          }
      }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'us'=>$us,
        ]);
    }

    public function actionHistorial_producto($cod_prod, $usuario, $origen, $fecha_desde, $fecha_hasta)
    {
        $model = $this->findModel($cod_prod);
        $usuario = $usuario == '' ? NULL : $usuario;
        $fecha_desde = @$fecha_desde ? date('Y-m-d 00:00:01', strtotime( $fecha_desde )) : date('Y-m-d 00:00:00');
        $fecha_hasta = @$fecha_hasta ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : date('Y-m-d 23:59:59');
        return $this->renderAjax('historial_producto', [
          'model' => $model,
          'usuario'=>$usuario,
          'fecha_desde' => $fecha_desde,
          'origen'=>$origen == '' ? ['Compra', 'Prefactura', 'Venta', 'Movimiento', 'Devolución'] : [$origen],
          'fecha_hasta' => $fecha_hasta
        ]);
    }

    /*public function actionHistorial_producto($value='')
    {
      // code...
    }*/

    /**
     * Creates a new ProductoServicios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductoServicios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El producto ha sido registrado correctamente.');
            return $this->redirect(['create', 'model' => $model]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Updates an existing ProductoServicios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El producto ha sido actualizado correctamente.');
            return $this->redirect(['update', 'id' => $model->codProdServicio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionAsignar_proveedor()
    {
      $prod_pro = new ProductoProveedores();
      if(Yii::$app->request->isAjax){
        $codProdServicio = Yii::$app->request->post('codProdServicio');
        $codProveedores = Yii::$app->request->post('codProveedores');
        $codigo_proveedor = Yii::$app->request->post('codigo_proveedor');
        $prod_prov = ProductoProveedores::find()->where(['codProdServicio'=>$codProdServicio])->andWhere('codProveedores = :codProveedores',[':codProveedores' => $codProveedores ])->one();
        if (@$prod_prov) {
          echo "repetido";
        } else {
          $prod_pro->codProdServicio = $codProdServicio;
          $prod_pro->codProveedores = $codProveedores;
          $prod_pro->codigo_proveedor = $codigo_proveedor;
          $prod_pro->estado = 'Disponible';
          $prod_pro->save();
          echo "no repetido";
        }
      }
    }

    //elimina un proveedor del producto
    public function actionElimina_proveedor(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $det_ord_com = DetalleOrdenCompra::find()->where(['idProd_prov'=>$id])->one();
            $det_ord_com_tem = DetalleOrdenCompraTemporal::find()->where(['idProd_prov'=>$id])->one();
            if (@$det_ord_com || @$det_ord_com_tem) {
              echo "asignado";
            } else {
              \Yii::$app->db->createCommand()->delete('tbl_producto_proveedores', 'idProd_prov = '.(int) $id)->execute();
              echo "no asignado";
          }
        }
    }

    //debuelve datos a la modal de actualizacion de proveedor
    public function actionActualiza_modal_proveedor()
    {
      if(Yii::$app->request->isAjax){
          $idProd_prov = Yii::$app->request->post('idProd_prov');
          $prod_prov = ProductoProveedores::find()->where(['idProd_prov'=>$idProd_prov])->one();
          $arr = array('idProd_prov'=>$prod_prov->idProd_prov, 'codProveedores'=>$prod_prov->codProveedores, 'codigo_proveedor'=>$prod_prov->codigo_proveedor);
          echo json_encode($arr);
      }
    }

    //actualiza los datos modificados en la modal
    public function actionActualiza_proveedor()
    {
      if(Yii::$app->request->isAjax){
          $idProd_prov = Yii::$app->request->post('idProd_prov');
          $codProveedores = Yii::$app->request->post('codProveedores');
          $codigo_proveedor = Yii::$app->request->post('codigo_proveedor');
          $prod_prov = ProductoProveedores::find()->where(['idProd_prov'=>$idProd_prov])->one();
          if ($prod_prov->codProveedores == $codProveedores) {
            $prod_prov->codProveedores = $codProveedores;
            $prod_prov->codigo_proveedor = $codigo_proveedor;
            $prod_prov->save();
            echo "no repetido";
          } else {
            echo "repetido";
          }

        }
    }

    public function actionAsignar_marca()
    {
      $prod_pro = new ProductoCatalogo();
      if(Yii::$app->request->isAjax){
        $codProdServicio = Yii::$app->request->post('codProdServicio');
        $id_marcas = Yii::$app->request->post('id_marcas');
        $codigo_marca = Yii::$app->request->post('codigo_marca');
        $prod_prov = ProductoCatalogo::find()->where(['codProdServicio'=>$codProdServicio])->andWhere('codigo = :codigo',[':codigo' => $codigo_marca ])->one();
        if (@$prod_prov) {
          echo "repetido";
        } else {
          $prod_pro->codProdServicio = $codProdServicio;
          $prod_pro->id_marcas = $id_marcas;
          $prod_pro->codigo = $codigo_marca;
          $prod_pro->save();
          echo "no repetido";
        }
      }
    }

    //elimina una marca al producto
    public function actionElimina_marca(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            //$det_ord_com = DetalleOrdenCompra::find()->where(['idProd_prov'=>$id])->one();
            //$det_ord_com_tem = DetalleOrdenCompraTemporal::find()->where(['idProd_prov'=>$id])->one();
            //if (@$det_ord_com || @$det_ord_com_tem) {
            //  echo "asignado";
            //} else {
              \Yii::$app->db->createCommand()->delete('tbl_producto_catalogo', 'id_Prod_marca = '.(int) $id)->execute();
              echo "no asignado";
          //}
        }
    }

    //debuelve datos a la modal de actualizacion de proveedor
    public function actionActualiza_modal_marca()
    {
      if(Yii::$app->request->isAjax){
          $id_Prod_marca = Yii::$app->request->post('id_Prod_marca');
          $prod_cat = ProductoCatalogo::find()->where(['id_Prod_marca'=>$id_Prod_marca])->one();
          $arr = array('id_Prod_marca'=>$prod_cat->id_Prod_marca, 'id_marcas'=>$prod_cat->id_marcas, 'codigo'=>$prod_cat->codigo);
          echo json_encode($arr);
      }
    }

    //actualiza los datos modificados en la modal
    public function actionActualiza_marca()
    {
      if(Yii::$app->request->isAjax){
          $id_Prod_marca = Yii::$app->request->post('id_Prod_marca');
          $id_marcas = Yii::$app->request->post('id_marcas');
          $codigo = Yii::$app->request->post('codigo');
          $prod_cat = ProductoCatalogo::find()->where(['id_Prod_marca'=>$id_Prod_marca])->one();
          if ($prod_cat->id_marcas == $id_marcas) {
            $prod_cat->id_marcas = $id_marcas;
            $prod_cat->codigo = $codigo;
            $prod_cat->save();
            echo "no repetido";
          } else {
            echo "repetido";
          }

        }
    }
    /**
     * Deletes an existing ProductoServicios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
      if(Yii::$app->request->post())
      {
          $id = Html::encode($_POST["codProdServicio"]);
          if($id)
          {
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El producto se ha eliminado correctamente.');
            return $this->redirect(['index']);
          } else {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El producto no fue eliminado.'.$id);
            return $this->redirect(['index']);
          }
      }
    }

    /**
     * Finds the ProductoServicios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductoServicios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductoServicios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
