<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Usuarios;
use backend\models\Clientes;
use backend\models\Proveedores;

date_default_timezone_set('America/Costa_Rica');
class ReporteController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','rbancos_mostrar','rcaja_mostrar','rcompras_mostrar','rinventaio_mostrar',
                'rproveedores_mostrar','rusuarios_mostrar','rorden_servicio_mostrar',
                'rbancos_m_movimiento_libros','ibancos_m_movimiento_libros',
                'ibancos_m_cuentas_bancarias','rcaja_m_venta_detalle','icaja_m_venta_detalle', 'rcaja_m_desechadas','icaja_m_desechadas',
                'rcaja_m_venta_balance_caja','icaja_m_venta_balance_caja','rcaja_m_venta_gra_ex',
                'icaja_m_venta_gra_ex','rcaja_m_venta_cuen_cobrar','icaja_m_venta_cuen_cobrar','rcaja_m_venta_abonos',
                'icaja_m_venta_abonos','rcaja_m_venta_ndebito','icaja_m_venta_ndebito',
                'rcaja_m_venta_ncredito','icaja_m_venta_ncredito','rcaja_m_d151',
                'icaja_m_d151','rcaja_m_pagos_x_caja','icaja_m_pagos_x_caja','rcaja_m_resumen_comisiones','icaja_m_resumen_comisiones','rcaja_m_detalle_comisiones','icaja_m_detalle_comisiones',
                'rcompras_m_orden_compra_x_proveedor','icompras_m_orden_compra_x_proveedor','rcompras_m_resumen_compras',
                'icompras_m_resumen_compras','rcompras_m_compras_gra_ex','icompras_m_compras_gra_ex',
                'rcompras_m_detalle_compras','icompras_m_detalle_compras',
                'iinventario_m_costo','rinventario_m_costo_categoria','iinventario_m_costo_categoria',
                'rinventario_m_x_categoria','iinventario_m_x_categoria','rinventario_m_x_debajo_minimo',
                'iinventario_m_x_debajo_minimo','rprovee_m_resumen_cuenta_pagar','iprovee_m_resumen_cuenta_pagar',
                'rprovee_m_notas_debito','iprovee_m_notas_debito','rprovee_m_notas_credito',
                'iprovee_m_notas_credito','rprovee_m_d151','iprovee_m_d151',
                'rfuncionario_m_resu_ventas','ifuncionario_m_resu_ventas','rfuncionario_m_resu_notas_credito',
                'ifuncionario_m_resu_notas_credito','iorden_servicio_m_pendiente','rorden_servicio_m_resumen_meca',
                'iorden_servicio_m_resumen_meca','rorden_servicio_m_detalle_meca','iorden_servicio_m_detalle_meca',
                'icaja_m_ot_resumen', 'rcaja_m_ot_detalle', 'icaja_m_ot_detalle' , 'rprovee_m_tramites' ,'iprovee_m_tramites',
                'rinventario_m_producto_no_vendido','iinventario_m_producto_no_vendido','rcaja_m_d151_detalle','icaja_m_d151_detalle',
                'rprovee_m_d151_detalle','iprovee_m_d151_detalle', 'rcaja_m_apartado_cancelado', 'icaja_m_apartado_cancelado',
                'rcaja_m_apartado_lista', 'icaja_m_apartado_lista', 'rcaja_m_recepcion_hacienda_ge', 'icaja_m_recepcion_hacienda_ge',
                'cargar-cliente-periodo','cargar-proveedor-periodo','rcaja_m_d151_detalle_export'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

  public function actionIndex()
  {
    return $this->render('index', [
    ]);
  }

  //=========================REPORTE X MODULO=======================

  //MODULO BANCOS
  public function actionRbancos_mostrar($tipo)
  {
    return $this->renderAjax('rbancos_mostrar', [
      'tipo' => $tipo,
    ]);
  }

  //MODULO CAJA
  public function actionRcaja_mostrar($tipo)
  {
    return $this->renderAjax('rcaja_mostrar', [
      'tipo' => $tipo,
    ]);
  }

  //MODULO COMPRAS
  public function actionRcompras_mostrar($tipo)
  {
    return $this->renderAjax('rcompras_mostrar', [
      'tipo' => $tipo,
    ]);
  }

  //MODULO INVENTARIO
  public function actionRinventaio_mostrar()
  {
    if(Yii::$app->request->isAjax){
      $tipo = Yii::$app->request->post('tipo');
      echo $this->renderAjax('rinventaio_mostrar', [
        'tipo' => $tipo
      ]);
    }
  }

  //MODULO PROVEEDORES
  public function actionRproveedores_mostrar($tipo)
  {
    return $this->renderAjax('rproveedores_mostrar', [
      'tipo' => $tipo,
    ]);
  }

  //MODULO DE USUARIOS
  public function actionRusuarios_mostrar($tipo)
  {
    return $this->renderAjax('rusuarios_mostrar', [
      'tipo' => $tipo,
    ]);
  }

  //MODULO DE ORDEN DE SERVICIO
  public function actionRorden_servicio_mostrar($tipo)
  {
    return $this->renderAjax('rorden_servicio_mostrar', [
      'tipo' => $tipo,
    ]);
  }

  //=========================REPORTE BANCOS=======================
  public function actionRbancos_m_movimiento_libros()
  {
    $idBancos  = $_GET['idBancos'];
    $fecha_desde2  = $_GET['fecha_desde2'];
    $fecha_hasta2  = $_GET['fecha_hasta2'];
    return $this->renderAjax('rbancos_m_movimiento_libros', [
      'idBancos' => $idBancos, 'fecha_desde2' => $fecha_desde2, 'fecha_hasta2' => $fecha_hasta2
    ]);
  }

  public function actionIbancos_m_movimiento_libros()
  {
    $idBancos  = $_GET['idBancos'];
    $fecha_desde2  = $_GET['fecha_desde2'];
    $fecha_hasta2  = $_GET['fecha_hasta2'];
    return $this->renderAjax('ibancos_m_movimiento_libros', [
      'idBancos' => $idBancos, 'fecha_desde2' => $fecha_desde2, 'fecha_hasta2' => $fecha_hasta2
    ]);
  }

  //ibancos_m_cuentas_bancarias
  public function actionIbancos_m_cuentas_bancarias()
  {
    return $this->renderAjax('ibancos_m_cuentas_bancarias', []);
  }//fin de la funcion

  //=========================REPORTE CAJA=======================
  //Ventas detalle
  public function actionRcaja_m_venta_detalle()
  {
    $fecha_desde = $_GET['fecha_desde'];
    $fecha_hasta = $_GET['fecha_hasta'];
    return $this->renderAjax('rcaja_m_venta_detalle', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  public function actionIcaja_m_venta_detalle()
  {
    $fecha_desde = $_GET['fecha_desde'];
    $fecha_hasta = $_GET['fecha_hasta'];
    return $this->renderAjax('icaja_m_venta_detalle', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  //Resumen balance de caja
  public function actionRcaja_m_venta_balance_caja($fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('rcaja_m_venta_balance_caja', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  public function actionIcaja_m_venta_balance_caja($fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('icaja_m_venta_balance_caja', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  //Resumen ventas Gravadas / Exentas
  public function actionRcaja_m_venta_gra_ex($fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('rcaja_m_venta_gra_ex', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  public function actionIcaja_m_venta_gra_ex($fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('icaja_m_venta_gra_ex', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }
  //Cuenta por cobrar
  public function actionRcaja_m_venta_cuen_cobrar($lista_clientes, $facturas_vencidas)
  {
    return $this->renderAjax('rcaja_m_venta_cuen_cobrar', ['lista_clientes'=>$lista_clientes, 'facturas_vencidas'=>$facturas_vencidas]);
  }
  public function actionIcaja_m_venta_cuen_cobrar($lista_clientes, $facturas_vencidas)
  {
    return $this->renderAjax('icaja_m_venta_cuen_cobrar', ['lista_clientes'=>$lista_clientes, 'facturas_vencidas'=>$facturas_vencidas]);
  }
  //ABONOS
  public function actionRcaja_m_venta_abonos($lista_clientes, $fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('rcaja_m_venta_abonos', [
      'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  public function actionIcaja_m_venta_abonos($lista_clientes, $fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('icaja_m_venta_abonos', [
      'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  //NOTAS DE debito
  public function actionRcaja_m_venta_ndebito($lista_clientes, $fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('rcaja_m_venta_ndebito', [
      'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  public function actionIcaja_m_venta_ndebito($lista_clientes, $fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('icaja_m_venta_ndebito', [
      'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  //NOTAS DE CREDITO
  public function actionRcaja_m_venta_ncredito($lista_clientes, $fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('rcaja_m_venta_ncredito', [
      'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  public function actionIcaja_m_venta_ncredito($lista_clientes, $fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('icaja_m_venta_ncredito', [
      'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  //REPORTES DE FACTURAS RECHASADAS
  public function actionRcaja_m_desechadas($fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('rcaja_m_desechadas', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }
  public function actionIcaja_m_desechadas($fecha_desde, $fecha_hasta)
  {
    return $this->renderAjax('icaja_m_desechadas', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
    ]);
  }

  //REPORTES DE FACTURAS OT DETALLE
  public function actionIcaja_m_ot_resumen()
  {
    return $this->renderAjax('icaja_m_ot_resumen', [ ]);
  }

  //REPORTES DE FACTURAS OT DETALLE
  public function actionRcaja_m_ot_detalle($fecha_desde, $fecha_hasta,$lista_cliente_ot)
  {
    return $this->renderAjax('rcaja_m_ot_detalle', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'lista_cliente_ot'=>$lista_cliente_ot
    ]);
  }
  public function actionIcaja_m_ot_detalle($fecha_desde, $fecha_hasta,$lista_cliente_ot)
  {
    return $this->renderAjax('icaja_m_ot_detalle', [
      'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'lista_cliente_ot'=>$lista_cliente_ot
    ]);
  }

  //Reporte D151
  public function actionRcaja_m_d151($anoSeleccionado)
  {
    return $this->renderAjax('rcaja_m_d151', [
      'anoSeleccionado' => $anoSeleccionado
    ]);
  }

  public function actionIcaja_m_d151($anoSeleccionado)
  {
    return $this->renderAjax('icaja_m_d151', [
      'anoSeleccionado' => $anoSeleccionado
    ]);
  }

  //Reporte D151 Detalle
  public function actionRcaja_m_d151_detalle($anoSeleccionado,$lista_clientes)
  {
    return $this->renderAjax('rcaja_m_d151_detalle', [
      'anoSeleccionado' => $anoSeleccionado, 'lista_clientes'=>$lista_clientes
    ]);
  }

  public function actionIcaja_m_d151_detalle($anoSeleccionado,$lista_clientes)
  {
    return $this->renderAjax('icaja_m_d151_detalle', [
      'anoSeleccionado' => $anoSeleccionado, 'lista_clientes'=>$lista_clientes
    ]);
  }

  public function actionRcaja_m_d151_detalle_export($anoSeleccionado,$lista_clientes,$tipo_exportacion)
  {
    if ($anoSeleccionado != '0'){
      $anoConsultar_desde = $anoSeleccionado - 1;
    }
    $anoPasado = date('Y', strtotime('-1 year'));
    $anoActual = $anoPasado + 1;

    $fechaFactura1 = $anoSeleccionado != '0' ? $anoConsultar_desde.'-10-01' : $anoPasado.'-10-01';
    $fechaFactura2 = $anoSeleccionado != '0' ? $anoSeleccionado.'-09-30' : $anoActual.'-09-30';
    if($lista_clientes != '0'){
      $clientes = Clientes::find()->where('idCliente IN ('.$lista_clientes.')',[])->all();

      $filename = 'excel' . ".xls";
      header("Content-Type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=\"$filename\"");
      foreach ($clientes as $key => $cliente) {
        /*si se le pasa el valor de idCliente directo en el create comman produce un error*/
        $idCliente = $cliente['idCliente'];
        /*buscamos el monto total para validar que sea mayor o igual al parametro de monto minimo de MIN*/
        $sql = "SELECT SUM(subtotal-porc_descuento) AS sumD151 FROM `tbl_encabezado_factura`
        WHERE idCliente = :cliente AND idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
          (SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC'
          GROUP BY idCabeza_Factura) fact_not
          ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
          WHERE fact.total_a_pagar = fact_not.suma_nc)
          AND fecha_inicio BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura !='Anulada'  GROUP BY idCliente DESC;";

          $command = \Yii::$app->db->createCommand($sql);
          $command->bindParam(":fechaFactura1", $fechaFactura1);
          $command->bindParam(":fechaFactura2", $fechaFactura2);
          $command->bindParam(":cliente", $idCliente);
          $montoD151 = $command->queryOne();
          /*al hacer un queryOne nos trae un array simple*/
          if($montoD151['sumD151'] >= Yii::$app->params['limite_monto_venta_MIN']){/*Limite impuesto por el MIN en venta de articulos. ¢2,500,000*/
            $sql = "SELECT idCabeza_Factura AS n_interno, fe AS n_fe, subtotal, porc_descuento AS descuento,
            iva AS impuesto, total_a_pagar AS total, idCliente, fecha_inicio FROM `tbl_encabezado_factura`
            WHERE idCliente = :cliente AND idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
              (SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC'
              GROUP BY idCabeza_Factura) fact_not
              ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
              WHERE fact.total_a_pagar = fact_not.suma_nc)
              AND fecha_inicio BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura !='Anulada'";

              $command = \Yii::$app->db->createCommand($sql);
              $command->bindParam(":fechaFactura1", $fechaFactura1);
              $command->bindParam(":fechaFactura2", $fechaFactura2);
              $command->bindParam(":cliente", $idCliente);
              $facturas = $command->queryAll();

              if(!empty($facturas)){
                echo $cliente['idCliente'].' '.$cliente['nombreCompleto']."\n";
                $mostrarEncabezado = true;
                $declarar = $mt_subtotal = $mt_descuento = 0;
              foreach($facturas as $key => $factura) {
                /*var_dump($factura);
                echo "<br />";*/
                $mt_subtotal += $factura['subtotal'];
                $mt_descuento += $factura['descuento'];

                if($mostrarEncabezado){
                  $enbazedado= ['N° Interno',
                  'N° Factura Electrónica',
                  'Fecha Inicio',
                  'Subtotal',
                  'Descuento',
                  'Impuesto',
                  'Total'];
                  echo implode("\t", $enbazedado) . "\n";
                  // echo "<br />";
                  $mostrarEncabezado = false;
                }
                echo $factura['n_interno']."\t";
                if($factura['n_fe'] != null){
                  echo "N° ".$factura['n_fe']."\t";
                }else{
                  echo $factura['n_fe']."\t";
                }
                echo Date('d-m-Y', strtotime($factura['fecha_inicio']))."\t";
                echo $factura['subtotal']."\t";
                echo $factura['descuento']."\t";
                echo $factura['impuesto']."\t";
                echo $factura['total']."\n";
                // echo $factura['idCliente']."\t";
                // echo implode("\t", array_values($factura)) . "\n";
                // echo "<br />";
              }/* fin foreach $facturas*/
              echo "\t"."\t".'Totales: '."\t".$mt_subtotal."\t".$mt_descuento."\t".'Declarar:'."\t".($mt_subtotal-$mt_descuento)."\n"."\n";
            }/*fin facturas*/
          } /* fin if sumD151 >= monto minimo*/
        }
        /*
        if(!empty($records))
        echo implode("\t", array_keys($row)) . "\n";
        foreach($records as $row) {
        echo implode("\t", array_values($row)) . "\n";
      }*/

    } /*fin if($lista_clientes != '0')*/
    exit;
  }

  public function actionCargarClientePeriodo($anno){
    $sql = "SELECT SUM(ef.subtotal-ef.porc_descuento) AS sumD151, ef.idCliente FROM `tbl_encabezado_factura` ef
    WHERE ef.idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
      (SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC'
      GROUP BY idCabeza_Factura) fact_not
      ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
      WHERE fact.total_a_pagar = fact_not.suma_nc)
      AND ef.fecha_inicio BETWEEN :fechaFactura1 AND :fechaFactura2 AND ef.estadoFactura !='Anulada'  GROUP BY ef.idCliente DESC
      HAVING sumD151 >=".Yii::$app->params['limite_monto_venta_MIN'].";";
      $anno = (int)$anno;
      $annoAnterior = $anno-1;
      $fechaFactura1 = $annoAnterior.'-10-01';
      $fechaFactura2 = $anno.'-09-30';
      $command = \Yii::$app->db->createCommand($sql);
      $command->bindParam(":fechaFactura1", $fechaFactura1);
      $command->bindParam(":fechaFactura2", $fechaFactura2);
      $facturas = $command->queryAll();

      $clientesArray = [];
      foreach ($facturas as $key => $factura) {
        $clientesArray[]=$factura['idCliente'];
      }
      $clientes = Clientes::find()->where(['IN','idCliente',$clientesArray])->all();
      $opcion=null;
      foreach ($clientes as $cliente) {
        $opcion .= "<option value='".$cliente['idCliente']."'>".$cliente['idCliente'].' - '.$cliente['nombreCompleto']."</option>";
      }
      return $opcion;
    }

    //Pagos x Caja
    public function actionRcaja_m_pagos_x_caja()
    {
      $fecha_desde = $_GET['fecha_desde'];
      $fecha_hasta = $_GET['fecha_hasta'];
      return $this->renderAjax('rcaja_m_pagos_x_caja', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcaja_m_pagos_x_caja()
    {
      $fecha_desde = $_GET['fecha_desde'];
      $fecha_hasta = $_GET['fecha_hasta'];
      return $this->renderAjax('icaja_m_pagos_x_caja', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    //RECEPCIONES DE HACIENDA
    public function actionRcaja_m_recepcion_hacienda_ge($lista_documento_electronico, $fecha_desde, $fecha_hasta, $lista_resolucion, $lista_respuesta)
    {
      return $this->renderAjax('rcaja_m_recepcion_hacienda_ge', [
        'lista_documento_electronico' => $lista_documento_electronico,
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_resolucion' => $lista_resolucion,
        'lista_respuesta' => $lista_respuesta
      ]);
    }
    public function actionIcaja_m_recepcion_hacienda_ge($lista_documento_electronico, $fecha_desde, $fecha_hasta, $lista_resolucion, $lista_respuesta)
    {
      return $this->renderAjax('icaja_m_recepcion_hacienda_ge', [
        'lista_documento_electronico' => $lista_documento_electronico,
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_resolucion' => $lista_resolucion,
        'lista_respuesta' => $lista_respuesta
      ]);
    }

    //RESUMEN COMICIONES
    public function actionRcaja_m_resumen_comisiones($lista_clientes, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcaja_m_resumen_comisiones', [
        'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcaja_m_resumen_comisiones($lista_clientes, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('icaja_m_resumen_comisiones', [
        'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    //DETALLE COMICIONES
    public function actionRcaja_m_detalle_comisiones($lista_clientes, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcaja_m_detalle_comisiones', [
        'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcaja_m_detalle_comisiones($lista_clientes, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('icaja_m_detalle_comisiones', [
        'lista_clientes' => $lista_clientes, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    //LISTA DE APARTADO
    public function actionRcaja_m_apartado_lista($filtro_vendedor_apartado, $incluir_abono_apart, $apart_vencidas, $apart_al_dia, $lista_clientes, $fecha_desde, $fecha_hasta, $fecha_desde_ab, $fecha_hasta_ab)
    {
      return $this->renderAjax('rcaja_m_apartado_lista', [
        'filtro_vendedor_apartado' => $filtro_vendedor_apartado,
        'incluir_abono_apart' => $incluir_abono_apart,
        'apart_vencidas' => $apart_vencidas,
        'apart_al_dia' => $apart_al_dia,
        'lista_clientes' => $lista_clientes,
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'fecha_desde_ab' => $fecha_desde_ab,
        'fecha_hasta_ab' => $fecha_hasta_ab
      ]);
    }

    public function actionIcaja_m_apartado_lista($filtro_vendedor_apartado, $incluir_abono_apart, $apart_vencidas, $apart_al_dia, $lista_clientes, $fecha_desde, $fecha_hasta, $fecha_desde_ab, $fecha_hasta_ab)
    {
      return $this->renderAjax('icaja_m_apartado_lista', [
        'filtro_vendedor_apartado' => $filtro_vendedor_apartado,
        'incluir_abono_apart' => $incluir_abono_apart,
        'apart_vencidas' => $apart_vencidas,
        'apart_al_dia' => $apart_al_dia,
        'lista_clientes' => $lista_clientes,
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'fecha_desde_ab' => $fecha_desde_ab,
        'fecha_hasta_ab' => $fecha_hasta_ab
      ]);
    }

    //APARTADOS CANCELADOS
    public function actionRcaja_m_apartado_cancelado($lista_clientes, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcaja_m_apartado_cancelado', [
        'lista_clientes' => $lista_clientes,
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcaja_m_apartado_cancelado($lista_clientes, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcaja_m_apartado_cancelado', [
        'lista_clientes' => $lista_clientes,
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta
      ]);
    }

    //=========================REPORTE COMPRAS=======================

    //Ordenes Compra X Proveedor
    public function actionRcompras_m_orden_compra_x_proveedor($lista_proveedores, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcompras_m_orden_compra_x_proveedor', [
        'lista_proveedores' => $lista_proveedores, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcompras_m_orden_compra_x_proveedor($lista_proveedores, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('icompras_m_orden_compra_x_proveedor', [
        'lista_proveedores' => $lista_proveedores, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    //RESUMEN COMPRAS A PROVEEDOR
    public function actionRcompras_m_resumen_compras($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcompras_m_resumen_compras', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcompras_m_resumen_compras($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('icompras_m_resumen_compras', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);

    }

    //RESUMEN COMPRAS A PROVEEDOR
    public function actionRcompras_m_detalle_compras($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcompras_m_detalle_compras', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcompras_m_detalle_compras($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('icompras_m_detalle_compras', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);

    }

    //RESUMEN COMPRAS GRABADAS / EXCENTAS
    public function actionRcompras_m_compras_gra_ex($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rcompras_m_compras_gra_ex', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);
    }

    public function actionIcompras_m_compras_gra_ex($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('icompras_m_compras_gra_ex', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta
      ]);

    }

    //=========================REPORTE DE INVENTARIO=======================

    //Costo inventario
    public function actionIinventario_m_costo()
    {
      echo $this->renderAjax('iinventario_m_costo', []);
    }

    //Resumen costo inventario categoria
    public function actionRinventario_m_costo_categoria($lista_filtro_cagtegoria_inven)
    {
      return $this->renderAjax('rinventario_m_costo_categoria', [
        'lista_filtro_cagtegoria_inven' => $lista_filtro_cagtegoria_inven
      ]);
    }
    public function actionIinventario_m_costo_categoria($lista_filtro_cagtegoria_inven)
    {
      return $this->renderAjax('iinventario_m_costo_categoria', [
        'lista_filtro_cagtegoria_inven' => $lista_filtro_cagtegoria_inven
      ]);
    }

    //producto inventario categoria
    public function actionRinventario_m_x_categoria($lista_filtro_cagtegoria_inven, $filtro_cantidad)
    {
      return $this->renderAjax('rinventario_m_x_categoria', [
        'lista_filtro_cagtegoria_inven' => $lista_filtro_cagtegoria_inven,
        'filtro_cantidad' => $filtro_cantidad
      ]);
    }
    public function actionIinventario_m_x_categoria($lista_filtro_cagtegoria_inven, $filtro_cantidad)
    {
      return $this->renderAjax('iinventario_m_x_categoria', [
        'lista_filtro_cagtegoria_inven' => $lista_filtro_cagtegoria_inven,
        'filtro_cantidad' => $filtro_cantidad
      ]);
    }

    //producto inventario debajo del minimo
    public function actionRinventario_m_x_debajo_minimo($lista_filtro_cagtegoria_inven)
    {
      return $this->renderAjax('rinventario_m_x_debajo_minimo', [
        'lista_filtro_cagtegoria_inven' => $lista_filtro_cagtegoria_inven
      ]);
    }
    public function actionIinventario_m_x_debajo_minimo($lista_filtro_cagtegoria_inven)
    {
      return $this->renderAjax('iinventario_m_x_debajo_minimo', [
        'lista_filtro_cagtegoria_inven' => $lista_filtro_cagtegoria_inven
      ]);
    }

    //producto inventario no vendido
    public function actionRinventario_m_producto_no_vendido($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rinventario_m_producto_no_vendido', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta'=>$fecha_hasta
      ]);
    }
    public function actionIinventario_m_producto_no_vendido($fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('iinventario_m_producto_no_vendido', [
        'fecha_desde' => $fecha_desde, 'fecha_hasta'=>$fecha_hasta
      ]);
    }

    //=========================REPORTE PROVEEDORES=======================
    //RESUMEN CUENTAS POR PAGAR (PROVEEDORES)
    public function actionRprovee_m_resumen_cuenta_pagar($lista_filtro_proveedores)
    {
      return $this->renderAjax('rprovee_m_resumen_cuenta_pagar', [
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }
    public function actionIprovee_m_resumen_cuenta_pagar($lista_filtro_proveedores)
    {
      return $this->renderAjax('iprovee_m_resumen_cuenta_pagar', [
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }
    //RESUMEN TRAMITE (PROVEEDORES)
    public function actionRprovee_m_tramites($fecha_desde, $fecha_hasta, $lista_filtro_proveedores)
    {
      return $this->renderAjax('rprovee_m_tramites', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }
    public function actionIprovee_m_tramites($fecha_desde, $fecha_hasta, $lista_filtro_proveedores)
    {
      return $this->renderAjax('iprovee_m_tramites', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }
    //NOTAS DE DEBITO A PROVEEDORES
    public function actionRprovee_m_notas_debito($fecha_desde, $fecha_hasta, $lista_filtro_proveedores)
    {
      return $this->renderAjax('rprovee_m_notas_debito', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }
    public function actionIprovee_m_notas_debito($fecha_desde, $fecha_hasta, $lista_filtro_proveedores)
    {
      return $this->renderAjax('iprovee_m_notas_debito', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }
    //NOTAS DE CREDITO A PROVEEDORES
    public function actionRprovee_m_notas_credito($fecha_desde, $fecha_hasta, $lista_filtro_proveedores)
    {
      return $this->renderAjax('rprovee_m_notas_credito', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }
    public function actionIprovee_m_notas_credito($fecha_desde, $fecha_hasta, $lista_filtro_proveedores)
    {
      return $this->renderAjax('iprovee_m_notas_credito', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_proveedores' => $lista_filtro_proveedores
      ]);
    }

    //Reporte D151 Proveedores
    public function actionRprovee_m_d151($anoSeleccionado)
    {
      return $this->renderAjax('rprovee_m_d151', [
        'anoSeleccionado' => $anoSeleccionado
      ]);
    }

    public function actionIprovee_m_d151($anoSeleccionado)
    {
      return $this->renderAjax('iprovee_m_d151', [
        'anoSeleccionado' => $anoSeleccionado
      ]);
    }

    //Reporte D151 detalle Proveedores
    public function actionRprovee_m_d151_detalle($anoSeleccionado, $lista_proveedores)
    {
      return $this->renderAjax('rprovee_m_d151_detalle', [
        'anoSeleccionado' => $anoSeleccionado, 'lista_proveedores'=>$lista_proveedores
      ]);
    }

    public function actionIprovee_m_d151_detalle($anoSeleccionado, $lista_proveedores)
    {
      return $this->renderAjax('iprovee_m_d151_detalle', [
        'anoSeleccionado' => $anoSeleccionado, 'lista_proveedores'=>$lista_proveedores
      ]);
    }

    public function actionCargarProveedorPeriodo($anno){
      $sql = "SELECT SUM(subTotal-descuento) AS sumD151, idProveedor FROM `tbl_compras`
      WHERE idCompra NOT IN (SELECT fact.idCompra FROM tbl_compras fact INNER JOIN
        (SELECT idCompra, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_pagar` WHERE tipo_mov = 'nc' GROUP BY idCompra) fact_not
        ON fact.idCompra = fact_not.idCompra
        WHERE fact.total = fact_not.suma_nc)
        AND fechaDocumento BETWEEN :fechaFactura1 AND :fechaFactura2  GROUP BY idProveedor DESC
        HAVING sumD151 >=".Yii::$app->params['limite_monto_compra_MIN'].";";
        $anno = (int)$anno;
        $annoAnterior = $anno-1;
        $fechaFactura1 = $annoAnterior.'-10-01';
        $fechaFactura2 = $anno.'-09-30';
        $command = \Yii::$app->db->createCommand($sql);
        $command->bindParam(":fechaFactura1", $fechaFactura1);
        $command->bindParam(":fechaFactura2", $fechaFactura2);
        $facturas = $command->queryAll();

        $clientesArray = [];
        foreach ($facturas as $key => $factura) {
          $clientesArray[]=$factura['idProveedor'];
        }
        $clientes = Proveedores::find()->where(['IN','codProveedores',$clientesArray])->all();
        $opcion=null;
        $s = 0;
        foreach ($clientes as $cliente) {
          $s++;
          $opcion .= "<option value='".$cliente['codProveedores']."'>".$cliente['codProveedores'].' - '.$cliente['nombreEmpresa']."</option>";
        }
        return $opcion;
        // echo "cantidad rows: ".$s;
      }

    //=========================REPORTE USUARIOS DEL SISTEMA (FUNCIONARIOS)=======================
    //RESUMEN DE VENTAS
    public function actionRfuncionario_m_resu_ventas($fecha_desde, $fecha_hasta, $lista_filtro_funcionarios)
    {
      return $this->renderAjax('rfuncionario_m_resu_ventas', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_funcionarios' => $lista_filtro_funcionarios
      ]);
    }
    public function actionIfuncionario_m_resu_ventas($fecha_desde, $fecha_hasta, $lista_filtro_funcionarios)
    {
      return $this->renderAjax('ifuncionario_m_resu_ventas', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_funcionarios' => $lista_filtro_funcionarios
      ]);
    }
    //RESUMEN DE NOTAS DE CREDITO
    public function actionRfuncionario_m_resu_notas_credito($fecha_desde, $fecha_hasta, $lista_filtro_funcionarios)
    {
      return $this->renderAjax('rfuncionario_m_resu_notas_credito', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_funcionarios' => $lista_filtro_funcionarios
      ]);
    }
    public function actionIfuncionario_m_resu_notas_credito($fecha_desde, $fecha_hasta, $lista_filtro_funcionarios)
    {
      return $this->renderAjax('ifuncionario_m_resu_notas_credito', [
        'fecha_desde' => $fecha_desde,
        'fecha_hasta' => $fecha_hasta,
        'lista_filtro_funcionarios' => $lista_filtro_funcionarios
      ]);
    }

    //=========================REPORTE DE ORDEN DE SERCICIOS=======================
    //ORDEN DE SERVICIO PENDIENTE
    public function actionIorden_servicio_m_pendiente()
    {
      return $this->renderAjax('iorden_servicio_m_pendiente', []);
    }
    //ORDEN DE SERVICIO POR MECANICOS
    public function actionRorden_servicio_m_resumen_meca($lista_filtro_mecanico, $fecha_desde, $fecha_hasta)
    {
      return $this->renderAjax('rorden_servicio_m_resumen_meca', [
        'lista_filtro_mecanico'=>$lista_filtro_mecanico,
        'fecha_desde'=>$fecha_desde,
        'fecha_hasta'=>$fecha_hasta]);
      }
      public function actionIorden_servicio_m_resumen_meca($lista_filtro_mecanico, $fecha_desde, $fecha_hasta)
      {
        return $this->renderAjax('iorden_servicio_m_resumen_meca', [
          'lista_filtro_mecanico'=>$lista_filtro_mecanico,
          'fecha_desde'=>$fecha_desde,
          'fecha_hasta'=>$fecha_hasta]);
        }
        //ORDEN DE SERVICIO POR MECANICOS DETALLE
        public function actionRorden_servicio_m_detalle_meca($lista_filtro_mecanico, $fecha_desde, $fecha_hasta)
        {
          return $this->renderAjax('rorden_servicio_m_detalle_meca', [
            'lista_filtro_mecanico'=>$lista_filtro_mecanico,
            'fecha_desde'=>$fecha_desde,
            'fecha_hasta'=>$fecha_hasta]);
          }
          public function actionIorden_servicio_m_detalle_meca($lista_filtro_mecanico, $fecha_desde, $fecha_hasta)
          {
            return $this->renderAjax('iorden_servicio_m_detalle_meca', [
              'lista_filtro_mecanico'=>$lista_filtro_mecanico,
              'fecha_desde'=>$fecha_desde,
              'fecha_hasta'=>$fecha_hasta]);
            }


          }//fin del controlador
