<?php

namespace backend\controllers;

use Yii;
use backend\models\UsuariosDerechos;
use backend\models\search\UsuariosDerechosSearch;
use backend\models\DerechosUsuarios;
use backend\models\search\DerechosUsuariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Operacion;
use yii\filters\AccessControl;

/**
 * UsuariosDerechosController implements the CRUD actions for UsuariosDerechos model.
 */
//class UsuariosDerechosController extends Controller
class UsuariosDerechosController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete'],
            'allow' => true,
            'roles' => ['@'],
          ]
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

    /**
     * Lists all UsuariosDerechos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuariosDerechosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsuariosDerechos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

      /*  return $this->redirect('index',[
            'model' => $this->findModelDerechos($id),
            ]);*/
    }

    /**
     * Creates a new UsuariosDerechos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsuariosDerechos();

        $tipoOperaciones = Operacion::find()->all();//Para crear operaciones

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tipoOperaciones' => $tipoOperaciones
            ]);
        }
    }

    /**
     * Updates an existing UsuariosDerechos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
   /* public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/
    public $categoriesArray;
    public function actionUpdate($id)
    {
       /*$model = $this->findModel(\Yii::$app->user->identity->id);
    if ($model == null) {
      $model = new ProfileReader();
      $model->rol_id = \Yii::$app->user->identity->id;
    }

    if ($model->load(Yii::$app->request->post()) && $model->save()) {

      //unlink the categories first to avoid duplicates
      $model->unlinkAll('categories', true);
      //link the categories to the pen name
      foreach ($model->categoriesArray as $pc) {
        if ($pc) {
          foreach ($pc as $sc) {
            $sc = (new Operacion)->findOne($sc);
            $model->link('categories', $sc);
          }
        }
      }

      return $this->redirect(['update']);
    } else {

      //get the categories and separate them into groups based on rol_id
      foreach ($model->UsuariosDerechos as $c) {
        $model->categoriesArray[$c->rol_id][] = $c;
      }

      return $this->render('update', [
          'model' => $model,
      ]);
    }*/
    //-----------------------------------------------------------------------------------------
        $model = $this->findModel($id);

        /*Lo que hacemos aquí, es recuperar las opciones que han sido marcadas,
        usando el método getRolOperaciones del modelo. Encadenado a la llamada al método getRolOperaciones,
        tenemos la llamada al método asArray, que devuelve los modelos en la forma de un arregla. Finalmente,
        encadenamos con el método all que devuelve todos los resultados.*/

       // $tipoOperaciones = Operacion::find()->all();
       // $tipoAsercaDe = Operacion::findAll(array('id'=>'1'));
        $tipoClitI = Operacion::findAll(array('id'=>'2')); $tipoClitC = Operacion::findAll(array('id'=>'3'));
        $tipoClitU = Operacion::findAll(array('id'=>'4')); $tipoClitD = Operacion::findAll(array('id'=>'5'));
        $tipoClitV = Operacion::findAll(array('id'=>'11'));
        $tipoFunI = Operacion::findAll(array('id'=>'6')); $tipoFunC = Operacion::findAll(array('id'=>'7'));
        $tipoFunU = Operacion::findAll(array('id'=>'8')); $tipoFunD = Operacion::findAll(array('id'=>'9'));
        $tipoFunV = Operacion::findAll(array('id'=>'10'));
        $tipoMecI = Operacion::findAll(array('id'=>'16')); $tipoMecC = Operacion::findAll(array('id'=>'17'));
        $tipoMecU = Operacion::findAll(array('id'=>'18')); $tipoMecD = Operacion::findAll(array('id'=>'19'));
        $tipoMecV = Operacion::findAll(array('id'=>'20'));
        $tipoMarI = Operacion::findAll(array('id'=>'21')); $tipoMarC = Operacion::findAll(array('id'=>'22'));
        $tipoMarU = Operacion::findAll(array('id'=>'23')); $tipoMarD = Operacion::findAll(array('id'=>'24'));
        $tipoMarV = Operacion::findAll(array('id'=>'25'));
        $tipoModI = Operacion::findAll(array('id'=>'26')); $tipoModC = Operacion::findAll(array('id'=>'27'));
        $tipoModU = Operacion::findAll(array('id'=>'28')); $tipoModD = Operacion::findAll(array('id'=>'29'));
        $tipoModV = Operacion::findAll(array('id'=>'30'));
        $tipoVeI = Operacion::findAll(array('id'=>'31')); $tipoVeC = Operacion::findAll(array('id'=>'32'));
        $tipoVeU = Operacion::findAll(array('id'=>'33')); $tipoVeD = Operacion::findAll(array('id'=>'34'));
        $tipoVeV = Operacion::findAll(array('id'=>'35'));
        $tipoFaI = Operacion::findAll(array('id'=>'36')); $tipoFaC = Operacion::findAll(array('id'=>'37'));
        $tipoFaU = Operacion::findAll(array('id'=>'38')); $tipoFaD = Operacion::findAll(array('id'=>'39'));
        $tipoFaV = Operacion::findAll(array('id'=>'40'));
        $tipoSeI = Operacion::findAll(array('id'=>'41')); $tipoSeC = Operacion::findAll(array('id'=>'42'));
        $tipoSeU = Operacion::findAll(array('id'=>'43')); $tipoSeD = Operacion::findAll(array('id'=>'44'));
        $tipoSeV = Operacion::findAll(array('id'=>'45'));
        $tipoPrI = Operacion::findAll(array('id'=>'46')); $tipoPrC = Operacion::findAll(array('id'=>'47'));
        $tipoPrU = Operacion::findAll(array('id'=>'48')); $tipoPrD = Operacion::findAll(array('id'=>'49'));
        $tipoPrV = Operacion::findAll(array('id'=>'50'));
        $tipoPI = Operacion::findAll(array('id'=>'51')); $tipoPC = Operacion::findAll(array('id'=>'52'));
        $tipoPU = Operacion::findAll(array('id'=>'53')); $tipoPD = Operacion::findAll(array('id'=>'54'));
        $tipoPV = Operacion::findAll(array('id'=>'55'));
        $tipoOrSeI = Operacion::findAll(array('id'=>'56')); $tipoOrSeC = Operacion::findAll(array('id'=>'57'));
        $tipoOrSeU = Operacion::findAll(array('id'=>'58')); $tipoOrSeD = Operacion::findAll(array('id'=>'59'));
        $tipoOrSeV = Operacion::findAll(array('id'=>'60'));
        $tipoVerI = Operacion::findAll(array('id'=>'61')); $tipoVerC = Operacion::findAll(array('id'=>'62'));
        $tipoVerU = Operacion::findAll(array('id'=>'63')); $tipoVerD = Operacion::findAll(array('id'=>'64'));
        $tipoVerV = Operacion::findAll(array('id'=>'65'));
        $tipoComI = Operacion::findAll(array('id'=>'66')); $tipoComC = Operacion::findAll(array('id'=>'67'));
        $tipoComU = Operacion::findAll(array('id'=>'68')); $tipoComD = Operacion::findAll(array('id'=>'69'));
        $tipoComV = Operacion::findAll(array('id'=>'70'));
        $tipoCueB = Operacion::findAll(array('id' => '71'));
        $tipoTPI = Operacion::findAll(array('id' => '72'));$tipoTPMo = Operacion::findAll(array('id' => '73'));
        $tipoTPCP = Operacion::findAll(array('id' => '74'));
        $tipoTipmI = Operacion::findAll(array('id'=>'75')); $tipoTipmC = Operacion::findAll(array('id'=>'76'));
        $tipoTipmU = Operacion::findAll(array('id'=>'77')); $tipoTipmD = Operacion::findAll(array('id'=>'78'));
        $tipoTipmV = Operacion::findAll(array('id'=>'79'));
        $tipoMovI = Operacion::findAll(array('id'=>'80')); $tipoMovC = Operacion::findAll(array('id'=>'81'));
        $tipoMovU = Operacion::findAll(array('id'=>'82')); $tipoMovD = Operacion::findAll(array('id'=>'83'));
        $tipoMovA = Operacion::findAll(array('id'=>'84'));
        $tipoFacD = Operacion::findAll(array('id'=>'85'));
        $tipoDevPro = Operacion::findAll(array('id'=>'132'));
        $tipoMovCreI = Operacion::findAll(array('id'=>'86')); $tipoMovCreA = Operacion::findAll(array('id'=>'87'));
        $tipoMovCreNC = Operacion::findAll(array('id'=>'88')); $tipoMovCreCF = Operacion::findAll(array('id'=>'89'));
        $tipoRes_doXML = Operacion::findAll(array('id'=>'134'));
        $tipoOrComI = Operacion::findAll(array('id'=>'90'));
        $tipoOrComC = Operacion::findAll(array('id'=>'91'));
        $tipoTranspI = Operacion::findAll(array('id'=>'92')); $tipoTranspC = Operacion::findAll(array('id'=>'93'));
        $tipoTranspU = Operacion::findAll(array('id'=>'94')); $tipoTranspD = Operacion::findAll(array('id'=>'95'));
        $tipoLibB = Operacion::findAll(array('id'=>'96'));
        $tipoCaPa = Operacion::findAll(array('id'=>'97')); $tipoCoPa = Operacion::findAll(array('id'=>'98'));
        $tipoProf_vt = Operacion::findAll(array('id'=>'99')); $tipoProf_c = Operacion::findAll(array('id'=>'100'));
        $tipoProf_u = Operacion::findAll(array('id'=>'101'));
        $tipoprefa_vt = Operacion::findAll(array('id'=>'102')); $tipoprefa_c = Operacion::findAll(array('id'=>'103'));
        $tipoprefa_u = Operacion::findAll(array('id'=>'104')); $tipoprefa_d = Operacion::findAll(array('id'=>'105'));
        $tipocaja_vt = Operacion::findAll(array('id'=>'106')); $tipocaja_dev = Operacion::findAll(array('id'=>'107'));
        $tipocaja_can = Operacion::findAll(array('id'=>'108')); $tipocaja_rev = Operacion::findAll(array('id'=>'109'));
        $tipocategas_vt = Operacion::findAll(array('id'=>'110')); $tipocategas_c = Operacion::findAll(array('id'=>'111'));
        $tipocategas_u = Operacion::findAll(array('id'=>'112')); $tipocategas_d = Operacion::findAll(array('id'=>'113'));
        $tipotipgas_vt = Operacion::findAll(array('id'=>'114')); $tipotipgas_c = Operacion::findAll(array('id'=>'115'));
        $tipotipgas_u = Operacion::findAll(array('id'=>'116')); $tipotipgas_d = Operacion::findAll(array('id'=>'117'));
        $tipoprovgas_vt = Operacion::findAll(array('id'=>'118')); $tipoprovgas_c = Operacion::findAll(array('id'=>'119'));
        $tipoprovgas_u = Operacion::findAll(array('id'=>'120')); $tipoprovgas_d = Operacion::findAll(array('id'=>'121'));
        $tiporegistgas_d = Operacion::findAll(array('id'=>'122'));
        $tiporbancos_m = Operacion::findAll(array('id'=>'123')); $tiporcaja_m = Operacion::findAll(array('id'=>'124'));
        $tiporcompras_m = Operacion::findAll(array('id'=>'125')); $tiporinventario_m = Operacion::findAll(array('id'=>'126'));
        $tipoorden_servicio_m = Operacion::findAll(array('id'=>'127')); $tipoproveedores_m = Operacion::findAll(array('id'=>'128'));
        $tipousuarios_m = Operacion::findAll(array('id'=>'129'));
        $tipomarpro = Operacion::findAll(array('id'=>'133')); /*marcas productos index*/

        $model->operaciones = \yii\helpers\ArrayHelper::getColumn(
        /*Notemos también que todo esto ocurre dentro del método getColumn de ArrayHelper. Este método,
        devuelve los valores de una columna específica de un arreglo. En este caso, ‘operacion_id’.*/
            $model->getRolOperaciones()->asArray()->all(),
            'operacion_id'
        );
        if ($model->load(Yii::$app->request->post())) {
            /*Lo que estamos haciendo aquí, luego de cargar el modelo con los parámetros
            recibidos via post, es comprobar si se encuentra presente: $_POST['UsuariosDerechos']['operaciones']*/
            if (!isset($_POST['UsuariosDerechos']['operaciones'])) {
                /*El motivo de ello, es que esta variable no estará presente cuando no se haya marcado ninguna operación.
                Si no se ha seleccionado ninguna operación, hacemos: $model->operaciones = [];*/
                $model->operaciones = [];
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        /*Esto es necesario por lo siguiente. Si el rol tenía asignadas operaciones, estas se encontrarán guardadas en
        $model->operaciones. Si luego se destildan todas las operaciones, entonces la variable $_POST[‘Rol’][‘operaciones’]
        no estará presente para sobreescribir a $model->operaciones. Si no asignaramos un arreglo vacío, se volverían a
        insertar exactamente los mismos registros que antes de desmarcar todas las opciones.*/
        else {
           return $this->render('update', [
                'model' => $model,
               // 'tipoAsercaDe' => $tipoAsercaDe,
                'tipoClitI' => $tipoClitI, 'tipoClitC' => $tipoClitC, 'tipoClitU' => $tipoClitU,
                'tipoClitD' => $tipoClitD, 'tipoClitV' => $tipoClitV,
                'tipoFunI' => $tipoFunI, 'tipoFunC' => $tipoFunC, 'tipoFunU' => $tipoFunU,
                'tipoFunD' => $tipoFunD, 'tipoFunV' => $tipoFunV,
                'tipoMecI' => $tipoMecI, 'tipoMecC' => $tipoMecC, 'tipoMecU' => $tipoMecU,
                'tipoMecD' => $tipoMecD, 'tipoMecV' => $tipoMecV,
                'tipoMarI' => $tipoMarI, 'tipoMarC' => $tipoMarC, 'tipoMarU' => $tipoMarU,
                'tipoMarD' => $tipoMarD, 'tipoMarV' => $tipoMarV,
                'tipoModI' => $tipoModI, 'tipoModC' => $tipoModC, 'tipoModU' => $tipoModU,
                'tipoModD' => $tipoModD, 'tipoModV' => $tipoModV,
                'tipoVeI' => $tipoVeI, 'tipoVeC' => $tipoVeC, 'tipoVeU' => $tipoVeU,
                'tipoVeD' => $tipoVeD, 'tipoVeV' => $tipoVeV,
                'tipoFaI' => $tipoFaI, 'tipoFaC' => $tipoFaC, 'tipoFaU' => $tipoFaU,
                'tipoFaD' => $tipoFaD, 'tipoFaV' => $tipoFaV,
                'tipoSeI' => $tipoSeI, 'tipoSeC' => $tipoSeC, 'tipoSeU' => $tipoSeU,
                'tipoSeD' => $tipoSeD, 'tipoSeV' => $tipoSeV,
                'tipoPrI' => $tipoPrI, 'tipoPrC' => $tipoPrC, 'tipoPrU' => $tipoPrU,
                'tipoPrD' => $tipoPrD, 'tipoPrV' => $tipoPrV,
                'tipoPI' => $tipoPI, 'tipoPC' => $tipoPC, 'tipoPU' => $tipoPU,
                'tipoPD' => $tipoPD, 'tipoPV' => $tipoPV,
                'tipoOrSeI' => $tipoOrSeI, 'tipoOrSeC' => $tipoOrSeC, 'tipoOrSeU' => $tipoOrSeU,
                'tipoOrSeD' => $tipoOrSeD, 'tipoOrSeV' => $tipoOrSeV,
                'tipoVerI' => $tipoVerI, 'tipoVerC' => $tipoVerC, 'tipoVerU' => $tipoVerU,
                'tipoVerD' => $tipoVerD, 'tipoVerV' => $tipoVerV,
                'tipoComI' => $tipoComI, 'tipoComC' => $tipoComC, 'tipoComU' => $tipoComU,
                'tipoComD' => $tipoComD, 'tipoComV' => $tipoComV,
                'tipoCueB'=>$tipoCueB,
                'tipoTPI'=>$tipoTPI, 'tipoTPMo'=> $tipoTPMo ,'tipoTPCP'=>$tipoTPCP,
                'tipoTipmI' => $tipoTipmI, 'tipoTipmC' => $tipoTipmC, 'tipoTipmU' => $tipoTipmU,
                'tipoTipmD' => $tipoTipmD, 'tipoTipmV' => $tipoTipmV,
                'tipoMovI' => $tipoMovI, 'tipoMovC' => $tipoMovC, 'tipoMovU' => $tipoMovU,
                'tipoMovD' => $tipoMovD, 'tipoMovA' => $tipoMovA,
                'tipoFacD' => $tipoFacD,
                'tipoDevPro' => $tipoDevPro,
                'tipoMovCreI'=>$tipoMovCreI, 'tipoMovCreA'=>$tipoMovCreA,
                'tipoMovCreNC' => $tipoMovCreNC, 'tipoMovCreCF' => $tipoMovCreCF,
                'tipoRes_doXML' => $tipoRes_doXML,
                'tipoOrComI' => $tipoOrComI, 'tipoOrComC' => $tipoOrComC,
                'tipoTranspI' => $tipoTranspI, 'tipoTranspC' => $tipoTranspC,
                'tipoTranspU' => $tipoTranspU, 'tipoTranspD' => $tipoTranspD,
                'tipoLibB' => $tipoLibB,
                'tipoCaPa' => $tipoCaPa, 'tipoCoPa' => $tipoCoPa,
                'tipoProf_vt' => $tipoProf_vt, 'tipoProf_c' => $tipoProf_c,
                'tipoProf_u' => $tipoProf_u,
                'tipoprefa_vt' => $tipoprefa_vt, 'tipoprefa_c' => $tipoprefa_c,
                'tipoprefa_u' => $tipoprefa_u, 'tipoprefa_d' => $tipoprefa_d,
                'tipocaja_vt' => $tipocaja_vt, 'tipocaja_dev' => $tipocaja_dev,
                'tipocaja_can' => $tipocaja_can, 'tipocaja_rev' => $tipocaja_rev,
                'tipocategas_vt' => $tipocategas_vt, 'tipocategas_c' => $tipocategas_c,
                'tipocategas_u' => $tipocategas_u, 'tipocategas_d' => $tipocategas_d,
                'tipotipgas_vt' => $tipotipgas_vt, 'tipotipgas_c' => $tipotipgas_c,
                'tipotipgas_u' => $tipotipgas_u, 'tipotipgas_d' => $tipotipgas_d,
                'tipoprovgas_vt' => $tipoprovgas_vt, 'tipoprovgas_c' => $tipoprovgas_c,
                'tipoprovgas_u' => $tipoprovgas_u, 'tipoprovgas_d' => $tipoprovgas_d,
                'tiporegistgas_d' => $tiporegistgas_d,
                'tiporbancos_m' => $tiporbancos_m, 'tiporcaja_m' => $tiporcaja_m,
                'tiporcompras_m' => $tiporcompras_m, 'tiporinventario_m' => $tiporinventario_m,
                'tipoorden_servicio_m' => $tipoorden_servicio_m, 'tipoproveedores_m' => $tipoproveedores_m,
                'tipousuarios_m' => $tipousuarios_m,
                'tipomarpro' => $tipomarpro
            ]);
        }
    }

    /**
     * Deletes an existing UsuariosDerechos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UsuariosDerechos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsuariosDerechos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsuariosDerechos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

   /*
    public function findModelDerechos($id)
    {
        if (($model = DerechosUsuarios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }*/
    //funcion que me debuelve los id con los nombres de todos los registro para la actualizacion de estudiantes.
    /*public function getMenuModuloReg()
    {
        return CHtml::listData(RegistroEstudiante::model()->findAll(),"id_RE","selectName");
    }*/
}
