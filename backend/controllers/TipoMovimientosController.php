<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use backend\models\TipoMovimientos;
use backend\models\search\TipoMovimientosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//---------------------para la modal de eliminar con permiso de admin
use yii\web\Response;
use common\models\DeletePass; //para confirmar eliminacion de clientes
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;

/**
 * TipoMovimientosController implements the CRUD actions for TipoMovimientos model.
 */
class TipoMovimientosController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'only' => ['index','view','create','update','delete'],
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all TipoMovimientos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TipoMovimientosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single TipoMovimientos model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TipoMovimientos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TipoMovimientos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoTipo]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TipoMovimientos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoTipo]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TipoMovimientos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete()
    {
      if(Yii::$app->request->post())
      {

        $id = Html::encode($_POST["codigoTipo"]);

        if($id)
        {
          $this->findModel($id)->delete();
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El tipo de movimiento se ha eliminado correctamente.');
          return $this->redirect(['index']);
        }
      }
    }

    /**
     * Finds the TipoMovimientos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TipoMovimientos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TipoMovimientos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
