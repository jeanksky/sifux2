<?php

namespace backend\controllers;

use Yii;
use backend\models\TramitePago;
use backend\models\search\TramitePagoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Proveedores;
use backend\models\ComprasInventario;
use backend\models\DebitoSession;
use backend\models\MovimientoPagar;
use backend\models\Empresa;
use backend\models\ContactosPagos;
use backend\models\MedioPagoProveedor;
use backend\models\MovimientoLibros;
use backend\models\CuentasBancarias;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\TipoDocumento;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * TramitePagoController implements the CRUD actions for TramitePago model.
 */
class TramitePagoController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete','movimientos','completar_pago',
                'vista_factura','addproveedor','limpiar','busqueda_proveedor','obtener_facturas_marcadas',
                'obtener_facturas_marcadas_regreso','agrega_factura_lista','tbl_uno','tbl_dos',
                'mov_facturafecha','agrega_nota_credito','actualiza_nota_credito','elimina_nota_credito',
                'tbl_nc','imprimir_movimientos','tbl_nd','agrega_nota_debito', 'nc_tramite',
                'actualiza_nota_debito','elimina_nota_debito','tbl_ab','agrega_abono',
                'generar_pago','agrega_nota_credito_tramite','actualiza_nota_credito_tramite',
                'elimina_nota_credito_tramite','tbl_nc_tramite','montopagar','valida_numero_cheque',
                'valida_numero_comprovante_trans', 'actualizar_pago','listatramite', 'obtenerfacturasdetramite','imprimir',
                'enviar_tramite','eliminar_tramite','buscar_factura_cancelada','vista_movimientos'
              ],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all TramitePago models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TramitePagoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TramitePago model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionVista_factura($id)
    {
        $model = ComprasInventario::find()->where(["=",'idCompra', $id])->one();
        return $this->renderAjax('vista_factura', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new TramitePago model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TramitePago();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idTramite_pago]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TramitePago model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idTramite_pago]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TramitePago model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TramitePago model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TramitePago the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TramitePago::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddproveedor()
    {
        if(Yii::$app->request->isAjax){
            $codProveedor = Yii::$app->request->post('codProveedor');
            if ($modelproveedor = Proveedores::find()->where(['=','codProveedores', $codProveedor])->one()){
                //if($compra = ComprasInventario::find()->where(['=','idProveedor', $modelproveedor->codProveedores])/*->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])*/->one()){
                  $this->cargardatos($codProveedor);
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Las facturas pendientes de '.$modelproveedor->nombreEmpresa.' se han cargado');

                //} else {
                //  Yii::$app->getSession()->setFlash('info', '<span class="glyphicon glyphicon-flag"></span> <strong>Atención!</strong> No se registran facturas para este proveedor.');
                //}
                Yii::$app->response->redirect(array('tramite-pago/index'));
            }else{
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> El ID digitado no pertenece a ningun proveedor registrado.');
                Yii::$app->response->redirect(array('tramite-pago/index'));
            }
        }
    }

    //carga el proveedor en sssion para decirle al sistema el provedor con el que se trabajará
    public function cargardatos($idProveedor){
        $modelproveedor = Proveedores::find()->where(['=','codProveedores', $idProveedor])->one();
        //$montodisponible = $modelproveedor->montoMaximoCredito - $modelproveedor->montoTotalEjecutado;
        DebitoSession::setIDproveedor($modelproveedor->codProveedores);
        DebitoSession::setProveedor($modelproveedor->nombreEmpresa);//Agrego el nombre del proveedor
    }

    //permite vaciar la session y quitar los datos del proveedor en pantalla
    public function actionLimpiar(){
        DebitoSession::setIDproveedor(null);
        DebitoSession::setProveedor(null);


        Yii::$app->response->redirect(array('tramite-pago/index'));
    }

    //me busca los proveedres registrados en bd
        public function actionBusqueda_proveedor(){
         if(Yii::$app->request->isAjax){
                    $consultaBusqueda  = Yii::$app->request->post('valorBusqueda');

                    //Filtro anti-XSS
                    $caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
                    $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
                    $consultaBusqueda = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda);

                    $mensaje = "";

                    //Comprueba si $consultaBusqueda está seteado
                    if (isset($consultaBusqueda)) {
                        $consultaBusquedalimpia = "%".$consultaBusqueda."%";
                        //Selecciona todo de la tabla idCliente
                        //donde el nombre sea igual a $consultaBusqueda,
                        //o el apellido sea igual a $consultaBusqueda,
                        //o $consultaBusqueda sea igual a nombre + (espacio) + apellido
                        $sql = "SELECT * FROM tbl_proveedores
                        WHERE codProveedores LIKE :like_
                        OR nombreEmpresa LIKE :like_
                        OR CONCAT(codProveedores,' ',nombreEmpresa)  LIKE :like_";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":like_", $consultaBusquedalimpia);
                        $consulta = $command->queryAll();
                        //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                        //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                            //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                            //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                            //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle


                            foreach($consulta as $resultados){
                                $codProveedores = $resultados['codProveedores'];
                                $nombreEmpresa = $resultados['nombreEmpresa'];
                                //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                                $accion = Html::a('', '', [
                                    'class' => 'glyphicon glyphicon-plus',
                                    'onclick'=>'agrega_proveedor("'.$codProveedores.'")',
                                    'data-dismiss'=>'modal',
                                    'title' => Yii::t('app', 'Cargar proveedor')
                                ]);
                                //Output

                         $mensaje .='<tbody><tr>
                                      <td width="50" align="center">'.$codProveedores.'</td>
                                      <td width="400">'.$nombreEmpresa.'</td>
                                      <td width="60" align="center">'.$accion.'</td>
                                    </tr></tbody>';

                            }
                    //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
                }
                echo $mensaje;
            }//fin ajax
        }//fin de funcion de busqueda de productos

//--------------------------------------------------------------------------------------------------------
//----------------------------------------------area de movimientos---------------------------------------
//--------------------------------------------------------------------------------------------------------

        //actualizo tbl_compras para que todas las credi_atencion que estan seleccionadas
        //tengan un estado de Proceso y poder cancelar
        public function actionObtener_facturas_marcadas(){
            if(Yii::$app->request->isAjax){
                $checkboxValues  = Yii::$app->request->post('checkboxValues');
                $array = explode(",", $checkboxValues);

                $longitud = count($array);
                if ($checkboxValues) {
                    for($i=0; $i<$longitud; $i++) {
                        //$estado_pago = 'Proceso';
                        $estadoFacturaViejo = 'Pendiente';
                        $estadoFacturaNuevo = 'Proceso';
                        /*$sql = "UPDATE tbl_compras SET estado_pago = :estado_pago WHERE idCompra = :idCompra";

                            $command = \Yii::$app->db->createCommand($sql);
                            $command->bindParam(":estado_pago", $estado_pago);
                            $command->bindValue(":idCompra", $array[$i]);
                            $command->execute();*/
                        $fact_compr = ComprasInventario::find()->where(['=','idCompra', $array[$i]])->one();
                        if ($fact_compr->estado_pago == $estadoFacturaViejo) {
                            $fact_compr->estado_pago = $estadoFacturaNuevo;
                            $fact_compr->save();
                        }
                    }
                    echo '<span class="label label-success" style="font-size:15pt;">Facturas agregadas correctamente</span>';
                } else echo '<span class="label label-info" style="font-size:15pt;">No se ha obtenido resultados</span>';

            }
            //Yii::$app->response->redirect(array('tramite-pago/index'));
        }

        //actualizo tbl_compras para que todas las credi_atencion que estan seleccionadas
        //tengan un estado de Pendientes y regresarlas al campo de facturas pendientes
        public function actionObtener_facturas_marcadas_regreso(){
            if(Yii::$app->request->isAjax){
                $checkboxValues  = Yii::$app->request->post('checkboxValues');
                $array = explode(",", $checkboxValues);

                $longitud = count($array);
                if ($checkboxValues) {
                    for($i=0; $i<$longitud; $i++) {
                        $estado_pago = 'Pendiente';

                        $sql = "UPDATE tbl_compras SET estado_pago = :estado_pago WHERE idCompra = :idCompra";

                            $command = \Yii::$app->db->createCommand($sql);
                            $command->bindParam(":estado_pago", $estado_pago);
                            $command->bindValue(":idCompra", $array[$i]);
                            $command->execute();
                    }
                    echo '<span class="label label-success" style="font-size:15pt;">Facturas devueltas correctamente</span>';
                } else echo '<span class="label label-info" style="font-size:15pt;">No se ha obtenido resultados</span>';
            }
            //Yii::$app->response->redirect(array('tramite-pago/index'));
        }

        public function actionAgrega_factura_lista(){
            if(Yii::$app->request->isAjax){
                 $factura  = Yii::$app->request->post('factura');
                 $idProveedor  = Yii::$app->request->post('idProveedor');
                 $estado_pago = 'Proceso';
                 $factura_compra = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere('numFactura LIKE :numFactura',[':numFactura'=>'%'.$factura])->one();
                 if ($factura_compra) {
                     if($factura_compra->idProveedor == $idProveedor && $factura_compra->estado_pago == 'Pendiente'){
                        $factura_compra->estado_pago = $estado_pago;
                        $factura_compra->save();
                        echo '<span class="label label-success" style="font-size:15pt;">Factura agregada</span>';
                     } else {
                        echo '<span class="label label-info" style="font-size:15pt;">Número de factura no corresponde al proveedor</span>';
                     }
                 } else {
                    echo '<span class="label label-info" style="font-size:15pt;">Factura no encontrada</span>';
                 }
            }
            //Yii::$app->response->redirect(array('tramite-pago/index'));
        }

        //esta accion me permite llamar la tabla de facturas pendientes luego de ingresar una factura a facturas
        //en proceso de forma individual
        public function actionTbl_uno()
        {
            $idProveedor  = $_GET['idProveedor'];
            $conditionfecha  = $_GET['conditionfecha'];
            return $this->renderAjax('tbl_uno', ['idProveedor' => $idProveedor, 'conditionfecha' => $conditionfecha ]);
        }

        //esta accion me permite llamar la tabla de facturas en proseso luego de ingresar una factura a facturas
        //en proceso de forma individual
        public function actionTbl_dos()
        {
            $idProveedor  = $_GET['idProveedor'];
            return $this->renderAjax('tbl_dos', ['idProveedor' => $idProveedor ]);
        }

        //esta accion me obtiene los movimientos de notas de credito, debito y abono
        //correspondiente a la factura seleccionada
        public function actionMovimientos()
        {
            $idCompra  = $_GET['id'];
            $div  = $_GET['div'];
            $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
            return $this->renderAjax('movimientos',['factura_compra'=>$factura_compra, 'div'=>$div]);
        }

        //agrega facturas a cancelar solo con precionar un boton, segun la fecha
        public function actionMov_facturafecha()
        {
            if(Yii::$app->request->isAjax){
                $idProveedor  = Yii::$app->request->post('idProveedor');
                $conditionfecha  = Yii::$app->request->post('conditionfecha');

                $facturasCredito = '';
                $notificacion = '';

                if ($conditionfecha == 'hoy') {
                    $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago AND fechaVencimiento = :tipofecha", [":estado_pago"=>'Pendiente', ':tipofecha' => date('Y-m-d')])->orderBy(['idCompra' => SORT_DESC])->all();
                    $notificacion = $facturasCredito ? '<span class="label label-success" style="font-size:15pt;">Facturas vencidas de hoy agregadas</span>' : '<span class="label label-info" style="font-size:15pt;">No se presentan facturas</span>';
                }
                else if ($conditionfecha == 'vencidas') {
                    $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago AND fechaVencimiento < :tipofecha", [":estado_pago"=>'Pendiente', ':tipofecha' => date('Y-m-d')])->orderBy(['idCompra' => SORT_DESC])->all();
                    $notificacion = $facturasCredito ? '<span class="label label-success" style="font-size:15pt;">Facturas con +1 de vencimiento agregadas</span>' : '<span class="label label-info" style="font-size:15pt;">No se presentan facturas</span>';
                }
                else if ($conditionfecha == 'porvencer') {
                    $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago AND
                        (fechaVencimiento = :tipofecha + INTERVAL 5 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 4 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 3 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 2 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 1 DAY)", [":estado_pago"=>'Pendiente', ':tipofecha' => date('Y-m-d')])->orderBy(['idCompra' => SORT_DESC])->all();
                        $notificacion = $facturasCredito ? '<span class="label label-success" style="font-size:15pt;">Facturas por vencer agregadas</span>' : '<span class="label label-info" style="font-size:15pt;">No se presentan facturas</span>';
                }
                    if ($facturasCredito != '') {
                    foreach ($facturasCredito as $factura_compra => $factura) {
                        $factura_compra = ComprasInventario::find()->where(['=','numFactura', $factura['numFactura']])->andWhere('idProveedor = :idProveedor',[':idProveedor'=>$idProveedor])->one();
                        if ($factura_compra->estado_pago = 'Pendiente') {
                            $factura_compra->estado_pago = 'Proceso';
                            $factura_compra->save();
                        }

                    }
                }

                echo $notificacion;
            }
        }

//--------------------------------------------------------------------------------------------------------
//----------------------------------------------area de movimientos---------------------------------------
//--------------------------------------------------------------------------------------------------------
        //crea una nueva nota de credito
        public function actionAgrega_nota_credito()
        {
            if(Yii::$app->request->isAjax){
                $bandera = '';
                $estado_pago = '';
                $detalle_nc  = Yii::$app->request->post('detalle_nc');
                $monto_nc  = floatval(str_replace(",", "", Yii::$app->request->post('monto_nc')));
                $numero_nc  = intval(Yii::$app->request->post('numero_nc'));
                $idCompra = Yii::$app->request->post('idCompra');

                $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
                $proveedor = Proveedores::find()->where(['=','codProveedores', $factura_compra->idProveedor])->one();
                $monto_de_la_factura = floatval($factura_compra->total); //extraigo el total a pagar de la compra
                $saldo_de_la_factura = floatval($factura_compra->credi_saldo); //extraigo el saldd de la compra

                if ( $saldo_de_la_factura >= $monto_nc ) {
                    //$command = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov != 'nd' AND idCompra = " . "'".$idCompra."'");
                    //$suma_monto_movimiento = floatval($command->queryScalar()); //ingreso la suma del monto en una variable

                    $monto_anterior = $saldo_de_la_factura;//$monto_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                    $saldo_pendiente = $monto_anterior - $monto_nc; //le resto el monto de la nota de credito con el monto anterior para saber el monto pendiente

                    $movi_pagar = new MovimientoPagar();
                    $movi_pagar->idCompra = $factura_compra->idCompra;
                    $movi_pagar->numFactura = $factura_compra->numFactura;
                    $movi_pagar->fecha_mov = date("Y-m-d");
                    $movi_pagar->tipo_mov = 'nc';
                    $movi_pagar->detalle = $detalle_nc;
                    $movi_pagar->numero_mov = $numero_nc;
                    $movi_pagar->monto_anterior = $monto_anterior;
                    $movi_pagar->monto_movimiento = $monto_nc;
                    $movi_pagar->saldo_pendiente = $saldo_pendiente;
                    $movi_pagar->usuario = Yii::$app->user->identity->username;
                    $movi_pagar->proveedor = $proveedor->nombreEmpresa;

                    if ($movi_pagar->save()) {
                        $factura_compra->credi_saldo = $saldo_pendiente;
                        $factura_compra->estado_pago = $saldo_de_la_factura == $monto_nc ? 'Cancelada' : 'Pendiente';
                        $factura_compra->save();
                        $proveedor->credi_saldo -= $monto_nc;
                        $proveedor->save();
                        $estado_pago = $saldo_de_la_factura == $monto_nc ? 'Cancelada' : 'Pendiente';// me debuelve el estado para saber si necesito quitar la factura de la vista
                        $bandera = '<span class="label label-success" style="font-size:12pt;">Nota de crédito aplicada correctamente</span>';
                    } else {
                        $bandera = '<span class="label label-danger" style="font-size:12pt;">Nota de crédito no aplicada</span>';
                    }
                } else {
                    $bandera = '<span class="label label-danger" style="font-size:12pt;">Error, El móndo de la nota supera el saldo</span>';
                }
                $arr = array('bandera'=>$bandera, 'estado_pago'=>$estado_pago);
                echo json_encode($arr);
            }

        }

        //actualizar nota de credito
        public function actionActualiza_nota_credito()
        {
            if(Yii::$app->request->isAjax){
                $bandera = '';
                $estado_pago = '';
                $idMov  = Yii::$app->request->post('idMov');
                $detalle_nc  = Yii::$app->request->post('detalle_nc');
                $numero_nc  = intval(Yii::$app->request->post('numero_nc'));
                $monto_movi_new  = floatval(str_replace(",", "", Yii::$app->request->post('monto_nc')));
                $idCompra = Yii::$app->request->post('idCompra');

                $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
                $proveedor = Proveedores::find()->where(['=','codProveedores', $factura_compra->idProveedor])->one();
                $movi_pagar = MovimientoPagar::find()->where(['=','idMov', $idMov])->one();
                $saldo_de_la_factura = floatval($factura_compra->credi_saldo); //extraigo el saldo a pagar de la compra

                //comprovamos si el monto de la nota no supera el monto del saldo
                if ( $saldo_de_la_factura >= $monto_movi_new ) {
                    //$monto_ante_old = $movi_pagar->monto_anterior;
                    $monto_movi_old = $movi_pagar->monto_movimiento;
                    $saldo_pend_old = $movi_pagar->saldo_pendiente;

                    //$monto_anterior = ($monto_ante_old + $monto_movi_old) - $monto_movi_new;
                    //$movi_pagar->monto_anterior = $monto_anterior;
                    $movi_pagar->detalle = $detalle_nc;
                    $movi_pagar->numero_mov = $numero_nc;
                    $movi_pagar->monto_movimiento = $monto_movi_new;
                    $movi_pagar->saldo_pendiente = $saldo_pend_old + $monto_movi_old - $monto_movi_new;

                    if ($movi_pagar->save()) {
                        $factura_compra->credi_saldo = ($factura_compra->credi_saldo + $monto_movi_old) - $monto_movi_new;
                        $factura_compra->estado_pago = $saldo_de_la_factura == $monto_movi_new ? 'Cancelada' : 'Pendiente';
                        $factura_compra->save();
                        $proveedor->credi_saldo = ($proveedor->credi_saldo + $monto_movi_old) - $monto_movi_new;
                        $proveedor->save();
                        $estado_pago = $saldo_de_la_factura == $monto_movi_new ? 'Cancelada' : 'Pendiente';// me debuelve el estado para saber si necesito quitar la factura de la vista
                        $bandera = '<span class="label label-success" style="font-size:12pt;">Nota de crédito corregida correctamente</span>';
                    } else {
                        $bandera = '<span class="label label-danger" style="font-size:12pt;">Nota de crédito no aplicada</span>';
                    }
                } else {
                    $bandera = '<span class="label label-danger" style="font-size:12pt;">Error, El móndo de la nota supera el saldo</span>';
                }
                $arr = array('bandera'=>$bandera, 'estado_pago'=>$estado_pago);
                echo json_encode($arr);

            }
        }

        //eliminar una nota de credito
        public function actionElimina_nota_credito()
        {
            if(Yii::$app->request->isAjax){
                $idMov  = Yii::$app->request->post('idMov');
                $idCompra = Yii::$app->request->post('idCompra');
                $monto_movimiento = floatval(Yii::$app->request->post('monto_movimiento'));
                \Yii::$app->db->createCommand()->delete('tbl_movi_pagar', 'idMov = '.(int) $idMov)->execute();
                $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
                $proveedor = Proveedores::find()->where(['=','codProveedores', $factura_compra->idProveedor])->one();
                $factura_compra->credi_saldo += $monto_movimiento;
                $factura_compra->save();
                $proveedor->credi_saldo += $monto_movimiento;
                $proveedor->save();
                echo '<span class="label label-success" style="font-size:12pt;">Nota de crédito eliminada correctamente</span>';
            }
        }

        //llamo la tabla de notas de credito por ajax
        public function actionTbl_nc()
        {
            $idCompra  = $_GET['id'];
            return $this->renderAjax('tbl_nc', ['idCompra' => $idCompra ]);
        }

        //imorimir notas de credito
        public function actionImprimir_movimientos($idCompra, $tipo_mov)
        {
            $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();

            $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
            $content = '
            <div class="panel panel-default">
                <div class="panel-body">
                    <table>
                        <tbody>
                        <tr>
                        <td>
                        <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                        </td>
                        <td style="text-align: left;">
                        <strong>'.$empresa->nombre.'</strong><br>
                        <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                        <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                        <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                        <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                        <strong>Dirección:</strong> '.$empresa->email.'
                        </td>
                        <tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Factura N° (Doc.Ref): '.$factura_compra->numFactura. '<br>Fecha: '.date("d-m-Y").'</h4>';

            $content .= $this->renderAjax('imprimir_movimientos',['factura_compra' => $factura_compra, 'tipo_mov' => $tipo_mov]);

            $imprimir = '
                <!DOCTYPE html>
                <html>
                <head>
                    <meta charset="UTF-8">
                    <title>Movimiento</title>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                    <script type="text/javascript">
                        function imprimir() {
                            var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";

                            if (window.print) {
                                window.print();
                                setTimeout("self.close()", 50 );
                            } else {
                                alert("La función de impresion no esta soportada por su navegador.");
                            }
                        }
                    </script>
                    <style type="text/css">
                        footer{
                          display: none;
                        }
                    </style>
                </head>
                <body onload="imprimir();">

                '.$content.'
                </body>

                </html>';

            echo $imprimir;

        }

//------------------------------------------------------------------------------------------------
        //llamo la tabla de notas de DEBITO por ajax
        public function actionTbl_nd()
        {
            $idCompra  = $_GET['id'];
            return $this->renderAjax('tbl_nd', ['idCompra' => $idCompra ]);
        }

        //crea una nueva nota de credito
        public function actionAgrega_nota_debito()
        {
            if(Yii::$app->request->isAjax){
                $bandera = '';
                $detalle_nd  = Yii::$app->request->post('detalle_nd');
                $numero_nd  = intval(Yii::$app->request->post('numero_nd'));
                $monto_nd  = floatval(str_replace(",", "", Yii::$app->request->post('monto_nd')));
                $idCompra = Yii::$app->request->post('idCompra');

                $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
                $proveedor = Proveedores::find()->where(['=','codProveedores', $factura_compra->idProveedor])->one();
                $monto_de_la_factura = floatval($factura_compra->total); //extraigo el total a pagar de la compra
                $saldo_de_la_factura = floatval($factura_compra->credi_saldo); //extraigo el saldd de la compra

                    //$command = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND numFactura = " . "'".$numerofactura."'");
                    //$suma_monto_movimiento = floatval($command->queryScalar()); //ingreso la suma del monto en una variable

                    $monto_anterior = $saldo_de_la_factura;// - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                    $saldo_pendiente = $monto_anterior + $monto_nd; //le resto el monto de la nota de credito con el monto anterior para saber el monto pendiente

                    $movi_pagar = new MovimientoPagar();
                    $movi_pagar->idCompra = $factura_compra->idCompra;
                    $movi_pagar->numFactura = $factura_compra->numFactura;
                    $movi_pagar->fecha_mov = date("Y-m-d");
                    $movi_pagar->tipo_mov = 'nd';
                    $movi_pagar->detalle = $detalle_nd;
                    $movi_pagar->numero_mov = $numero_nd;
                    $movi_pagar->monto_anterior = $monto_anterior;
                    $movi_pagar->monto_movimiento = $monto_nd;
                    $movi_pagar->saldo_pendiente = $saldo_pendiente;
                    $movi_pagar->usuario = Yii::$app->user->identity->username;
                    $movi_pagar->proveedor = $proveedor->nombreEmpresa;

                    if ($movi_pagar->save()) {
                        $factura_compra->credi_saldo = $saldo_pendiente;
                        $factura_compra->save();
                        $proveedor->credi_saldo += $monto_nd;
                        $proveedor->save();
                        $bandera = '<span class="label label-success" style="font-size:12pt;">Nota de débito aplicada correctamente</span>';
                    } else {
                        $bandera = '<span class="label label-danger" style="font-size:12pt;">Nota de débito no aplicada</span>';
                    }

                echo $bandera;
            }

        }//fin nota de debito

        //actualizar nota de debito
        public function actionActualiza_nota_debito()
        {
            if(Yii::$app->request->isAjax){
                $bandera = '';
                $idMov  = Yii::$app->request->post('idMov');
                $detalle_nd  = Yii::$app->request->post('detalle_nd');
                $monto_movi_new  = floatval(Yii::$app->request->post('monto_nd'));
                $numero_nd = intval(Yii::$app->request->post('numero_nd'));
                $idCompra = Yii::$app->request->post('idCompra');

                $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
                $proveedor = Proveedores::find()->where(['=','codProveedores', $factura_compra->idProveedor])->one();
                $movi_pagar = MovimientoPagar::find()->where(['=','idMov', $idMov])->one();
                $saldo_de_la_factura = floatval($factura_compra->credi_saldo); //extraigo el saldo a pagar de la compra

                    //$monto_ante_old = $movi_pagar->monto_anterior;
                    $monto_movi_old = $movi_pagar->monto_movimiento;
                    $saldo_pend_old = $movi_pagar->saldo_pendiente;

                    //$monto_anterior = ($monto_ante_old + $monto_movi_old) - $monto_movi_new;
                    //$movi_pagar->monto_anterior = $monto_anterior;
                    $movi_pagar->detalle = $detalle_nd;
                    $movi_pagar->monto_movimiento = $monto_movi_new;
                    $movi_pagar->numero_mov = $numero_nd;
                    $movi_pagar->saldo_pendiente = $saldo_pend_old - $monto_movi_old + $monto_movi_new;

                    if ($movi_pagar->save()) {
                        $factura_compra->credi_saldo = ($factura_compra->credi_saldo - $monto_movi_old) + $monto_movi_new;
                        $factura_compra->save();
                        $proveedor->credi_saldo = ($proveedor->credi_saldo - $monto_movi_old) + $monto_movi_new;
                        $proveedor->save();
                        $bandera = '<span class="label label-success" style="font-size:12pt;">Nota de débito corregida correctamente</span>';
                    } else {
                        $bandera = '<span class="label label-danger" style="font-size:12pt;">Nota de débito no aplicada</span>';
                    }
                echo $bandera;

            }
        }//fin nota de debito

        //eliminar una nota de debito
        public function actionElimina_nota_debito()
        {
            if(Yii::$app->request->isAjax){
                $idMov  = Yii::$app->request->post('idMov');
                $idCompra = Yii::$app->request->post('idCompra');
                $monto_movimiento = floatval(Yii::$app->request->post('monto_movimiento'));
                \Yii::$app->db->createCommand()->delete('tbl_movi_pagar', 'idMov = '.(int) $idMov)->execute();
                $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
                $proveedor = Proveedores::find()->where(['=','codProveedores', $factura_compra->idProveedor])->one();
                $factura_compra->credi_saldo -= $monto_movimiento;
                $factura_compra->save();
                $proveedor->credi_saldo -= $monto_movimiento;
                $proveedor->save();
                echo '<span class="label label-success" style="font-size:12pt;">Nota de débito eliminada correctamente</span>';
            }
        }

        //------------------------------------------------------------------------------------------------
        //llamo la tabla ABONO por ajax
        public function actionTbl_ab()
        {
            $idCompra  = $_GET['id'];
            return $this->renderAjax('tbl_ab', ['idCompra' => $idCompra ]);
        }

        //crea una nueva nota de credito
        public function actionAgrega_abono()
        {
            if(Yii::$app->request->isAjax){
                $bandera = '';
                $detalle_ab  = Yii::$app->request->post('detalle_ab');
                $numero_ab = intval(Yii::$app->request->post('numero_ab'));
                $monto_ab  = floatval(str_replace(",", "", Yii::$app->request->post('monto_ab')));
                $idCompra = Yii::$app->request->post('idCompra');

                $factura_compra = ComprasInventario::find()->where(['=','idCompra', $idCompra])->one();
                $proveedor = Proveedores::find()->where(['=','codProveedores', $factura_compra->idProveedor])->one();
                $monto_de_la_factura = floatval($factura_compra->total); //extraigo el total a pagar de la compra
                $saldo_de_la_factura = floatval($factura_compra->credi_saldo); //extraigo el saldd de la compra

                if ( $saldo_de_la_factura > $monto_ab ) {
                    //$command = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov != 'nd' AND numFactura = " . $numerofactura);
                    //$suma_monto_movimiento = floatval($command->queryScalar()); //ingreso la suma del monto en una variable

                    $monto_anterior = $saldo_de_la_factura;//$monto_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                    $saldo_pendiente = $monto_anterior - $monto_ab; //le resto el monto de la nota de credito con el monto anterior para saber el monto pendiente

                    $movi_pagar = new MovimientoPagar();
                    $movi_pagar->idCompra = $factura_compra->idCompra;
                    $movi_pagar->numFactura = $factura_compra->numFactura;
                    $movi_pagar->fecha_mov = date("Y-m-d");
                    $movi_pagar->tipo_mov = 'ab';
                    $movi_pagar->numero_mov = $numero_ab;
                    $movi_pagar->detalle = $detalle_ab;
                    $movi_pagar->monto_anterior = $monto_anterior;
                    $movi_pagar->monto_movimiento = $monto_ab;
                    $movi_pagar->saldo_pendiente = $saldo_pendiente;
                    $movi_pagar->usuario = Yii::$app->user->identity->username;
                    $movi_pagar->proveedor = $proveedor->nombreEmpresa;

                    if ($movi_pagar->save()) {
                        $factura_compra->credi_saldo = $saldo_pendiente;
                        $factura_compra->save();
                        $proveedor->credi_saldo -= $monto_ab;
                        $proveedor->save();
                        $bandera = '<span class="label label-success" style="font-size:12pt;">Abono aplicado correctamente</span>';
                    } else {
                        $bandera = '<span class="label label-danger" style="font-size:12pt;">Abono no aplicado</span>';
                    }
                } else {
                    $bandera = '<span class="label label-danger" style="font-size:12pt;">Error, El monto es mayor o igual al saldo, <br> preferiblemente proceda hacer <br> un trámite de cancelación</span>';
                }
                echo $bandera;
            }

        }//fin agrega abono

//------------------------------------------------------------------------------------------
//----------------area de generar pagos y notras de credito al tramite-----------------------------------------------------
//------------------------------------------------------------------------------------------
        //obtengo la vista de generar pagos
        public function actionGenerar_pago()
        {
            $idProveedor  = $_GET['idProveedor'];
            return $this->renderAjax('generar_pago', ['idProveedor' => $idProveedor ]);
        }

        //crea una nueva nota de credito
        public function actionAgrega_nota_credito_tramite()
        {
            if(Yii::$app->request->isAjax){
                $bandera = '';
                $detalle_nc  = Yii::$app->request->post('detalle_nc');
                $monto_credito  = Yii::$app->request->post('monto_credito');
                $monto_debito  = Yii::$app->request->post('monto_debito');
                $monto_nc  = floatval(Yii::$app->request->post('monto_nc'));
                $codProveedores = Yii::$app->request->post('codProveedores');
                $total_monto_documento = floatval(Yii::$app->request->post('total_monto_documento'));
                $porcen_decuento = floatval(str_replace(",", "", Yii::$app->request->post('porcen_decuento')));
                $monto_tramite_pagar = floatval(str_replace(",", "", Yii::$app->request->post('monto_tramite_pagar')));//quitamos las comas

                $proveedor = Proveedores::find()->where(['=','codProveedores', $codProveedores])->one();

                if ( $monto_tramite_pagar >= $monto_nc ) {

                    $movi_pagar = new MovimientoPagar();
                    $movi_pagar->numFactura = "";
                    $movi_pagar->idTramite_pago = 0;
                    $movi_pagar->fecha_mov = date("Y-m-d");
                    $movi_pagar->tipo_mov = 'nc';
                    $movi_pagar->detalle = $detalle_nc;
                    $movi_pagar->monto_movimiento = $monto_nc;
                    $movi_pagar->usuario = Yii::$app->user->identity->username;
                    $movi_pagar->proveedor = $proveedor->nombreEmpresa;

                    if ($movi_pagar->save()) {
                        $bandera = '<span class="label label-success" style="font-size:12pt;">Nota de crédito agregada</span>';
                    } else {
                        $bandera = '<span class="label label-danger" style="font-size:12pt;">Nota de crédito no aplicada</span>';
                    }
                } else {
                    $bandera = '<span class="label label-danger" style="font-size:12pt;">Error, Este móndo supera el saldo</span>';
                }
                //obtengo instancia de Movimiento a pagar para saber cuantas notas de credito sin asignacion se han hecho para este proveedor
                $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$proveedor->nombreEmpresa])
                ->andWhere("tipo_mov = :tipo_mov AND numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
                    [":tipo_mov"=>'nc', ":numFactura"=>"", ":idTramite_pago"=>0])->orderBy(['idMov' => SORT_DESC])->all();
                $cantidad_nc_tramite = 0;
                $montototal_nc = 0;
                $suma_saldo_y_nc = 0;
                $monto_descuento = 0;
                if ($movi_pagar) {
                    foreach ($movi_pagar as $notacredito) {
                        $cantidad_nc_tramite +=1;
                        $montototal_nc += $notacredito['monto_movimiento'];
                    }
                    $descuento = $porcen_decuento*0.01;
                    $subtotal = ($total_monto_documento - $monto_credito) / Yii::$app->params['reverso_impuesto'];//
                    $monto_descuento = $subtotal * $descuento;
                    //$monto_descuento = ($suma_saldo_proceso - $montototal_nc) * $descuento;

                    $suma_saldo_y_nc_des = ((($total_monto_documento - $montototal_nc) - $monto_descuento) - $monto_credito) + $monto_debito;
                    $suma_saldo_y_nc = ($total_monto_documento - $monto_credito) + $monto_debito;
                }
                $arr = array('bandera'=>$bandera, 'cantidad_nc_tramite'=>$cantidad_nc_tramite,
                    'suma_saldo_y_nc' => $suma_saldo_y_nc, 'monto_descuento' => number_format($monto_descuento,2),
                    'suma_saldo_y_nc2' => number_format($suma_saldo_y_nc_des,2));
                echo json_encode($arr);
            }

        } //fin agrega_nota_credito_tramite

        //actualizar nota de credito
        public function actionActualiza_nota_credito_tramite()
        {
            if(Yii::$app->request->isAjax){
                $bandera = '';
                $idMov  = Yii::$app->request->post('idMov');
                $detalle_nc  = Yii::$app->request->post('detalle_nc');
                $monto_credito  = Yii::$app->request->post('monto_credito');
                $monto_debito  = Yii::$app->request->post('monto_debito');
                $monto_movi_new  = floatval(Yii::$app->request->post('monto_nc'));//nuevo monto del movimiento nc
                $codProveedores = Yii::$app->request->post('codProveedores');
                $porcen_decuento = floatval(str_replace(",", "", Yii::$app->request->post('porcen_decuento')));
                $total_monto_documento = floatval(Yii::$app->request->post('total_monto_documento'));
                $movi_pagar = MovimientoPagar::find()->where(['=','idMov', $idMov])->one();

                $monto_descuento = 0;
                $suma_saldo_y_nc = 0;

                    //$monto_anterior = ($monto_ante_old + $monto_movi_old) - $monto_movi_new;
                    //$movi_pagar->monto_anterior = $monto_anterior;
                    $movi_pagar->detalle = $detalle_nc;
                    $movi_pagar->monto_movimiento = $monto_movi_new;

                    if ($movi_pagar->save()) {
                        $proveedor = Proveedores::find()->where(['=','codProveedores', $codProveedores])->one();
                        //obtengo instancia de Movimiento a pagar para saber cuantas notas de credito sin asignacion se han hecho para este proveedor
                        $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$proveedor->nombreEmpresa])
                        ->andWhere("tipo_mov = :tipo_mov AND numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
                            [":tipo_mov"=>'nc', ":numFactura"=>"", ":idTramite_pago"=>0])->orderBy(['idMov' => SORT_DESC])->all();

                        $montototal_nc = 0;
                        if ($movi_pagar) {
                            foreach ($movi_pagar as $notacredito) {
                                $montototal_nc += $notacredito['monto_movimiento'];
                            }
                            $descuento = $porcen_decuento*0.01;
                            $subtotal = ($total_monto_documento - $monto_credito) / Yii::$app->params['reverso_impuesto'];//
                            $monto_descuento = $subtotal * $descuento;
                            //$monto_descuento = ($suma_saldo_proceso - $montototal_nc) * $descuento;

                            $suma_saldo_y_nc_des = ((($total_monto_documento - $montototal_nc) - $monto_descuento) - $monto_credito) + $monto_debito;
                            $suma_saldo_y_nc = ($total_monto_documento - $monto_credito) + $monto_debito;
                        }

                        $bandera = '<span class="label label-success" style="font-size:12pt;">Nota de crédito corregida</span>';
                    } else {
                        $bandera = '<span class="label label-danger" style="font-size:12pt;">Nota de crédito no aplicada</span>';
                    }
                $arr = array('bandera'=>$bandera,
                    'suma_saldo_y_nc' => $suma_saldo_y_nc, 'monto_descuento' => number_format($monto_descuento,2), 'suma_saldo_y_nc2' => number_format($suma_saldo_y_nc_des,2));
                echo json_encode($arr);

            }
        }

        //eliminar una nota de credito para tramite
        public function actionElimina_nota_credito_tramite()
        {
            if(Yii::$app->request->isAjax){
                $idMov  = Yii::$app->request->post('idMov');
                $idProveedor  = Yii::$app->request->post('idProveedor');
                $monto_credito  = Yii::$app->request->post('monto_credito');
                $monto_debito  = Yii::$app->request->post('monto_debito');
                $porcen_decuento = floatval(str_replace(",", "", Yii::$app->request->post('porcen_decuento')));
                $total_monto_documento = floatval(Yii::$app->request->post('total_monto_documento'));
                \Yii::$app->db->createCommand()->delete('tbl_movi_pagar', 'idMov = '.(int) $idMov)->execute();
                //obtengo instancia de Movimiento a pagar para saber cuantas notas de credito sin asignacion se han hecho para este proveedor
                $proveedor = Proveedores::find()->where(['codProveedores'=>$idProveedor])->one();
                $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$proveedor->nombreEmpresa])
                ->andWhere("tipo_mov = :tipo_mov AND numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
                    [":tipo_mov"=>'nc', ":numFactura"=>"", ":idTramite_pago"=>0])->orderBy(['idMov' => SORT_DESC])->all();
                $cantidad_nc_tramite = 0;
                $montototal_nc = 0;
                //$suma_saldo_y_nc = 0;
                //$monto_descuento = 0;
                if ($movi_pagar) {
                    foreach ($movi_pagar as $notacredito) {
                        $cantidad_nc_tramite +=1;
                        $montototal_nc += $notacredito['monto_movimiento'];
                    }
                }
                $descuento = $porcen_decuento*0.01;
                $subtotal = ($total_monto_documento - $monto_credito) / Yii::$app->params['reverso_impuesto'];//
                $monto_descuento = $subtotal * $descuento;
                //$monto_descuento = ($suma_saldo_proceso - $montototal_nc) * $descuento;

                $suma_saldo_y_nc_des = ((($total_monto_documento - $montototal_nc) - $monto_descuento) - $monto_credito) + $monto_debito;
                $suma_saldo_y_nc = ($total_monto_documento - $monto_credito) + $monto_debito;
                $bandera = '<span class="label label-success" style="font-size:12pt;">Nota de crédito eliminada</span>';
                $arr = array('bandera'=>$bandera, 'cantidad_nc_tramite'=>$cantidad_nc_tramite,
                'monto_descuento' => number_format($monto_descuento,2),
                'suma_saldo_y_nc' => $suma_saldo_y_nc, 'suma_saldo_y_nc2' => number_format($suma_saldo_y_nc_des,2));
                echo json_encode($arr);

            }
        }

        //consulta solo nc de tramite
        public function actionNc_tramite($nombreEmpresa)
        {
          $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$nombreEmpresa])
          ->andWhere("numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
          	[":numFactura"=>"", ":idTramite_pago"=>0])->all();
            $montototal_nc = 0;
            if ($movi_pagar) {
            	foreach ($movi_pagar as $movimiento) {
        				if ($movimiento->tipo_mov == 'nd') {
        					$montototal_nd += $movimiento['monto_movimiento'];
        				} else {
        	    		$montototal_nc += $movimiento['monto_movimiento'];
        				}
            	}
              return $montototal_nc;
            } else {
              return 0;
            }
        }

        //llamo la tabla de notas de credito por tramite desde ajax
        public function actionTbl_nc_tramite()
        {
            $codProveedores  = $_GET['codProveedores'];
            return $this->renderAjax('tbl_nc_tramite', ['codProveedores' => $codProveedores ]);
        }

        //obtengo el valor de monto descuento y monto a pagar para darle formato decimal (.00)
        public function actionMontopagar()
        {
            if(Yii::$app->request->isAjax){
                $monto_descuento = Yii::$app->request->post('monto_descuento');
                $monto_pagar = floatval(Yii::$app->request->post('monto_pagar'));//monto a pagar con impuestos
                $monto_credito = floatval(Yii::$app->request->post('monto_credito'));
                $monto_debito = floatval(Yii::$app->request->post('monto_debito'));
                $codProveedores = Yii::$app->request->post('idProveedor');
                $proveedor = Proveedores::find()->where(['=','codProveedores', $codProveedores])->one();
                //obtenemos las notas de credito al monto del tramite a pagar
                $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$proveedor->nombreEmpresa])
                ->andWhere("tipo_mov = :tipo_mov AND numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
                    [":tipo_mov"=>'nc', ":numFactura"=>"", ":idTramite_pago"=>0])->orderBy(['idMov' => SORT_DESC])->all();
                $montototal_nc = 0;
                if ($movi_pagar) {
                  foreach ($movi_pagar as $notacredito) {
                    $montototal_nc += $notacredito['monto_movimiento'];
                  }
                }
                $monto_total_pagar = $monto_pagar - $montototal_nc + $monto_debito;
                $arr = array('monto_descuento'=>number_format(floatval($monto_descuento),2), 'monto_total_pagar'=>number_format($monto_total_pagar,2));
                echo json_encode($arr);
            }
        }

        //funcion que comprueba que los numeros de cheques no sean repetidos para el mismo banco
        public function actionValida_numero_cheque()
        {
            if(Yii::$app->request->isAjax){
                $numero_cheque = Yii::$app->request->post('numero_cheque');
                $id_cuenta_bancaria_lo_ch = (int)Yii::$app->request->post('id_cuenta_bancaria_lo_ch');
                $pasa = 'si';

                $cuenta_escrita = CuentasBancarias::find()->where(['idBancos'=>$id_cuenta_bancaria_lo_ch])->one();

                $medio_pago_pro = MedioPagoProveedor::find()->where(['medio_pago'=>'cheque'])->all();

                foreach ($medio_pago_pro as $key => $pago) {
                    $cuenta_consultada = CuentasBancarias::find()->where(['idBancos'=>$pago['cuenta_bancaria_local']])->one();
                    if ($pago['numero_cheque'] == $numero_cheque /*&& $pago['cuenta_bancaria_local'] == $id_cuenta_bancaria_lo_ch*/&& $cuenta_consultada->idEntidad_financiera == $cuenta_escrita->idEntidad_financiera) {
                        $pasa = 'no';
                    }
                }
                echo $pasa;

            }
        }
        //funcion que comprueba que los numeros de transferencia no sean repetidos para el mismo banco
        public function actionValida_numero_comprovante_trans()
        {
            if(Yii::$app->request->isAjax){
                $numero_comprovante = Yii::$app->request->post('numero_comprovante');
                $id_cuenta_bancaria_lo_tb = (int)Yii::$app->request->post('id_cuenta_bancaria_lo_tb');
                $pasa = 'si';

                $cuenta_escrita = CuentasBancarias::find()->where(['idBancos'=>$id_cuenta_bancaria_lo_tb])->one();

                $medio_pago_pro = MedioPagoProveedor::find()->where(['medio_pago'=>'transferencia'])->all();

                foreach ($medio_pago_pro as $key => $pago) {
                    $cuenta_consultada = CuentasBancarias::find()->where(['idBancos'=>$pago['cuenta_bancaria_local']])->one();
                    if ($pago['numero_recibo_cancelacion'] == $numero_comprovante /*&& $pago['cuenta_bancaria_local'] == $id_cuenta_bancaria_lo_tb*/&& $cuenta_consultada->idEntidad_financiera == $cuenta_escrita->idEntidad_financiera) {
                        $pasa = 'no';
                    }
                    $movilibros = MovimientoLibros::find()->where(['idCuenta_bancaria'=>$cuenta_consultada->idBancos])->andWhere("numero_documento = :numero_documento", [":numero_documento"=>$numero_comprovante])->one();
                    if (@$movilibros) {//...po lo que notifico imprimiendo repetido
                        $pasa = 'no';
                    }
                    if (@$idTramite_pago = DebitoSession::getTramiteupdate()) {
                        $mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
                        if ( $numero_comprovante == $mediopago->numero_recibo_cancelacion ) {
                          $pasa = 'si';
                        }
                    }
                }
                echo $pasa;

            }
        }

        //funcion que completa el proceso de pago de facturas pendientes
        public function actionCompletar_pago()
        {
            if(Yii::$app->request->isAjax){
                $model = new TramitePago();//habro una instancia de tramite de pago
                $model_mediopago = new MedioPagoProveedor();//habro una instancia de medio pago de proveedor
                $model->idProveedor = Yii::$app->request->post('idProveedor');
                $model->cantidad_monto_documento = Yii::$app->request->post('cantidad_monto_documento');
                $model->monto_saldo_pago = Yii::$app->request->post('monto_saldo_pago');
                $model->porcentaje_descuento = Yii::$app->request->post('porcentaje_descuento');
                $model->monto_tramite_pagar = floatval(str_replace(",", "", Yii::$app->request->post('monto_tramite_pagar')));//quitamos las comas
                $model->recibo_cancelacion = '';
                $idContacto_pago = 0;
                $tipo_pago = Yii::$app->request->post('tipo_pago');
                if ($tipo_pago == 'efectivo') {//obtengo los datos de pago para tipo efectivo
                    $idContacto_pago = Yii::$app->request->post('idContacto_pago_ef');
                    $model->usuario_cancela = Yii::$app->user->identity->username;
                    $model->fecha_cancela = date("Y-m-d H:i:s");
                    $model->usuario_aplica = Yii::$app->user->identity->username;
                    $model->fecha_aplica = date("Y-m-d H:i:s");
                    $model->estado_tramite = 'Cancelado';
                }else if ($tipo_pago == 'cheque') {//obtengo los datos de pago para tipo cancelado
                    $idContacto_pago = Yii::$app->request->post('idContacto_pago_ch');
                    $model->usuario_cancela = Yii::$app->user->identity->username;
                    $model->fecha_cancela = date("Y-m-d H:i:s");
                    $model->usuario_aplica = Yii::$app->user->identity->username;
                    $model->fecha_aplica = date("Y-m-d H:i:s");
                    $model->estado_tramite = 'Cancelado';
                } else if ($tipo_pago == 'transferencia'){//obtengo los datos de pago para tipo transferencia
                    $model->estado_tramite = 'Pendiente';
                    $idContacto_pago = Yii::$app->request->post('idContacto_pago_tb');
                }
                //habro una instancia de los contactos pago para obtener el email del contacto
                $contacto_pago = ContactosPagos::find()->where(['=','idContacto_pago', $idContacto_pago])->one();
                //si no hay contacto de pago no se usa email
                $model->email_proveedor = $idContacto_pago != 0 ? $contacto_pago->email : '';
                //Si no viene prioridad le asignamos una predeterminada
                $model->prioridad_pago = Yii::$app->request->post('prioridad') == '' ? 'Media' : Yii::$app->request->post('prioridad');
                $model->usuario_registra = Yii::$app->user->identity->username;
                $model->fecha_registra = date("Y-m-d H:i:s");
                //si no viene con detalle asignamos unos predeterminado
                $model->detalle = Yii::$app->request->post('detalle') == '' ? 'Sin detalle' : Yii::$app->request->post('detalle');

                    //guardamos el tramite de pago
                    if ($model->save()) {//si esto sucede abrimos una intancia de las facturas por compra que en ese momento estan circulando en el tramite
                        $facturas_proceso = ComprasInventario::find()->where(['=','idProveedor', $model->idProveedor])->andWhere("estado_pago = :estado_pago", [":estado_pago"=>'Proceso'])->orderBy(['idCompra' => SORT_DESC])->all();
                        //asi que si el tramite se hizo en efectivo...


                        $proveedor = Proveedores::find()->where(['codProveedores'=>$model->idProveedor])->one();
                        $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$proveedor->nombreEmpresa])
                        ->andWhere("tipo_mov = :tipo_mov AND numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
                        [":tipo_mov"=>'nc', ":numFactura"=>"", ":idTramite_pago"=>0])->orderBy(['idMov' => SORT_DESC])->all();
                        //Si hay notas de creditos asignarle las notas que se le aplico a la transferencia
                        if (@$movi_pagar) {
                                foreach ($movi_pagar as $tramite => $mov) {
                                $tramite = MovimientoPagar::find()->where(['=','idMov', $mov['idMov']])->one();
                                $tramite->idTramite_pago = $model->idTramite_pago;
                                $tramite->save();
                            }
                        }

                    //si el medio de pago es efectivo...
                    if ($tipo_pago=='efectivo') {
                        $model_mediopago->idTramite_pago = $model->idTramite_pago;
                        $model_mediopago->medio_pago = $tipo_pago;
                        //$model_mediopago->idTipo_documento = 1;
                        $model_mediopago->numero_recibo_cancelacion = Yii::$app->request->post('numero_recibo_cancelacion_ef');
                        $model_mediopago->nombre_agente = Yii::$app->request->post('nombre_agente_ef');
                        $model_mediopago->idContacto_pago = $contacto_pago->idContacto_pago;
                        //...cambiaremos el estado_pago de las facturas por compra a "Cancelada" y le asignamos su respectivo tramite de pago
                        foreach ($facturas_proceso as $factura_proces => $value) {
                            $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
                            $factura_proces->estado_pago = 'Cancelada';
                            $factura_proces->idTramite_pago = $model->idTramite_pago;
                            $factura_proces->credi_saldo = 0.00;
                            $factura_proces->save();//guardamos cambios en la factura de compra
                        }
                        //OJO:este se envia por correo
                    } elseif ($tipo_pago=='cheque') {//y si es el tramite se hizo por cheque...
                        $model_mediopago->idTramite_pago = $model->idTramite_pago;
                        $model_mediopago->medio_pago = $tipo_pago;
                        $model_mediopago->idTipo_documento = 1;//al ser 1 el tipo de documento el sistema sabrá que es 'Pago de proveedor'
                        $model_mediopago->numero_cheque = Yii::$app->request->post('numero_cheque');
                        $model_mediopago->numero_recibo_cancelacion = Yii::$app->request->post('numero_recibo_cancelacion_ch');
                        $model_mediopago->nombre_agente = Yii::$app->request->post('nombre_agente_ch');
                        $model_mediopago->idContacto_pago = $contacto_pago->idContacto_pago;
                        $model_mediopago->fecha_documento = date('Y-m-d', strtotime( Yii::$app->request->post('fecha_documento_cheque') ));
                        $model_mediopago->cuenta_bancaria_local = Yii::$app->request->post('id_cuenta_bancaria_lo_ch');
                        //...cambiaremos el estado_pago de las facturas por compra a "Cancelada" y le asignamos su respectivo tramite de pago
                        foreach ($facturas_proceso as $factura_proces => $value) {
                            $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
                            $factura_proces->estado_pago = 'Cancelada';
                            $factura_proces->idTramite_pago = $model->idTramite_pago;
                            $factura_proces->credi_saldo = 0.00;
                            $factura_proces->save();//guardamos cambios en la factura de compra
                        }
                        //tiene que crear movimiento en libros en el instante, accion que se presenta abajo
                        //OJO:este se envia por correo
                    } elseif ($tipo_pago=='transferencia') {//y si el tramite se hizo por transferencia...
                        $model_mediopago->idTramite_pago = $model->idTramite_pago;
                        $model_mediopago->medio_pago = $tipo_pago;
                        $model_mediopago->idTipo_documento = 1;//al ser 1 el tipo de documento el sistema sabrá que es 'Pago de proveedor'
                        $model_mediopago->idContacto_pago = $contacto_pago->idContacto_pago;
                        $model_mediopago->cuenta_bancaria_local = Yii::$app->request->post('id_cuenta_bancaria_lo_tb');
                        //$model_mediopago->numero_recibo_cancelacion = Yii::$app->request->post('numero_comprovante');
                        $model_mediopago->cuenta_bancaria_proveedor = Yii::$app->request->post('id_cuenta_bancaria_pr');
                        //...cambiaremos el estado_pago de las facturas por compra a "Trámite" y le asignamos su respectivo tramite de pago
                        foreach ($facturas_proceso as $factura_proces => $value) {
                            $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
                            $factura_proces->estado_pago = 'Trámite';
                            $factura_proces->idTramite_pago = $model->idTramite_pago;
                            $factura_proces->save();//guardamos cambios en la factura de compra
                        }
                        //se tiene que crear movimiento en libros pero hasta que se confirme o aplique y estado_tramite pase a Cancelada
                        //se debe considerar tambien que el saldo de proveedor se debe modificar y almacenar en el historial por MedioPagoProveedor
                    }
                    //actualizamos el saldo del proveedor
                    if ($tipo_pago!='transferencia') {
                      $proveedor->credi_saldo -= $model->monto_saldo_pago;//actualizamos el saldo del proveedor
                      $model_mediopago->saldo_proveedor = $proveedor->credi_saldo;//almacenamos el saldo del proveedor como historial
                      $proveedor->save();
                    }

                    //cuando tengamos cargado el modelo medio pago de acuerdo al tipo de pago procedemos a guardar
                    if ($model_mediopago->save()) {
                        //Una ves guardado y editado los cambios del tramite enviamos al correo la informacion de pago
                        //siempre y cuando no sea transferencia, ya que esta no se ha cancelado
                        if ($tipo_pago != 'transferencia') {
                            if ($this->enviar_tramite_correo($model->idTramite_pago)=='true') {
                                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Trámite enviado al contacto de pago.');
                            } else {
                                Yii::$app->getSession()->setFlash('info', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Observación!</strong> No se notificó por correo porque no se encontró la dirección del destinatario. Compruebe la dirección de correo o active las notificaciones.');
                            }
                        }
                        //cuando se halla guardado nececitamos hacer una condicion para el medio pago "cheque"
                        //y es para proceder a la creacion del movimiento en libro automatizado
                        if ($tipo_pago=='cheque') {
                            //habro una intancia para saber el nombre de la casa comercial
                            $proveedor = Proveedores::find()->where(['=','codProveedores', $model->idProveedor])->one();
                            //habro una instancia para obtener info de la cuenta bancaria que estoy usando
                            $cuenta = CuentasBancarias::find()->where(['idBancos'=>$model_mediopago->cuenta_bancaria_local])->one();
                            //habro una instancia para obtener el tipo de moneda de la cuenta con la estoy usando
                            $moneda = Moneda::find()->where(['idTipo_moneda'=>$cuenta->idTipo_moneda])->one();
                            //habro una instancia para obtener el tipo de cambio y compararla con la moneda que ya tengo de la cuenta
                            $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();

                            //abrimos una intancia de movimiento libros donde se procese a crear un movimiento automatizado
                            $model_movi_libros = new MovimientoLibros();
                            $model_movi_libros->idCuenta_bancaria = $model_mediopago->cuenta_bancaria_local;
                            $model_movi_libros->idTipoMoneda = $cuenta->idTipo_moneda;
                            $model_movi_libros->idTipodocumento = $model_mediopago->idTipo_documento;
                            $model_movi_libros->idMedio_pago = $model_mediopago->idMedio_pago;
                            $model_movi_libros->numero_documento = $model_mediopago->numero_cheque;
                            $model_movi_libros->fecha_documento = date("Y-m-d");//$model_mediopago->fecha_documento;
                            $model_movi_libros->monto = $model->monto_tramite_pagar;
                            $model_movi_libros->tasa_cambio = null;
                            $model_movi_libros->monto_moneda_local = 0.00;
                            //con esto compruebo si hay que hacer tipo cambio.
                            if ($moneda->monedalocal!='x') {
                                $model_movi_libros->tasa_cambio = $tipocambio->valor_tipo_cambio;
                                $model_movi_libros->monto_moneda_local = $model->monto_tramite_pagar*$tipocambio->valor_tipo_cambio;
                            }
                            $model_movi_libros->beneficiario = $proveedor->nombreEmpresa;
                            $model_movi_libros->detalle = 'PAGO DE FACTURAS CON CHEQUE';
                            $model_movi_libros->fecha_registro = date("Y-m-d");
                            $model_movi_libros->usuario_registra = Yii::$app->user->identity->nombre;
                            //guardamos el movimiento en libros, si esto es asi...
                            if ($model_movi_libros->save()) {
                                //...Debemos modificar el monto de cuentas bancarias correspondiendo al movimiento, en ese caso pago a proveedor
                                //habrimos instancia del tipo de documento para que el sistema entienda fue un pago a proveedor
                                $modeltipodoc = TipoDocumento::find()->where(['idTipo_documento'=>$model_movi_libros->idTipodocumento])->one();
                                ////habrimos instancia para guardar en cuentas bancarias correspondioente al movimiento en libros
                                $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$model_movi_libros->idCuenta_bancaria])->one();
                                //si tipo de movimiento es credito sumele el monto de libros;
                                if ($modeltipodoc->tipo_movimiento == 'Crédito') {
                                    $modelcuentabancaria->saldo_libros += $model_movi_libros->monto;
                                } else //de lo contrario restele
                                {
                                    $modelcuentabancaria->saldo_libros -= $model_movi_libros->monto;
                                }
                                //guardamos actualizacion en cuenta bancaria
                                if ($modelcuentabancaria->save()) {//y modificamos el saldo en libros para el control de saldo por movimiento
                                    $libros = MovimientoLibros::find()->where(['idMovimiento_libro'=>$model_movi_libros->idMovimiento_libro])->one();
                                    $libros->saldo = $modelcuentabancaria->saldo_libros;
                                    $libros->save();
                                }

                            }//fin $model_movi_libros->save
                            else {
                                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> NO se pudo guardar en libros.');
                            }
                        }// fin $tipo_pago=='cheque'

                    }//$model_mediopago->save
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El pago a las facturas se registró correctamente');
                    return $this->redirect(['index']);
                }

            }
        }  //fin funcion completar pago

        //funcion que completa el proceso de pago de facturas pendientes
        public function actionActualizar_pago()
        {
          if(Yii::$app->request->isAjax){
            $idTramite_pago = DebitoSession::getTramiteupdate();
            $model = TramitePago::find()->where(['idTramite_pago'=>$idTramite_pago])->one();//habro una instancia del tramite de pago cargado
            $model_mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite_pago])->one();//habro una instancia del medio pago de proveedor
            $model->idProveedor = Yii::$app->request->post('idProveedor');
            $model->cantidad_monto_documento = Yii::$app->request->post('cantidad_monto_documento');
            $model->monto_saldo_pago = Yii::$app->request->post('monto_saldo_pago');
            $model->porcentaje_descuento = Yii::$app->request->post('porcentaje_descuento');
            $model->detalle_reverso = Yii::$app->request->post('detalle_reverso');
            $model->monto_tramite_pagar = floatval(str_replace(",", "", Yii::$app->request->post('monto_tramite_pagar')));//quitamos las comas
            $model->recibo_cancelacion = '';
            $idContacto_pago = 0;
            $tipo_pago = Yii::$app->request->post('tipo_pago');
            if ($tipo_pago == 'transferencia'){//obtengo los datos de pago para tipo transferencia
                $model->estado_tramite = 'Pendiente';
                $idContacto_pago = Yii::$app->request->post('idContacto_pago_tb');
            }
            //habro una instancia de los contactos pago para obtener el email del contacto
            $contacto_pago = ContactosPagos::find()->where(['=','idContacto_pago', $idContacto_pago])->one();
            //si no hay contacto de pago no se usa email
            $model->email_proveedor = $idContacto_pago != 0 ? $contacto_pago->email : '';
            //Si no viene prioridad le asignamos una predeterminada
            $model->prioridad_pago = Yii::$app->request->post('prioridad') == '' ? 'Media' : Yii::$app->request->post('prioridad');
            $model->usuario_registra = Yii::$app->user->identity->username;
            $model->fecha_registra = date("Y-m-d H:i:s");
            //si no viene con detalle asignamos unos predeterminado
            $model->detalle = Yii::$app->request->post('detalle') == '' ? 'Sin detalle' : Yii::$app->request->post('detalle');

            //guardamos el tramite de pago
            if ($model->save()) {

              $facturas_proceso = ComprasInventario::find()->where(['=','idProveedor', $model->idProveedor])->andWhere("estado_pago = :estado_pago", [":estado_pago"=>'Proceso'])->orderBy(['idCompra' => SORT_DESC])->all();

              $proveedor = Proveedores::find()->where(['codProveedores'=>$model->idProveedor])->one();
              $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$proveedor->nombreEmpresa])
              ->andWhere("tipo_mov = :tipo_mov AND numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
              [":tipo_mov"=>'nc', ":numFactura"=>"", ":idTramite_pago"=>0])->orderBy(['idMov' => SORT_DESC])->all();
              //Si hay notas de creditos asignarle las notas que se le aplico a la transferencia
              if (@$movi_pagar) {
                      foreach ($movi_pagar as $tramite => $mov) {
                      $tramite = MovimientoPagar::find()->where(['=','idMov', $mov['idMov']])->one();
                      $tramite->idTramite_pago = $model->idTramite_pago;
                      $tramite->save();
                  }
              }
              if ($tipo_pago=='transferencia') {//y si el tramite se hizo por transferencia...
                  $model_mediopago->idTramite_pago = $model->idTramite_pago;
                  $model_mediopago->medio_pago = $tipo_pago;
                  $model_mediopago->idTipo_documento = 1;//al ser 1 el tipo de documento el sistema sabrá que es 'Pago de proveedor'
                  $model_mediopago->idContacto_pago = $contacto_pago->idContacto_pago;
                  $model_mediopago->cuenta_bancaria_local = Yii::$app->request->post('id_cuenta_bancaria_lo_tb');
                  //$model_mediopago->numero_recibo_cancelacion = Yii::$app->request->post('numero_comprovante');
                  $model_mediopago->cuenta_bancaria_proveedor = Yii::$app->request->post('id_cuenta_bancaria_pr');
                  //...cambiaremos el estado_pago de las facturas por compra a "Trámite" y le asignamos su respectivo tramite de pago
                  foreach ($facturas_proceso as $factura_proces => $value) {
                      $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
                      $factura_proces->estado_pago = 'Trámite';
                      $factura_proces->idTramite_pago = $model->idTramite_pago;
                      $factura_proces->save();//guardamos cambios en la factura de compra
                  }
                  //se tiene que crear movimiento en libros pero hasta que se confirme o aplique y estado_tramite pase a Cancelada
                  //se debe considerar tambien que el saldo de proveedor se debe modificar y almacenar en el historial por MedioPagoProveedor
              }
              //actualizamos el saldo del proveedor
              /*$proveedor->credi_saldo -= $model->monto_saldo_pago;//actualizamos el saldo del proveedor
              $model_mediopago->saldo_proveedor = $proveedor->credi_saldo;//almacenamos el saldo del proveedor como historial
              $proveedor->save();*/
              //cuando tengamos cargado el modelo medio pago de acuerdo al tipo de pago procedemos a guardar
              if ($model_mediopago->save()) {
                DebitoSession::setTramiteupdate(null);
                DebitoSession::setIDproveedor_tramite_upd(null);
              }//fin if $model_mediopago->save()
            }//fin if $model->save()
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El trámite de pago se modificó correctamente');
            return $this->redirect(['tramite-pago-confirmar/index']);
          }//fin ajax
        }//fin funsion de actualizar tramite

//------------------------------------------------------------------------------------------
//----------------area de generar pagos-----------------------------------------------------
//------------------------------------------------------------------------------------------
    //obtengo la vista de generar pagos
    public function actionListatramite()
        {
            $idProveedor  = $_GET['idProveedor'];
            $prioridad_pago = $_GET['busqueda_prio'];
            $estado_tramite = $_GET['busqueda_est'];
            $fecha_reg_tramite = $_GET['fecha_reg_tramite'] == '' ? '' : date('Y-m-d', strtotime( $_GET['fecha_reg_tramite'] ));
            $factura = $_GET['factura'];
            return $this->renderAjax('listatramite', ['idProveedor' => $idProveedor, 'prioridad_pago' => $prioridad_pago,
                'estado_tramite' => $estado_tramite, 'fecha_reg_tramite' => $fecha_reg_tramite, 'factura' => $factura ]);
        }
        //esta accion me obtiene las facturas correspondiente al tramite
        public function actionObtenerfacturasdetramite()
        {
            $idTramite_pago  = $_GET['idtramite'];
            $tramite = TramitePago::find()->where(['=','idTramite_pago', $idTramite_pago])->one();
            return $this->renderAjax('obtenerfacturasdetramite',['tramite'=>$tramite]);
        }

        //accion que me imprime para hoja normal de carta
    public function actionImprimir($id) {

        $empresaimagen = new Empresa();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();

        $tramite = $this->findModel($id);
        $content = '
        <div class="panel panel-default">
            <div class="panel-body">
                <table>
                    <tbody>
                    <tr>
                    <td>
                    <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                    </td>
                    <td style="text-align: left;">
                    <strong>'.$empresa->nombre.'</strong><br>
                    <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                    <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                    <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                    <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                    <strong>Dirección:</strong> '.$empresa->email.'
                    </td>
                    <tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Trámite N°: '.$id. '<br>Fecha: '.date('d-m-Y h:i:s A', strtotime( $tramite->fecha_registra )).'</h4>';

        $content .= $this->renderAjax('imprimefacturasdetramite',['tramite' => $tramite]);
       /* $content = $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);*/

       $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Trámite</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";

                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';

        echo $imprimir;

    }//fin actionReport

    //Para enviar el tramite al correo desde la accion de envio
    public function actionEnviar_tramite(){
        if(Yii::$app->request->isAjax){

            $id = intval(Yii::$app->request->post('id'));
            if ($this->enviar_tramite_correo($id)=='true') {
                echo '<center><span class="label label-success" style="font-size:15pt;">Trámite enviado al contacto de pago correspondiente</span></center><br>';
            } else{
                echo '<center><span class="label label-warning" style="font-size:15pt;">Disculpe. No se encuentró el correo destinatario. <br> Compruebe la dirección de correo o active las notificaciones.</span></center><br>';
            }
        }//fin if ajax
    }//fin funcion

    //enviar tramite al correo desde una llamada a la funcion
    public function enviar_tramite_correo($id)
    {
        $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $tramite = $this->findModel($id);
            $medio_pago_proveedor = MedioPagoProveedor::find()->where(['=','idTramite_pago', $tramite->idTramite_pago])->one();
            $contactopago = ContactosPagos::find()->where(['=','idContacto_pago', $medio_pago_proveedor->idContacto_pago])->one();

            if (@$contactopago) {
                $proveedor = Proveedores::find()->where(['codProveedores'=>$tramite->idProveedor])->one();
                if (@$contactopago->email && $proveedor->notific_cntc_pagos == 'Si') {

                $email = $contactopago->email;

                $informacion_de_tramite = $this->renderAjax('correofacturasdetramite',['tramite' => $tramite]);
                $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
                $content = '
                <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
                <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
                <br>
                <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
                  <tbody><tr>
                    <td align="center" valign="top">
                      <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                        <tbody><tr>
                            <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                              <table>
                                <tbody><tr>
                                  <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                                    &nbsp;&nbsp;&nbsp;&nbsp;

                                  </td>
                                  <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                                  </td>
                                </tr>
                              </tbody></table>
                            </td>
                        </tr>
                        <tr>
                          <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                           <br>
                            <table>
                                <tbody>
                                <tr>
                                <td>
                                <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                                </td>
                                <td style="text-align: left;">
                                <strong>'.$empresa->nombre.'</strong><br>
                                <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                                <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                                <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                                <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                                <strong>Dirección:</strong> '.$empresa->email.'
                                </td>
                                <tr>
                                </tbody>
                            </table>
                            <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">ENVIO DEL TRAMITE # '.$id.'<br><small> '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>

                            '.$informacion_de_tramite.'


                <br><br><br>

                          </td>
                        </tr>
                        <tr>
                          <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                            Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                          </td>
                        </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody></table>
                <br></div></div><div class="adL">

                </div></div></div> ';
                //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                $subject = "Tramite de pago de ".$empresa->nombre;
                //Enviamos el correo
                Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                ->setTo($email)//para
                ->setFrom($empresa->email_contabilidad)//de
                ->setSubject($subject)
                ->setHtmlBody($content)
                ->send();
                return 'true';
                }
                else {
                    return 'false';
                }
            } else {
                return 'false';
            }//fin @$contactopago
            //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El trámite fue enviado correctamente.');
            //Yii::$app->response->redirect(array('tramite-pago/index'));
    }

    public function actionEliminar_tramite($id)
    {
        $model = $this->findModel($id);
        $facturas_compra = ComprasInventario::find()->where(['=','idTramite_pago', $id])->all();
        foreach ($facturas_compra as $factura_proces => $value) {

            $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
            $factura_proces->estado_pago = 'Proceso';
            $factura_proces->idTramite_pago = null;
            $factura_proces->save();//guardamos cambios en la factura de compra
        }
            $movi_pagar = MovimientoPagar::find()->where(['idTramite_pago'=>$id])->all();
            //Si hay notas de creditos asignarle las notas que se le aplico a la transferencia
            if (@$movi_pagar) {
                foreach ($movi_pagar as $tramite => $mov) {
                    $tramite = MovimientoPagar::find()->where(['=','idMov', $mov['idMov']])->one();
                    $tramite->idTramite_pago = 0;
                    $tramite->save();
                }
            }
        \Yii::$app->db->createCommand()->delete('tbl_medio_pago_proveedor', 'idTramite_pago = '.$id )->execute();//elimino el medio de pago del trámite obsoleto
        $this->findModel($id)->delete();//elimino el trámite obsoleto
        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El trámite se desechó con éxito y las facturas regresaron a estado "Proceso" para un nuevo trámite.');
        Yii::$app->response->redirect(array('tramite-pago/index'));//regreso al index
    }

    //--------------------------------------------------------------------------------------------------
    //--------------------------------------------AREA DE FACTURAS CANCELADAS---------------------------
    //--------------------------------------------------------------------------------------------------

    //obtengo la vista de generar pagos
    public function actionBuscar_factura_cancelada()
        {
            $idProveedor  = $_GET['idProveedor'];
            $numFactura = $_GET['numFactura'];
            $formaPago = $_GET['proceso_forma_pago'];
            $fechaRegistro = $_GET['fechaRegistro'] == '' ? '' : date('Y-m-d', strtotime( $_GET['fechaRegistro'] ));
            $fechaVencimiento = $_GET['fechaVencimiento'] == '' ? '' : date('Y-m-d', strtotime( $_GET['fechaVencimiento'] ));
            return $this->renderAjax('facturascanceladas', [
                'idProveedor' => $idProveedor, 'numFactura' => $numFactura,
                'formaPago' => $formaPago, 'fechaRegistro' => $fechaRegistro, 'fechaVencimiento' => $fechaVencimiento]);
        }

        public function actionVista_movimientos($id)
        {
            $model_mov = MovimientoPagar::find()->where(["=",'idCompra', $id])->one();
            return $this->renderAjax('vista_movimientos', [
                'model_mov' => $model_mov,
            ]);
        }
}
