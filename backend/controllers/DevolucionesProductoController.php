<?php

namespace backend\controllers;

use Yii;
use backend\models\DevolucionesProducto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\FacturasDia;
use backend\models\DetalleFacturas;
use backend\models\DevProductosDevueltos;
use backend\models\Clientes;
use backend\models\MovimientoCobrar;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\ProductoServicios;
use backend\models\UnidadMedidaProducto;
use backend\models\Factun;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use yii\helpers\Url;
use backend\models\EncabezadoCaja;
use backend\models\FormasPago;
use backend\models\HistorialEnvioEmail;
use backend\models\Funcionario;
use yii\filters\AccessControl;
require("mpdf/mpdf.php");
/**
 * DevolucionesProductoController implements the CRUD actions for DevolucionesProducto model.
 */
class DevolucionesProductoController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete','cargar-factura','devolver_producto','cargar_todos','descartar_producto',
        'aplicar_devolucion','re_aplicar_devolucion','enviar_correo_factura_movimiento','pdf','refrescar', 'consultar_estados_movimiento'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete','cargar-factura','devolver_producto','cargar_todos','descartar_producto',
            'aplicar_devolucion','re_aplicar_devolucion','enviar_correo_factura_movimiento','pdf','refrescar', 'consultar_estados_movimiento'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

    /**
     * Lists all DevolucionesProducto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DevolucionesProducto::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DevolucionesProducto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
      if(Yii::$app->request->isAjax){
      $id = Yii::$app->request->post('id');//intval($_GET['idCl']);
        $facturas = FacturasDia::find()
        ->where('fe LIKE :busqueda OR idCabeza_Factura LIKE :busqueda',[':busqueda' => '%'.$id.'%'])
        ->one();
        if ($id=='') {
          echo '<center><h1><small>Esperando una factura</small><h1></center><br>';
        } else echo $this->renderAjax('view', [
              'model' => $facturas,
          ]);
      }

    }

    /**
     * Creates a new DevolucionesProducto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DevolucionesProducto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idDevolucion]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DevolucionesProducto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idDevolucion]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DevolucionesProducto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DevolucionesProducto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DevolucionesProducto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DevolucionesProducto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCargarFactura()
    {
      if(Yii::$app->request->isAjax){
        $busqueda = Yii::$app->request->post('value');
        $busqueda_fu = Yii::$app->request->post('busqueda_fu');
        $busqueda_fec = Yii::$app->request->post('busqueda_fec');
        $busqueda = '%'.$busqueda.'%';
        if ($busqueda_fu != '') {
          $busqueda = '';
          $busqueda_fu = '%'.$busqueda_fu.'%';
        }

        $busqueda_fec = date('Y-m-d', strtotime( $busqueda_fec ));
        $sql = 'SELECT t2.idCabeza_Factura, t2.fecha_emision, t2.fe, IF (t2.cliente > 0, t1.cliente, t2.cliente) AS nombreCompleto, t2.estadoFactura, t2.funcionario
        FROM
        ( SELECT fa.idCabeza_Factura, cl.nombreCompleto AS cliente
          FROM `tbl_funcionario` fu
          INNER JOIN `tbl_encabezado_factura` fa ON fu.idFuncionario = fa.codigoVendedor
          INNER JOIN `tbl_clientes` cl ON fa.idCliente = cl.idCliente
        ) t1 RIGHT JOIN (
          SELECT fa.idCabeza_Factura, fa.fecha_emision, fa.fe, fa.idCliente AS cliente, fa.estadoFactura,
          CONCAT( fu.nombre," ",fu.apellido1," ",fu.apellido2 ) AS funcionario, fu.idFuncionario
          FROM `tbl_funcionario` fu INNER JOIN `tbl_encabezado_factura` fa
          ON fu.idFuncionario = fa.codigoVendedor
        ) t2
        ON t1.idCabeza_Factura = t2.idCabeza_Factura
        WHERE t2.fecha_emision = :busqueda_fec
        AND (t2.fe LIKE :busqueda
        OR t2.idCabeza_Factura LIKE :busqueda
        OR t2.cliente LIKE :busqueda
        OR t1.cliente LIKE :busqueda
        OR t2.estadoFactura LIKE :busqueda
        OR t2.funcionario LIKE :busqueda_fu
        OR CONCAT( t2.idCabeza_Factura,t2.fe,IF (t2.cliente > 0, t1.cliente, t2.cliente),t2.estadoFactura) LIKE :busqueda
        )
        ORDER BY t2.idCabeza_Factura DESC
        LIMIT 50';
        $command = \Yii::$app->db->createCommand($sql);
        $command->bindParam(":busqueda", $busqueda);
        $command->bindParam(":busqueda_fu", $busqueda_fu);
        $command->bindParam(":busqueda_fec", $busqueda_fec);
        $consulta = $command->queryAll();
        $lineas = '<table class="items table table-sm table-inverse" width="100%">
        <tr class="table-warning">
          <th width="5%"><font face="arial" size=4>ID</font></th>
          <th width="15%"><font face="arial" size=4>FE / TE</font></th>
          <th width="13%"><font face="arial" size=4>Fecha emisión</font></th>
          <th width="30%"><font face="arial" size=4>Nombre Cliente</font></th>
          <th width="10%"><font face="arial" size=4>Estado</font></th>
          <th width="28%"><font face="arial" size=4>Funcionario responsable</font></th>
        </tr>';
        foreach($consulta as $resultados){
          $fe = "'".$resultados['fe']."'";
          $lineas .= '<tr tabindex="0" style="cursor:pointer" onclick="javascript:agrega_factura('.$fe.')" onkeypress="$(document).keypress(function(event){ if(event.which == 13) agrega_factura('.$fe.') })">
                        <td width="5%">'.$resultados['idCabeza_Factura'].'</td>
                        <td width="15%">'.$resultados['fe'].'</td>
                        <td width="13%">'.date('d-m-Y', strtotime( $resultados['fecha_emision'] )).'</td>
                        <td width="30%">'.$resultados['nombreCompleto'].'</td>
                        <td width="10%">'.$resultados['estadoFactura'].'</td>
                        <td width="28%">'.$resultados['funcionario'].'</td>
                      </tr>';
        }
        echo $lineas.'</table>';
      }
    }

    public function actionDevolver_producto()
    {
      if(Yii::$app->request->isAjax){
        $codProdServicio = Yii::$app->request->post('codProdServicio');
        $idCabeza_Factura = Yii::$app->request->post('idCabeza_Factura');
        $cant_fact = Yii::$app->request->post('cant_fact');
        $cant_devolver = Yii::$app->request->post('cant_devolver');
        $idDetalleFactura = Yii::$app->request->post('idDetalleFactura');
        $DevProductosDevueltos = new DevProductosDevueltos();
        $detalle_factura = DetalleFacturas::findOne($idDetalleFactura);
        //buscamos en los productos devueltos correspondientes a esta factura que ya hayan sigo agregados en devoluciones
        if (@$devProdDev = $DevProductosDevueltos->find()->where(['idDetalleFactura'=>$idDetalleFactura])->all()) {
          foreach ($devProdDev as $key => $value) {
            $cant_fact -= $value->cantidad_dev;
          }
        }
        if ($cant_devolver > $cant_fact) {
          $campo_notificacion = '<br><center><div class="alert alert-danger">No puedes devolver una cantidad mayor a la registrada en la factura + cantidad ya en devolución</div></center>';
        } else {
          $total = $detalle_factura->precio_unitario * $cant_devolver;
          $DevProductosDevueltos->idDetalleFactura = $idDetalleFactura;
          $DevProductosDevueltos->cantidad_dev = $cant_devolver;
          $DevProductosDevueltos->descuento = $detalle_factura->descuento_producto;
          $DevProductosDevueltos->iv = $detalle_factura->subtotal_iva;
          $DevProductosDevueltos->total = $total;
          $DevProductosDevueltos->fecha_hora = date('Y-m-d H:i:s',time());
          $DevProductosDevueltos->estado = 'Pendiente';
          $DevProductosDevueltos->idFactura = $idCabeza_Factura;
          $DevProductosDevueltos->save();
          $campo_notificacion = '<br><center><div class="alert alert-success">Productos agregados correctamente</div></center>';
        }
        $facturas = FacturasDia::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
        $estadistica_producto = $this->renderAjax('view', [
              'model' => $facturas,
          ]);
        $arr = array( 'estadistica_producto'=>$estadistica_producto, 'campo_notificacion'=>$campo_notificacion );
        echo json_encode($arr);
      }
    }

    public function actionCargar_todos()
    {
      $idCabeza_Factura = Yii::$app->request->post('idCabeza_Factura');
      $cont = 0;
      $detalle_factura = DetalleFacturas::find()->where(['idCabeza_factura'=>$idCabeza_Factura])->all();
      foreach ($detalle_factura as $key => $value) {
        $cant_fact = 0;
        //buscamos en los productos devueltos correspondientes a esta factura que ya hayan sigo agregados en devoluciones
        if (@$devProdDev = DevProductosDevueltos::find()->where(['idDetalleFactura'=>$value->idDetalleFactura])->all()) {
          foreach ($devProdDev as $key_ => $value_) {
            $cant_fact += $value_->cantidad_dev;
          }
        }
        $cant_fact = $value->cantidad - $cant_fact;
        if ($cant_fact > 0) {
          $cont += 1;
          $DevProductosDevueltos = new DevProductosDevueltos();
          $total = $value->precio_unitario * $cant_fact;
          $DevProductosDevueltos->idDetalleFactura = $value->idDetalleFactura;
          $DevProductosDevueltos->cantidad_dev = $cant_fact;
          $DevProductosDevueltos->descuento = $value->descuento_producto;
          $DevProductosDevueltos->iv = $value->subtotal_iva;
          $DevProductosDevueltos->total = $total;
          $DevProductosDevueltos->fecha_hora = date('Y-m-d H:i:s',time());
          $DevProductosDevueltos->estado = 'Pendiente';
          $DevProductosDevueltos->idFactura = $idCabeza_Factura;
          $DevProductosDevueltos->save();
        }
      }

      $facturas = FacturasDia::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
      $estadistica_producto = $this->renderAjax('view', [
            'model' => $facturas,
        ]);

      if ($cont > 0) {
        $campo_notificacion = '<br><center><div class="alert alert-success">Productos agregados correctamente</div></center>';
      } else {
        $campo_notificacion = '<br><center><div class="alert alert-info">No se encuentran productos a devolver</div></center>';
      }
      $arr = array( 'estadistica_producto'=>$estadistica_producto, 'campo_notificacion'=>$campo_notificacion );
      echo json_encode($arr);
    }

    public function actionDescartar_producto()
    {
      if(Yii::$app->request->isAjax){
        $idProd_dev = Yii::$app->request->post('idProd_dev');
        $idFactura = Yii::$app->request->post('idFactura');
        $facturas = FacturasDia::find()->where(['idCabeza_Factura'=>$idFactura])->one();
        \Yii::$app->db->createCommand()->delete('tbl_dev_productos_devueltos', 'idProd_dev = '.(int) $idProd_dev )->execute();
        $estadistica_producto = $this->renderAjax('view', [
              'model' => $facturas,
          ]);
        $campo_notificacion = '<br><center><div class="alert alert-info">Producto descartado correctamente</div></center>';
        $arr = array( 'estadistica_producto'=>$estadistica_producto, 'campo_notificacion'=>$campo_notificacion );
        echo json_encode($arr);
      }
    }

    public function actionAplicar_devolucion()
    {
      if(Yii::$app->request->isAjax){
        $idCabeza_Factura = Yii::$app->request->post('idCabeza_Factura');
        $subtotal = Yii::$app->request->post('subtotal');
        $descuento = Yii::$app->request->post('descuento');
        $impuesto_venta = Yii::$app->request->post('impuesto_venta');
        $monto_nota_credito = floatval(str_replace(",", "",Yii::$app->request->post('monto_nota_credito')));
        $concepto = Yii::$app->request->post('detalle_devolucion');
        $tipo = 'nc';
        //----------------------------------------------------
        $identificacion = $tipo_identificacion = $email = '';

        $bandera = 'No cancelada';
        $model = FacturasDia::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
        $cliente = $model->idCliente;
        $provincia = $canton = $distrito = $barrio = $otras_senas = $telefono = '0';
        $cliente_model = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
        $plazo = $diasCredito = '';
        if (@$cliente_model) {//cliente registrado (FActura electronica)
          $cliente = $cliente_model->nombreCompleto;
          $identificacion = $cliente_model->identificacion;
          if ($cliente_model->tipoIdentidad == 'Física') {
            $tipo_identificacion = '01';
          } else if($cliente_model->tipoIdentidad == 'Jurídica'){
            $tipo_identificacion = '02';
          } else if($cliente_model->tipoIdentidad == 'DIMEX'){
            $tipo_identificacion = '03';
          } else {
            $tipo_identificacion = '04';
          }
          $email = $cliente_model->email;
          $diasCredito = strval($cliente_model->diasCredito);
          $provincia = $cliente_model->provincia;
          $canton = $cliente_model->canton;
          $distrito = $cliente_model->distrito;
          $barrio = $cliente_model->barrio;
          $otras_senas = $cliente_model->direccion;
          $telefono = $cliente_model->celular;
          $identificacion_cliente = [
            'tipo' => $tipo_identificacion,
            'numero' => $identificacion
          ];
          $cliente_iv_exonerado = $cliente_model->iv == 1 ? true : false;
          $tipo_doc = '01';
        } else {//tiquete electronico
          $cliente = $model->idCliente;
          $email = NULL;
          $identificacion_cliente = [];
          $cliente_iv_exonerado = false;
          $tipo_doc = '04';
        }
          //$modelfactura = MovimientoCredito::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one(); //Obtengo la factura
          $saldo_de_la_factura = $model->total_a_pagar; //extraigo el total a pagar de la factura

          $model_m_cobrar = new MovimientoCobrar(); //con estos procesos puedo obtener el saldo pendiente de esta factura
          $suma_monto_movimiento = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->sum('monto_movimiento');
          $saldo_fac = $model->total_a_pagar - $suma_monto_movimiento;
          //al comparar valido que las notas no superen el monto de la factura
          //lo interesante es que necesito hacer una comparacion muy exacta cuando se trata de montos iguales, por eso uso number_format()
          //como se ve a continuacion ya que en algun momento dado despues de varios procesos por abono o NC puede pasar solo por montos iguales
          //por el contrario el monto siempre será mayor al saldo de la factura
          //if ( (number_format($saldo_fac,2) == number_format($model->total_a_pagar,2)) || ($saldo_fac > $model->total_a_pagar) ) {
              //una ves confirmo que las notas no superan el monto de la factura buelvo a recorrer las notas
              //para aplicarlas cada una a la factura
              //ENVIAR MOVIMIENTO
              $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
              $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
              $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
              $condicion_venta = '01'; //01 = Contado, 02 = Crédito, 03 = Consignación, 04 = Apartado, 05 = Arrendamiento con opción de compra, 06 = Arrendamiento en función financiera, 99 = Otros
              $tipo_pago = ['01'];
              $fecha_documento = date('Y-m-d').'T'.date('H:i:s',time());
              //consulta para extraer el ultimo ID de movimiento por cobrar
              $sql_mov = 'SELECT idMov
              FROM tbl_movi_cobrar
              ORDER BY idMov DESC LIMIT 1';
              $command_mov = \Yii::$app->db->createCommand($sql_mov);
              $consulta = $command_mov->queryAll();
              $numero_original = NULL;
              foreach($consulta as $resultados){ $numero_original = $resultados['idMov']; }//se extrae el ultimo ID
              //$numero_original = strval($idCabeza_Factura);
              $tipo_documento = $tipo=='nc' ? '03' : '02';
              if ($model->tipoFacturacion == '5') {//Credito
                $condicion_venta = '02';
                $plazo = $diasCredito.' dias.';
              } else {
                $condicion_venta = '01'; //01 = Contado, 02 = Crédito, 03 = Consignación, 04 = Apartado, 05 = Arrendamiento con opción de compra, 06 = Arrendamiento en función financiera, 99 = Otros
              }

              $detalle_devol = DevProductosDevueltos::find()->where(['idFactura'=>$model->idCabeza_Factura])
              ->andWhere('estado = :estado',['estado'=>"Pendiente"])->all();
              $array_detalle[] = null; $codigo = 'ORD_SERV'; $descripcion = 'ORD_SERV'; $unidad = 'Sp'; $servicio = true;//datos del detalle
              //Impuesto General sobre las ventas
              $tarifa_impuesto = Yii::$app->params['porciento_impuesto']; $tipo_impuesto = '07';
              $impuesto[] = [];
              if ($cliente_iv_exonerado == true) {//entra aqui se es un cliente exonerado
                  $tipo_exoneracion = '03'; //01 = Compras Autorizadas, 02 = Ventas exentas a diplomáticos, 03 = Orden de compra(instituciones públicas y otros organismos), 04 = Exenciones Dirección General de Hacienda, 05 = Zonas Francas, 99 = Otros
                  $numero_documento_exonerado = 1;
                  $nombre_instituto_exonerado = $cliente;
                  $fecha_documento_exonerado = $fecha_documento;
                  $porcentaje_exonerado = 100;
                  $impuesto[] = [
                                 'codigo' => $tipo_impuesto,
                                 'tarifa' => $tarifa_impuesto,
                                 'exoneracion' => [
                                                   'tipo_documento' => $tipo_exoneracion,
                                                   'numero_documento' => $numero_documento_exonerado,
                                                   'nombre_institucion' => $nombre_instituto_exonerado,
                                                   'fecha_emision' => $fecha_documento_exonerado,
                                                   'porcentaje_compra' => $porcentaje_exonerado
                                                  ]
                                ];
                }
                $key_gl = 0; $c_gl = 0;
                //recorremos el detalle de la devolucion
                foreach ($detalle_devol as $key => $value) {
                  $key_gl = $key;
                  $c = $key+1;//contador
                  $c_gl = $c;
                  $moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
                  $detall_fac = DetalleFacturas::findOne($value->idDetalleFactura);
                  if ($producto = ProductoServicios::find()->where(['codProdServicio'=>$detall_fac->codProdServicio])->one()) {
                    $unid_med = UnidadMedidaProducto::find()->where(['idUnidadMedida'=>$producto->idUnidadMedida])->one();
                    $codigo = $producto->codProdServicio;
                    $descripcion = $producto->nombreProductoServicio;
                    $unidad = $unid_med->simbolo;
                    $servicio = $unidad == 'Sp' ? true : false;
                    $tarifa_impuesto = $unidad == 'Sp' ? 6.5 : Yii::$app->params['porciento_impuesto'];
                    $tipo_impuesto = $unidad == 'Sp' ? '07' : '01';

                    if ($cliente_iv_exonerado != true) {//entra aqui ssi no paso por cliente exonerado
                        if ($producto->exlmpuesto == 'No') {
                            $tarifa_impuesto = $unidad == 'Sp' ? 6.5 : Yii::$app->params['porciento_impuesto'];
                            $impuesto[$key] = [ 'codigo' => $tipo_impuesto, 'tarifa' => $tarifa_impuesto ];
                          } else {
                            $impuesto[$key] = [ 'codigo' => '98', 'tarifa' => 0.00 ];
                          }
                      }//si no es exonerado se ejecuta el impuesto normal
                  }
                  $porcentaje_descuento = (int) round(($value->descuento/$value->cantidad_dev) * 100 / $detall_fac->precio_unitario,0);
                  $array_detalle[$key] = [//creamos el array para montar todos los productos para el arreglo de detalle
                                  'numero_linea'=>$value->idDetalleFactura,
                                  'tipo_codigo' => '01',
                                  'codigo' => substr($codigo, 0, 20),
                                  'cantidad' => $value->cantidad_dev,
                                  'unidad_medida' => $unidad,
                                  'detalle' => substr($descripcion, 0, 150),
                                  //'moneda' => $moneda->abreviatura,
                                  'precio_unitario' => $detall_fac->precio_unitario,
                                  'monto_descuento' => $value->descuento,
                                  'naturaleza_descuento' => $value->descuento > 0.00 ? 'Oferta' : '',
                                  'impuesto' => $impuesto,
                                  'servicio' => $servicio,
                                  'bonificacion' => false
                                 ];
                  if ($cliente_iv_exonerado != true) { $impuesto[$key] = []; }
                }//fin de foreach del detalle de la devolucion
                //si la factura incluye el % de servicio de restautante, se agrega aqui
                if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
                  $precio_servicio_rest = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
                  if ($cliente_iv_exonerado != true) {
                    $impuesto[$key_gl+1] = [ 'codigo' => '98', 'tarifa' => 0.00 ];
                  }
                  $array_detalle[$key_gl+1] = [//creamos el array para montar la linea por el servicio 10%
                          'numero_linea'=>$c_gl+1,
                          'tipo_codigo' => '01',// 01 = Codigo del producto del vendedor, 02 = Codigo del producto del comprador, 03 = Codigo del producto asignado por la industria, 04 = Codigo de uso interno, 99 = Otros
                          'codigo' => '_SERVIREST',
                          'cantidad' => 1,
                          'unidad_medida' => 'Unid',
                          //'unidad_medida_comercial' => NULL,
                          'detalle' => 'SERVICIO DE RESTAURANTE',
                          'precio_unitario' => $precio_servicio_rest,
                          'monto_descuento' => 0.00,
                          'naturaleza_descuento' => '',
                          'impuesto' => $impuesto,
                          'servicio' => true,
                          'bonificacion' => false
                         ];
                   if ($cliente_iv_exonerado != true) { $impuesto[$key_gl+1] = []; }//vaciamos el impuesto
                }
                $documento_referencia[] = [
                                                     'tipo_doc'=>$tipo_doc,
                                                     'numero'=>$model->clave_fe,
                                                     'fecha_emision'=>date('Y-m-d', strtotime( $model->fecha_emision )).'T'.date('H:i:s', strtotime( $model->fecha_emision )),
                                                     'codigo'=>'03',
                                                     'razon'=>strval($concepto)
                                                  ];
              $json_movimiento = [//seteamos parametros de la factura
                   'id_externo' => strval($numero_original+1),
                   'tipo_documento' => $tipo_documento,//02= nota de debito, 03= nota de credito
                   'fecha_emision' => $fecha_documento,
                   'medio_pago' => $tipo_pago,//['01','04'] //
                   'receptor' => [
                                   'razon_social' => $cliente,
                                   'identificacion' => $identificacion_cliente,
                                   'identificacion_extranjero' => null,
                                   'razon_comercial' => $cliente,
                                   'ubicacion' => [
                                                     "provincia" => $provincia,
                                                     "canton" => $canton,
                                                     "distrito" => $distrito,
                                                     "barrio" => $barrio,
                                                     "otras_senas" => $otras_senas
                                                  ],
                                   'telefono' => [
                                                   "codigo_pais" => "",
                                                   "num_telefono" => ""
                                                 ],
                                   'fax'=> '',
                                   //'correo_electronico' => $email
                                 ],
                   'condicion_venta' => $condicion_venta,//crédito o contado
                   'plazo_credito' => $plazo,
                   'detalles' => $array_detalle,//agregamos el detalle de la factura
                   'resumen_documento' => [
                                           "codigo_moneda" => $moneda_local->abreviatura,
                                           "tipo_cambio" => $tipocambio->valor_tipo_cambio
                                         ],
                   'informacion_referencia' => $documento_referencia

                ];
              //ENVIAMOS LA FACTURA AL API PARA PROCEDER A FIRMAR ELECTRONICAMENTE EL DOCUMENTO
              $numero_mov = $clave_mov =  $estado_hacienda = $id_mov_factun = '0';
              $tran_api = Yii::$app->db->beginTransaction();
              try{
              $mensajes_error = '';
              if ( $empresa->factun_conect == 1 ) {//comprovamos que sea factura electronica
                $consultar_factun = new Factun();
                $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/Enviar';
                $result_movimiento = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir, $json_movimiento, 'POST'));

                //buscamos los datos electronicos para almacenarlos en el movimiento
                foreach ($result_movimiento->data as $key => $value) {
                  //con esto obtenemos los datos de la factura
                  if ($key == 'clave') { $clave_mov = $value; }
                  if ($key == 'numero_factura') { $numero_mov = $value; }//con esto obtenemos el numero de mpvimiento de hacienda para atribuircelo al archivo
                  if ($key == 'estado_hacienda') { $estado_hacienda = $value; }
                  if ($key == 'consecutivo') { $id_mov_factun = $value; }
                }

                //recorro las notificaciones de error
                foreach ($result_movimiento->mensajes as $key => $value) {
                  if ($key!='codigo_mensaje'&&$key!='mensaje'&&$key!='detalle_mensaje') {
                    $mensajes_error .= '<br> > '.$value;
                  }
                }
              } else {
                $clave_mov = '-';
                $estado_hacienda = '00';
                $numero_mov = '0';
                $id_mov_factun = -1;
              }
               //Asigno la nota de credito en los campos que corresponden a la tabla de la base de datos
                  $monto_anterior = $saldo_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                  $saldo_pendiente = $tipo=='nc' ? $monto_anterior - $monto_nota_credito : $monto_anterior + $monto_nota_credito; //le resto o sumo el monto del movimeinto (NC o ND) con el monto anterior para saber el monto pendiente
                  if ($saldo_pendiente < 0) {
                    $saldo_a_favor = $saldo_pendiente * -1;//creo un saldo a favor del cliente cuando las notas de credito superan la factura ya abonada
                    $saldo_pendiente = 0.00;
                  } else {
                    $saldo_a_favor = 0;
                  }

                  //Si el saldo esta en 0 se cancela la factura
                  if ( $saldo_pendiente == 0.00 ) {
                      $estadoFactura = 'Cancelada';
                      $fecha_final = date("Y-m-d");
                      $bandera = 'Cancelada';
                    } else {
                      $estadoFactura = 'Crédito';
                      $fecha_final = NULL;
                    }//Modificamos el saldo y el estado de la factura
                if ($clave_mov != '0') {
                  if ($model->estadoFactura == 'Crédito') {
                      $sql = "UPDATE tbl_encabezado_factura SET fecha_final = :fecha_final, estadoFactura = :estadoFactura, credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idCabeza_Factura";
                      $command = \Yii::$app->db->createCommand($sql);
                      $command->bindParam(":fecha_final", $fecha_final);
                      $command->bindParam(":estadoFactura", $estadoFactura);
                      $command->bindParam(":credi_saldo", $saldo_pendiente);
                      $command->bindValue(":idCabeza_Factura", $idCabeza_Factura);
                      $command->execute();
                  }
                  //fin modificacion de factura de credito

                  $model_m_cobrar->factun_id_mov = $id_mov_factun;
                  $model_m_cobrar->fecmov = date("Y-m-d H:i:s",time());
                  $model_m_cobrar->tipmov = $tipo=='nc' ? 'NC' : 'ND';
                  $model_m_cobrar->concepto = $concepto;
                  $model_m_cobrar->monto_anterior = $model->estadoFactura == 'Crédito' ? $monto_anterior : 0.00;
                  $model_m_cobrar->monto_movimiento = $monto_nota_credito;
                  $model_m_cobrar->saldo_pendiente = $model->estadoFactura == 'Crédito' ? $saldo_pendiente : 0.00;
                  $model_m_cobrar->usuario = Yii::$app->user->identity->username;
                  $model_m_cobrar->idCabeza_Factura = $idCabeza_Factura;
                  $model_m_cobrar->idCliente = $cliente;
                  $model_m_cobrar->idCli = (int)$cliente;
                  $model_m_cobrar->mov_el = $numero_mov;
                  $model_m_cobrar->clave_mov = $clave_mov;
                  $model_m_cobrar->xml_mov = '';
                  $model_m_cobrar->xml_resp = '';
                  $model_m_cobrar->estado_hacienda = '00';
                  $model_m_cobrar->estado_detalle = '';
                  $idMov = null;
                  if ($model_m_cobrar->save()) {//Guardo la nota de credito
                    //Creo el nuevo recibo por NC
                      $idMov = $model_m_cobrar->idMov;
                      $modelcliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
                      if (@$modelcliente) {//Para Clientes registrados
                      //
                      $nuevacantidad = FacturasDia::find()->where(['estadoFactura'=>'Crédito'])->andWhere('idCliente = :idCliente', [ 'idCliente'=>$modelcliente->idCliente ])->sum('credi_saldo'); //$modelcliente->montoTotalEjecutado - ($monto_nota_credito + $saldo_a_favor);
                      $recibo = new Recibos();
                      $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
                      $recibo->monto = $monto_nota_credito;
                      $recibo->cliente = $model->idCliente;
                      $recibo->usuario = Yii::$app->user->identity->username;
                      $recibo->tipoRecibo = 'NC';
                      $recibo->saldo_cuenta = $nuevacantidad <= 0 ? 0.00 : $nuevacantidad;//$model->estadoFactura == 'Crédito' ? $nuevacantidad : 0.00;
                      //Creo el detalle del recibo
                      if ($recibo->save()) {
                          $rec_deta = new RecibosDetalle();//instancia para almacenar la nota de credito
                          $rec_deta->idRecibo = $recibo->idRecibo;
                          $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                          $rec_deta->tipo = 'NC';
                          $rec_deta->referencia = 'Factura: '. $idCabeza_Factura . ' - ' . $concepto;
                          $rec_deta->fecha = date("Y-m-d H:i:s",time());
                          $rec_deta->monto = $monto_nota_credito;
                          $rec_deta->save();
                          if ($model->estadoFactura == 'Crédito') {
                            $modelcliente->montoTotalEjecutado = $nuevacantidad <= 0 ? 0.00 : $nuevacantidad;
                            $modelcliente->saldo_a_favor += $saldo_a_favor;
                          } else {
                            $modelcliente->saldo_a_favor += $model->tipoFacturacion == '4' ? $monto_nota_credito : 0;
                          }
                          $modelcliente->save();
                        }
                      }//fin comprovacion de clientes
                      else { //Para clientes no registrados
                        $suma_monto_nc = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])->sum('monto_movimiento');
                        $suma_monto_nd = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'ND'])->sum('monto_movimiento');
                        $saldo_fac = $monto_nota_credito - $suma_monto_nc + $suma_monto_nd;
                        $recibo = new Recibos();
                        $recibo->fechaRecibo = date("Y-m-d H:i:s",time());
                        $recibo->monto = $monto_nota_credito;
                        $recibo->cliente = $model->idCliente;
                        $recibo->usuario = Yii::$app->user->identity->username;
                        $recibo->tipoRecibo = 'NC';
                        $recibo->saldo_cuenta = $model->estadoFactura == 'Crédito' ? $saldo_fac : 0.00;
                        //Creo el detalle del recibo
                        if ($recibo->save()) {
                            $rec_deta = new RecibosDetalle();//instancia para almacenar la nota de credito
                            $rec_deta->idRecibo = $recibo->idRecibo;
                            $rec_deta->idDocumento = $model_m_cobrar->idMov; //id de nota de credito
                            $rec_deta->tipo = 'NC';
                            $rec_deta->referencia = 'Factura: '. $idCabeza_Factura . ' - ' . $concepto;
                            $rec_deta->fecha = date("Y-m-d H:i:s",time());
                            $rec_deta->monto = $monto_nota_credito;
                            $rec_deta->save();
                          }
                      }
                  }
                  $campo_notificacion = '<center><div class="alert alert-success">Productos aplicados correctamente, nota de crédito generada.</div></center>';
                  if ($bandera == 'Cancelada') {
                      $campo_notificacion .= '<center><div class="alert alert-success">Esta factura ha sido revocada y cancelada con este movimiento.</div></center>';
                    } else {
                       //$campo_notificacion .= '<center><div class="alert alert-success">Movimiento aplicado correctamente.</div></center>';
                    }
                    $devolucion = new DevolucionesProducto();
                    $devolucion->idFactura = $idCabeza_Factura;
                    $devolucion->descripcion_devolucion = $concepto;
                    $devolucion->estado_devolucion_producto = 'Aplicado';
                    $devolucion->fecha_hora = date('Y-m-d H:i:s',time());
                    $devolucion->usuario = Yii::$app->user->identity->username;
                    $devolucion->subtotal = $subtotal;
                    $devolucion->descuento = $descuento;
                    $devolucion->impuesto_venta = $impuesto_venta;
                    $devolucion->total = $monto_nota_credito;
                    $devolucion->idMov = $idMov;
                    if ($devolucion->save()) {
                      foreach ($detalle_devol as $key => $value) {
                        $key = DevProductosDevueltos::findOne($value->idProd_dev);
                        $detall_fac = DetalleFacturas::findOne($value->idDetalleFactura);
                        if ($detall_fac->codProdServicio != '0') {
                          $cod_prod = $detall_fac->codProdServicio;
                          $sql3 = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                          $command3 = \Yii::$app->db->createCommand($sql3);
                          $command3->bindParam(":codProdServicio", $cod_prod);
                          $cantidad = $command3->queryOne();
                          $nuevacantidad = $cantidad['cantidadInventario'] + $value->cantidad_dev;
                          $sql4 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidadInventario WHERE codProdServicio = :codProdServicio";
                          $position = \Yii::$app->db->createCommand($sql4);
                          $position->bindParam(":cantidadInventario", $nuevacantidad);
                          $position->bindValue(":codProdServicio", $cod_prod);
                          $position->execute();
                          $this->historial_producto_in($detall_fac->codProdServicio, $devolucion->idDevolucion, $value->cantidad_dev, 'SUM');
                        }
                        $key->idDevolucion = $devolucion->idDevolucion;
                        $key->estado = 'Aplicado';
                        $key->save();
                      }
                    }
                    $model = FacturasDia::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
                } else {
                  $campo_notificacion = '<center><div class="alert alert-danger">Se encontraron problemas. <br>'.$mensajes_error.'</div></center>';
                }
                $estadistica_producto = $this->renderAjax('view', [
                      'model' => $model,
                  ]);
                $tran_api->commit();
                $arr = array( 'estadistica_producto'=>$estadistica_producto, 'campo_notificacion'=>$campo_notificacion );
                echo json_encode($arr);
              } catch (\Exception $e) {
                /*si ocurre un error en toda la transaccion hace un rollBack y muestra el error*/
                $tran_api->rollBack();
                $model = FacturasDia::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
                $estadistica_producto = $this->renderAjax('view', [
                      'model' => $model,
                  ]);
                $campo_notificacion = '<br><center><div class="alert alert-danger">Error, no hay comunicación con factun. '.$e.'</div></center>';
                $arr = array( 'estadistica_producto'=>$estadistica_producto, 'campo_notificacion'=>$campo_notificacion );
                echo json_encode($arr);
                }
        /*  } else {
            $model = FacturasDia::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
            $estadistica_producto = $this->renderAjax('view', [
                  'model' => $model,
              ]);
              $campo_notificacion =  '<br><center><div class="alert alert-danger">Error; Las notas de crédito superan el saldo pendiente de la factura.</div></center>';
              $arr = array( 'estadistica_producto'=>$estadistica_producto, 'campo_notificacion'=>$campo_notificacion );
              echo json_encode($arr);
          }*/

        //----------------------------------------------------

      }
    }

    public function actionConsultar_estados_movimiento()
    {
      $consultar_factun = new Factun();
      $movimientos_cobrar = MovimientoCobrar::find()->where(['tipmov'=>'NC'])->andWhere('factun_id_mov > 0 AND estado_hacienda NOT IN ("01","03") AND fecmov > "2019-04-01"',[])->all();
      foreach ($movimientos_cobrar as $key => $movimiento_cobrar) {
        $clave = $movimiento_cobrar->clave_mov;
        $estado_hacienda = '00'; $detalle = '';
        $api_consultar = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/Consultar/'.$clave;
        $result_conslt = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_consultar, 'PUT'));
        if($result_conslt != null){
          foreach ($result_conslt->data as $key => $r_hac) {
              if ($key == 'detalle') { $detalle = $r_hac; }
              if ($key == 'estado_hacienda') { $estado_hacienda = $r_hac; }//con esto obtenemos el numero de factura de hacienda para atribuircelo al archivo
            }
            if ( $estado_hacienda == "01" || $estado_hacienda == "03" ) {
              //consultamos el XML
              $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/XML/'.$clave;
              $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
              //consultamos el XML respuesta
              $api_xml_respuesta = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/RespuestaXML/'.$clave;
              $result_xml_resp = $consultar_factun->ConeccionFactun_sin_pan($api_xml_respuesta, 'GET');
            } else {
              $result_xml = '';
              $result_xml_resp = '';
            }

          $movi_cobrar = MovimientoCobrar::findOne($movimiento_cobrar->idMov);
          $movi_cobrar->estado_hacienda = $estado_hacienda;
          $movi_cobrar->estado_detalle = $detalle;
          $movi_cobrar->xml_mov = $result_xml;
          $movi_cobrar->xml_resp = $result_xml_resp;
          if ($movi_cobrar->save()) {
            $factura = FacturasDia::findOne($movimiento_cobrar->idCabeza_Factura);
            if ($estado_hacienda=='01' && @$cliente = Clientes::find()->where(['idCliente'=>$factura->idCliente])->one()) {
              $this->enviar_correo_factura_movimiento( $factura->idCabeza_Factura, $cliente->email, $movimiento_cobrar->idMov );
            }
          }
        }
        if ($estado_hacienda == '05') {//no ha sido enviado, se requiere volver a facturar
          $api_facturar = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/Reenviar/'.$clave;
          $facturar = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_facturar, 'PUT'));
        }
      }
      echo '<br><center><span class="label label-success" style="font-size:12pt;">Movimientos sincronizados con Hacienda correctamente.</span></center>';
    }

    //vuelve aplicar la devolucion cuando existe una nota de credito rechazada
    public function actionRe_aplicar_devolucion()
    {
      if(Yii::$app->request->isAjax){
        $idDevolucion = Yii::$app->request->post('idDevolucion');
        $devolucion = DevolucionesProducto::findOne($idDevolucion);
        $movimiento_cobrar = MovimientoCobrar::findOne($devolucion->idMov);
        $factura = FacturasDia::findOne($movimiento_cobrar->idCabeza_Factura);
        $cliente_model = Clientes::find()->where(['idCliente'=>$factura->idCliente])->one();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
        $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
        $tipo_documento = $movimiento_cobrar->tipmov=='NC' ? '03' : '02';
        $plazo = ''; $concepto = $devolucion->descripcion_devolucion;
        $fecha_documento = date("Y-m-d");
        $condicion_venta = '01';
        $tipo_pago = '01';
        $diasCredito = '';
        if (@$cliente_model) {
          $cliente = $cliente_model->nombreCompleto;
          $identificacion = $cliente_model->identificacion;
          if ($cliente_model->tipoIdentidad == 'Física') {
            $tipo_identificacion = '01';
          } else if($cliente_model->tipoIdentidad == 'Jurídica'){
            $tipo_identificacion = '02';
          } else if($cliente_model->tipoIdentidad == 'DIMEX'){
            $tipo_identificacion = '03';
          } else {
            $tipo_identificacion = '04';
          }
          $email = $cliente_model->email;
          $diasCredito = strval($cliente_model->diasCredito);
        } else {
          $cliente = $factura->idCliente;
          $identificacion = $tipo_identificacion = $email = '';
        }
        if ($factura->tipoFacturacion == '1') {//efectivo
          $condicion_venta = '01'; $tipo_pago = '01';
        } elseif($factura->tipoFacturacion == '2'){//deposito
          $condicion_venta = '01'; $tipo_pago = '04';
        } elseif ($factura->tipoFacturacion == '3') {//tarjeta
          $condicion_venta = '01'; $tipo_pago = '02';
        } elseif ($factura->tipoFacturacion == '5') {//cheque
          $condicion_venta = '01'; $tipo_pago = '03';
        } elseif ($factura->tipoFacturacion == '4') {//Credito
          $condicion_venta = '02';
          $plazo = $diasCredito;
        } //crédito
        $json_movimiento = [
          "fecha_documento" => strval($fecha_documento),
          "numero_original" => strval($devolucion->idMov),
          "tipo_documento" => strval($tipo_documento), //02= nota de debito, 03= nota de credito
          "condicion_venta" => strval($condicion_venta),
          "tipo_pago" => strval($tipo_pago),
          "plazo" => $plazo,
          "sucursal_id" => intval($empresa->sucursal_id),
          "compania_id" => intval($empresa->compania_id),
          "moneda" => strval($moneda_local->abreviatura),
          "referencia_id" => intval($factura->factun_id),
          "tipo_cambio" => floatval($tipocambio->valor_tipo_cambio),
          "identificacion" => $identificacion,
          "tipo_identificacion" => $tipo_identificacion,
          "nombre_cliente" => strval($cliente),
          "email" => $email,
          "razon_de_nota" => strval($concepto)
        ];
        $consultar_factun = new Factun();
        $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Crear';
        $result_movimiento = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir, $json_movimiento, 'POST'));
        $detalle_devol = DevProductosDevueltos::find()->where(['idDevolucion'=>$idDevolucion])->all();
        $array_detalle[] = null; $codigo = 'ORD_SERV'; $descripcion = 'ORD_SERV'; $unidad = 'Sp'; $servicio = true;//datos del detalle
        foreach ($detalle_devol as $key => $value) {
          $c = $key+1;//contador
          $moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
          $detall_fac = DetalleFacturas::findOne($value->idDetalleFactura);
          if ($producto = ProductoServicios::find()->where(['codProdServicio'=>$detall_fac->codProdServicio])->one()) {
            $unid_med = UnidadMedidaProducto::find()->where(['idUnidadMedida'=>$producto->idUnidadMedida])->one();
            $codigo = $producto->codProdServicio;
            $descripcion = $producto->nombreProductoServicio;
            $unidad = $unid_med->simbolo;
            $servicio = $unidad == 'Sp' ? true : false;
          }
          $porcentaje_descuento = (int) round(($value->descuento/$value->cantidad_dev) * 100 / $detall_fac->precio_unitario,0);
          $array_detalle[$key] = [//creamos el array para montar todos los productos para el arreglo de detalle
                          'numero_linea'=>$value->idDetalleFactura,
                          'cantidad' => $value->cantidad_dev,
                          'tipo_codigo' => '01',
                          'codigo' => $codigo,
                          'descripcion' => $descripcion,
                          'unidad' => $unidad,
                          'moneda' => $moneda->abreviatura,
                          'tipo_impuesto' => '01',
                          'porcentaje_impuesto' => $value->iv,
                          'precio_unitario' => $detall_fac->precio_unitario,
                          'porcentaje_descuento' => $porcentaje_descuento,
                          'servicio' => $servicio,
                         ];

        }//fin de foreach
        $json_detalle = [
            'documento_id' => $result_movimiento->id,
            'detalles' => $array_detalle,
          ];
        $api_dir2 = 'https://'.Yii::$app->params['url_api'].'/api/v1/Detalle/Crear';
        $result_detalle = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir2, $json_detalle, 'POST'));
        //EJECUTAR NOTA
        $api_dir3 = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Facturar/'.$result_detalle->id;
        $facturar = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir3, 'PUT'));
        //compruebo respuesta de factun
        if ($result_movimiento->id > 0) {
          //buscamos los datos electronicos para almacenarlos en el movimiento
          $consultar_factun = new Factun();
          $api_cosult = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Consultar/'.$result_movimiento->id;
          $result_conslt = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_cosult, 'PUT'));
          $numero_mov = $clave_mov =  $estado_hacienda = $estado_detalle = '';
          foreach ($result_conslt->data as $key => $value) {
            //con esto obtenemos los datos de la factura
            if ($key == 'clave') { $clave_mov = $value; }
            if ($key == 'numero_documento') { $numero_mov = $value; }//con esto obtenemos el numero de mpvimiento de hacienda para atribuircelo al archivo
            if ($key == 'estado_hacienda') { $estado_hacienda = $value; }
            if ($key == 'detalle_mensaje') { $estado_detalle = $value; }
          }
          $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/XML/'.$result_movimiento->id;
          $api_xml_respuesta = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/RespuestaXML/'.$result_movimiento->id;
          $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
          $result_xml_resp = $consultar_factun->ConeccionFactun_sin_pan($api_xml_respuesta, 'GET');

          $movimiento_cobrar->factun_id_mov = $result_movimiento->id;
          $movimiento_cobrar->fecmov = date("Y-m-d H:i:s",time());
          $movimiento_cobrar->mov_el = $numero_mov;
          $movimiento_cobrar->clave_mov = $clave_mov;
          $movimiento_cobrar->xml_mov = $result_xml;
          $movimiento_cobrar->xml_resp = $result_xml_resp;
          $movimiento_cobrar->estado_hacienda = $estado_hacienda;
          $movimiento_cobrar->estado_detalle = $estado_detalle;
          $movimiento_cobrar->save();

          $devolucion->fecha_hora = date('Y-m-d H:i:s',time());
          $devolucion->save();
          $campo_notificacion = '<center><div class="alert alert-success">Reenviada a hacienda correctamente.</div></center>';
        } else {
          $campo_notificacion = '<br><center><div class="alert alert-danger">Error, no hay comunicación con factun.</div></center>';
        }

        //$factura = FacturasDia::find()->where(['idCabeza_Factura'=>$devolucion->idFactura])->one();
        $estadistica_producto = $this->renderAjax('view', [
              'model' => $factura,
          ]);

        $arr = array( 'estadistica_producto'=>$estadistica_producto, 'campo_notificacion'=>$campo_notificacion );
        echo json_encode($arr);
      }
    }

    //llena el historial del producto deacuerdo a cada movimiento de ingreso del producto a la ventas
    public function historial_producto_in($codProdServicio, $id_documento, $cant_mov, $tipo)
    {
      //ingresar producto en historial de producto
        $inventario = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
        //$codProdServicio = $inventario->codProdServicio;
        $fecha = date('Y-m-d H:i:s',time());
        $origen = 'Devolución';
        if ($tipo=='RES') {
          $cant_ant = $inventario->cantidadInventario + $cant_mov;
          $cant_new = $inventario->cantidadInventario;
        } else {
          $cant_ant = $inventario->cantidadInventario - $cant_mov;
          $cant_new =  $inventario->cantidadInventario;
        }

        $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
        $usuario = $funcionario->idFuncionario;
        $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
        $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
        $position_h_p->bindParam(":codProdServicio", $codProdServicio);
        $position_h_p->bindParam(":fecha", $fecha);
        $position_h_p->bindParam(":origen", $origen);
        $position_h_p->bindParam(":id_documento", $id_documento);
        $position_h_p->bindValue(":cant_ant", $cant_ant);
        $position_h_p->bindParam(":tipo", $tipo);
        $position_h_p->bindValue(":cant_mov", $cant_mov);
        $position_h_p->bindValue(":cant_new", $cant_new);
        $position_h_p->bindParam(":usuario", $usuario);
        $position_h_p->execute();

    }

    public function actionEnviar_correo_factura_movimiento($id, $email, $idMov)
    {
      $enviar_factura = $this->enviar_correo_factura_movimiento($id, $email, $idMov);
      echo $enviar_factura;
    }

    //enviar factura al correo desde una llamada a la funcion
    public function enviar_correo_factura_movimiento( $id, $email, $idMov )
    {
      $consultar_factun = new Factun();
        $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $model = FacturasDia::findOne($id);
            if (@$model) {
              $id_factun = $model->factun_id;
              $encabezado = '<table>
                  <tbody>
                  <tr>
                  <td>
                  <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                  </td>
                  <td style="text-align: left;">
                  <strong>'.$empresa->nombre.'</strong><br>
                  <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                  <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                  <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                  <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                  <strong>Dirección:</strong> '.$empresa->email.'
                  </td>
                  <tr>
                  </tbody>
              </table>';
              $movimiento = MovimientoCobrar::findOne($idMov);
              $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
              $opcion_movimiento = '<h4 style="text-align:right; color: #FF0040; font-family: Segoe UI, sans-serif;">INFORME POR NOTA DE CRÉDITO</h4>';
              $content = '
              <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO">
                <div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
                  <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
                    <br>
                    <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
                      <tbody><tr>
                        <td align="center" valign="top">
                          <table width="80%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                            <tbody>
                              <tr>
                                <td align="right" style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">

                                </td>
                              </tr>
                              <tr>
                                <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                                 <br>
                                  <br><br>
                                  '.$encabezado.$opcion_movimiento.'
                                  <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">APLICADA A LA FACTURA (ID: '.$id.') - '.$model->fe.'<br><small> Notificación el dia '.date('d-m-Y | h:i:s A').'&nbsp;&nbsp;</small></h4>
                                    <center><h1>Nota de crédito electrónico No. '.$movimiento->mov_el.'
                                  <br><br><small>Adjunto estan los documentos electrónicos.</small></h1></center><br><br><br><br><br>
                                </td>
                              </tr>
                              <tr>
                                <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                                  Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                    </table>
                  <br><br>
                  </div>
                </div>
              </div>';

              //CONSULTAR
              $clave = $numero_factura = $estado_hacienda = '';
              $stylesheet = file_get_contents('css/style_print.css');
              //con factura electronica  |||||||||||||||||||||||||||||||||||||||||||||||||
                $pdf_html = $this->pdf_html($model, $movimiento);
                if ($movimiento->estado_hacienda=='01') {
                  //$comprobante_electronico = 'Comprobante electrónico: ' . $numero_factura;
                  $archivo_xml = 'NC_'.$movimiento->clave_mov.".xml";//creamos el nombre del archivo xml, y como de aqui se obtiene no se le dice en que directorio esta
                  $archivo_xml_resp = 'NC_RESPUESTA_'.$movimiento->clave_mov.".xml";
                  $archivo_pdf = 'NC_'.$movimiento->clave_mov.'.pdf';//creamos el nombre del arhivo pdf, ...
                  //creamos el xml que esta en bd
                  $archivo = fopen($archivo_xml, "a+");//creamos el archivo xml y con permiso de lectura y escritura
                  fwrite($archivo, $movimiento->xml_mov);//escribimos los datos xml en el archivo
                  fclose($archivo);//cerramos el archivo
                  //creamos el archivo xml de respuesta que esta en bd
                  $archivo2 = fopen($archivo_xml_resp, "a+");//creamos el archivo xml y con permiso de lectura y escritura
                  fwrite($archivo2, $movimiento->xml_resp);//escribimos los datos xml en el archivo
                  fclose($archivo2);//cerramos el archivo

                  $caja_pdf = new EncabezadoCaja();
                  $caja_pdf->_pdf_nc($movimiento->mov_el, $stylesheet, $pdf_html, $archivo_pdf);
                  $subject = 'Presentación de Nota de crédito de '.$empresa->nombre;
                  //Enviamos el correo
                  Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                  ->setTo($email)//para
                  ->setFrom($empresa->email_contabilidad)//de
                  ->setSubject($subject)
                  ->setHtmlBody($content)
                  ->attach($archivo_xml)->attach($archivo_xml_resp)->attach($archivo_pdf)//se agragan todos los adjuntos
                  ->send();
                  unlink($archivo_xml);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
                  unlink($archivo_xml_resp);
                  unlink($archivo_pdf);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
                  $histrial_email = new HistorialEnvioEmail();
                  $histrial_email->tipo_documento = 'NC';
                  $histrial_email->idDocumento = $movimiento->idMov;
                  $histrial_email->fecha_envio = date('Y-m-d H:i:s',time());
                  $histrial_email->email = $email;
                  $histrial_email->usuario = Yii::$app->user->identity->username;
                  $histrial_email->save();
                  sleep(5);
                  return '<br><span class="label label-success" style="font-size:15pt;">Factura enviada correctamente.</span>';
                }//fin consulta si esta aprovado por hacienda
                else {
                  return '<br><span class="label label-danger" style="font-size:15pt;">No puedes enviar facturas al correo sin aprobación de Hacienda.</span>';
                }
            } //fin @$model
            //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> La cotización fué enviada correctamente.');
            //$this->redirect(['update', 'id' => $id]);
    }// fin de envio_email

    //funcion para imprimir el Pdf
    public function actionPdf($idFactura, $idMov)
    {
      $model = FacturasDia::findOne($idFactura);
      $movimiento = MovimientoCobrar::findOne($idMov);

      $pdf_html = $this->pdf_html($model, $movimiento);
      $archivo_pdf = 'Nota de crédito No. '.$movimiento->mov_el.'.pdf';
      $stylesheet = file_get_contents('css/style_print.css');
      $caja_pdf = new EncabezadoCaja();
      $caja_pdf->_pdf_nc_show($movimiento->mov_el, $stylesheet, $pdf_html, $archivo_pdf);

      //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Se está descargando el PDF.');
      //return $this->redirect(['index']);
    }

    public function pdf_html($model, $movimiento)
    {
      $empresaimagen = new Empresa();
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $devolucion = DevolucionesProducto::find()->where(['idMov'=>$movimiento->idMov])->one();
      $products = DevProductosDevueltos::find()->where(["=",'idDevolucion', $devolucion->idDevolucion])->all();
      $detalle_factura = '';
      if($products) {
        foreach($products as $position => $product) {
          $detalle_factura_db = DetalleFacturas::findOne($product->idDetalleFactura);
          $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$detalle_factura_db->codProdServicio])->one();
          $descrip = $detalle_factura_db->codProdServicio != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
          $detalle_factura .= '<tr>
            <td class="no">'.$product->cantidad_dev.'</td>
            <td class="cod" style="font-size: 0.8em;">'.$detalle_factura_db->codProdServicio.'</td>
            <td class="desc" style="font-size: 0.8em;"><strong>'.$descrip.'</strong></td>
            <td class="unit" style="font-size: 0.8em;">'.number_format($detalle_factura_db->precio_unitario, 2).'</td>
            <td class="qty" style="font-size: 0.8em;">'.number_format($product->descuento,2).'</td>
            <td class="unit" style="font-size: 0.8em;">'.number_format($product->iv,2).'</td>
            <td class="total">'.number_format($product->total,2).'</td>
          </tr>';
        }
      }

      $info_cliente = $info_invoice = $tipo_documento = '';
      if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
          $tipo_documento = 'FE';
          $info_cliente = '
          <h2 class="name">'.$cliente->nombreCompleto.'<br>ID: '.$cliente->identificacion.'</h2>
          <div class="address">'.$cliente->direccion.'</div>
          <div class="email">'.$cliente->celular.' / '.$cliente->telefono.'<br><a href="mailto:'.$cliente->email.'">'.$cliente->email.'</a></div>';
      } else{
        $tipo_documento = 'TE';
          $info_cliente = '<h2 class="name">'.$model->idCliente.'</h2>';
      }
      $tipo_pago = '';
      if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
          $tipo_pago = $modelP->desc_forma;
      }

        $info_invoice = '
        <h2 class="name" style="color: #57B223;">NCE: '.$movimiento->mov_el.'</h2>
        <div style="font-size: 0.8em;  color: #FF0000;">Clave: '.$movimiento->clave_mov.'</div>
        <div class="date">Fecha emitida: '.date('d-m-Y | h:i:s A', strtotime($movimiento->fecmov)).'</div>
        <div style="font-size: 1.0em; color: #0087C3;">Aplicada a '.$tipo_documento.': '.$model->fe.'</div>';


      $footer_pdf = 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';
      if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
        $servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
        $servicio_restaurante = '<tr>
                         <td colspan="3"></td>
                         <td colspan="3">SERVICIO 10%: </td>
                         <td>'.number_format($servicio,2).'</td>
                     </tr>';
      } else {
        $servicio_restaurante = '';
      }
      return '<header class="clearfix">
            <div id="company">
              <table>
              <tr>
              <td>
              <img src="'.$empresaimagen->getImageurl('pdf').'" height="10%">
              </td>
              <td>
                <div id="company">
                  <h2 class="name">'.$empresa->nombre.' - ID: '.$empresa->ced_juridica.'</h2>
                  <div>'.$empresa->localidad.' - '.$empresa->direccion.'</div>
                  <div>'.$empresa->telefono.' / '.$empresa->fax.'</div>
                  <div><a href="mailto:'.$empresa->sitioWeb.'">'.$empresa->sitioWeb.'</a> | <a href="mailto:'.$empresa->email.'">'.$empresa->email.'</a></div>
                </div>
              </td>
              </tr>
              </table>
            </div>
      </header>
      <main>
        <div id="details" class="clearfix">
          <table class="table_info">
            <tr>
              <th class="derecha">
                <div id="client">
                  <div class="to">Factura a:</div>
                  '.$info_cliente.'
                </div>
              </th>
              <th class="izquierda">
                <div id="invoice">
                  <h1 style="color: #0087C3;">Nota de Crédito ID. '.$movimiento->idMov.'</h1>
                  '.$info_invoice.'
                  <h3 style="font-size: 1.0em; color: #0087C3;">Condición Venta: '.$model->estadoFactura.' | Tipo de Pago:	'.$tipo_pago.'</h3>
              </th>
            </tr>
          </table>
        </div>
        <table border="0" cellspacing="0" cellpadding="0" class="table_detalle">
          <thead>
            <tr>
              <th class="no">C.</th>
              <th class="cod">COD</th>
              <th class="desc">DESCRIPCIÓN</th>
              <th class="unit">PREC UNIT</th>
              <th class="qty">DESCTO</th>
              <th class="unit">IV %</th>
              <th class="total">TOTAL</th>
            </tr>
          </thead>
          <tbody>
            '.$detalle_factura.'
          </tbody>
          <tfoot>
            '.$servicio_restaurante.'
            <tr>
              <td colspan="3"></td>
              <td colspan="3">SUBTOTAL</td>
              <td>'.number_format($devolucion->subtotal,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3">TOTAL DESCUENTO</td>
              <td>'.number_format($devolucion->descuento,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3">TOTAL IV</td>
              <td>'.number_format($devolucion->impuesto_venta,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3"><strong>TOTAL A PAGAR</strong></td>
              <td><strong>'.number_format($devolucion->total,2).'</strong></td>
            </tr>
          </tfoot>
        </table>
        <div id="thanks">¡Gracias por su preferencia!</div>
        <div id="notices">
          <div>Observación:</div>
          <div class="notice">'.$devolucion->descripcion_devolucion.'</div>
        </div>
      </main>
      <footer>
        '.$footer_pdf.'
      </footer>';
    }//fin pdf


    public function actionRefrescar()
    {
      if(Yii::$app->request->isAjax){
        $idCabeza_Factura = Yii::$app->request->post('idCabeza_Factura');
        $model = FacturasDia::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
        echo $this->renderAjax('view', [   'model' => $model   ]);
      }
    }

    public function actionImprimir_movimiento_credito($id)
    {
      echo $this->render('imprimir_movimiento_credito', [//aqui llama imprimir_movimiento_credito que imprime la factura correspondiente a este id
            'id' => $id,
        ]);
    }

    public function actionImprimir_devoluciones($id)
    {
      echo $this->render('imprimir_devoluciones', [//aqui llama imprimir_movimiento_credito que imprime la factura correspondiente a este id
            'id' => $id,
        ]);
    }
}
