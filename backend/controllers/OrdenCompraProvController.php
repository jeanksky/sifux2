<?php

namespace backend\controllers;

use Yii;
use backend\models\OrdenCompraProv;
use backend\models\DetalleOrdenCompra;
use backend\models\ProductoPedido;
use backend\models\ProductoServicios;
use backend\models\ContactosPedidos;
use backend\models\search\OrdenCompraProvSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OrdenCompraSession;
use backend\models\Proveedores;
use backend\models\DetalleCompra;
use backend\models\ProductoProveedores;
use backend\models\ComprasInventario;
use backend\models\Empresa;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\DetalleOrdenCompraTemporal;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\filters\AccessControl;

/**
 * OrdenCompraProvController implements the CRUD actions for OrdenCompraProv model.
 */
class OrdenCompraProvController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete','limpiar',
                'busqueda_proveedor', 'addproveedor', 'actualizar_producto_modal',
                'agrega_producto_pedido', 'obtener_productos_pedidos_regreso', 'orden_compra_productos_disponibles',
                'orden_compra_productos_disponibles_rango_fechas', 'orden_compra_productos_pedido', 'estadistica_producto', 'buscar_estadistica_producto',
                'imprimir_orden_compra', 'obtenerordencompra', 'lista_orden_compra',
                'enviar_orden_compra','completar_compra'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all OrdenCompraProv models.
     * @return mixed
     */
    public function actionIndex()
    {
        OrdenCompraSession::setFechasrendimiento(null);
        $searchModel = new OrdenCompraProvSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrdenCompraProv model.
     * @param integer Response$id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrdenCompraProv model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrdenCompraProv();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idOrdenCompra]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OrdenCompraProv model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idOrdenCompra]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OrdenCompraProv model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrdenCompraProv model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenCompraProv the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenCompraProv::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //permite vaciar la session y quitar los datos del proveedor en pantalla
    public function actionLimpiar(){
        OrdenCompraSession::setIDproveedor(null);
        OrdenCompraSession::setProveedor(null);


        Yii::$app->response->redirect(array('orden-compra-prov/index'));
    }

    //me busca los proveedres registrados en bd
        public function actionBusqueda_proveedor(){
         if(Yii::$app->request->isAjax){
                    $consultaBusqueda  = Yii::$app->request->post('valorBusqueda');

                    //Filtro anti-XSS
                    $caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
                    $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
                    $consultaBusqueda = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda);

                    $mensaje = "";

                    //Comprueba si $consultaBusqueda está seteado
                    if (isset($consultaBusqueda)) {
                        $consultaBusquedalimpia = "%".$consultaBusqueda."%";
                        //Selecciona todo de la tabla idCliente
                        //donde el nombre sea igual a $consultaBusqueda,
                        //o el apellido sea igual a $consultaBusqueda,
                        //o $consultaBusqueda sea igual a nombre + (espacio) + apellido
                        $sql = "SELECT * FROM tbl_proveedores
                        WHERE codProveedores LIKE :like_
                        OR nombreEmpresa LIKE :like_
                        OR CONCAT(codProveedores,' ',nombreEmpresa)  LIKE :like_";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":like_", $consultaBusquedalimpia);
                        $consulta = $command->queryAll();
                        //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                        //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                            //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                            //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                            //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle


                            foreach($consulta as $resultados){
                                $codProveedores = $resultados['codProveedores'];
                                $nombreEmpresa = $resultados['nombreEmpresa'];
                                //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                                $accion = Html::a('', '', [
                                    'class' => 'glyphicon glyphicon-plus',
                                    'onclick'=>'agrega_proveedor("'.$codProveedores.'")',
                                    'data-dismiss'=>'modal',
                                    'title' => Yii::t('app', 'Cargar proveedor')
                                ]);
                                //Output

                         $mensaje .='<tbody><tr>
                                      <td width="50" align="center">'.$codProveedores.'</td>
                                      <td width="400">'.$nombreEmpresa.'</td>
                                      <td width="60" align="center">'.$accion.'</td>
                                    </tr></tbody>';

                            }
                    //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
                }
                echo $mensaje;
            }//fin ajax
        }//fin de funcion de busqueda de productos

        public function actionAddproveedor()
        {
            if(Yii::$app->request->isAjax){
                $codProveedor = Yii::$app->request->post('codProveedor');
                if ($modelproveedor = Proveedores::find()->where(['=','codProveedores', $codProveedor])->one()){
                    if($pro_asociado = ProductoProveedores::find()->where(['=','codProveedores', $modelproveedor->codProveedores])->one()){
                        $this->cargardatos($codProveedor);
                        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Los productos del proveedor '.$modelproveedor->nombreEmpresa.' se han cargado');

                } else {
                    Yii::$app->getSession()->setFlash('info', '<center><h3><span class="glyphicon glyphicon-flag"></span> <strong>Atención!</strong> '.$modelproveedor->nombreEmpresa.' todavia no cuenta con productos asociados</h3></center>');
                }
                            Yii::$app->response->redirect(array('orden-compra-prov/index'));
                }else{
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> El ID digitado no pertenece a ningun proveedor registrado.');
                    Yii::$app->response->redirect(array('orden-compra-prov/index'));
                }
            }
        }

        //carga el proveedor en sssion para decirle al sistema el provedor con el que se trabajará
        public function cargardatos($idProveedor){
            $modelproveedor = Proveedores::find()->where(['=','codProveedores', $idProveedor])->one();
            //$montodisponible = $modelproveedor->montoMaximoCredito - $modelproveedor->montoTotalEjecutado;
            OrdenCompraSession::setIDproveedor($modelproveedor->codProveedores);
            OrdenCompraSession::setProveedor($modelproveedor->nombreEmpresa);//Agrego el nombre del proveedor
        }

        //muestra la vista de la modal que actualiza los productos en Inventario
        public function actionActualizar_producto_modal($id, $submit = false)
        {
          $modelproducto = ProductoServicios::find()->where(['=','codProdServicio', $id])->one();
          if ( $modelproducto->load(Yii::$app->request->post()) && Yii::$app->request->isAjax && $submit == false )
          {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ActiveForm::validate($modelproducto);
          }
          if ( $modelproducto->load(Yii::$app->request->post()) ) {

              if ($modelproducto->save()) {
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El producto ha sido actualizado correctamente.');
                $modelproducto->refresh();
                return '<script type="text/javascript">
               self.close();
               </script>';//$this->redirect(['index']);
              }
             else { Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($modelproducto); }
          } else {
              return $this->renderAjax('actualizar_producto_modal', ['codProdServicio' => $id, 'modelproducto'=>$modelproducto ]);
          }

        }
        //esta funcion me actualiza el producto de la modal que es seleccionado
      /*  public function actionActualiza_prod_inv($id, $submit = false)
        {
          $modelproducto = ProductoServicios::find()->where(['=','codProdServicio', $id])->one();
            if (Yii::$app->request->isAjax && $modelproducto->load(Yii::$app->request->post()) && $submit == false) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($modelproducto);
            }
            if ( $modelproducto->load(Yii::$app->request->post()) ) {
                if ($modelproducto->save()) {
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El producto ha sido actualizado correctamente.');
                  $modelproducto->refresh();
                  Yii::$app->response->format = Response::FORMAT_JSON;
                  return $this->redirect(['index']);
                } else {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($modelproducto);
                }
            } else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> El producto no pudo ser ingresado.');
                return $this->redirect(['index']);
            }
        }*/

        //pasa productos disponibles a la tabla de productos para pedido
        public function actionAgrega_producto_pedido()
        {
          if(Yii::$app->request->isAjax){
              $codigo_producto  = Yii::$app->request->post('codigo_producto');
              $cantidad_sugerida  = Yii::$app->request->post('cantidad_sugerida');
              $cantidad_pedir  = Yii::$app->request->post('cantidad_pedir');
              $idProveedor = Yii::$app->request->post('idProveedor');
              $idProd_prov = Yii::$app->request->post('idProd_prov');
              if ($cantidad_pedir == '' || $cantidad_pedir == '0') {
                echo '<br><center><div class="alert alert-danger"><strong>Eey!!,</strong> el producto '.$codigo_producto.' no cuenta con cantidad sujerida, intente de nuevo.</div></center>';
              } else {
                  $comprueba_detalle_temporal = DetalleOrdenCompraTemporal::find()->where(['=','codProdServicio',$codigo_producto])->one();
                  if (@$comprueba_detalle_temporal) {
                    $producto_proveedores = ProductoProveedores::find()->where(['=', 'idProd_prov', $comprueba_detalle_temporal->idProd_prov])->one();
                    $proveedor = Proveedores::find()->where(['=','codProveedores', $producto_proveedores->codProveedores])->one();
                    echo '<br><center><div class="alert alert-danger"><strong>Eey!!,</strong> el producto '.$codigo_producto.' ya ha sido reservado para un nuevo pedido a '.$proveedor->nombreEmpresa.'.</div></center>';
                  } else {
                    $detalle_temporal = new DetalleOrdenCompraTemporal();
                    $detalle_temporal->idProveedores = $idProveedor;
                    $detalle_temporal->codProdServicio = $codigo_producto;
                    $detalle_temporal->idProd_prov = $idProd_prov;
                    $detalle_temporal->cantidad_sugerida = $cantidad_sugerida;
                    $detalle_temporal->cantidad_pedir = $cantidad_pedir;
                    $detalle_temporal->save();
                    $prod_prov = ProductoProveedores::find()
                    ->where(['=','codProveedores', $idProveedor])
                    ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$codigo_producto])->one();
                    $prod_prov->estado = 'Pedido';
                    $prod_prov->save();
                    echo '<br><center><div class="alert alert-success">El producto '.$codigo_producto.' fue agregado al proceso de compra con una cantidad a pedir de '.$cantidad_pedir.' unidades.</div></center>';
                  }
              }
            }
        }
        //devuelve los productos en el pedido para la tabla de productos disponibles
        public function actionObtener_productos_pedidos_regreso()
        {
          if(Yii::$app->request->isAjax){
              $checkboxValues  = Yii::$app->request->post('checkboxValues');
              $idProveedor =  Yii::$app->request->post('idProveedor');
              $array = explode(",", $checkboxValues);

              $longitud = count($array);
              if ($checkboxValues) {
                  for($i=0; $i<$longitud; $i++) {
                    $prod_prov = ProductoProveedores::find()
                    ->where(['=','codProveedores', $idProveedor])
                    ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$array[$i]])->one();
                    $prod_prov->estado = 'Disponible';
                    if ($prod_prov->save()) {
                      \Yii::$app->db->createCommand()->delete('tbl_detalle_orden_compra_temp', 'idProveedores = '.(int) $idProveedor.' AND codProdServicio = "'.$array[$i].'"' )->execute();
                    }

                      /*$estado_pago = 'Pendiente';

                      $sql = "UPDATE tbl_compras SET estado_pago = :estado_pago WHERE idCompra = :idCompra";

                          $command = \Yii::$app->db->createCommand($sql);
                          $command->bindParam(":estado_pago", $estado_pago);
                          $command->bindValue(":idCompra", $array[$i]);
                          $command->execute();*/
                  }
                  echo '<br><div class="alert alert-success" style="font-size:15pt;">Producto(s) devueltos correctamente</div>';
              } else echo '<br><div class="alert alert-info" style="font-size:15pt;">No se ha obtenido resultados por devolver</div>';
          }
        }

        //esta accion me permite llamar la tabla de orden_compra_productos_disponibles en el momento de refrescar
        public function actionOrden_compra_productos_disponibles()
        {
            $idProveedor  = $_GET['idProveedor'];
            $filtro_minimo = $_GET['filtro_minimo'];
            $filtro_excluir = $_GET['filtro_excluir'];
            $dias_rendimiento = $_GET['dias_rendimiento'];
            $prov_fech_desde = $_GET['prov_fech_desde'];
            $prov_fech_hasta = $_GET['prov_fech_hasta'];
            $limit = $_GET['limit'];
            if ($dias_rendimiento=='0') {
              OrdenCompraSession::setDiasrendimiento(null);
            }else {
              OrdenCompraSession::setDiasrendimiento($dias_rendimiento);
            }
            return $this->renderAjax('orden_compra_productos_disponibles',
            [
              'idProveedor' => $idProveedor,
              'limit' => $limit,
              'filtro_minimo' => $filtro_minimo,
              'filtro_excluir' => $filtro_excluir,
              'prov_fech_desde' => $prov_fech_desde,
              'prov_fech_hasta' => $prov_fech_hasta
            ]);
        }

        public function actionOrden_compra_productos_disponibles_rango_fechas()
        {
            $idProveedor  = $_GET['idProveedor'];
            $filtro_minimo = $_GET['filtro_minimo'];
            $filtro_excluir = $_GET['filtro_excluir'];
            $prov_fech_desde = $_GET['prov_fech_desde'];
            $prov_fech_hasta = $_GET['prov_fech_hasta'];
            $check = $_GET['check'];
            OrdenCompraSession::setDiasrendimiento(null);

            if ($check=='si') {
              OrdenCompraSession::setFechasrendimiento('si');
            } else {
              OrdenCompraSession::setFechasrendimiento(null);
            }
            return $this->renderAjax('orden_compra_productos_disponibles',
            [
              'idProveedor' => $idProveedor,
              'filtro_minimo' => $filtro_minimo,
              'filtro_excluir' => $filtro_excluir,
              'prov_fech_desde' => $prov_fech_desde,
              'prov_fech_hasta' => $prov_fech_hasta
            ]);
        }

        //esta accion me permite llamar la tabla de orden_compra_productos_pedido en el momento de refrescar
        public function actionOrden_compra_productos_pedido()
        {
            $idProveedor  = $_GET['idProveedor'];
            return $this->renderAjax('orden_compra_productos_pedido', ['idProveedor' => $idProveedor ]);
        }

        //esta funcion me llena el modilo de estadistica del productos
        public function actionEstadistica_producto()
        {
          $idProducto = $_GET['idProducto'];
          $idProveedor = $_GET['idProveedor'];
          $fecha_desde = $_GET['desde'];
          $fecha_hasta = $_GET['hasta'];
          $fedes = date('Y-m-d', strtotime( $fecha_desde ));//fecha desde con formato sql
          $fehas = date('Y-m-d', strtotime( $fecha_hasta ));//fecha hasta con formato sql
          return $this->renderAjax('estadistica_producto', ['idProveedor' => $idProveedor, 'idProducto' => $idProducto, 'fedes' => $fedes, 'fehas' => $fehas]);
        }

        public function actionBuscar_estadistica_producto()
        {
          if(Yii::$app->request->isAjax){
            $prov_fech_desde = Yii::$app->request->post('prov_fech_desde');
            $prov_fech_hasta = Yii::$app->request->post('prov_fech_hasta');
            $idProveedor = Yii::$app->request->post('idProveedor');
            $fedes = date('Y-m-d', strtotime( $prov_fech_desde ));
            $fehas = date('Y-m-d', strtotime( $prov_fech_hasta ));
            $codProdServicio = Yii::$app->request->post('codProdServicio');
            $sql_comprados = "SELECT SUM(cd.`cantidadEntrada`) AS cantidad_compra FROM tbl_compras c INNER JOIN tbl_detalle_compra cd
            ON cd.`idCompra` = c.`idCompra`
            WHERE cd.`codProdServicio` = :codProdServicio
            AND cd.`estado` = 'Aplicado'
            AND c.`fechaRegistro` BETWEEN :fecha_desde AND :fecha_hasta";
                $command = \Yii::$app->db->createCommand($sql_comprados);
                $command->bindParam(":codProdServicio", $codProdServicio);
                $command->bindParam(":fecha_desde", $fedes);
                $command->bindParam(":fecha_hasta", $fehas);
                $result_comprados = $command->queryOne();
            $sql_vendidos = "SELECT SUM(vd.`cantidad`) AS cantidad_venta FROM tbl_encabezado_factura v INNER JOIN tbl_detalle_facturas vd
            ON vd.`idCabeza_factura` = v.`idCabeza_Factura`
            WHERE vd.`codProdServicio`= :codProdServicio
            AND v.`estadoFactura` IN ('Cancelada','Crédito')
            AND v.`fecha_inicio` BETWEEN :fecha_desde AND :fecha_hasta";
                $command2 = \Yii::$app->db->createCommand($sql_vendidos);
                $command2->bindParam(":codProdServicio", $codProdServicio);
                $command2->bindParam(":fecha_desde", $fedes);
                $command2->bindParam(":fecha_hasta", $fehas);
                $result_vendidos = $command2->queryOne();
            //este html me muestra los proveedores que se le compra el producto seleccionado al rango de fechas
            $sql_prov_com = "SELECT MAX(cd.`idDetalleCompra`) AS detalle_max, MAX(c.`fechaRegistro`) AS fecha_max, c.`idProveedor` FROM tbl_compras c
                             INNER JOIN tbl_detalle_compra cd
                             ON c.`idCompra` = cd.`idCompra`
                             WHERE cd.`codProdServicio` = :codProdServicio
                             AND c.`fechaRegistro` BETWEEN :fecha_desde AND :fecha_hasta
                             GROUP BY c.`idProveedor` ORDER BY detalle_max DESC";
                $command_prov_com = \Yii::$app->db->createCommand($sql_prov_com);
                $command_prov_com->bindParam(":codProdServicio", $codProdServicio);
                $command_prov_com->bindParam(":fecha_desde", $fedes);
                $command_prov_com->bindParam(":fecha_hasta", $fehas);
                $result_prov_com = $command_prov_com->queryAll();
            $proveedores_compra = '';
            foreach($result_prov_com as $resultados){
              $detallecompras = DetalleCompra::find()->where(['=','idDetalleCompra', $resultados['detalle_max']])->one();
              $proveedores = Proveedores::find()->where(['=','codProveedores', $resultados['idProveedor']])->one();
              $proveedores_compra .= '<tr>
                      <td>'.$resultados['idProveedor'].'</td>
                      <td>'.$proveedores->nombreEmpresa.'</td>
                      <td>'.date('d-m-Y', strtotime( $resultados['fecha_max'] )).'</td>
                      <td align="right">'.number_format(@$detallecompras->precioCompra,2).'</td>
                    </tr>';
              $detallecompras = '';
            }
            $html_proveedores_compra = '<table class="items table table-striped">
              <thead>
                <tr>
                  <th><font face="arial" size=2>Id.Prov</font></th>
                  <th><font face="arial" size=2>Nombre proveedor</font></th>
                  <th><font face="arial" size=2>Fech.Compra</font></th>
                  <th style="text-align:right"><font face="arial" size=2>Precio</font></th>
                </tr>
              </thead>
              <tbody>
                '.$proveedores_compra.'
              </tbody>
            </table>';
            //este otro html me muestra los proveedore que ya cuentan con un pedido de este producto seleccionado con el rango de fecha
            $proveedores_con_producto = '';
            $producto_pedido = ProductoPedido::find()->where(['<>','idProveedor', $idProveedor])
            ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$codProdServicio])->all();
               foreach($producto_pedido as $producto){
                 $orden_compra_prov = OrdenCompraProv::find()->where(['=','idOrdenCompra', $producto['idOrdenCompra']])
                 ->andWhere("estado = :estado", [":estado"=>'Generada'])->one();
                 if (@$orden_compra_prov) {
                   $detalle_orden_compr = DetalleOrdenCompra::find()->where(['=','idOrdenCompra', $producto['idOrdenCompra']])
                   ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$codProdServicio])->one();
                   $proveedores = Proveedores::find()->where(['=','codProveedores', $producto['idProveedor']])->one();
                   $proveedores_con_producto .= '<tr class="danger parpadea text2">
                           <td>' .$proveedores->nombreEmpresa.'</td>
                           <td style="text-align:center">'.date('d-m-Y', strtotime( $orden_compra_prov->fecha_registro )).'</td>
                           <td style="text-align:center">'.$detalle_orden_compr->cantidad_pedir.'</td>
                         </tr>';
                 }
               }
            $html_proveedores_con_pedido = '<table class="items table table-striped">
              <thead>
                <tr>
                  <th><font face="arial" size=2>Nombre proveedor</font></th>
                  <th><font face="arial" size=2>Fecha pedido</font></th>
                  <th><font face="arial" size=2>Cantidad pedido</font></th>
                </tr>
              </thead>
              <tbody>
                '.$proveedores_con_producto.'
              </tbody>
            </table>';
            $comprados = $result_comprados['cantidad_compra'] == null ? 0 : $result_comprados['cantidad_compra'];
            $vendidos = $result_vendidos['cantidad_venta'] == null ? 0 : $result_vendidos['cantidad_venta'];
            $agotamiento= 0;
            $abastecimiento= 0;
            $rendimiento= 0;
            if ($comprados!=0) {
              $agotamiento = number_format(($vendidos*100)/$comprados,1);
              $abastecimiento = number_format(100 - $agotamiento,1);
              $rendimiento = number_format($vendidos/$comprados*100,1);
            }
            $rendimiento_div = '<div class="progress progress-striped active" title="Verde: Abastecimiento ('. $abastecimiento .'%) / Rojo: Agotamiento ('. $agotamiento .'%)">
              <div class="progress-bar progress-bar-success" style="width: '. $abastecimiento .'%"><font size=3>'. $abastecimiento .'%</font></div>
              <div class="progress-bar progress-bar-danger" style="width: '. $agotamiento .'%"><font size=3>'. $agotamiento .'%</font></div>
            </div>
            <div class="progress progress-striped active" title="El porcentaje de rendimiento en consumo es de'. $rendimiento .'%">
              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: '. $rendimiento .'%" >
                <font size=4>'. $rendimiento .'%</font>
              </div>
            </div>';
            $arr = array( 'comprados'=>$comprados,
                          'vendidos'=>$vendidos,
                          'rendimiento_div' => $rendimiento_div,
                          'html_p_c'=>$html_proveedores_compra,
                          'html_p_p'=>$html_proveedores_con_pedido );
            echo json_encode($arr);
          }
        }

        public function actionCompletar_compra()
        {
          if(Yii::$app->request->isAjax){
            $idProveedor = Yii::$app->request->post('idProveedor');
            $idContacto_pedido = Yii::$app->request->post('idContacto_pedido');
            $idTransporte = Yii::$app->request->post('idTransporte');
            $observacion_pedido = Yii::$app->request->post('observacion_pedido');
            $prioridad = Yii::$app->request->post('prioridad');
            $detalle_orden_compr_tem = DetalleOrdenCompraTemporal::find()->where(['=','idProveedores', $idProveedor])->orderBy(['idDetaOrdComTempo' => SORT_ASC])->all();
            if (@$detalle_orden_compr_tem) {
              //creamos la orden de compra
              $orden_compra_prov = new OrdenCompraProv();
              $orden_compra_prov->idProveedor = $idProveedor;
              $orden_compra_prov->fecha_registro = date("Y-m-d");
              $orden_compra_prov->prioridad = $prioridad;
              $orden_compra_prov->idContacto_pedido = $idContacto_pedido;
              $orden_compra_prov->id_transporte = $idTransporte;
              $orden_compra_prov->observaciones = $observacion_pedido;
              $orden_compra_prov->estado = 'Generada';
              if ($orden_compra_prov->save()) { //si la orden de compra es guardada se crea el detalle de la orden de compra
                foreach($detalle_orden_compr_tem as $detalle_orden_compr => $product) {
                  $detalle_orden_compr = new DetalleOrdenCompra();//creamos el detalle de la orden de compra
                  $detalle_orden_compr->idOrdenCompra = $orden_compra_prov->idOrdenCompra;
                  $detalle_orden_compr->codProdServicio = $product['codProdServicio'];
                  $detalle_orden_compr->idProd_prov = $product['idProd_prov'];
                  $detalle_orden_compr->cantidad_sugerida = $product['cantidad_sugerida'];
                  $detalle_orden_compr->cantidad_pedir = $product['cantidad_pedir'];
                  $detalle_orden_compr->save();
                  //luego de guardar el detalle de orden de compra guardo una relacion de producto pedido para
                  //tener un control si ya el producto fue pedido
                  $producto_pedido = new ProductoPedido();
                  $producto_pedido->idOrdenCompra = $orden_compra_prov->idOrdenCompra;
                  $producto_pedido->codProdServicio = $product['codProdServicio'];
                  $producto_pedido->idProveedor = $orden_compra_prov->idProveedor;
                  $producto_pedido->save();
                  //luego voy eliminando el producto en la tabla tbl_detalle_orden_compra_temp una ves se ingresen en la tabla tbl_detalle_orden_compra
                  \Yii::$app->db->createCommand()->delete('tbl_detalle_orden_compra_temp', 'idDetaOrdComTempo = '.(int) $product['idDetaOrdComTempo'] )->execute();
                }
                if ($this->enviar_compra_correo($orden_compra_prov->idOrdenCompra)=='true') {
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> La orden de compra se realizó y se informó al proveedor correctamente ');
                  return $this->redirect(['index']);
                } else {
                  Yii::$app->getSession()->setFlash('info', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> La orden de compra se realizó correctamente, pero ocurrió un problema al enviarla por correo, intente más tarde enviarla desde la lista de compras.');
                  return $this->redirect(['index']);
                }

              } else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Orden de compra no realizada.');
                return $this->redirect(['index']);
              }
            } else {
              Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> NO se encontraron productos en el nuevo pedido, asegúrese de llenar la tabla de productos para nuevo pedido.');
              return $this->redirect(['index']);
            }

          }
        }

        public function actionImprimir_orden_compra()
        {
          $idOrdenCompra = $_GET['idOrdenCompra'];
          echo $this->renderAjax('vista_orden_compra',['idOrdenCompra' => $idOrdenCompra]);
        }

        public function actionEnviar_orden_compra()
        {
          if(Yii::$app->request->isAjax){
            $idOrdenComprar = Yii::$app->request->post('idOrdenCompra');
            if ($this->enviar_compra_correo($idOrdenComprar)=='true') {
              echo '<center><span class="label label-success" style="font-size:15pt;">Orden re-envi@da.</span></center><br>';
            } else {
              echo '<center><span class="label label-warning" style="font-size:15pt;">Disculpe. No se encuentró el correo destinatario. <br> Compruebe la dirección de correo.</span></center><br>';
            }
          }
        }

        public function enviar_compra_correo($idOrdenCompra)
        {
          $ordencompraprov = OrdenCompraProv::find()->where([ 'idOrdenCompra'=>$idOrdenCompra ])->one();
          $contacto_pedidos = ContactosPedidos::find()->where([ 'idContacto_pedido'=>$ordencompraprov->idContacto_pedido ])->one();
          $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
          $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
          $contenido_correo = '
          <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
          <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
          <br>
          <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
            <tbody><tr>
              <td align="center" valign="top">
                <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                  <tbody><tr>
                      <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                        <table>
                          <tbody><tr>
                            <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                              &nbsp;&nbsp;&nbsp;&nbsp;

                            </td>
                            <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                            </td>
                          </tr>
                        </tbody></table>
                      </td>
                  </tr>
                  <tr>
                    <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                     <br> <a ><img width="70" height=""src="'.Url::base(true).'/img/'.$logo_sistema.'" class="CToWUd"></a>
                      <br><br>
                      '.$this->renderAjax('vista_orden_compra',['idOrdenCompra' => $idOrdenCompra]).'
                      <br><br><br>
                    </td>
                  </tr>
                </tbody></table>
              </td>
            </tr>
          </tbody></table>
          <br></div></div><div class="adL">

          </div></div></div> ';
          $subject = "Nueva orden de compra de ".$empresa->nombre." No. ".$idOrdenCompra;
          //Enviamos el correo
          if (@$contacto_pedidos->email) {
            Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$contenido_correo])
            ->setTo($contacto_pedidos->email)//para
            ->setFrom($empresa->email_proveeduria)//de
            ->setSubject($subject)
            ->setHtmlBody($contenido_correo)
            ->send();
            return 'true';
          } else {
            return 'false';
          }

        }

        public function actionObtenerordencompra()
        {
          $orden_compra = $_GET['idOrdenCompra'];
          $ordencompraprov = OrdenCompraProv::find()->where([ 'idOrdenCompra'=>$orden_compra ])->one();
          $contacto_pedidos = ContactosPedidos::find()->where([ 'idContacto_pedido'=>$ordencompraprov->idContacto_pedido ])->one();
          $detalle_orden_compra = DetalleOrdenCompra::find()->where(['idOrdenCompra'=>$orden_compra])->orderBy(['idDetaOrdCom' => SORT_ASC])->all();
          $nombre =  @$contacto_pedidos ? $contacto_pedidos->nombre : 'El contacto fue eliminado';
          $celular =  @$contacto_pedidos ? $contacto_pedidos->celular : '';
          $email =  @$contacto_pedidos ? $contacto_pedidos->email : '';
          return '<div class="col-lg-2">
                    <h3>Observaciones</h3>
                    '.$ordencompraprov->observaciones.'
                    <h3>Contacto Pedido</h3>
                    <strong>Nombre: </strong>'.$nombre.'<br>
                    <strong>Tel-movil: </strong>'.$celular.'<br>
                    <strong>Correo: </strong>'.$email.'<br><br><br>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
          					  <div class="btn-group" role="group">
          					    <a href="#" data-toggle="tab" class="btn btn-default" id="impri_a" onclick="imprimir_orden_compra('.$orden_compra.')">Imprimir</a>
          					  </div>
          					  <div class="btn-group" role="group">
          					    <a href="#" data-toggle="tab" class="btn btn-default" id="enviar_a" onclick="enviar_orden_compra('.$orden_compra.')">Re-envi@r</a>
          					  </div>
          					</div>
                    <br><div id="notificaciones_envio_correo"></div>
                  </div>
                  <div class="col-lg-10">'.$this->renderAjax('orden_compra_detalle',['detalle_orden_compra'=>$detalle_orden_compra]).'</div>';
        }

        public function actionLista_orden_compra()
        {
          $idProveedor  = $_GET['idProveedor'];
          $busqueda_prio = $_GET['busqueda_prio'];
          $busqueda_est = $_GET['busqueda_est'];
          $fecha_reg_compra = $_GET['fecha_reg_compra'] == '' ? '' : date('Y-m-d', strtotime( $_GET['fecha_reg_compra'] ));
          $compra = $_GET['compra'];
          return $this->renderAjax('lista_orden_compra', [
                        'idProveedor' => $idProveedor,
                        'busqueda_prio' => $busqueda_prio,
                        'busqueda_est' => $busqueda_est,
                        'fecha_reg_compra' => $fecha_reg_compra,
                        'idCompra' => $compra ]);
        }
}
