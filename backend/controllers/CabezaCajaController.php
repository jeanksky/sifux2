<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use backend\models\EncabezadoPrefactura;
use backend\models\EncabezadoCaja;
use backend\models\Sistema_empresa;
use backend\models\search\EncabezadoCajaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Compra_update;
use backend\models\Compra_view;
use yii\bootstrap\Modal;//
use backend\models\FormasPago;//
use backend\models\DetalleFacturas;
use backend\models\Funcionario;
use backend\models\Empresa;//
use backend\models\Clientes;//para obtener el cliente
use backend\models\ProductoServicios;
use backend\models\CuentasBancarias;
use backend\models\MovimientoLibros;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\MedioPago;
use backend\models\MovimientoCredito;
use backend\models\UnidadMedidaProducto;
use backend\models\Clave_electronica;
use kartik\mpdf\Pdf;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;
use backend\models\EntidadesFinancieras;
use backend\models\HistorialEnvioEmail;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\jui\DatePicker;
require("mpdf/mpdf.php");
//use mPDF;
use backend\models\Factun;
use yii2tech\crontab\CronTab;
use yii2tech\crontab\CronJob;
use yii\filters\AccessControl;

/**
 * CabezaCajaController implements the CRUD actions for EncabezadoCaja model.
 */
class CabezaCajaController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'only' => ['view','index','addpago','datospago','footer','obtener_correo','report', 'confirmar_facturas_hacienda', 'condicion_venta', 'lista_pendiente', 'agregar_monto_pago', 'delete_medio_pago', 'consulta_estado_hacienda'],
              'rules' => [
                [
                  'actions' => ['view','index','addpago','datospago','footer','obtener_correo','report', 'confirmar_facturas_hacienda', 'condicion_venta', 'lista_pendiente', 'agregar_monto_pago', 'delete_medio_pago', 'consulta_estado_hacienda'],
                  'allow' => true,
                  'roles' => ['@'],
                ]
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EncabezadoCaja models.
     * @return mixed
     */
    public function actionIndex()
    {
      //  $searchModel = new EncabezadoCajaSearch();
      //  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', []);
    }

    //llena el historial del producto deacuerdo a cada movimiento de ingreso del producto a la ventas
    public function historial_producto($idFa, $tipo, $origen)
    {
      //ingresar producto en historial de producto
      $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$idFa])->all();
      foreach ($detalle_venta as $key => $prod) {
        $inventario = ProductoServicios::find()->where(['codProdServicio'=>$prod->codProdServicio])->one();
        $codProdServicio = $inventario->codProdServicio;
        $fecha = date('Y-m-d H:i:s',time());
        $id_documento = $prod->idCabeza_factura;
        $cant_mov = $prod->cantidad;
        if ($tipo=='RES') {
          $cant_ant = $inventario->cantidadInventario + $cant_mov;
          $cant_new =  $inventario->cantidadInventario;
        } else {
          $cant_ant = $inventario->cantidadInventario;
          $cant_new =  $inventario->cantidadInventario + $cant_mov;
        }
        $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
        $usuario = $funcionario->idFuncionario;
        $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
        $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
        $position_h_p->bindParam(":codProdServicio", $codProdServicio);
        $position_h_p->bindParam(":fecha", $fecha);
        $position_h_p->bindParam(":origen", $origen);
        $position_h_p->bindParam(":id_documento", $id_documento);
        $position_h_p->bindValue(":cant_ant", $cant_ant);
        $position_h_p->bindParam(":tipo", $tipo);
        $position_h_p->bindValue(":cant_mov", $cant_mov);
        $position_h_p->bindValue(":cant_new", $cant_new);
        $position_h_p->bindParam(":usuario", $usuario);
        $position_h_p->execute();
      }
    }

    public function finalizar_credito($idC, $tipo, $id_factura_factun, $numero_factura, $clave, $estado_hacienda, $consecutivo_fe, $consecutivo_te)
    {
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $model = $this->findModel($idC);
        $cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
        $facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $model->idCliente])->andWhere("estadoFactura = :estadoFactura AND fecha_vencimiento < :tipofecha", [':estadoFactura'=>'Crédito', ':tipofecha' => date('Y-m-d')])->one();
        if (@$facturasCredito) {
          $cliente->estadoCuenta = 'Cerrada';
          $cliente->autorizacion = 'No';
        }
        $fecha_emision = date('Y-m-d');
        $nuevafecha = strtotime ( '+'.$cliente->diasCredito.' day' , strtotime ( $fecha_emision ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        $fechavencimiento = ($model->tipoFacturacion == 5) ? $nuevafecha : '';
        $credi_saldo = $model->total_a_pagar;
        $nuevoestado = 'Crédito';

          $sql = "UPDATE tbl_encabezado_factura
          SET factun_id = :factun_id,
          fe = :fe, clave_fe = :clave_fe, fecha_emision = :fecha_emision,
          estado_hacienda = :estado_hacienda,
          estadoFactura = :estado,
          fecha_vencimiento = :fechavencimiento,
          estado_detalle = '-',
          credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idC";
          $command = \Yii::$app->db->createCommand($sql);
          $command->bindParam(":estado", $nuevoestado);
          $command->bindParam(":fecha_emision", $fecha_emision);
          $command->bindParam(":fechavencimiento", $fechavencimiento);
          $command->bindParam(":credi_saldo", $credi_saldo);
          $command->bindParam(":factun_id", $id_factura_factun);
          $command->bindParam(":clave_fe", $clave);
          $command->bindParam(":fe", $numero_factura);
          $command->bindParam(":estado_hacienda", $estado_hacienda);
          $command->bindValue(":idC", $idC);
          $command->execute();
          $cliente->save();
          $this->historial_producto($idC, 'SUM', 'Prefactura');
          $this->historial_producto($idC, 'RES', 'Venta');
          $empresa->consecutivo_fe = $consecutivo_fe;
          $empresa->consecutivo_te = $consecutivo_te;
          $empresa->save();
          //if ($tipo==1) {
          //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Acreditación de factura finalizada correctamente, y enviada por correo.');
          /*} else {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Acreditación de factura finalizada correctamente.');
          }*/
    }

    /**
     * Displays a single EncabezadoCaja model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EncabezadoCaja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new EncabezadoCaja();
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing EncabezadoCaja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->idCabeza_Factura]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Deletes an existing EncabezadoCaja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();
    //
    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the EncabezadoCaja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EncabezadoCaja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EncabezadoCaja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //agregar la cantidad de pago para que debuelva el buelto que debe dar al cliente
    public function actionAddpago()
    {
        if(Yii::$app->request->isAjax){
          $id = Yii::$app->request->post('id');
          $factura = $this->findModel($id);
          $servicio = ($factura->subtotal - $factura->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
          if (Yii::$app->params['servicioRestaurante']==true && $factura->ignorar_porc_ser != 'checked') {
            $total = $factura->total_a_pagar + $servicio;
          } else {
            $total = $factura->total_a_pagar;//obtengo el total a pagarde la factura desde update.
          }
          //-------------------aqui consultamos si no hay un monto de pago ya ot_incluido
          $medio_pago = Compra_view::getContenidoMedioPago();
          $suma_monto_pago = 0;
          if (@$medio_pago) {
            foreach ($medio_pago as $key => $value) {
              $suma_monto_pago += $value['monto_cj'];
            }
          }
          //-------------------fin de consulta de monto de pago
              $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
              $moneda_cambio = Moneda::find()->where(['idTipo_moneda'=>$tipocambio->idTipo_moneda])->one();
              $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
              $valor = Yii::$app->request->post('valor');//obtengo valor con lo que va a pagar
              $dollar = Yii::$app->request->post('dollar');
              $resultado = ($valor - $total)+$suma_monto_pago; //debuelvo el valor del vuelto

              if ($dollar=='no') {
                $respuesta = $resultado >= 0 ? 'Vuelto' : 'Faltante';
                $resultado_simbolo = '<small>'.$respuesta.':</small> '.$moneda_local->simbolo.number_format($resultado,2); //imprimo el valor del vuelto
                $arr = array('valorencolones'=>'', 'resultado_simbolo'=>$resultado_simbolo, 'resultado'=>$resultado, 'pago_real'=>$valor);
                echo json_encode($arr);
              } else {
                $cambiodollar = $valor * $tipocambio->valor_tipo_cambio;
                $resultado = ($cambiodollar - $total)+$suma_monto_pago;
                $resultado_dollares = $resultado / $tipocambio->valor_tipo_cambio;
                $valorencolones = 'Valor en moneda local: '.$moneda_local->simbolo.number_format($cambiodollar,2);
                $respuesta = $resultado >= 0 ? 'Vuelto' : 'Faltante';
                $resultado_simbolo = '<small>Tip.Cambio: ' . $moneda_local->simbolo . $tipocambio->valor_tipo_cambio.'<br>'.$moneda_cambio->simbolo.number_format($resultado_dollares,2).'</small><br>'.
                '<small>'.$respuesta.':</small> '.$moneda_local->simbolo.number_format($resultado,2); //imprimo el valor del vuelto
                $arr = array('valorencolones'=>$valorencolones, 'resultado_simbolo'=>$resultado_simbolo, 'resultado'=>$resultado, 'pago_real'=>$cambiodollar);
                echo json_encode($arr);
              }


          }
    }

    private function borrarTodoU()
    {
      Compra_update::setContenidoComprau(array());
      //Compra::setProveedor(null);
      //Compra::setDoc(null);
      Compra_update::setVendedoru(null);
      Compra_update::setClienteu(null);
      Compra_update::setPagou(null);
      Compra_update::setMediou(null);
      Compra_update::setComprobau(null);
      Compra_update::setTotalauxiliar(null);
      Compra_update::setCabezaFactura(null);
      Compra_update::setCuentau(null);
      Compra_update::setFechadepo(null);
     //Compra::setContenidoCheque(array());
    }

    //Esta funcion me devuelve los datos que necesita la modal que se usa para cancelar factura desde el index de cabaza-caja
    public function actionDatospago()
    {
        if(Yii::$app->request->isAjax){
                    $idC = Yii::$app->request->post('idC');//obtengo el valor del id del encabezado
                    $tipo = Yii::$app->request->post('tipo');//obtengo el tipo de pago
                    $total = Yii::$app->request->post('total');//obtengo el total a pagar por esa factura
                    $factura = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$idC])->one();//obtengo el valor de caja para luego obtener el cliente corespondiente a la factura seleccionada desde el index
                    //$idcliente = Yii::$app->request->post('cliente');
                    if ($idC > 0) {
                        Compra_update::setPagou($tipo);//lleno la session de tipo de pago
                        Compra_update::setCabezaFactura($idC);//lleno la session del id encabezado de esa factura
                        Compra_update::setTotalauxiliar($total);//lleno la session del total a cobrar de esa factura

                        //------Modal para Cancelar factura
                        $id = Compra_update::getCabezaFactura();//obtengo id del encabezado

                        $pago = Compra_update::getPagou();//obtengo el tipo de pago con el que va a pagar el cliente

                        $servicio = ($factura->subtotal - $factura->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
                        if (Yii::$app->params['servicioRestaurante']==true && $factura->ignorar_porc_ser != 'checked') {
                          $total_a_pagar = Compra_update::getTotalauxiliar() + $servicio;
                        } else {
                          $total_a_pagar = Compra_update::getTotalauxiliar();//obtengo el total a pagarde la factura desde update.
                        }

                        //obtengo el cliente
                        $cliente_elejido = null;//declaramos una variable del cliente que vamos a mostrar en la pantalla modal de cobro

                        $moneda_local = '°'; //declaro variable de moneda local
                        //Con la siguiente condición hacemos que muestre el nombre del cliente sin inportar si es del sistema o no.
                        $correo_cliente = '';
                        if($cliente = Clientes::find()->where(['idCliente'=>$factura->idCliente])->one())
                        {//si el id es correspondiente a uno que esta en el sistema que imprima el cliente del sistema
                            $cliente_elejido = $cliente->nombreCompleto;
                            $correo_cliente = $cliente->email;
                        }else{//de los contrario
                            if ($factura->idCliente==""){
                                //Si el id no existe es porque no existe en el sistema, esto me obliga a brincar al else
                                $cliente_elejido = 'Cliente no encontrado';
                            }
                            else{//donde la unica opción es mostrar el id del cliente de la factura, que sera en ese caso el nombre del cliente no registrado en el sistema
                                $cliente_elejido = $factura->idCliente;
                            }
                        }
                        //---------------------------------------------------------------------------------------------
                        $modelPago = FormasPago::find()->where(['id_forma'=>$pago])->one(); //obtengo de las formas de pago el tipo de pago que necesito
                        //obtengo los dos ultimos campos para agregar a una factura cancelada
                        //EL medio por el cual se hizo el cobro dependiendo del tipo de pago
                        $medio = '';
                        $comprovacion = '';
                        $cuenta = '';
                        $fechadepo = '';
                        $datos_vuelto = '';
                        $data = ArrayHelper::map(EntidadesFinancieras::find()->all(), 'idEntidad_financiera', 'descripcion');
                        $data2 = ArrayHelper::map(CuentasBancarias::find()->all(), 'numero_cuenta', 'numero_cuenta');
                        if ($modelPago->desc_forma=='Depósito') {
                            Compra_update::setFechadepo(date("d-m-Y"));
                            $medio = Select2::widget([
                                       // 'model' => $model,
                                       // 'attribute' => 'tipoFacturacion',
                                        'name' => '',
                                        'data' => $data,
                                        'options' => ['id'=>'ddl-entidad', 'onchange'=>'obtenMedio(this.value)','placeholder' => 'Seleccione entidad financiera...'],
                                        'pluginOptions' => [
                                            'initialize'=> true,
                                            'allowClear' => true
                                        ],
                                    ]).'<br>';
                            //Y la comprobación de pago dependiendo del medio en que se hizo el cobro
                            $cuenta = DepDrop::widget([
                                       // 'model' => $model,
                                       // 'attribute' => 'tipoFacturacion',
                                        'name' => '',
                                        'data' => $data2,
                                        'type' => DepDrop::TYPE_SELECT2,
                                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                        'options' => ['id'=>'tipoCuentaBancaria', 'onchange'=>'obtenCuenta(this.value)','placeholder' => 'Seleccione...'],
                                        'pluginOptions' => [
                                            'depends'=>['ddl-entidad'],
                                            'initialize'=> true, //esto inicializa instantaniamente junto a entidad financiera
                                            'placeholder' => 'Seleccione cuenta...',
                                            'url' => Url::to(['cabeza-prefactura/cuentas']),
                                            'loadingText' => 'Cargando...',
                                            //'allowClear' => true
                                        ],
                                    ]);
                            $fechadepo = DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName','value'=>date("d-m-Y"), 'options' => ['class'=>'form-control', 'readonly' => true, 'placeholder'=>'Fecha depósito', 'id'=>'fechadepo', 'onchange'=>'obtenFecha(this.value)']]);
                            $comprovacion = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion', 'disabled'=>false, 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value);']);

                        } else if ($modelPago->desc_forma=='Tarjeta') {
                            $medio = Html::input('text', '', '', ['size' => '19', 'id' => 'ddl-entidad', 'class' => 'form-control', 'placeholder' =>' Medio ( MasterCard, VISA, otra )', 'onkeyup' => 'obtenMedio(this.value);']).
                            "<input type='hidden' id='tipoCuentaBancaria' value='-'>";
                            //Y la comprobación de pago dependiendo del medio en que se hizo el cobro
                            $comprovacion = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion', 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value);']);
                        } else if ($modelPago->desc_forma=='Cheque') {
                            $medio = Select2::widget([
                                       // 'model' => $model,
                                       // 'attribute' => 'tipoFacturacion',
                                        'name' => '',
                                        'data' => $data,
                                        'options' => ['id'=>'ddl-entidad', 'onchange'=>'obtenMedio(this.value)','placeholder' => 'Seleccione entidad financiera...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]).
                            "<input type='hidden' id='tipoCuentaBancaria' value='-'>";
                            //Y la comprobación de pago dependiendo del medio en que se hizo el cobro
                            $comprovacion = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion', 'class' => 'form-control', 'placeholder' =>' # Cheque', 'onkeyup' => 'obtenCom(this.value);']);
                        } else {
                          $datos_vuelto = "<tr>
                              <td align='right'><h3>PAGÓ CON:&nbsp;&nbsp;</h3></td>
                              <td align='right' width='150'>( Dolar: <input type='checkbox' class='checkdolar' name='checkdolar' /> )".
                              Html::input('text', 'cantidad de pago', '',
                              ['size' => '10', 'id' => 'pago',
                              'class' => 'form-control input-lg',
                              'onkeypress'=>'return isNumberDe(event)',
                              'style'=>'font-family: Fantasy; font-size: 30pt; text-align: center;',
                              'onkeyup' => 'obtenerPago(event.keyCode,this.value,'.$idC.');'
                              ])."<div id='valorencolones'></div></td>
                          </tr>
                          <tr>
                              <td align='right' valign='bottom'><h3>VUELTO:&nbsp;&nbsp;</h3></td>
                              <td align='right'><h3><div id='vuelto'></div></h3></td>
                          </tr>";
                            $medio = "<input type='hidden' id='ddl-entidad' value='-'>
                                      <input type='hidden' id='tipoCuentaBancaria' value='-'>
                                      <input type='hidden' id='comprobacion' value='-'>
                                      Si gusta ingrese el monto de pago y el sistema le devolverá la diferencia. Buen día.";
                        }
                        $moneda = Moneda::find()->where(['monedalocal'=>'x'])->one(); //obtengo la instancia que contiene la moneda local
                        if (@$moneda) {
                         $moneda_local = $moneda->simbolo; //asigno la moneda local a la variable
                        }
                        //imprimo lo que corresponde el cuerpo de la modal de cobro para cancelar factura
                        echo "<script type='text/javascript'>

                              </script>
                              <div class='well'>
                              <div style='text-align:right;' id='div_correo_cliente'>
                              <div class='col-xs-8'></div>
                              <div class='col-xs-4'>".Html::input('text', 'cantidad de pago', $correo_cliente, ['size' => '30', 'placeholder'=>' Escribe el correo del cliente', 'id' => 'correo_cliente', 'class' => 'form-control input-sm', 'onkeyup' => '', 'disabled' => true])."</div><br>
                              </div>
                                <div class='panel-body'>
                                    <center><h3>".$cliente_elejido."</h3></center>
                                    <div class='col-xs-6'>
                                        <div class='panel panel-primary'>
                                            <div class='panel-heading'><strong>Tipo de pago: </strong>".$modelPago->desc_forma."</div>
                                            <div class='panel-body'>
                                             <div class='col-xs-12'>".$medio." </div>
                                             <div class='col-xs-7'>".$cuenta. " </div>
                                             <div class='col-xs-5'>" .$fechadepo. " </div>
                                             <div class='col-xs-12'>".$comprovacion." </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-xs-6' id='autofocus'>
                                    <table>
                                        <tr>
                                            <td align='right'><h3>TOTAL A PAGAR:&nbsp;&nbsp;</h3></td>
                                            <td align='right'><h3>".$moneda_local.number_format($total_a_pagar,2)."</td>
                                        </tr>
                                        ".$datos_vuelto."

                                    </table>
                                    <input type='hidden' id='vueltoinput'>
                                    <input type='hidden' id='idCliente' value='".$factura->idCliente."'>
                                    </div>
                                </div>
                              </div>";
                    }//fin if comprovacion de encabezado factura

                }//fin if ajax
    }//fin de la funsión

    //Esta función me permite agregar al footer de la modal los botones que daran acción al cobro de la factura
    public function actionFooter()
    {
        if(Yii::$app->request->isAjax){
            $idCa = Yii::$app->request->post('idCa'); //Obtengo el valor del id de cabeza de la factura
            //Imprimo el boton de cancelar que me permite pasar la factura a estado cancelado y el boton salir, para borrar lo de sessión y no hacer nada mas, para que luego tomar una nueva factura a cancelar
            echo '<div id="alerta_cancelar"></div>'.Html::a('Cancelar factura', '#', [
                'class' => 'btn btn-primary',
                //'target'=>'_blank',
                'data-loading-text'=>"Espere, Cancelación en proceso...",
                'id'=>'boton-cancelar',
                'onclick'=>'regresarindex('.$idCa.', this)']).
                ' '.Html::a('Salir', ['/cabeza-prefactura/regresarborrar-caja', 'id'=>$idCa], ['class' => 'btn btn-primary']);
        }
    }

    public function actionObtener_correo()
    {
      if(Yii::$app->request->isAjax){
        $idCliente = Yii::$app->request->post('idCliente');
        if($cliente = Clientes::findOne($idCliente))
        {
            return $cliente->email;
        }else{
            if ($idCliente=="")
            return 'Cliente no asignado';
            else
            return "";
        }
      }
    }

    public function actionLista_pendiente()
    {
      return $this->renderAjax('lista_pendiente', []);
    }

    public function actionAgregar_monto_pago()
    {
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('idCa');
        $nuevo_pago = floatval(Yii::$app->request->post('pago'));
        $tipo_pago = Yii::$app->request->post('tipo_pago');
        $tipo_del_medio = Yii::$app->request->post('ddl_entidad');
        $cuenta_deposito = Yii::$app->request->post('tipoCuentaBancaria');
        $fechadepo = Yii::$app->request->post('fechadepo');
        $comprobacion = Yii::$app->request->post('comprobacion');
        $vueltoinput = Yii::$app->request->post('vueltoinput');

        $prefactura = EncabezadoPrefactura::findOne($id);
        $medio_pago = Compra_view::getContenidoMedioPago();
        $suma_monto_pago = 0.00;
        $contador = 0;
        if (@$medio_pago) {
          foreach ($medio_pago as $key => $value) {
            $contador += 1;
            $suma_monto_pago += $value['monto_cj'];
          }
        }
        if (Yii::$app->params['servicioRestaurante']==true && $prefactura->ignorar_porc_ser != 'checked') {
          $servicio = ($prefactura->subtotal - $prefactura->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
          $total_a_pagar = $prefactura->total_a_pagar + $servicio;
        } else {
          $total_a_pagar = $prefactura->total_a_pagar;
        }
          $pago_acumulado = $nuevo_pago + $suma_monto_pago;
          $pago_final = $pago_acumulado > $total_a_pagar ? ($total_a_pagar-$suma_monto_pago) : $nuevo_pago;
        if ($contador < 4 && $pago_final > 0) {
          $_POST['formpa_cj'] = $tipo_pago;
          $_POST['medio_cj'] = $tipo_del_medio;
          $_POST['comprob_cj'] = $comprobacion;
          $_POST['cuenta_cj'] = $cuenta_deposito;
          $_POST['fechadepo_cj'] = $fechadepo;
          $_POST['monto_cj'] = $pago_final;
          $medio_pago[] = $_POST;
          sleep(1);
          Compra_view::setContenidoMedioPago($medio_pago);
          return true;
        }
      }
    }

    public function actionDelete_medio_pago($id)
    {
      //Descodificamos el array JSON
      $medio_pago = JSON::decode(Yii::$app->session->get('mp_caja'), true);
      //Eliminamos el atributo pasado por parámetro
      unset($medio_pago[$id]); unset($medio_pago[$id]);
      //Volvemos a codificar y guardar el contenido
      Yii::$app->session->set('mp_caja', JSON::encode($medio_pago));
      return true;
    }

    public function cancelar_factura($id, $id_factura_factun, $numero_factura, $clave, $estado_hacienda, $consecutivo_fe, $consecutivo_te, $medio_pago){
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();

                /*$id = Yii::$app->request->post('idCa');
                $id_factura_factun = Yii::$app->request->post('consecutivo');
                $numero_factura = Yii::$app->request->post('numero_factura');
                $clave = Yii::$app->request->post('clave');
                $estado_hacienda = Yii::$app->request->post('estado_hacienda');*/
                $estado = 'Cancelada';
                $fecha_final = date('Y-m-d');
                $factura_emitida = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$id])->one();
                $codigoVendedor = $factura_emitida->codigoVendedor;
                    $mensaje_vuelto = '';
                    $tipo_documento = '04';
                    if ($cliente = Clientes::findOne($factura_emitida->idCliente)) { $tipo_documento = '01'; }
                    /*  $consultar_factun = new Factun();
                      $api_cosult = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Consultar/'.$id_factura_factun;
                      $result_conslt = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_cosult, 'PUT'));
                      $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/XML/'.$id_factura_factun;
                      $api_xml_respuesta = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/RespuestaXML/'.$id_factura_factun;
                      $clave = $numero_factura = $estado_hacienda = $estado_detalle = '';
                      foreach ($result_conslt->data as $key => $value) {
                          //con esto obtenemos los datos de la factura
                          if ($key == 'clave') { $clave = $value; }
                          if ($key == 'numero_documento') { $numero_factura = $value; }
                          if ($key == 'estado_hacienda') { $estado_hacienda = $value; }
                          if ($key == 'detalle_mensaje') { $estado_detalle = $value; }
                        }
                      $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
                      $result_xml_resp = $consultar_factun->ConeccionFactun_sin_pan($api_xml_respuesta, 'GET');*/
                      //almacenamos los datos a la base de datos
                      $factura_emitida->fe = $numero_factura;
                      $factura_emitida->clave_fe = $clave;
                      //$factura_emitida->xml_fe = $result_xml;
                      //$factura_emitida->xml_resp = $result_xml_resp;
                      $factura_emitida->estadoFactura = $estado;
                      $factura_emitida->fecha_final = $fecha_final;
                      $factura_emitida->fecha_emision = $fecha_final;
                      $factura_emitida->factun_id = $id_factura_factun;
                      $factura_emitida->estado_hacienda = $estado_hacienda;
                      $factura_emitida->estado_detalle = '-';
                      //$factura_emitida->estado_detalle = $estado_detalle;
                    if ($factura_emitida->save()) {
                      $empresa->consecutivo_fe = $consecutivo_fe;
                      $empresa->consecutivo_te = $consecutivo_te;
                      $empresa->save();
                      //compruebo que los datos para factura electronica existen

                      $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
                        $estadopago = 'Cancelación';
                        $pago_registrado = false;
                        //buscamos los movimientos de pago
                        //$medio_pago = Compra_view::getContenidoMedioPago();
                        //recorremos los diferentes medios de pagos asignados a la factura
                        foreach ($medio_pago as $key => $value) {
                          $idForma_Pago = $value['formpa_cj'];
                          $tipo_del_medio = $value['medio_cj'];
                          $comprobacion = $value['comprob_cj'];
                          $monto = $value['monto_cj'];
                          $cuenta = $value['cuenta_cj'];
                          $fechadepo = $idForma_Pago!=4 ? date('Y-m-d') : date('Y-m-d', strtotime( $value['fechadepo_cj'] ));
                          $sql2 = "INSERT INTO tbl_medio_pago (idCabeza_factura, idForma_Pago, tipo_del_medio, comprobacion, cuenta_deposito, estado, fecha_deposito, monto)
                                   VALUES (:idCabeza_factura, :idForma_Pago, :tipo_del_medio, :comprobacion, :cuenta_deposito, :estado, :fecha_deposito, :monto)";
                          $command2 = \Yii::$app->db->createCommand($sql2);
                          $command2->bindParam(":idCabeza_factura", $id);
                          $command2->bindParam(":idForma_Pago", $idForma_Pago);
                          $command2->bindParam(":tipo_del_medio", $tipo_del_medio);
                          $command2->bindParam(":comprobacion", $comprobacion);
                          $command2->bindParam(":monto", $monto);
                          $command2->bindParam(":cuenta_deposito", $cuenta);
                          $command2->bindParam(":fecha_deposito", $fechadepo);
                          $command2->bindParam(":estado", $estadopago);
                          if ($command2->execute()) {
                            $pago_registrado = true;
                            $detalle = '';
                            if ($idForma_Pago == 4) {//si el medio de pago en turno es un deposito bancario entra aqui
                              $modelcuentabancaria = CuentasBancarias::find()->where(['idEntidad_financiera'=>$tipo_del_medio])->andWhere("numero_cuenta = :numero_cuenta", [":numero_cuenta"=>$cuenta])->one();

                              $this->crearmovimientolibro($modelcuentabancaria->idBancos, $comprobacion, $id, $monto, $fechadepo);
                              Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> Se ha creado un nuevo movimiento en libro.');
                            }//fin if si es deposito
                            //elimino el medio de pago en turno que ya fue aplicado
                            $medio_pago = JSON::decode(Yii::$app->session->get('mp_caja'), true);
                            unset($medio_pago[$key]); unset($medio_pago[$key]);
                            Yii::$app->session->set('mp_caja', JSON::encode($medio_pago));
                          }
                        }//fin foreach almacenamiento medio pago
                        //$command2->bindValue(":id", $id);
                        if ($pago_registrado == true){
                        Compra_view::setContenidoMedioPago(array());//

                        //modifica historial
                        $this->historial_producto($id, 'SUM', 'Prefactura');
                        $this->historial_producto($id, 'RES', 'Venta');
                        //if ($correo_cliente != '') {
                          //$this->enviar_correo_factura($id, $correo_cliente, $id_factura_factun);
                          /*$date = new DateTime();//obtenemos el tiempo actual del servidor apahe
                          $date->modify('+1 minute');//le aumentamos un minuto
                          $cronJob = new CronJob();//https://github.com/yii2tech/crontab
                          $cronJob->min = strval($date->format('i'));//asignamos los minutos a ejecutar
                          $cronJob->hour = strval($date->format('H'));//asignamos la hora a ejecutar y ejecutamos
                          $cronJob->command = 'php '.str_replace("backend", "", Yii::getAlias('@backend')).'yii enviofactura/factura_mail';

                          $cronTab = new CronTab();
                          $cronTab->setJobs([
                              $cronJob
                          ]);
                          $cronTab->apply();*/
                          //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Factura enviada por correo.');
                        //}
                        $this->borrarTodoU();
                        //$this->actionReport($id);
                        //return $this->render('index', ['id' => $id]);
                        //Yii::$app->response->redirect(['cabeza-caja/index','id'=>$id]);
                        }
                        //$this->crea_historial_producto($id, $codigoVendedor);
                    }//fin actualiza encabezado
                    return true;
            }
    /*public function crea_historial_producto($id, $codigoVendedor)
    {
      $productos = DetalleCompra::find()->where(["=",'idCabeza_Facturar', $id])->all();
          foreach($productos as $position => $product)
          {
            $fecha = date('Y-m-d H:i:s',time());
            $origen = 'Venta';
            $id_documento = $product['idCabeza_factura'];
            $cant_ant = ;
            $tipo = 'RES';
            $cant_mov = $product['cantidad'];
            $cant_new = ;
            $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
            $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
            $position_h_p->bindParam(":codProdServicio", $codProdServicio);
            $position_h_p->bindParam(":fecha", $fecha);
            $position_h_p->bindParam(":origen", $origen);
            $position_h_p->bindParam(":id_documento", $id_documento);
            $position_h_p->bindValue(":cant_ant", $cant_ant);
            $position_h_p->bindParam(":tipo", $tipo);
            $position_h_p->bindValue(":cant_mov", $cant_mov);
            $position_h_p->bindValue(":cant_new", $cant_new);
            $position_h_p->bindParam(":usuario", $codigoVendedor);
            $position_h_p->execute();
          }
    }*/

    public function crearmovimientolibro($idCuenta_bancaria, $comprobacion, $idCabeza_Factura, $monto, $fechadepo)
    {
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
      //$facturas_caja = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
      //habro una instancia para obtener el tipo de moneda de la cuenta con la estoy usando
      $moneda = Moneda::find()->where(['idTipo_moneda'=>$modelcuentabancaria->idTipo_moneda])->one();
      //habro una instancia para obtener el tipo de cambio y compararla con la moneda que ya tengo de la cuenta
      $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
      //abrimos una intancia de movimiento libros donde se procese a crear un movimiento automatizado
      $model_movi_libros = new MovimientoLibros();
      $model_movi_libros->idCuenta_bancaria = $idCuenta_bancaria;
      $model_movi_libros->idTipoMoneda = $modelcuentabancaria->idTipo_moneda;
      $model_movi_libros->idTipodocumento = 2;
      $model_movi_libros->idMedio_pago = null;
      $model_movi_libros->numero_documento = (int) $comprobacion;
      $model_movi_libros->fecha_documento = $fechadepo;//$model_mediopago->fecha_documento;
      $model_movi_libros->monto = $monto;
      $model_movi_libros->tasa_cambio = null;
      $model_movi_libros->monto_moneda_local = 0.00;
      //con esto compruebo si hay que hacer tipo cambio.
      if ($moneda->monedalocal!='x') {
          $model_movi_libros->monto = $monto / $tipocambio->valor_tipo_cambio;
          $model_movi_libros->tasa_cambio = $tipocambio->valor_tipo_cambio;
          $model_movi_libros->monto_moneda_local = $monto;
      }
      $model_movi_libros->beneficiario = $empresa->nombre;
      $model_movi_libros->detalle = 'COBRO DE FACTURA POR TRANSFERENCIA BANCARIA';
      $model_movi_libros->fecha_registro = date("Y-m-d");
      $model_movi_libros->usuario_registra = Yii::$app->user->identity->nombre;
      //guardamos el movimiento en libros, si esto es asi...
      if ($model_movi_libros->save()) {
          //...Debemos modificar el monto de cuentas bancarias correspondiendo al movimiento, en ese caso pago a proveedor
          ////habrimos instancia para guardar en cuentas bancarias correspondioente al movimiento en libros
          //$modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
          //como el tipo de movimiento es debito el saldo en libros de cuenta bancaaria es igual a su misma cantidad menos el monto del nuevo movimiento en debito
          $modelcuentabancaria->saldo_libros += $model_movi_libros->monto;

          //guardamos actualizacion en cuenta bancaria
          if ($modelcuentabancaria->save()) {//y modificamos el saldo en libros para el control de saldo por movimiento
              $libros = MovimientoLibros::find()->where(['idMovimiento_libro'=>$model_movi_libros->idMovimiento_libro])->one();
              $libros->saldo = $modelcuentabancaria->saldo_libros;//porque es hasta aqui que el saldo en libros lleva este monto
              $libros->save();
          }
        }//fin $model_movi_libros->save
        return true;
    }

    public function actionConsulta_estado_hacienda()
    {
      if(Yii::$app->request->isAjax){
        $sistema_empresa = new Sistema_empresa();
        return json_encode($sistema_empresa->estado_hacienda());
      }//fin if ajax
    }

    //cremos la factura electronica
    public function actionFactura_electronica()
    {
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('idCa');
        $vuelto = Yii::$app->request->post('vueltoinput') == '' || Yii::$app->request->post('vueltoinput') < 0 ? '0.00': number_format(Yii::$app->request->post('vueltoinput'),2);
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $factura_emitida = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$id])->one();
        $tipo_documento = @Clientes::findOne($factura_emitida->idCliente) ? '01' : '04';
        $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
              //GENERAMOS LA CLAVE Y EL CONSECUTIVO ELECTRONICO PARA POSTERIORMENTE EN SEGUNDO PLANO EFECTUAR EL ENVIO AL API FACTUN
              $clave = $estado_hacienda = $numero_factura = $id_factura_factun = '0';
              $tran_api = Yii::$app->db->beginTransaction();
              try{
                $medio_pago = Compra_view::getContenidoMedioPago();
                $medio_pago = $factura_emitida->tipoFacturacion != 5 ? $medio_pago : '1';
                if ($medio_pago) {
                $mensajes_error = '';
                if ( $empresa->factun_conect == 1 ) {//comprovamos que sea factura electronica
                  $sql = "SELECT LPAD (consecutivo_fe, 10, '0') AS fe,  LPAD (consecutivo_te, 10, '0') AS te, consecutivo_fe, consecutivo_te, ced_juridica, sucursal FROM tbl_empresa WHERE idEmpresa = 1";
                  $command = \Yii::$app->db->createCommand($sql);
                  $this_empresa = $command->queryOne();
                  $terminal = '00001';
                  $sucursal = $this_empresa['sucursal'];
                  $tipoDocumento = $tipo_documento == '01' ? 'FE' : 'TE';
                  $tipoCedula = Yii::$app->params['tipo_empresa'];//fisico, juridico, dimex o nite
                  $cedula = $this_empresa['ced_juridica'];
                  $situacion = 'normal';
                  $codigoPais = '506';
                  $consecutivo = $tipo_documento == '01' ? $this_empresa['fe'] : $this_empresa['te'];
                  $consecutivo_fe = $this_empresa['consecutivo_fe'];
                  $consecutivo_te = $this_empresa['consecutivo_te'];
                  if ($tipo_documento == '01') {
                    $consecutivo_fe += 1;
                  } else {
                    $consecutivo_te += 1;
                  }
                  $codigoSeguridad = '2605'.substr($consecutivo, 6);
                  $clave_electronica = new Clave_electronica();
                  $datos_electronicos = $clave_electronica->getClave($terminal, $sucursal, $tipoDocumento, $tipoCedula, $cedula, $situacion, $codigoPais, $consecutivo, $codigoSeguridad);
                  foreach ($datos_electronicos as $key => $value) {
                    if ($key == 'clave') { $clave = $value; }
                    if ($key == 'consecutivo') { $numero_factura = $value; }
                  }
                  $id_factura_factun = 0;
                  $json_result_facturacion = '{
                    "id":1,
                    "data":{"clave":"-"}
                  }';
                } else {//aqui procese si no es requisito factura electronica
                  $clave = '-';
                  $estado_hacienda = '00';
                  $numero_factura = '0';
                  $id_factura_factun = -1;
                  $json_result_facturacion = '{
                    "id":1,
                    "data":{"clave":"-"}
                  }';
                }


                //devuelve valores del resultado de factun
                $idForma_Pago = (int)$factura_emitida->tipoFacturacion;
                $mensaje_vuelto = '<br><center><h1>
                Vuelto '.$moneda_local->simbolo.$vuelto.'</h1></center>';
                $resultado_vuelto = '';

                //consultamos el documento electronico que acabamos de procesar
                if ($clave != '0') {
                  if ($factura_emitida->tipoFacturacion != 5) {//cancelamos la factura si no es acredito
                    $this->cancelar_factura($id, $id_factura_factun, $numero_factura, $clave, '10', $consecutivo_fe, $consecutivo_te, $medio_pago);
                    $resultado_vuelto = '<center>
                            <h1>
                              <span class="glyphicon glyphicon-ok-sign"></span>
                              <strong>Éxito!</strong> Factura cancelada correctamente.'.$mensaje_vuelto.'
                            </h1>
                            <input type="button" value="Salir" onClick="self.close();" onKeyPress="self.close();" style="text-decoration: none; padding: 10px; font-weight: 600; font-size: 20px; color: #ffffff; background-color: #1883ba; border-radius: 6px; border: 2px solid #0016b0;" />
                          </center>';
                  } else {//acreditamos la prefactura
                    $this->finalizar_credito($id, 0, $id_factura_factun, $numero_factura, $clave, '10', $consecutivo_fe, $consecutivo_te);
                    $resultado_vuelto = '<center>
                            <h1>
                              <span class="glyphicon glyphicon-ok-sign"></span>
                              <strong>Éxito!</strong> Factura firmada y acreditada
                            </h1>
                            <input type="button" value="Salir" onClick="self.close();" onKeyPress="self.close();" style="text-decoration: none; padding: 10px; font-weight: 600; font-size: 20px; color: #ffffff; background-color: #1883ba; border-radius: 6px; border: 2px solid #0016b0;" />
                          </center>';
                  }
                }//fin if si no existe clave
               $arr = [
                        'json_result_facturacion'=>$json_result_facturacion,
                        'resultado_vuelto'=>$resultado_vuelto,
                        'mensajes_error'=>$mensajes_error
                      ];
            } else {
              /*si ocurre un error en toda la transaccion hace un rollBack y muestra el error*/
              $json_result_facturacion = '{
                "id":1,
                "data": {},
                "mensajes":{
                  "mensaje":"Ups, se bórro el método de pago",
                  "detalle_mensaje":"Algo pasó en el ultimo momento, disculpa!"
                }
              }';
              if ( Yii::$app->params['caja_multiple'] == true) {
                $arr = [
                         'json_result_facturacion'=>$json_result_facturacion,
                         'resultado_vuelto'=>'',
                         'mensajes_error'=>"De su parte no tiene que solucionar nada, la factura está creada en el módulo de caja general, vuelva a emitir la factura, disculpa el inconveniente."
                       ];
              } else {
                $arr = [
                         'json_result_facturacion'=>$json_result_facturacion,
                         'resultado_vuelto'=>'',
                         'mensajes_error'=>"Vuelva a seleccionar la factura, o refresque de nuevo la pantalla si es necesario e ingrese de nuevo el método de pago."
                       ];
              }
            }//fin else
            $tran_api->commit();
              } catch (\Exception $e) {
                /*si ocurre un error en toda la transaccion hace un rollBack y muestra el error*/
                $tran_api->rollBack();
                $json_result_facturacion = '{
                  "id":1,
                  "data": {},
                  "mensajes":{
                    "mensaje":"Se encontraron problemas de progración.",
                    "detalle_mensaje":"Consulte a su técnico local."
                  }
                }';
                $arr = [
                         'json_result_facturacion'=>$json_result_facturacion,
                         'resultado_vuelto'=>'',
                         'mensajes_error'=>"Fallo: " . $e->getMessage()
                       ];
              }
             return json_encode($arr);
      }//fin if ajax
    }//fin de la funcion

    //esta funcion envia las facturas a hacienda, las que estan en estado hacienda "10"
    public function enviar_factura_hacienda($id)
    {
      $consultar_factun = new Factun();
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $factura_emitida = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$id])->one();
      $numero_original = strval($factura_emitida->idCabeza_Factura);
      $fecha_documento = $factura_emitida->fecha_emision.'T'.date('H:i:s',time());//date('Y-m-d').'T'.date('H:i:s',time());//strval($factura_emitida->fecha_final);
      $id_externo = strval($factura_emitida->idCabeza_Factura);
      $plazo = $diasCredito = '';//para los clientes con credito
      $identificacion = $tipo_identificacion = $nombre_cliente = ''; //datos de clientes
      $provincia = $canton = $distrito = $barrio = $otras_senas = $telefono = '0';
      $tipo_documento = @Clientes::findOne($factura_emitida->idCliente) ? '01' : '04';
      $cliente_iv_exonerado = false;
      if ($tipo_documento == '01') {//Si es factura electronica
        $cliente = Clientes::findOne($factura_emitida->idCliente);
        $identificacion = strval($cliente->identificacion);
        if ($cliente->tipoIdentidad == 'Física') {
          $tipo_identificacion = '01';
        } else if($cliente->tipoIdentidad == 'Jurídica'){
          $tipo_identificacion = '02';
        } else if($cliente->tipoIdentidad == 'DIMEX'){
          $tipo_identificacion = '03';
        } else {
          $tipo_identificacion = '04';
        }
        $nombre_cliente = $cliente->nombreCompleto;
        $email = $cliente->email;
        $diasCredito = strval($cliente->diasCredito);
        $provincia = $cliente->provincia;
        $canton = $cliente->canton;
        $distrito = $cliente->distrito;
        $barrio = $cliente->barrio;
        $otras_senas = $cliente->direccion;
        $telefono = $cliente->celular;
        $identificacion_cliente = [
          'tipo' => $tipo_identificacion,
          'numero' => $identificacion
        ];
        $cliente_iv_exonerado = $cliente->iv == 1 ? true : false;
      } else { // de lo contrario si es tiquete electronico
        $nombre_cliente = $factura_emitida->idCliente;
        $email = NULL;
        $identificacion_cliente = [];
        $cliente_iv_exonerado = false;
      }
      $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
      $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
      $condicion_venta = '01'; //01 = Contado, 02 = Crédito, 03 = Consignación, 04 = Apartado, 05 = Arrendamiento con opción de compra, 06 = Arrendamiento en función financiera, 99 = Otros
      $tipo_pago = [];
      //obtengo la forma de pago en el sistema para enviar el tipo_pago
      if ($factura_emitida->tipoFacturacion == 5) {//Credito
        $condicion_venta = '02';
        $plazo = $diasCredito.' dias.';
        $tipo_pago = ['01'];
      } else {
        $medio_pago = MedioPago::find()->where(['idCabeza_factura'=>$id])->all();
        foreach ($medio_pago as $key => $value) {
          $tipo_pago[] = '0'.strval($value['idForma_Pago']);
        }
      }

      $resultado = 0;
         //RECORRER DETALLE
         $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$factura_emitida->idCabeza_Factura])->all();
         $array_detalle[] = null; $codigo = 'ORD_SERV'; $descripcion = 'ORD_SERV'; $unidad = 'Sp'; $servicio = true;//datos del detalle
         //$moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
         $tarifa_impuesto = Yii::$app->params['porciento_impuesto'];
         $tipo_impuesto = '01';
         //buscamos y recorremos el DETALLE DE LA FACTURA
         //Impuesto General sobre las ventas
         $impuesto[] = [];
         if ($cliente_iv_exonerado == true) {//entra aqui se es un cliente exonerado
             $tipo_exoneracion = '03'; //01 = Compras Autorizadas, 02 = Ventas exentas a diplomáticos, 03 = Orden de compra(instituciones públicas y otros organismos), 04 = Exenciones Dirección General de Hacienda, 05 = Zonas Francas, 99 = Otros
             $numero_documento_exonerado = 1;
             $nombre_instituto_exonerado = $nombre_cliente;
             $fecha_documento_exonerado = $fecha_documento;
             $porcentaje_exonerado = 100;
             $impuesto[] = [
                            'codigo' => $tipo_impuesto,
                            'tarifa' => $tarifa_impuesto,
                            'exoneracion' => [
                                              'tipo_documento' => $tipo_exoneracion,
                                              'numero_documento' => $numero_documento_exonerado,
                                              'nombre_institucion' => $nombre_instituto_exonerado,
                                              'fecha_emision' => $fecha_documento_exonerado,
                                              'porcentaje_compra' => $porcentaje_exonerado
                                             ]
                           ];
          }
         $key_gl = 0; $c_gl = 0;
         //recorremos el detalle de la factura
         foreach ($detalle_venta as $key => $value) {
           $key_gl = $key;
           $c = $key+1;//contador
           $c_gl = $c;
           //hacemos esta condicion a pesar de que casi siempre son productos los que ingresan, pero como existen
           //las ordenes de servicio y al no ser un producto como tal se clasifican a parte
           if ($producto = ProductoServicios::find()->where(['codProdServicio'=>$value->codProdServicio])->one()) {
             $unid_med = UnidadMedidaProducto::find()->where(['idUnidadMedida'=>$producto->idUnidadMedida])->one();
             $codigo = $producto->codProdServicio;
             $descripcion = $producto->nombreProductoServicio;
             $unidad = $unid_med->simbolo;
             $servicio = $unidad == 'Sp' ? true : false;

             $tipo_impuesto = $unidad == 'Sp' ? '07' : '01';
             if ($cliente_iv_exonerado != true) {//entra aqui ssi no paso por cliente exonerado
                 if ($producto->exlmpuesto == 'No') {
                     $tarifa_impuesto = $unidad == 'Sp' ? 6.5 : Yii::$app->params['porciento_impuesto'];
                     $impuesto[$key] = [ 'codigo' => $tipo_impuesto, 'tarifa' => $tarifa_impuesto ];
                   } else {
                     $impuesto[$key] = [ 'codigo' => '98', 'tarifa' => 0.00 ];
                   }
               }//si no es exonerado se ejecuta el impuesto normal

           }
           //detalle de la factura por linea
           $array_detalle[$key] = [//creamos el array para montar todos los productos para el arreglo de detalle
                   'numero_linea'=>$c,
                   'tipo_codigo' => '01',// 01 = Codigo del producto del vendedor, 02 = Codigo del producto del comprador, 03 = Codigo del producto asignado por la industria, 04 = Codigo de uso interno, 99 = Otros
                   'codigo' => substr($codigo, 0, 20),
                   'cantidad' => $value->cantidad,
                   'unidad_medida' => $unidad,
                   //'unidad_medida_comercial' => NULL,
                   'detalle' => substr($descripcion, 0, 150),
                   'precio_unitario' => $value->precio_unitario,
                   'monto_descuento' => $value->descuento_producto,
                   'naturaleza_descuento' => $value->descuento_producto > 0.00 ? 'Oferta' : '',
                   'impuesto' => $impuesto,
                   'servicio' => $servicio,
                   'bonificacion' => false
                  ];
                  if ($cliente_iv_exonerado != true) { $impuesto[$key] = []; }//vaciamos el impuesto
         }//fin de foreach del detalle de la factura
         //si la factura incluye el % de servicio de restautante, se agrega aqui
         if (Yii::$app->params['servicioRestaurante']==true && $factura_emitida->ignorar_porc_ser != 'checked') {
           $precio_servicio_rest = ($factura_emitida->subtotal - $factura_emitida->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
           if ($cliente_iv_exonerado != true) {
             $impuesto[$key_gl+1] = [ 'codigo' => '98', 'tarifa' => 0.00 ];
           }
           $array_detalle[$key_gl+1] = [//creamos el array para montar la linea por el servicio 10%
                   'numero_linea'=>$c_gl+1,
                   'tipo_codigo' => '01',// 01 = Codigo del producto del vendedor, 02 = Codigo del producto del comprador, 03 = Codigo del producto asignado por la industria, 04 = Codigo de uso interno, 99 = Otros
                   'codigo' => '_SERVIREST',
                   'cantidad' => 1,
                   'unidad_medida' => 'Unid',
                   //'unidad_medida_comercial' => NULL,
                   'detalle' => 'SERVICIO DE RESTAURANTE',
                   'precio_unitario' => $precio_servicio_rest,
                   'monto_descuento' => 0.00,
                   'naturaleza_descuento' => '',
                   'impuesto' => $impuesto,
                   'servicio' => true,
                   'bonificacion' => false
                  ];
            if ($cliente_iv_exonerado != true) { $impuesto[$key_gl+1] = []; }//vaciamos el impuesto
         }
           $factura = [//seteamos parametros de la factura
                'id_externo' => $numero_original,
                'clave' => $factura_emitida->clave_fe,
                'tipo_documento' => $tipo_documento, //factura electronica o tiquete electronico
                'fecha_emision' => $fecha_documento,
                'medio_pago' => $tipo_pago, //['01','04'] //
                'receptor' => [
                                'razon_social' => $nombre_cliente,
                                'identificacion' => $identificacion_cliente,
                                'identificacion_extranjero' => null,
                                'razon_comercial' => $nombre_cliente,
                                'ubicacion' => [
                                                  "provincia" => $provincia,
                                                  "canton" => $canton,
                                                  "distrito" => $distrito,
                                                  "barrio" => $barrio,
                                                  "otras_senas" => $otras_senas
                                               ],
                                'telefono' => [
                                                "codigo_pais" => "",
                                                "num_telefono" => ""
                                              ],
                                'fax'=> '',
                                //'correo_electronico' => $email
                              ],
                'condicion_venta' => $condicion_venta,//crédito o contado
                'plazo_credito' => $plazo,
                'detalles' => $array_detalle,//agregamos el detalle de la factura
                'resumen_documento' => [
                                        "codigo_moneda" => $moneda_local->abreviatura,
                                        "tipo_cambio" => $tipocambio->valor_tipo_cambio
                                       ]

             ];

      $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/Enviar';
      $json_result_facturacion = $consultar_factun->ConeccionFactun_con_pan($api_dir, $factura, 'POST');
      $array_result_facturacion = json_decode($json_result_facturacion);

      //recorro los datos de respuesta
      $numero_factura = $id_factura_factun = 0;
      foreach ($array_result_facturacion->data as $key => $value) {
        if ($key=='clave') { $clave = $value; }
        //if ($key=='estado_hacienda') { $estado_hacienda = $value; }
        if ($key=='numero_factura') { $numero_factura = $value; }
        if ($key=='consecutivo') { $id_factura_factun = $value; }
      }
      //recorro las notificaciones de error
      $mensajes_error = '';
      foreach ($array_result_facturacion->mensajes as $key => $value) {
        if ($key!='codigo_mensaje'&&$key!='mensaje'&&$key!='detalle_mensaje') {
          $mensajes_error .= '<br> > '.$value;
        }
      }
      if ($id_factura_factun > 0) {
        $estado_hacienda = '00';
        $sql = "UPDATE tbl_encabezado_factura SET fe = :fe, clave_fe = :clave_fe, factun_id = :factun_id, estado_hacienda = :estado_hacienda WHERE idCabeza_Factura = :id";
        $command = \Yii::$app->db->createCommand($sql);
        $command->bindParam(":fe", $numero_factura);
        $command->bindParam(":clave_fe", $clave);
        $command->bindParam(":factun_id", $id_factura_factun);
        $command->bindParam(":estado_hacienda", $estado_hacienda);
        $command->bindParam(":id", $id);
        $command->execute();
      } else {
        $sql = "UPDATE tbl_encabezado_factura SET estado_detalle = :estado_detalle  WHERE idCabeza_Factura = :id";
        $command = \Yii::$app->db->createCommand($sql);
        $command->bindParam(":estado_detalle", $mensajes_error);
        $command->bindParam(":id", $id);
        $command->execute();
      }
    }//fin funcion de facturar ante hacienda

    //confirmamos las facturas a hacienda, siempre que el estado_hacienda no sea ni 01 o 03
    public function actionConfirmar_facturas_hacienda()
    {
      $facturas = EncabezadoCaja::find()->where(['>','factun_id', -1])
      ->andWhere('estado_hacienda NOT IN ("01","03") AND fecha_emision > "2019-04-01"',[])->all();
      foreach ($facturas as $key => $factura) {
        if ($factura->factun_id > -1) {
          if ($factura->estado_hacienda == '10') {//llama la funcion de facturar ante hacienda, primer proceso
            $this->enviar_factura_hacienda($factura->idCabeza_Factura);
          } else {//es para las facturas que ya fueron al API para consultar estados
            $consultar_factun = new Factun();
            $detalle = $estado_hacienda = '';
            $id = $factura->idCabeza_Factura;
            $clave = $factura->clave_fe;
            $api_cosult = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/Consultar/'.$clave;
            $result_consultar = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_cosult, 'PUT'));
            if($result_consultar != null){
              foreach ($result_consultar->data as $key => $value) {
                if ($key=='detalle') { $detalle = $value; }
                if ($key=='estado_hacienda') { $estado_hacienda = $value; }
              }
              if ($estado_hacienda == '01' || $estado_hacienda == '03') {
                //consultamos el XML
                $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/XML/'.$clave;
                $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
                //consultamos el XML respuesta
                $api_xml_respuesta = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/RespuestaXML/'.$clave;
                $result_xml_resp = $consultar_factun->ConeccionFactun_sin_pan($api_xml_respuesta, 'GET');
              } else {
                $result_xml = '';
                $result_xml_resp = '';
              }
              $sql = "UPDATE tbl_encabezado_factura SET xml_fe = :xml_fe, xml_resp = :xml_resp, estado_hacienda = :estado_hacienda, estado_detalle = :estado_detalle WHERE idCabeza_Factura = :id";
              $command = \Yii::$app->db->createCommand($sql);
              $command->bindParam(":xml_fe", $result_xml);
              $command->bindParam(":xml_resp", $result_xml_resp);
              $command->bindParam(":estado_hacienda", $estado_hacienda);
              $command->bindParam(":estado_detalle", $detalle);
              $command->bindValue(":id", $id);
              if ($command->execute()) {
                if ($estado_hacienda=='01' && @$cliente = Clientes::find()->where(['idCliente'=>$factura->idCliente])->one()) {
                  $this->enviar_correo_factura($id, $cliente->email, $facturas->factun_id);
                }
              }
              if ($estado_hacienda=='05') {
                $api_facturar = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/Reenviar/'.$clave;
                $facturar = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_facturar, 'PUT'));
              }//fin de estado hacienda si es 05
            }//fin if de consultar a factun
          }//fin else de facturas que ya fueron enviadas al API y requieren comprovacion de hacienda

          //$movimiento = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
          //$movimiento->estado_hacienda = $estado_hacienda;
          //$movimiento->save();
        }//fin if de factuas que son electronicas
      }//fin foreach que recorre las facturas
      //$estado_movimiento = $this->render('/devoluciones-producto/consultar_estados_movimiento');
      echo '<br><center><span class="label label-success" style="font-size:12pt;">Facturas sincronizadas con Hacienda correctamente.</span></center>';
    }

    //enviar proforma al correo desde una llamada a la funcion
    public function enviar_correo_factura($id, $email, $id_factun)
    {
      //$consultar_factun = new Factun();
        $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $model = $this->findModel($id);
            if (@$model) {

                $encabezado = '<table>
                    <tbody>
                    <tr>
                    <td>
                    <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                    </td>
                    <td style="text-align: left;">
                    <strong>'.$empresa->nombre.'</strong><br>
                    <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                    <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                    <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                    <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                    <strong>Dirección:</strong> '.$empresa->email.'
                    </td>
                    <tr>
                    </tbody>
                </table>';
                $informacion_factura_cancelada = $this->renderAjax('view',['model' => $model]);
                $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
                $content = '
                <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO">
                  <div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
                    <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
                      <br>
                      <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
                        <tbody><tr>
                          <td align="center" valign="top">
                            <table width="80%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                              <tbody>
                                <tr>
                                  <td align="right" style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">

                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                                   <br>
                                    <br><br>
                                    '.$encabezado.'
                                    <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">ENVÍO DE FACTURA # '.$id.'<br><small> '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>
                                    '.$informacion_factura_cancelada.'
                                    <br><br><br><br><br><br><br><br><br>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                                    Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                      </table>
                    <br><br>
                    </div>
                  </div>
                </div>
                ';


                //CONSULTAR
                $clave = $numero_factura = $estado_hacienda = '';
                $stylesheet = file_get_contents('css/style_print.css');
                if ($id_factun == -1) {//sin factura electronica  |||||||||||||||||||||||||||||||||||||||||||||||||
                  $pdf_html = $this->pdf_html($model, $id_factun, '', '');
                  //$comprobante_electronico = 'No cuenta con comprobante electrónico';
                  $archivo_pdf = 'Factura No. '.$id.'.pdf';
                  /*$pdf = new mPDF();
                  $pdf->title = 'Factura No. '.$id;
                  //$pdf->SetHtmlHeader($comprobante_electronico);
                  $pdf->WriteHTML($stylesheet,1);
                  $pdf->WriteHTML($pdf_html,2);
                  //$pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());
                  $pdf->Output($archivo_pdf,'');*/
                  $caja_pdf = new EncabezadoCaja();
                  $caja_pdf->_pdf($id, $stylesheet, $pdf_html, $archivo_pdf);
                  //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                  $subject = "Nueva factura por compra a ".$empresa->nombre;
                  //Enviamos el correo
                  Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                  ->setTo($email)//para
                  ->setFrom($empresa->email_contabilidad)//de
                  ->setSubject($subject)
                  ->setHtmlBody($content)
                  ->attach($archivo_pdf)//->attach($adjunto[2])//se agragan todos los adjuntos
                  ->send();
                  unlink($archivo_pdf);//eliminamos el archivo pdf (que teniamos temporal) para que no se quede en el servidor
                  $histrial_email = new HistorialEnvioEmail();
                  $histrial_email->tipo_documento = 'FA';
                  $histrial_email->idDocumento = $id;
                  $histrial_email->fecha_envio = date('Y-m-d H:i:s',time());
                  $histrial_email->email = $email;
                  $histrial_email->usuario = Yii::$app->user->identity->username;
                  $histrial_email->save();
                  sleep(5);
                } else { //con factura electronica  |||||||||||||||||||||||||||||||||||||||||||||||||
                  //obtengo los datos de la factura desde factun
                  //$result_factura = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_consultar, 'GET'));
                  $numero_factura = $model->fe;
                  $estado_hacienda = $model->estado_hacienda;
                  $clave = $model->clave_fe;
                    $pdf_html = $this->pdf_html($model, $id_factun, $clave, $numero_factura);
                    if ($estado_hacienda=='01') {
                      //$comprobante_electronico = 'Comprobante electrónico: ' . $numero_factura;
                      $result_xml = $model->xml_fe;//consultamos el xml
                      $archivo_xml = 'FE_'.$clave.".xml";//creamos el nombre del archivo xml, y como de aqui se obtiene no se le dice en que directorio esta
                      $archivo_xml_resp = 'FE_RESPUESTA_'.$model->clave_fe.".xml";
                      $archivo_pdf = $clave.'.pdf';//creamos el nombre del arhivo pdf, ...
                      //creamos el xml en el servidor (estará temporalmente)
                      $archivo = fopen($archivo_xml, "a+");//creamos el archivo xml y con permiso de lectura y escritura
                      fwrite($archivo, $result_xml);//escribimos los datos xml en el archivo
                      fclose($archivo);//cerramos el archivo
                      //creamos el archivo xml de respuesta que esta en bd
                      $archivo2 = fopen($archivo_xml_resp, "a+");//creamos el archivo xml y con permiso de lectura y escritura
                      fwrite($archivo2, $model->xml_resp);//escribimos los datos xml en el archivo
                      fclose($archivo2);//cerramos el archivo
                      //creamos el Pdf en el servidor (estará temporalmente)
                      $caja_pdf = new EncabezadoCaja();
                      $caja_pdf->_pdf($id, $stylesheet, $pdf_html, $archivo_pdf);
                      //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                      $subject = "Nueva factura por compra a ".$empresa->nombre;
                      //Enviamos el correo
                      Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                      ->setTo($email)//para
                      ->setFrom($empresa->email_contabilidad)//de
                      ->setSubject($subject)
                      ->setHtmlBody($content)
                      ->attach($archivo_xml)->attach($archivo_xml_resp)->attach($archivo_pdf)//->attach($adjunto[2])//se agragan todos los adjuntos
                      ->send();
                      unlink($archivo_xml);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
                      unlink($archivo_pdf);//eliminamos el archivo pdf (que teniamos temporal) para que no se quede en el servidor
                      unlink($archivo_xml_resp);
                      $histrial_email = new HistorialEnvioEmail();
                      $histrial_email->tipo_documento = 'FA';
                      $histrial_email->idDocumento = $id;
                      $histrial_email->fecha_envio = date('Y-m-d H:i:s',time());
                      $histrial_email->email = $email;
                      $histrial_email->usuario = Yii::$app->user->identity->username;
                      $histrial_email->save();
                      sleep(5);
                      echo '<center><span class="label label-success" style="font-size:12pt;">Enviado al correo correctamente.</span></center><br>';
                    }//fin consulta si esta aprovado por hacienda
                    else {
                      echo '<center><span class="label label-warning" style="font-size:12pt;">Disculpe, no fue posible enviar factura al correo.</span></center><br>';
                    }
                }//fin if factura electronica
              //  echo '<center><span class="label label-success" style="font-size:12pt;">Factura enviada correctamente.</span></center><br>';

            } //fin @$model
            //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> La cotización fué enviada correctamente.');
            //$this->redirect(['update', 'id' => $id]);
    }// fin de envio_email

    public function pdf_html($model, $id_factun, $clave, $numero_factura)
    {
      $empresaimagen = new Empresa();
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $model->idCabeza_Factura])->all();
      $detalle_factura = '';
      if($products) {
        foreach($products as $position => $product) {
          $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
          $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
          $detalle_factura .= '<tr>
            <td class="no">'.$product['cantidad'].'</td>
            <td class="cod" style="font-size: 0.8em;">'.$product['codProdServicio'].'</td>
            <td class="desc" style="font-size: 0.8em;"><strong>'.$descrip.'</strong></td>
            <td class="unit" style="font-size: 0.8em;">'.number_format($product['precio_unitario'], 2).'</td>
            <td class="qty" style="font-size: 0.8em;">'.number_format($product['descuento_producto'],2).'</td>
            <td class="unit" style="font-size: 0.8em;">'.number_format($product['subtotal_iva'],2).'</td>
            <td class="total">'.number_format(($product['precio_unitario']*$product['cantidad']),2).'</td>
          </tr>';
        }
      }

      $info_cliente = $info_invoice = $tipo_documento = '';
      if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
          $tipo_documento = 'FE';
          $info_cliente = '
          <h2 class="name">'.$cliente->nombreCompleto.'<br>ID: '.$cliente->identificacion.'</h2>
          <div class="address">'.$cliente->direccion.'</div>
          <div class="email">'.$cliente->celular.' / '.$cliente->telefono.'<br><a href="mailto:'.$cliente->email.'">'.$cliente->email.'</a></div>';
      } else{
        $tipo_documento = 'TE';
          $info_cliente = '<h2 class="name">'.$model->idCliente.'</h2>';
      }
      $tipo_pago = $feven = '';
      if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
          $tipo_pago = $modelP->desc_forma;
          if ($model->estadoFactura!='Crédito') {
            $feven = '';
          }
          else {
            $feven = 'Fecha de vencimiento: '.$model->fecha_vencimiento;
          }
      }
      if ($id_factun == -1) {
        $info_invoice = '
        <div class="date">Fecha de emisión: '.$model->fecha_inicio.'</div>
        <div class="date">'.$feven.'</div>';
      } else {
        $info_invoice = '
        <h2 class="name" style="color: #57B223;">'.$tipo_documento.': '.$numero_factura.'</h2>
        <div class="date">Fecha de emisión: '.$model->fecha_inicio.'</div>
        <div class="date">'.$feven.'</div>
        <div style="font-size: 0.8em;  color: #FF0000;">Clave: '.$clave.'</div>';
      }

      $footer_pdf = $id_factun == -1 ? 'Autorizado mediante resolución 11-97 del 05/09/1997 de la D.G.T.D' : 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';
      $servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
      if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
        $servicio_restaurante = '<tr>
                         <td colspan="3"></td>
                         <td colspan="3">SERVICIO 10%: </td>
                         <td>'.number_format($servicio,2).'</td>
                     </tr>';
        $subtotal = $model->subtotal+$servicio;
        $total_a_pagar = $model->total_a_pagar+$servicio;
      } else {
        $servicio_restaurante = '';
        $subtotal = $model->subtotal;
        $total_a_pagar = $model->total_a_pagar;
      }
      return '<header class="clearfix">
            <div id="company">
              <table>
              <tr>
              <td>
              <img src="'.$empresaimagen->getImageurl('pdf').'" height="10%">
              </td>
              <td>
                <div id="company">
                  <h2 class="name">'.$empresa->nombre.' - ID: '.$empresa->ced_juridica.'</h2>
                  <div>'.$empresa->localidad.' - '.$empresa->direccion.'</div>
                  <div>'.$empresa->telefono.' / '.$empresa->fax.'</div>
                  <div><a href="mailto:'.$empresa->sitioWeb.'">'.$empresa->sitioWeb.'</a> | <a href="mailto:'.$empresa->email.'">'.$empresa->email.'</a></div>
                </div>
              </td>
              </tr>
              </table>
            </div>
      </header>
      <main>
        <div id="details" class="clearfix">
          <table class="table_info">
            <tr>
              <th class="derecha">
                <div id="client">
                  <div class="to">Factura a:</div>
                  '.$info_cliente.'
                </div>
              </th>
              <th class="izquierda">
                <div id="invoice">
                  <h3 style="color: #0087C3;">Número interno. '.$model->idCabeza_Factura.'</h3>
                  '.$info_invoice.'
                  <h3 style="font-size: 1.0em; color: #0087C3;">Condición Venta: '.$model->estadoFactura.' | Tipo de Pago:	'.$tipo_pago.'</h3>
                </div>
              </th>
            </tr>
          </table>
        </div>
        <table border="0" cellspacing="0" cellpadding="0" class="table_detalle">
          <thead>
            <tr>
              <th class="no">C.</th>
              <th class="cod">COD</th>
              <th class="desc">DESCRIPCIÓN</th>
              <th class="unit">PREC UNIT</th>
              <th class="qty">DESCTO</th>
              <th class="unit">IV %</th>
              <th class="total">TOTAL</th>
            </tr>
          </thead>
          <tbody>
            '.$detalle_factura.'
          </tbody>
          <tfoot>
            '.$servicio_restaurante.'
            <tr>
              <td colspan="3"></td>
              <td colspan="3">SUBTOTAL</td>
              <td>'.number_format($subtotal,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3">TOTAL DESCUENTO</td>
              <td>'.number_format($model->porc_descuento,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3">TOTAL IV</td>
              <td>'.number_format($model->iva,2).'</td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td colspan="3"><strong>TOTAL A PAGAR</strong></td>
              <td><strong>'.number_format($total_a_pagar,2).'</strong></td>
            </tr>
          </tfoot>
        </table>
        <div id="thanks">¡Gracias por su preferencia!</div>
        <div id="notices">
          <div>Observación:</div>
          <div class="notice">'.$model->observacion.'</div>
        </div>
      </main>
      <footer>
        '.$footer_pdf.'
      </footer>';
    }//fin pdf

    public function actionCondicion_venta($id)
    {
      $model = $this->findModel($id);
      echo $this->renderAjax('condicion_venta', ['model' => $model]);
    }

    public function actionTicket($id)
    {
      return $this->render('ticket', ['id' => $id]);
    }

    public function actionOrden($id)
    {
      return $this->render('orden', ['id' => $id]);
    }

    public function actionReport($id)
    {
        $modelestado = $this->findModel($id);//Solo se usa para actualizar estadoFactura y que la imprecion no lleve el estado como "Crédito Caja" sino solo Crédito
        return $this->render('index', ['id' => $id]);
        /*$empresaimagen = new Empresa();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();

        //coneccion con la impresora
        $equipo = 'Dell300914';
        $impresora = 'TSP100';
        $connector = new WindowsPrintConnector("smb://".$equipo."/".$impresora);
        $printer = new Printer($connector);

        if ($modelestado->estadoFactura == 'Crédito Caja') {

                $cliente = Clientes::find()->where(['idCliente'=>$modelestado->idCliente])->one();
                $nuevafecha = strtotime ( '+'.$cliente->diasCredito.' day' , strtotime ( $modelestado->fecha_inicio ) ) ;
                $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
                $fechavencimiento = ($modelestado->tipoFacturacion == 4) ? $nuevafecha : '';
                $credi_saldo = $modelestado->total_a_pagar;

            $nuevoestado = 'Crédito';
                    $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado, fecha_vencimiento = :fechavencimiento, credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idC";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":estado", $nuevoestado);
                    $command->bindParam(":fechavencimiento", $fechavencimiento);
                    $command->bindParam(":credi_saldo", $credi_saldo);
                    $command->bindValue(":idC", $id);
                    $command->execute();
        }
        //Encabezado empresa--------------------------------------------
        //$logo = EscposImage::load($empresaimagen->getImageurl('html'), false);
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer -> text( "\n\n" . $empresa->nombre . "\n");
        $printer -> text( $empresa->localidad.' - '.$empresa->direccion . "\n" );
        $printer -> text( $empresa->sitioWeb.' / '.$empresa->email . "\n" );
        $printer -> text( $empresa->telefono.' / '. $empresa->fax . "\n\n\n" );
        $printer -> selectPrintMode();
        $printer -> feed();
        //Encabezado factura--------------------------------------------
        $modelcaja = $this->findModel($id);//vuelvo a llamar al modelo actual pero esta ves con las modificaciones que se aplicaron
        $printer -> text( 'Factura No: ' . $id ."\n");
        $printer -> text( 'Fecha: ' . $modelcaja->fecha_inicio ."\n\n");
        $printer -> text( "CLIENTE:------------------------------\n");
        if (@$cliente = Clientes::find()->where(['idCliente'=>$modelcaja->idCliente])->one()) {
            $printer -> text( "Nombre: ".$cliente->nombreCompleto . "\n");
            $printer -> text( "Telefono: ".$cliente->telefono . "\n");
            $printer -> text( "Direccion: ".$cliente->direccion . "\n");
        } else {
            $printer -> text( $modelcaja->idCliente . "\n");
        }
        $printer -> text( "--------------------------------------\n\n");

        $printer -> text( 'Estado de la factura: ' . $modelcaja->estadoFactura ."\n");

        //$fecha_final = $modelcaja->fecha_final=='' ? 'Pendiente' : $modelcaja->fecha_final ;
        //$printer -> text( 'Fecha Final: ' . $fecha_final ."\n");

        if(@$modelP = FormasPago::find()->where(['id_forma'=>$modelcaja->tipoFacturacion])->one()){
            if ($modelcaja->estadoFactura!='Crédito') {
                $printer -> text( 'Tipo de pago: ' . $modelP->desc_forma ."\n");
            } else {
                    $printer -> text( 'Fecha Vencimiento: ' . $modelcaja->fecha_vencimiento ."\n");
                }
            }

        $printer -> text( 'Observacion: ' . $modelcaja->observacion ."\n\n\n");

        //Detalle de factura--------------------------------------------
        $products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $modelcaja->idCabeza_Factura])->all();
        $detallefactura = '';
        foreach($products as $product) {
            $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
            $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
            $detallefactura .= new item($product['cantidad']. ' ' . substr($descrip,0,22), number_format(($product['precio_unitario']*$product['cantidad']),2));
        }
        //$printer -> text( "\n\n\n" );
        //Cierre de impresión
        $printer -> text( $detallefactura ."---------------------------------------------\n" );
        $printer -> text(new item("                 SUBTOTAL:", number_format($modelcaja->subtotal,2)));
        $printer -> text(new item("                 TOTAL DESCUENTO:", number_format($modelcaja->porc_descuento,2)));
        $printer -> text(new item("                 TOTAL IV:", number_format($modelcaja->iva,2)));
        $printer -> text(new item("                 TOTAL A PAGAR:",number_format($modelcaja->total_a_pagar,2))."\n\n");

        $modelF = Funcionario::find()->where(['idFuncionario'=>$modelcaja->codigoVendedor])->one();
        $printer -> text( "Facturado por: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2 . "\n\n\n\n");

        $printer -> feed(2);//Esto me permite cortar la factura en la base del papel
        $printer -> cut();
        $printer -> close();
        Yii::$app->response->redirect(array('cabeza-caja/index'));*/
        /*$content = '
        <div class="panel panel-default">
            <div class="panel-body">
                <table>
                    <tbody>
                    <tr>
                    <td>
                    <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                    </td>
                    <td style="text-align: left;">
                    <strong>'.$empresa->nombre.'</strong><br>
                    <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                    <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                    <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                    <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                    <strong>Dirección:</strong> '.$empresa->email.'
                    </td>
                    <tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Factura N°: '.$id. '<br>Fecha: '.date('d-m-Y').'</h4>';
       */
        /*$content .= $this->renderAjax('view',['model' => $this->findModel($id)]);

        $imprimir = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Factura</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
                <style type="text/css">
                    footer{
                      display: none;
                    }
                </style>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';

        echo $imprimir;*/
        /*$stylesheet = file_get_contents('http://uni-bus.tellan.ru/vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css');
        $pdf = new mPDF();
        $pdf->title = 'Factura No. '.$id;
        $pdf->SetHtmlHeader($header);
        $pdf->WriteHTML($stylesheet,1);
        $pdf->WriteHTML($content,2);
        $pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());
        $pdf->Output('Factura No. '.$id.'.pdf','I');
        exit;*/
        /*$pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => 'Factura No. '.$id],
            'methods' => [
                'SetHtmlHeader'=>[$header],
                'SetFooter'=>[' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa()],
            ]
        ]);
        return $pdf->render();*/
    }//fin actionReport



}//fin clase


//clase para agrupar numeros en fatura
/*class item
{
    private $nombre;
    private $precio;
    private $signo;

    public function __construct($nombre = '', $precio = '', $signo = false)
    {
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> signo = $signo;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 35;
        if ($this -> signo) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> nombre, $leftCols) ;

        $sign = ($this -> signo ? '$ ' : '');
        $right = str_pad($sign . $this -> precio, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}*/
