<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use backend\models\EncabezadoPrefactura;
use backend\models\ProductoCatalogo;
use backend\models\ProductoProveedores;
use backend\models\Proveedores;
use backend\models\OrdenCompraProv;
use backend\models\DetalleFacturas;
use backend\models\Model;
use backend\models\Clave_electronica;
use backend\models\search\EncabezadoPrefacturaSearch;
use backend\models\search\ProductoServiciosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Clientes;
use backend\models\ClientesApartados;
use backend\models\Funcionario;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use backend\models\Compra;//Prueba
use backend\models\Compra_update;//Prueba
use backend\models\Compra_view;//Facturas para la vista
use backend\models\ProductoServicios;//Prueba
use backend\models\OrdenServicio;
use yii\helpers\Html;
use mPDF;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use backend\models\MovimientoLibros;
use backend\models\Tribunales;
use backend\models\Modelo;
use backend\models\FacturaOt;
use backend\models\Ot;
use backend\models\Empresa;
use backend\models\MovimientoApartado;
use backend\models\MedioPago;
use backend\models\EncabezadoCaja;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\Factun;
use backend\models\UnidadMedidaProducto;
use backend\models\DetalleFacturasTemp;
/**
 * CabezaPrefacturaController implements the CRUD actions for EncabezadoPrefactura model.
 */
class CabezaPrefacturaController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['index','view','factura_ot_print','create','tabla','update','regresarborrar','regresarborrar-u',
              'regresarborrar-caja','delete','activar_btn_facturacion','cliente2', 'cliente2-u','clientenew','sincliente',
              'sincliente_u','clientenew-u', 'deletecliente','deletecliente-u','vendedor2', 'vendedor2-u', 'deletevendedor',
              'deletevendedor-u', 'addpago', 'addpago-u','addagentecomision','addagentecomision-u','habilitar_ot','addmedio',
              'addmedio-u','addcom','addcom-u','addcuenta-u','addfechadepo-u','addobservacion','addobservacion-u',
              'compruebatipopago','deletepago','deletepago-u','deleteagente','deleteagente-u','additem',
              'additemu','detalle','detalle_update','datos_ot','datos_otu','deleteitem','deleteitemu','updatecantidad','updatecantidadu',
              'modificar_linea_cr','modificar_linea_up','updatedescuento','updatedescuentou','addpro','getpreciototal','getpreciototalu','caja',
              'devolverfactura','desechar','deleteall','complete','ignorar','ignorar_u','completerminado','complete-u','cuentas',
              'cancelar-factura','busqueda_producto','report', 'condicion_venta', 'obtener_pago',
              'completando_facturacion', 'aplicardescuentogeneral_cr', 'aplicardescuentogeneral_up',
              //apartados
              'condicion_venta',
              'crear_apartado','apartados','mov_apartados','agregar_mov_apartado','agregar_monto_pago'],
              'rules' => [
                  [
                      'actions' => ['index','view','factura_ot_print','create','tabla','update','regresarborrar','regresarborrar-u',
                      'regresarborrar-caja','delete','activar_btn_facturacion','cliente2', 'cliente2-u','clientenew','sincliente',
                      'sincliente_u','clientenew-u', 'deletecliente','deletecliente-u','vendedor2', 'vendedor2-u', 'deletevendedor',
                      'deletevendedor-u', 'addpago', 'addpago-u','addagentecomision','addagentecomision-u','habilitar_ot','addmedio',
                      'addmedio-u','addcom','addcom-u','addcuenta-u','addfechadepo-u','addobservacion','addobservacion-u',
                      'compruebatipopago','deletepago','deletepago-u','deleteagente','deleteagente-u','additem',
                      'additemu','detalle','detalle_update','datos_ot','datos_otu','deleteitem','deleteitemu','updatecantidad','updatecantidadu',
                      'modificar_linea_cr','modificar_linea_up','updatedescuento','updatedescuentou','addpro','getpreciototal','getpreciototalu','caja',
                      'devolverfactura','desechar','deleteall','complete','ignorar','ignorar_u','completerminado','complete-u','cuentas',
                      'cancelar-factura','busqueda_producto','report', 'condicion_venta','obtener_pago',
                      'completando_facturacion', 'aplicardescuentogeneral_cr', 'aplicardescuentogeneral_up',
                      //apartados
                      'condicion_venta',
                      'crear_apartado','apartados','mov_apartados','agregar_mov_apartado','agregar_monto_pago'
                      ],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all EncabezadoPrefactura models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EncabezadoPrefacturaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EncabezadoPrefactura model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $c=null;

        Compra_view::setContenidoFactura(null);
        Compra_view::setCabezaFactura($model->idCabeza_Factura);
        //Abrimos el contenido
        $compra = Compra_view::getContenidoFactura();

        //Me obtiene los datos del detalle del encabezado correspondiente
                $query = new yii\db\Query();
                $data = $query->select(['codProdServicio','cantidad','precio_unitario','precio_por_cantidad', 'descuento_producto', 'subtotal_iva'])
                    ->from('tbl_detalle_facturas')
                    ->where(['=','idCabeza_factura', $model->idCabeza_Factura])
                    ->distinct()
                    ->all();
        //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
                foreach($data as $row){
                    $codProdServicio = $row['codProdServicio'];
                    $cantidad = $row['cantidad'];
                    $precio_unitario = $row['precio_unitario'];
                    $descuento = $row['descuento_producto'];
                    $iva = $row['subtotal_iva'];
                   if ($codProdServicio>=0) {
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        if($c==null)
                        {
                            $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();

                            $descripcion = '';
                            if ($modelProd) {
                                $descripcion = $codProdServicio != "0" ? $modelProd->nombreProductoServicio : 'Orden de Servicio';
                            } else {
                                $descripcion = $codProdServicio == "0" ? 'Orden de Servicio' : 'Este producto ya no existe en invetario';
                            }

                            $_POST['cantidad']= $codProdServicio != "0" ? $cantidad : 1;//por una extraña razón la cantidad de orden de servicio se incrementa al agregar un producto, por eso le agrego forzadamente cantidad 1 todas las veces.
                            $_POST['cod']= $codProdServicio;
                            $_POST['desc']= $descripcion;
                            $_POST['precio']= $precio_unitario;
                            //$_POST['dcto']= ?;//recordar que esto se cambia
                            $_POST['dcto'] = $descuento;
                            //Iva en forma manual, se puede cargar de la DB si se desea
                            $_POST['iva'] = $iva;

                            $compra[] = $_POST;
                            //echo $model->nombreProductoServicio;
                        }
                        //Guardamos el contenido
                        Compra_view::setContenidoFactura($compra);
                    }
                }

        return $this->renderAjax('view', [
            'model' => $model,
        ]);
    }


    //se imprime facturas ot print

    public function actionFactura_ot_print($id)
    {
        return $this->render('factura_ot_print', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EncabezadoPrefactura model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    //Funcion para la vita crear
    public function actionCreate()
    {
        $vendedornull = Compra::getVendedornull();

        if ($vendedornull!='1') {

           $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
           if ($funcionario) {
               Compra::setVendedor($funcionario->idFuncionario);

           } else {

           }

            Compra::setVendedornull(null);
        }
        $this->cargardetallecreate();
        return $this->render('create');

    }

    public function cargardetallecreate()
    {
        //Definimos c como null para comprobación
        $c=null;

        //Me obtiene los datos del detalle del encabezado correspondiente
                $query = new yii\db\Query();
                $data = $query->select(['cod','desc','precio','cantidad','dcto', 'iva', 'fecha_hora'])
                    ->from('tbl_detalle_facturas_temp')
                    ->where(['=','usuario', Yii::$app->user->identity->username])
                    ->all();
        //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
        if($data)
                foreach($data as $row){
                    $cod = $row['cod'];
                    $desc = $row['desc'];
                    $precio = $row['precio'];
                    $cantidad = $row['cantidad'];
                    $dcto = $row['dcto'];
                    $iva = $row['iva'];
                    $fecha_hora = $row['fecha_hora'];
                   if ($cod!='') {
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        if($c==null)
                        {
                            $_POST['cod'] = $row['cod'];
                            $_POST['desc'] = $row['desc'];
                            $_POST['precio'] = $row['precio'];
                            $_POST['cantidad'] = $row['cantidad'];
                            //$_POST['dcto']= ?;//recordar que esto se cambia
                            $_POST['dcto'] = $row['dcto'];
                            //Iva en forma manual, se puede cargar de la DB si se desea
                            $_POST['iva'] = $row['iva'];
                            $_POST['fecha_hora'] = $row['fecha_hora'];
                            $compra[] = $_POST;
                            //echo $model->nombreProductoServicio;
                        }
                        //Guardamos el contenido
                        Compra::setContenidoCompra($compra);
                    }
                }
    }

    public function actionTabla()
    {
        return $this->render('tabla');
    }
    //CANCELACION RAPIDA
    //con esto ejecuto la modal donde se procederá a facturar
    public function actionCondicion_venta()
    {
      if(($estadoF = Compra::getEstadoFactura()) && ($FechaCompraIni = Compra::getFechaInicial()) && ($vendedor = Compra::getVendedor()) && ($cliente = Compra::getCliente()) && ($pago = Compra::getPago()) && ($productos = Compra::getContenidoCompra()) )
      {
        $bandera = 'arriba'; //Significa que dará paso a almacenar factura con estado Caja o Crédito Caja
        $resumen = Compra::getTotal(true);
        if ($client_ot = Clientes::find()->where(['idCliente'=>Compra::getNombre_ot()])->one()) {
          $ot = Ot::find()->where(['idCliente'=>Compra::getNombre_ot()])->one();
          $monto_acreditado = $ot->montoTotalEjecutado_ot;
          $montodisponiblecredito = $client_ot->montoMaximoCredito - $monto_acreditado;
          if ($resumen['total'] > $montodisponiblecredito) {
            $bandera = 'abajo';
            }
        }
        if ($bandera=='arriba') {
          $nuevoestado = ($pago != '5') ? 'Caja' : 'Crédito Caja';
          if ($nuevoestado == 'Crédito Caja') {
              if($modelclient = Clientes::find()->where(['idCliente'=>$cliente])->one()){
                if ((($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Abierta')) || (($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Cerrada') && ($modelclient->autorizacion == 'Si'))) {
                  $bandera = 'arriba';
                } else {
                  $bandera = 'abajo';
                  Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                  Yii::$app->response->redirect(array('cabeza-prefactura/create' ));
                }
              } else {
                $bandera = 'abajo';
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                Yii::$app->response->redirect(array('cabeza-prefactura/create'));
              }
          }

        } else {//esta bandera es para devolver error si el responsable O.T exede el monto credito y ot
          $bandera = 'abajo';
          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Esta factura no puede ser acreditada porque el responsable de O.T excede el monto de crédito más O.T permitido.');
          Yii::$app->response->redirect(array('cabeza-prefactura/create' ));
        }
        if ($bandera == 'arriba') {
          echo $this->renderAjax('condicion_venta', ['tipoFacturacion'=>$pago]);
        }
      } else {
        echo "incompleto";
      }
    }

    //funcion que me calcula en tiempo real el cambio o faltante a pagarde
    //agregar la cantidad de pago para que debuelva el buelto que debe dar al cliente
    public function actionObtener_pago()
    {
        if(Yii::$app->request->isAjax){
          $servicio_ignorado = Compra::getServicio_porc_ign();
          $resumen_total = Compra::getTotal(true);
          $servicio = ($resumen_total['subtotal'] - $resumen_total['dcto'])*(Yii::$app->params['porciento_servicio']/100);
          if (Yii::$app->params['servicioRestaurante']==true && $servicio_ignorado != 'checked') {
            $total = $resumen_total['total'] + $servicio;
          } else {
            $total = $resumen_total['total'];//obtengo el total a pagarde la factura desde update.
          }
          //-------------------aqui consultamos si no hay un monto de pago ya ot_incluido
          $medio_pago = Compra_view::getContenidoMedioPago();
          $suma_monto_pago = 0;
          if (@$medio_pago) {
            foreach ($medio_pago as $key => $value) {
              $suma_monto_pago += $value['monto_cj'];
            }
          }
          //-------------------fin de consulta de monto de pago
              $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
              $moneda_cambio = Moneda::find()->where(['idTipo_moneda'=>$tipocambio->idTipo_moneda])->one();
              $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
              $valor = Yii::$app->request->post('valor');//obtengo valor con lo que va a pagar
              $dollar = Yii::$app->request->post('dollar');
              $resultado = ($valor - $total)+$suma_monto_pago; //debuelvo el valor del vuelto

              if ($dollar=='no') {
                $respuesta = $resultado >= 0 ? 'Vuelto' : 'Faltante';
                $resultado_simbolo = '<small>'.$respuesta.':</small> '.$moneda_local->simbolo.number_format($resultado,2); //imprimo el valor del vuelto
                $arr = array('valorencolones'=>'', 'resultado_simbolo'=>$resultado_simbolo, 'resultado'=>$resultado, 'pago_real'=>$valor);
                echo json_encode($arr);
              } else {
                $cambiodollar = $valor * $tipocambio->valor_tipo_cambio;
                $resultado = ($cambiodollar - $total)+$suma_monto_pago;
                $resultado_dollares = $resultado / $tipocambio->valor_tipo_cambio;
                $valorencolones = 'Valor en moneda local: '.$moneda_local->simbolo.number_format($cambiodollar,2);
                $respuesta = $resultado >= 0 ? 'Vuelto' : 'Faltante';
                $resultado_simbolo = '<small>Tip.Cambio: ' . $moneda_local->simbolo . $tipocambio->valor_tipo_cambio.'<br>'.$moneda_cambio->simbolo.number_format($resultado_dollares,2).'</small><br>'.
                '<small>'.$respuesta.':</small> '.$moneda_local->simbolo.number_format($resultado,2); //imprimo el valor del vuelto
                $arr = array('valorencolones'=>$valorencolones, 'resultado_simbolo'=>$resultado_simbolo, 'resultado'=>$resultado, 'pago_real'=>$cambiodollar);
                echo json_encode($arr);
              }


          }
    }

    //con esta funcion agregamos el monto pago
    public function actionAgregar_monto_pago()
    {
      if(Yii::$app->request->isAjax){
        $nuevo_pago = floatval(Yii::$app->request->post('pago'));
        $tipo_pago = Yii::$app->request->post('tipo_pago');
        $tipo_del_medio = Yii::$app->request->post('ddl_entidad');
        $cuenta_deposito = Yii::$app->request->post('tipoCuentaBancaria');
        $fechadepo = Yii::$app->request->post('fechadepo');
        $comprobacion = Yii::$app->request->post('comprobacion');
        $vueltoinput = Yii::$app->request->post('vueltoinput');
        $tran = Yii::$app->db->beginTransaction();
        try {
          $resumen_total = Compra::getTotal(true);
          $servicio_ignorado = Compra::getServicio_porc_ign();
          if (Yii::$app->params['servicioRestaurante']==true && $servicio_ignorado != 'checked') {
            $servicio = ($resumen_total['subtotal'] - $resumen_total['dcto'])*(Yii::$app->params['porciento_servicio']/100);
            $total_a_pagar = $resumen_total['total'] + $servicio;
          } else {
            $total_a_pagar = $resumen_total['total'];
          }

          $medio_pago = Compra_view::getContenidoMedioPago();
          $suma_monto_pago = 0.00;
          $contador = 0;
          if (@$medio_pago) {
            foreach ($medio_pago as $key => $value) {
              $contador += 1;
              $suma_monto_pago += $value['monto_cj'];
            }
          }
            $pago_acumulado = $nuevo_pago + $suma_monto_pago;
            //$resumen_total = Compra::getTotal(true);
            $pago_final = $pago_acumulado > $total_a_pagar ? ($total_a_pagar-$suma_monto_pago) : $nuevo_pago;
          if ($contador < 4 && $pago_final > 0) {
            $medio_pago_new = Compra_view::getContenidoMedioPago();

              $_POST['formpa_cj'] = $tipo_pago;
              $_POST['medio_cj'] = $tipo_del_medio;
              $_POST['comprob_cj'] = $comprobacion;
              $_POST['cuenta_cj'] = $cuenta_deposito;
              $_POST['fechadepo_cj'] = $fechadepo;
              $_POST['monto_cj'] = $pago_final;
              $medio_pago_new[] = $_POST;
              sleep(1);
            Compra_view::setContenidoMedioPago($medio_pago_new);
          }
          $tran->commit();
          return true;
        } catch (\Exception $e) {
          /*si ocurre un error en toda la transaccion hace un rollBack y muestra el error*/
          $tran->rollBack();
          return false;
        }
      }
    }

    /**
     * Updates an existing EncabezadoPrefactura model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    //función para la vista de actualizar

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


                $compra = Compra_update::getContenidoComprau(); /////
                //Baseamos el contenido que puede quedar de otro detalle
                Compra_update::setContenidoComprau(null);
                Compra_update::setCabezaFactura(null);

                //Abrimos el contenido
                Compra::setCabezaFactura($id);

                //llamamos la funcion que me permite cargar los datos en el detalle
                $this->cargardetalleupdate($model);

            return $this->render('update', [
                'model' => $model,
            ]);
    }

    public function cargardetalleupdate($model)
    {
        //Definimos c como null para comprobación
        $c=null;

        //Me obtiene los datos del detalle del encabezado correspondiente
                $query = new yii\db\Query();
                $data = $query->select(['idDetalleFactura','codProdServicio','cantidad','precio_unitario','precio_por_cantidad', 'descuento_producto', 'subtotal_iva'])
                    ->from('tbl_detalle_facturas')
                    ->where(['=','idCabeza_factura', $model->idCabeza_Factura])
                  //  ->distinct()
                    ->orderBy(['idDetalleFactura' => SORT_DESC])
                    ->all();
                $nivel = 'Presiona para mostrar la orden de servicio, verifique que esté al 100% antes de facturar o acreditar';
        //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
                foreach($data as $row){
                    $codProdServicio = $row['codProdServicio'];
                    $cantidad = $row['cantidad'];
                    $precio_unitario = $row['precio_unitario'];
                    $descuento = $row['descuento_producto'];
                    $iva = $row['subtotal_iva'];
                   if ($codProdServicio!='') {
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        if($c==null)
                        {   //cargo las variables de la tabla
                            //$orden = Html::a('Orden de Servicio', '../web/index.php?r=orden-servicio%2Fview&id='.$model->idOrdenServicio);
                            //Modal con datos: https://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
                            $orden = Html::a('Orden de Servicio', ['#'], [
                                'id' => 'activity-index-link-update',
                                'data-toggle' => 'modal',//'data-placement'=>'left',
                                'data-trigger'=>'hover', 'data-content'=>$nivel,
                                'data-target' => '#modalOrden',
                                'data-url' => Url::to(['orden-servicio/view', 'id' => $model->idOrdenServicio]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Orden de Servicio correspondiente'),
                                ]);

                            $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();

                            $descripcion = '';

                            if ($modelProd) {
                                $descripcion = $codProdServicio != "0" ? $modelProd->nombreProductoServicio : $orden;
                            } else {
                                $descripcion = $codProdServicio == "0" ? $orden : 'Este producto ya no existe en invetario';
                            }

                            $_POST['cantidad']= $codProdServicio != "0" ? $cantidad : 1;//por una extraña razón la cantidad de orden de servicio se incrementa al agregar un producto, por eso le agrego forzadamente cantidad 1 todas las veces.
                            $_POST['cod']= $codProdServicio;
                            $_POST['desc']= $descripcion;
                            $_POST['precio']= $precio_unitario;
                            //$_POST['dcto']= ?;//recordar que esto se cambia
                            $_POST['dcto'] = $descuento;
                            //Iva en forma manual, se puede cargar de la DB si se desea
                            $_POST['iva'] = $iva;

                            $compra[] = $_POST;
                            //echo $model->nombreProductoServicio;
                        }
                        //Guardamos el contenido
                        Compra_update::setContenidoComprau($compra);
                    }
                }
    }

    //función para borrar datos en session compra y regresar al index
    public function actionRegresarborrar()
            {
            //Para borrar todo
            if ($data = Compra::getContenidoCompra())
            foreach($data as $row){
                $cantidad = $row['cantidad'];
                $codProdServicio = $row['cod'];

                //Obtenemos la cantidad al producto que queremos modificar
                $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":codProdServicio", $codProdServicio);
                $cant = $command->queryOne();

                $nuevacantidad = $cant['cantidadInventario'] + $cantidad;
                //actualizamos nueva cantidad a cada uno de los productos
                $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                    $command2 = \Yii::$app->db->createCommand($sql2);
                    $command2->bindParam(":cantidad", $nuevacantidad);
                    $command2->bindValue(":codProdServicio", $codProdServicio);
                    $command2->execute();
            }
            $this->borrarTodo();
            Yii::$app->response->redirect(array('cabeza-prefactura/index'));
            }
    //función para borrar datos en session compra_update y regresar al index
    public function actionRegresarborrarU()
            {
            //Para borrar todo
            $this->borrarTodoU();
            Yii::$app->response->redirect(array('cabeza-prefactura/index'));
            }
    public function actionRegresarborrarCaja()
            {
            //Para borrar todo
            $this->borrarTodoU();
            Yii::$app->response->redirect(array('cabeza-caja/index'));
            }

    /**
     * Deletes an existing EncabezadoPrefactura model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EncabezadoPrefactura model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EncabezadoPrefactura the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EncabezadoPrefactura::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //Esta funcion me permite borrar todo lo que estan en session compra
    private function borrarTodo()
    {
      Compra::setContenidoCompra(array());
      \Yii::$app->db->createCommand()->delete('tbl_detalle_facturas_temp', 'usuario = "'.Yii::$app->user->identity->username.'"' )->execute();
      //Compra::setProveedor(null);
      Compra::setPago(null);
      //Compra::setDoc(null);
      Compra::setVendedor(null);
      Compra::setCliente(null);
      Compra::setPago(null);
      Compra::setMedio(null);
      Compra::setComproba(null);
      Compra::setVendedornull(null);
      Compra::setObserva(null);
      Compra::setAgenteComision(null);
      Compra::setServicio_porc_ign(null);
      Compra::setOT(null);//----------------OT
      Compra::setNombre_ot(null);
      Compra::setPlaca_ot(null);
      Compra::setMarca_ot(null);
      Compra::setModelo_ot(null);
      Compra::setVersion_ot(null);
      Compra::setAno_ot(null);
      Compra::setMotor_ot(null);
      Compra::setCilindrada_ot(null);
      Compra::setCombustible_ot(null);
      Compra::setTransmision_ot(null);
     //Compra::setContenidoCheque(array());
    }

    //Esta funcion me permite borrar todo lo que estan en session compra_update
    private function borrarTodoU()
    {
      Compra_update::setContenidoComprau(array());
      //Compra::setProveedor(null);
      //Compra::setDoc(null);
      Compra_update::setVendedoru(null);
      Compra_update::setClienteu(null);
      Compra_update::setPagou(null);
      Compra_update::setMediou(null);
      Compra_update::setComprobau(null);
      Compra_update::setTotalauxiliar(null);
      Compra_update::setCabezaFactura(null);
      Compra_update::setCuentau(null);
     //Compra::setContenidoCheque(array());
    }

    //Esta funcion me predice si hay datos completos para mostrar los botones de facturacion
    public function actionActivar_btn_facturacion(){
        if(Yii::$app->request->isAjax){
          $products = Compra::getContenidoCompra();
          $vendedor = Compra::getVendedor();
          $cliente = Compra::getCliente();
          $tipopago = Compra::getPago();
          if($products && $tipopago && $vendedor && $cliente) {
            echo "completo";
          } else {
            echo 'incompleto';
          }
        }
    }

            //esta funcion me agrega clientes que ya existen para session compra
            public function actionCliente2(){
                if(Yii::$app->request->isAjax){
                    $idCliente = Yii::$app->request->post('idCliente');
                    $producto_venta = Compra::getContenidoCompra();//obtenemos todos los productos apartados
                    if (@$producto_venta){
                      foreach ($producto_venta as $position => $product){//recorremos cada producto
                        //buscamos el producto en turno en inventario
                        $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$producto_venta[$position]['cod']])->one();
                        //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
                        $producto_venta[$position]['iva'] = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                        Compra::setContenidoCompra($producto_venta);//guardamos en sessión
                      } //return true;
                    }

                      //si el cliente seleccionado esta en base de datos obtenemos un objeto
                      if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$idCliente])->one())
                      { //comprovamos si es exento de impuesto
                        if ($cliente_ob->iv == true) {//si es un cliente exento vuelve a recorrer los productos
                          if (@$producto_venta){
                            foreach ($producto_venta as $position => $product){
                              $producto_venta[$position]['iva'] = '0.00';//para asignarle un valor 0.00 exento a todos los productos
                              Compra::setContenidoCompra($producto_venta);//volvemos a guardar
                            }
                          }
                        }
                      }

                  /*  $sql = "SELECT idCliente, nombreCompleto from tbl_clientes where idCliente = :idCliente ";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindValue(":idCliente", $idCliente);
                    $result = $command->queryOne();*/
                    Compra::setCliente((int)$idCliente);
                    Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    //echo $result ['nombreCompleto'];
                }
            }
            //esta funcion me agrega clientes que ya existen para session compra_udate
            public function actionCliente2U(){
                $id = Compra_update::getCabezaFactura();
                $bandera = 'arriba';
                $modelestado = $this->findModel($id);
                if(Yii::$app->request->isAjax){
                    $idCliente = Yii::$app->request->post('idCliente');
                    $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$id])->all();
                    if (@$detalle_venta)
                    foreach ($detalle_venta as $position => $product){//recorremos cada producto
                      $position = DetalleFacturas::findOne($product->idDetalleFactura);
                      //buscamos el producto en turno en inventario
                      $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$product->codProdServicio])->one();
                      if (@$producto_serv) {
                        //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
                        $position->subtotal_iva = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : 0.00;
                        $position->save();//guardamos en sessión
                      } else {
                        $position->subtotal_iva = Yii::$app->params['porciento_impuesto'];
                        $position->save();
                      }
                    }
                    if ($modelestado->tipoFacturacion == 5)
                    if ($modelestado->estadoFactura == 'Crédito Caja' || $modelestado->estadoFactura == 'Caja') {
                        if($modelclient = Clientes::find()->where(['idCliente'=>$idCliente])->one()){
                            if ((($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Abierta')) || (($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Cerrada') && ($modelclient->autorizacion == 'Si'))) {
                                        $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                                        $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                                        if ($modelestado->total_a_pagar > $montodisponiblecredito) {
                                            $bandera = 'abajo';
                                            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente no puede tener facturas de crédito porque excede el monto de crédito permitido.');
                                            Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$id ));
                                        }
                                        else{

                                        $idCliente = $modelclient->idCliente;
                                        $monto_a_credito = $modelestado->total_a_pagar + $modelclient->montoTotalEjecutado;
                                        $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
                                        $command = \Yii::$app->db->createCommand($sql);
                                        $command->bindParam(":montoTotalEjecutado", $monto_a_credito);
                                        $command->bindValue(":idCliente", $idCliente);
                                        $command->execute();


                                        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Cliente aceptado para crédito.');
                                        //Yii::$app->response->redirect(array('cabeza-prefactura/index'));
                                        }
                                    } else {
                                        $bandera = 'abajo';
                                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                                        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$id ));
                                    }
                        }
                    }else{
                        $bandera = 'abajo';
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$id));
                    }

                    if ($bandera == 'arriba') {
                      if (@$modelclient = Clientes::find()->where(['idCliente'=>$idCliente])->one()) {
                        if ($modelclient->iv == true) {//si es un cliente exento vuelve a recorrer los productos
                          if (@$detalle_venta)
                          foreach ($detalle_venta as $position => $product){
                            $position = DetalleFacturas::findOne($product->idDetalleFactura);
                            $position->subtotal_iva = 0.00;
                            $position->save();
                          }
                        }
                      }

                    $sql = "UPDATE tbl_encabezado_factura SET idCliente = :idCliente WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":idCliente", $idCliente);
                        $command->bindParam(":id", $id);
                        $command->execute();
                    Compra_update::setClienteu((int)$idCliente);
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
                    //echo $result ['nombreCompleto'];
                    }

                }
            }

            //Esta funcion me agrega clientes nuevos para session compra
            public function actionClientenew(){
                $tribunales = new Tribunales();
                if (Yii::$app->request->post('new-cliente')) {
                  $producto_venta = Compra::getContenidoCompra();//obtenemos todos los productos apartados
                  if(@$producto_venta){
                    foreach ($producto_venta as $position => $product){//recorremos cada producto
                      //buscamos el producto en turno en inventario
                      $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$producto_venta[$position]['cod']])->one();
                      //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
                      $producto_venta[$position]['iva'] = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                      Compra::setContenidoCompra($producto_venta);//guardamos en sessión
                    }
                  }

                    $idCliente = Yii::$app->request->post('new-cliente');
                    $modelclient = Clientes::find()->where(['identificacion'=>$idCliente])->one();
                    if (@$modelclient) {
                      if ($modelclient->iv == true) {
                      //si es un cliente exento vuelve a recorrer los productos
                        if(@$producto_venta){
                          foreach ($producto_venta as $position => $product){
                            $producto_venta[$position]['iva'] = '0.00';//para asignarle un valor 0.00 exento a todos los productos
                            Compra::setContenidoCompra($producto_venta);//volvemos a guardar
                          }
                        }
                      }
                      $id_cli = $modelclient->idCliente;
                      Compra::setCliente($id_cli);
                    } else {
                      $nombre_tribunal = $tribunales->obtener_nombre_completo($idCliente);
                      if ($nombre_tribunal != 'Sin resultado') {
                          Compra::setCliente($nombre_tribunal);
                      } else {
                        $nombre_tribunal_juridico = $tribunales->obtener_nombre_juridico($idCliente);
                        if ($nombre_tribunal_juridico != 'Sin resultado') {
                          Compra::setCliente($nombre_tribunal_juridico);
                        } else {
                        Compra::setCliente($idCliente);
                        }
                      }
                    }
                    Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ningun cliente, el espacio estaba vacío.');
                    Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                }
            }
            //agregar espacio sin cliente en create
            public function actionSincliente()
            {
              $idCliente  = $_GET['cliente'];
              $producto_venta = Compra::getContenidoCompra();//obtenemos todos los productos apartados
              if(@$producto_venta){
                foreach ($producto_venta as $position => $product){//recorremos cada producto
                  //buscamos el producto en turno en inventario
                  $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$producto_venta[$position]['cod']])->one();
                  //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
                  $producto_venta[$position]['iva'] = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                  Compra::setContenidoCompra($producto_venta);//guardamos en sessión
                }
              }

                //si el cliente seleccionado esta en base de datos obtenemos un objeto
                if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$idCliente])->one())
                { //comprovamos si es exento de impuesto
                  if ($cliente_ob->iv == true) {//si es un cliente exento vuelve a recorrer los productos
                    if(@$producto_venta){
                      foreach ($producto_venta as $position => $product){
                        $producto_venta[$position]['iva'] = '0.00';//para asignarle un valor 0.00 exento a todos los productos
                        Compra::setContenidoCompra($producto_venta);//volvemos a guardar
                      }
                    }
                  }
                }
              Compra::setCliente($idCliente);
              Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }
            //agregar espacio sin cliente en update
            public function actionSincliente_u($id)
            {
              $model = $this->findModel($id);
              if ($model->estadoFactura=='Crédito Caja') {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Cliente no permitido para factura de crédito.');
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
              }else {
                $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$id])->all();
                if (@$detalle_venta) {
                  foreach ($detalle_venta as $position => $product){//recorremos cada producto
                    $position = DetalleFacturas::findOne($product->idDetalleFactura);
                    //buscamos el producto en turno en inventario
                    $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$product->codProdServicio])->one();
                    //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
                    if (@$producto_serv) {
                      $position->subtotal_iva = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                      $position->save();//guardamos en sessión
                    } else {
                      $position->subtotal_iva = Yii::$app->params['porciento_impuesto'];
                      $position->save();//guardamos en sessión
                    }

                  }
                }

                $idCliente = 'Factura sin cliente';
                $sql = "UPDATE tbl_encabezado_factura SET idCliente = :idCliente WHERE idCabeza_Factura = :id";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":idCliente", $idCliente)->bindParam(":id", $id)->execute();
                Compra_update::setClienteu($idCliente);
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
              }
            }
            //Esta funcion me agrega clientes nuevos para session compra_update
            public function actionClientenewU(){
              $tribunales = new Tribunales();
                $id = Compra_update::getCabezaFactura();
                $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$id])->all();
                if (@$detalle_venta)
                foreach ($detalle_venta as $position => $product){//recorremos cada producto
                  $position = DetalleFacturas::findOne($product->idDetalleFactura);
                  //buscamos el producto en turno en inventario
                  $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$product->codProdServicio])->one();
                  //luego de buscar en inventario comprovamos si esta exento de impuesto para considerarlo en la venta
                  if (@$producto_serv) {
                    $position->subtotal_iva = $producto_serv->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : 0.00;
                    $position->save();//guardamos en sessión
                  } else {
                    $position->subtotal_iva = Yii::$app->params['porciento_impuesto'];
                    $position->save();
                  }
                }
                if (Yii::$app->request->post('new-cliente')) {
                    $idCliente = Yii::$app->request->post('new-cliente');
                    $modelclient = Clientes::find()->where(['identificacion'=>$idCliente])->one();
                    if (@$modelclient) {
                      //verificamos que el cliente no sea exento de iv
                      if ($modelclient->iv == true) {//si es un cliente exento vuelve a recorrer los productos
                        if (@$detalle_venta)
                        foreach ($detalle_venta as $position => $product){
                          $position = DetalleFacturas::findOne($product->idDetalleFactura);
                          $position->subtotal_iva = 0.00;
                          $position->save();
                        }
                      }
                      $idCliente = $modelclient->idCliente;
                    } else {
                      $nombre_tribunal = $tribunales->obtener_nombre_completo($idCliente);
                      $nombre_tribunal_juridico = $tribunales->obtener_nombre_juridico($idCliente);
                      if ($nombre_tribunal != 'Sin resultado') {
                        $idCliente = $nombre_tribunal;
                      } elseif ($nombre_tribunal_juridico != 'Sin resultado') {
                        $idCliente = $nombre_tribunal_juridico;
                      }
                    }
                    $model = $this->findModel($id);
                    if ($model->estadoFactura=='Crédito Caja') {
                      Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Cliente no permitido para factura de crédito.');
                      Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
                    }else {

                    $sql = "UPDATE tbl_encabezado_factura SET idCliente = :idCliente WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":idCliente", $idCliente);
                        $command->bindParam(":id", $id);
                        $command->execute();
                    Compra_update::setClienteu($idCliente);
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
                  }
                }else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se agregó ningun cliente, el espacio estaba vacío.');
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
                }

            }

            //Esta funcion me elimina los datos de cliente almacenados en session compra
            public function actionDeletecliente()
            {
                //Para borrar cliente
                Compra::setCliente(null);
                Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }
            //Esta funcion me elimina los datos de cliente almacenados en session compra_update
            public function actionDeleteclienteU($id)
            {
                //para modificar el monto total ejecutado del cliente con las condiciones correspondientes
                $modelestado = $this->findModel($id);
                if ($modelestado->tipoFacturacion == 5)
                    if ($modelestado->estadoFactura == 'Crédito Caja' || $modelestado->estadoFactura == 'Caja') {
                        if($modelclient = Clientes::find()->where(['idCliente'=>$modelestado->idCliente])->one()){
                            //if ($modelclient->credito == 'Si') {
                                $montoTotalEjecutado = $modelclient->montoTotalEjecutado - $modelestado->total_a_pagar;
                                $idCliente = $modelclient->idCliente;
                                $sqlc = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :id";
                                $commandc = \Yii::$app->db->createCommand($sqlc);
                                $commandc->bindParam(":montoTotalEjecutado", $montoTotalEjecutado);
                                $commandc->bindParam(":id", $idCliente);
                                $commandc->execute();
                           // }
                        }
                }

                $sql = "UPDATE tbl_encabezado_factura SET idCliente = '' WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":id", $id);
                        $command->execute();
                //Para borrar cliente
                Compra_update::setClienteu(null);
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
            }

            //Esta funcion me amacena los datos del vendedor en session compra
            public function actionVendedor2(){
                if(Yii::$app->request->isAjax){
                    $idFuncionario = Yii::$app->request->post('idFuncionario');
                    Compra::setVendedor((int)$idFuncionario);

                    Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                }
            }
            //Esta funcion me amacena los datos del vendedor en session compra_update
            public function actionVendedor2U(){
                $id = Compra_update::getCabezaFactura();
                if(Yii::$app->request->isAjax){
                    $idFuncionario = Yii::$app->request->post('idFuncionario');
                    $sql = "UPDATE tbl_encabezado_factura SET codigoVendedor = :idFuncionario WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":idFuncionario", $idFuncionario);
                        $command->bindParam(":id", $id);
                        $command->execute();
                    Compra_update::setVendedoru((int)$idFuncionario);
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
                }
            }

            //Esta funcion me elimina los datos del vendedor almacenados en session compra
            public function actionDeletevendedor()
            {
                Compra::setVendedor(null);//Para borrar vendedor
                Compra::setVendedornull('1');
                Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }
            //Esta funcion me elimina los datos del vendedor almacenados en session compra_update
            public function actionDeletevendedorU($id)
            {
                $sql = "UPDATE tbl_encabezado_factura SET codigoVendedor = NULL WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":id", $id);
                        $command->execute();
                Compra_update::setVendedoru('');//Para borrar vendedor al actualizar
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
            }

            //Esta funcion me agrega los datos del pago en session compra
            public function actionAddpago(){
                if(Yii::$app->request->isAjax){
                        $tipoFacturacion = Yii::$app->request->post('tipoFacturacion');
                        Compra::setPago((int)$tipoFacturacion);
                    }
                    Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }
            //Esta funcion me agrega los datos del pago en session compra
               public function actionAddagentecomision(){
                if(Yii::$app->request->isAjax){
                        $idAgenteComision = Yii::$app->request->post('idAgenteComision');
                        Compra::setAgenteComision((int)$idAgenteComision);
                    }
                    Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }

                 //Esta funcion me agrega los datos del pago en session compra
               public function actionAddagentecomisionU(){
                $id = Compra_update::getCabezaFactura();
                if(Yii::$app->request->isAjax){
                        $idAgenteComision = Yii::$app->request->post('idAgenteComision');
                        $sql = "UPDATE tbl_encabezado_factura SET idAgenteComision = :idAgenteComision WHERE idCabeza_Factura = :id";
                              $command = \Yii::$app->db->createCommand($sql);
                              $command->bindParam(":idAgenteComision", $idAgenteComision);
                              $command->bindParam(":id", $id);
                              $command->execute();
                    }
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
            }
            //Esta funcion me agrega los datos del pago en session compra_update
            public function actionAddpagoU(){
                $id = Compra_update::getCabezaFactura();
                if(Yii::$app->request->isAjax){
                    $bandera = 'arriba';
                        $tipoFacturacion = Yii::$app->request->post('tipoFacturacion');
                        $modelestado = $this->findModel($id);
                        if ($modelestado->estadoFactura == 'Crédito Caja' || $modelestado->estadoFactura == 'Caja') {
                            if ($tipoFacturacion != 5) {
                                $estadoFactura = 'Caja';
                                $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estadoFactura WHERE idCabeza_Factura = :id";
                                $command = \Yii::$app->db->createCommand($sql);
                                $command->bindParam(":estadoFactura", $estadoFactura);
                                $command->bindParam(":id", $id);
                                $command->execute();
                            } else {

                            if($modelclient = Clientes::find()->where(['idCliente'=>$modelestado->idCliente])->one()){
                                    if ((($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Abierta')) || (($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Cerrada') && ($modelclient->autorizacion == 'Si'))) {
                                        $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                                        $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                                        if ($modelestado->total_a_pagar > $montodisponiblecredito) {
                                            $bandera = 'abajo';
                                            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Esta factura no puede ser acreditada porque excede el monto de crédito permitido del cliente.');
                                            Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$id ));
                                        }
                                        else{

                                        $idCliente = $modelclient->idCliente;
                                        $monto_a_credito = $modelestado->total_a_pagar + $modelclient->montoTotalEjecutado;
                                        $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
                                        $command = \Yii::$app->db->createCommand($sql);
                                        $command->bindParam(":montoTotalEjecutado", $monto_a_credito);
                                        $command->bindValue(":idCliente", $idCliente);
                                        $command->execute();


                                        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Factura acreditada correctamente.');
                                        //Yii::$app->response->redirect(array('cabeza-prefactura/index'));
                                        }
                                    } else {
                                        $bandera = 'abajo';
                                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                                        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$id ));
                                    }
                                } //fin if($modelclient = Clientes::find()->where(['idCliente'=>$modelestado->idCliente])->one())
                                else{
                                    $bandera = 'abajo';
                                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                                    Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$id));
                                }
                                if ($bandera == 'arriba') {
                                    $estadoFactura = 'Crédito Caja';
                                    $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estadoFactura WHERE idCabeza_Factura = :id";
                                    $command = \Yii::$app->db->createCommand($sql);
                                    $command->bindParam(":estadoFactura", $estadoFactura);
                                    $command->bindParam(":id", $id);
                                    $command->execute();
                                }

                            }
                        }
                        if ($bandera == 'arriba') {
                        $sql = "UPDATE tbl_encabezado_factura SET tipoFacturacion = :tipoFacturacion WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":tipoFacturacion", $tipoFacturacion);
                        $command->bindParam(":id", $id);
                        $command->execute();
                        Compra_update::setPagou((int)$tipoFacturacion);
                        Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
                        }
                    }
            }

            //esta funcion me permite habilitar la factura como O.T
            public function actionHabilitar_ot()
            {
              if(Yii::$app->request->isAjax){
                  $modo = Yii::$app->request->post('modo');
                  if ($modo=='true') {
                    Compra::setOT($modo);
                    Yii::$app->getSession()->setFlash('info', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> O.T habilitado.');
                  } else{
                    Compra::setOT(null);
                    Compra::setNombre_ot(null);
                    Compra::setPlaca_ot(null);
                    Compra::setMarca_ot(null);
                    Compra::setModelo_ot(null);
                    Compra::setVersion_ot(null);
                    Compra::setAno_ot(null);
                    Compra::setMotor_ot(null);
                    Compra::setCilindrada_ot(null);
                    Compra::setCombustible_ot(null);
                    Compra::setTransmision_ot(null);
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> O.T deshabilitado.');
                  }
                  Yii::$app->response->redirect(array('cabeza-prefactura/create'));
              }
            }

            //Esta funcion me agrega los datos del medio en session compra
            public function actionAddmedio(){
                if(Yii::$app->request->isAjax){
                    $medio = Yii::$app->request->post('medio');
                    Compra::setMedio($medio);
                }
            }
            //Esta funcion me agrega los datos del medio en session compra_update
            public function actionAddmedioU(){
                $id = Compra_update::getCabezaFactura();
                if(Yii::$app->request->isAjax){
                    $medio = Yii::$app->request->post('medio');
                    /*$idPago = Yii::$app->request->post('idPago');
                    $sql = "UPDATE tbl_medio_pago SET idForma_Pago = :idForma_Pago , tipo_del_medio = :tipo_del_medio WHERE idCabeza_factura = :id ";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":idForma_Pago", $idPago);
                        $command->bindParam(":tipo_del_medio", $medio);
                        $command->bindParam(":id", $id);
                        $command->execute();*/
                    Compra_update::setMediou($medio);
                }
            }

            //Esta funcion me agrega los comprobacion del pago en session compra
            public function actionAddcom(){
                if(Yii::$app->request->isAjax){
                    $comprobacion = Yii::$app->request->post('comprobacion');
                    Compra::setComproba($comprobacion);
                }
            }

            //Esta funcion me agrega los comprobacion del pago en session compra_update
            public function actionAddcomU(){
                if(Yii::$app->request->isAjax){
                    $comprobacion = Yii::$app->request->post('comprobacion');
                    Compra_update::setComprobau($comprobacion);

                }
            }

            //Esta funcion me agrega el numero de cuenta del pago por cheque en session compra_update
            public function actionAddcuentaU(){
                if(Yii::$app->request->isAjax){
                    $cuenta = Yii::$app->request->post('cuenta');
                    Compra_update::setCuentau($cuenta);
                }
            }

            //Esta funcion me agrega la fecha de un deposito an session compra_update
            public function actionAddfechadepoU(){
                if(Yii::$app->request->isAjax){
                    $fechadepo = Yii::$app->request->post('fechadepo');
                    Compra_update::setFechadepo($fechadepo);
                }
            }

            //Esta funcion me agrega la observacion en session compra
            public function actionAddobservacion(){
                if(Yii::$app->request->isAjax){
                    $observacion = Yii::$app->request->post('observacion');
                    Compra::setObserva($observacion);
                }
            }

            //Esta funcion actualiza la observacion en base de datos
            public function actionAddobservacionU(){
                if(Yii::$app->request->isAjax){
                    $observacion = Yii::$app->request->post('observacion');
                    $idCabeza = Yii::$app->request->post('idCabeza');
                    $sql = "UPDATE tbl_encabezado_factura SET observacion = :observacion WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":observacion", $observacion);
                        $command->bindParam(":id", $idCabeza);
                        $command->execute();
                }
            }

            //esta funcion comprueba que no se repita el comprovante para una cuenta bancaria
            public function actionCompruebatipopago(){
                if(Yii::$app->request->isAjax){
                    $cuenta_deposito = Yii::$app->request->post('cuenta_deposito');
                    $comprobacion = Yii::$app->request->post('comprobacion');
                    $entidadFinanc = Yii::$app->request->post('entidadFinanc');
                    $tipo = Yii::$app->request->post('tipo');
                    $comprobacion_espejo = Yii::$app->request->post('comprobacion');
                    /*if ($tipo==4) {//si tipo es transferencia
                      $comprobacion_espejo = '';
                    }*/
                    //consulto cuenta bancaria para obtener el dato que me identifica...
                    $modelcuentabancaria = CuentasBancarias::find()->where(['idEntidad_financiera'=>$entidadFinanc])->andWhere("numero_cuenta = :numero_cuenta", [":numero_cuenta"=>$cuenta_deposito])->one();
                    //...el id que ocupo comprovar si corresponde a los mismos datos que existen ya en un movimiento en libros
                    $movilibros = @MovimientoLibros::find()->where(['idCuenta_bancaria'=>$modelcuentabancaria->idBancos])->andWhere("numero_documento = :numero_documento", [":numero_documento"=>(int) $comprobacion])->one();
                    //Obtenemos el medio de pago y consulto si ambos concuerdan
                    $sql = "SELECT IF( EXISTS(
                     SELECT *
                     FROM tbl_medio_pago
                     WHERE (idForma_Pago = 4 AND comprobacion = :comprobacion AND cuenta_deposito = :cuenta_deposito)
                     OR (idForma_Pago = 3 AND comprobacion = :comprobacion_espejo AND tipo_del_medio = :tipo_del_medio)
                     OR (idForma_Pago = 2 AND comprobacion = :comprobacion_espejo AND tipo_del_medio = :tipo_del_medio)), 1, 0) AS 'info'";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":comprobacion", $comprobacion);
                    $command->bindParam(":comprobacion_espejo", $comprobacion_espejo);
                    $command->bindParam(":cuenta_deposito", $cuenta_deposito);
                    $command->bindParam(":tipo_del_medio", $entidadFinanc);
                    $mediopago = $command->queryOne();
                    //si info == 1 significa que existe un dato en tbl_medio_pago o oncluyo ya sea si se cumple @$movimiento existe un datro parecido en
                    //movimiento libro, o sea que ese dato es repetido...
                    if ($mediopago['info']==1 || @$movilibros) {//...po lo que notifico imprimiendo repetido
                        echo "repetido";
                    } else {
                      echo "no repetido";
                    }
                }
            }

            //Esta funcion me elimina los datos de pago almacenados en session compra
            public function actionDeletepago()
            {
                //Para borrar pago
                Compra::setPago(null);
                Compra::setMedio(null);
                Compra::setComproba(null);
                Compra::setVendedornull('1');
                Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }

             public function actionDeleteagente()
            {
                //Para borrar Agente
                Compra::setAgenteComision(null);
                Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }

                public function actionDeleteagenteU($id)
            {
                //Para borrar Agente
             // $id = Compra_update::getCabezaFactura();
                //Compra_update::setAgenteComisionu(null);
                $sql = "UPDATE tbl_encabezado_factura SET idAgenteComision = NULL WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":id", $id);
                        $command->execute();
                Compra_update::setAgenteComisionu(null);
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
            }

            //Esta funcion me elimina los datos de pago almacenados en session compra_update
            public function actionDeletepagoU($id)
            {
                //Para borrar pago

                //para modificar el monto total ejecutado del cliente con las condiciones correspondientes
                $modelestado = $this->findModel($id);
                if ($modelestado->tipoFacturacion == 5)
                    if ($modelestado->estadoFactura == 'Crédito Caja' || $modelestado->estadoFactura == 'Caja') {
                        if($modelclient = Clientes::find()->where(['idCliente'=>$modelestado->idCliente])->one()){
                            //if ($modelclient->credito == 'Si') {
                                $montoTotalEjecutado = $modelclient->montoTotalEjecutado - $modelestado->total_a_pagar;
                                $idCliente = $modelclient->idCliente;
                                $sqlc = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :id";
                                $commandc = \Yii::$app->db->createCommand($sqlc);
                                $commandc->bindParam(":montoTotalEjecutado", $montoTotalEjecutado);
                                $commandc->bindParam(":id", $idCliente);
                                $commandc->execute();
                            //}
                        }
                }


                $sql = "UPDATE tbl_encabezado_factura SET tipoFacturacion = '' WHERE idCabeza_Factura = :id";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":id", $id);
                        $command->execute();
                Compra_update::setPagou(null);
                Compra_update::setMediou(null);
                Compra_update::setComprobau(null);
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $id));
            }



            //Esta funcion me agrega los datos del detalle en session compra
            //Agregar datos para la prefactura (create)
            public function actionAdditem()
            {
                //Definimos c como null para comprobación
                $c=null;
                //Abrimos el contenido
                $compra = Compra::getContenidoCompra();

                if(Yii::$app->request->isAjax){
                    $codProdServicio = strtoupper(strval(Yii::$app->request->post('codProdServicio')));
                    $cantidad = Yii::$app->request->post('cantidad');
                    $descuento = Yii::$app->request->post('descuento');

                    //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                    $sql = "SELECT cantidadInventario, cantidadMinima FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $codProdServicio);
                    $idproducto = $command->queryOne();

                    $nuevacantidad = $idproducto['cantidadInventario'] - $cantidad; //Aqui la alamcenamos la nueva cantidad
                   if ($nuevacantidad >= $idproducto['cantidadMinima']){
                            echo 'cantidad_minima';
                        }
                   if ($nuevacantidad >= 0/*$idproducto['cantidadMinima']*/) {
                        //actualizamos en tbl_producto_servicios el la nueva cantidad
                        $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                        $command2 = \Yii::$app->db->createCommand($sql2);
                        $command2->bindParam(":cantidad", $nuevacantidad);
                        $command2->bindParam(":codProdServicio", $codProdServicio);
                        $command2->execute();

                        //Consultamos si el existe producto para aumentar la cantidad
                        if($compra){
                         foreach ($compra as $position => $product)
                            {
                                if(strval($product['cod'])==$codProdServicio)
                                {
                                $compra[$position]['cantidad']+=$cantidad;
                                $c=1;
                                $cantidad_u = floatval($compra[$position]['cantidad']);
                                $usuario = Yii::$app->user->identity->username;
                                $sql3 = "UPDATE tbl_detalle_facturas_temp SET cantidad = :cantidad WHERE usuario = :usuario AND cod = :cod";
                                $command3 = \Yii::$app->db->createCommand($sql3);
                                $command3->bindParam(":cantidad", $cantidad_u);
                                $command3->bindParam(":usuario", $usuario);
                                $command3->bindParam(":cod", $codProdServicio);
                                $command3->execute();
                                }
                            }

                          }
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        $model_ = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                        if($c==null)
                        {
                            $model = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                            $iv = $model->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0.00';
                            if(@$cliente = Compra::getCliente()){
                              if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$cliente])->one())
                              {
                                  $iv = $cliente_ob->iv == true ? '0.00' : $iv;
                              }
                            }
                            $descuent = $model->anularDescuento!='Si' ? $descuento : '0.00';
                            $_POST['cod']= $model->codProdServicio;
                            $_POST['desc']= $model->nombreProductoServicio;
                            $_POST['precio']= $model->precioVenta;//recordar que esto se cambia
                            //$_POST['dcto']= ?;//recordar que esto se cambia
                            $_POST['dcto'] = $descuent;
                            //Iva en forma manual, se puede cargar de la DB si se desea
                            $_POST['iva'] = $iv;
                            $_POST['fecha_hora'] = date('Y-m-d H:i:s',time());
                            $compra[] = $_POST;
                            //respaldamos el producto de la factura en base de datos
                            $producto_db = new DetalleFacturasTemp();
                            $producto_db->usuario = Yii::$app->user->identity->username;
                            $producto_db->cod = $model->codProdServicio;
                            $producto_db->desc = $model->nombreProductoServicio;
                            $producto_db->precio = $model->precioVenta;
                            $producto_db->cantidad = 1;
                            $producto_db->dcto = $descuent;
                            $producto_db->iva = $iv;
                            $producto_db->fecha_hora = date('Y-m-d H:i:s',time());
                            $producto_db->save();

                            if ($descuento && ($descuent!=$descuento)) {
                                Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> El producto '.$model_->nombreProductoServicio.' no se le puede aplicar descuento.');
                            }
                            //echo $model->nombreProductoServicio;
                        }
                        //Guardamos el contenido en session
                        //Compra::setContenidoCompra($compra);

                        if ( $model_->cantidadMinima >= $model_->cantidadInventario) {
                            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$model_->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                            Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                        }
                        //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    }else{
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    }
                    //
                }
                //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> no se obtubo el dato');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                //return $this->render('create', ['time' => date('H:i:s')]);
            }

            //Esta funcion me agrega los datos del detalle en session compra_update
            //Agregar datos para la prefactura (update manual)
            public function actionAdditemu()
            {
                $detalleFac = new DetalleFacturas();
                //Definimos c como null para comprobación
                $c=null;
                //Abrimos el contenido
                //$compra = Compra_update::getContenidoComprau();

                if(Yii::$app->request->isAjax){
                    //obtenemos los datos por post
                    $codProdServicio = strtoupper(strval(Yii::$app->request->post('codProdServicio')));
                    $cantidad = Yii::$app->request->post('cantidad');
                    $descuento = Yii::$app->request->post('descuento');
                    $cabezafactura = Yii::$app->request->post('idCabeza');

                        //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                        $sql_ = "SELECT cantidadInventario, cantidadMinima FROM tbl_producto_servicios WHERE codProdServicio = :codProd";
                        $command_ = \Yii::$app->db->createCommand($sql_);
                        $command_->bindParam(":codProd", $codProdServicio);
                        $idproducto = $command_->queryOne();
                        $nuevacantidad = $idproducto['cantidadInventario'] - $cantidad; //Aqui la alamcenamos la nueva cantidad

                   if ($nuevacantidad >= 0/*$idproducto['cantidadMinima']*/) {
                     $ppc = $nuevacantidad;
                        //actualizamos en tbl_producto_servicios en la nueva cantidad
                        $sql2_ = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                        $command2_ = \Yii::$app->db->createCommand($sql2_);
                        $command2_->bindParam(":cantidad", $nuevacantidad);
                        $command2_->bindParam(":codProdServicio", $codProdServicio);
                        $command2_->execute();

                           //Consultamos si el existe producto para aumentar la cantidad en tbl_detalle_facturas
                            $sql = "SELECT * FROM tbl_detalle_facturas WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                            $command = \Yii::$app->db->createCommand($sql);
                            $command->bindParam(":codProdServicio", $codProdServicio);
                            $command->bindValue(":idCabeza_factura", $cabezafactura);
                            $consulta = $command->queryOne();
                            //if($compra)
                            if(strval($consulta['codProdServicio'])==$codProdServicio && $consulta['idCabeza_factura']==$cabezafactura)
                                {
                                       /* $compra[$position]['cantidad']+=$cantidad;
                                       $c=1;*/
                                       //actualizamos en tbl_detalle_facturas en la nueva cantidad
                                       $cantidadnueva = $consulta['cantidad'];//cantidad de tbl_detalle_facturas
                                       $cantidadnueva+=$cantidad;
                                       $ppc = $consulta['precio_unitario'] * $cantidadnueva;
                                       $sql2 = "UPDATE tbl_detalle_facturas SET cantidad = :cantidad, precio_por_cantidad = :ppc WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                                       $command2 = \Yii::$app->db->createCommand($sql2);
                                       $command2->bindParam(":cantidad", $cantidadnueva);
                                       $command2->bindParam(":ppc", $ppc);
                                       $command2->bindParam(":codProdServicio", $codProdServicio);
                                       $command2->bindValue(":idCabeza_factura", $cabezafactura);
                                       $command2->execute();
                                    $c=1;
                                    $this->historial_producto_in($codProdServicio, $cabezafactura, 1, 'RES');
                                }

                       // }
                        //Si no existe producto repetido, se ingresa normalmente
                        $model_ = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                        if($c==null)
                        {
                            $resultdescuento = $model_->anularDescuento!='Si' ? $descuento : '0';
                            $subtotal_iva = $model_->exlmpuesto!='Si' ? Yii::$app->params['porciento_impuesto'] : '0';
                            $cabeFa = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$cabezafactura])->one();
                              if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$cabeFa->idCliente])->one())
                              {
                                  $subtotal_iva = $cliente_ob->iv == true ? '0.00' : $subtotal_iva;
                              }
                            $precio_por_cantidad = $cantidad*$model_->precioVenta;
                            $descuento_producto = $precio_por_cantidad*($resultdescuento/100);

                            $detalleFac->idCabeza_factura = $cabezafactura;
                            $detalleFac->codProdServicio = $model_->codProdServicio;
                            $detalleFac->cantidad = $cantidad;
                            $detalleFac->precio_unitario = $model_->precioVenta;
                            $detalleFac->precio_por_cantidad = $precio_por_cantidad;
                            $detalleFac->descuento_producto = $descuento_producto;
                            $detalleFac->subtotal_iva = $subtotal_iva;
                            $detalleFac->fecha_hora = date('Y-m-d H:i:s',time());
                            $detalleFac->save();
                            $this->historial_producto_in($model_->codProdServicio, $cabezafactura, 1, 'RES');
                        }
                        //Guardamos el contenido
                        //Compra_update::setContenidoComprau($compra);
                       if ( $model_->cantidadMinima >= $model_->cantidadInventario) {
                            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$model_->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                            Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$cabezafactura));
                        }
                        //Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$cabezafactura));
                    }else{
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$cabezafactura));
                    }

                }
                //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> no se obtubo el dato');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                //Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $cabezafactura));

            }

            //en proceso de forma individual
            public function actionDetalle()
            {
                $this->cargardetallecreate();
                $products = Compra::getContenidoCompra();

                return $this->renderAjax('detalle', ['products' => $products]);
            }

            public function actionDetalle_update()
            {
                $id  = $_GET['id'];
                $model = $this->findModel($id);
                //llemamos la funcion que me permite cargar los datos en el detalle en update
                $this->cargardetalleupdate($model);
                //retornamos la vista del detalle_update
                return $this->renderAjax('detalle_update', [
                    'model' => $model
                ]);
            }

            //esta funcion me ingresa las marcas ot en session-
            public function actionDatos_ot()
            {
              if(Yii::$app->request->isAjax){
                $inp = Yii::$app->request->post('inp');
                $valor = Yii::$app->request->post('valor');
                if ($inp=='cl') {
                  $cliente = Clientes::find()->where(['idCliente'=>$valor])->one();
                  $cliente_ot = Ot::find()->where(['idCliente'=>$valor])->one();
                  $monto_ejecutado = $cliente->montoTotalEjecutado + $cliente_ot->montoTotalEjecutado_ot;
                  if ($cliente->estadoCuenta == 'Cerrada') {
                    Compra::setNombre_ot(null);
                    $notif = '<div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Disculpe!</strong> pero '.$cliente->nombreCompleto.' no tiene permiso por tener cerrado el crédito.
                        </div>';
                  } elseif ($monto_ejecutado >= $cliente->montoMaximoCredito) {
                    Compra::setNombre_ot(null);
                    $notif = '<div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Disculpe!</strong> pero '.$cliente->nombreCompleto.' no tiene permiso por ecceder el monto de crédito + O.T.
                        </div>';
                  }
                  else {
                    Compra::setNombre_ot($valor);
                    $notif = 'aseptado';
                  }
                  echo $notif;
                } elseif ($inp=='pl') {
                  Compra::setPlaca_ot($valor); echo true;
                } elseif ($inp=='ma') {
                  $datos = '<option value="">Seleccionar</option>';
                  $modelos = Modelo::find()->where(['codMarca'=>$valor])->all();
                  foreach ($modelos as $key => $modelo) {
                    $datos .= '<option value="'.$modelo["codModelo"].'">'.$modelo["nombre_modelo"].'</option>';
                  }
                  Compra::setMarca_ot($valor);
                  echo $datos;
                } elseif ($inp=='mo') {
                  Compra::setModelo_ot($valor); echo true;
                } elseif ($inp=='ve') {
                  Compra::setVersion_ot($valor); echo true;
                } elseif ($inp=='an') {
                  Compra::setAno_ot($valor); echo true;
                } elseif ($inp=='mot') {
                  Compra::setMotor_ot($valor); echo true;
                } elseif ($inp=='ci') {
                  Compra::setCilindrada_ot($valor); echo true;
                } elseif ($inp=='co') {
                  Compra::setCombustible_ot($valor); echo true;
                } elseif ($inp=='tr') {
                  Compra::setTransmision_ot($valor); echo true;
                }
              }
            }

            //esta funcion me ingresa las marcas ot en bd-
            public function actionDatos_otu()
            {

              if(Yii::$app->request->isAjax){
                $idCabeza_Factura = Yii::$app->request->post('idCabeza_Factura');
                $inp = Yii::$app->request->post('inp');
                $valor = Yii::$app->request->post('valor');
                $get_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
                if ($inp=='cl') {
                  $cliente = Clientes::find()->where(['idCliente'=>$valor])->one();
                  if ($cliente->estadoCuenta == 'Cerrada') {
                    Compra::setNombre_ot(null);
                    $notif = '<div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Disculpe!</strong> pero '.$cliente->nombreCompleto.' no tiene permiso por tener cerrado el crédito.
                        </div>';
                  } else {

                    $cliente_ant = Ot::find()->where(['idCliente'=>$get_ot->idCliente])->one();
                    $cliente_new = Ot::find()->where(['idCliente'=>$valor])->one();
                    $factura = EncabezadoPrefactura::findOne($idCabeza_Factura);
                    if (@$cliente_ant) {
                      $cliente_ant->montoTotalEjecutado_ot -= $factura->total_a_pagar;//asignamos nuevo valor
                      $cliente_ant->save();
                    }
                    $cliente_new->montoTotalEjecutado_ot += $factura->total_a_pagar;//asignamos nuevo valor
                    $cliente_new->save(); //guardamos los valores en los ot del cliente
                    $get_ot->idCliente = $valor; $notif = 'aseptado';
                  }
                  echo $notif;
                } elseif ($inp=='pl') {
                  $get_ot->placa = $valor; echo true;
                } elseif ($inp=='ma') {
                  $datos = '<option value="">Seleccionar</option>';
                  $modelos = Modelo::find()->where(['codMarca'=>$valor])->all();
                  foreach ($modelos as $key => $modelo) {
                    $datos .= '<option value="'.$modelo["codModelo"].'">'.$modelo["nombre_modelo"].'</option>';
                  }
                  $get_ot->codMarca = $valor; echo true;
                  echo $datos;
                } elseif ($inp=='mo') {
                  $get_ot->codModelo = $valor; echo true;
                } elseif ($inp=='ve') {
                  $get_ot->codVersion = $valor; echo true;
                } elseif ($inp=='an') {
                  $get_ot->ano = $valor; echo true;
                } elseif ($inp=='mot') {
                  $get_ot->motor = $valor; echo true;
                } elseif ($inp=='ci') {
                  $get_ot->cilindrada = $valor; echo true;
                } elseif ($inp=='co') {
                  $get_ot->combustible = $valor; echo true;
                } elseif ($inp=='tr') {
                  $get_ot->transmision = $valor; echo true;
                }
                $get_ot->save();
              }
            }

            //Esta funcion me agrega los datos del detalle en session
            //Agregar datos para la prefactura (update automatico)
           /* public function actionAdditem2()
            {
                //Definimos c como null para comprobación
                $c=null;
                //Baseamos el contenido que puede quedar de otro detalle
                Compra_update::setContenidoComprau(null);
                //Abrimos el contenido
                $compra = Compra_update::getContenidoComprau();

                $query = new yii\db\Query();
                $data = $query->select(['codProdServicio','cantidad','precio_unitario','precio_por_cantidad', 'descuento_producto', 'subtotal_iva'])
                    ->from('tbl_detalle_facturas')
                    ->where(['=','idCabeza_factura',$id_])
                    ->distinct()
                    ->all();
                 foreach($data as $row){
                    $codProdServicio = $row['codProdServicio'];
                    $cantidad = $row['cantidad'];
                    $precio_unitario = $row['precio_unitario'];
                    $descuento = $row['descuento_producto'];
                    $iva = $row['subtotal_iva'];
                   if ($codProdServicio>0) {
                        //Si no existe producto repetido, se asigna lo mandado por POST
                        if($c==null)
                        {
                            $_model = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                            //$_POST['cantidad']= $cantidad;
                            $_POST['cod']= $codProdServicio;
                            $_POST['desc']= $_model->nombreProductoServicio;
                            $_POST['precio']= $precio_unitario;
                            //$_POST['dcto']= ?;//recordar que esto se cambia
                            $_POST['dcto'] = $descuento;
                            //Iva en forma manual, se puede cargar de la DB si se desea
                            $_POST['iva'] = $iva;

                            $compra[] = $_POST;
                            //echo $_model->nombreProductoServicio;
                        }
                        //Guardamos el contenido
                        Compra_update::setContenidoComprau($compra);
                    }
                    //
                }
                //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> no se obtubo el dato');
                Yii::$app->response->redirect(array('cabeza-prefactura/update'));
            }*/

            //Esta funsion me elimina la linea que selecciono eliminar en el detalle de la prefactura - create
            public function actionDeleteitem()
            {
                $id = intval($_GET['id']);
                //$can = intval($_GET['can']);
                $idpro = strval($_GET['idpro']);
                $data = Compra::getContenidoCompra();


                //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                    $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $idpro);
                    $idproducto = $command->queryOne();

                foreach($data as $row){
                    if ($row['cod']==$idpro) {
                        $can = $row['cantidad'];
                        $nuevacantidad = $idproducto['cantidadInventario'] + $can; //Aqui la alamcenamos la nueva cantidad
                        //Actualizamos la cantidad al producto que no será tomado en cuenta
                        $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                        $command2 = \Yii::$app->db->createCommand($sql2);
                        $command2->bindParam(":cantidad", $nuevacantidad);
                        $command2->bindParam(":codProdServicio", $row['cod']);
                        $command2->execute();
                    }
                }
                //Descodificamos el array JSON
                $compra = JSON::decode(Yii::$app->session->get('compra'), true);
                //Eliminamos el atributo pasado por parámetro
                unset($compra[$id]);
                //Volvemos a codificar y guardar el contenido
                Yii::$app->session->set('compra', JSON::encode($compra));
                //Eliminamos de la base de datos
                \Yii::$app->db->createCommand()->delete('tbl_detalle_facturas_temp', 'usuario = "'.Yii::$app->user->identity->username.'" AND cod = "'.$idpro.'"' )->execute();
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));

            }
            /*public function actionDeleteitem()
            {
                $bandera = null;
                $id = intval($_GET['id']);
                $can = intval($_GET['can']);
                $idpro = intval($_GET['idpro']);

                //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                    $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $idpro);
                    $idproducto = $command->queryOne();


                if ($bandera==null) {
                    $nuevacantidad = $idproducto['cantidadInventario'] + $can; //Aqui la alamcenamos la nueva cantidad
                    //Actualizamos la cantidad al producto que no será tomado en cuenta
                    $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                    $command2 = \Yii::$app->db->createCommand($sql2);
                    $command2->bindParam(":cantidad", $nuevacantidad);
                    $command2->bindParam(":codProdServicio", $idpro);
                    $command2->execute();
                }
                //Descodificamos el array JSON
                $compra = JSON::decode(Yii::$app->session->get('compra'), true);
                //Eliminamos el atributo pasado por parámetro
                unset($compra[$id]);
                //Volvemos a codificar y guardar el contenido
                Yii::$app->session->set('compra', JSON::encode($compra));

                Yii::$app->response->redirect(array('cabeza-prefactura/create'));

            }*/

            //Esta funsion me elimina la linea que selecciono eliminar en el detalle de la prefactura - update
            public function actionDeleteitemu()
            {
                $cod = strval($_GET['id0']);//strval($_GET['id0']);
                $id = intval($_GET['id1']);//posicion
                $idCabeza = intval($_GET['id2']);//idCabeza_Factura
                $can = intval($_GET['can']);

                //Obtenemos la cantidad actual de tbl_producto_servicios en base de datos
                    $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $cod);
                    $idproducto = $command->queryOne();
                //Obtenemos la cantidad actual de tbl_detalle_facturas en base de datos
                    $sql3 = "SELECT cantidad FROM tbl_detalle_facturas WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                    $command3 = \Yii::$app->db->createCommand($sql3);
                    $command3->bindParam(":codProdServicio", $cod);
                    $command3->bindValue(":idCabeza_factura", $idCabeza);
                    $idcant = $command3->queryOne();

                    $nuevacantidad = $idproducto['cantidadInventario'] + $idcant['cantidad']; //Aqui la alamcenamos la nueva cantidad con la suma de la cantidad en el detalle más la del inventario


                    $bandera = 'abajo';
                    if ($bandera == 'abajo') {
                        //Descodificamos el array JSON
                        $comprau = JSON::decode(Yii::$app->session->get('comprau'), true);
                        //Eliminamos el atributo pasado por parámetro
                        unset($comprau[$id]);
                        //Volvemos a codificar y guardar el contenido
                        Yii::$app->session->set('comprau', JSON::encode($comprau));
                        //Compra_update::setContenidoComprau(null);
                        //Eliminamos de la base de datos
                        \Yii::$app->db->createCommand()->delete('tbl_detalle_facturas', 'idCabeza_factura = '.$idCabeza.' AND codProdServicio = "'.$cod.'"' )->execute();
                        $bandera = 'arriba';
                    }
                    if ($bandera == 'arriba') {
                        //Actualizamos la cantidad al producto que no será tomado en cuenta
                        $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                        $command2 = \Yii::$app->db->createCommand($sql2);
                        $command2->bindParam(":cantidad", $nuevacantidad);
                        $command2->bindParam(":codProdServicio", $cod);
                        $command2->execute();
                        $this->historial_producto_in($cod, $idCabeza, $idcant['cantidad'], 'SUM');
                    }




                //Regresamos a la prefactura
                Yii::$app->response->redirect(array( 'cabeza-prefactura/update', 'id' => $idCabeza ));
            }

            //funcion para devolver el dato TOTAL modificando la cantidad para la tabla CREATE en prefactura
            public function actionUpdatecantidad()
            {
                //Abrimos el contenido
                $compra = Compra::getContenidoCompra();

                foreach($_GET as $key => $value)
                    {
                        if(substr($key, 0, 9) == 'cantidad_')//dejo solo el numero de posision
                        {
                            if (!is_numeric($value) || $value <= 0 || $value == ' '|| $value == '')
                                {
                                    throw new \yii\base\Exception('Cantidad Invalida');
                                }
                            else
                                {
                                    $position = explode('_', $key);
                                    $position = $position[1];
                                    //$cantidad = 0;
                                    //Si existe cantidad
                                    if(isset($compra[$position]['cantidad'])){

                                      $cod = $compra[$position]['cod'];
                                      $cantidad = $compra[$position]['cantidad'];
                                      //$compra[$position]['cantidad'] = $value;

                                      //$cantidad: monto anterior, $value: valor nuevo
                                      if ($this->movimientocantidad($value, $cantidad, $cod)==true) {
                                          $compra[$position]['cantidad'] = $value;
                                          $precio=$compra[$position]['precio']*$value;//$compra[$position]['cantidad'];

                                          $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                                          $usuario = Yii::$app->user->identity->username;
                                          $sql3 = "UPDATE tbl_detalle_facturas_temp SET cantidad = :cantidad WHERE usuario = :usuario AND cod = :cod";
                                          $command3 = \Yii::$app->db->createCommand($sql3);
                                          $command3->bindParam(":cantidad", $value);
                                          $command3->bindParam(":usuario", $usuario);
                                          $command3->bindParam(":cod", $cod);
                                          $command3->execute();
                                          if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario) {
                                              Compra::setContenidoCompra($compra);
                                              Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                                              Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                                            }
                                          else {
                                                  Compra::setContenidoCompra($compra);//Guardamos el contenido en session
                                                  $descuento_producto = (($compra[$position]['precio']*$compra[$position]['dcto'])/100);
                                                  $total = number_format(($precio),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                  $descuento = number_format(($descuento_producto * $compra[$position]['cantidad']),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                  $arr = array('total'=>$total, 'descuento'=>$descuento );
                                                  echo json_encode($arr);
                                                }
                                      } else {
                                          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                                          //Regresamos a la prefactura
                                          Yii::$app->response->redirect(array( 'cabeza-prefactura/create'));
                                      }
                                    }
                                }
                        } $value = null;
                    }

            }

            //aplica descuento a toda la lista en create
            public function actionAplicardescuentogeneral_cr($porc_descuento)
            {
                $usuario = Yii::$app->user->identity->username;
                $detalle_temp = DetalleFacturasTemp::find()->where(['usuario'=>$usuario])->all();
                $bandera = false;
                foreach ($detalle_temp as $key_det => $detalle_prod) {
                  $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$detalle_prod->cod])->one();
                  $porc_descuento = $porc_descuento == '' ? 0 : $porc_descuento;
                  if ($c_producto->anularDescuento!='Si') {
                    $cod = $detalle_prod->cod;
                    $sql3 = "UPDATE tbl_detalle_facturas_temp SET dcto = :dcto WHERE usuario = :usuario AND cod = :cod";
                    $command3 = \Yii::$app->db->createCommand($sql3);
                    $command3->bindParam(":dcto", $porc_descuento);
                    $command3->bindParam(":usuario", $usuario);
                    $command3->bindParam(":cod", $cod);
                    $command3->execute();
                  } else {
                    $bandera = true;
                  }
                }
                if ($bandera == true) {
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-info-sign"></span> <strong>Procesado correctamente!</strong> Pero se encontraron uno o más productos que no tienen permitido descuento.');
                  //Regresamos a la prefactura
                  Yii::$app->response->redirect(array( 'cabeza-prefactura/create'));
                } else {
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok"></span> <strong>Procesado correctamente!</strong>');
                  //Regresamos a la prefactura
                  Yii::$app->response->redirect(array( 'cabeza-prefactura/create'));
                }
            }

            //aplica descuento a toda la lista en update
            public function actionAplicardescuentogeneral_up($porc_descuento, $idCa)
            {
                $detalle_temp = DetalleFacturas::find()->where(['idCabeza_factura'=>$idCa])->all();
                $bandera = false;
                foreach ($detalle_temp as $key_det => $detalle_prod) {
                  $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$detalle_prod->codProdServicio])->one();
                  $porc_descuento = $porc_descuento == '' ? 0 : $porc_descuento;
                  $descuento_producto = round($detalle_prod->precio_unitario * ($porc_descuento/100), 2);
                  if ($c_producto->anularDescuento!='Si') {
                    $cod = $detalle_prod->codProdServicio;
                    $sql3 = "UPDATE tbl_detalle_facturas SET descuento_producto = :descuento_producto WHERE idCabeza_factura = :idCa AND codProdServicio = :cod";
                    $command3 = \Yii::$app->db->createCommand($sql3);
                    $command3->bindParam(":descuento_producto", $descuento_producto);
                    $command3->bindParam(":idCa", $idCa);
                    $command3->bindParam(":cod", $cod);
                    $command3->execute();
                  } else {
                    $bandera = true;
                  }
                }
                if ($bandera == true) {
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-info-sign"></span> <strong>Procesado correctamente!</strong> Pero se encontraron uno o más productos que no tienen permitido descuento.');
                  //Regresamos a la prefactura
                  Yii::$app->response->redirect(array( 'cabeza-prefactura/update', 'id'=>$idCa));
                } else {
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok"></span> <strong>Procesado correctamente!</strong>');
                  //Regresamos a la prefactura
                  Yii::$app->response->redirect(array( 'cabeza-prefactura/update', 'id'=>$idCa));
                }
            }

            //modifica la cantidad y el descuento de la line de la factura en el create
            public function actionModificar_linea_cr($cantidad_input, $porc_descuento, $position)
            {
              $compra = Compra::getContenidoCompra();
              if (!is_numeric($cantidad_input) || $cantidad_input <= 0 || $cantidad_input == ' '|| $cantidad_input == '')
                  {
                      throw new \yii\base\Exception('Cantidad Invalida');
                  }
              else
                  {
                    if(isset($compra[$position]['cantidad'])){

                      $cod = $compra[$position]['cod'];
                      $cantidad = $compra[$position]['cantidad'];
                      //$compra[$position]['cantidad'] = $value;
                      $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                      //$cantidad: monto anterior, $value: valor nuevo
                      if ($this->movimientocantidad($cantidad_input, $cantidad, $cod)==true) {
                          $compra[$position]['cantidad'] = $cantidad_input;
                          $precio=$compra[$position]['precio']*$cantidad_input;//$compra[$position]['cantidad'];
                          $porc_descuento = $porc_descuento == '' ? 0 : $porc_descuento;
                          $porc_descuento = $c_producto->anularDescuento!='Si' ? $porc_descuento : 0;
                          $compra[$position]['dcto'] = $porc_descuento;

                          $usuario = Yii::$app->user->identity->username;
                          $sql3 = "UPDATE tbl_detalle_facturas_temp SET cantidad = :cantidad, dcto = :dcto WHERE usuario = :usuario AND cod = :cod";
                          $command3 = \Yii::$app->db->createCommand($sql3);
                          $command3->bindParam(":cantidad", $cantidad_input);
                          $command3->bindParam(":dcto", $porc_descuento);
                          $command3->bindParam(":usuario", $usuario);
                          $command3->bindParam(":cod", $cod);
                          $command3->execute();
                          $alerta = '';
                          if ($c_producto->anularDescuento=='Si') {
                            $alerta .= '<div class="alert alert-warning" role="alert">
                                    <strong>Alerta!</strong> El producto '.$c_producto->nombreProductoServicio.' tiene anulado el descuento. Considere esta alerta.
                                  </div>';
                          }
                          if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario ) {
                              Compra::setContenidoCompra($compra);
                              //Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                              //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                              $alerta .= '<div class="alert alert-warning" role="alert">
                                      <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.
                                    </div>';
                              echo $alerta;
                            }
                          else {
                                  Compra::setContenidoCompra($compra);//Guardamos el contenido en session
                                  echo $alerta;
                                }
                      } else {
                        echo 'vacio';
                          //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                          //Regresamos a la prefactura
                          //Yii::$app->response->redirect(array( 'cabeza-prefactura/create'));
                      }
                    }
                  }
            }

            public function actionModificar_linea_up($cantidad_input, $porc_descuento, $position)
            {
              $compra = Compra_update::getContenidoComprau();
              $cab = Compra_update::getCabezaFactura();
              if (!is_numeric($cantidad_input) || $cantidad_input <= 0 || $cantidad_input == ' '|| $cantidad_input == '')
                  {
                      throw new \yii\base\Exception('Cantidad Invalida');
                  }
              else
                  {
                    if(isset($compra[$position]['cantidad'])){

                      $cod = $compra[$position]['cod'];
                      $cantidad = $compra[$position]['cantidad'];
                      //$compra[$position]['cantidad'] = $value;
                      $model_ = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();

                      //$cantidad: monto anterior, $value: valor nuevo
                      if ($this->movimientocantidad($cantidad_input, $cantidad, $cod)==true) {
                          $compra[$position]['cantidad'] = $cantidad_input;
                          $precio=$compra[$position]['precio']*$cantidad_input;//$compra[$position]['cantidad'];
                          $porc_descuento = $porc_descuento == '' ? 0 : $porc_descuento;
                          $resultdescuento = $model_->anularDescuento!='Si' ? $porc_descuento : 0;
                          $descuento_producto = $compra[$position]['precio'] * ($resultdescuento/100);
                          //$compra[$position]['dcto'] = $porc_descuento;
                          $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                          $usuario = Yii::$app->user->identity->username;
                          $detalle_factura = DetalleFacturas::find()->where(['idCabeza_factura'=>$cab])
                          ->andWhere('codProdServicio = :codProdServicio',[':codProdServicio'=>$cod])->one();
                          $cantidad_bd = $detalle_factura->cantidad;
                          $detalle_factura->cantidad = $cantidad_input;
                          $detalle_factura->descuento_producto = round($descuento_producto,2);
                          $detalle_factura->precio_por_cantidad = $precio;
                          $detalle_factura->save();

                          if ($compra[$position]['cantidad'] > $cantidad_bd) {
                            $cant_diferencia = $cantidad_bd - $compra[$position]['cantidad'];
                            $this->historial_producto_in($cod, $cab, abs($cant_diferencia), 'RES');
                          } elseif ($compra[$position]['cantidad'] < $cantidad_bd) {
                            $cant_diferencia = $compra[$position]['cantidad'] - $cantidad_bd;
                            $this->historial_producto_in($cod, $cab, abs($cant_diferencia), 'SUM');
                          }

                          $alerta = '';
                          if ($c_producto->anularDescuento=='Si' && $porc_descuento > 0) {
                            $alerta .= '<div class="alert alert-warning" role="alert">
                                    <strong>Alerta!</strong> El producto '.$c_producto->nombreProductoServicio.' tiene anulado el descuento. Considere esta alerta.
                                  </div>';
                          }

                          if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario ) {
                              Compra_update::setContenidoComprau($compra);
                              //Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                              //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                              $alerta .= '<br><div class="alert alert-warning" role="alert">
                                      <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.
                                    </div>';
                              echo $alerta;
                            }
                          else {
                                  Compra_update::setContenidoComprau($compra);//Guardamos el contenido en session
                                  echo $alerta;
                                }
                      } else {
                        echo 'vacio';
                          //Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                          //Regresamos a la prefactura
                          //Yii::$app->response->redirect(array( 'cabeza-prefactura/create'));
                      }
                    }
                  }
            }

            //funcion para devolver el dato TOTAL modificando la cantidad para la tabla UPDATE en prefactura y en la base de datos
            public function actionUpdatecantidadu()
            {
                //Abrimos el contenido
                $compra = Compra_update::getContenidoComprau();
                $cab = Compra_update::getCabezaFactura();

                foreach($_GET as $key => $value)
                    {
                        if(substr($key, 0, 9) == 'cantidad_')
                        {
                            if (!is_numeric($value) || $value <= 0 || $value == ' ')
                                {
                                    throw new \yii\base\Exception('Cantidad Invalida');
                                }
                            else
                                {
                                    $position = explode('_', $key);
                                    $position = $position[1];

                                    //$cantidad = null; $cod = null;

                                    //Si existe cantidad
                                    if(isset($compra[$position]['cantidad']))
                                    {
                                        $cod = $compra[$position]['cod'];
                                        $cantidad = $compra[$position]['cantidad'];
                                    if ($this->movimientocantidad($value, $cantidad, $cod)==true) {
                                        //$tipo = $cantidadnueva > $cantidad ? 'RES' : 'SUM';
                                        $compra[$position]['cantidad'] = $value;
                                        $precio=$compra[$position]['precio']*$value;//$compra[$position]['cantidad'];

                                        //Guardamos cantidad en base de datos
                                        if ($cod!='0') {
                                            /*$sql2 = "UPDATE tbl_detalle_facturas SET cantidad = :cantidad WHERE codProdServicio = :codProdServicio AND idCabeza_factura = :idCabeza_factura";
                                            $command2 = \Yii::$app->db->createCommand($sql2);
                                            $command2->bindParam(":cantidad", $compra[$position]['cantidad']);
                                            $command2->bindParam(":codProdServicio", $cod);
                                            $command2->bindValue(":idCabeza_factura", $cab);
                                            $command2->execute();*/
                                            $detalle_factura = DetalleFacturas::find()->where(['idCabeza_factura'=>$cab])
                                            ->andWhere('codProdServicio = :codProdServicio',[':codProdServicio'=>$cod])->one();
                                            $cantidad_bd = $detalle_factura->cantidad;
                                            $detalle_factura->cantidad = $compra[$position]['cantidad'];
                                            $detalle_factura->precio_por_cantidad = $precio;
                                            $detalle_factura->save();
                                            if ($compra[$position]['cantidad'] > $cantidad_bd) {
                                              $cant_diferencia = $cantidad_bd - $compra[$position]['cantidad'];
                                              $this->historial_producto_in($cod, $cab, abs($cant_diferencia), 'RES');
                                            } elseif ($compra[$position]['cantidad'] < $cantidad_bd) {
                                              $cant_diferencia = $compra[$position]['cantidad'] - $cantidad_bd;
                                              $this->historial_producto_in($cod, $cab, abs($cant_diferencia), 'SUM');
                                            }
                                        }
                                        $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                                        if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario ) {
                                            Compra_update::setContenidoComprau($compra);
                                            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                                            Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$cab));
                                          }
                                        else {
                                                Compra_update::setContenidoComprau($compra);//Guardamos el contenido en session
                                                $total = number_format(($precio),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                $descuento = number_format(($detalle_factura->descuento_producto * $compra[$position]['cantidad']),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                $arr = array('total'=>$total, 'descuento'=>$descuento );
                                                echo json_encode($arr);
                                              }
                                    } else {
                                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                                        //Regresamos a la prefactura
                                        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$cab));
                                    }
                                  }
                                }
                        }
                    }

            }//fin de la funcion
            //llamo esta funcipon cada ves que edito la cantidad para que me actualice en tiempo real la cantidad de cada producto agregado o quitado en la prefactura o factura
            public function movimientocantidad($val, $cant, $cod)
            {//$cant: monto anterior, $val: valor actual
              $inventario = ProductoServicios::findOne($cod);
                 //Esta variable cuenta con la condicion si suma o resta la cantidad de los productos de inventario

                 $cantidadnueva = $inventario->cantidadInventario + ($cant - $val);
             //Si la cantidad es mayor o igual a cantidad accede a proceder con la actualizacion de cantidad en inventario
             if ($cantidadnueva >= 0/*$cantidad['cantidadMinima']*/) {

                  $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                  $command2 = \Yii::$app->db->createCommand($sql2);
                  $command2->bindParam(":cantidad", $cantidadnueva);
                  $command2->bindParam(":codProdServicio", $cod);
                  $command2->execute();

                  $cantidadnueva = null;
                  return true;
                }
                else {
                    return false;
                }
            }

            //función para actualizar DESCUENTO desde la edicion de cantidad en la tabla de CREATE
            public function actionUpdatedescuento()
            {
                //Abrimos el contenido
                $compra = Compra::getContenidoCompra();
                $value = null;
                foreach($_GET as $key => $value)
                    {
                        if(substr($key, 0, 9) == 'cantidad_')
                        {

                            if (!is_numeric($value) || $value <= 0 || $value == ' ')
                                {
                                    throw new \yii\base\Exception('Cantidad Invalida');
                                }
                            else
                                {
                                    $position = explode('_', $key);
                                    $position = $position[1];

                                    $cantidad = null; $cod = null;
                                    //Si existe cantidad
                                    if(isset($compra[$position]['cantidad']))
                                    {
                                        $cod = $compra[$position]['cod'];
                                        $cantidad = $compra[$position]['cantidad'];
                                    }
                                    //echo number_format($compra[$position]['cantidad']*(($compra[$position]['precio']*$compra[$position]['dcto'])/100),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                    //echo $value*$precio;
                                    /*if ($this->movimientocantidad($value, $cantidad, $cod)==true) {
                                        $compra[$position]['cantidad'] = $value;
                                        echo number_format($compra[$position]['cantidad']*(($compra[$position]['precio']*$compra[$position]['dcto'])/100),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                        $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                                        if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario) {
                                            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                                            Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                                            return Compra::setContenidoCompra($compra);
                                        }
                                        else { return Compra::setContenidoCompra($compra); }
                                    }
                                    else {
                                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                                        //Regresamos a la prefactura
                                        Yii::$app->response->redirect(array( 'cabeza-prefactura/create'));
                                    }*/

                                       //Guardamos el contenido
                                    //return Compra::setContenidoCompra($compra);
                                }
                        }  $value = null;
                    }

            }
            //función para actualizar DESCUENTO desde la edicion de cantidad en la tabla de prefactura UPDATE
            public function actionUpdatedescuentou()
            {
                //Abrimos el contenido
                $compra = Compra_update::getContenidoComprau();
                $cab = Compra_update::getCabezaFactura();

                foreach($_GET as $key => $value)
                    {
                        if(substr($key, 0, 9) == 'cantidad_')
                        {
                            if (!is_numeric($value) || $value <= 0 || $value == ' ')
                                {
                                    throw new \yii\base\Exception('Cantidad Invalida');
                                }
                            else
                                {
                                    $position = explode('_', $key);
                                    $position = $position[1];
                                    $cantidad = null; $cod = null; $c_producto = null;
                                    //Si existe cantidad
                                    if(isset($compra[$position]['cantidad']))
                                    {
                                        $cod = $compra[$position]['cod'];
                                        $cantidad = $compra[$position]['cantidad'];
                                    }
                                    /*
                                    if ($this->movimientocantidad($value, $cantidad, $cod)==true) {
                                        $compra[$position]['cantidad'] = $value;
                                        //echo round($compra[$position]['cantidad']*(($compra[$position]['precio']*$compra[$position]['dcto'])/100),2);
                                        echo number_format($compra[$position]['cantidad']*(($compra[$position]['dcto'])),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                        //round($product['cantidad']*(($product['precio']*$product['dcto'])/100),2),
                                        //echo $value*$precio;
                                           //Guardamos el contenido

                                        $c_producto = ProductoServicios::find()->where(['codProdServicio'=>$cod])->one();
                                        if ( $c_producto->cantidadMinima >= $c_producto->cantidadInventario) {
                                            Yii::$app->getSession()->setFlash('warning', '<span class="glyphicon glyphicon-flag"></span> <strong>Alerta!</strong> La cantidad del producto '.$c_producto->nombreProductoServicio.' ya alcanzó o sobrepasa la cantidad mínima. Considere esta alerta.');
                                            Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$cab));
                                            return Compra_update::setContenidoComprau($compra);
                                        }
                                        else { return Compra_update::setContenidoComprau($compra); }
                                    }
                                    else {
                                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.');
                                        //Regresamos a la prefactura
                                        Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $cab));
                                    }*/
                                }
                        } $value = null;
                    }

            }

           /* public function actionUpdateprecio()
            {
            //Para actualizar precio
            $compra= Compra::getContenidoCompra();

            foreach(Yii::$app->request->get() as $key => $value) //$_GET
             {
                if(substr($key, 0, 7) == 'precio_')
                {
                    if (!is_numeric($value) || $value <= 0 || $value == ' ')
                        throw new \yii\base\Exception('Precio Invalido');
                    $position = explode('_', $key);
                    $position = $position[1];
                    if(isset($compra[$position]['precio']))
                    $compra[$position]['precio'] = $value;
                    $precio=$compra[$position]['precio']-(($compra[$position]['precio']*$compra[$position]['dcto'])/100);
                        echo $precio*$compra[$position]['cantidad'];
                        return Compra::setContenidoCompra($compra);
                }
             }
            }*/
         /*   public function actionUpdatepreciou()
            {
            //Para actualizar precio
            $compra = Compra_update::getContenidoComprau();

            foreach(Yii::$app->request->get() as $key => $value) //$_GET
             {
                if(substr($key, 0, 7) == 'precio_')
                {
                    if (!is_numeric($value) || $value <= 0 || $value == ' ')
                        throw new \yii\base\Exception('Precio Invalido');
                    $position = explode('_', $key);
                    $position = $position[1];
                    if(isset($compra[$position]['precio']))
                    $compra[$position]['precio'] = $value;
                    $precio=$compra[$position]['precio']-(($compra[$position]['precio']*$compra[$position]['dcto'])/100);
                        echo $precio*$compra[$position]['cantidad'];
                        return Compra_update::setContenidoCompra($comprau);
                }
             }
            }*/

            public function actionAddpro() {
            //Imprimimos el resumen
            echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'id' => 'codProd', 'class' => 'form-control', 'placeholder' =>'Ingrese código de producto aqui', 'onkeyup'=>'javascript:this.value=Mayuculas(event.keyCode, this.value)', 'onchange' => 'javascript:agrega_producto(this.value)']);
            }

            public function actionGetpreciototal() {
            //Imprimimos el resumen
            echo Compra::getTotal(false);
            }
            public function actionGetpreciototalu() {
            //Imprimimos el resumen
            echo Compra_update::getTotalu();
            }

            public function actionCrear_apartado()
            {
              $products = Compra_update::getContenidoComprau();
              $vendedor = Compra_update::getVendedoru();
              $idCliente = Compra_update::getClienteu();
              $pago = Compra_update::getPagou();
              $idC = Compra_update::getCabezaFactura();
              if($products && $vendedor && $idCliente && $pago){
                $cliente_apartado = ClientesApartados::find()->where(['idCliente'=>$idCliente])->one();
                if (@$cliente_apartado && $cliente_apartado->estado == 'Abierta') {
                  $factura = EncabezadoPrefactura::findOne($idC);
                  $apartados = EncabezadoPrefactura::find()->where(['idCliente'=>$idCliente])->andWhere('estadoFactura = "Apartado"',[])->all();
                  $monto_disponible_apartado = 0;
                  $vencida = 'no';
                  foreach ($apartados as $key => $apartado) {
                    $movi_apartados = MovimientoApartado::find()->where(['idCabeza_Factura'=>$apartado->idCabeza_Factura])->all();
                    $mov_apartados = 0;
                    foreach ($movi_apartados as $key => $m_apatado) { $mov_apartados += $m_apatado->monto_movimiento; }
                    $monto_disponible_apartado += $apartado->total_a_pagar - $mov_apartados;
                    $fecha_vencimiento = strtotime ( '+'.$cliente_apartado->dias_apartado.' day' , strtotime ( $apartado->fecha_inicio ) ) ;
                    if ($fecha_vencimiento < strtotime(date('Y-m-d'))) {
                      $vencida = 'si';
                    }
                  }
                if ($vencida=='si') {
                  Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, Este cliente presenta apartados vencidos.');
                  Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $idC));
                } else {
                  //si el monto maximo de apartado es mayor a a suma del monto disponile para apartado mas el total de la factura entrante.
                  if ($cliente_apartado->montoMaximoApartado > ($factura->total_a_pagar+$monto_disponible_apartado)) {
                    $nuevoestado = 'Apartado';
                    $cabeFa = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$idC])->one();
                    $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado, apartado = 'x' WHERE idCabeza_Factura = :idC";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":estado", $nuevoestado);
                    $command->bindValue(":idC", $idC);
                    $command->execute();
                    Yii::$app->getSession()->setFlash('success', '<center><h1><span class="fa fa-heart-o"></span><br><small><strong>Éxito!</strong> Apartada correctamente.<br></small><br>Corresponde al ID: '.$idC.'<h1></center>');
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $idC));
                  } else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, Este cliente excede el monto máximo de Apartados.');
                    //Regresamos a la prefactura
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $idC));
                  }
                }
              } else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, Asegúrese de que el cliente tenga abierto el permiso de apartado.');
                //Regresamos a la prefactura
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $idC));
              }
              } else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, Asegúrese de que los datos estén completos. No puede dejar espacios en blanco o enviar una factura a apartados.');
                //Regresamos a la prefactura
                Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $idC));
              }
            }

            public function actionApartados($idCabeza_Factura, $cliente, $fecha_inicio)
            {
              $fecha_inicio = $fecha_inicio!='' ? '%'.date('Y-m-d', strtotime( $fecha_inicio )).'%' : '%%';
              return $this->renderAjax('apartados',[
                'idCabeza_Factura' => $idCabeza_Factura,
                'cliente' => $cliente,
                'fecha_inicio' => $fecha_inicio
              ]);
            }

            public function actionMov_apartados($id)
            {
              return $this->renderAjax('mov_apartados',['id' => $id]);
            }

            public function actionCaja()
                {
                  $products = Compra_update::getContenidoComprau();
                  $vendedor = Compra_update::getVendedoru();
                  $cliente = Compra_update::getClienteu();
                  $pago = Compra_update::getPagou();
                  $idC = Compra_update::getCabezaFactura();
                  if($products && $vendedor && $cliente && $pago){
                    $cabeFa = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$idC])->one();


                    $nuevoestado = ($cabeFa->tipoFacturacion != 5) ? 'Caja' : 'Crédito Caja';
                    if ($nuevoestado == 'Crédito Caja') {
                        $this->condicion_credito();
                    } else {
                        $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado WHERE idCabeza_Factura = :idC";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":estado", $nuevoestado);
                        $command->bindValue(":idC", $idC);
                        $command->execute();

                        if ($cabeFa->idOrdenServicio!=null) {//si hay orden de servicio se ejecuta
                            $estadoO = 0;
                            $cierre = date("Y-m-d");
                            $idOS = $cabeFa->idOrdenServicio;
                            $sql2 = "UPDATE tbl_orden_servicio SET estado = :estado, fechaCierre = :cierre WHERE idOrdenServicio = :idOS";
                            $command2 = \Yii::$app->db->createCommand($sql2);
                            $command2->bindParam(":estado", $estadoO);
                            $command2->bindParam(":cierre", $cierre);
                            $command2->bindValue(":idOS", $idOS);
                            $command2->execute();
                        }
                        //si hay orden de trabajo se ejecuta
                        if (@$fact_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$cabeFa->idCabeza_Factura])->one()) {
                          $cliente_ot = Ot::find()->where(['idCliente'=>$fact_ot->idCliente])->one();//katy
                          $monto_ejecutado = $cliente_ot->montoTotalEjecutado_ot;
                          $cliente_ot->montoTotalEjecutado_ot = $monto_ejecutado - $cabeFa->total_a_pagar;
                          $cliente_ot->save();
                        }
                        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La factura ha sido enviada a caja correctamente.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/index'));
                    }
                  } else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Disculpa, Asegúrese de que los datos estén completos. No puede dejar espacios en blanco o enviar una factura a caja sin productos.');
                    //Regresamos a la prefactura
                    Yii::$app->response->redirect(array('cabeza-prefactura/update','id' => $idC));
                  }
                }

            public function actionDevolverfactura($idC)
                {
                    $nuevoestado = 'Pendiente';
                        $cabeFa = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$idC])->one();
                        $modelclient = Clientes::find()->where(['idCliente'=>$cabeFa->idCliente])->one();
                        $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado WHERE idCabeza_Factura = :idC";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":estado", $nuevoestado);
                        $command->bindValue(":idC", $idC);
                        $command->execute();
                        if ($cabeFa->estadoFactura == 'Crédito Caja') {
                          $idCliente = $modelclient->idCliente;
                          $monto_a_credito = $modelclient->montoTotalEjecutado - $cabeFa->total_a_pagar;
                          $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
                          $command = \Yii::$app->db->createCommand($sql);
                          $command->bindParam(":montoTotalEjecutado", $monto_a_credito);
                          $command->bindValue(":idCliente", $idCliente);
                          $command->execute();
                        }
                        if (@$fact_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$cabeFa->idCabeza_Factura])->one()) {
                          $cliente_ot = Ot::find()->where(['idCliente'=>$fact_ot->idCliente])->one();//katy
                          $monto_ejecutado = $cliente_ot->montoTotalEjecutado_ot;
                          $cliente_ot->montoTotalEjecutado_ot = $monto_ejecutado + $cabeFa->total_a_pagar;
                          $cliente_ot->save();
                        }
                        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La factura '.$idC.' ha sido regresada a prefactura.');
                        Yii::$app->response->redirect(array('cabeza-caja/index'));
                }

            public function condicion_credito()
                {
                    $idC = Compra_update::getCabezaFactura();
                    $cabeFa = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$idC])->one();
                    $cliente = Compra_update::getClienteu();
                    if($modelclient = Clientes::find()->where(['idCliente'=>$cliente])->one()){
                    if ((($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Abierta')) || (($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Cerrada') && ($modelclient->autorizacion == 'Si'))) {
                        $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                        $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                        if ($cabeFa->total_a_pagar > $montodisponiblecredito) {
                            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Esta factura no puede ser acreditada porque excede el monto de crédito permitido del cliente.');
                            Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$idC ));
                        }
                        else{
                        $nuevoestado = 'Crédito Caja';
                        //$fechafin = date('Y-m-d');
                        //$sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado, fecha_final = :fechafin WHERE idCabeza_Factura = :idC";$sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado, fecha_final = :fechafin WHERE idCabeza_Factura = :idC";
                        $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado WHERE idCabeza_Factura = :idC";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":estado", $nuevoestado);
                        //$command->bindParam(":fechafin", $fechafin);
                        $command->bindValue(":idC", $idC);
                        $command->execute();

                        $idCliente = $modelclient->idCliente;
                        $monto_a_credito = $cabeFa->total_a_pagar + $modelclient->montoTotalEjecutado;
                        $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":montoTotalEjecutado", $monto_a_credito);
                        $command->bindValue(":idCliente", $idCliente);
                        $command->execute();

                        if ($cabeFa->idOrdenServicio!=null) {
                            $estadoO = 0;
                            $cierre = date("Y-m-d");
                            $idOS = $cabeFa->idOrdenServicio;
                            $sql2 = "UPDATE tbl_orden_servicio SET estado = :estado, fechaCierre = :cierre WHERE idOrdenServicio = :idOS";
                            $command2 = \Yii::$app->db->createCommand($sql2);
                            $command2->bindParam(":estado", $estadoO);
                            $command2->bindParam(":cierre", $cierre);
                            $command2->bindValue(":idOS", $idOS);
                            $command2->execute();
                        }
                        if (@$fact_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$idC])->one()) {
                          $cliente_ot = Ot::find()->where(['idCliente'=>$fact_ot->idCliente])->one();//katy
                          $monto_ejecutado = $cliente_ot->montoTotalEjecutado_ot;
                          $cliente_ot->montoTotalEjecutado_ot = $monto_ejecutado - $cabeFa->total_a_pagar;
                          $cliente_ot->save();
                        }
                        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Factura acreditada correctamente.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/index'));
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente NO TIENE CRÉDITO AUTORIZADO.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$idC ));
                    }
                    }else{
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente NO TIENE CRÉDITO AUTORIZADO.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$idC ));
                    }
                }

            public function actionDesechar()
            {
                //$data = Compra_update::getContenidoComprau();
                $idC = Compra_update::getCabezaFactura();
                $prefactura = $this->findModel($idC);
                $orden_servicio = OrdenServicio::findOne($prefactura->idOrdenServicio);
                if (@$orden_servicio) {//compruebo si cuenta con orden de servicio
                  Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong> Esta factura cuenta con una Orden de Servicio, elimine la Orden <strong>ID num. '.$prefactura->idOrdenServicio.'</strong> antes de desechar la factura');
                  Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$idC));
                } else {

                $bandera = 0;
                if ($data = Compra_update::getContenidoComprau()) {
                  $this->historial_producto($idC, 'SUM', 'Prefactura');
                  foreach($data as $row){
                    $cantidad = $row['cantidad'];
                    $codProdServicio = $row['cod'];

                    //Obtenemos la cantidad al producto que queremos modificar
                    $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                    $command = \Yii::$app->db->createCommand($sql);
                    $command->bindParam(":codProdServicio", $codProdServicio);
                    $cant = $command->queryOne();

                    $nuevacantidad = $cant['cantidadInventario'] + $cantidad;
                    //actualizamos nueva cantidad a cada uno de los productos
                    $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                        $command2 = \Yii::$app->db->createCommand($sql2);
                        $command2->bindParam(":cantidad", $nuevacantidad);
                        $command2->bindValue(":codProdServicio", $codProdServicio);
                        $command2->execute();
                        $bandera = 1;
                }//fin  foreach para devolver el producto
              }
          //  if ($bandera == 1) {
                $bandera = true;
                $nuevoestado = 'Anulada';
                $fechafin = date('Y-m-d');
                $sql3 = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado, fecha_final = :fechafin WHERE idCabeza_Factura = :idC";
                $command3 = \Yii::$app->db->createCommand($sql3);
                $command3->bindParam(":estado", $nuevoestado);
                $command3->bindParam(":fechafin", $fechafin);
                $command3->bindValue(":idC", $idC);
                $command3->execute();
                $estado = Compra_update::getEstadoFacturau();
                if ($estado == 'Crédito Caja') {
                    $bandera = false;
                    $modelestado = $this->findModel($idC);
                    $modelclient = Clientes::find()->where(['idCliente'=>$modelestado->idCliente])->one();
                    if (@$modelclient) {
                      $idCliente = $modelclient->idCliente;
                      $monto_a_credito = $modelclient->montoTotalEjecutado - $modelestado->total_a_pagar;
                      $sql4 = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
                      $command4 = \Yii::$app->db->createCommand($sql4);
                      $command4->bindParam(":montoTotalEjecutado", $monto_a_credito);
                      $command4->bindValue(":idCliente", $idCliente);
                      $command4->execute();

                    }
                }
                if ($bandera==true) { //dependiendo por donde pase me debuelve a cabeza.prefactura o a cabeza.caja
                  if (@$fact_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$idC])->one()) {
                    if(@$cliente_ot = Ot::find()->where(['idCliente'=>$fact_ot->idCliente])->one())//katy
                    { $monto_ejecutado = $cliente_ot->montoTotalEjecutado_ot;
                      $cliente_ot->montoTotalEjecutado_ot = $monto_ejecutado - $prefactura->total_a_pagar;
                      $cliente_ot->save();
                    }
                  }
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Factura desechada correctamente.');
                    Yii::$app->response->redirect(array('cabeza-prefactura/index'));
                } else {
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Factura desechada correctamente.');
                    Yii::$app->response->redirect(array('cabeza-caja/index'));
                }

          //  }
            /*}else{
                \Yii::$app->db->createCommand()->delete('tbl_encabezado_factura', 'idCabeza_Factura = '.(int) $idC)->execute();
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>ATENCIÓN!</strong><br><br>La factura fue eliminada por falta de detalles.');
                Yii::$app->response->redirect(array('cabeza-prefactura/index'));
            }*/


            }
            }

            //llena el historial del producto deacuerdo a cada movimiento de ingreso del producto a la ventas
            public function historial_producto($idFa, $tipo, $origen)
            {
              //ingresar producto en historial de producto
              $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$idFa])->all();
              foreach ($detalle_venta as $key => $prod) {
                $inventario = ProductoServicios::find()->where(['codProdServicio'=>$prod->codProdServicio])->one();
                if (@$inventario) {
                  $codProdServicio = $inventario->codProdServicio;
                  $fecha = date('Y-m-d H:i:s',time());
                  $id_documento = $prod->idCabeza_factura;
                  $cant_mov = $prod->cantidad;
                  if ($tipo=='RES') {
                    $cant_ant = $inventario->cantidadInventario + $cant_mov;
                    $cant_new =  $inventario->cantidadInventario;
                  } else {
                    $cant_ant = $inventario->cantidadInventario;
                    $cant_new =  $inventario->cantidadInventario + $cant_mov;
                  }
                  $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
                  $usuario = $funcionario->idFuncionario;
                  $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
                  $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
                  $position_h_p->bindParam(":codProdServicio", $codProdServicio);
                  $position_h_p->bindParam(":fecha", $fecha);
                  $position_h_p->bindParam(":origen", $origen);
                  $position_h_p->bindParam(":id_documento", $id_documento);
                  $position_h_p->bindValue(":cant_ant", $cant_ant);
                  $position_h_p->bindParam(":tipo", $tipo);
                  $position_h_p->bindValue(":cant_mov", $cant_mov);
                  $position_h_p->bindValue(":cant_new", $cant_new);
                  $position_h_p->bindParam(":usuario", $usuario);
                  $position_h_p->execute();
                }

              }
            }

            //llena el historial del producto deacuerdo a cada movimiento de ingreso del producto a la ventas
            public function historial_producto_in($codProdServicio, $idFa, $cant_mov, $tipo)
            {
              //ingresar producto en historial de producto
                $inventario = ProductoServicios::find()->where(['codProdServicio'=>$codProdServicio])->one();
                //$codProdServicio = $inventario->codProdServicio;
                $fecha = date('Y-m-d H:i:s',time());
                $origen = 'Prefactura';
                $id_documento = $idFa;
                if ($tipo=='RES') {
                  $cant_ant = $inventario->cantidadInventario + $cant_mov;
                  $cant_new = $inventario->cantidadInventario;
                } else {
                  $cant_ant = $inventario->cantidadInventario - $cant_mov;
                  $cant_new =  $inventario->cantidadInventario;
                }

                $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
                $usuario = $funcionario->idFuncionario;
                $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
                $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
                $position_h_p->bindParam(":codProdServicio", $codProdServicio);
                $position_h_p->bindParam(":fecha", $fecha);
                $position_h_p->bindParam(":origen", $origen);
                $position_h_p->bindParam(":id_documento", $id_documento);
                $position_h_p->bindValue(":cant_ant", $cant_ant);
                $position_h_p->bindParam(":tipo", $tipo);
                $position_h_p->bindValue(":cant_mov", $cant_mov);
                $position_h_p->bindValue(":cant_new", $cant_new);
                $position_h_p->bindParam(":usuario", $usuario);
                $position_h_p->execute();

            }

            //Esta funsión permite vaciar todo loa datos en session y debolver la cantidad de los productos a inventario
            public function actionDeleteall()
            {
            //Para borrar todo
            if ($data = Compra::getContenidoCompra())
            foreach($data as $row){
                $cantidad = $row['cantidad'];
                $codProdServicio = $row['cod'];

                //Obtenemos la cantidad al producto que queremos modificar
                $sql = "SELECT cantidadInventario FROM tbl_producto_servicios WHERE codProdServicio = :codProdServicio";
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":codProdServicio", $codProdServicio);
                $cant = $command->queryOne();

                $nuevacantidad = $cant['cantidadInventario'] + $cantidad;
                //actualizamos nueva cantidad a cada uno de los productos
                $sql2 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidad WHERE codProdServicio = :codProdServicio";
                    $command2 = \Yii::$app->db->createCommand($sql2);
                    $command2->bindParam(":cantidad", $nuevacantidad);
                    $command2->bindValue(":codProdServicio", $codProdServicio);
                    $command2->execute();
            }
            $this->borrarTodo();
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Datos borrados correctamente.');
            Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }
            /*public function actionDeleteallU()
            {
            //Para borrar todo
            $this->borrarTodoU();
            Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }*/

            public function actionComplete(){
                if(($estadoF = Compra::getEstadoFactura()) && ($FechaCompraIni = Compra::getFechaInicial()) && ($vendedor = Compra::getVendedor()) && ($cliente = Compra::getCliente()) && ($pago = Compra::getPago()) && ($productos = Compra::getContenidoCompra()) )
                {
                    $CabCompra = new EncabezadoPrefactura();
                    $resumen = Compra::getTotal(true);
                   /* $CabCompra->attributes = array(
                            'fecha_inicio' => date('Y-m-d'),
                            'idCliente' => $cliente,
                            //'idOrdenServicio'
                            'porc_descuento' => $resumen['dcto'],
                            'iva' => $resumen['iva'],
                            'total_a_pagar' => $resumen['total'],
                            'estadoFactura' => $estadoF,
                            'tipoFacturacion' => $pago,
                            'codigoVendedor' => $vendedor,
                            'subtotal' => $resumen['subtotal']
                    );*/
                    $CabCompra->fecha_inicio = date('Y-m-d');
                    $CabCompra->idCliente = $cliente;
                    $CabCompra->porc_descuento = $resumen['dcto'];
                    $CabCompra->iva = $resumen['iva'];
                    $CabCompra->total_a_pagar = $resumen['total'];
                    $CabCompra->estadoFactura = $estadoF;
                    $CabCompra->tipoFacturacion = $pago;
                    $CabCompra->idAgenteComision = Compra::getAgenteComision() ? Compra::getAgenteComision() : null;
                    $CabCompra->idAgenteComision = Compra_update::getAgenteComisionu() ? Compra_update::getAgenteComisionu() : null;
                    $CabCompra->codigoVendedor = $vendedor;
                    $CabCompra->subtotal = $resumen['subtotal'];
                    $CabCompra->observacion = Compra::getObserva() ? Compra::getObserva() : 'Ninguna';
                    $CabCompra->ignorar_porc_ser = Compra::getServicio_porc_ign() ? Compra::getServicio_porc_ign() : '';
                  //  if ($CabCompra->validate())
                    //{   #guardamos cabecera de factura
                        if ($CabCompra->save()) {
                        // guardamos detalle factura
                            foreach($productos as $position => $product)
                            {
                                $position = new DetalleFacturas();
                                $position->idCabeza_factura = $CabCompra->idCabeza_Factura;
                                $position->codProdServicio = $product['cod'];
                                $position->cantidad = $product['cantidad'];
                                $position->precio_unitario = $product['precio'];
                                $position->precio_por_cantidad = round(($product['precio']*$product['cantidad']),2);
                                $position->descuento_producto = round(/*$product['cantidad']**/(($product['precio']*$product['dcto'])/100),2);
                                $position->subtotal_iva = $product['iva'];
                                $position->fecha_hora = $product['fecha_hora'];
                                $position->save();
                            }
                            if (@$get_ot = Compra::getOT()) {
                              $this->ot_incluido($CabCompra->idCabeza_Factura, 'pr');
                            }
                            $this->historial_producto($CabCompra->idCabeza_Factura, 'RES', 'Prefactura');
                            $this->borrarTodo();
                            Yii::$app->getSession()->setFlash('success', '<center><h1><span class="glyphicon glyphicon-ok-sign"></span> <small><strong>Éxito!</strong> Prefactura almacenada correctamente.</small><br>Corresponde al ID: '.$CabCompra->idCabeza_Factura.'<h1></center>');
                            Yii::$app->response->redirect(array('cabeza-prefactura/update', 'id'=>$CabCompra->idCabeza_Factura));
                        }
                  //  }
                    else {
                        $error='';
                        foreach ($CabCompra->getErrors() as $position => $er)
                        $error.=$er[0].'<br/>';
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br>'.$CabCompra->estadoFactura.'Disculpa, no se efectuó el proceso de prefacturación.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    }

                } else {
                 Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Asegúrese de que los datos estén completos.');
                 Yii::$app->response->redirect(array('cabeza-prefactura/create'));
               }
                //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Prefactura almacenada correctamente.');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }

            public function ot_incluido($factura, $mod)
            {
              $factura_ot = new FacturaOt();
              $factura_ot->idCabeza_Factura = $factura;
              $factura_ot->idCliente = Compra::getNombre_ot() ? Compra::getNombre_ot() : '';
              $factura_ot->placa = Compra::getPlaca_ot() ? Compra::getPlaca_ot() : '';
              $factura_ot->codMarca = Compra::getMarca_ot() ? Compra::getMarca_ot() : '';
              $factura_ot->codModelo = Compra::getModelo_ot() ? Compra::getModelo_ot() : '';
              $factura_ot->codVersion = Compra::getVersion_ot() ? Compra::getVersion_ot() : '';
              $factura_ot->ano = Compra::getAno_ot() ? Compra::getAno_ot() : '';
              $factura_ot->motor = Compra::getMotor_ot() ? Compra::getMotor_ot() : '';
              $factura_ot->cilindrada = Compra::getCilindrada_ot() ? Compra::getCilindrada_ot() : '';
              $factura_ot->combustible = Compra::getCombustible_ot() ? Compra::getCombustible_ot() : '';
              $factura_ot->transmision = Compra::getTransmision_ot() ? Compra::getTransmision_ot() : '';
              $factura_ot->save();
              if ($mod=='pr') {
                $fact_obj = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$factura])->one();
                if (@$cliente_ot = Ot::find()->where(['idCliente'=>Compra::getNombre_ot()])->one()) {
                  $monto_ejecutado = $cliente_ot->montoTotalEjecutado_ot;
                  $cliente_ot->montoTotalEjecutado_ot = $cliente_ot->montoTotalEjecutado_ot + $fact_obj->total_a_pagar;
                  $cliente_ot->save();
                }
              }
            }

            //ignorar % servicio o credito vencido
            public function actionIgnorar($tipo, $estado)
            {
              if ($tipo=='porc_se') {
                $estado = $estado != 'checked' ? null : $estado;
                Compra::setServicio_porc_ign($estado);
              }

            }

            //public function actionIgnorar($tipo, $estado)
            public function actionIgnorar_u($tipo, $estado, $id)
            {
              if ($tipo=='porc_se') {
                $estado = $estado != 'checked' ? null : $estado;
                $fact_obj = EncabezadoPrefactura::findOne($id);
                $fact_obj->ignorar_porc_ser = $estado;
                $fact_obj->save();
              }

            }

            public function actionCompleterminado(){
                if(($estadoF = Compra::getEstadoFactura()) && ($FechaCompraIni = Compra::getFechaInicial()) && ($vendedor = Compra::getVendedor()) && ($cliente = Compra::getCliente()) && ($pago = Compra::getPago()) && ($productos = Compra::getContenidoCompra()) )
                {
                  $bandera = 'arriba'; //Significa que dará paso a almacenar factura con estado Caja o Crédito Caja
                  $resumen = Compra::getTotal(true);
                  if ($client_ot = Clientes::find()->where(['idCliente'=>Compra::getNombre_ot()])->one()) {
                    $ot = Ot::find()->where(['idCliente'=>Compra::getNombre_ot()])->one();
                    $monto_acreditado = $ot->montoTotalEjecutado_ot;
                    $montodisponiblecredito = $client_ot->montoMaximoCredito - $monto_acreditado;
                    if ($resumen['total'] > $montodisponiblecredito) {
                      $bandera = 'abajo';
                      }
                  }
                  if ($bandera=='arriba') {
                    $observacion = Compra::getObserva();//obtiene la observacion
                    $nuevoestado = ($pago != 5) ? 'Caja' : 'Crédito Caja';
                    if ($nuevoestado == 'Crédito Caja') {
                        if($modelclient = Clientes::find()->where(['idCliente'=>$cliente])->one()){
                            if ((($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Abierta')) || (($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Cerrada') && ($modelclient->autorizacion == 'Si'))) {
                                $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                                $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                                if ($resumen['total'] > $montodisponiblecredito) {
                                    $bandera = 'abajo';
                                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Esta factura no puede ser acreditada porque excede el monto de crédito permitido del cliente.');
                                } else {
                                    $idCliente = $modelclient->idCliente;
                                    $monto_a_credito = $resumen['total'] + $modelclient->montoTotalEjecutado;
                                    $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
                                    $command = \Yii::$app->db->createCommand($sql);
                                    $command->bindParam(":montoTotalEjecutado", $monto_a_credito);
                                    $command->bindValue(":idCliente", $idCliente);
                                    $command->execute();
                                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Factura acreditada correctamente.');

                                //Yii::$app->response->redirect(array('cabeza-prefactura/index'));
                                }//fin else if $resumen['total'] > $montodisponiblecredito
                            } else {
                                $bandera = 'abajo';
                                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                                Yii::$app->response->redirect(array('cabeza-prefactura/create' ));
                            }
                            }else{
                                $bandera = 'abajo';
                                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                                Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                            } //fin else if $modelclient
                    } //Fin if nuevoestado

                    //si hasta aqui no hay bandera abajo se ejecuta el proceso de guardado de la prefactura con el nuevo estado
                    if ($bandera == 'arriba') {
                    $CabCompra = new EncabezadoPrefactura();
                    $CabCompra->fecha_inicio = date('Y-m-d');
                    $CabCompra->idCliente = $cliente;
                    $CabCompra->porc_descuento = $resumen['dcto'];
                    $CabCompra->iva = $resumen['iva'];
                    $CabCompra->total_a_pagar = $resumen['total'];
                    $CabCompra->estadoFactura = $nuevoestado;
                    $CabCompra->tipoFacturacion = $pago;
                    $CabCompra->idAgenteComision = Compra::getAgenteComision() ? Compra::getAgenteComision() : null;
                    $CabCompra->idAgenteComision = Compra_update::getAgenteComisionu() ? Compra_update::getAgenteComisionu() : null;
                    $CabCompra->codigoVendedor = $vendedor;
                    $CabCompra->subtotal = $resumen['subtotal'];
                    $CabCompra->observacion = Compra::getObserva() ? Compra::getObserva() : 'Ninguna';
                    $CabCompra->ignorar_porc_ser = Compra::getServicio_porc_ign() ? Compra::getServicio_porc_ign() : '';
                  //  if ($CabCompra->validate())
                    //{   #guardamos cabecera de factura
                        if ($CabCompra->save()) {
                        // guardamos detalle factura
                            foreach($productos as $position => $product)
                            {
                                $position = new DetalleFacturas();
                                $position->idCabeza_factura = $CabCompra->idCabeza_Factura;
                                $position->codProdServicio = $product['cod'];
                                $position->cantidad = $product['cantidad'];
                                $position->precio_unitario = $product['precio'];
                                $position->precio_por_cantidad = round(($product['precio']*$product['cantidad']),2);
                                $position->descuento_producto = round(/*$product['cantidad']**/(($product['precio']*$product['dcto'])/100),2);
                                $position->subtotal_iva = $product['iva'];
                                $position->fecha_hora = $product['fecha_hora'];
                                $position->save();
                            }
                            if (@$get_ot = Compra::getOT()) {
                              $this->ot_incluido($CabCompra->idCabeza_Factura, 'ca');
                            }
                            $this->historial_producto($CabCompra->idCabeza_Factura, 'RES', 'Prefactura');
                            $this->borrarTodo();
                            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La prefactura ha sido enviada a caja correctamente.');

                        }//Fin if $CabCompra
                  //  }
                    else {
                        $error='';
                        foreach ($CabCompra->getErrors() as $position => $er)
                        $error.=$er[0].'<br/>';
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>'.$CabCompra->estadoFactura.'Disculpa, no se efectuó el proceso de prefacturación.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    } //fin else if $CabCompra
                    Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    }//fin if bandera
                    else {

                        Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    }//fin else if bandera

                  }  else {//esta bandera es para devolver error si el responsable O.T exede el monto credito y ot
                      Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Esta factura no puede ser acreditada porque el responsable de O.T excede el monto de crédito más O.T permitido.');
                      Yii::$app->response->redirect(array('cabeza-prefactura/create' ));
                    }

                }//Fin if condiciones de session
                else {
                 Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Asegúrese de que los datos estén completos.');
                 Yii::$app->response->redirect(array('cabeza-prefactura/create'));
               }
                //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Prefactura almacenada correctamente.');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));

            }//Fin de la funcion

            //enviar a caja sin refrescar pagina
            public function actionCompletando_facturacion()
            {
              if(($estadoF = Compra::getEstadoFactura()) && ($FechaCompraIni = Compra::getFechaInicial()) && ($vendedor = Compra::getVendedor()) && ($cliente = Compra::getCliente()) && ($pago = Compra::getPago()) && ($productos = Compra::getContenidoCompra()) )
              {
                $bandera = 'arriba'; //Significa que dará paso a almacenar factura con estado Caja o Crédito Caja
                $resumen = Compra::getTotal(true);
                if ($client_ot = Clientes::find()->where(['idCliente'=>Compra::getNombre_ot()])->one()) {
                  $ot = Ot::find()->where(['idCliente'=>Compra::getNombre_ot()])->one();
                  $monto_acreditado = $ot->montoTotalEjecutado_ot;
                  $montodisponiblecredito = $client_ot->montoMaximoCredito - $monto_acreditado;
                  if ($resumen['total'] > $montodisponiblecredito) {
                    $bandera = 'abajo';
                    }
                }
                if ($bandera=='arriba') {
                  $observacion = Compra::getObserva();//obtiene la observacion
                  $nuevoestado = ($pago != 5) ? 'Caja' : 'Crédito Caja';
                  if ($nuevoestado == 'Crédito Caja') {
                      if($modelclient = Clientes::find()->where(['idCliente'=>$cliente])->one()){
                          if ((($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Abierta')) || (($modelclient->credito == 'Si') && ($modelclient->estadoCuenta == 'Cerrada') && ($modelclient->autorizacion == 'Si'))) {
                              $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                              $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                              if ($resumen['total'] > $montodisponiblecredito) {
                                  $bandera = 'abajo';
                                  Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Esta factura no puede ser acreditada porque excede el monto de crédito permitido del cliente.');
                              } else {
                                  $idCliente = $modelclient->idCliente;
                                  $monto_a_credito = $resumen['total'] + $modelclient->montoTotalEjecutado;
                                  $sql = "UPDATE tbl_clientes SET montoTotalEjecutado = :montoTotalEjecutado WHERE idCliente = :idCliente";
                                  $command = \Yii::$app->db->createCommand($sql);
                                  $command->bindParam(":montoTotalEjecutado", $monto_a_credito);
                                  $command->bindValue(":idCliente", $idCliente);
                                  $command->execute();

                              //Yii::$app->response->redirect(array('cabeza-prefactura/index'));
                              }//fin else if $resumen['total'] > $montodisponiblecredito
                          } else {
                              $bandera = 'abajo';
                              Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                              Yii::$app->response->redirect(array('cabeza-prefactura/create' ));
                          }
                          }else{
                              $bandera = 'abajo';
                              Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Este cliente  NO TIENE CRÉDITO AUTORIZADO.');
                              Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                          } //fin else if $modelclient
                  } //Fin if nuevoestado

                  //si hasta aqui no hay bandera abajo se ejecuta el proceso de guardado de la prefactura con el nuevo estado
                  if ($bandera == 'arriba') {
                  $CabCompra = new EncabezadoPrefactura();
                  $CabCompra->fecha_inicio = date('Y-m-d');
                  $CabCompra->idCliente = $cliente;
                  $CabCompra->porc_descuento = $resumen['dcto'];
                  $CabCompra->iva = $resumen['iva'];
                  $CabCompra->total_a_pagar = $resumen['total'];
                  $CabCompra->estadoFactura = $nuevoestado;
                  $CabCompra->tipoFacturacion = $pago;
                  $CabCompra->idAgenteComision = Compra::getAgenteComision() ? Compra::getAgenteComision() : null;
                  $CabCompra->idAgenteComision = Compra_update::getAgenteComisionu() ? Compra_update::getAgenteComisionu() : null;
                  $CabCompra->codigoVendedor = $vendedor;
                  $CabCompra->subtotal = $resumen['subtotal'];
                  $CabCompra->observacion = Compra::getObserva() ? Compra::getObserva() : 'Ninguna';
                  $CabCompra->ignorar_porc_ser = Compra::getServicio_porc_ign() ? Compra::getServicio_porc_ign() : '';
                //  if ($CabCompra->validate())
                  //{   #guardamos cabecera de factura
                      if ($CabCompra->save()) {
                      // guardamos detalle factura
                          foreach($productos as $position => $product)
                          {
                              $position = new DetalleFacturas();
                              $position->idCabeza_factura = $CabCompra->idCabeza_Factura;
                              $position->codProdServicio = $product['cod'];
                              $position->cantidad = $product['cantidad'];
                              $position->precio_unitario = $product['precio'];
                              $position->precio_por_cantidad = round(($product['precio']*$product['cantidad']),2);
                              $position->descuento_producto = round(/*$product['cantidad']**/(($product['precio']*$product['dcto'])/100),2);
                              $position->subtotal_iva = $product['iva'];
                              $position->fecha_hora = $product['fecha_hora'];
                              $position->save();
                          }
                          if (@$get_ot = Compra::getOT()) {
                            $this->ot_incluido($CabCompra->idCabeza_Factura, 'ca');
                          }
                          $this->historial_producto($CabCompra->idCabeza_Factura, 'RES', 'Prefactura');
                          $this->borrarTodo();
                          //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Facturación efectuada correctamente.');
                          return $CabCompra->idCabeza_Factura;
                      }//Fin if $CabCompra
                //  }
                  else {
                      $error='';
                      foreach ($CabCompra->getErrors() as $position => $er)
                      $error.=$er[0].'<br/>';
                      Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>'.$CabCompra->estadoFactura.'Disculpa, no se efectuó el proceso de prefacturación.');
                      Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                  } //fin else if $CabCompra
                  Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                  }//fin if bandera
                  else {

                      Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                  }//fin else if bandera

                }  else {//esta bandera es para devolver error si el responsable O.T exede el monto credito y ot
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> Esta factura no puede ser acreditada porque el responsable de O.T excede el monto de crédito más O.T permitido.');
                    Yii::$app->response->redirect(array('cabeza-prefactura/create' ));
                  }

              }//Fin if condiciones de session
              else {
               Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Asegúrese de que los datos estén completos.');
               Yii::$app->response->redirect(array('cabeza-prefactura/create'));
             }
            }//fin de funcion

            public function actionCompleteU(){
                if(($estadoF = Compra_update::getEstadoFacturau()) && ($FechaCompraIni = Compra_update::getFechaInicialu()) && ($vendedor = Compra_update::getVendedoru()) && ($cliente = Compra_update::getClienteu()) && ($pago = Compra_update::getPagou()) && ($productos = Compra_update::getContenidoComprau()))
                {
                    $CabCompra = new EncabezadoPrefactura();
                    $resumen = Compra_update::getTotalu(true);
                    $CabCompra->fecha_inicio = date('Y-m-d');
                    $CabCompra->idCliente = $cliente;
                    $CabCompra->porc_descuento = $resumen['dcto'];
                    $CabCompra->iva = $resumen['iva'];
                    $CabCompra->total_a_pagar = $resumen['total'];
                    $CabCompra->estadoFactura = $estadoF;
                    $CabCompra->tipoFacturacion = $pago;
                    $CabCompra->codigoVendedor = $vendedor;
                    $CabCompra->subtotal = $resumen['subtotal'];

                  //  if ($CabCompra->validate())
                    //{   #guardamos cabecera de factura
                        if ($CabCompra->save()) {
                        // guardamos detalle factura
                            foreach($productos as $position => $product)
                            {
                                $position = new DetalleFacturas();
                                $position->idCabeza_factura = $CabCompra->idCabeza_Factura;
                                $position->codProdServicio = $product['cod'];
                                $position->cantidad = $product['cantidad'];
                                $position->precio_unitario = $product['precio'];
                                $position->precio_por_cantidad = round(($product['precio']*$product['cantidad']),2);
                                $position->descuento_producto = round($product['cantidad']*(($product['precio']*$product['dcto'])/100),2);
                                $position->subtotal_iva = $product['iva'];
                                $position->save();
                            }
                            $this->borrarTodoU();
                            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Prefactura almacenada correctamente.');
                            Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                        }
                  //  }
                    else {
                        $error='';
                        foreach ($CabCompra->getErrors() as $position => $er)
                        $error.=$er[0].'<br/>';
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>'.$CabCompra->estadoFactura.'Disculpa, no se efectuó el proceso de prefacturación.');
                        Yii::$app->response->redirect(array('cabeza-prefactura/create'));
                    }
                }
                //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Prefactura almacenada correctamente.');
                //Yii::$app->response->redirect(array('cabeza-prefactura/create'));
            }


            //este conjunto de funciones obtiene los datos del banco para alimentar la opcion
            //de despliegue de las cuentas correspondientes a ese banco
            public function actionCuentas() {
                 $out = [];
                 if (isset($_POST['depdrop_parents'])) {
                     $parents = $_POST['depdrop_parents'];
                     if ($parents != null) {
                         $cuenta = $parents[0];
                         $out = $this->getCuentas($cuenta);
                         echo Json::encode(['output'=>$out, 'selected'=>'']);
                         return;
                     }
                 }
                 echo Json::encode(['output'=>'', 'selected'=>'']);
             }
             protected function getCuentas($idEntidad_financiera){
                 //$datas = EntidadesFinancieras::findAll(array('idEntidad_financiera'=>$idEntidad_financiera));
                 //$idCentb = \yii\helpers\ArrayHelper::map($datas,'idBancos','idBancos');
                 $datas2 = CuentasBancarias::find()->where(['idEntidad_financiera'=>$idEntidad_financiera])->all();
                 return $this->MapData($datas2,'numero_cuenta','numero_cuenta','descripcion');
                 /*return ArrayHelper::map($datas2, 'numero_cuenta', function($element) {
                     return $element['numero_cuenta'].' - '.$element['descripcion'];
                 });*/
             }
             protected function MapData($datas,$fieldId,$fieldName, $descripcion){
                 $obj = [];
                 foreach ($datas as $key => $value) {
                     array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}.' - '.$value->{$descripcion}]);
                 }
                 return $obj;
             }
             //fin conjunto de funciones


        //me busca los productos en inventario
        public function actionBusqueda_producto(){
         if(Yii::$app->request->isAjax){
                    $consultaBusqueda_cod = Yii::$app->request->post('valorBusqueda_cod');
                    $consultaBusqueda_des = Yii::$app->request->post('valorBusqueda_des');
                    $consultaBusqueda_cat = Yii::$app->request->post('valorBusqueda_cat');
                    $consultaBusqueda_cpr = Yii::$app->request->post('valorBusqueda_cpr');
                    $consultaBusqueda_ubi = Yii::$app->request->post('valorBusqueda_ubi');
                    $consultaBusqueda_fam = Yii::$app->request->post('valorBusqueda_fam');

                    //Filtro anti-XSS
                    $caracteres_malos = array("<", ">", "\"", "'", "<", ">", "'");
                    $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
                    $consultaBusqueda_cod = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cod);
                    $consultaBusqueda_des = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_des);
                    $consultaBusqueda_cat = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cat);
                    $consultaBusqueda_cpr = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cpr);
                    //$consultaBusqueda_ubi = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_ubi);

                    $mensaje = "";

                    //Comprueba si $consultaBusqueda está seteado
                    if (isset($consultaBusqueda_cod) && isset($consultaBusqueda_des) && isset($consultaBusqueda_fam) && isset($consultaBusqueda_cat) && isset($consultaBusqueda_cpr) && isset($consultaBusqueda_ubi)) {
                        $consultaBusquedalimpia = "%".$consultaBusqueda_cod."%".$consultaBusqueda_des."%".$consultaBusqueda_ubi."%";
                        /*$consultaBusquedalimpia_des =
                        $consultaBusquedalimpia_ubi = */
                        $tipo = 'Producto';
                        $consultaBusqueda_cod = '%'.$consultaBusqueda_cod.'%';
                        $consultaBusqueda_des = '%'.$consultaBusqueda_des.'%';
                        //$consultaBusqueda_fam = '%'.$consultaBusqueda_fam.'%';
                        $consultaBusqueda_cat = '%'.$consultaBusqueda_cat.'%';
                        $consultaBusqueda_cpr = '%'.$consultaBusqueda_cpr.'%';
                        $consultaBusqueda_ubi = '%'.$consultaBusqueda_ubi.'%';
                        $sql = "SELECT tps.codProdServicio, tps.codFamilia, tps.nombreProductoServicio, tps.cantidadInventario, tps.ubicacion, tps.precioVentaImpuesto
                        FROM tbl_producto_servicios tps INNER JOIN tbl_familia  tf
                        ON tps.codFamilia = tf.codFamilia
                        WHERE tps.codProdServicio IN "."(";
                        $sql .= " SELECT pr.`codProdServicio`
                          FROM `tbl_producto_catalogo` pr_c RIGHT JOIN tbl_producto_servicios pr
                          ON pr_c.`codProdServicio` = pr.`codProdServicio`
                          LEFT JOIN `tbl_producto_proveedores` pr_p
                          ON pr.`codProdServicio` = pr_p.codProdServicio
                          WHERE pr.tipo = 'Producto'";
                          if($consultaBusqueda_cod != '%%'){
                            $sql .= " AND pr.codProdServicio LIKE :codProdServicio ";
                          }
                          if($consultaBusqueda_des != '%%'){
                            $sql .= " AND pr.nombreProductoServicio LIKE :nombreProductoServicio ";
                          }
                          if($consultaBusqueda_ubi != '%%'){
                            $sql .= " AND pr.ubicacion LIKE :ubicacion ";
                          }
                          if($consultaBusqueda_cat != '%%'){
                            $sql .= " AND pr_c.`codigo` LIKE :codigo ";
                          }/*else{
                            $sql .= " AND pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo ";
                          }*/
                            //$sql .= " AND IF ( :codigo <> '%%', pr_c.`codigo` LIKE :codigo, pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo) ";
                          if($consultaBusqueda_cpr != '%%'){
                            $sql .= " AND pr_p.`codigo_proveedor` LIKE  :codigo_prov ";
                          }
                          $sql .= ")";
                          if($consultaBusqueda_fam == ''){
                            $sql .= " AND tps.codFamilia LIKE '%%' ";
                          }else{
                            $sql .= " AND tps.codFamilia = :codFamilia ";
                          }
                              $sql .= " LIMIT 50";
                    $command = \Yii::$app->db->createCommand($sql);
                    if($consultaBusqueda_cod != '%%'){ $command->bindParam(":codProdServicio", $consultaBusqueda_cod); }
                    if($consultaBusqueda_des != '%%'){ $command->bindParam(":nombreProductoServicio", $consultaBusqueda_des); }
                       //$command->bindParam(":codFamilia_", $consultaBusqueda_fam);
                    if($consultaBusqueda_fam != '')  { $command->bindParam(":codFamilia", $consultaBusqueda_fam); }
                    if($consultaBusqueda_cpr != '%%'){ $command->bindParam(":codigo_prov", $consultaBusqueda_cpr); }
                    if($consultaBusqueda_cat != '%%'){$command->bindParam(":codigo", $consultaBusqueda_cat); }
                    if($consultaBusqueda_ubi != '%%'){ $command->bindParam(":ubicacion", $consultaBusqueda_ubi); }
                    $consulta = $command->queryAll();
                        //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                        //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                            //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                            //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                            //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle
                            /*$consulta = ProductoServicios::find()->where(['=','tipo', $tipo])
                            ->andFilterWhere(['like', 'codProdServicio', $consultaBusqueda_cod ])
                            ->andFilterWhere(['like', 'nombreProductoServicio', $consultaBusqueda_des ])
                            //->andFilterWhere(['like', 'codigo_proveedor', $_ ])//proveedor
                            //->andFilterWhere(['like', 'codigo', $_ ])//catalogo
                            ->andFilterWhere(['like', 'ubicacion', $consultaBusqueda_ubi ])
                            ->orderBy(['nombreProductoServicio' => SORT_DESC])->limit(50)->all();*/
                            $p = 0;
                            foreach($consulta as $resultados){
                                $p += 1;
                                $codProdServicio = $resultados['codProdServicio'];
                                $nombreProductoServicio = $resultados['nombreProductoServicio'];
                                $ubicacion = $resultados['ubicacion'];
                                $cantidadInventario = $resultados['cantidadInventario'];
                                $presiopro = '₡ '.number_format($resultados['precioVentaImpuesto'],2);
                                //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                                $accion = Html::a('', '', [
                                    'class' => 'glyphicon glyphicon-plus',
                                    'data-datac' => $codProdServicio,
                                    'onclick'=>'javascript:agrega_producto($(this).data("datac"))',
                                    'data-dismiss'=>'modal',
                                    'title' => Yii::t('app', 'Cargar producto'),
                                    /*'data' => [
                                        'confirm' => '¿Seguro que quieres agregar?',
                                        'method' => 'post',
                                    ],*/
                                ]);
                                //Output
                          $catalogo = '';
                          $codigo_proveedor = '';
                         $marcas_asociadas = ProductoCatalogo::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                         foreach ($marcas_asociadas as $marcas) { $catalogo .= $marcas->codigo.' * ';}
                         $codigos_proveedores = ProductoProveedores::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                         foreach ($codigos_proveedores as $cod_prov) {//recorro lo productos con proveedores para ver su estado
                           if ($cod_prov->estado == 'Pedido') {//se eligen los productos pedidos
                             $proveedor = Proveedores::findOne($cod_prov->codProveedores);//se les obtiene el proveedor y la orden de compra
                             $orden_compra = OrdenCompraProv::find()->where(['estado'=>'Generada'])->andWhere('idProveedor = :idProveedor',[':idProveedor'=>$cod_prov->codProveedores])->one();
                             if (@$orden_compra) {//si presenta un pedido informa en la consulta que el producto esta pedido por si este no se encuentra en el inventario
                               $f_pedido = date('d-m-Y', strtotime( $orden_compra->fecha_registro ));
                               $codigo_proveedor .= '<a href="#" title="Este producto ya fue pedido al proveedor '.$proveedor->nombreEmpresa.' el día '.$f_pedido.'.">'.$cod_prov->codigo_proveedor.' * </a>';
                             } else {//si el producto se encuentra e estado pedido pero no esta asociado a ninguna compra se informa que este producto debe ser reportado
                               $codigo_proveedor .= '<a href="#" style="color:#ffa800;" title="Reporte este código, porque se encuentra en un estado PEDIDO pero no esta asociado a ninguna orden de compra.">'.$cod_prov->codigo_proveedor.' * </a>';;
                             }

                           } else {
                             $codigo_proveedor .= $cod_prov->codigo_proveedor.' * ';
                           }
                         }
                        $factura_temp = DetalleFacturasTemp::find()->where(['cod'=>$codProdServicio])->all();
                        $usuarios_reserva = '';
     										foreach ($factura_temp as $value_temp) {
     											$usuarios_reserva .= '<span class="label label-primary" style="font-size: small;">('.$value_temp->cantidad.')-'.$value_temp->usuario.'</span><br>';
     										}
                         $codpro = "'".$codProdServicio."'";
                         $mensaje .='
                         <tbody tabindex="0" onkeypress="$(document).keypress(function(event){ if(event.which == 13) agrega_producto('.$codpro.') })" >
                                    <tr style="cursor:pointer" id="'.$codpro.'" onclick="javascript:agrega_producto('.$codpro.'); this.onclick = null;">
                                      <td id="start" width="150">'.$codProdServicio.'</td>
                                      <td width="450">'.$nombreProductoServicio.'</td>
                                      <td width="100" align="center">'.$cantidadInventario.'</td>
                                      <td width="150" align="center">'.$catalogo.'</td>
                                      <td width="150" align="center">'.$codigo_proveedor.'</td>
                                      <td width="50" align="center">'.$ubicacion.'</td>
                                      <td width="140" align="right">'.$presiopro.'</td>
                                      <td width="60" align="right">'.$usuarios_reserva.'</td>
                                      </tr>
                                    </tbody>';



                            }



                    //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
                }
                echo '<table class="row_selected" >'.$mensaje.'</table>';
            }
        }//fin de funcion de busqueda de productos

        //PARA EL CASO DE FACTURAS O.T SE PUEDEN IMPRIMIR

        public function actionReport($id) {
            $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $content = $this->renderAjax('factura_ot_print',['model' => $this->findModel($id)]);
           /* $content = $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);*/

           $imprimir = '
                <!DOCTYPE html>
                <html>
                <head>
                    <meta charset="UTF-8">
                    <title>FACTURA O.T</title>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                    <script type="text/javascript">
                        function imprimir() {
                            if (window.print) {
                                window.print();
                                setTimeout("self.close()", 50 );
                            } else {
                                alert("La función de impresion no esta soportada por su navegador.");
                            }
                        }
                    </script>
                    <style type="text/css">
                        footer{
                          display: none;

                        }
                    </style>
                </head>
                <body onload="imprimir();">
                '.$content.'
                </body>
                </html>';
            echo $imprimir;
        }

    //con esta funcion agrego movimientos a las facturas de Apartados
    public function actionAgregar_mov_apartado()
    {
      if(Yii::$app->request->isAjax){
          $pago  = floatval(str_replace(",", "", Yii::$app->request->post('pago')));
          $referencia  = Yii::$app->request->post('referencia');
          $tipo_pago_abono = Yii::$app->request->post('tipo_pago_abono');
          $idCabeza_Factura = Yii::$app->request->post('idCa');
          $movimiento = 'no';

              $modelfactura = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one(); //Obtengo la factura
              $saldo_de_la_factura = $modelfactura->total_a_pagar; //extraigo el total a pagar de la factura
                  $model_m_apartado = new MovimientoApartado(); //obtengo la instancia MovimientoCobrar por donde voy a almacenar las notas de credito

                  //Obtengo la suma del monto_movimiento que es el monto que se ha aplicado historicamente
                  $command = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_apartado WHERE idCabeza_Factura = " . $idCabeza_Factura);
                  $suma_monto_movimiento = $command->queryScalar(); //ingreso la suma del monto en una variable

                  //Asigno la nota de credito en los campos que corresponden a la tabla de la base de datos

                  $monto_anterior = $saldo_de_la_factura - $suma_monto_movimiento; //Resto la suma del monto con el saldo de la factura para saber el monto anterior
                  $saldo_pendiente = $monto_anterior - $pago; //le resto el monto de la nota de credito con el monto anterior para saber el monto pendiente
                  //al comparar valido que las notas no superen el monto de la factura
                  //lo interesante es que necesito hacer una comparacion muy exacta cuando se trata de montos iguales, por eso uso number_format()
                  //como se ve a continuacion ya que en algun momento dado despues de varios procesos por abono o NC solo por montos iguales puede pasar
                  //por el contrario el monto siempre será mayor al saldo de la factura
                  $tran = Yii::$app->db->beginTransaction();
                  $mensajes_error = '';
                  try {
                  if ((number_format($pago,2) == number_format($monto_anterior,2)) || ($pago < $monto_anterior) && $pago > 0) {
                    $bandera_cancelada = 'false';
                    $consecutivo_fe = $consecutivo_te = 0;
                    //si el monto del pago es igual al del monto del saldo (monto anterios) se cancela la factura
                    if(number_format($pago,2) == number_format($monto_anterior,2)){
                      $bandera_cancelada = 'true';
                      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
                      if ( $empresa->factun_conect == 1 ) {//consirmo si es factra electronica
                        //$facturacion = $this->factura_electronica($idCabeza_Factura, '01');
                        /*if ($facturacion['exito'] == true) {//confirmo si se ejecutó la factura electronica
                          $clave = $facturacion['clave'];
                          $estado_hacienda = $facturacion['estado_hacienda'];
                          $numero_factura = $facturacion['numero_factura'];
                          $consecutivo = $facturacion['consecutivo'];
                        } else {
                          $bandera_cancelada = 'offline';
                          $mensajes_error = $facturacion['mensajes_error'];
                        }*/
                        $tipo_documento = '01';
                        $sql = "SELECT LPAD (consecutivo_fe, 10, '0') AS fe,  LPAD (consecutivo_te, 10, '0') AS te, consecutivo_fe, consecutivo_te, ced_juridica, sucursal FROM tbl_empresa WHERE idEmpresa = 1";
                        $command = \Yii::$app->db->createCommand($sql);
                        $this_empresa = $command->queryOne();
                        $terminal = '00001';
                        $sucursal = $this_empresa['sucursal'];
                        $tipoDocumento = $tipo_documento == '01' ? 'FE' : 'TE';
                        $tipoCedula = Yii::$app->params['tipo_empresa'];//fisico, juridico, dimex o nite
                        $cedula = $this_empresa['ced_juridica'];
                        $situacion = 'normal';
                        $codigoPais = '506';
                        $consecut = $tipo_documento == '01' ? $this_empresa['fe'] : $this_empresa['te'];
                        $consecutivo_fe = $this_empresa['consecutivo_fe'];
                        $consecutivo_te = $this_empresa['consecutivo_te'];
                        if ($tipo_documento == '01') {
                          $consecutivo_fe += 1;
                        } else {
                          $consecutivo_te += 1;
                        }
                        $codigoSeguridad = '2605'.substr($consecut, 6);
                        $clave_electronica = new Clave_electronica();
                        $datos_electronicos = $clave_electronica->getClave($terminal, $sucursal, $tipoDocumento, $tipoCedula, $cedula, $situacion, $codigoPais, $consecut, $codigoSeguridad);
                        $clave = $numero_factura = '';
                        foreach ($datos_electronicos as $key => $value) {
                          if ($key == 'clave') { $clave = $value; }
                          if ($key == 'consecutivo') { $numero_factura = $value; }
                        }
                        $estado_hacienda = '10';
                        $consecutivo = 0;
                      } else {
                        $clave = '-';
                        $estado_hacienda = '00';
                        $numero_factura = '0';
                        $consecutivo = -1;
                      }
                    }//fin comprovación si el monto de pago es igual al saldo que queda por pagar.

                    $idImprimir = 0;
                    if ($bandera_cancelada != 'offline') {//si no es offline puede pasar
                    $model_m_apartado->fecmov = date("Y-m-d H:i:s",time());
                    $model_m_apartado->concepto = $referencia;
                    $model_m_apartado->monto_anterior = $monto_anterior;
                    $model_m_apartado->monto_movimiento = $pago;
                    $model_m_apartado->saldo_pendiente = $saldo_pendiente;
                    $model_m_apartado->usuario = Yii::$app->user->identity->username;
                    $model_m_apartado->idCabeza_Factura = $idCabeza_Factura;
                    $model_m_apartado->idCliente = $modelfactura->idCliente;
                    if($model_m_apartado->save()) //Guardo el movimienrto apartados
                    {
                        $modelcliente = Clientes::find()->where(['idCliente'=>$modelfactura->idCliente])->one();
                        $nuevacantidad = $modelcliente->montoTotalEjecutado - $pago;
                        //$this->actionImprimir_movimiento_credito($idCabeza_Factura);
                        if ($bandera_cancelada == 'true') {
                          $this->cancelar_factura($idCabeza_Factura, $estado_hacienda, $clave, $numero_factura, $consecutivo, $consecutivo_fe, $consecutivo_te);//procedo a cancelar
                        }
                    }

                    $fechadepo = date('Y-m-d');
                    if ($tipo_pago_abono == 4) {//Si el pago es por deposito bancario se ingresa aqui para dirigir a la funcion que creara un movimento en libro
                      $idEntidad_financiera = Yii::$app->request->post('ddl_entidad');
                      $numero_cuenta = Yii::$app->request->post('tipoCuentaBancaria');
                      $fechadepo = Yii::$app->request->post('fechadepo');
                      $comprobacion = Yii::$app->request->post('comprobacion');
                      $modelcuentabancaria = CuentasBancarias::find()->where(['idEntidad_financiera'=>$idEntidad_financiera])->andWhere("numero_cuenta = :numero_cuenta", [":numero_cuenta"=>$numero_cuenta])->one();

                      $this->crearmovimientolibro_apartado($modelcuentabancaria->idBancos, $comprobacion, $idCabeza_Factura, $pago, $fechadepo);
                      $movimiento = 'si';
                    }
                    $tipo_del_medio = Yii::$app->request->post('ddl_entidad');
                    $comprobacion = Yii::$app->request->post('comprobacion');
                    $cuenta = $tipo_pago_abono==2 ? '' : Yii::$app->request->post('tipoCuentaBancaria');
                    $estadopago = $bandera_cancelada == 'true' ? 'Cancelación' :'Abono Apart';

                    $tbl_medio_pago = new MedioPago();
                    $tbl_medio_pago->idCabeza_factura = $idCabeza_Factura;
                    $tbl_medio_pago->idForma_Pago = $tipo_pago_abono;
                    $tbl_medio_pago->tipo_del_medio = $tipo_pago_abono != 1 ? $tipo_del_medio : '';
                    $tbl_medio_pago->comprobacion = $tipo_pago_abono != 1 ? $comprobacion : '';
                    $tbl_medio_pago->cuenta_deposito = $tipo_pago_abono != 1 ? $cuenta : '';
                    $tbl_medio_pago->fecha_deposito = date('Y-m-d');
                    $tbl_medio_pago->estado = $estadopago;
                    $tbl_medio_pago->monto = $pago;
                    if ($tbl_medio_pago->save()) {//actualizamos el movimiento por cobrar (abono) agregandole el medio de pago
                      $m_cobrar = $model_m_apartado->find()->where(['idMov'=>$model_m_apartado->idMov])->one();
                      $m_cobrar->idMetPago = $tbl_medio_pago->idMetPago;
                      $m_cobrar->save();
                    }
                  }
                  $not_adicional = $movimiento=='no' ? '' : '(Nuevo movimiento en libro)';
                  if ($bandera_cancelada == 'true') {
                      echo '<span style="font-size:10pt;" class="label label-success"><strong>Éxito!</strong> Esta Factura ha sido cancelada. '.$not_adicional.'</span>' ;
                  } else if ($bandera_cancelada == 'offline') {
                    echo '<span style="font-size:10pt;" class="label label-danger"><strong>Error!</strong> No se pudo cancelar, Factura Electrónica offline.</span>
                    <br><br><div class="alert alert-danger" role="alert"><center><strong>Atienda lo siguiente antes de proceder a facturar:</strong></center>'.$mensajes_error.'</div>';
                  } else {
                    echo '<span style="font-size:10pt;" class="label label-success"> <strong>Éxito!</strong> Abono aplicado correctamente al apartado. '.$not_adicional.'</span>';
                  }
              } else {
                  echo '<span style="font-size:10pt;" class="label label-danger"><strong>Error!</strong> El monto es negativo o supera el saldo pendiente de pagar.</span> ';
              }
              $tran->commit();
              } catch (\Exception $e) {
                /*si ocurre un error en toda la transaccion hace un rollBack y muestra el error*/
                $tran->rollBack();
                echo '<span style="font-size:10pt;" class="label label-danger"><strong>Error!</strong> Se encuentran datos incongruentes. '.$e.'</span> ';
              }
      }
    }

    //imprimir movimientos de apartados
    public function actionImprimir_abono_apartado($id)
    {
      return $this->render('imprimir_abono_apartado', [
          'id' => $id
      ]);
    }

    public function cancelar_factura($id, $estado_hacienda, $clave, $numero_factura, $id_factura_factun, $consecutivo_fe, $consecutivo_te){
        $estado = 'Cancelada';
        $fecha_final = date('Y-m-d');
        $mensaje_vuelto = '';
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        //if ( $id_factura_factun == 0) {
        //  return false;
          /*Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong> No hay conexión don el API para generar la factura electrónica');
          Yii::$app->response->redirect(['cabeza-caja/index','id'=>$id]);*/
        //} else {
          $factura_emitida = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$id])->one();
          //almacenamos los datos a la base de datos
          $factura_emitida->fe = $numero_factura;
          $factura_emitida->clave_fe = $clave;
          //$factura_emitida->xml_fe = $result_xml;
          //$factura_emitida->xml_resp = $result_xml_resp;
          $factura_emitida->estadoFactura = $estado;
          $factura_emitida->fecha_final = $fecha_final;
          $factura_emitida->fecha_emision = $fecha_final;
          $factura_emitida->factun_id = $id_factura_factun;
          $factura_emitida->estado_hacienda = '10';
          $factura_emitida->estado_detalle = '-';
        if ($factura_emitida->save()) {
          $empresa->consecutivo_fe = $consecutivo_fe;
          $empresa->consecutivo_te = $consecutivo_te;
          $empresa->save();

            $this->historial_producto($id, 'SUM', 'Prefactura');
            $this->historial_producto($id, 'RES', 'Venta');
            $this->borrarTodoU();

        }//fin actualiza encabezado
        return true;
        //}
    }

    //crea factura electronica para apartados
    public function factura_electronica($id, $idForma_Pago_ultimo)
    {
      $consultar_factun = new Factun();
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $factura_emitida = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$id])->one();
      $fecha_documento = date('Y-m-d').'T'.date('H:i:s',time());//strval($factura_emitida->fecha_final);
      $numero_original = strval($factura_emitida->idCabeza_Factura);
      $plazo = $diasCredito = '';//para los clientes con credito
      $identificacion = $tipo_identificacion = $nombre_cliente = $email = ''; //datos de clientes
      $provincia = $canton = $distrito = $barrio = $otras_senas = $telefono = '0';
      $tipo_documento = @Clientes::findOne($factura_emitida->idCliente) ? '01' : '04';
      if ($tipo_documento == '01') {//Si es factura electronica
        $cliente = Clientes::findOne($factura_emitida->idCliente);
        $identificacion = strval($cliente->identificacion);
        if ($cliente->tipoIdentidad == 'Física') {
          $tipo_identificacion = '01';
        } else if($cliente->tipoIdentidad == 'Jurídica'){
          $tipo_identificacion = '02';
        } else if($cliente->tipoIdentidad == 'DIMEX'){
          $tipo_identificacion = '03';
        } else {
          $tipo_identificacion = '04';
        }
        $nombre_cliente = $cliente->nombreCompleto;
        $email = $cliente->email;
        $diasCredito = strval($cliente->diasCredito);
        $provincia = $cliente->provincia;
        $canton = $cliente->canton;
        $distrito = $cliente->distrito;
        $barrio = $cliente->barrio;
        $otras_senas = $cliente->direccion;
        $telefono = $cliente->celular;
        $identificacion_cliente = [
          'tipo' => $tipo_identificacion,
          'numero' => $identificacion
        ];
        $cliente_iv_exonerado = $cliente->iv == 1 ? true : false;
      } else { // de lo contrario si es tiquete electronico
        $nombre_cliente = $factura_emitida->idCliente;
        $email = NULL;
        $identificacion_cliente = [];
        $cliente_iv_exonerado = false;
      }
      $moneda_local = Moneda::find()->where(['monedalocal'=>'x'])->one();
      $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
      $condicion_venta = '01'; //01 = Contado, 02 = Crédito, 03 = Consignación, 04 = Apartado, 05 = Arrendamiento con opción de compra, 06 = Arrendamiento en función financiera, 99 = Otros
      $tipo_pago = [$idForma_Pago_ultimo];
      //obtengo la forma de pago en el sistema para enviar el tipo_pago
      if ($factura_emitida->tipoFacturacion == 5) {//Credito
        $condicion_venta = '02';
        $plazo = $diasCredito.' dias.';
        //$tipo_pago = ['01'];
      } else {
      //  $medio_pago = Compra_view::getContenidoMedioPago();
      $medio_pago = MedioPago::find()->where(['idCabeza_factura'=>$id])->all();
        foreach ($medio_pago as $key => $value) {
          //$tipo_pago[] = '0'.strval($value['idForma_Pago']);
        }
      }

      $resultado = 0;
      //RECORRER DETALLE
      $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$factura_emitida->idCabeza_Factura])->all();
      $array_detalle[] = null; $codigo = 'ORD_SERV'; $descripcion = 'ORD_SERV'; $unidad = 'Sp'; $servicio = true;//datos del detalle
      //$moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
      $tarifa_impuesto = 6.5; $tipo_impuesto = '07';
      $impuesto[] = [];
      if ($cliente_iv_exonerado == true) {//entra aqui se es un cliente exonerado
          $tipo_exoneracion = '03'; //01 = Compras Autorizadas, 02 = Ventas exentas a diplomáticos, 03 = Orden de compra(instituciones públicas y otros organismos), 04 = Exenciones Dirección General de Hacienda, 05 = Zonas Francas, 99 = Otros
          $numero_documento_exonerado = 1;
          $nombre_instituto_exonerado = $nombre_cliente;
          $fecha_documento_exonerado = $fecha_documento;
          $porcentaje_exonerado = 100;
          $impuesto[] = [
                         'codigo' => $tipo_impuesto,
                         'tarifa' => $tarifa_impuesto,
                         'exoneracion' => [
                                           'tipo_documento' => $tipo_exoneracion,
                                           'numero_documento' => $numero_documento_exonerado,
                                           'nombre_institucion' => $nombre_instituto_exonerado,
                                           'fecha_emision' => $fecha_documento_exonerado,
                                           'porcentaje_compra' => $porcentaje_exonerado
                                          ]
                        ];
       }
      foreach ($detalle_venta as $key => $value) {
        $c = $key+1;//contador
        //hacemos esta condicion a pesar de que casi siempre son productos los que ingresan, pero como existen
        //las ordenes de servicio y al no ser un producto como tal se clasifican a parte
        if ($producto = ProductoServicios::find()->where(['codProdServicio'=>$value->codProdServicio])->one()) {
          $unid_med = UnidadMedidaProducto::find()->where(['idUnidadMedida'=>$producto->idUnidadMedida])->one();
          $codigo = $producto->codProdServicio;
          $descripcion = $producto->nombreProductoServicio;
          $unidad = $unid_med->simbolo;
          $servicio = $unidad == 'Sp' ? true : false;
          $tarifa_impuesto = $unidad == 'Sp' ? 6.5 : Yii::$app->params['porciento_impuesto'];
          $tipo_impuesto = $unidad == 'Sp' ? '07' : '01';

          if ($cliente_iv_exonerado != true) {//entra aqui ssi no paso por cliente exonerado
              if ($producto->exlmpuesto == 'No') {
                  $tarifa_impuesto = $unidad == 'Sp' ? 6.5 : Yii::$app->params['porciento_impuesto'];
                  $impuesto[$key] = [ 'codigo' => $tipo_impuesto, 'tarifa' => $tarifa_impuesto ];
                } else {
                  $impuesto[$key] = [ 'codigo' => '98', 'tarifa' => 0.00 ];
                }
            }//si no es exonerado se ejecuta el impuesto normal
        }

        //Impuesto General sobre las ventas
        $array_detalle[$key] = [//creamos el array para montar todos los productos para el arreglo de detalle
                'numero_linea'=>$c,
                'tipo_codigo' => '01',// '01' = Codigo del producto del vendedor, '02' = Codigo del producto del comprador, '03' = Codigo del producto asignado por la industria, '04' = Codigo de uso interno, '99' = Otros
                'codigo' => substr($codigo, 0, 20),
                'cantidad' => $value->cantidad,
                'unidad_medida' => $unidad,
                //'unidad_medida_comercial' => NULL,
                'detalle' => substr($descripcion, 0, 150),
                'precio_unitario' => $value->precio_unitario,
                'monto_descuento' => $value->descuento_producto,
                'naturaleza_descuento' => $value->descuento_producto > 0.00 ? 'Oferta' : '',
                'impuesto' => $impuesto,
                'servicio' => $servicio,
                'bonificacion' => false
               ];
               if ($cliente_iv_exonerado != true) { $impuesto[$key] = []; }
      }//fin de foreach del detalle de la factura
           //ENVIAR ENCABEZADO
           $factura = [//seteamos parametros de la factura
                'id_externo' => $numero_original,
                'tipo_documento' => $tipo_documento,//factura electronica o tiquete electronico
                'fecha_emision' => $fecha_documento,
                'medio_pago' => $tipo_pago,//['01','04'] //
                'receptor' => [
                                'razon_social' => $nombre_cliente,
                                'identificacion' => $identificacion_cliente,
                                'identificacion_extranjero' => null,
                                'razon_comercial' => $nombre_cliente,
                                'ubicacion' => [
                                                  "provincia" => $provincia,
                                                  "canton" => $canton,
                                                  "distrito" => $distrito,
                                                  "barrio" => $barrio,
                                                  "otras_senas" => $otras_senas
                                               ],
                                'telefono' => [
                                                "codigo_pais" => "",
                                                "num_telefono" => ""
                                              ],
                                'fax'=> '',
                                //'correo_electronico' => $email
                              ],
                'condicion_venta' => $condicion_venta,//crédito o contado
                'plazo_credito' => $plazo,
                'detalles' => $array_detalle,//agregamos el detalle de la factura
                'resumen_documento' => [
                                        "codigo_moneda" => $moneda_local->abreviatura,
                                        "tipo_cambio" => $tipocambio->valor_tipo_cambio
                                       ]

             ];
             //ENVIAMOS LA FACTURA AL API PARA PROCEDER A FIRMAR ELECTRONICAMENTE EL DOCUMENTO
             $clave = $estado_hacienda = $numero_factura = $id_factura_factun = '0';
             $tran_api = Yii::$app->db->beginTransaction();
             try{
               $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Documento/Enviar';
               $json_result_facturacion = $consultar_factun->ConeccionFactun_con_pan($api_dir, $factura, 'POST');
               $array_result_facturacion = json_decode($json_result_facturacion);

               //recorro los datos de respuesta
               foreach ($array_result_facturacion->data as $key => $value) {
                 if ($key=='clave') { $clave = $value; }
                 if ($key=='estado_hacienda') { $estado_hacienda = $value; }
                 if ($key=='numero_factura') { $numero_factura = $value; }
                 if ($key=='consecutivo') { $id_factura_factun = $value; }
               }
               $mensajes_error = '';
               //recorro las notificaciones de error
               foreach ($array_result_facturacion->mensajes as $key => $value) {
                 if ($key!='codigo_mensaje'&&$key!='mensaje'&&$key!='detalle_mensaje') {
                   $mensajes_error .= '<br> > '.$value;
                 }
               }
               $arr = [
                        'clave'=>$clave,
                        'estado_hacienda'=>$estado_hacienda,
                        'numero_factura'=>$numero_factura,
                        'consecutivo'=>$id_factura_factun,
                        'exito'=>$array_result_facturacion->exito,
                        'mensajes_error'=>$mensajes_error
                      ];
               $tran_api->commit();
              } catch (\Exception $e) {
                $tran_api->rollBack();
                $arr = [
                        'clave'=>$clave,
                        'estado_hacienda'=>$estado_hacienda,
                        'numero_factura'=>$numero_factura,
                        'consecutivo'=>$id_factura_factun,
                        'exito'=>false,
                        'mensajes_error'=>"Fallo: " . $e->getMessage()
                      ];
              }

           return $arr;
    }


    public function crearmovimientolibro_apartado($idCuenta_bancaria, $comprobacion, $idCabeza_Factura, $monto, $fechadepo)
    {
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
      //$facturas_caja = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->one();
      //habro una instancia para obtener el tipo de moneda de la cuenta con la estoy usando
      $moneda = Moneda::find()->where(['idTipo_moneda'=>$modelcuentabancaria->idTipo_moneda])->one();
      //habro una instancia para obtener el tipo de cambio y compararla con la moneda que ya tengo de la cuenta
      $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
      //abrimos una intancia de movimiento libros donde se procese a crear un movimiento automatizado
      $model_movi_libros = new MovimientoLibros();
      $model_movi_libros->idCuenta_bancaria = $idCuenta_bancaria;
      $model_movi_libros->idTipoMoneda = $modelcuentabancaria->idTipo_moneda;
      $model_movi_libros->idTipodocumento = 2;
      $model_movi_libros->idMedio_pago = null;
      $model_movi_libros->numero_documento = (int) $comprobacion;
      $model_movi_libros->fecha_documento = date('Y-m-d', strtotime( $fechadepo ));//$model_mediopago->fecha_documento;
      $model_movi_libros->monto = $monto;
      $model_movi_libros->tasa_cambio = null;
      $model_movi_libros->monto_moneda_local = 0.00;
      //con esto compruebo si hay que hacer tipo cambio.
      if ($moneda->monedalocal!='x') {
          $model_movi_libros->monto = $monto / $tipocambio->valor_tipo_cambio;
          $model_movi_libros->tasa_cambio = $tipocambio->valor_tipo_cambio;
          $model_movi_libros->monto_moneda_local = $monto;
      }
      $model_movi_libros->beneficiario = $empresa->nombre;
      $model_movi_libros->detalle = 'PAGO DE FACTURA CON TRANSFERENCIA BANCARIA';
      $model_movi_libros->fecha_registro = date("Y-m-d");
      $model_movi_libros->usuario_registra = Yii::$app->user->identity->nombre;
      //guardamos el movimiento en libros, si esto es asi...
      if ($model_movi_libros->save()) {
          //...Debemos modificar el monto de cuentas bancarias correspondiendo al movimiento, en ese caso pago a proveedor
          ////habrimos instancia para guardar en cuentas bancarias correspondioente al movimiento en libros
          //$modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$idCuenta_bancaria])->one();
          //como el tipo de movimiento es debito el saldo en libros de cuenta bancaaria es igual a su misma cantidad menos el monto del nuevo movimiento en debito
          $modelcuentabancaria->saldo_libros += $model_movi_libros->monto;

          //guardamos actualizacion en cuenta bancaria
          if ($modelcuentabancaria->save()) {//y modificamos el saldo en libros para el control de saldo por movimiento
              $libros = MovimientoLibros::find()->where(['idMovimiento_libro'=>$model_movi_libros->idMovimiento_libro])->one();
              $libros->saldo = $modelcuentabancaria->saldo_libros;//porque es hasta aqui que el saldo en libros lleva este monto
              $libros->save();
          }
        }//fin $model_movi_libros->save
    }


}
