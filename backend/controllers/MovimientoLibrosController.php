<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use backend\models\MovimientoLibros;
use backend\models\CuentasBancarias;
use backend\models\Moneda;
use backend\models\TipoDocumento;
use backend\models\TipoCambio;
use backend\models\search\MovimientoLibrosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
//---------------------para la modal de eliminar con permiso de admin
use common\models\DeletePass; //para confirmar eliminacion de clientes
/**
 * MovimientoLibrosController implements the CRUD actions for MovimientoLibros model.
 */
class MovimientoLibrosController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'only' => ['index','obtenermontomonedalocal', 'view','create','consulta_repetido',
            'update','delete','obtenercuenta','obtenertipodocumento'],
            'rules' => [
              [
                'actions' => ['index','obtenermontomonedalocal', 'view','create','consulta_repetido',
                'update','delete','obtenercuenta','obtenertipodocumento'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all MovimientoLibros models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MovimientoLibrosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    public function actionObtenermontomonedalocal()
    {
        if(Yii::$app->request->isAjax){
            $monto_mon_local = Yii::$app->request->post('monto_mon_local');
            echo number_format($monto_mon_local,2);
        }
    }

    /**
     * Displays a single MovimientoLibros model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MovimientoLibros model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {
        $model = new MovimientoLibros();

        if ( Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false ) {
            //return $this->redirect(['view', 'id' => $model->idMovimiento_libro]);
            if ($model->validate()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
            }
        }
        if ($model->load(Yii::$app->request->post()))
        {
            $model->monto = floatval(str_replace(",", "", $model->monto));
            $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$model->idCuenta_bancaria])->one();
            $modeltipodoc = TipoDocumento::find()->where(['idTipo_documento'=>$model->idTipodocumento])->one();
            //quita el formato, pasa de varchar a float para guardar en BD
            $model->monto_moneda_local = floatval(str_replace(",", "", $model->monto_moneda_local));

            //debemos de guardar el saldo hasta este momento de lo que hay en la cuenta bancaria de saldo_libros, cada movimiento en
            //libros esta registrado
            if ($modeltipodoc->tipo_movimiento == 'Crédito') {
                $model->saldo = $modelcuentabancaria->saldo_libros + $model->monto;
            } else {
                $model->saldo = $modelcuentabancaria->saldo_libros - $model->monto;
            }

            if ($model->save()) {
                //$saldo_libros = $model->monto + $modelcuentabancaria->saldo_libros;
                if ($modeltipodoc->tipo_movimiento == 'Crédito') {
                    $modelcuentabancaria->saldo_libros += $model->monto;
                } else
                {
                    $modelcuentabancaria->saldo_libros -= $model->monto;
                }
                $modelcuentabancaria->save();

                $model->refresh();
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Movimiento en libro creado correctamente.');
                return $this->redirect(['index', 'carga'=>true]);
            }
            else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
                //return $this->redirect(['index']);
            }
        }
        return $this->renderAjax('create', [
                'model' => $model,
            ]);
    }

    //me comprueba si existe un documento igual al mismo banco
    public function actionConsulta_repetido()
    {
        if(Yii::$app->request->isAjax){
                $numero_documento = Yii::$app->request->post('numero_documento');
                $id_cuenta_bancaria = (int)Yii::$app->request->post('id_cuenta_bancaria');
                $pasa = 'si';

                $cuenta_escrita = CuentasBancarias::find()->where(['idBancos'=>$id_cuenta_bancaria])->one();
                $movimientos = MovimientoLibros::find()->all();

                foreach ($movimientos as $key => $movimiento) {
                    $cuenta_consultada = CuentasBancarias::find()->where(['idBancos'=>$movimiento['idCuenta_bancaria']])->one();
                    if ($movimiento['numero_documento'] == $numero_documento && $cuenta_consultada->idEntidad_financiera == $cuenta_escrita->idEntidad_financiera)
                    {
                      $pasa = 'no';
                    }
                  }
                echo $pasa;

            }
    }

    /**
     * Updates an existing MovimientoLibros model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $submit = false)
    {
        $model = $this->findModel($id);
        $model_old = $this->findModel($id);//con esta intancia puedo obbtener los datos anteriores antes de guardar los nuevos y porder hacer comparaciones

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->monto = floatval(str_replace(",", "", $model->monto));
            //return $this->redirect(['view', 'id' => $model->idMovimiento_libro]);
            //$cuen_banc_new = Yii::$app->request->post('idCuenta_bancaria');
            $cuen_banc_old = $model_old->idCuenta_bancaria;
            $tipodocument_old = $model_old->idTipodocumento;
            $monto_old = $model_old->monto;//obtengo el monto antes de la ejecusión
            $model->monto_moneda_local = floatval(str_replace(",", "", $model->monto_moneda_local));


            if ($model->save()) {
                $modeltipodoc = TipoDocumento::find()->where(['idTipo_documento'=>$model->idTipodocumento])->one();
                $modeltipodoc_old = TipoDocumento::find()->where(['idTipo_documento'=>$tipodocument_old])->one();
                $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=> $model->idCuenta_bancaria])->one();
                //necesitamos hacer una intancia de cuanta bancaria con el anterior id de la cuenta
                $modelcuentabancaria_old = CuentasBancarias::find()->where(['idBancos'=> $cuen_banc_old])->one();
                //si la cuenta bancaria es diferente?
                if ($model->idCuenta_bancaria != $cuen_banc_old) {
                    //si el tipo de movimiento es diferente
                    if ($modeltipodoc->tipo_movimiento != $modeltipodoc_old->tipo_movimiento) {
                        //si el monto es diferente y el movimiento tambien es diferente
                        if($monto_old != $model->monto || $monto_old == $model->monto){
                            if ($modeltipodoc->tipo_movimiento == 'Crédito') {//aqui entra si es credito
                                $modelcuentabancaria_old->saldo_libros = $modelcuentabancaria_old->saldo_libros + $monto_old;
                                $modelcuentabancaria->saldo_libros = $modelcuentabancaria->saldo_libros + $model->monto;
                            } else //aqui entra si es debito
                            {
                                $modelcuentabancaria_old->saldo_libros = $modelcuentabancaria_old->saldo_libros - $monto_old;
                                $modelcuentabancaria->saldo_libros = $modelcuentabancaria->saldo_libros - $model->monto;
                            }
                        }//fin monto igual o monto diferente, movimiento diferente

                    } else {//si el tipo de movimiento es el mismo
                        if ($modeltipodoc->tipo_movimiento == 'Crédito') {//aqui entra si es credito
                            //debe de regresar el valor a la cuenta anterior restanto el monto viejo al saldo en libros
                            $modelcuentabancaria_old->saldo_libros = $modelcuentabancaria_old->saldo_libros - $monto_old;
                            //luego debe agregar el valor a la nueva cuenta restando el monto viejo al saldo en libros y
                            //sumarle el nuevo monto
                            $modelcuentabancaria->saldo_libros = $modelcuentabancaria->saldo_libros /*- $monto_old*/ + $model->monto;
                        } else //aqui entra si es debito
                        {
                            //debe de regresar el valor a la cuenta anterior sumando el monto viejo al saldo en libros
                            $modelcuentabancaria_old->saldo_libros = $modelcuentabancaria_old->saldo_libros + $monto_old;
                            //luego debe agregar el valor a la nueva cuenta teniendo la diferencia del monto nuevo al viejo y sumarcelo al saldo en libros
                            //$modelcuentabancaria->saldo_libros = $monto_old - $model->monto + $modelcuentabancaria->saldo_libros;
                            $modelcuentabancaria->saldo_libros = $modelcuentabancaria->saldo_libros - $model->monto;
                        }

                    }
                    $modelcuentabancaria_old->save(); //guardamos los cambios a la cuenta bancaria anterior
                    //guardamos los datos a la cuenta bancaria nueva y actualizamos el datos del saldo en el movimiento de libros
                    if ($modelcuentabancaria->save()) {
                        $model_update = $this->findModel($id);
                        $model_update->saldo = $modelcuentabancaria->saldo_libros;
                        $model_update->save();
                    }

                } //si la cuenta bancaria es la misma
                  else {
                    if($monto_old != $model->monto && $modeltipodoc->tipo_movimiento == $modeltipodoc_old->tipo_movimiento){
                        if ($modeltipodoc->tipo_movimiento == 'Crédito') {//aqui entra si es credito

                            $modelcuentabancaria->saldo_libros = $modelcuentabancaria->saldo_libros - $monto_old + $model->monto;
                        } else //aqui entra si es debito
                        {
                            $modelcuentabancaria->saldo_libros = $monto_old - $model->monto + $modelcuentabancaria->saldo_libros;
                        }
                    } else {/*Que no se haga nada porque el monto es el mismo y el movieminto no cambio mucho menos la cuenta bancaria*/}
                    //si el monto es diferente y el movimiento tambien es diferente
                    if(($monto_old != $model->monto || $monto_old == $model->monto) && $modeltipodoc->tipo_movimiento != $modeltipodoc_old->tipo_movimiento){
                        if ($modeltipodoc->tipo_movimiento == 'Crédito') {//aqui entra si es credito

                            $modelcuentabancaria->saldo_libros = $modelcuentabancaria->saldo_libros + $monto_old + $model->monto;
                        } else //aqui entra si es debito
                        {
                            $modelcuentabancaria->saldo_libros = $modelcuentabancaria->saldo_libros - $monto_old - $model->monto;
                        }
                    }//fin monto igual o monto diferente, movimiento diferente
                    //if($monto_old != $model->monto && $modeltipodoc->tipo_movimiento != $modeltipodoc_old->tipo_movimiento){}
                    //guardamos los datos en la cuenta bancaria y actualizamos el datos del saldo en el movimiento de libros
                    if ($modelcuentabancaria->save()) {
                        $model_update = $this->findModel($id);
                        $model_update->saldo = $modelcuentabancaria->saldo_libros;
                        $model_update->save();
                    }
                }//fin else

                $model->refresh();
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Movimiento en libro actualizado correctamente.');
                return $this->redirect(['index']);
            } else{
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }
        return $this->renderAjax('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing MovimientoLibros model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
      if(Yii::$app->request->post())
      {
        $id = Html::encode($_POST["idMovimiento_libro"]);
        if($id)
        {
          //se debe obtener la intancia primero antes de eliminar ya que si no la intancia no sabra que monto del movimiento
          //en libros se debolverá a la cuenta bancaria.
          $model = $this->findModel($id);
          $this->findModel($id)->delete();//luego de eso ya se elimina y ya con la instancia cargada se procede...
          //...a devolver el monto

          $modeltipodoc = TipoDocumento::find()->where(['idTipo_documento'=>$model->idTipodocumento])->one();
          $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$model->idCuenta_bancaria])->one();
          //$saldo_libros = $model->monto + $modelcuentabancaria->saldo_libros;
          if ($modeltipodoc->tipo_movimiento == 'Crédito') {
              $modelcuentabancaria->saldo_libros -= $model->monto;
          } else
          {
              $modelcuentabancaria->saldo_libros += $model->monto;
          }
          $modelcuentabancaria->save();
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El Movimiento en libros se ha eliminado correctamente.');
          return $this->redirect(['index']);
        }
      }
    }

    /**
     * Finds the MovimientoLibros model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MovimientoLibros the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MovimientoLibros::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionObtenercuenta()
    {
        if(Yii::$app->request->isAjax){
            $idBancos = Yii::$app->request->post('idBancos');
            $cuenta = CuentasBancarias::find()->where(['idBancos'=>$idBancos])->one();
            $moneda = Moneda::find()->where(['idTipo_moneda'=>$cuenta->idTipo_moneda])->one();
            $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
            $arr = array('idTipo_moneda'=>$moneda->idTipo_moneda, 'descrip_moneda'=>$moneda->descripcion, 'local'=>$moneda->monedalocal, 'valorcambio'=>$tipocambio->valor_tipo_cambio, 'simbolo'=>$moneda->simbolo);
            echo json_encode($arr);
        }
    }

    public function actionObtenertipodocumento()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_documento = Yii::$app->request->post('idTipo_documento');
            $tipodocumento = TipoDocumento::find()->where(['idTipo_documento'=>$idTipo_documento])->one();
            echo $tipodocumento->tipo_movimiento;
        }
    }
}
