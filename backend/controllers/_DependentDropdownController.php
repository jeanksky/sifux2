<?php


namespace frontend\controllers;

use yii\web\Controller;
use common\models\HtmlHelpers;
use backend\models\Servicios;

class DependentDropdownController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionServicios($id){
        echo HtmlHelpers::checkboxList(Servicios::className(), 'codFamilia', $id, 'codFamilia', 'descripcion');
    }

    
}