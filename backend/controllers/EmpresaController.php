<?php

namespace backend\controllers;
use Yii;
use backend\models\Empresa;
use backend\models\search\EmpresaSearch;
use backend\models\EncabezadoPrefactura;
use backend\models\CabezaCaja;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\models\Factun;
use yii\helpers\Url;
use yii\filters\AccessControl;

require("mpdf/mpdf.php");
use mPDF;
/**
 * EmpresaController implements the CRUD actions for Empresa model.
 */
class EmpresaController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'only' => ['update','deleteimg','activar_factun','guardar_datos_factun','crear_carpeta','factura','pdf'],
              'rules' => [
                [
                  'actions' => ['update','deleteimg','activar_factun','guardar_datos_factun','crear_carpeta','factura','pdf'],
                  'allow' => true,
                  'roles' => ['@'],
                ]
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $searchModel = new EmpresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Displays a single Empresa model.
     * @param integer $id
     * @return mixed
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Empresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Empresa();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {

    //         //get the intance of the uploaded file
    //         $imageName = $model->nombre;
    //         $model->image = UploadedFile::getInstance($model,'image');
    //         $model->image->saveAs('backend/images/'.$imageName.'.'.$model->image->extension);

    //         //save the path in the db colum
    //         $model->file ='backend/images/'.$imageName.'.'.$model->file->extension;
    //         return $this->redirect(['view', 'id' => $model->idEmpresa]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }
  /*  public function actionCreate()
    {
        $model = new Empresa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->file = UploadedFile::getInstance($model, 'file');
            $save_file = '';
            if($model->file){
                $imagepath = 'uploads/logo/'; // Create folder under web/uploads/logo
                $model->logo = $imagepath.$model->file->name;
                $save_file = 1;
            }
             if ($model->save()) {
            if($save_file){
                $model->file->saveAs($model->logo);
            }
             return $this->redirect(['/site/index']);
        }
       } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

}*/

    /**
     * Updates an existing Empresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['/site/index']);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file_et = UploadedFile::getInstance($model, 'file_et');
            $save_file = '';
            $save_file_et = '';
            if($model->file){
                $imagepath = 'uploads/logo/';
                $model->logo = $imagepath.$model->file->name;
                $save_file = 1;
            }
            if($model->file_et){
                $imagepath = 'uploads/logo/';
                $model->logo_etiqueta = $imagepath.$model->file_et->name;
                $save_file_et = 1;
            }
            if ($model->save()) {
                if($save_file){
                        $model->file->saveAs($model->logo);
                    }
                if($save_file_et){
                        $model->file_et->saveAs($model->logo_etiqueta);
                    }
             return $this->redirect(['/site/index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Empresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
  /*  public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

     // Add function for delete image or file
    public function actionDeleteimg($id, $field)
    {

            $img = $this->findModel($id)->$field;
            if($img){
                if (!unlink($img)) {
                return false;
            }
        }

        $img = $this->findModel($id);
        $img->$field = NULL;
        $img->update();

        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Finds the Empresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionActivar_factun()
    {
      if(Yii::$app->request->isAjax){
        $factun_conect = Yii::$app->request->post('factun_conect');
        $model = Empresa::findOne(1);
        $model->factun_conect = $factun_conect;
        $model->save();
        return true;
      }
    }

    public function actionGuardar_datos_factun()
    {
      if(Yii::$app->request->isAjax){
        $user_factun = Yii::$app->request->post('user_factun');
        $pass_factun = Yii::$app->request->post('pass_factun');
        $compania_id = Yii::$app->request->post('compania_id');
        $sucursal_id = Yii::$app->request->post('sucursal_id');
        $empresa = Empresa::findOne(1);
        $empresa->user_factun = $user_factun;
        $empresa->pass_factun = $pass_factun;
        $empresa->compania_id = (int)$compania_id;
        $empresa->sucursal_id = (int)$sucursal_id;
        if ($empresa->save()) {
          echo '<center><span class="label label-success">Conexión guardada corectamente.</span></center>';
        }else {
          echo '<center><span class="label label-important">No se guardo la conexión.</span></center>';
        }
      }
    }

    public function actionCrear_carpeta()
    {
      if(Yii::$app->request->isAjax){
        $serv = $_SERVER['DOCUMENT_ROOT'] . '/';
        $carpeta = $serv.Yii::$app->request->post('nombre_carpeta');
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
            echo $carpeta;
        }
      }
    }

    public function actionFactura()// {"exito":true,"id":303,"data":{},"mensajes":{}}
    {
      $consultar_factun = new Factun();
      $empresa = Empresa::findOne(1);
      //ENVIAR ENCABEZADO
    /*  $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Crear';
      $json_encabezado = [
        "fecha_documento" => "2018-04-18",
        "numero_original" => "03",
        "tipo_documento" => "04",
        "condicion_venta" => "01",
        "tipo_pago" => "01",
        "plazo" => "",
        "sucursal_id" => $empresa->sucursal_id,//198,
        "compania_id" => $empresa->compania_id,//156,
        "moneda" => "CRC",
        "tipo_cambio" => 569.87,
        "identificacion" => "",
        "tipo_identificacion" => "",
        "nombre_cliente" => "",
        "email" => "",
        "razon_de_nota" => "Factura"
      ];
      $result_encabezado = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir, $json_encabezado, 'POST'));
      //ENVIAR DETALLE
      $api_dir2 = 'https://'.Yii::$app->params['url_api'].'/api/v1/Detalle/Crear';
      $json_detalle = [
          'documento_id' => $result_encabezado->id,
          'detalles' => [
            0 => [
                  'numero_linea' => 1,
                  'cantidad' => 3,
                  'tipo_codigo' => '01',
                  'codigo' => 'AZUC',
                  'descripcion' => 'Azucar 1000g',
                  'unidad' => 'g',
                  'moneda' => 'CRC',
                  'tipo_impuesto' => '01',
                  'porcentaje_impuesto' => Yii::$app->params['porciento_impuesto'],
                  'precio_unitario' => 1320.50,
                  'porcentaje_descuento' => 0,
                  'servicio' => false,
                ],
            1 => [
                  'numero_linea' => 2,
                  'cantidad' => 2,
                  'tipo_codigo' => '01',
                  'codigo' => 'TEMAN',
                  'descripcion' => 'Té de manzanilla',
                  'unidad' => 'g',
                  'moneda' => 'CRC',
                  'tipo_impuesto' => '01',
                  'porcentaje_impuesto' => Yii::$app->params['porciento_impuesto'],
                  'precio_unitario' => 10,
                  'porcentaje_descuento' => 0,
                  'servicio' => false,
                ],
          ],
        ];
      $result_detalle = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir2, $json_detalle, 'POST'));
      //FACTURAR
      $api_dir3 = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Facturar/'.$result_detalle->id;
      $facturar = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir3, 'PUT'));*/

      //CONSULTAR
      $id_factun = 606; //este es el numero de la factura
      $clave = ''; $numero_factura = '';
      $api_consultar = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/'.$id_factun;
      $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/XML/'.$id_factun;
      //obtengo los datos de la factura desde factun
      $result_factura = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_consultar, 'GET'));
        foreach (/*$result_factura->data*/$result_factura as $key => $value) {
          if ($key == 'clave') { $clave = $value; }//con esto obtenemos el numero de factura de hacienda para atribuircelo al archivo
          if ($key == 'numero_factura') { $numero_factura = $value; }
        }
      $this->enviar_correo_factura(42, 'jeank17@gmail.com', 546);
     $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');//consultamos el xml
      $archivo_xml = $clave.".xml";//creamos el nombre del archivo xml, y como de aqui se obtiene no se le dice en que directorio esta
      $archivo_pdf = $clave.'.pdf';//creamos el nombre del arhivo pdf, ...
      //creamos el xml en el servidor (estará temporalmente)
      $archivo = fopen($archivo_xml, "a+");//creamos el archivo xml y con permiso de lectura y escritura
      fwrite($archivo, $result_xml);//escribimos los datos xml en el archivo
      fclose($archivo);//cerramos el archivo
      //creamos el Pdf en el servidor (estará temporalmente)
      /*$pdf = new mPDF();
      $pdf->title = 'Factura No. ';
      $pdf->SetHtmlHeader('encabezado');
      $pdf->WriteHTML('listo perro');
      $pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());
      $pdf->Output($archivo_pdf,'');//de esta forma se guarda en el servidor otras opciones 'I'=para ver en pantalla, 'D'=para descargar
      //fin de creacion de PDF
*/
        //lo siguiente es para imprimir el archivo en el navegador
        header("Content-type: application/octet-stream");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment; filename=\"$archivo_xml\"\n");//imprimimos el archivo
        readfile($archivo_xml);//se lee el archivo para mostrarlo
        //Enviamos el correo
      /*  Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>'esto es una prueba'])
        ->setTo('jeank17@gmail.com')//para
        ->setFrom($empresa->email_notificaciones)//de
        ->setSubject('nueva prueba')
        ->setHtmlBody('esto es una prueba')
        ->attach($archivo_xml)->attach($archivo_pdf)//se agragan todos los adjuntos
        ->send();*/
        unlink($archivo_xml);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
      //  unlink($archivo_pdf);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor

      Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Respuesta:</strong>. listo '.$clave);
      return $this->redirect(['create']);
    }

    public function actionPdf()
    {
      return $this->render('pdf');
    }

    public function pdf()
    {
        return '<header class="clearfix">
              <div id="company">
                <table>
                <tr>
                <td>
                <img src="https://staticaltmetric.s3.amazonaws.com/uploads/2015/10/dark-logo-for-site.png" width="30%">
                </td>
                <td>
                <h2 class="name">Company Name - ID: 000000</h2>
                <div>455 Foggy Heights, AZ 85004, US</div>
                <div>(602) 519-0450</div>
                <div><a href="mailto:company@example.com">company@example.com</a></div>
                </td>
                </tr>
                </table>
              </div>
        </header>
        <main>
          <div id="details" class="clearfix">
            <table class="table_info">
              <tr>
                <th class="derecha">
                  <div id="client">
                    <div class="to">Factura a:</div>
                    <h2 class="name">John Doe</h2>
                    <div class="address">796 Silver Harbour, TX 79273, US</div>
                    <div class="email"><a href="mailto:john@example.com">john@example.com</a></div>
                  </div>
                </th>
                <th class="izquierda">
                  <div id="invoice">
                    <h1 style="color: #0087C3;">Factura no. 00001</h1>
                    <h2 class="name" style="color: #57B223;">FA: 00100001010000000007</h2>
                    <div class="date">Fecha de la factura: 01/06/2014</div>
                    <div class="date">Fecha de vencimiento: 30/06/2014</div>
                    <div style="font-size: 0.8em;  color: #FF0000;">Clave: 50625041800050359010200100001010000000007173033080</div>
                    <h3 style="font-size: 1.0em; color: #0087C3;">Condición Venta: Crédito | Tipo de Pago:	Efectivo</h3>
                  </div>
                </th>
              </tr>
            </table>
          </div>
          <table border="0" cellspacing="0" cellpadding="0" class="table_detalle">
            <thead>
              <tr>
                <th class="no">C.</th>
                <th class="cod">COD</th>
                <th class="desc">DESCRIPCIÓN</th>
                <th class="unit">PREC UNIT</th>
                <th class="qty">DESCTO</th>
                <th class="unit">IV %</th>
                <th class="total">TOTAL</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="no">999</td>
                <td class="cod">CODIGO</td>
                <td class="desc"><strong>Website Design</strong></td>
                <td class="unit">$40.00</td>
                <td class="qty">30</td>
                <td class="unit">13.00</td>
                <td class="total">$1,200.00</td>
              </tr>
              <tr>
                <td class="no">02</td>
                <td class="cod">CODIGO</td>
                <td class="desc"><h3>Website Development</h3></td>
                <td class="unit">$40.00</td>
                <td class="qty">80</td>
                <td class="unit">13.00</td>
                <td class="total">$3,200.00</td>
              </tr>
              <tr>
                <td class="no">03</td>
                <td class="cod">CODIGO</td>
                <td class="desc"><h3>Search Engines Optimization</h3></td>
                <td class="unit">$40.00</td>
                <td class="qty">20</td>
                <td class="unit">13.00</td>
                <td class="total">$800.00</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="3"></td>
                <td colspan="3">SUBTOTAL</td>
                <td>$5,200.00</td>
              </tr>
              <tr>
                <td colspan="3"></td>
                <td colspan="3">TOTAL DESCUENTO</td>
                <td>$5,200.00</td>
              </tr>
              <tr>
                <td colspan="3"></td>
                <td colspan="3">TOTAL IV</td>
                <td>$1,300.00</td>
              </tr>
              <tr>
                <td colspan="3"></td>
                <td colspan="3"><strong>TOTAL A PAGAR</strong></td>
                <td><strong>$1,644,500.00</strong></td>
              </tr>
            </tfoot>
          </table>
          <div id="thanks">¡Gracias por su preferencia!</div>
          <div id="notices">
            <div>Observación:</div>
            <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
          </div>
        </main>
        <footer>
          Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D<br>
          Autorizado mediante resolución 11-97 del 05/09/1997 de la D.G.T.D
        </footer>';
    }

    public function enviar_correo_factura($id, $email, $id_factun)
    {
      $consultar_factun = new Factun();
        $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $model = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$id])->one();
            if (@$model) {

              $encabezado = '<table>
                  <tbody>
                  <tr>
                  <td>
                  <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                  </td>
                  <td style="text-align: left;">
                  <strong>'.$empresa->nombre.'</strong><br>
                  <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                  <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                  <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                  <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                  <strong>Dirección:</strong> '.$empresa->email.'
                  </td>
                  <tr>
                  </tbody>
              </table>';
              $informacion_factura_cancelada = $this->renderAjax('view',['model' => $model]);
              $pdf_ = $this->pdf();
              $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
              $content = '
              <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO">
                <div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
                  <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
                    <br>
                    <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
                      <tbody><tr>
                        <td align="center" valign="top">
                          <table width="80%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                            <tbody>
                              <tr>
                                <td align="right" style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">

                                </td>
                              </tr>
                              <tr>
                                <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                                 <br>
                                  <br><br>
                                  '.$encabezado.'
                                  <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">ENVÍO DE FACTURA # '.$id.'<br><small> '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>
                                  '.$informacion_factura_cancelada.'
                                  <br><br><br><br><br><br><br><br><br>
                                </td>
                              </tr>
                              <tr>
                              <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                                Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                    </table>
                  <br><br>
                  </div>
                </div>
              </div>
              ';
              //CONSULTAR
              $clave = $numero_factura = '';
              $comprobante_electronico = '';
              $api_consultar = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/'.$id_factun;
              $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/XML/'.$id_factun;
              //obtengo los datos de la factura desde factun
              $result_factura = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_consultar, 'GET'));
                foreach ($result_factura as $key => $value) {
                  //con esto obtenemos los datos de la factura
                  if ($key == 'clave') { $clave = $value; }
                  if ($key == 'numero_factura') { $numero_factura = $value; }
                }
              $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');//consultamos el xml
              $archivo_xml = $clave.".xml";//creamos el nombre del archivo xml, y como de aqui se obtiene no se le dice en que directorio esta
              $archivo_pdf = $clave.'.pdf';//creamos el nombre del arhivo pdf, ...
              //creamos el xml en el servidor (estará temporalmente)
              $archivo = fopen($archivo_xml, "a+");//creamos el archivo xml y con permiso de lectura y escritura
              fwrite($archivo, $result_xml);//escribimos los datos xml en el archivo
              fclose($archivo);//cerramos el archivo
              //creamos el Pdf en el servidor (estará temporalmente)
              $stylesheet = file_get_contents('css/style_print.css');
              $pdf = new mPDF();
              $pdf->title = 'Factura No. '.$id;
              //$pdf->SetHtmlHeader($comprobante_electronico);
              $pdf->WriteHTML($stylesheet,1);
              $pdf->WriteHTML($pdf_,2);
              //$pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());
              $pdf->Output($archivo_pdf,'');
              //Creamos el mensaje que será enviado a la cuenta de correo del usuario
              $subject = "Nueva factura por compra a ".$empresa->nombre;
              //Enviamos el correo
              Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
              ->setTo($email)//para
              ->setFrom($empresa->email_contabilidad)//de
              ->setSubject($subject)
              ->setHtmlBody($content)
              ->attach($archivo_xml)->attach($archivo_pdf)//->attach($adjunto[2])//se agragan todos los adjuntos
              ->send();
                unlink($archivo_xml);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
                unlink($archivo_pdf);//eliminamos el archivo xml (que teniamos temporal) para que no se quede en el servidor
              //  echo '<center><span class="label label-success" style="font-size:15pt;">Factura enviada correctamente.</span></center><br>';

            } //fin @$model
            //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> La cotización fué enviada correctamente.');
            //$this->redirect(['update', 'id' => $id]);
    }
}
