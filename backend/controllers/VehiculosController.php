<?php

namespace backend\controllers;

use Yii;
use backend\models\Vehiculos;
use backend\models\search\VehiculosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OrdenServicio;

use backend\models\Clientes;
use backend\models\Modelo;
use backend\models\Marcas;
use backend\models\ListaFacturaVehicular;
use kartik\widgets\DepDrop;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
//---------------------para la modal
use yii\web\Response;
use common\models\DeletePass; //para confirmar eliminacion de clientes
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;


/**
 * VehiculosController implements the CRUD actions for Vehiculos model.
 */
class VehiculosController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete','modelo'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete','modelo'],
            'allow' => true,
            'roles' => ['@'],
          ]
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

    /**
     * Lists all Vehiculos models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $searchModel = new VehiculosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/
    public function actionIndex()
    {
        $searchModel = new VehiculosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single Vehiculos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }

        $model = $this->findModel($id);
        ListaFacturaVehicular::setContenidoFacturaVe(null);
        $facturas = ListaFacturaVehicular::getContenidoFacturaVe();

        //Me obtiene los datos del encabezado correspondiente
        $query = new yii\db\Query();
        $data = $query->select(['estadoFactura','idCabeza_Factura','idOrdenServicio','fecha_inicio','fecha_final','subtotal','porc_descuento','iva','total_a_pagar'])
             ->from('tbl_encabezado_factura')
             ->where(['=','idCliente', $model->idCliente])
             ->andWhere(['!=','idOrdenServicio', ''])
             ->distinct()
             ->all();
        //Recorre dos datos para cargar las variables de la tabla correspondiente a ese encabezado prefactura
        foreach($data as $row){
            $estadoFactura = $row['estadoFactura'];
            $idCabeza_Factura = $row['idCabeza_Factura'];
            $fecha_inicio = $row['fecha_inicio'];
            $fecha_final = $row['fecha_final'];
            $subtotal = $row['subtotal'];
            $porc_descuento = $row['porc_descuento'];
            $iva = $row['iva'];
            $total_a_pagar = $row['total_a_pagar'];
            $estadoFactura = $row['estadoFactura'];
            $idOrdenServicio = $row['idOrdenServicio'];

            $Orden = OrdenServicio::find()->where(['idOrdenServicio'=>$idOrdenServicio])->one();

            if(@$Orden->placa==$model->placa)
            {
                $_POST['idOrdenServicio']= $idOrdenServicio;
                $_POST['estadoFactura']= $estadoFactura;
                $_POST['idCabeza_Factura']= $idCabeza_Factura;
                $_POST['fecha_inicio']= $fecha_inicio;
                $_POST['fecha_final']= $fecha_final;
                $_POST['subtotal']= $subtotal;
                $_POST['porc_descuento']= $porc_descuento;
                $_POST['iva']= $iva;
                $_POST['total_a_pagar']= $total_a_pagar;

                $facturas[] = $_POST;;
            }
            ListaFacturaVehicular::setContenidoFacturaVe($facturas);
        }

        return $this->render('view', [
            'model' => $model,
            'us'=>$us,
        ]);
    }

    /**
     * Creates a new Vehiculos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vehiculos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El vehículo ha sido registrado correctamente.');
            return $this->redirect(['create', 'model' => $model]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Vehiculos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El vehículo ha sido actualizado correctamente.');
            return $this->redirect(['update', 'id' => $model->placa]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Vehiculos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
    public function actionDelete()
    {
        if(Yii::$app->request->post())
        {
            $placa = Html::encode($_POST["placa"]);
            if((int) $placa)
            {
                $ordenS = OrdenServicio::find()->where(['placa'=>$placa])->all();
                if(!$ordenS)
                {
                    Vehiculos::deleteAll("placa=:placa", [":placa" => $placa]);
                    if(!isset($_GET['ajax']))  {
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El vehículo se ha eliminado correctamente.');
                    return $this->redirect(['index']);
                    }
                }
                else
                {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> Imposible eliminar vehículo, contiene registros asociados.');
                    return $this->redirect(['index']);
                }
            }
            else
            {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br> El vehículo no se ha podido eliminar.');
                return $this->redirect(['index']);
            }
        }
        else
        {
            return $this->redirect(['index']);
        }
    }

    // THE CONTROLLER

    public function actionModelo() {
     $out = [];
     if (isset($_POST['depdrop_parents'])) {
         $parents = $_POST['depdrop_parents'];
         if ($parents != null) {
             $codMarca = $parents[0];
             $out = $this->getModelo($codMarca);
             echo Json::encode(['output'=>$out, 'selected'=>'']);
             return;
         }
     }
     //echo Json::encode(['output'=>'', 'selected'=>'']);
 }



  protected function getModelo($codMarca){
     $datas = Marcas::find()->where(['codMarca'=>$codMarca])->all();
     $codMarca = \yii\helpers\ArrayHelper::map($datas,'codMarca','codMarca');
     $datas2 = Modelo::find()->where(['codMarca'=>$codMarca])->all();
     return $this->MapData($datas2,'codModelo','nombre_modelo');
 }

 protected function MapData($datas,$fieldId,$fieldName){
     $obj = [];
     foreach ($datas as $key => $value) {
         array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}]);
     }
     return $obj;
 }


    /**
     * Finds the Vehiculos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vehiculos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vehiculos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
