<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use backend\models\CuentasBancarias;
use backend\models\search\CuentasBancariasSearch;
use backend\models\CategoriaTipoCambio;
use backend\models\TipoDocumento;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Moneda;
use backend\models\EntidadesFinancieras;
use backend\models\TipoCambio;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
//---------------------para la modal de eliminar con permiso de admin
use common\models\DeletePass; //para confirmar eliminacion de clientes

/**
 * CuentasBancariasController implements the CRUD actions for CuentasBancarias model.
 */
class CuentasBancariasController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                [
                  'actions' => ['index','view','create','update','delete','agrega_moneda',
                  'actualiza_modal_moneda','actualizar_moneda','elimina_moneda','local_moneda',
                  'agrega_entidad_fi','actualiza_modal_entidad','actualizar_entidad',
                  'elimina_entidad','categoria_tipo_cambio','agrega_categoria','actualiza_modal_categoria',
                  'actualizar_categoria','elimina_categoria','tipo_cambio',
                  'agrega_tcambio','actualiza_modal_tcambio','actualizar_tcambio',
                  'elimina_tcambio','valorcambiousar','agrega_documento',
                  'actualiza_modal_documento','actualizar_documento','elimina_documento'
                ],
                  'allow' => true,
                  'roles' => ['@'],
                ]
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CuentasBancarias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CuentasBancariasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $us = new DeletePass();
        if ( $us->load(Yii::$app->request->post()) && Yii::$app->request->isAjax  )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($us);
        }
        if ($us->load(Yii::$app->request->post())) {
            if ($us->validate())
            {
                $us->username = null;
                $us->password = null;
            }
            else
            {
                $us->getErrors();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'us'=>$us,
        ]);
    }

    /**
     * Displays a single CuentasBancarias model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CuentasBancarias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {
        $model = new CuentasBancarias();

        if ( Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false ) {
            //return $this->redirect(['view', 'id' => $model->idMovimiento_libro]);
            if ($model->validate()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            //antes de guardar debo tomar el valor float y quitarle los separadores porque no son reconocidos por la BD
            $model->saldo_libros = floatval(str_replace(",", "", $model->saldo_libros));
            $model->saldo_bancos = floatval(str_replace(",", "", $model->saldo_bancos));
            if ($model->save()) {//una ves hecho eso se guarda normal
                $model->refresh();
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Cuenta bancaria creada correctamente.');
                //return $this->redirect(['update', 'id' => $model->idBancos]);
                return $this->redirect(['index']);
            }
            else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }
            return $this->renderAjax('create', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing CuentasBancarias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id, $submit = false)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            //antes de guardar debo tomar el valor float y quitarle los separadores porque no son reconocidos por la BD
            $model->saldo_libros = floatval(str_replace(",", "", $model->saldo_libros));
            $model->saldo_bancos = floatval(str_replace(",", "", $model->saldo_bancos));
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Cuenta bancaria actualizada correctamente.');
                //return $this->redirect(['update', 'id' => $model->idBancos]);
                $model->refresh();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->redirect(['index']);
            }
            else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->renderAjax('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing CuentasBancarias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete()
    {
      if(Yii::$app->request->post())
      {
        $id = Html::encode($_POST["idBancos"]);
        if($id)
        {
          $this->findModel($id)->delete();
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Cuenta bancaria eliminada correctamente.');
          return $this->redirect(['index']);
        }
      }
    }

    /**
     * Finds the CuentasBancarias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CuentasBancarias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CuentasBancarias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //agrega moneda a BD
    public function actionAgrega_moneda()
    {
        $moneda = new Moneda();
        if(Yii::$app->request->isAjax){
            $abrevi = Yii::$app->request->post('abrevi');
            $descrip = Yii::$app->request->post('descrip');
            $simbol = Yii::$app->request->post('simbol');

            $moneda->abreviatura = $abrevi;
            $moneda->descripcion = $descrip;
            $moneda->simbolo = $simbol;
            $moneda->save();
            echo 'n';
        }
    }

    //obtiene los datos de la moneda seleccionada en base de datos para actualizar
    public function actionActualiza_modal_moneda()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_moneda = Yii::$app->request->post('idTipo_moneda');
            $moneda = Moneda::find()->where(['idTipo_moneda'=>$idTipo_moneda])->one();
            $arr = array('idTipo_moneda'=>$moneda->idTipo_moneda, 'abreviatura'=>$moneda->abreviatura, 'descripcion'=>$moneda->descripcion, 'simbolo'=>$moneda->simbolo);
            echo json_encode($arr);
        }
    }

    //actualiza la moneda seleccionada en base de datos
    public function actionActualizar_moneda()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_moneda = Yii::$app->request->post('idTipo_moneda');
            $abrevi = Yii::$app->request->post('abrevi');
            $descrip = Yii::$app->request->post('descrip');
            $simbol = Yii::$app->request->post('simbol');
            $moneda = Moneda::find()->where(['idTipo_moneda'=>$idTipo_moneda])->one();
            $moneda->abreviatura = $abrevi;
            $moneda->descripcion = $descrip;
            $moneda->simbolo = $simbol;
            $moneda->save();
        }
    }

    ////elimina una moneda
    public function actionElimina_moneda(){
        if(Yii::$app->request->isAjax){
            $idTipo_moneda = Yii::$app->request->post('idTipo_moneda');
            \Yii::$app->db->createCommand()->delete('tbl_moneda', 'idTipo_moneda = '.(int) $idTipo_moneda)->execute();
        }
    }

    //actualiza la moneda local
    public function actionLocal_moneda()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_moneda = Yii::$app->request->post('idTipo_moneda');
            $monedamarcada = Moneda::find()->all();
            foreach($monedamarcada as $moneda) {
                $moneda->monedalocal = '';
                $moneda->save();
            }
            $moneda = Moneda::find()->where(['idTipo_moneda'=>$idTipo_moneda])->one();
            $moneda->monedalocal = 'x';
            $moneda->save();
        }
    }

    public function actionAgrega_entidad_fi()
    {
        $entidad = new EntidadesFinancieras();
        if(Yii::$app->request->isAjax){
            $abrevi = Yii::$app->request->post('abrevi');
            $descrip = Yii::$app->request->post('descrip');
            $telefono1 = Yii::$app->request->post('telefono1');
            $telefono2 = Yii::$app->request->post('telefono2');

            $entidad->abreviatura = $abrevi;
            $entidad->descripcion = $descrip;
            $entidad->telefono1 = $telefono1;
            $entidad->telefono2 = $telefono2;
            $entidad->save();
        }
    }

    //obtiene los datos de la entidad financiera seleccionada en base de datos para actualizar
    public function actionActualiza_modal_entidad()
    {
        if(Yii::$app->request->isAjax){
            $idEntidad_financiera = Yii::$app->request->post('idEntidadf');
            $entidad = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$idEntidad_financiera])->one();
            $arr = array('idEntidad_financiera'=>$entidad->idEntidad_financiera, 'abreviatura'=>$entidad->abreviatura, 'descripcion'=>$entidad->descripcion, 'telefono1'=>$entidad->telefono1, 'telefono2'=>$entidad->telefono2);
            echo json_encode($arr);
        }
    }

    //actualiza la entidad financiera seleccionada en base de datos
    public function actionActualizar_entidad()
    {
        if(Yii::$app->request->isAjax){
            $idEntidad_financiera = Yii::$app->request->post('idEntidad_financiera');
            $abrevi = Yii::$app->request->post('abrevi');
            $descrip = Yii::$app->request->post('descrip');
            $telefono1 = Yii::$app->request->post('telefono1');
            $telefono2 = Yii::$app->request->post('telefono2');
            $entidad = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$idEntidad_financiera])->one();
            $entidad->abreviatura = $abrevi;
            $entidad->descripcion = $descrip;
            $entidad->telefono1 = $telefono1;
            $entidad->telefono2 = $telefono2;
            $entidad->save();
        }
    }

    ////elimina un  ontacto de pedido
    public function actionElimina_entidad(){
        if(Yii::$app->request->isAjax){
            $idEntidad_financiera = Yii::$app->request->post('idEntidad_financiera');
            \Yii::$app->db->createCommand()->delete('tbl_entidades_financieras', 'idEntidad_financiera = '.(int) $idEntidad_financiera)->execute();
        }
    }
//----------------------------------------------------------MODULO CATEGORIA TIPO DE CAMBIO
    public function actionCategoria_tipo_cambio()
    {
        return $this->renderAjax('categoria_tipo_cambio');
    }

    //agrega la categoria tipo de caambio a base de datos
    public function actionAgrega_categoria()
    {
        $categoriatipoc = new CategoriaTipoCambio();
        if(Yii::$app->request->isAjax){
            $categoria = Yii::$app->request->post('catego');
            $idTipo_moneda = Yii::$app->request->post('ddl_moneda');
            $idEntidad_financiera = Yii::$app->request->post('ddl_entidadfina');

            $categoriatipoc->categoria = $categoria;
            $categoriatipoc->idTipo_moneda = $idTipo_moneda;
            $categoriatipoc->idEntidad_financiera = $idEntidad_financiera;
            $categoriatipoc->save();
        }
    }

    //obtiene los datos de la categoria tipo cambio seleccionada en base de datos para actualizar
    public function actionActualiza_modal_categoria()
    {
        if(Yii::$app->request->isAjax){
            $idCategoria = Yii::$app->request->post('idCategoria');
            $categoriatipoc = CategoriaTipoCambio::find()->where(['idCategoria'=>$idCategoria])->one();
            $arr = array('idCategoria'=>$categoriatipoc->idCategoria, 'categoria'=>$categoriatipoc->categoria, 'idTipo_moneda'=>$categoriatipoc->idTipo_moneda, 'idEntidad_financiera'=>$categoriatipoc->idEntidad_financiera);
            echo json_encode($arr);
        }
    }

    //actualiza los datos en base de datos editados en la modal de actualizacionde categoria de tipo de camnio
    public function actionActualizar_categoria()
    {
        if(Yii::$app->request->isAjax){
            $idCategoria = Yii::$app->request->post('idCategoria');
            $categoria = Yii::$app->request->post('catego');
            $idTipo_moneda = Yii::$app->request->post('ddl_moneda');
            $idEntidad_financiera = Yii::$app->request->post('ddl_entidadfina');
            $categoriatipoc = CategoriaTipoCambio::find()->where(['idCategoria'=>$idCategoria])->one();
            $categoriatipoc->idCategoria = $idCategoria;
            $categoriatipoc->categoria = $categoria;
            $categoriatipoc->idTipo_moneda = $idTipo_moneda;
            $categoriatipoc->idEntidad_financiera = $idEntidad_financiera;
            $categoriatipoc->save();
        }
    }

    //funcion que me elimina la categorria seleccionada
    public function actionElimina_categoria()
    {
        if(Yii::$app->request->isAjax){
            $idCategoria = Yii::$app->request->post('idCategoria');
            $tipocamb = TipoCambio::find()->where(['idCategoria'=>$idCategoria])->one();
            if ($tipocamb) {
                echo'
                        <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>¡Error!</strong> Este registro no se puede eliminar, cuenta con datos asociados.
                        </div>
                    ';
            } else {
                \Yii::$app->db->createCommand()->delete('tbl_categoria_tipo_cambio', 'idCategoria = '.(int) $idCategoria)->execute();
                echo "";
            }


        }
    }

//-----------------------------------------------------------------------MODULO TIPO CAMBIO
    public function actionTipo_cambio()
    {
        return $this->renderAjax('tipo_cambio');
    }
    //Agrega el itpo de cambio de la moneda a base de datos
    public function actionAgrega_tcambio()
    {
        $tipocam = new TipoCambio();
        if(Yii::$app->request->isAjax){
            $ddl_idCategoria = Yii::$app->request->post('ddl_idCategoria');
            $valorcambio = Yii::$app->request->post('valorcambio');
            $categoriatipocambio = CategoriaTipoCambio::find()->where(['idCategoria'=>$ddl_idCategoria])->one();
            $fecha = date("Y-m-d H:i:s", time());

            $tipocam->idTipo_moneda = $categoriatipocambio->idTipo_moneda;
            $tipocam->idCategoria = $ddl_idCategoria;
            $tipocam->fecha_hora = $fecha;
            $tipocam->valor_tipo_cambio = $valorcambio;
            $tipocam->save();
            $this->redirect(['index','cambio']);
        }
    }

    //obtiene los datos del tipo de cambio seleccionado en base de datos para actualizar
    public function actionActualiza_modal_tcambio()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_cabio = Yii::$app->request->post('idTipo_cabio');
            $tipocamb = TipoCambio::find()->where(['idTipo_cabio'=>$idTipo_cabio])->one();
            $arr = array('idTipo_cabio'=>$tipocamb->idTipo_cabio, 'idCategoria'=>$tipocamb->idCategoria, 'valor_tipo_cambio'=>$tipocamb->valor_tipo_cambio);
            echo json_encode($arr);
        }
    }

    //actualiza los datos en base de datos editados en la modal de actualizacion de tipo de cambio
    public function actionActualizar_tcambio()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_cabio = Yii::$app->request->post('idtcambio');
            $idCategoria = Yii::$app->request->post('ddl_idCategoria_');
            $valor_tipo_cambio = Yii::$app->request->post('valorcambio_');

            $categoriatipocambio = CategoriaTipoCambio::find()->where(['idCategoria'=>$idCategoria])->one();
            $fecha = date("Y-m-d H:i:s", time());

            $tipocam = TipoCambio::find()->where(['idTipo_cabio'=>$idTipo_cabio])->one();
            $tipocam->idTipo_moneda = $categoriatipocambio->idTipo_moneda;
            $tipocam->idCategoria = $idCategoria;
            $tipocam->fecha_hora = $fecha;
            $tipocam->valor_tipo_cambio = $valor_tipo_cambio;
            $tipocam->save();
        }
    }
     //funcion que me elimina el tipo de cambio seleccionado
    public function actionElimina_tcambio()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_cabio = Yii::$app->request->post('idTipo_cabio');
            \Yii::$app->db->createCommand()->delete('tbl_tipo_cambio', 'idTipo_cabio = '.(int) $idTipo_cabio)->execute();
        }
    }

    //funcion para poner el valor de tipo de cambio por defecto
    public function actionValorcambiousar()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_cabio = Yii::$app->request->post('idTipo_cabio');
            $tipocambios = TipoCambio::find()->all();
            foreach($tipocambios as $tipcam) { //borro todo en la columna usar para luego obtener el valor a usar
                $tipcam->usar = '';
                $tipcam->save();
            }
            $tp = TipoCambio::find()->where(['idTipo_cabio'=>$idTipo_cabio])->one();//asigno valor a usar
            $tp->usar = 'x';
            $tp->save();
        }
    }

//-----------------------------------------------------------------------MODULO TIPO DOCUMENTO

    //agrega EL TIPO DE DOCUMENTO a base de datos
    public function actionAgrega_documento()
    {
        $tipodoc = new TipoDocumento();
        if(Yii::$app->request->isAjax){
            $descripcion = Yii::$app->request->post('descripcion');
            $tipo_movimiento = Yii::$app->request->post('tipo');
            $estado = Yii::$app->request->post('estado');

            $tipodoc->descripcion = $descripcion;
            $tipodoc->tipo_movimiento = $tipo_movimiento;
            $tipodoc->estado = $estado;
            $tipodoc->save();
        }
    }

    //obtiene los datos del tipo documento seleccionado en base de datos para actualizar
    public function actionActualiza_modal_documento()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_documento = Yii::$app->request->post('idTipo_documento');
            $tipodoc = TipoDocumento::find()->where(['idTipo_documento'=>$idTipo_documento])->one();
            $arr = array('idTipo_documento'=>$tipodoc->idTipo_documento, 'descripcion'=>$tipodoc->descripcion, 'tipo_movimiento'=>$tipodoc->tipo_movimiento, 'estado'=>$tipodoc->estado);
            echo json_encode($arr);
        }
    }

     //actualiza los datos en base de datos editados en la modal de actualizacion de tipo de documento
    public function actionActualizar_documento()
    {
        if(Yii::$app->request->isAjax){
            $idTipo_documento = Yii::$app->request->post('idTipo_documento');
            $descripcion = Yii::$app->request->post('descripcion');
            $tipo = Yii::$app->request->post('tipo');
            $estado = Yii::$app->request->post('estado');
            $categoriatipoc = TipoDocumento::find()->where(['idTipo_documento'=>$idTipo_documento])->one();
            $categoriatipoc->idTipo_documento = $idTipo_documento;
            $categoriatipoc->descripcion = $descripcion;
            $categoriatipoc->tipo_movimiento = $tipo;
            $categoriatipoc->estado = $estado;
            $categoriatipoc->save();
        }
    }
    //funcion que me elimina la categorria seleccionada
    public function actionElimina_documento()
    {
      if(Yii::$app->request->post())
      {

        $id = Html::encode($_POST["idTipo_documento"]);

        if($id)
        {
          \Yii::$app->db->createCommand()->delete('tbl_tipo_documento', 'idTipo_documento = '.(int) $id)->execute();
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Éxito!</strong><br><br> El Tipo de documento se ha eliminado correctamente.');
          return $this->redirect(['index','documento']);
        }
      }
    }
}
