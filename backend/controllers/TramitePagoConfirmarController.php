<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use backend\models\TramitePagoConfirmar;
use backend\models\TramitePago;
use backend\models\search\TramitePagoConfirmarSearch;
use backend\models\MedioPagoProveedor;
use backend\models\Proveedores;
use backend\models\CuentasBancarias;
use backend\models\CuentasBancariasProveedor;
use backend\models\DebitoSession;
use backend\models\ComprasInventario;
use backend\models\MovimientoPagar;
use backend\models\ContactosPagos;
use backend\models\Moneda;
use backend\models\TipoCambio;
use backend\models\TipoDocumento;
use backend\models\MovimientoLibros;
use backend\models\Empresa;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\filters\AccessControl;
/**
 * TramitePagoConfirmarController implements the CRUD actions for TramitePagoConfirmar model.
 */
class TramitePagoConfirmarController extends BaseController
{
  public function behaviors()
  {
      return [
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
              [
                'actions' => ['index','view','create','update','delete','confirmar',
                'listatramitecancelar','listatramiteconfirmar',
                'buscar_tramite_cancelar','buscar_tramite_confirmar',
                'reasignar_prioridad_tramites','inyectardatostramite',
                'tabla_facturas','reversar_tramite',
                'culminar_tramite_primero','modal_pagar_tramite',
                'valida_documento_banco','cancelar_tramite_pendiente',
                'devolver_tramite','aplicar_tramite_pendiente',
                'anular_tramite'],
                'allow' => true,
                'roles' => ['@'],
              ]
            ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all TramitePagoConfirmar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TramitePagoConfirmarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionConfirmar()
    {
      return $this->render('confirmar', []);
    }

    /**
     * Displays a single TramitePagoConfirmar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TramitePagoConfirmar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TramitePagoConfirmar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idTramite_pago]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TramitePagoConfirmar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idTramite_pago]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TramitePagoConfirmar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TramitePagoConfirmar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TramitePagoConfirmar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TramitePagoConfirmar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //me trae el html que contiene la lista de tramites por confirmar
    public function actionListatramitecancelar()
    {
        return $this->renderAjax('listatramitecancelar', [
            'id_tramite' => null, 'fecha_busqueda' => '',
            'idProveedor' => null, 'prioridad' => '']);
    }

    //me trae el html que contiene la lista de tramites por confirmar
    public function actionListatramiteconfirmar()
    {
        return $this->renderAjax('listatramiteconfirmar', [
            'id_tramite' => null, 'fecha_busqueda' => '',
            'idProveedor' => null, 'prioridad' => '']);
    }

    //funcion que me filtra los tramites por confirmar
    public function actionBuscar_tramite_cancelar()
    {
      $id_tramite = $_GET['id_tramite'] == '' ? null : (int)$_GET['id_tramite'];
      $fecha_busqueda = $_GET['fecha_busqueda'] == '' ? '' : date('Y-m-d', strtotime( $_GET['fecha_busqueda'] ));
      $id_proveedor  = $_GET['id_proveedor'] == '' ? null :  (int)$_GET['id_proveedor'];
      $prioridad = $_GET['prioridad'];

      echo $this->renderAjax('listatramitecancelar', [
          'id_tramite' => $id_tramite, 'fecha_busqueda' => $fecha_busqueda,
          'idProveedor' => $id_proveedor, 'prioridad' => $prioridad]);
    }

    //funcion que me filtra los tramites por confirmar
    public function actionBuscar_tramite_confirmar()
    {
      $id_tramite = $_GET['id_tramite'] == '' ? null : (int)$_GET['id_tramite'];
      $fecha_busqueda = $_GET['fecha_busqueda'] == '' ? '' : date('Y-m-d', strtotime( $_GET['fecha_busqueda'] ));
      $id_proveedor  = $_GET['id_proveedor'] == '' ? null :  (int)$_GET['id_proveedor'];
      $prioridad = $_GET['prioridad'];

      echo $this->renderAjax('listatramiteconfirmar', [
          'id_tramite' => $id_tramite, 'fecha_busqueda' => $fecha_busqueda,
          'idProveedor' => $id_proveedor, 'prioridad' => $prioridad]);
    }

    //funcion que me cambia la prioridad de los tramites pendientes
    public function actionReasignar_prioridad_tramites(){
        if(Yii::$app->request->isAjax){
            $checkboxValues  = Yii::$app->request->post('checkboxValues');
            $prioridad_pago  = Yii::$app->request->post('prioridad');
            $array = explode(",", $checkboxValues);
            $longitud = count($array);
            if ($checkboxValues) {
                for($i=0; $i<$longitud; $i++) {
                    $tramite = TramitePago::find()->where(['=','idTramite_pago', $array[$i]])->one();
                    $tramite->prioridad_pago = $prioridad_pago;
                    $tramite->save();
                }
                echo '<br><br><br><center><span class="label label-success" style="font-size:15pt;">La prioridad de los tramites seleccionados fue modificada con exito</span></center>';
            } else echo '<br><br><br><center><span class="label label-info" style="font-size:15pt;">No se ha obtenido resultados, marque al menos una casilla</span></center>';

        }
        //Yii::$app->response->redirect(array('tramite-pago/index'));
    }

    //inyecta los datos de consulta de tramite que quiero ver al seleccionar uno de la lista
    public function actionInyectardatostramite()
    {
      if(Yii::$app->request->isAjax){
        $idTramite = Yii::$app->request->post('id');
        $idProv = Yii::$app->request->post('idProv');
        $tramite = TramitePagoConfirmar::findOne($idTramite);
        $proveedor = Proveedores::findOne($idProv);
        $mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite])->one();
        $cuentbanpro = CuentasBancariasProveedor::findOne($mediopago->cuenta_bancaria_proveedor);
        $documento_banco = $tramite->documento_banco=='' ? 'no' : 'si';
        $arr = array('proveedor'=>$proveedor->nombreEmpresa, 'entidad_financiera'=>$cuentbanpro->entidad_bancaria,
            'dcp_cu_ba_pr' => $cuentbanpro->descripcion, 'cuent_ba_pr' => $cuentbanpro->cuenta_bancaria,
            'total_cancelar' => $tramite->monto_tramite_pagar, 'documento_banco'=>$documento_banco);
        echo json_encode($arr);
      }
    }

    //llamo la tabla de notas de credito por tramite desde ajax
    public function actionTabla_facturas()
    {
        $idTramite_pago = $_GET['id'];
        $idProv = $_GET['idProv'];
        return $this->renderAjax('tabla_facturas', ['idTramite_pago' => $idTramite_pago, 'idProv' => $idProv ]);
    }

    //esta funcion me reversa el tramite seleccionado para editarlo
    public function actionReversar_tramite()
    {
      if(Yii::$app->request->isAjax){
        //si hay un tramite cargado en session regresar al index con la notificacion de que culmite el tramite pendiente de modificar
        if (@$idTramite_pago = DebitoSession::getTramiteupdate()) {
          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span>
          <font size=4><strong>Error!</strong><br>Existe un trámite en modificación, asegúrese de culminar el
          proceso pendiente antes de hacer un nuevo retroceso.</font> ' . Html::a('Culminar proceso pendiente', ['culminar_tramite_primero'], ['class' => 'btn btn-default']));
          return $this->redirect(['index']);
        } //de lo contrario proceda con la revercion del tramite
        else {
          $idTramite_pago  = Yii::$app->request->post('idTramite_pago');
          $tramite = TramitePago::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
          $modelproveedor = Proveedores::find()->where(['=','codProveedores', $tramite->idProveedor])->one();
          $facturas_compra = ComprasInventario::find()->where(['=','idTramite_pago', $idTramite_pago])->all();
          $tramite->estado_tramite = 'Edición';
          if ($tramite->save()) {
            $saldofacturas = 0;
            foreach ($facturas_compra as $factura_proces => $value) {

                $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
                $factura_proces->estado_pago = 'Proceso';
                $factura_proces->idTramite_pago = null;
                $saldofacturas += $factura_proces->credi_saldo;//sumo los saldos de la factura
                $factura_proces->save();//guardamos cambios en la factura de compra
            }
            /*$modelproveedor->credi_saldo += $saldofacturas;//modificamos (sumamos) el saldo del proveedor con las facturas devueltas al ser rebersado el tramite
            $modelproveedor->save();*/
            //buscamos si hay notas de creditos asignadas al trámite
            $movi_pagar = MovimientoPagar::find()->where(['idTramite_pago'=>$idTramite_pago])->all();
            //Si hay notas de creditos asignarle las notas que se le aplico a la transferencia
            if (@$movi_pagar) {
                foreach ($movi_pagar as $tramite => $mov) {
                    $tramite = MovimientoPagar::find()->where(['=','idMov', $mov['idMov']])->one();
                    $tramite->idTramite_pago = 0;
                    $tramite->save();
                }
            }
          }
          DebitoSession::setIDproveedor($modelproveedor->codProveedores);//Cargo en session el id del proveedor
          DebitoSession::setProveedor($modelproveedor->nombreEmpresa);//Cargo en session el nombre del proveedor
          DebitoSession::setIDproveedor_tramite_upd($modelproveedor->codProveedores);//Cargo en session el id del proveedor que esta pendiente para la actualizacion del tramite
          DebitoSession::setTramiteupdate($idTramite_pago);//cargo en session el tramite
          //DebitoSession::setTramiteupdate(null);
          Yii::$app->getSession()->setFlash('info', '<center><font face="arial" size=5><span class="glyphicon glyphicon-flag"></span><strong>Atención!</strong> Diríjase al área de <strong>"Generar pago"</strong> para modificar el trámite cargado.</font></center>');
          return $this->redirect(['tramite-pago/index']);
        }
      }
    }

    //esta funcion me permite ir en busca del tramite pendiente cargado para modificar
    public function actionCulminar_tramite_primero()
    {
      Yii::$app->getSession()->setFlash('info', '<center><font face="arial" size=5><span class="glyphicon glyphicon-flag"></span><strong>Atención!</strong> Diríjase al área de <strong>"Generar pago"</strong> para modificar el trámite cargado.</font></center>');
      return $this->redirect(['tramite-pago/index']);
    }

    //obtiene los datos del tramite seleccionado en base de datos para mostrar en modal antes de la cancelacion
    public function actionModal_pagar_tramite()
    {
        if(Yii::$app->request->isAjax){
            $idTramite_pago = Yii::$app->request->post('idTramite_pago');
            $tramite = TramitePago::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
            $proveedor = Proveedores::findOne($tramite->idProveedor);
            $mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
            $cuentbanloc = CuentasBancarias::find()->where(['idBancos'=>$mediopago->cuenta_bancaria_local])->one();
            $cuentbanpro = CuentasBancariasProveedor::findOne($mediopago->cuenta_bancaria_proveedor);
            $contacto = ContactosPagos::findOne($mediopago->idContacto_pago);
            $observaciones = $tramite->observaciones == '' ? 'NA' : $tramite->observaciones;
            $documento_banco = $tramite->documento_banco == '' ? '' : $tramite->documento_banco;
            $arr = array( 'no_tramite'=>$tramite->idTramite_pago,
                          'fecha_registra'=>date('d-m-Y', strtotime( $tramite->fecha_registra )),
                          'proveedor'=>$proveedor->nombreEmpresa,
                          'monto_pagar'=>number_format($tramite->monto_tramite_pagar,2),
                          'tipo_movimiento'=>'Pago de proveedor',
                          'usuario_cancela'=>Yii::$app->user->identity->username,
                          'fecha_cancelacion'=>date('d-m-Y'),
                          'cuenta_bancaria_local'=>$cuentbanloc->numero_cuenta,
                          'observaciones'=>$observaciones,
                          'documento_banco'=>$documento_banco,
                          'estado_tramite'=>'Cancelado',
                          'cuenta_proveedor'=>$cuentbanpro->cuenta_bancaria,
                          'contacto'=>$contacto->nombre,
                          'e_contacto'=>$contacto->email
                        );
            echo json_encode($arr);
        }
    }

    //esta funcion me permite comprovar que no exista un documento de banco que esté repetido
    public function actionValida_documento_banco()
    {
      if(Yii::$app->request->isAjax){
          $doc_banco = Yii::$app->request->post('doc_banco');
          $idTramite_pago = Yii::$app->request->post('idTramite_pago');
          $mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
          $cuenta_escrita = CuentasBancarias::find()->where(['idBancos'=>$mediopago->cuenta_bancaria_local])->one();
          $pasa = 'si';
          $tramites = TramitePago::find()->all();
          foreach ($tramites as $key => $tramite) {
              $mediopago_consulta = MedioPagoProveedor::find()->where(['idTramite_pago'=>$tramite['idTramite_pago']])->one();
              $cuenta_consultada = CuentasBancarias::find()->where(['idBancos'=>$mediopago_consulta->cuenta_bancaria_local])->one();
              $movilibros = MovimientoLibros::find()->where(['idCuenta_bancaria'=>$mediopago_consulta->cuenta_bancaria_local])->andWhere("numero_documento = :numero_documento", [":numero_documento"=>(int) $doc_banco])->one();
              if (@$movilibros) {//...po lo que notifico imprimiendo repetido
                  $pasa = 'no';
              } else if ( $tramite['documento_banco'] == $doc_banco && $cuenta_consultada->idEntidad_financiera == $cuenta_escrita->idEntidad_financiera ) {
                $pasa = 'no';
                if ( $tramite['idTramite_pago'] == $idTramite_pago ) {
                  $pasa = 'si';
                }//fin if
              }//fin if
            }
          echo $pasa;
        }
    }

    //esta funcion me permite cancelar el tramite, hacer el pago, enviarlo por email y modificar los datos en movimiento en libros.
    public function actionCancelar_tramite_pendiente()
    {
      if(Yii::$app->request->isAjax){
        $no_tramite = Yii::$app->request->post('no_tramite');
        $doc_banco = Yii::$app->request->post('doc_banco');
        $observaciones = Yii::$app->request->post('observaciones');
        $tramite = TramitePago::find()->where(['idTramite_pago'=>$no_tramite])->one();

        $tramite->usuario_cancela = Yii::$app->user->identity->username;
        $tramite->fecha_cancela = date("Y-m-d H:i:s");
        $tramite->documento_banco = $doc_banco;
        $tramite->observaciones = $observaciones;
        $tramite->estado_tramite = 'Precancelado';
        if ($tramite->save()) {
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Trámite cancelado, pendiente de aplicar.');
          return $this->redirect(['index']);
        }//fin $tramite->save()
      }//fin ajax
    }//fin de la funcion de cancelar tramite pendiente

    //enviar tramite al correo desde una llamada a la funcion
    public function enviar_tramite_correo($id)
    {
        $empresaimagen = new Empresa();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $tramite = TramitePago::find()->where(['idTramite_pago'=>$id])->one();//$this->findModel($id);
            $medio_pago_proveedor = MedioPagoProveedor::find()->where(['=','idTramite_pago', $tramite->idTramite_pago])->one();
            $contactopago = ContactosPagos::find()->where(['=','idContacto_pago', $medio_pago_proveedor->idContacto_pago])->one();

            if (@$contactopago) {
                $proveedor = Proveedores::find()->where(['codProveedores'=>$tramite->idProveedor])->one();
                if (@$contactopago->email && $proveedor->notific_cntc_pagos == 'Si') {

                $email = $contactopago->email;

                $informacion_de_tramite = $this->renderAjax('correofacturasdetramite',['tramite' => $tramite]);
                $logo_sistema = Yii::$app->params['tipo_sistema'] == 'lutux' ? 'logo_sistema.png' : 'logo_sistema2.png';
                $content = '
                <div id=":1x2" class="ii gt m1508a9e3a80e6f41 adP adO"><div id=":1x1" class="a3s" style="overflow: hidden;"><u></u>
                <div style="background:#f0f0f0;margin:0;padding:10px 0" bgcolor="#f0f0f0">
                <br>
                <table class="text-justify" border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0">
                  <tbody><tr>
                    <td align="center" valign="top">
                      <table width="730" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom-color:#e0e0e0;border-bottom-style:solid;border-bottom-width:1px;color:#434343;font-family: Segoe UI">
                        <tbody><tr>
                            <td style="background:#4169E1;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                              <table>
                                <tbody><tr>
                                  <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px">
                                    &nbsp;&nbsp;&nbsp;&nbsp;

                                  </td>
                                  <td style="border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;width:320px">
                                  </td>
                                </tr>
                              </tbody></table>
                            </td>
                        </tr>
                        <tr>
                          <td bgcolor="#ffffff" style="background:#ffffff;border-left-color:#e0e0e0;border-left-style:solid;border-left-width:0px;padding-left:30px;padding-right:30px">
                           <br>
                            <table>
                                <tbody>
                                <tr>
                                <td>
                                <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                                </td>
                                <td style="text-align: left;">
                                <strong>'.$empresa->nombre.'</strong><br>
                                <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                                <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                                <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                                <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                                <strong>Dirección:</strong> '.$empresa->email.'
                                </td>
                                <tr>
                                </tbody>
                            </table>
                            <h4 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">ENVIO DEL TRAMITE # '.$id.'<br><small> '.date('d-m-Y').'&nbsp;&nbsp;</small></h4>

                            '.$informacion_de_tramite.'


                <br><br><br>

                          </td>
                        </tr>
                        <tr>
                          <td align="right" style="background:#4169E1;color:#e0e0e0;border-left-style:solid;border-left-width:0px;height:70px" bgcolor="#156997">
                            Emitido desde el sistema de facturación <a href="www.neluxps.com"><img width="70" height="" src="'.Url::base(true).'/img/'.$logo_sistema.'"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                          </td>
                        </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody></table>
                <br></div></div><div class="adL">

                </div></div></div> ';
                //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                $subject = "Tramite de pago de ".$empresa->nombre;
                //Enviamos el correo
                Yii::$app->mailer->compose("@common/mail/layouts/html", ["content"=>$content])
                ->setTo($email)//para
                ->setFrom($empresa->email_contabilidad)//de
                ->setSubject($subject)
                ->setHtmlBody($content)
                ->send();
                return 'true';
                }
                else {
                    return 'false';
                }
            } else {
                return 'false';
            }//fin @$contactopago
            //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El trámite fue enviado correctamente.');
            //Yii::$app->response->redirect(array('tramite-pago/index'));
    }

    //devuelvo un tramite para volver a cancelar
    public function actionDevolver_tramite($idTramite_pago)
    {
      $tramite = TramitePago::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
      $tramite->estado_tramite = 'Pendiente';
      $tramite->save();
      Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El trámite se ha devuelto.');
      return $this->redirect(['index']);
    }

    //esta funcion me permite aplicar el tramite que esta en estrado Precancelado
    public function actionAplicar_tramite_pendiente(){
      if(Yii::$app->request->isAjax){
        $idTramite_pago = Yii::$app->request->post('idTramite_pago');
        //habro una intancia para saber el nombre de la casa comercial
        $tramite = TramitePago::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
        $model_mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
        $proveedor = Proveedores::find()->where(['codProveedores'=>$tramite->idProveedor])->one();
        $model_mediopago->saldo_proveedor = $proveedor->credi_saldo;//almacenamos el saldo del proveedor como historial
        $model_mediopago->numero_recibo_cancelacion = $tramite->documento_banco;
        $proveedor->credi_saldo -= $tramite->monto_saldo_pago;//actualizamos el saldo del proveedor
        $model_mediopago->fecha_documento = date("Y-m-d");
        $model_mediopago->saldo_proveedor = $proveedor->credi_saldo;
        $proveedor->save();
        //creamos una instancia solo para las compras cuyo id pertenecen al tramite
        $facturas_proceso = ComprasInventario::find()->where(['=','idTramite_pago', $tramite->idTramite_pago])->all();
        foreach ($facturas_proceso as $factura_proces => $value) {
            $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
            $factura_proces->estado_pago = 'Cancelada';
            $factura_proces->idTramite_pago = $tramite->idTramite_pago;
            $factura_proces->credi_saldo = 0.00;
            $factura_proces->save();//guardamos cambios en la factura de compra
        }
        $cuenta_bancaria_local = $model_mediopago->cuenta_bancaria_local;//asignamos la cuenta bancaria del tramite (ID) a una variable que necesitamos para movimiento en libros
        $idTipo_documento = $model_mediopago->idTipo_documento;
        $idMedio_pago = $model_mediopago->idMedio_pago;
        $numero_recibo_cancelacion = $model_mediopago->numero_recibo_cancelacion;
        if ($model_mediopago->save()) {
          //habro una intancia para saber el nombre de la casa comercial
          //$proveedor = Proveedores::find()->where(['=','codProveedores', $tramite->idProveedor])->one();
          //habro una instancia para obtener info de la cuenta bancaria que estoy usando
          $cuenta = CuentasBancarias::find()->where(['idBancos'=>$cuenta_bancaria_local])->one();
          //habro una instancia para obtener el tipo de moneda de la cuenta con la estoy usando
          $moneda = Moneda::find()->where(['idTipo_moneda'=>$cuenta->idTipo_moneda])->one();
          //habro una instancia para obtener el tipo de cambio y compararla con la moneda que ya tengo de la cuenta
          $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();

          //abrimos una intancia de movimiento libros donde se procese a crear un movimiento automatizado
          $model_movi_libros = new MovimientoLibros();
          $model_movi_libros->idCuenta_bancaria = $cuenta_bancaria_local;
          $model_movi_libros->idTipoMoneda = $cuenta->idTipo_moneda;
          $model_movi_libros->idTipodocumento = $model_mediopago->idTipo_documento;
          $model_movi_libros->idMedio_pago = $model_mediopago->idMedio_pago;
          $model_movi_libros->numero_documento = $model_mediopago->numero_recibo_cancelacion;
          $model_movi_libros->fecha_documento = date("Y-m-d");//$model_mediopago->fecha_documento;
          $model_movi_libros->monto = $tramite->monto_tramite_pagar;
          $model_movi_libros->tasa_cambio = null;
          $model_movi_libros->monto_moneda_local = 0.00;
          //con esto compruebo si hay que hacer tipo cambio.
          if ($moneda->monedalocal!='x') {
              $model_movi_libros->tasa_cambio = $tipocambio->valor_tipo_cambio;
              $model_movi_libros->monto_moneda_local = $tramite->monto_tramite_pagar*$tipocambio->valor_tipo_cambio;
          }
          $model_movi_libros->beneficiario = $proveedor->nombreEmpresa;
          $model_movi_libros->detalle = 'PAGO DE FACTURAS CON TRANSFERENCIA BANCARIA';
          $model_movi_libros->fecha_registro = date("Y-m-d");
          $model_movi_libros->usuario_registra = Yii::$app->user->identity->nombre;
          //guardamos el movimiento en libros, si esto es asi...
          if ($model_movi_libros->save()) {
              $tramite->estado_tramite = 'Cancelado';
              $tramite->usuario_aplica = Yii::$app->user->identity->username;
              $tramite->fecha_aplica = date("Y-m-d H:i:s");
              $tramite->save();
              //...Debemos modificar el monto de cuentas bancarias correspondiendo al movimiento, en ese caso pago a proveedor
              ////habrimos instancia para guardar en cuentas bancarias correspondioente al movimiento en libros
              $modelcuentabancaria = CuentasBancarias::find()->where(['idBancos'=>$cuenta_bancaria_local])->one();
              //como el tipo de movimiento es debito el saldo en libros de cuenta bancaaria es igual a su misma cantidad menos el monto del nuevo movimiento en debito
              $modelcuentabancaria->saldo_libros -= $model_movi_libros->monto;

              //guardamos actualizacion en cuenta bancaria
              if ($modelcuentabancaria->save()) {//y modificamos el saldo en libros para el control de saldo por movimiento
                  $libros = MovimientoLibros::find()->where(['idMovimiento_libro'=>$model_movi_libros->idMovimiento_libro])->one();
                  $libros->saldo = $modelcuentabancaria->saldo_libros;//porque es hasta aqui que el saldo en libros lleva este monto
                  $libros->save();
              }
              Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> El trámite de pago se canceló correctamente');
              //comprovamos si se puede o no enviar la cancelacion del tramite por correo
              if ($this->enviar_tramite_correo($idTramite_pago)=='true') {
                  Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Trámite enviado al contacto de pago.');
              } else {
                  Yii::$app->getSession()->setFlash('info', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Observación!</strong> No se notificó por correo porque no se encontró la dirección del destinatario. Compruebe la dirección de correo o active las notificaciones.');
              }
          }//fin $model_movi_libros->save
          else {
              Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong> NO se pudo guardar en libros.');
          }//fin else
          //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Cancelación de trámite aplicada.');
          return $this->redirect(['confirmar']);
        }//fin $model_mediopago->save()



      }
    }

    //funcion para anular el tramite
    public function actionAnular_tramite()
    {
      if(Yii::$app->request->isAjax){
          $idTramite_pago  = Yii::$app->request->post('idTramite_pago');
          $detalle_anulacion = Yii::$app->request->post('detalle_anulacion');
          $codProveedores = Yii::$app->request->post('idProv');
          $tramite = TramitePago::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
          $mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
          $mediopago->numero_recibo_cancelacion = '';
          $modelproveedor = Proveedores::find()->where(['=','codProveedores', $codProveedores])->one();
          $facturas_compra = ComprasInventario::find()->where(['=','idTramite_pago', $idTramite_pago])->all();
          $tramite->estado_tramite = 'Anulado';
          $tramite->documento_banco = '';
          $tramite->detalle_anulacion = $detalle_anulacion;
          if ($tramite->save()) {
            $mediopago->save();
            $saldofacturas = 0;
            foreach ($facturas_compra as $factura_proces => $value) {
                $factura_proces = ComprasInventario::find()->where(['=','idCompra', $value['idCompra']])->one();
                $factura_proces->estado_pago = 'Proceso';
                $factura_proces->idTramite_pago = null;
                $saldofacturas += $factura_proces->credi_saldo;//sumo los saldos de la factura
                $factura_proces->save();//guardamos cambios en la factura de compra
            }
            /*$modelproveedor->credi_saldo += $saldofacturas;//modificamos (sumamos) el saldo del proveedor con las facturas devueltas al ser cancelado el tramite
            $modelproveedor->save();*/
            //buscamos si hay notas de creditos asignadas al trámite
            $movi_pagar = MovimientoPagar::find()->where(['idTramite_pago'=>$idTramite_pago])->all();
            //Si hay notas de creditos asignarle las notas que se le aplico a la transferencia
            if (@$movi_pagar) {
                foreach ($movi_pagar as $tramite => $mov) {
                    $tramite = MovimientoPagar::find()->where(['=','idMov', $mov['idMov']])->one();
                    $tramite->idTramite_pago = 0;
                    $tramite->save();
                }
            }
          }
          //DebitoSession::setTramiteupdate(null);
          Yii::$app->getSession()->setFlash('info', '<center><font face="arial" size=5><span class="glyphicon glyphicon-flag"></span><strong>Atención!</strong> El trámite se anuló correctamente, y las facturas están listas para un nuevo trámite.</font></center>');
          return $this->redirect(['index']);
      }//fin ajax
    }//fin funcion
}
