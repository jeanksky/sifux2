<?php

namespace backend\controllers;
use backend\models\Proveedores;
use backend\models\Moneda;
use backend\models\TipoCambio;
use Yii;
use yii\web\UploadedFile;
use backend\models\ComprasInventario;
use backend\models\search\ComprasInventarioSearch;
use backend\models\ProductoServicios;
use backend\models\ProductoCatalogo;
use backend\models\ProductoProveedores;
use backend\models\Funcionario;
use backend\models\DetalleOrdenCompra;
use backend\models\CompraInventarioSession;
use backend\models\DetalleCompra;
use backend\models\OrdenCompraProv;
use backend\models\Notificar_compra;
use backend\models\HistorialProducto;
use backend\models\RecepcionHacienda;
use backend\models\ProductoPedido;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use backend\models\Empresa;
use backend\models\Etiquetas;
use backend\models\Factun;
use kartik\mpdf\Pdf;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
//use Picqer\Barcode;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;
require("mpdf/mpdf.php");
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
use yii\filters\AccessControl;
/**
 * ComprasInventarioController implements the CRUD actions for ComprasInventario model.
 */
class ComprasInventarioController extends BaseController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => [
          'index','view','create','update','delete','cancelaru','sumafecha','desactivarsession', 'run_producto_input',
          'desactivarsessionu', 'obtenerprecioventa', 'obtenerutilidadimpu','obtenerventauti','agrega_modalprod','agrega_modalprod_update','view',
          'actualiza_modalprod_listo','actualiza_modalprod_listo_update','actualiza_modalprod','actualiza_modalprod_update','deleteitem','deleteitem_update',
          'run_producto', 'run_producto_air', 'agrega_prod_inv', 'convertir_dolar','agrega_prod_inv_update', 'busqueda_producto', 'cargar_compra', 'cargar_compra_bd',
          'guardarcompra', 'aplicarcompra', 'report', 'etiquetas','run_producto_etiqueta', 'run_agrega_etiqueta', 'deleteetiqueta',
          'actualiza_modaletiq', 'agrega_compra_etiqueta', 'obtener_productos_marcados_create', 'obtener_productos_marcados_update', 'renviar_xml_not',
          'actualiza_modaletiq_listo', 'obtener_etiquetas_marcadas', 'eliminar_etiquetas_marcadas','obtenermontoflete', 'obtenermontoflete_u', 'enviar_xml_not', 'pdf_resolucion'
        ],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete','cancelaru','sumafecha','desactivarsession', 'run_producto_input',
            'desactivarsessionu', 'obtenerprecioventa', 'obtenerutilidadimpu','obtenerventauti','agrega_modalprod','agrega_modalprod_update','view',
            'actualiza_modalprod_listo','actualiza_modalprod_listo_update','actualiza_modalprod','actualiza_modalprod_update','deleteitem','deleteitem_update',
            'run_producto', 'run_producto_air', 'agrega_prod_inv', 'convertir_dolar','agrega_prod_inv_update', 'busqueda_producto', 'cargar_compra', 'cargar_compra_bd',
            'guardarcompra', 'aplicarcompra', 'report', 'etiquetas','run_producto_etiqueta', 'run_agrega_etiqueta', 'deleteetiqueta',
            'actualiza_modaletiq', 'agrega_compra_etiqueta', 'obtener_productos_marcados_create', 'obtener_productos_marcados_update', 'renviar_xml_not',
            'actualiza_modaletiq_listo', 'obtener_etiquetas_marcadas', 'eliminar_etiquetas_marcadas','obtenermontoflete', 'obtenermontoflete_u', 'enviar_xml_not', 'pdf_resolucion'
          ],
          'allow' => true,
          'roles' => ['@'],
        ]
      ],
    ],
    'verbs' => [
      'class' => VerbFilter::className(),
      'actions' => [
        'delete' => ['post'],
      ],
    ],
  ];
}

    public function borrartodosession(){
        CompraInventarioSession::setFormapago(null);
        CompraInventarioSession::setFechadocumento(null);
        CompraInventarioSession::setFecharegistro(null);
        CompraInventarioSession::setFechavencimiento(null);
        CompraInventarioSession::setProveedor(null);
        CompraInventarioSession::setNofactura(null);
        CompraInventarioSession::setSubtotal(null);
        CompraInventarioSession::setInterno_prov(null);
        CompraInventarioSession::setDescuento(null);
        CompraInventarioSession::setImpuesto(null);
        CompraInventarioSession::setTotal(null);
        CompraInventarioSession::setOrdenCompra(null);
    }
    public function borrartodosessionu(){
        CompraInventarioSession::setFormapagoup(null);
        CompraInventarioSession::setFechadocumentoup(null);
        CompraInventarioSession::setFecharegistroup(null);
        CompraInventarioSession::setFechavencimientoup(null);
        CompraInventarioSession::setProveedorup(null);
        CompraInventarioSession::setNofacturaup(null);
        CompraInventarioSession::setInterno_provup(null);
        CompraInventarioSession::setSubtotalup(null);
        CompraInventarioSession::setDescuentoup(null);
        CompraInventarioSession::setImpuestoup(null);
        CompraInventarioSession::setTotalup(null);

    }

    public function borrartodosession_air(){
        CompraInventarioSession::setProductoAir(null);
        CompraInventarioSession::setPrecioAir(null);
        CompraInventarioSession::setUtilidadAir(null);
        CompraInventarioSession::setVentaAir(null);
        CompraInventarioSession::setVentaImAir(null);
    }
    /**
     * Lists all ComprasInventario models.
     * @return mixed
     */
    public function actionIndex()
    {
        CompraInventarioSession::setactualizando(null);
        $searchModel = new ComprasInventarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCancelaru($id)
    {
        CompraInventarioSession::setactualizando(null);
        return $this->redirect(['update', 'id' => $id, 'carga'=>false]);
    }

    /**
     * Displays a single ComprasInventario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ComprasInventario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ComprasInventario();
        if ($model->load(Yii::$app->request->post()) && $model->validate()/* && $model->save()*/) {
           // return $this->redirect(['view', 'id' => $model->idCompra]);
            $descuento = '0.00';
            if ($model->descuento != null && $model->descuento != '' && floatval($model->descuento)>0) {
              $descuento = floatval(str_replace(",", "", $model->descuento));
            }

            $subTotal = floatval($model->subTotal) > 0 ? floatval(str_replace(",", "", $model->subTotal)) : '0.00';
            $impuesto = floatval($model->impuesto) > 0 ? floatval(str_replace(",", "", $model->impuesto)) : '0.00';
            $total = floatval($model->total) > 0 ? floatval(str_replace(",", "", $model->total)) : '0.00';
            $n_interno_prov = $model->n_interno_prov == '' ? '-' : $model->n_interno_prov;
            $this->borrartodosession();
            CompraInventarioSession::setFormapago($model->formaPago);
            CompraInventarioSession::setFechadocumento($model->fechaDocumento);
            CompraInventarioSession::setFecharegistro($model->fechaRegistro);
            CompraInventarioSession::setFechavencimiento($model->fechaVencimiento);
            CompraInventarioSession::setProveedor($model->idProveedor);
            CompraInventarioSession::setNofactura($model->numFactura);
            CompraInventarioSession::setInterno_prov($n_interno_prov);
            CompraInventarioSession::setSubtotal($subTotal);
            CompraInventarioSession::setDescuento($descuento);
            CompraInventarioSession::setImpuesto($impuesto);
            CompraInventarioSession::setTotal($total);

            return $this->redirect(['create', 'carga'=>false]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ComprasInventario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->credi_saldo = $model->formaPago=='Crédito' ? floatval(str_replace(",", "", $model->total)) : 0.00;
            $descuento = 0;
            if ($model->descuento != null && $model->descuento != '' && floatval($model->descuento)>0) {
              $descuento = floatval(str_replace(",", "", $model->descuento));
            }
            $model->descuento = $descuento;
            $model->subTotal = floatval($model->subTotal) > 0 ? floatval(str_replace(",", "", $model->subTotal)) : 0.00;
            $model->impuesto = floatval($model->impuesto) > 0 ? floatval(str_replace(",", "", $model->impuesto)) : 0.00;
            $model->total = floatval($model->total) > 0 ? floatval(str_replace(",", "", $model->total)) : 0.00;
            if ($model->save()) {
              CompraInventarioSession::setactualizando(null);
              return $this->redirect(['update', 'id' => $model->idCompra, 'carga'=>false]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ComprasInventario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ComprasInventario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ComprasInventario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ComprasInventario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionSumafecha()
    {
        if(Yii::$app->request->isAjax){
            $diascredito = 30;
            $fechaRegistro = Yii::$app->request->post('fechaR');
            $idProveedor = Yii::$app->request->post('idProveedor');
            $proveedor = Proveedores::find()->where(['codProveedores' => $idProveedor])->one();
            if ($proveedor->diasCredito > 0) {
                $diascredito = $proveedor->diasCredito;
            }
            $fechavenci = strtotime ( '+'.$diascredito.' day' , strtotime ( $fechaRegistro ) ) ;
            echo date ( 'j-m-Y' , $fechavenci );

        }
    }

    public function actionDesactivarsession()
    {
        /*if(Yii::$app->request->isAjax){
            //$formaPago = Yii::$app->request->post('formaPago');
        }*/
        $this->borrartodosession();
        CompraInventarioSession::setContenidoProducto(array());
        return $this->redirect(['create']);
    }

    public function actionDesactivarsessionu($id)
    {
        /*if(Yii::$app->request->isAjax){
            //$formaPago = Yii::$app->request->post('formaPago');
        }*/
        $this->borrartodosessionu();
        //CompraInventarioSession::setContenidoProducto(array());
        CompraInventarioSession::setactualizando('SI');
        return $this->redirect(['update', 'id'=>$id, 'carga'=>false]);
    }

    //convertir de dolar a moneda local
    public function actionConvertir_dolar()
    {
      if(Yii::$app->request->isAjax){
        $precio_dolar = Yii::$app->request->post('precio_dolar');
        $tipo_cambio = TipoCambio::find()->where(['usar'=>'x'])->one();
        $precioc = $tipo_cambio->valor_tipo_cambio * $precio_dolar;
        echo number_format($precioc,2);
      }
    }

    // obtiene precio de venta desde cualquier entrada de la compra
    public function actionObtenerprecioventa()
    {
      if(Yii::$app->request->isAjax){
        $precioc = floatval(str_replace(",", "", Yii::$app->request->post('precioc')));
        $porcentu = floatval(str_replace(",", "", Yii::$app->request->post('porcentu')));
        $descuento = floatval(str_replace(",", "", Yii::$app->request->post('descuento')));
        CompraInventarioSession::setDescuento_consecutivo($descuento);
        $impuesto = Yii::$app->params['porciento_impuesto'];
        $descto = $descuento/100;
        $utilidad = $porcentu/100;
        $iva = $impuesto / 100;
        $margen_precioc_descuento = $precioc * $descto;
        $precioc_cn_descuento = $precioc - $margen_precioc_descuento; //almaceno el precio de compra menos descuento
        $margen_precioc_utilidad = $precioc_cn_descuento * $utilidad;
        $precioc_cn_utilidad = $precioc_cn_descuento + $margen_precioc_utilidad; //almacena precio de compra (descuento incluido si es el caso) mas utilidad
        $margen_precio_impuesto = $precioc_cn_utilidad * $iva;
        $precio_cn_impuesto = $precioc_cn_utilidad + $margen_precio_impuesto;//almacena precio de compra (descuento incluido si es el caso mas utilidad) mas impuesto

        $arr = array('preciov'=>number_format($precioc_cn_utilidad,2),
                    'precioventaimpu' => number_format($precio_cn_impuesto,2));
        echo json_encode($arr);
      }//fin if ajax
    }//fin funcion actionObtenerprecioventa

    //obtiene desde el precio de venta sin impuesto el precio con impuesto, la utilidad
    public function actionObtenerutilidadimpu()
    {
      if(Yii::$app->request->isAjax){
        $precioc = floatval(str_replace(",", "", Yii::$app->request->post('precioc')));
        $preciov = floatval(str_replace(",", "", Yii::$app->request->post('preciov')));
        $descuento = floatval(str_replace(",", "", Yii::$app->request->post('descuento')));

        $precio_unit_gan = $descuento == null ? $preciov : $preciov * 100  / ( 100 - $descuento );//filtro el precio de unidad de ganancia si incluye descuento
        $utilidad_individual = $precio_unit_gan - $precioc;
        $subtotal_utilidad_indiv = $utilidad_individual / $precioc;
        $porcentage_utilidad = $subtotal_utilidad_indiv * 100;

        $impuesto = Yii::$app->params['porciento_impuesto'];
        $iva = $impuesto / 100;
        $margen_precio_impuesto = $preciov * $iva;
        $precio_cn_impuesto = $preciov + $margen_precio_impuesto;

        $arr = array('porcentu'=>number_format($porcentage_utilidad,2),
                     'precioventaimpu' => number_format($precio_cn_impuesto,2));
        echo json_encode($arr);
      }//fin if ajax
    }

    //obtiene desde el precio de venta con impuesto el precio sin impuesto y la utilidad
    public function actionObtenerventauti()
    {
      if(Yii::$app->request->isAjax){
        $precioc = floatval(str_replace(",", "", Yii::$app->request->post('precioc')));
        $precioventaimpu = floatval(str_replace(",", "", Yii::$app->request->post('precioventaimpu')));
        $descuento = floatval(str_replace(",", "", Yii::$app->request->post('descuento')));

        $impuesto = Yii::$app->params['porciento_impuesto'];
        $iva = $impuesto / 100;
        $preciov = $precioventaimpu / (1 + $iva);

        $precio_unit_gan = $descuento == null ? $preciov : $preciov * 100  / ( 100 - $descuento );//filtro el precio de unidad de ganancia si incluye descuento
        $utilidad_individual = $precio_unit_gan - $precioc;
        $subtotal_utilidad_indiv = $utilidad_individual / $precioc;
        $porcentage_utilidad = $subtotal_utilidad_indiv * 100;

        $arr = array('porcentu'=>number_format($porcentage_utilidad,2),
                     'preciov' => number_format($preciov,2));
        echo json_encode($arr);
      }//fin if ajax
    }

    //agrega producto desde la modal
    public function actionAgrega_modalprod(){
        $this->borrartodosession_air();
        $modeldetallecompra = new DetalleCompra();
        $c=null;
        $producto = CompraInventarioSession::getContenidoProducto();
       if ($modeldetallecompra->load(Yii::$app->request->post())) {
            $modeldetallecompra->precioCompra = floatval(str_replace(",", "", $modeldetallecompra->precioCompra));
            $modeldetallecompra->descuento = floatval(str_replace(",", "", $modeldetallecompra->descuento));
            $modeldetallecompra->porcentUtilidad = floatval(str_replace(",", "", $modeldetallecompra->porcentUtilidad));
            $modeldetallecompra->preciosinImp = floatval(str_replace(",", "", $modeldetallecompra->preciosinImp));
            $modeldetallecompra->precioconImp = floatval(str_replace(",", "", $modeldetallecompra->precioconImp));
            if($producto)
             foreach ($producto as $position => $product)
                {
                    if($product['codProdServicio']==$modeldetallecompra->codProdServicio)
                    {
                    //$producto[$position]['cantidad']+=$cantidad;
                    $c=1;
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Este producto ya ha sido agregado anteriormente.');
                    Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
                    }
                }
           // $_POST['noEntrada'] = $modeldetallecompra->noEntrada;
            //$_POST['nolinia'] =
            if($c==null){
                $_POST['codigo_proveedor'] = $modeldetallecompra->codigo_proveedor;
                $_POST['codProdServicio'] = $modeldetallecompra->codProdServicio;
                $_POST['precioCompra'] = $modeldetallecompra->precioCompra;
                $_POST['cantidadEntrada'] = $modeldetallecompra->cantidadEntrada;
                $_POST['descuentu'] = $modeldetallecompra->descuento;
                $_POST['exImpuesto'] = $modeldetallecompra->exImpuesto;
                $_POST['porcentUtilidad'] = $modeldetallecompra->porcentUtilidad;
                $_POST['preciosinImp'] = $modeldetallecompra->preciosinImp;
                $_POST['precioconImp'] = $modeldetallecompra->precioconImp;
                $_POST['usuario'] = $modeldetallecompra->usuario;
                $_POST['estado'] = $modeldetallecompra->estado;
                $_POST['modificado'] = 'No';
                $producto[] = $_POST;
            }
            CompraInventarioSession::setContenidoProducto($producto);
            Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
        }
    }

    public function actionAgrega_modalprod_update($id){
        $this->borrartodosession_air();
        $modeldetallecompra = new DetalleCompra();
        $c=null;
        //$producto = CompraInventarioSession::getContenidoProducto();
       if ($modeldetallecompra->load(Yii::$app->request->post())) {
         $modeldetallecompra->precioCompra = floatval(str_replace(",", "", $modeldetallecompra->precioCompra));
         $modeldetallecompra->descuento = floatval(str_replace(",", "", $modeldetallecompra->descuento));
         $modeldetallecompra->porcentUtilidad = floatval(str_replace(",", "", $modeldetallecompra->porcentUtilidad));
         $modeldetallecompra->preciosinImp = floatval(str_replace(",", "", $modeldetallecompra->preciosinImp));
         $modeldetallecompra->precioconImp = floatval(str_replace(",", "", $modeldetallecompra->precioconImp));
            //Consultamos si el existe producto para aumentar la cantidad en tbl_detalle_compra
            $codProdServicio = $modeldetallecompra->codProdServicio;
            $sql = "SELECT * FROM tbl_detalle_compra WHERE codProdServicio = :codProdServicio AND idCompra = :idCompra";
            $command = \Yii::$app->db->createCommand($sql);
            $command->bindParam(":codProdServicio", $codProdServicio);
            $command->bindValue(":idCompra", $id);
            $consulta = $command->queryOne();

                if($consulta['codProdServicio']==$codProdServicio && $consulta['idCompra']==$id)
                {
                //$producto[$position]['cantidad']+=$cantidad;
                $c=1;
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Este producto ya ha sido agregado anteriormente.');
                Yii::$app->response->redirect(array('compras-inventario/update', 'id'=>$id, 'carga'=>false));
                }
            //si la bandera c sigue null se procede a guardar el detalle
            if($c==null){
                $modeldetallecompra->save();//y desde el detalle guardado de recorren los valores
                /*$producto = DetalleCompra::find()->where(["=",'idCompra', $id])->all();
                $impuesto = Yii::$app->params['porciento_impuesto'];
                $porc_descuento = $precioporcantidad = 0;
                foreach ($producto as $position => $product)
                { //al recoger los valores sumo el ciclo para obtener el subtotal y descuento
                  $precioporcantidad += $product['precioCompra'] * $product['cantidadEntrada'];
                  $porc_descuento += ($product['precioCompra'] * $product['descuento'] / 100) * $product['cantidadEntrada'];
                }*/
                $compra = ComprasInventario::find()->where(['idCompra'=>$id])->one();
                $compra->fletes = null;
                $compra->iv_fletes = '';
                /*$compra->subTotal = $precioporcantidad;
                $compra->descuento = $porc_descuento;
                $compra->impuesto = ($precioporcantidad - $porc_descuento) * $impuesto / 100;
                $compra->total = $precioporcantidad - $porc_descuento + (($precioporcantidad - $porc_descuento) * $impuesto / 100);*/
                $compra->save();
            }

            Yii::$app->response->redirect(array('compras-inventario/update', 'id'=>$id, 'carga'=>false));
        }
    }

    //cargar una orden de compra en sesion para una cnueva compra
    public function actionCargar_compra()
    {
      /*CompraInventarioSession::setSubtotal(null);
      CompraInventarioSession::setDescuento(null);
      CompraInventarioSession::setImpuesto(null);
      CompraInventarioSession::setTotal(null);*/
      if(Yii::$app->request->isAjax){
          $idOrdenCompra = Yii::$app->request->post('idOrdenCompra');
          $detalle_orden_compra = DetalleOrdenCompra::find()->where(['idOrdenCompra'=>$idOrdenCompra])->all();
          CompraInventarioSession::setOrdenCompra($idOrdenCompra);
          CompraInventarioSession::setContenidoProducto(array());
          foreach ($detalle_orden_compra as $producto => $value) {
            $producto_proveedor = ProductoProveedores::find()->where(['idProd_prov'=>$value['idProd_prov']])->one();
            $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$value['codProdServicio']])->one();
            $producto = CompraInventarioSession::getContenidoProducto();
            $porcentUtilidad = $producto_serv->precioVenta / $producto_serv->precioCompra * 100 - 100;
                $_POST['codigo_proveedor'] = $producto_proveedor->codigo_proveedor;
                $_POST['codProdServicio'] = $value['codProdServicio'];
                $_POST['precioCompra'] = $producto_serv->precioCompra;
                $_POST['cantidadEntrada'] = $value['cantidad_pedir'];
                $_POST['descuentu'] = '';
                $_POST['exImpuesto'] = $producto_serv->exlmpuesto;
                $_POST['porcentUtilidad'] = number_format($porcentUtilidad,2);
                $_POST['preciosinImp'] = $producto_serv->precioVenta;
                $_POST['precioconImp'] = $producto_serv->precioVentaImpuesto;
                $_POST['usuario'] = Yii::$app->user->identity->username;
                $_POST['estado'] = 'No Aplicado';
                $_POST['modificado'] = 'No';
                $producto[] = $_POST;
            CompraInventarioSession::setContenidoProducto($producto);
          }

          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Datos de la compra cargados correctamente.');
          Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
        }
    }

    //carga una orden de compra en una compra ya en base de datos antes de aplicar la compras
    public function actionCargar_compra_bd()
    {
      if(Yii::$app->request->isAjax){
          $idOrdenCompra = Yii::$app->request->post('idOrdenCompra');
          $idCompra = Yii::$app->request->post('idCompra');
          $detalle_orden_compra = DetalleOrdenCompra::find()->where(['idOrdenCompra'=>$idOrdenCompra])->all();
          $compra = ComprasInventario::find()->where(['idCompra'=>$idCompra])->one();
          $compra->idOrdenCompra = $idOrdenCompra;
          $compra->fletes = null;
          $compra->iv_fletes = '';
          $subTotal = 0; $impuesto = Yii::$app->params['porciento_impuesto'];
          \Yii::$app->db->createCommand()->delete('tbl_detalle_compra', 'idCompra = '.(int) $idCompra)->execute();
          foreach ($detalle_orden_compra as $detalle_compra => $value) {
            $producto_proveedor = ProductoProveedores::find()->where(['idProd_prov'=>$value['idProd_prov']])->one();
            $producto_serv = ProductoServicios::find()->where(['codProdServicio'=>$value['codProdServicio']])->one();
            $detalle_compra = new DetalleCompra();
            $porcentUtilidad = $producto_serv->precioVenta / $producto_serv->precioCompra * 100 - 100;
                $detalle_compra->idCompra = $idCompra;
                $detalle_compra->codigo_proveedor = $producto_proveedor->codigo_proveedor;
                $detalle_compra->codProdServicio = $value['codProdServicio'];
                $detalle_compra->precioCompra = $producto_serv->precioCompra;
                $detalle_compra->cantidadAnterior = $producto_serv->cantidadInventario;
                $detalle_compra->cantidadEntrada = $value['cantidad_pedir'];
                $detalle_compra->descuento = 0.00;
                $detalle_compra->exImpuesto = $producto_serv->exlmpuesto;
                $detalle_compra->porcentUtilidad = number_format($porcentUtilidad,2);
                $detalle_compra->preciosinImp = $producto_serv->precioVenta;
                $detalle_compra->precioconImp = $producto_serv->precioVentaImpuesto;
                $detalle_compra->usuario = Yii::$app->user->identity->username;
                $detalle_compra->estado = 'No Aplicado';
                $subTotal += $detalle_compra->precioCompra * $detalle_compra->cantidadEntrada;
            $detalle_compra->save();
          }
          //$compra->subTotal = $subTotal;
          //$compra->descuento = null;
          //$compra->impuesto = $subTotal * $impuesto / 100;
          //$compra->total = $subTotal + $compra->impuesto;
          $compra->save();
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>Datos de la compra cargados correctamente.');
          Yii::$app->response->redirect(array('compras-inventario/update', 'id'=>$idCompra, 'carga'=>false));
        }
    }

    public function actionActualiza_modalprod_listo(){
        $modeldetallecompra = new DetalleCompra();
        $producto = CompraInventarioSession::getContenidoProducto();
       if ($modeldetallecompra->load(Yii::$app->request->post())) {
         $modeldetallecompra->precioCompra = floatval(str_replace(",", "", $modeldetallecompra->precioCompra));
         $modeldetallecompra->descuento = floatval(str_replace(",", "", $modeldetallecompra->descuento));
         $modeldetallecompra->porcentUtilidad = floatval(str_replace(",", "", $modeldetallecompra->porcentUtilidad));
         $modeldetallecompra->preciosinImp = floatval(str_replace(",", "", $modeldetallecompra->preciosinImp));
         $modeldetallecompra->precioconImp = floatval(str_replace(",", "", $modeldetallecompra->precioconImp));
            if($producto)
             foreach ($producto as $position => $product)
                {
                    if($product['codProdServicio']==$modeldetallecompra->codProdServicio)
                    {
                    //$producto[$position]['cantidad']+=$cantidad;
                    $producto[$position]['codigo_proveedor'] = $modeldetallecompra->codigo_proveedor;
                    $producto[$position]['codProdServicio'] = $modeldetallecompra->codProdServicio;
                    $producto[$position]['precioCompra'] = $modeldetallecompra->precioCompra;
                    $producto[$position]['cantidadEntrada'] = $modeldetallecompra->cantidadEntrada;
                    $producto[$position]['descuentu'] = $modeldetallecompra->descuento;
                    $producto[$position]['exImpuesto'] = $modeldetallecompra->exImpuesto;
                    $producto[$position]['porcentUtilidad'] = $modeldetallecompra->porcentUtilidad;
                    $producto[$position]['preciosinImp'] = $modeldetallecompra->preciosinImp;
                    $producto[$position]['precioconImp'] = $modeldetallecompra->precioconImp;
                    $producto[$position]['usuario'] = $modeldetallecompra->usuario;
                    $producto[$position]['estado'] = $modeldetallecompra->estado;
                    $producto[$position]['modificado'] = 'Si';
                    }
                    CompraInventarioSession::setContenidoProducto($producto);
                }

                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El dato se ha actualizado correctamente.');
                Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
        }
    }

      public function actionActualiza_modalprod_listo_update($id){
        $modeldetallecompra = DetalleCompra::find()->where(['idDetalleCompra'=>$id])->one();
       if ($modeldetallecompra->load(Yii::$app->request->post())) {
         $modeldetallecompra->precioCompra = floatval(str_replace(",", "", $modeldetallecompra->precioCompra));
         $modeldetallecompra->descuento = floatval(str_replace(",", "", $modeldetallecompra->descuento));
         $modeldetallecompra->porcentUtilidad = floatval(str_replace(",", "", $modeldetallecompra->porcentUtilidad));
         $modeldetallecompra->preciosinImp = floatval(str_replace(",", "", $modeldetallecompra->preciosinImp));
         $modeldetallecompra->precioconImp = floatval(str_replace(",", "", $modeldetallecompra->precioconImp));
         $modeldetallecompra->modificado = 'Si';
            if ($modeldetallecompra->save()) {
              /*$producto = DetalleCompra::find()->where(["=",'idCompra', $modeldetallecompra->idCompra])->all();
              $impuesto = Yii::$app->params['porciento_impuesto'];
              $porc_descuento = $precioporcantidad = 0;
              foreach ($producto as $position => $product)
              { //al recoger los valores sumo el ciclo para obtener el subtotal y descuento
                $precioporcantidad += $product['precioCompra'] * $product['cantidadEntrada'];
                $porc_descuento += ($product['precioCompra'] * $product['descuento'] / 100) * $product['cantidadEntrada'];
              }*/
              $compra = ComprasInventario::find()->where(['idCompra'=>$modeldetallecompra->idCompra])->one();
              $compra->fletes = null;
              $compra->iv_fletes = '';
              /*$compra->subTotal = $precioporcantidad;
              $compra->descuento = $porc_descuento;
              $compra->impuesto = ($precioporcantidad - $porc_descuento) * $impuesto / 100;
              $compra->total = $precioporcantidad - $porc_descuento + (($precioporcantidad - $porc_descuento) * $impuesto / 100);*/
              $compra->save();
                Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El dato se ha actualizado correctamente.');
            Yii::$app->response->redirect(array('compras-inventario/update', 'id'=>$modeldetallecompra->idCompra, 'carga'=>false));
            }
        }
        Yii::$app->response->redirect(array('compras-inventario/update', 'id'=>$modeldetallecompra->idCompra, 'carga'=>false));
    }

    //muestra el formulario que me va actualizar el detalle seleccionado
    public function actionActualiza_modalprod(){
        if(Yii::$app->request->isAjax){
            $position = Yii::$app->request->post('position');
            $producto = CompraInventarioSession::getContenidoProducto();
            if($producto)
            foreach ($producto as $positionall => $product)
            {
                if ($positionall==$position) {
                    //echo $producto[$positionall]['codProdServicio'];
                    $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$producto[$positionall]['codProdServicio']])->one();
                    $modeldetallecomprau = new DetalleCompra();
                    $modeldetallecomprau->exImpuesto = $producto[$positionall]['exImpuesto'];
                    //-------------------------
                    $moned_lo = Moneda::find()->where(['monedalocal'=>'x'])->one();
                    $moneda_local = $moned_lo->simbolo;
                    $proveedor = Proveedores::find()->where(['=', 'codProveedores', CompraInventarioSession::getProveedor()])->one();
                        $moneda = Moneda::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->one();
                        $moneda_prov = '°';
                        if (@$moneda) {
                         $moneda_prov = $moneda->simbolo;
                        }
                    $precio_compra_pro = '';
                    $form = ActiveForm::begin([ 'options' => ['name'=>'caja'], 'action'=>['/compras-inventario/actualiza_modalprod_listo'] ]);
                    if ($moneda_local != $moneda_prov) {
                      $precio_compra_pro = '<div class="col-xs-5">
                              <label>Calculadora</label>
                              <input class="form-control" id="precio_dolar_" placeholder="Dolares" type="text" onkeyup="javascript:convertir_dolar_u()">
                            </div>
                            <div class="col-xs-7">'.$form->field($modeldetallecomprau, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                                        //'name' => 'input-33',
                                        'options' => ['value'=>$producto[$positionall]['precioCompra'], 'class'=>'precioc_ form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta_()', "onkeypress"=>"return isNumberDe(event)"],
                                        'clientOptions' => [
                                            'alias' =>  'decimal',
                                            'groupSeparator' => ',',
                                            'autoGroup' => true
                                        ],
                                    ]).'</div>';
                    } else {
                    $precio_compra_pro = $form->field($modeldetallecomprau, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                                      //'name' => 'input-33',
                                      'options' => ['class'=>'precioc_ form-control','value'=>$producto[$positionall]['precioCompra'], 'onkeyup'=>'javascript:obtenerPrecioVenta_()', "onkeypress"=>"return isNumberDe(event)"],
                                      'clientOptions' => [
                                          'alias' =>  'decimal',
                                          'groupSeparator' => ',',
                                          'autoGroup' => true
                                      ],
                                  ]);
                      }
                    $formulario = '<div class="panel panel-default">
                     <div class="panel-body">
                     <div class="well col-lg-12"><center>'.$modelpro->nombreProductoServicio.'
                     <p><strong>Precio de Venta Actual:</strong> ₡'.number_format($modelpro->precioVenta,2).' - <strong>Precio de venta + impuesto:</strong> ₡'.number_format($modelpro->precioVentaImpuesto,2).' - <strong>Cantidad de Inventario:</strong> '.$modelpro->cantidadInventario . ' unidades'.
                     '</p></center></div>
                     <div class="col-lg-4">'.
                    $form->field($modeldetallecomprau, 'codProdServicio')->textInput(['value'=>$producto[$positionall]['codProdServicio'], 'readonly'=>true]).
                    //$form->field($modeldetallecomprau, 'precioCompra')->textInput(['class'=>'precioc_ form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta_()','value'=>$producto[$positionall]['precioCompra']]).
                    $precio_compra_pro.
                    $form->field($modeldetallecomprau, 'cantidadEntrada')->textInput(['value'=>$producto[$positionall]['cantidadEntrada']]).
                    $form->field($modeldetallecomprau, 'descuento')->textInput(['class'=>'descuento_ form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta_()', 'value'=>$producto[$positionall]['descuentu']]).
                    '</div>'.
                    '<div class="col-lg-4">'.
                    $form->field($modeldetallecomprau, 'exImpuesto')->radioList(['Si'=>'Si','No'=>'No']).
                    $form->field($modeldetallecomprau, 'porcentUtilidad')->textInput(['class'=>'porcentu_ form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta_()', 'value'=>$producto[$positionall]['porcentUtilidad']]).
                    $form->field($modeldetallecomprau, 'preciosinImp')->textInput(['class'=>'preciov_ form-control', 'onkeyup'=>'obtenerUtilidadImpu_()', 'value'=>$producto[$positionall]['preciosinImp']]).
                    $form->field($modeldetallecomprau, 'precioconImp')->textInput(['class'=>'precioventaimpu_ form-control', 'onkeyup'=>'obtenerVentaUti_()', 'value'=>$producto[$positionall]['precioconImp']]).
                    '</div>'.
                    '<div class="col-lg-4">'.
                    $form->field($modeldetallecomprau, 'codigo_proveedor')->textInput(['value'=>$producto[$positionall]['codigo_proveedor'], 'readonly'=>false]).
                    $form->field($modeldetallecomprau, 'usuario')->textInput(['value'=>$producto[$positionall]['usuario'], 'readonly'=>true]).
                    $form->field($modeldetallecomprau, 'estado')->textInput(['value'=>$producto[$positionall]['estado'], 'readonly'=>true]).
                    '<input type="submit" value="Actualizar" class="btn btn-primary" id="agregar_pro_comp_u">'.
                     //Html::submitButton('<i class="glyphicon glyphicon-shopping-cart"></i> Actualizar compra', ['class' => 'btn btn-primary']).
                    '</div>
                    </div>
                    </div>';

                    echo $formulario;
                    ActiveForm::end();//El final del activefrom debe de estar despues del echo para que funcione el submit
                }//fin if
            }
        } else echo 'No tenemos resultados';
    }

    //funcion que me permite enviar xml a hacienda para confirmar resolución de compra
    public function actionEnviar_xml_not($id)
    {
      $model = new Notificar_compra();
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        if ($model->load(Yii::$app->request->post())) {
          $model->archivo = UploadedFile::getInstance($model, 'archivo');
          $save_file = '';
          $file = '';
          if($model->archivo){
              $imagepath = 'uploads/';
              $file = $imagepath.$model->archivo->name;
              $save_file = 1;
          }
          if($save_file){
                  $model->archivo->saveAs($file);
              }
        }

        $xml = file_get_contents($file);
        $base64_xml = base64_encode($xml);
        unlink($file);
        $json_recepcion = [
          "resolucion" => $model->resolucion,
          "detalle" => substr($model->detalle, 0, 80),
          "xml" => $base64_xml
        ];
        $consultar_factun = new Factun();
        $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Procesar';
        $result_movimiento = json_decode($consultar_factun->ConeccionFactun_con_pan($api_dir, $json_recepcion, 'POST'));
        $mensaje = $detalle_mensaje = ''; $clave_recepcion = ''; $fecha_recepcion = ''; $mensajes_error = '';
        foreach ($result_movimiento->data as $key => $value) {
                  if ($key == 'estado_recepcion') { $mensaje = $value; }
                  if ($key == 'clave_recepcion') { $clave_recepcion = $value; }
                  if ($key == 'fecha_resolucion') { $fecha_recepcion = $value; }
                }
        foreach ($result_movimiento->mensajes as $key => $value) {
                  //if ($key == '') { $detalle_mensaje = $value; }
                  if ($key!='codigo_mensaje'&&$key!='mensaje'&&$key!='detalle_mensaje') {
                    $mensajes_error .= '<br> > '.$value;
                  }
                }
        if ($result_movimiento->id > 0) {
          if ($mensaje == '01' || $mensaje == '03') {//siempre y cuando el estado sea 03 o 01 se extrae el xml
            $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/v2/Recepcion/XML/'.$clave_recepcion;
            $api_xml_r = 'https://'.Yii::$app->params['url_api'].'/api/v2/Recepcion/RespuestaXML/'.$clave_recepcion;
            $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
            $result_xml_r = $consultar_factun->ConeccionFactun_sin_pan($api_xml_r, 'GET');
          } else {
            $result_xml = '';
            $result_xml_r = '';
          }
          $idCompra = $id;
          $id_factun = $result_movimiento->id;
          $tipo_documento = $model->tipo_documento;
          $resolucion = $model->resolucion;
          if ($model->resolucion == 1) { $resolucion = 'Aceptado'; }
          else if ($model->resolucion == 2) { $resolucion = 'Parcialmente aceptado'; }
          else { $resolucion = 'Rechazado'; }
          $detalle = $model->detalle;
          //$fecha = date("Y-m-d H:i:s",time());
          //$mensaje = $mensaje.' '.$detalle_mensaje;
          $usuario_registra = Yii::$app->user->identity->username;
          $sql = "INSERT INTO recepcion_hacienda (idCompra, id_factun, tipo_documento, resolucion, detalle, respuesta_hacienda, fecha, clave_recepcion, xml_recepcion, xml_respuesta, usuario_registra)
                   VALUES (:idCompra, :id_factun, :tipo_documento, :resolucion, :detalle, :respuesta_hacienda, :fecha, :clave_recepcion, :xml_recepcion, :xml_respuesta, :usuario_registra)";
          $command = \Yii::$app->db->createCommand($sql);
          $command->bindParam(":idCompra", $idCompra);
          $command->bindParam(":id_factun", $id_factun);
          $command->bindParam(":tipo_documento", $tipo_documento);
          $command->bindParam(":resolucion", $resolucion);
          $command->bindParam(":detalle", $detalle);
          $command->bindParam(":respuesta_hacienda", $mensaje);
          $command->bindParam(":clave_recepcion", $clave_recepcion);
          $command->bindParam(":fecha", $fecha_recepcion);
          $command->bindParam(":xml_recepcion", $result_xml);
          $command->bindParam(":xml_respuesta", $result_xml_r);
          $command->bindParam(":usuario_registra", $usuario_registra);
          //$command2->bindValue(":id", $id);
          if ($command->execute()){
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span><strong>Listo: </strong>Proceso enviado a Hacienda correcamente.');
            Yii::$app->response->redirect(array( 'compras-inventario/update', 'id' => $id, 'carga'=>false ));
          }
        } else {
          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error:</strong> No se pudo procesar el envío, esto se puede deber a que exista un parámetro equívoco en el XML. <br>'.$mensajes_error);
          Yii::$app->response->redirect(array( 'compras-inventario/update', 'id' => $id, 'carga'=>false ));
        }

    }

    public function actionRenviar_xml_not()
    {
      $recepcion_hacienda_general = RecepcionHacienda::find()->where('respuesta_hacienda IN ("NO_ENVIADO", "05")',[])->all();
      foreach ($recepcion_hacienda_general as $key_recep => $recepcion) {
        $consultar_factun = new Factun();
        $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Reenviar/'.$recepcion->clave_recepcion;
        $result_movimiento = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir, 'POST'));
        $mensaje = $fecha_recepcion = '';
        foreach ($result_movimiento->data as $key => $value) {
                  if ($key == 'estado_recepcion') { $mensaje = $value; }
                  if ($key == 'fecha_resolucion') { $fecha_recepcion = $value; }
                }
        $key_recep = RecepcionHacienda::find()->where(['clave_recepcion'=>$recepcion->clave_recepcion])->one();
        $key_recep->respuesta_hacienda = $mensaje;
        $key_recep->fecha = $fecha_recepcion;
        $key_recep->save();
      }
      $recepcion_hacienda_general = RecepcionHacienda::find()->where('respuesta_hacienda NOT IN ("01", "NO_ENVIADO", "03", "05", "Procesada correctamente ")',[])->all();
      foreach ($recepcion_hacienda_general as $key_recep => $recepcion) {
        $consultar_factun = new Factun();
        $api_dir = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/Consultar/'.$recepcion->clave_recepcion;
        $result_movimiento = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_dir, 'PUT'));
        $mensaje = $fecha_recepcion = '';
        foreach ($result_movimiento->data as $key => $value) {
                  if ($key == 'estado_hacienda') { $mensaje = $value; }
                }
        $key_recep = RecepcionHacienda::find()->where(['clave_recepcion'=>$recepcion->clave_recepcion])->one();
        $key_recep->respuesta_hacienda = $mensaje;
        if ($mensaje == '01' || $mensaje == '03') {
          $api_xml = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/XML/'.$recepcion->clave_recepcion;
          $api_xml_r = 'https://'.Yii::$app->params['url_api'].'/api/V2/Recepcion/RespuestaXML/'.$recepcion->clave_recepcion;
          $result_xml = $consultar_factun->ConeccionFactun_sin_pan($api_xml, 'GET');
          $result_xml_r = $consultar_factun->ConeccionFactun_sin_pan($api_xml_r, 'GET');
          $key_recep->xml_recepcion = $result_xml;
          $key_recep->xml_respuesta = $result_xml_r;
        }
        $key_recep->save();
      }
      echo "string";
    }

    public function actionActualiza_modalprod_update(){
        if(Yii::$app->request->isAjax){
            $idDetalleCompra = Yii::$app->request->post('idDetalleCompra');
            $producto = DetalleCompra::find()->where(['idDetalleCompra' => $idDetalleCompra])->one();
            if($producto)
                 {
                    //echo $producto[$positionall]['codProdServicio'];
                    $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$producto->codProdServicio])->one();
                    //$modeldetallecomprau = new DetalleCompra();
                    //$modeldetallecomprau->exImpuesto = $producto->exImpuesto;
                    //---------------------------
                    $moned_lo = Moneda::find()->where(['monedalocal'=>'x'])->one();
                    $moneda_local = $moned_lo->simbolo;
                    $proveedor = Proveedores::find()->where(['=', 'codProveedores', CompraInventarioSession::getProveedorup()])->one();
                        $moneda = Moneda::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->one();
                        $moneda_prov = '°';
                        if (@$moneda) {
                         $moneda_prov = $moneda->simbolo;
                        }
                    $precio_compra_pro = '';
                    $form = ActiveForm::begin([ 'options' => ['name'=>'caja'], 'action'=>['/compras-inventario/actualiza_modalprod_listo_update', 'id'=>$producto->idDetalleCompra] ]);
                    if ($moneda_local != $moneda_prov) {
                      $precio_compra_pro = '<div class="col-xs-5">
                              <label>Calculadora</label>
                              <input class="form-control" id="precio_dolar_" placeholder="Dolares" type="text" onkeyup="javascript:convertir_dolar_u()">
                            </div>
                            <div class="col-xs-7">'.$form->field($producto, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                                        //'name' => 'input-33',
                                        'options' => ['class'=>'precioc_ form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta_()', "onkeypress"=>"return isNumberDe(event)"],
                                        'clientOptions' => [
                                            'alias' =>  'decimal',
                                            'groupSeparator' => ',',
                                            'autoGroup' => true
                                        ],
                                    ]).'</div>';
                    } else {
                    $precio_compra_pro = $form->field($producto, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                                      //'name' => 'input-33',
                                      'options' => ['class'=>'precioc_ form-control','onkeyup'=>'javascript:obtenerPrecioVenta_()', "onkeypress"=>"return isNumberDe(event)"],
                                      'clientOptions' => [
                                          'alias' =>  'decimal',
                                          'groupSeparator' => ',',
                                          'autoGroup' => true
                                      ],
                                  ]);
                      }
                    $formulario = '<div class="panel panel-default">
                     <div class="panel-body">
                     <div class="well col-lg-12"><center>'.$modelpro->nombreProductoServicio.'
                     <p><strong>Precio de Venta Actual:</strong> ₡'.number_format($modelpro->precioVenta,2).' - <strong>Precio de venta + impuesto:</strong> ₡'.number_format($modelpro->precioVentaImpuesto,2).' - <strong>Cantidad de Inventario:</strong> '.$modelpro->cantidadInventario . ' unidades'.
                     '</p></center></div>
                     <div class="col-lg-4">'.
                    $form->field($producto, 'codProdServicio')->textInput([ 'readonly'=>true]).
                    //$form->field($producto, 'precioCompra')->textInput(['id'=>'precioc_', 'onkeyup'=>'javascript:obtenerPrecioVenta_(this.value)']).
                    $precio_compra_pro.
                    $form->field($producto, 'cantidadEntrada')->textInput().
                    //$form->field($producto, 'descuento')->textInput().
                    $form->field($producto, 'descuento')->textInput(['class'=>'descuento_ form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta_()']).
                    '</div>'.
                    '<div class="col-lg-4">'.
                    $form->field($producto, 'exImpuesto')->radioList(['Si'=>'Si','No'=>'No']).
                    $form->field($producto, 'porcentUtilidad')->textInput(['class'=>'porcentu_ form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta_()']).
                    //$form->field($producto, 'preciosinImp')->textInput(['id'=>'preciov_', 'onkeyup'=>'obtenerUtilidadImpu_(this.value)']).
                    $form->field($producto, 'preciosinImp')->textInput(['class'=>'preciov_ form-control', 'onkeyup'=>'obtenerUtilidadImpu_()']).
                    //$form->field($producto, 'precioconImp')->textInput(['id'=>'precioventaimpu_', 'onkeyup'=>'obtenerVentaUti_(this.value)']).
                    $form->field($producto, 'precioconImp')->textInput(['class'=>'precioventaimpu_ form-control', 'onkeyup'=>'obtenerVentaUti_()']).
                    '</div>'.
                    '<div class="col-lg-4">'.
                    $form->field($producto, 'codigo_proveedor')->textInput(['readonly'=>false]).
                    $form->field($producto, 'usuario')->textInput(['readonly'=>true]).
                    $form->field($producto, 'estado')->textInput(['readonly'=>true]).
                    $form->field($producto, 'idCompra')->textInput(['type'=>'hidden']).
                    '<input type="submit" value="Actualizar" class="btn btn-primary">'.
                     //Html::submitButton('<i class="glyphicon glyphicon-shopping-cart"></i> Actualizar compra', ['class' => 'btn btn-primary']).
                    '</div>
                    </div>
                    </div>';

                    echo $formulario;
                    ActiveForm::end();//El final del activefrom debe de estar despues del echo para que funcione el submit
                }//fin if
        } else echo 'No tenemos resultados';
    }



    public function actionDeleteitem($id)
    {
        //Descodificamos el array JSON
        $producto = JSON::decode(Yii::$app->session->get('prodcom'), true);
        //Eliminamos el atributo pasado por parámetro
        unset($producto[$id]);
        //Volvemos a codificar y guardar el contenido
        Yii::$app->session->set('prodcom', JSON::encode($producto));
        Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
    }

    public function actionDeleteitem_update(){
        $cod = $_GET['cod'];
        $id = intval($_GET['id']);
        //eliminamos el producto del detalle de compra
        \Yii::$app->db->createCommand()->delete('tbl_detalle_compra', 'idCompra = '.$id.' AND codProdServicio = "'.$cod.'"' )->execute();
        //recorremos de nuevo todas las lineas para refrescar los valores de compra setactualizando
        //el encabezado e compras con los nuevos valores
        /*$producto = DetalleCompra::find()->where(["=",'idCompra', $id])->all();
        $impuesto = Yii::$app->params['porciento_impuesto'];
        $porc_descuento = $precioporcantidad = 0;
        foreach ($producto as $position => $product)
        { //al recoger los valores sumo el ciclo para obtener el subtotal y descuento
          $precioporcantidad += $product['precioCompra'] * $product['cantidadEntrada'];
          $porc_descuento += ($product['precioCompra'] * $product['descuento'] / 100) * $product['cantidadEntrada'];
        }*/
        $compra = ComprasInventario::find()->where(['idCompra'=>$id])->one();
        $compra->fletes = null;
        $compra->iv_fletes = '';
        /*$compra->subTotal = $precioporcantidad;
        $compra->descuento = $porc_descuento;
        $compra->impuesto = ($precioporcantidad - $porc_descuento) * $impuesto / 100;
        $compra->total = $precioporcantidad - $porc_descuento + (($precioporcantidad - $porc_descuento) * $impuesto / 100); */
        $compra->save();
        //Regresamos a la prefactura
        Yii::$app->response->redirect(array( 'compras-inventario/update', 'id' => $id, 'carga'=>false ));
    }

    //Esta funcion me muestra los datos del producto a ingresar a la compra luego de obtenerlo de la busqueda del producto
    public function actionRun_producto(){
        if(Yii::$app->request->isAjax){
            $idproducto = Yii::$app->request->post('idproducto');
            $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
            $encab = '<center>'.$modelpro->nombreProductoServicio.'<p><strong>Precio de Venta Actual:</strong> ₡'.number_format($modelpro->precioVenta,2).' - <strong>Precio de venta + impuesto:</strong> ₡'.number_format($modelpro->precioVentaImpuesto,2).' - <strong>Cantidad de Inventario:</strong> '.$modelpro->cantidadInventario . ' unidades </p></center>';
            $descuento = CompraInventarioSession::getDescuento_consecutivo();
            //-----------------------------Al considerar un descuento recurrente hay que considerar tambien que
            //los valores de ventas con y sin iva se modifican
            $impuesto = Yii::$app->params['porciento_impuesto'];
            $descto = $descuento/100;
            $utilidad = $modelpro->porcentUnidad/100;
            $iva = $impuesto / 100;
            $margen_precioc_descuento = $modelpro->precioCompra * $descto;
            $precioc_cn_descuento = $modelpro->precioCompra - $margen_precioc_descuento; //almaceno el precio de compra menos descuento
            $margen_precioc_utilidad = $precioc_cn_descuento * $utilidad;
            $precioc_cn_utilidad = $precioc_cn_descuento + $margen_precioc_utilidad; //almacena precio de compra (descuento incluido si es el caso) mas utilidad
            $margen_precio_impuesto = $precioc_cn_utilidad * $iva;
            $precio_cn_impuesto = $precioc_cn_utilidad + $margen_precio_impuesto;//almacena precio de compra (descuento incluido si es el caso mas utilidad) mas impuesto
            //-----------------------------
            $arr = array('encab'=>$encab,
                        'precioCompra' => number_format($modelpro->precioCompra,2),
                        'descuento' => $descuento,
                        'porcentUtilidad'=>number_format($modelpro->porcentUnidad,2),
                        'preciosinImp'=>number_format($precioc_cn_utilidad/*$modelpro->precioVenta*/,2),
                        'precioconImp'=>number_format($precio_cn_impuesto/*$modelpro->precioVentaImpuesto*/,2));
            echo json_encode($arr);
        }
    }

    public function actionRun_producto_air(){
        if(Yii::$app->request->isAjax){
            $idproducto = Yii::$app->request->post('aircod');
            $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
            $encab = '<center>'.$modelpro->nombreProductoServicio.'<p><strong>Precio de Venta Actual:</strong> ₡'.number_format($modelpro->precioVenta,2).' - <strong>Precio de venta + impuesto:</strong> ₡'.number_format($modelpro->precioVentaImpuesto,2).' - <strong>Cantidad de Inventario:</strong> '.$modelpro->cantidadInventario . ' unidades </p></center>';
            $descuento = CompraInventarioSession::getDescuento_consecutivo();
            //-----------------------------Al considerar un descuento recurrente hay que considerar tambien que
            //los valores de ventas con y sin iva se modifican
            $impuesto = Yii::$app->params['porciento_impuesto'];
            $descto = $descuento/100;
            $utilidad = $modelpro->porcentUnidad/100;
            $iva = $impuesto / 100;
            $margen_precioc_descuento = $modelpro->precioCompra * $descto;
            $precioc_cn_descuento = $modelpro->precioCompra - $margen_precioc_descuento; //almaceno el precio de compra menos descuento
            $margen_precioc_utilidad = $precioc_cn_descuento * $utilidad;
            $precioc_cn_utilidad = $precioc_cn_descuento + $margen_precioc_utilidad; //almacena precio de compra (descuento incluido si es el caso) mas utilidad
            $margen_precio_impuesto = $precioc_cn_utilidad * $iva;
            $precio_cn_impuesto = $precioc_cn_utilidad + $margen_precio_impuesto;//almacena precio de compra (descuento incluido si es el caso mas utilidad) mas impuesto
            //-----------------------------
            $arr = array('encab'=>$encab,
                        'precioCompra' => number_format($modelpro->precioCompra,2),
                        'porcentUtilidad'=>number_format($modelpro->porcentUnidad,2),
                        'descuento' => $descuento,
                        'preciosinImp'=>number_format($precioc_cn_utilidad/*$modelpro->precioVenta*/,2),
                        'precioconImp'=>number_format($precio_cn_impuesto/*$modelpro->precioVentaImpuesto*/,2));
            echo json_encode($arr);
        }
    }

    //Esta fucion me agrega productos a base de datos desde la modal create
    public function actionAgrega_prod_inv(){
        $modelproducto = new ProductoServicios();
        if ($modelproducto->load(Yii::$app->request->post()) && $modelproducto->save()) {
            CompraInventarioSession::setProductoAir($modelproducto->codProdServicio);
            CompraInventarioSession::setPrecioAir($modelproducto->precioCompra);
            CompraInventarioSession::setUtilidadAir($modelproducto->porcentUnidad);
            CompraInventarioSession::setVentaAir($modelproducto->precioVenta);
            CompraInventarioSession::setVentaImAir($modelproducto->precioVentaImpuesto);
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El producto ha sido registrado correctamente.');
            Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>true));
        }
        else {
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>El sistema detecta que dejó campos incompletos.');
            Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
        }
    }

    //Esta fucion me agrega productos a base de datos desde la modal update
    public function actionAgrega_prod_inv_update($id){
        $modelproducto = new ProductoServicios();
        if ($modelproducto->load(Yii::$app->request->post()) && $modelproducto->save()) {
            CompraInventarioSession::setProductoAir($modelproducto->codProdServicio);
            CompraInventarioSession::setPrecioAir($modelproducto->precioCompra);
            CompraInventarioSession::setUtilidadAir($modelproducto->porcentUnidad);
            CompraInventarioSession::setVentaAir($modelproducto->precioVenta);
            CompraInventarioSession::setVentaImAir($modelproducto->precioVentaImpuesto);
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> El producto ha sido registrado correctamente.');
            Yii::$app->response->redirect(array('compras-inventario/update', 'id'=>$id, 'carga'=>true));
        }
        else {
            Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>El producto no ingresó.');
            Yii::$app->response->redirect(array('compras-inventario/update', 'id'=>$id, 'carga'=>false));
        }
    }

    public function actionRun_producto_input()
    {
      if(Yii::$app->request->isAjax){
        $idProveedor = Yii::$app->request->post('idProveedor');
        $idproducto = Yii::$app->request->post('idproducto');
        //obtenemos la instancia de ProductoProveedor para comprovar si existe un proveedor asociado con este producto
        $pro_pro = ProductoProveedores::find()->where(['codProveedores' => $idProveedor])
        ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio" => $idproducto])
        ->one();
        //una ves comprovado que en este turno el proveedor esta asociado al producto solo guarda el codigo del proveedor
        if (@$pro_pro) {
          echo $pro_pro->codigo_proveedor;
        }
      }
    }

    public function actionBusqueda_producto(){
         if(Yii::$app->request->isAjax){
           $idProveedor = Yii::$app->request->post('idProveedor');
           $consultaBusqueda_cod = Yii::$app->request->post('valorBusqueda_cod');
           $consultaBusqueda_des = Yii::$app->request->post('valorBusqueda_des');
           $consultaBusqueda_cat = Yii::$app->request->post('valorBusqueda_cat');
           $consultaBusqueda_cpr = Yii::$app->request->post('valorBusqueda_cpr');
           $consultaBusqueda_ubi = Yii::$app->request->post('valorBusqueda_ubi');
           $consultaBusqueda_fam = Yii::$app->request->post('valorBusqueda_fam');

           //Filtro anti-XSS
           $caracteres_malos = array("<", ">", "\"", "'", "<", ">", "'");
           $caracteres_buenos = array("&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;", "&#060;", "&#062;", "&#039;", "&#047;");
           $consultaBusqueda_cod = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cod);
           $consultaBusqueda_des = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_des);
           $consultaBusqueda_cat = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cat);
           $consultaBusqueda_cpr = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_cpr);
           //$consultaBusqueda_ubi = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda_ubi);

           $mensaje = "";

           //Comprueba si $consultaBusqueda está seteado
           if (isset($consultaBusqueda_cod) && isset($consultaBusqueda_des) && isset($consultaBusqueda_fam) && isset($consultaBusqueda_cat) && isset($consultaBusqueda_cpr) && isset($consultaBusqueda_ubi)) {
               $consultaBusquedalimpia = "%".$consultaBusqueda_cod."%".$consultaBusqueda_des."%".$consultaBusqueda_ubi."%";
               /*$consultaBusquedalimpia_des =
               $consultaBusquedalimpia_ubi = */
               $tipo = 'Producto';
               $consultaBusqueda_cod = '%'.$consultaBusqueda_cod.'%';
               $consultaBusqueda_des = '%'.$consultaBusqueda_des.'%';
               //$consultaBusqueda_fam = '%'.$consultaBusqueda_fam.'%';
               $consultaBusqueda_cat = '%'.$consultaBusqueda_cat.'%';
               $consultaBusqueda_cpr = '%'.$consultaBusqueda_cpr.'%';
               $consultaBusqueda_ubi = '%'.$consultaBusqueda_ubi.'%';
               $sql = "SELECT tps.codProdServicio, tps.codFamilia, tps.nombreProductoServicio, tps.cantidadInventario, tps.ubicacion, tps.precioVentaImpuesto
               FROM tbl_producto_servicios tps INNER JOIN tbl_familia  tf
               ON tps.codFamilia = tf.codFamilia
               WHERE tps.codProdServicio IN "."(";
               $sql .= " SELECT pr.`codProdServicio`
                 FROM `tbl_producto_catalogo` pr_c RIGHT JOIN tbl_producto_servicios pr
                 ON pr_c.`codProdServicio` = pr.`codProdServicio`
                 LEFT JOIN `tbl_producto_proveedores` pr_p
                 ON pr.`codProdServicio` = pr_p.codProdServicio
                 WHERE pr.tipo = 'Producto'";
                 if($consultaBusqueda_cod != '%%'){
                   $sql .= " AND pr.codProdServicio LIKE :codProdServicio ";
                 }
                 if($consultaBusqueda_des != '%%'){
                   $sql .= " AND pr.nombreProductoServicio LIKE :nombreProductoServicio ";
                 }
                 if($consultaBusqueda_ubi != '%%'){
                   $sql .= " AND pr.ubicacion LIKE :ubicacion ";
                 }
                 if($consultaBusqueda_cat != '%%'){
                   $sql .= " AND pr_c.`codigo` LIKE :codigo ";
                 }/*else{
                   $sql .= " AND pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo ";
                 }*/
                   //$sql .= " AND IF ( :codigo <> '%%', pr_c.`codigo` LIKE :codigo, pr_c.`codigo` IS NULL OR pr_c.`codigo` LIKE :codigo) ";
                 if($consultaBusqueda_cpr != '%%'){
                   $sql .= " AND pr_p.`codigo_proveedor` LIKE  :codigo_prov ";
                 }
                 $sql .= ")";
                 if($consultaBusqueda_fam == ''){
                   $sql .= " AND tps.codFamilia LIKE '%%' ";
                 }else{
                   $sql .= " AND tps.codFamilia = :codFamilia ";
                 }
                     $sql .= " LIMIT 50";
           $command = \Yii::$app->db->createCommand($sql);
           if($consultaBusqueda_cod != '%%'){ $command->bindParam(":codProdServicio", $consultaBusqueda_cod); }
           if($consultaBusqueda_des != '%%'){ $command->bindParam(":nombreProductoServicio", $consultaBusqueda_des); }
              //$command->bindParam(":codFamilia_", $consultaBusqueda_fam);
           if($consultaBusqueda_fam != '')  { $command->bindParam(":codFamilia", $consultaBusqueda_fam); }
           if($consultaBusqueda_cpr != '%%'){ $command->bindParam(":codigo_prov", $consultaBusqueda_cpr); }
           if($consultaBusqueda_cat != '%%'){$command->bindParam(":codigo", $consultaBusqueda_cat); }
           if($consultaBusqueda_ubi != '%%'){ $command->bindParam(":ubicacion", $consultaBusqueda_ubi); }
           $consulta = $command->queryAll();
                    //https://mimentevuela.wordpress.com/2015/08/09/busqueda-instantanea-con-ajax-php-y-mysql/


                    //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje

                        //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                        //echo 'Resultados para <strong>'.$consultaBusqueda.'</strong><br>';

                        //La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle


                        foreach($consulta as $resultados){
                            $precio_venta = $resultados['exlmpuesto'] == 'Si' ? $resultados['precioVenta'] : $resultados['precioVentaImpuesto'];
                            $codProdServicio = $resultados['codProdServicio'];
                            $nombreProductoServicio = $resultados['nombreProductoServicio'];
                            $ubicacion = $resultados['ubicacion'];
                            $cantidadInventario = $resultados['cantidadInventario'];
                            $presiopro = '₡ '.number_format($precio_venta,2);
                            //$accion = '<a href="#" onclick="javascript:carga_producto_compra('.$codProdServicio.')" class="glyphicon glyphicon-plus" data-dismiss="modal"></a>';
                            $accion = Html::a('', '', [
                                'class' => 'glyphicon glyphicon-plus',
                                'onclick'=>'carga_producto_compra("'.$codProdServicio.'")',
                                'data-dismiss'=>'modal',
                                'title' => Yii::t('app', 'Cargar producto'),
                                /*'data' => [
                                    'confirm' => '¿Seguro que quieres agregar?',
                                    'method' => 'post',
                                ],*/
                            ]);
                            //Output
                            $codpro = "'".$codProdServicio."'";
                            $codigo_proveedor_vini = "''";

                            //obtenemos la instancia de ProductoProveedor para comprovar si existe un proveedor asociado con este producto
                            $pro_pro = ProductoProveedores::find()->where(['codProveedores' => $idProveedor])
                            ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$codProdServicio])
                            ->one();
                            //una ves comprovado que en este turno el proveedor esta asociado al producto solo guarda el codigo del proveedor
                            if (@$pro_pro) {
                              $codigo_proveedor_vini = "'".$pro_pro->codigo_proveedor."'";
                            }
                            $catalogo = '';
                            $codigo_proveedor = '';
                           $marcas_asociadas = ProductoCatalogo::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                           foreach ($marcas_asociadas as $marcas) { $catalogo .= $marcas->codigo.' * ';}
                           $codigos_proveedores = ProductoProveedores::find()->where(['codProdServicio'=>$resultados['codProdServicio']])->all();
                           foreach ($codigos_proveedores as $cod_prov) { $codigo_proveedor .= $cod_prov->codigo_proveedor.' * ';}
                            $mensaje .='<tbody tabindex="0"  onkeypress="$(document).keypress(function(event){ if(event.which == 13) carga_producto_compra('.$codpro.', '.$codigo_proveedor_vini.')})">
                                  <tr  style="cursor:pointer" onclick="javascript:carga_producto_compra('.$codpro.', '.$codigo_proveedor_vini.')">
                                  <td id="start" width="150">'.$codProdServicio.'</td>
                                  <td width="450">'.$nombreProductoServicio.'</td>
                                  <td width="100" align="center">'.$cantidadInventario.'</td>
                                  <td width="150" align="center">'.$catalogo.'</td>
                                  <td width="150" align="center">'.$codigo_proveedor.'</td>
                                  <td width="50" align="center">'.$ubicacion.'</td>
                                  <td width="140" align="right">'.$presiopro.'</td>
                                  <td width="60" align="right"></td>
                                  </tr>
                                </tbody>';


                           /* $mensaje .= '
                            <p>
                            <strong>CÓDIGO:</strong> ' . $codProdServicio . '<br>
                            <strong>DESCRIPCIÓN:</strong> ' . $nombreProductoServicio . '<br>
                            <strong>CANT INVENT:</strong> ' . $cantidadInventario . '<br>
                            </p>';*/

                        }



                //$modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
            }
            echo $mensaje;
        }
    }



    //esta funcion me permite cambiar el descuento y la untilidad de forma masiva de los productos marcados desde CREATE
    public function actionObtener_productos_marcados_create()
    {
      if(Yii::$app->request->isAjax){
          $checkboxValues  = Yii::$app->request->post('checkboxValues');
          $descuento_masivo = Yii::$app->request->post('descuento_masivo');
          $utilidad_masiva = Yii::$app->request->post('utilidad_masiva');
          $array = explode(",", $checkboxValues);
          $producto = CompraInventarioSession::getContenidoProducto();
          if ($producto && $array) {
            $impuesto = Yii::$app->params['porciento_impuesto'];
            $codProdServicio = '';  $porc_descuento = $subTotal_productos = 0;
            foreach ($producto as $position => $product)
            {
              foreach ($array as &$key){
                if ($position == $key) {
                  $precioc = floatval($producto[$position]['precioCompra']);
                  $descuento = floatval($descuento_masivo);
                  $porcentu = floatval($utilidad_masiva);
                  $descuento = $descuento_masivo=='' ? $producto[$position]['descuentu'] : floatval($descuento_masivo);
                  $porcentu = $utilidad_masiva=='' ? $producto[$position]['porcentUtilidad'] : floatval($utilidad_masiva);
                  $descto = $descuento/100;
                  $utilidad = $porcentu/100;
                  $iva = $impuesto / 100;
                  $margen_precioc_descuento = $precioc * $descto;
                  $precioc_cn_descuento = $precioc - $margen_precioc_descuento; //almaceno el precio de compra menos descuento
                  $margen_precioc_utilidad = $precioc_cn_descuento * $utilidad;
                  $precioc_cn_utilidad = $precioc_cn_descuento + $margen_precioc_utilidad; //almacena precio de compra (descuento incluido si es el caso) mas utilidad
                  $margen_precio_impuesto = $precioc_cn_utilidad * $iva;
                  $precio_cn_impuesto = $precioc_cn_utilidad + $margen_precio_impuesto;
                  $producto[$position]['descuentu'] = round($descuento,2);
                  $producto[$position]['porcentUtilidad'] = round($porcentu,2);
                  $producto[$position]['preciosinImp'] = round($precioc_cn_utilidad,2);
                  $producto[$position]['precioconImp'] = round($precio_cn_impuesto,2);
                  $producto[$position]['modificado'] = 'Si';
                }
                CompraInventarioSession::setContenidoProducto($producto);
              }
            }

            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Se aplicaron los porcentajes a los productos seleccionados.');
            Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
          }
        }
    }//fin obtener productos marcados para editar descuento y utilidad masiva

    //esta funcion me permite cambiar el descuento y la untilidad de forma masiva de los productos marcados desde UPDATE
    public function actionObtener_productos_marcados_update()
    {
      if(Yii::$app->request->isAjax){
        $bandera = false;
          $checkboxValues  = Yii::$app->request->post('checkboxValues');
          $descuento_masivo = Yii::$app->request->post('descuento_masivo');
          $utilidad_masiva = Yii::$app->request->post('utilidad_masiva');
          $idCompra = Yii::$app->request->post('idCompra');
          $array = explode(",", $checkboxValues);
          $producto = DetalleCompra::find()->where(["=",'idCompra', $idCompra])->all();
          if ($producto && $array) {
            $impuesto = Yii::$app->params['porciento_impuesto'];
            $codProdServicio = ''; $porc_descuento = $precioporcantidad = 0;
            foreach ($producto as $position => $product)
            {
              foreach ($array as &$key){
                $position = DetalleCompra::find()->where(["=",'idDetalleCompra', $product['idDetalleCompra']])->one();
                if ($product['idDetalleCompra'] == $key) {

                  $precioc = floatval($product['precioCompra']);
                  $descuento = $descuento_masivo=='' ? $product['descuento'] : floatval($descuento_masivo);
                  $porcentu = $utilidad_masiva=='' ? $product['porcentUtilidad'] : floatval($utilidad_masiva);
                  $descto = $descuento/100;
                  $utilidad = $porcentu/100;
                  $iva = $impuesto / 100;
                  $margen_precioc_descuento = $precioc * $descto;
                  $precioc_cn_descuento = $precioc - $margen_precioc_descuento; //almaceno el precio de compra menos descuento
                  $margen_precioc_utilidad = $precioc_cn_descuento * $utilidad;
                  $precioc_cn_utilidad = $precioc_cn_descuento + $margen_precioc_utilidad; //almacena precio de compra (descuento incluido si es el caso) mas utilidad
                  $margen_precio_impuesto = $precioc_cn_utilidad * $iva;
                  $precio_cn_impuesto = $precioc_cn_utilidad + $margen_precio_impuesto;
                  $position->descuento = round($descuento,2);
                  $position->porcentUtilidad = round($porcentu,2);
                  $position->preciosinImp = round($precioc_cn_utilidad,2);
                  $position->precioconImp = round($precio_cn_impuesto,2);
                  $position->modificado = 'Si';
                  $position->save();//guardo su respectiva fila
                  $bandera = true;
                }
              }//sumo los precios de compra para agregarlos al subtotal y poder sacrle el impuesto y calcular el descuento
              //$precioporcantidad += $product['precioCompra'] * $product['cantidadEntrada'];
              //$porc_descuento += ($position->precioCompra * $position->descuento / 100) * $position->cantidadEntrada;
            }
            //actualizamos los totales de compras
            $compra = ComprasInventario::find()->where(['idCompra'=>$idCompra])->one();
            $compra->fletes = null;
            $compra->iv_fletes = '';
            //$compra->subTotal = $precioporcantidad;
            //$compra->descuento = $porc_descuento;
            //$compra->impuesto = ($precioporcantidad - $porc_descuento) * $impuesto / 100;
            //$compra->total = $precioporcantidad - $porc_descuento + (($precioporcantidad - $porc_descuento) * $impuesto / 100);
            if ($bandera == true) {
              $compra->save();
              Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br> Se aplicaron los porcentajes a los productos seleccionados.');
            } else {
              Yii::$app->getSession()->setFlash('info', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Error!</strong><br><br> No se presenta resultados.');
            }

            Yii::$app->response->redirect(array('compras-inventario/update', 'id' => $idCompra, 'carga'=>false));
          }
        }
    }

    public function actionObtenermontoflete()
    {
      if(Yii::$app->request->isAjax){
        $fletes = floatval(str_replace(",", "", Yii::$app->request->post('fletes') ));
        $impuesto_flete = Yii::$app->request->post('impuesto_flete');
        $impuesto_productos = Yii::$app->request->post('impuesto_productos');
        $total_productos = Yii::$app->request->post('total_productos');
        $iva = Yii::$app->params['porciento_impuesto'];
        $monto_impuesto_fletes = $impuesto_flete=='si' ? $fletes * $iva / 100 : 0;
        $impuesto_productos = $impuesto_productos + $monto_impuesto_fletes;
        $total_productos = $total_productos + $fletes + $monto_impuesto_fletes;
        //consultamos al proveedor para extraer su tipo de moneda y colocar el simbolo en el resultado
        $proveedor = Proveedores::find()->where(['=', 'codProveedores', CompraInventarioSession::getProveedor()])->one();
        $moneda = Moneda::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->one();
        $moneda_local = '°';
        if (@$moneda) {
         $moneda_local = $moneda->simbolo;
        }
        /*CompraInventarioSession::setImpuesto($impuesto_productos);
        CompraInventarioSession::setTotal($total_productos);*/
        $arr = array('impuestos'=>$moneda_local.number_format($impuesto_productos,2),
                     'total'=>$moneda_local.number_format($total_productos,2));
        echo json_encode($arr);
      }
    }

    //funcion para agregar fletes (incluyendo impuesto si es requerido) al total
    public function actionObtenermontoflete_u()
    {
      if(Yii::$app->request->isAjax){
        $fletes = Yii::$app->request->post('fletes')=='' ? null : floatval(str_replace(",", "", Yii::$app->request->post('fletes') ));
        $if_impuesto_flete = Yii::$app->request->post('impuesto_flete');
        $subTotal = Yii::$app->request->post('subTotal');
        $descuento_productos = Yii::$app->request->post('descuento_productos');
        $idCompra = Yii::$app->request->post('idCompra');
        $iva = Yii::$app->params['porciento_impuesto'];
        $subtot_descuento = $subTotal - $descuento_productos;
        $subtot_descuento_iv = $subtot_descuento * $iva / 100;
        $monto_impuesto_fletes = $if_impuesto_flete=='si' ? $fletes * $iva / 100 : 0;
        $impuesto_productos = $subtot_descuento_iv + $monto_impuesto_fletes;
        $total_productos = $subTotal - $descuento_productos + $impuesto_productos + $fletes;

        $compr = ComprasInventario::find()->where(['idCompra'=>$idCompra])->one();
        $compr->fletes = $fletes;
        $compr->iv_fletes = $if_impuesto_flete;
        /*$compr->impuesto = $impuesto_productos;
        $compr->total = $total_productos;*/
        $compr->save();
        //consultamos al proveedor para extraer su tipo de moneda y colocar el simbolo en el resultado
        $proveedor = Proveedores::find()->where(['=', 'codProveedores', $compr->idProveedor])->one();
        $moneda = Moneda::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->one();
        $moneda_local = '°';
        if (@$moneda) {
         $moneda_local = $moneda->simbolo;
        }
        $arr = array('impuestos'=>$moneda_local.number_format($impuesto_productos,2),
                     'total'=>$moneda_local.number_format($total_productos,2));
        echo json_encode($arr);
      }
    }

    //Esta funcion me guarda la compra con los detalles de compra a base de datos
    public function actionGuardarcompra(){
      if(Yii::$app->request->isAjax){
        $fletes = Yii::$app->request->post('fletes') == '' ? null : floatval(str_replace(",", "", Yii::$app->request->post('fletes') ));
        $impuesto_flete = Yii::$app->request->post('impuesto_flete');
         if (($formaPago = CompraInventarioSession::getFormapago()) && ($fechaDocumento = CompraInventarioSession::getFechadocumento()) &&
            ($fechaRegistro = CompraInventarioSession::getFecharegistro()) && ($idProveedor = CompraInventarioSession::getProveedor()) && ($numFactura = CompraInventarioSession::getNofactura()) &&
            ($subTotal = CompraInventarioSession::getSubtotal()) && ($impuesto = CompraInventarioSession::getImpuesto()) && ($total = CompraInventarioSession::getTotal()) &&
            ($productos = CompraInventarioSession::getContenidoProducto()) )
             {
                $fechaVencimiento = CompraInventarioSession::getFechavencimiento() != '' ? CompraInventarioSession::getFechavencimiento() : '';
                $descuento = CompraInventarioSession::getDescuento();
                $idOrdenCompra = CompraInventarioSession::getOrdenCompra();//si en session se encuentra una orden, esta se guarda en bd
                $n_interno_prov = CompraInventarioSession::getInterno_prov();
                $CompraInventario = new ComprasInventario;
                $compra_refistrada = ComprasInventario::find()->where(['idOrdenCompra'=>$idOrdenCompra])->one();
                if (@$idOrdenCompra && @$compra_refistrada) {
                  Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br>Encontramos que la Orden de Compra ya ha sido asignada a otra Compra.');
                  Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
                } else {
                $CompraInventario->idOrdenCompra = @$idOrdenCompra ? @$idOrdenCompra : null;
                $CompraInventario->formaPago = $formaPago;
                $CompraInventario->fechaDocumento = $fechaDocumento;
                $CompraInventario->fechaRegistro = $fechaRegistro;
                $CompraInventario->fechaVencimiento = $fechaVencimiento;
                $CompraInventario->idProveedor = $idProveedor;
                $CompraInventario->numFactura = $numFactura;
                $CompraInventario->n_interno_prov = $n_interno_prov;
                $CompraInventario->estado = 'Pendiente';
                //$CompraInventario->estado_pago = $formaPago == 'Crédito' ? 'Pendiente' : '';
                $CompraInventario->subTotal = floatval($subTotal);
                $CompraInventario->descuento = floatval($descuento);
                $CompraInventario->impuesto = floatval($impuesto);
                $CompraInventario->fletes = $fletes;
                $CompraInventario->iv_fletes = $impuesto_flete;
                $CompraInventario->total = floatval($total);
                if ($CompraInventario->save()) {
                  $idCompra = $CompraInventario->idCompra;
                    foreach ($productos as $position => $product) {
                        $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();

                        $position = new DetalleCompra();
                        $position->idCompra = $CompraInventario->idCompra;
                        $position->codProdServicio = $product['codProdServicio'];
                        $position->precioCompra = $product['precioCompra'];
                        $position->cantidadAnterior = $modelpro->cantidadInventario;
                        $position->cantidadEntrada = $product['cantidadEntrada'];
                        $position->descuento = $product['descuentu'] != '' ? $product['descuentu'] : '';
                        $position->exImpuesto = $product['exImpuesto'];
                        $position->codigo_proveedor = $product['codigo_proveedor'];
                        $position->porcentUtilidad = $product['porcentUtilidad'];
                        $position->preciosinImp = $product['preciosinImp'];
                        $position->precioconImp = $product['precioconImp'];
                        $position->usuario = $product['usuario'];
                        $position->estado = $product['estado'];
                        $position->modificado = $product['modificado'];
                        $position->save();
                    }//fin foreach
                      $this->borrartodosession();
                      CompraInventarioSession::setContenidoProducto(array());
                      Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La compra se ha registrado correctamente.');
                      Yii::$app->response->redirect(array('compras-inventario/update', 'id' => $CompraInventario->idCompra, 'carga'=>false));
                      echo $CompraInventario->idCompra;
                } //fin if
                else {
                        $error='';
                        foreach ($CompraInventario->getErrors() as $position => $er)
                        $error.=$er[0].'<br/>';
                        Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>La Compra no pudo ser registrada.');
                        Yii::$app->response->redirect(array('compras-inventario/create', 'carga'=>false));
                    }//fin else
               }//fin else que me comprueba si ya se esta usando esta orden de compra
             }//Fin if que obtiene los datos de session
         }
    }
    public function actionAplicarcompra($id){

        $model = $this->findModel($id);
        $idOrdenCompra = null;
        if ($model->load(Yii::$app->request->post())) {
          $idOrdenCompra = $model->idOrdenCompra == '' ? null : $model->idOrdenCompra;
        }
        $estado_pago = $model->formaPago == 'Contado' ? 'Cancelada' : 'Pendiente' ;
        $compr = ComprasInventario::find()->where(['idCompra'=>$id])->one();
        $credi_saldo = $compr->total;
        $nuevoestado = "Aplicado";
        $OrdenCompra = OrdenCompraProv::find()->where(['idOrdenCompra'=>$idOrdenCompra])->one();
        #Primero debemos comprovar que esta orden no fue asignada a otra compra.
        if (@$OrdenCompra->estado=='Completa') {
          $boton_ver_compra = Html::a('Ver', ['update', 'id' => $OrdenCompra->idCompra, 'carga'=>false], [
                          'class' => 'btn btn-info',
                          'title' => Yii::t('app', 'Ver la compra')]);
          Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>El sistema detecta que la Orden de Compra asignada ya fue asociada a la Compra número '.$OrdenCompra->idCompra.'. '.$boton_ver_compra);
          Yii::$app->response->redirect(array('compras-inventario/update', 'id' => $id, 'carga'=>false));
        } else {
          if (@$OrdenCompra) {
            $OrdenCompra->idCompra = $id;
            $OrdenCompra->fecha_ingreso_mercaderia = date("Y-m-d");
            $OrdenCompra->estado = 'Completa';
            if ($OrdenCompra->save()) {//cuando la orden de compra es actualizada ponemos disponibles los productos asociados a esa compra
              $producto_pedido = ProductoPedido::find()->where(['idOrdenCompra'=>$OrdenCompra->idOrdenCompra])->all();
              foreach ($producto_pedido as $key => $pp) {
                $pro_pro = ProductoProveedores::find()->where(['codProveedores' => $pp->idProveedor])
                ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$pp->codProdServicio])
                ->one();
                  $pro_pro->estado = 'Disponible';
                  $pro_pro->save();
              }
            }
          }
          $sql = "UPDATE tbl_compras SET estado = :estado, estado_pago = :estado_pago, credi_saldo = :credi_saldo, idOrdenCompra = :idOrdenCompra WHERE idCompra = :id";
          $command = \Yii::$app->db->createCommand($sql);
          $command->bindParam(":estado", $nuevoestado);
          $command->bindParam(":estado_pago", $estado_pago);
          $command->bindParam(":credi_saldo", $credi_saldo);
          $command->bindParam(":idOrdenCompra", $idOrdenCompra);
          $command->bindValue(":id", $id);
          $command->execute();
          $sql2 = "UPDATE tbl_detalle_compra SET estado = :estado WHERE idCompra = :id";
          $command2 = \Yii::$app->db->createCommand($sql2);
          $command2->bindParam(":estado", $nuevoestado);
          $command2->bindValue(":id", $id);
          $command2->execute();
          $productos = DetalleCompra::find()->where(["=",'idCompra', $id])->all();
              foreach($productos as $position => $product)
              {
                  $codProdServicio = $product['codProdServicio'];
                  $nuevacantidad = $product['cantidadAnterior'] + $product['cantidadEntrada'];//
                  $precioCompra = $product['precioCompra'];//
                  $porcentUtilidad = $product['porcentUtilidad'];//
                  $exImpuesto = $product['exImpuesto'];//
                  $preciosinImp = $product['preciosinImp'];//
                  $precioconImp =$product['precioconImp'];//
                  $codigo_proveedor = $product['codigo_proveedor'];
                  $sql3 = "UPDATE tbl_producto_servicios SET cantidadInventario = :cantidadInventario, exlmpuesto = :exImpuesto,
                  porcentUnidad = :porcentUtilidad, precioCompra = :precioCompra, precioVenta = :preciosinImp, precioVentaImpuesto = :precioconImp
                  WHERE codProdServicio = :codProdServicio";
                  $position = \Yii::$app->db->createCommand($sql3);
                  $position->bindParam(":cantidadInventario", $nuevacantidad);
                  $position->bindParam(":exImpuesto", $exImpuesto);
                  $position->bindParam(":porcentUtilidad", $porcentUtilidad);
                  $position->bindParam(":precioCompra", $precioCompra);
                  $position->bindParam(":preciosinImp", $preciosinImp);
                  $position->bindParam(":precioconImp", $precioconImp);
                  $position->bindValue(":codProdServicio", $codProdServicio);
                  $position->execute();
                  //obtenemos la instancia de ProductoProveedor para comprovar si existe un proveedor asociado con este producto
                  $pro_pro = ProductoProveedores::find()->where(['codProveedores' => $compr->idProveedor])
                  ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$codProdServicio])
                  ->one();
                  //una ves comprovado que en este turno el proveedor esta asociado al producto solo guarda el codigo del proveedor
                  if (@$pro_pro) {
                    $pro_pro->codigo_proveedor = $codigo_proveedor;
                    $pro_pro->estado = 'Disponible';
                    $pro_pro->save();
                  }else {// de lo contrario se crea una nueva relacion del proveedor con el producto
                    $new_pro_pro = new ProductoProveedores();
                    $new_pro_pro->codProdServicio = $codProdServicio;
                    $new_pro_pro->codProveedores = $compr->idProveedor;
                    $new_pro_pro->codigo_proveedor = $codigo_proveedor;
                    $new_pro_pro->estado = 'Disponible';
                    $new_pro_pro->save();
                    //al fin y al cabo cada producto puede consultar su proveedor que lo ofrece
                  } //de otro modo no se duplica el mismo asocie ya que si fue el mismo solo modifica el código de proveedor
                  $fecha = date('Y-m-d H:i:s',time());
                  $origen = 'Compra';
                  $id_documento = $product['idCompra'];
                  $cant_ant = $product['cantidadAnterior'];
                  $tipo = 'SUM';
                  $cant_mov = $product['cantidadEntrada'];
                  $cant_new = $nuevacantidad;
                  $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
                  $usuario = $funcionario->idFuncionario;
                  $sql_h_p = "INSERT INTO tbl_historial_producto VALUES( NULL, :codProdServicio, :fecha, :origen, :id_documento, :cant_ant, :tipo, :cant_mov, :cant_new, :usuario )";
                  $position_h_p = \Yii::$app->db->createCommand($sql_h_p);
                  $position_h_p->bindParam(":codProdServicio", $codProdServicio);
                  $position_h_p->bindParam(":fecha", $fecha);
                  $position_h_p->bindParam(":origen", $origen);
                  $position_h_p->bindParam(":id_documento", $id_documento);
                  $position_h_p->bindValue(":cant_ant", $cant_ant);
                  $position_h_p->bindParam(":tipo", $tipo);
                  $position_h_p->bindValue(":cant_mov", $cant_mov);
                  $position_h_p->bindValue(":cant_new", $cant_new);
                  $position_h_p->bindParam(":usuario", $usuario);
                  $position_h_p->execute();
              }
              if ($estado_pago == 'Pendiente') {
                  $model_proveedores = Proveedores::find()->where(['codProveedores' => $compr->idProveedor])->one();
                  $model_proveedores->credi_saldo += $credi_saldo;
                  $model_proveedores->save();
              }
          Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La compra se ha aplicado correctamente.');
          Yii::$app->response->redirect(array('compras-inventario/update', 'id' => $id, 'carga'=>false));
        }//fin else comprovar si ya la orden estaba completa
    }

    //imprimir en Pdf la compra
    public function actionReport($id)
    {
        $empresaimagen = new Empresa();
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $compra = $this->findModel($id);
        $fechareg = $compra->fechaRegistro;

        $content = '
        <div class="panel panel-default">
            <div class="panel-body">
                <table>
                    <tbody>
                    <tr>
                    <td>
                    <img src="'. $empresaimagen->getImageurl('html') .'" style="height: 120px;">
                    </td>
                    <td style="text-align: left;">
                    <strong>'.$empresa->nombre.'</strong><br>
                    <strong>Cédula jurídica:</strong> '.$empresa->ced_juridica.'<br>
                    <strong>Teléfono(s):</strong> '.$empresa->telefono.' / '.$empresa->fax.'<br>
                    <strong>Dirección:</strong> '.$empresa->localidad.' - '.$empresa->direccion.'<br>
                    <strong>Dirección:</strong> '.$empresa->sitioWeb.'<br>
                    <strong>Dirección:</strong> '.$empresa->email.'
                    </td>
                    <tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Compra N°: '.$id. '<br>Fecha: '.$fechareg.'</h4>';

        $content .= $this->renderAjax('view',['model' => $compra]);

        /*$stylesheet = file_get_contents('http://uni-bus.tellan.ru/vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css');
        $pdf = new mPDF();
        $pdf->title = 'Factura No. '.$id;
        $pdf->SetHtmlHeader($header);
        $pdf->WriteHTML($stylesheet,1);
        $pdf->WriteHTML($content,2);
        $pdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa());
        $pdf->Output('Factura No. '.$id.'.pdf','I');
        exit;*/
       /* $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => 'Factura No. '.$id],
            'methods' => [
                //'SetHtmlHeader'=>[$header],
                'SetFooter'=>[' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|'.$empresa->getEmpresa()],
            ]
        ]);
        return $pdf->render();*/

        $content = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Compra</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                <script type="text/javascript">
                    function imprimir() {
                        if (window.print) {
                            window.print();
                            setTimeout("self.close()", 50 );
                        } else {
                            alert("La función de impresion no esta soportada por su navegador.");
                        }
                    }
                </script>
            </head>
            <body onload="imprimir();">

            '.$content.'
            </body>

            </html>';


        echo $content;
    }

//Estas funciones son para el módulo de etiquetas
    //vista de etiquetas
    public function actionEtiquetas(){
        $etiquetas = Etiquetas::find()->all(); //obtengo las etiquetas que estan en base de datos
        return $this->render('etiquetas', ['etiquetas' => $etiquetas]);
    }//fin de funcion etiquetas

    //vista de etiquetas, es opcional como ejemplo para otra funsión.
    //esta función en este momento no esta en uso
    public function actionEtiquetas_marcadas(){
        return $this->render('etiquetas_marcadas');
    }//fin de funcion etiquetas

    //Esta funcion me muestra la información de la etiqueta a generar luego de obtenerla de la busqueda del producto
    public function actionRun_producto_etiqueta(){
        if(Yii::$app->request->isAjax){
            $idproducto = Yii::$app->request->post('idproducto');
            $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$idproducto])->one();
            $precio_venta = $modelpro->exlmpuesto == 'Si' ? $modelpro->precioVenta : $modelpro->precioVentaImpuesto;
            echo '<center><h3>'.$modelpro->nombreProductoServicio.
            '</h3><h4><strong>CÓDIGO:</strong> '.$modelpro->codProdServicio.
            ' &nbsp;&nbsp; - &nbsp;&nbsp; <strong>FECHA REGISTRO:</strong> '.date('j-m-Y').
            ' &nbsp;&nbsp; - &nbsp;&nbsp; <strong>UBI:</strong>'.$modelpro->ubicacion.
            ' &nbsp;&nbsp; - &nbsp;&nbsp; <strong>PRECIO:</strong> ₡' .number_format($precio_venta,2). '</h4>
             </center>
             <div class="col-lg-3">
             </div>
             <div class="col-lg-6">
             <div class="input-group">
             <span class="input-group-addon" id="basic-addon3">Cantidad de etiquetas</span>
             <input type="text" class="form-control" id="cant_etiq" value="'.$modelpro->cantidadInventario.'" placeholder="Search for...">
             <input type="hidden" name="producto_code" id="producto_code" value="'.$modelpro->codProdServicio.'">
             <span class="input-group-btn">
                <button class="btn btn-primary" type="button" onclick="run_agrega_etiquetas_cantidad()" >Agregar esta etiqueta</button>
             </span>
             </div>
             </div>';
        }
    }

    //Esta funcion me permite almacenar en base de datos los datos a ser impresos como etiquetas
    public function actionRun_agrega_etiqueta(){
        if(Yii::$app->request->isAjax){
            $cod_producto = Yii::$app->request->post('producto');
            $cantidad = Yii::$app->request->post('cantidad');
            $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$cod_producto])->one();
            $c=null;
            $etiquetas = new Etiquetas();
            if($c==null){
              $precio_venta = $modelpro->exlmpuesto == 'Si' ? $modelpro->precioVenta : $modelpro->precioVentaImpuesto;
                $etiquetas->codigo = $modelpro->codProdServicio;
                $etiquetas->nombre = $modelpro->nombreProductoServicio;
                $etiquetas->fecha_reg = date("Y-m-j");
                $etiquetas->ubi = $modelpro->ubicacion;
                $etiquetas->precio = $precio_venta;
                $etiquetas->cantidad = (int)$cantidad;
                $etiquetas->estado = 'ina';

                $etiquetas->save();

            }
            Yii::$app->response->redirect(array('compras-inventario/etiquetas'));
        }
    }

    //Esta funcion me permite almacenar en base de datos la compra seleccionada para ser impresos como etiquetas
    public function actionAgrega_compra_etiqueta(){
        if(Yii::$app->request->isAjax){
            $compra_factura = Yii::$app->request->post('compra');
            $idProveedor = Yii::$app->request->post('prov');
            $factura = ComprasInventario::find()->where(['idProveedor'=>(int)$idProveedor])
            ->andWhere("numFactura = :numFactura", [":numFactura"=>$compra_factura])->one();
            if (@$factura) {
                if ($factura->estado == 'Aplicado') {
                   $detallecompra = DetalleCompra::find()->where(['idCompra'=>$factura->idCompra])->all();
                    foreach ($detallecompra as $position => $etiqueta) {
                        $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$etiqueta->codProdServicio])->one();
                        $precio_venta = $modelpro->exlmpuesto == 'Si' ? $modelpro->precioVenta : $modelpro->precioVentaImpuesto;
                        $position = new Etiquetas();
                        $position->codigo = $etiqueta->codProdServicio;
                        $position->nombre = $modelpro->nombreProductoServicio;
                        $position->fecha_reg = date("Y-m-j");
                        $position->ubi = $modelpro->ubicacion;
                        $position->precio = $precio_venta;
                        $position->cantidad = (int)$modelpro->cantidadInventario;
                        $position->estado = 'ina';
                        $position->save();
                    }
                    Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>La compra se ha ingresado con éxito.');
                    Yii::$app->response->redirect(array('compras-inventario/etiquetas'));
                }
                else {
                    Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>Esta compra todavía no está aplicada.');
                    Yii::$app->response->redirect(array('compras-inventario/etiquetas'));
                }
            } else {
                Yii::$app->getSession()->setFlash('error', '<span class="glyphicon glyphicon-remove-sign"></span> <strong>Error!</strong><br><br>No se encontro ninguna compra.');
                Yii::$app->response->redirect(array('compras-inventario/etiquetas'));
            }

        }
    }

    //muestra el espacio para actualizar la cantidad de eiquetas a imprimir
    public function actionActualiza_modaletiq(){
        if(Yii::$app->request->isAjax){
            $position = Yii::$app->request->post('position');

            $etiqueta = Etiquetas::find()->where(['codigo'=>$position])->one();

            $form = ActiveForm::begin([ 'options' => ['name'=>'caja'], 'action'=>['/compras-inventario/actualiza_modaletiq_listo'] ]);
            $formulario = '<center><h3>'.$etiqueta->nombre.
            '</h3><h4><strong>CÓDIGO:</strong> '.$etiqueta->codigo.
            ' &nbsp;&nbsp; - &nbsp;&nbsp; <strong>FECHA REGISTRO:</strong> '.date("j-m-Y").
            ' &nbsp;&nbsp; - &nbsp;&nbsp; <strong>UBI:</strong>'.$etiqueta->ubi.
            ' &nbsp;&nbsp; - &nbsp;&nbsp; <strong>PRECIO:</strong> ₡' .number_format($etiqueta->precio,2). '</h4>
             </center>
             <div class="col-lg-3">
             <input type="hidden" name="position" id="position" value="'.$etiqueta->codigo.'">
             </div>
             <div class="col-lg-6">
             <div class="input-group">
             <span class="input-group-addon" id="basic-addon3">Cantidad de etiquetas</span>
             <input type="text" class="form-control" name="cantidad" id="cant_etiq" value="'.$etiqueta->cantidad.'" placeholder="Search for...">

             <span class="input-group-btn">
                <button class="btn btn-primary" type="submit" >Actualizar etiqueta</button>
             </span>
             </div>
             </div>
            </div>';

            echo $formulario;
            ActiveForm::end();//El final del activefrom debe de estar despues del echo para que funcione el submit

        } else echo 'No tenemos resultados';
    }

    //funcion para aplicar la actualizacion de la etiqueta seleccionada
    public function actionActualiza_modaletiq_listo(){

        $request = Yii::$app->request;
        $position = $request->post('position');
        $cantidad = $request->post('cantidad');

           $sql = "UPDATE tbl_etiquetas SET cantidad = :cantidad
            WHERE codigo = :position";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":cantidad", $cantidad);
                        $command->bindParam(":position", $position);

            if ($command->execute()){
            Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br><br>El dato se ha actualizado correctamente.');
            Yii::$app->response->redirect(array('compras-inventario/etiquetas'));
            }
    }

    //elimina la eliqueta seleccionada en la orden
    public function actionDeleteetiqueta($id)
    {
        \Yii::$app->db->createCommand()->delete('tbl_etiquetas', 'codigo = "'.$id.'"' )->execute();
        Yii::$app->response->redirect(array('compras-inventario/etiquetas'));
    }

    //actualizo las etiquetas que estan en bd para activarlas a la impresión
    public function actionObtener_etiquetas_marcadas(){
        if(Yii::$app->request->isAjax){
            $checkboxValues  = Yii::$app->request->post('checkboxValues');
            $array = explode(",", $checkboxValues);
            $etiqueta = Etiquetas::find()->all(); //obtengo las etiquetas que estan en base de datos
            //primero compruevo que ambas variables vengan con datos
            if($etiqueta && $array)
                foreach ($etiqueta as $position => $product)//recorro los datos de las etiquetas que estan en bd
                    {
                        $estado2 = 'ina';//inactivo todas las etiquetas para luego activar las seleccionadas
                        $codigo2 = $product['codigo'];

                        $sql2 = "UPDATE tbl_etiquetas SET estado = :estado WHERE codigo = :codigo";
                        $command2 = \Yii::$app->db->createCommand($sql2);
                        $command2->bindParam(":estado", $estado2);
                        $command2->bindParam(":codigo", $codigo2);
                        $command2->execute();

                        foreach ($array as &$key){//recorro las etiquetas que he seleccionado
                            //confirmo si esta etiqueta es igual a alguna que esté marcada
                            if($product['codigo']==$key)
                            {
                                $codigo = $product['codigo'];
                                $estado = 'act';//si es asi la activo para a ser impresa
                                $sql = "UPDATE tbl_etiquetas SET estado = :estado WHERE codigo = :codigo";
                                $command = \Yii::$app->db->createCommand($sql);
                                $command->bindParam(":estado", $estado);
                                $command->bindParam(":codigo", $codigo);
                                $command->execute();//con los datos modificados guardo los cambios
                            }
                        }

                    }


        }//fin if isAjax
        //echo $this->renderAjax('etiquetas_marcadas');
    }//fin funcion actionObtener_etiquetas_marcadas

    //elimino las etiquetas que estan marcadas
    public function actionEliminar_etiquetas_marcadas(){
        if(Yii::$app->request->isAjax){
            $checkboxValues  = Yii::$app->request->post('checkboxValues');
            $array = explode(",", $checkboxValues);
            $etiqueta = Etiquetas::find()->all(); //obtengo las etiquetas que estan en base de datos
            //primero compruevo que ambas variables vengan con datos
            if($etiqueta && $array)
                foreach ($etiqueta as $position => $product)//recorro los datos de las etiquetas que estan en bd
                    {
                        foreach ($array as &$key){//recorro las etiquetas que he seleccionado
                            //confirmo si esta etiqueta es igual a alguna que esté marcada
                            if($product['codigo']==$key)
                            {
                                \Yii::$app->db->createCommand()->delete('tbl_etiquetas', 'codigo = "'.$key.'"' )->execute();
                            }
                        }

                    }

            Yii::$app->response->redirect(array('compras-inventario/etiquetas'));
        }//fin if isAjax
        //echo $this->renderAjax('etiquetas_marcadas');
    }//fin funcion actionObtener_etiquetas_marcadas

    public function actionEtiquetas_compra($id)
    {
      return $this->render('etiquetas_compra', ['id' => $id]);
    }

    //funcion para imprimir el Pdf
    public function actionPdf_resolucion($id)
    {
      $consultar_factun = new Factun();
      $model = RecepcionHacienda::findOne($id);
      $compra = ComprasInventario::findOne($model->idCompra);
      $proveedor = Proveedores::findOne($compra->idProveedor);
      $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
      $tipo_mov = '';
      $num_mov_cons = '';
      if ($model->tipo_documento == 'Nota de crédito electronica') {
        $tipo_mov = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NCE Receptor: ';
        $num_mov_cons = '';
      }
      if ($model->tipo_documento == 'Nota de débito electronica') {
        $tipo_mov = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NDE Receptor: ';
        $num_mov_cons = '';
      }
      $pdf_html = '<h3 align="center">COMPROBANTE ELECTRÓNICO DE RECEPCIÓN DE DOCUMENTOS<br>DEL MINISTERIO DE HACIENDA</h3><br>';
      $pdf_html .= '<table>
                      <tr>
                        <td>Clave:</td>
                        <td>'.$model->clave_recepcion.'</td>
                      </tr>
                      <tr>
                        <td>Número Cédula Emisor:</td>
                        <td>'.$proveedor->cedula.'</td>
                      </tr>
                      <tr>
                        <td>Fecha Emisor:</td>
                        <td>'.date('d-m-Y h:i:s A', strtotime($model->fecha)).'</td>
                      </tr>
                      <tr>
                        <td>Monto Total Impuesto:</td>
                        <td>'.number_format($compra->impuesto,2).'</td>
                      </tr>
                      <tr>
                        <td>Total Factura:</td>
                        <td>'.number_format($compra->total,2).'</td>
                      </tr>
                      <tr>
                        <td>Número Cédula Receptor:</td>
                        <td>'.$empresa->ced_juridica.'</td>
                      </tr>
                    </table>';
      $pdf_html .= '<h4>Detalle de confirmación</h4>';
      $pdf_html .= '<table>
                      <tr>
                        <td>FE Receptor:</td>
                        <td>'.$compra->numFactura.'</td>
                        <td>'.$tipo_mov.'</td>
                        <td>'.$num_mov_cons.'</td>
                      </tr>
                      <tr>
                        <td>Mensaje:</td>
                        <td>'.$model->resolucion.'</td>
                      </tr>
                      <tr>
                        <td>Detalle Mensaje:</td>
                        <td>'.$model->detalle.'</td>
                      </tr>
                    </table><br><br>
                    <p align="center">"Autorizada mediante resolución N° DGT-R-48-2016 del 07-10-2016"</p>';
      $archivo_pdf = 'Recepción ID. '.$model->id.'.pdf';
      $stylesheet = file_get_contents('css/style_print.css');
      $caja_pdf = new ComprasInventario();
      $caja_pdf->_pdf_show_re($model->id, $stylesheet, $pdf_html, $archivo_pdf);

      //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Se está descargando el PDF.');
      //return $this->redirect(['index']);
    }
}
