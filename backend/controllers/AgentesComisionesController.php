<?php

namespace backend\controllers;

use Yii;
use backend\models\AgentesComisiones;
use backend\models\search\AgentesComisionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;

/**
* AgentesComisionesController implements the CRUD actions for AgentesComisiones model.
*/
class AgentesComisionesController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','view','create','update','delete','consulta_repetido'],
        'rules' => [
          [
            'actions' => ['index','view','create','update','delete','consulta_repetido'],
            'allow' => true,
            'roles' => ['@'],
          ]
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Lists all AgentesComisiones models.
  * @return mixed
  */
  public function actionIndex()
  {
    $searchModel = new AgentesComisionesSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Displays a single AgentesComisiones model.
  * @param integer $id
  * @return mixed
  */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  //me comprueba si existe un documento igual al mismo Agente Comision
  public function actionConsulta_repetido()
  {
    if(Yii::$app->request->isAjax){
      $numero_documento = Yii::$app->request->post('numero_documento');
      $pasa = 'si';


      $agtComision = AgentesComisiones::find()->where(['cedula'=>$numero_documento])->one();


      if (@$agtComision)
      {
        $pasa = 'no';
      }

      echo $pasa;

    }
  }//fin de actionConsulta_repetido()

  /**
  * Creates a new AgentesComisiones model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $model = new AgentesComisiones();
    $model->estadoAgenteComision = 'Activo';

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->save()) {
        $model->refresh();
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br>Agente Comision registrado correctamente.');
        //return $this->redirect(['view', 'id' => $model->idAgenteComision]);
        return $this->redirect(['index']);
      }
    } else {
      return $this->renderAjax('create', [
        'model' => $model,
      ]);
    }
  }

  /**
  * Updates an existing AgentesComisiones model.
  * If update is successful, the browser will be redirected to the 'view' page.
  * @param integer $id
  * @return mixed
  */
  public function actionUpdate($id, $submit = false)
  {
    $model = $this->findModel($id);

    if ( Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false ) {
      if ($model->validate()) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
      }
    }

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->save()) {
        $model->refresh();
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong><br>Agente Comision actualizado correctamente.');
        //return $this->redirect(['update', 'id' => $model->idBancos]);
        return $this->redirect(['index']);
      }
      else {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
      }
    }

    return $this->renderAjax('update', [
      'model' => $model,
    ]);


    /* if ($model->load(Yii::$app->request->post()) && $model->validate()) {
    if ($model->save()) {
    return $this->redirect(['index']);
  }
} else {
return $this->renderAjax('update', [
'model' => $model,
]);
}*/
}

/**
* Deletes an existing AgentesComisiones model.
* If deletion is successful, the browser will be redirected to the 'index' page.
* @param integer $id
* @return mixed
*/
public function actionDelete($id)
{
  $this->findModel($id)->delete();

  return $this->redirect(['index']);
}

/**
* Finds the AgentesComisiones model based on its primary key value.
* If the model is not found, a 404 HTTP exception will be thrown.
* @param integer $id
* @return AgentesComisiones the loaded model
* @throws NotFoundHttpException if the model cannot be found
*/
protected function findModel($id)
{
  if (($model = AgentesComisiones::findOne($id)) !== null) {
    return $model;
  } else {
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}


}
