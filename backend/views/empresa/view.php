<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Funcionario;
use backend\models\Clientes;
use backend\models\FormasPago;
use backend\models\Compra_view;
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;

/* @var $this yii\web\View */
/* @var $model backend\models\FacturasDia */

$this->title = $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Facturas Dias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $model->idCabeza_Factura])->all();
if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
    echo '<strong>Cliente: </strong>'.$cliente->nombreCompleto.'<br>
      <strong>Teléfono: </strong>'.$cliente->telefono.'<br>
      <strong>Dirección: </strong>'.$cliente->direccion.'<br>';
} else{
    echo '<strong>Cliente: </strong>'.$model->idCliente.'<br><br><br>';
}


?>


<div class="facturas-caja-view">
    <div class="panel panel-default">
        <div class="panel-heading">
          <br>
            <table width="100%">
                <tr>
                    <td>
                        <?php
                            echo '<strong>Fecha ingreso</strong>: '.$model->fecha_inicio;
                        ?>
                        <br>
                        <?php
                            if(@$modelF = Funcionario::find()->where(['idFuncionario'=>$model->codigoVendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                {

                                    echo "<strong>Vendedor</strong>: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2." ";
                                    echo "<br>";
                                }
                        ?>

                        <?php
                            echo '<strong>Estado de la factura</strong>: '.$model->estadoFactura;
                        ?>

                    </td>
                    <td>
                        <?php
                            $fecha_final = '';
                                if ($model->fecha_final=='') {
                                    $fecha_final = 'Pendiente';
                                } else {
                                    $fecha_final = $model->fecha_final;
                                }
                                echo '<strong>Fecha Final</strong>: '.$fecha_final;
                        ?><br>
                        <?php

                            if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
                                if ($model->estadoFactura!='Crédito') {
                                    echo "<strong>Tipo de pago</strong>: ".$modelP->desc_forma."<br>";
                                }
                                else {
                                    echo "<strong>Fecha Vencimiento</strong>: ".$model->fecha_vencimiento."<br>";
                                }
                            }
                            echo "<strong>Observación</strong>: ".$model->observacion;
                        ?>


                    </td>
                </tr>
            </table><br>
            </div>
            <div class="panel-body">
            <div class="col-lg-12">
                <?php
                        echo '<table class="items table table-striped" width="100%"';
                        echo '<thead>';
                        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th></tr>',
                                'CAN',
                                'No. FACTURA',
                                'DESCRIPCIÓN',
                                'PREC UNIT',
                                'DESC',
                                'IV %',
                                'TOTAL'
                                );
                        echo '</thead>';

                        if($products) {
                        echo '<tbody>';
                            foreach($products as $position => $product) {
                                $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                                $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
                                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>',
                                        $product['cantidad'],
                                        $product['codProdServicio'],
                                        $descrip,
                                        number_format($product['precio_unitario'], 2),
                                        number_format($product['descuento_producto'],2),//Descuento
                                        number_format($product['subtotal_iva'],2),
                                        number_format(($product['precio_unitario']*$product['cantidad']),2)

                                        );
                            }
                        echo '</tbody>';
                    }
                        echo '</table><div class="" >';
                        echo '<div class="resumen">
                           <h3 align="right">RESUMEN</h3>
                           <table class="" align="right">
                            <tbody>
                                <tr>
                                     <td class="detail-view">
                                        <table>
                                            <tr>
                                                <th align="left">SUBTOTAL: </th>
                                                <td align="right">'.number_format($model->subtotal,2).'</td>
                                            </tr>
                                            <tr>
                                                <th align="left">TOTAL DESCUENTO: &nbsp;</th>
                                                <td align="right">'.number_format($model->porc_descuento,2).'</td>
                                            </tr>
                                            <tr>
                                                <th align="left">TOTAL IV: </th>
                                                <td align="right"><span>'.number_format($model->iva,2).'</span></td>
                                            </tr>
                                             <tr>
                                                <th align="left">TOTAL A PAGAR: </th>
                                                <td align="right"><span><strong>'.number_format($model->total_a_pagar,2).'</strong></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                            </div>';
                        echo '</div>';
                ?>
            </div>
        </div>
    </div>

</div>
