<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Empresa */

$this->title = 'Actualizar Empresa: ' . ' ' . $model->nombre;
// $this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->idEmpresa, 'url' => ['view', 'id' => $model->idEmpresa]];
$this->params['breadcrumbs'][] = 'Actualizar información de empresa';
$checked = '';
if ($model->factun_conect==1) {
  $checked = 'checked';
}

?>
<style>
input[type=checkbox] {
    zoom:1.8;
}
</style>
<div class="empresa-update">

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <div class="col-lg-4">
        <?= Html::a('Regresar', ['/site/index'], ['class' => 'btn btn-default']) ?>
    </div>
    <div class="col-lg-8">
      <center>
        <label for="">Marque esta casilla para activar factura electrónica:</label><br>
        <input id="factun_activo" type="checkbox" name="factun_activo" value="" title="Al seleccionar esta casilla confirma que está aprobando que tiene una cuenta en factun para factura electrónica" <?= $checked ?> >
      </center>
      <div class="panel panel-info" style="height:100%; width:100%; overflow-y: auto; " id="div_factun"><br>
        <div class="col-xs-12"><center><strong>Importante:</strong> Ingrese correctamente los datos y no deje la casilla marcada si no va a usar Factura electrónica porque el sistema genera conflictos de usabilidad.</center><br></div>
        <div class="col-xs-4">
          <label for="email">Usuario factun (email)</label>
          <input type="email" value="<?= $model->user_factun ?>" name="email" class="form-control input-sm" id="user_factun" placeholder="Usuario factun" required>
        </div>
        <div class="col-xs-4">
          <label for="password">Password</label>
          <div class="input-group">
            <input type="password" value="<?= $model->pass_factun ?>" name="password" class="form-control input-sm" id="pass_factun" placeholder="Password Factun" required>
            <span class="input-group-btn">
              <button class="btn btn-default btn-sm" type="button" title="Comprueba conexión con Factun"><i class="fa fa-key" aria-hidden="true"></i></button>
            </span>
          </div>
        </div>
        <div class="col-xs-2">
          <label for="">ID Compañía</label>
          <input type="text" name="compania_id" value="<?= $model->compania_id ?>" class="form-control input-sm" id="compania_id" placeholder="ID Compañia" required>
        </div>
        <div class="col-xs-2">
          <label for="">ID sucursal</label>
          <input type="text" name="sucursal_id" value="<?= $model->sucursal_id ?>" class="form-control input-sm" id="sucursal_id" placeholder="ID Sucursal" required><br>
        </div>
        <div class="col-xs-12">
          <a class="btn btn-warning btn-sm btn-block" id="actualizar_pago_tr" onclick="actualizar_factun(this)" data-loading-text="Espere, guardando conexión..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar datos de Factun para factura electrónica</a>
          <div id="resultado_coneccion"></div>
          <br>
        </div>
      </div>
    </div>
    <div class="col-lg-12"><br>
      <?= $this->render('_form', [
          'model' => $model,
      ]) ?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
  $("#div_factun").css("display","none");
  var factun_conect = '<?= $model->factun_conect ?>';
        //me permite usar el rengo de fechas para obtener la cantidad sujerida
        $('#factun_activo').on('click', function() {
          var factun_conect = 0;
            if (this.checked == true){//Compruebo check---------------------------------------------------
              $("#div_factun").css("display","");
              factun_conect = 1;
            } else {//---------------------------------------------------
              $("#div_factun").css("display","none");
              factun_conect = 0;
            }//---------------------------------------------------
            $.ajax({
              url: "<?= Yii::$app->getUrlManager()->createUrl('empresa/activar_factun') ?>",
              type:"post",
              data : {'factun_conect' : factun_conect},
              success: function (data) { }
            });
        });
        if ( factun_conect == 1 ) {
          $("#div_factun").css("display","");
        }

});

//funcion que me permite agregar los datos de factura electronica de factun
function actualizar_factun(this_boton) {
  var user_factun = document.getElementById('user_factun').value;
  var pass_factun = document.getElementById('pass_factun').value;
  var compania_id = document.getElementById('compania_id').value;
  var sucursal_id = document.getElementById('sucursal_id').value;

  if (user_factun == '' || pass_factun == '' || compania_id == '' || sucursal_id == '') {
    document.getElementById("resultado_coneccion").innerHTML = '<center><span class="label label-danger">No deje espacios vacios.</span></center>';
    setTimeout(function() {//aparece la alerta en un milisegundo
        $("#resultado_coneccion").fadeIn();
    });
    setTimeout(function() {//se oculta la alerta luego de 4 segundos
        $("#resultado_coneccion").fadeOut();
    },3000);
  } else {
    var $btn = $(this_boton).button('loading');
    $.ajax({
      url: "<?= Yii::$app->getUrlManager()->createUrl('empresa/guardar_datos_factun') ?>",
      type:"post",
      data : {'user_factun' : user_factun, 'pass_factun' : pass_factun,
              'compania_id' : compania_id, 'sucursal_id' : sucursal_id },
      beforeSend: function() {
        setTimeout( function () { $btn.button('reset'); }, 60000 );
      },
      success: function (notif) {
        document.getElementById("resultado_coneccion").innerHTML = notif;
        setTimeout(function() {//aparece la alerta en un milisegundo
            $("#resultado_coneccion").fadeIn();
        });
        setTimeout(function() {//se oculta la alerta luego de 4 segundos
            $("#resultado_coneccion").fadeOut();
        },5000);

      }
    });
  }
  }
</script>
<?php
/*
class Create_database
{
 protected $pdo;

 public function __construct()
 {
 $this->pdo = new PDO("mysql:host=localhost;", "root", "jsky1987");
 }
 //creamos la base de datos y las tablas que necesitemos
 public function my_db()
 {
   $mi_bd = 'mi_bd';
        //creamos la base de datos si no existe
 $crear_db = $this->pdo->prepare('CREATE DATABASE IF NOT EXISTS '.$mi_bd.' COLLATE utf8_spanish_ci');
 $crear_db->execute();

 //decimos que queremos usar la tabla que acabamos de crear
 if($crear_db):
 $use_db = $this->pdo->prepare('USE '.$mi_bd);
 $use_db->execute();
 endif;

 //si se ha creado la base de datos y estamos en uso de ella creamos las tablas
 if($use_db):
 //creamos la tabla usuarios
 $crear_tb_users = $this->pdo->prepare("
 CREATE TABLE `user` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
   `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
   `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
   `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
   `status` smallint(6) DEFAULT '10',
   `created_at` int(11) DEFAULT NULL,
   `updated_at` int(11) DEFAULT NULL,
   `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
   `codigo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
   `tipo_usuario` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
   `verification_code` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
   `sesion` int(1) DEFAULT NULL,
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`nombre`,`codigo`,`tipo_usuario`,`verification_code`,`sesion`) values
 (1,'admin00','o6RaN6aCt_6bvQy-4nnrpKWTcwnyPpBQ','------',NULL,'jeank_sky@hotmail.com',10,1445293540,1447790078,'Administrador del Sistema','1','Administrador','15628465',0);
");
 $crear_tb_users->execute();

 endif;

 }
}
//ejecutamos la función my_db para crear nuestra bd y las tablas
$db = new Create_database();
$db->my_db();
*/
?>
