<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="empresa-form">
<!--?php $form = ActiveForm::begin(); ?-->
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Ingrese los datos que se le solicitan en el siguiente formulario.
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="col-lg-4">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'direccion')->textarea(['maxlength' => 130]) ?>

                <?= $form->field($model, 'localidad')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'ced_juridica')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'telefono')->
                            widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999-99-99',
                            ]) ?>
                <?= $form->field($model, 'fax')->
                            widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999-99-99',
                            ]) ?>



                <?= $form->field($model, 'sitioWeb')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'email')->
                widget(\yii\widgets\MaskedInput::className(), [
                            'name' => 'input-36',
                            'clientOptions' => [
                            'alias' =>  'email'
                             ],
                        ]) ?>
                <?= $form->field($model, 'email_gerencia')->
                widget(\yii\widgets\MaskedInput::className(), [
                            'name' => 'input-36',
                            'clientOptions' => [
                            'alias' =>  'email'
                             ],
                        ]) ?>
                <?= $form->field($model, 'email_proveeduria')->
                widget(\yii\widgets\MaskedInput::className(), [
                            'name' => 'input-36',
                            'clientOptions' => [
                            'alias' =>  'email'
                             ],
                        ]) ?>
                <?= $form->field($model, 'email_contabilidad')->
                widget(\yii\widgets\MaskedInput::className(), [
                            'name' => 'input-36',
                            'clientOptions' => [
                            'alias' =>  'email'
                             ],
                        ]) ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'email_notificaciones')->
                widget(\yii\widgets\MaskedInput::className(), [
                            'name' => 'input-36',
                            'clientOptions' => [
                            'alias' =>  'email'
                             ],
                        ]) ?>
                <!--?=  $form->field($model, 'file')->widget(FileInput::classname(), [
                'options'=>['accept'=>'image/*'],
                'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png']
                ]]);?-->
                <label>Logo de la empresa</label>
                <?php
                if ($model->logo) {
                    echo '<br> <div class="panel panel-default"><br><center>';
                    echo '<img src="'.\Yii::$app->request->BaseUrl.'/'.$model->logo.'" width="130px">&nbsp;&nbsp;&nbsp;';
                    echo Html::a('<i class="fa fa-trash-o fa-fw"></i>', ['deleteimg', 'id'=>$model->idEmpresa, 'field'=> 'logo'], ['class'=>'btn btn-danger']).'<p><br>';
                echo '</center> </div>';
                } else { ?>

                <?php echo $form->field($model, 'file')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]]);
                } ?>
                <label>Imágen de etiquetas (150x150 o 240x150)</label>
                <?php
                if ($model->logo_etiqueta) {
                    echo '<br> <div class="panel panel-default"><br><center>';
                    echo '<img src="'.\Yii::$app->request->BaseUrl.'/'.$model->logo_etiqueta.'" width="130px">&nbsp;&nbsp;&nbsp;';
                    echo Html::a('<i class="fa fa-trash-o fa-fw"></i>', ['deleteimg', 'id'=>$model->idEmpresa, 'field'=> 'logo_etiqueta'], ['class'=>'btn btn-danger']).'<p><br>';
                echo '</center> </div>';
                } else { ?>
                <?php echo $form->field($model, 'file_et')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]]);
                } ?>

                <div class="form-group" style="text-align:right">
                    <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar datos de la empresa', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary btn-block']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        </div>
    </div>
    <!-- /.panel -->
</div>
