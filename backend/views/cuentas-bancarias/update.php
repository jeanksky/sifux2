<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CuentasBancarias */

$this->title = 'Actualizar cuenta bancaria #: ' . ' ' . $model->idBancos;
$this->params['breadcrumbs'][] = ['label' => 'Cuentas Bancarias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idBancos, 'url' => ['view', 'id' => $model->idBancos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cuentas-bancarias-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
