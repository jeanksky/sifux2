<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use backend\models\Moneda;
use backend\models\EntidadesFinancieras;
use kartik\widgets\AlertBlock;
//----------------------------------------------Para la modal de elimacion por permiso admin
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario
//use kartik\grid\GridView;
//alertas http://librosweb.es/libro/bootstrap_3/capitulo_6/mensajes_de_alerta.html
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuentasBancariasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
$this->title = 'Cuentas Bancarias';
$this->params['breadcrumbs'][] = $this->title;

?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->
<!--script type="text/javascript" src="<?php //echo $jquery_min_js; ?>"></script-->
<script type="text/javascript">
    //muestra la vista para crear cuenta bancaria
    $(document).on('click', '#activity-link-create', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-create-cuenta').html(data);//muestro en el div los datos del create
                        $('#crearcuentamodal').modal('show');//muestro la modal

                    }
                );
            }));
    $(document).on('click', '#activity-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        document.getElementById("well-create-cuenta").innerHTML="";//tengo que destruir el contenido del div de well-create-movimiento para que well-update-movimiento esté limpio para la modal
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-update-cuenta').html(data);//muestro en el div los datos del create
                        $('#actualizarcuentamodal').modal('show');//muestro la modal

                    }
                );
            }));
    //Esta funcion me carga el idBancos para eliminar su respectiva cuenta bancaria
    function enviaResultado(id) {
        $('#org').val(id);
        }

    //me envia los datos al controlador que procede a almacenarlos
    function agrega_moneda(){
        var abrevi = document.getElementById('abrevi').value;
        var descrip = document.getElementById('descrip').value;
        var simbol = document.getElementById('simbol').value;

        if (abrevi =='' || descrip == '' || simbol =='') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/agrega_moneda') ?>",
                type:"post",
                data: { 'abrevi': abrevi, 'descrip' : descrip, 'simbol': simbol },
                success: function(data_){
                    $("#index-monedas").empty().load(location.href+' #index-monedas','');
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/categoria_tipo_cambio') ?>", function( data ) {
                      //document.getElementById("ref_cambio").innerHTML = 'data';
                      $('#ref_cambio').html(data);
                      //$("#ref_cambio").load(data);
                    });
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                    //$("#ddl-moneda").select2("destroy");
                    //$("#ref_cambio").empty().load(location.href+' #ref_cambio','');
                    //$('#ddl-moneda').empty().append('<option value=""></option>').select2("val", "");
                    $('#abrevi').val('');
                    $('#descrip').val('');
                    $('#simbol').val('');
                    //$('#modalcreatemonedas').hide("slow");
                    //$('#modalcreatemonedas').modal('toggle');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 57 (informar a Nelux)');
                }//fin error
            }); //fin ajax
        }//fin else
    }//fin de la funsion
    //me envia al controlador el dato a consultar para actualizar
    function actualiza_modal_moneda(idTipo_moneda) {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualiza_modal_moneda') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idTipo_moneda': idTipo_moneda },
                success: function(data){
                    $('#_idTipo_moneda').val(data.idTipo_moneda);
                    $('#_abrevi').val(data.abreviatura);
                    $('#_descrip').val(data.descripcion);
                    $('#_simbol').val(data.simbolo);
                    //$("#update-monedas").load(location.href+' #update-monedas','');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 77 (informar a Nelux)');
                }
            });
    }
    //me permite enviar al controlador los datos modificados en la modal
    function actualizar_moneda() {
        var idTipo_moneda = document.getElementById('_idTipo_moneda').value;
        var abrevi = document.getElementById('_abrevi').value;
        var descrip = document.getElementById('_descrip').value;
        var simbol = document.getElementById('_simbol').value;
        if (abrevi =='' || descrip == '' || simbol =='') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualizar_moneda') ?>",
                type:"post",
                data: { 'idTipo_moneda': idTipo_moneda, 'abrevi': abrevi, 'descrip' : descrip, 'simbol': simbol },
                success: function(data){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/categoria_tipo_cambio') ?>", function( data ) {
                      $('#ref_cambio').html(data);
                    });
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                    $("#index-monedas").load(location.href+' #index-monedas','');
                    $('#modalActualizamoneda').hide("slow");
                    $('#modalActualizamoneda').modal('toggle');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 106 (informar a Nelux)');
                }
            });
        }
    }

    //enviar consulta al controlador para eliminar moneda seleccionada
    function eliminarmoneda(idTipo_moneda) {
        if(confirm("Está seguro de eliminar esta moneda")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/elimina_moneda') ?>",
                type:"post",
                data: { 'idTipo_moneda': idTipo_moneda },
                success: function(data){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/categoria_tipo_cambio') ?>", function( data ) {
                      $('#ref_cambio').html(data);
                    });
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                    $("#index-monedas").load(location.href+' #index-monedas','');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 129 (informar a Nelux)');
                }
            });
        }
    }

    //me permite seleccionar una moneda como local, enviando el dato de la moneda al controlador
    function local_moneda(value) {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/local_moneda') ?>",
                type:"post",
                data: { 'idTipo_moneda': value },
                success: function(data){
                },
                error: function(msg, status,err){
                 //alert('No pasa 144');
                }
            });
    }

    //envia al controldor las entidades financieras a guardar en base de datos.
    function agrega_entidad() {
        var abrevi = document.getElementById('abrevi2').value;
        var descrip = document.getElementById('descrip2').value;
        var telefono1 = document.getElementById('telefono1').value;
        var telefono2 = document.getElementById('telefono2').value;
        if (abrevi =='' || descrip == '') {
            alert('Por favor no deje campos vacios, solo el 2do teléfono se acepta nulo');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/agrega_entidad_fi') ?>",
                type:"post",
                data: { 'abrevi': abrevi, 'descrip' : descrip, 'telefono1': telefono1, 'telefono2': telefono2 },
                success: function(data){
                    $("#index-entidades").load(location.href+' #index-entidades','');
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/categoria_tipo_cambio') ?>", function( data ) {
                      $('#ref_cambio').html(data);
                    });
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                    //location.reload();
                    $('#abrevi2').val('');
                    $('#descrip2').val('');
                    $('#telefono1').val('');
                    $('#telefono2').val('');
                    $('#modalcreateentidad').hide("slow");
                    $('#modalcreateentidad').modal('toggle');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 179 (informar a Nelux)');
                }
            });
        }
    }
    //me envia al controlador el dato a consultar para actualizar entidad
    function actualiza_modal_entidad(idEntidadf) {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualiza_modal_entidad') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idEntidadf': idEntidadf },
                success: function(data){
                    $('#_idEntidad_financiera').val(data.idEntidad_financiera);
                    $('#abrevi2_').val(data.abreviatura);
                    $('#descrip2_').val(data.descripcion);
                    $('#telefono1_').val(data.telefono1);
                    $('#telefono2_').val(data.telefono2);
                    //$("#update-monedas").load(location.href+' #update-monedas','');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 200 (informar a Nelux)');
                }
            });
    }
    //me permite enviar al controlador los datos modificados en la modal
    function actualizar_entidad() {
        var idEntidad_financiera = document.getElementById('_idEntidad_financiera').value;
        var abrevi = document.getElementById('abrevi2_').value;
        var descrip = document.getElementById('descrip2_').value;
        var telefono1 = document.getElementById('telefono1_').value;
        var telefono2 = document.getElementById('telefono2_').value;
        if (abrevi =='' || descrip == '' || simbol =='') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualizar_entidad') ?>",
                type:"post",
                data: { 'idEntidad_financiera': idEntidad_financiera, 'abrevi': abrevi, 'descrip' : descrip, 'telefono1': telefono1, 'telefono2': telefono2 },
                success: function(data){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/categoria_tipo_cambio') ?>", function( data ) {
                      $('#ref_cambio').html(data);
                    });
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                    $("#index-entidades").load(location.href+' #index-entidades','');
                    $('#modalActualizaentidad').hide("slow");
                    $('#modalActualizaentidad').modal('toggle');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 230 (informar a Nelux)');
                }
            });
        }
    }
    function eliminarentidad(idEntidad_financiera) {
        if(confirm("Está seguro de eliminar esta entidad")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/elimina_entidad') ?>",
                type:"post",
                data: { 'idEntidad_financiera': idEntidad_financiera },
                success: function(data){
                    $("#index-entidades").load(location.href+' #index-entidades','');
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/categoria_tipo_cambio') ?>", function( data ) {
                      $('#ref_cambio').html(data);
                    });
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 251 (informar a Nelux)');
                }
            });
        }
    }

    $(document).ready(function(){
        $('[data-toggle="modal"]').tooltip();
    });
</script>
<?php
  if(!isset($_REQUEST['1']))
        {}
        else{
        if ($_GET['1'] == 'entidades') {
            echo '
                <script type="text/javascript">
                    $(window).load(function(){
                        $("#modalindexentidadesf").modal("show");
                    });
                </script>
            ';
        } else if ($_GET['1'] == 'moneda') {
            echo '
                <script type="text/javascript">
                    $(window).load(function(){
                        $("#modalindexmonedas").modal("show");
                    });
                </script>
            ';
        } else if ($_GET['1'] == 'categoria'){
            echo '
                <script type="text/javascript">
                    $(window).load(function(){
                        $("#cambio").addClass("active"); //activo la pestaña tav
                        $("#cambio_a").tab("show"); //muestro lo que contiene la pestaña
                    });
                </script>';
        } else if ($_GET['1'] == 'documento'){
            echo '
                <script type="text/javascript">
                    $(window).load(function(){
                        $("#documento").addClass("active"); //activo la pestaña tav
                        $("#documento_a").tab("show"); //muestro lo que contiene la pestaña
                    });
                </script>';
        } else if ($_GET['1'] == 'cambio'){
            echo '
                <script type="text/javascript">
                    $(window).load(function(){
                        $("#tcambio").addClass("active"); //activo la pestaña tav
                        $("#tcambio_a").tab("show"); //muestro lo que contiene la pestaña
                    });
                </script>';
        }
    }
?>
<style>
    #crearcuentamodal .modal-dialog{/*tamaño de modal crear cuenta bancaria*/
    width: 70%!important;
    }

    #actualizarcuentamodal .modal-dialog{/*tamaño de modal actualizar cuenta bancaria*/
    width: 70%!important;
    }
    #modalcreateentidad .modal-dialog{/*tamaño de modal crear entidad*/
    width: 40%!important;
    }

    #modalActualizaentidad .modal-dialog{/*tamaño de modal actualizar entidad*/
    width: 40%!important;
    }
</style>
<div class="cuentas-bancarias-index">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>
    <h1><?= Html::encode($this->title) ?></h1>
    <p style="text-align:right">
        <?= Html::a('Monedas', '#', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modalindexmonedas']) ?>
        <?= Html::a('Entidades financieras', '#', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modalindexentidadesf']) ?>
    </p>
    <div id="tabs" >
        <ul class="nav nav-tabs nav-justified">
            <li class="active" role="presentation" ><a href="#cuentas" data-toggle="tab">Datos de cuentas bancarias</a></li>
            <li role="presentation"><a href="#cambio" id="cambio_a" data-toggle="tab">Categoría tipo de cambio</a></li>
            <li role="presentation"><a href="#tcambio" id="tcambio_a" data-toggle="tab">Tipo cambio</a></li>
            <li role="presentation"><a href="#documento" id="documento_a" data-toggle="tab">Tipo documento</a></li>
        </ul>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <div class="table-responsive tab-content">
            <div class="tab-pane fade in active" id="cuentas"><br>
            <center><h2>Datos de cuentas bancarias</h2></center>
            <p style="text-align:right">
                <?= Html::a('Crear nueva cuenta bancaria', '#', [
                    'id' => 'activity-link-create',
                    'data-toggle' => 'modal',
                    'class' => 'btn btn-success',
                    //'data-trigger'=>'hover',
                    //'data-target' => '#crearmovimientomodal',
                    'data-url' => Url::to(['create']),
                    'data-pjax' => '0',
                    'title' => Yii::t('app', 'Proceda a crear una nueva cuenta bancaria'),
                ]) ?>
            </p>
            <?php  $dataProvider->pagination->pageSize = 10; ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'idBancos',
                    //'idTipo_moneda',
                    [
                        'options'=>['style'=>'width:30px'],
                        'attribute' => 'tbl_moneda.descripcion',
                        'label' => 'Moneda',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $moneda = Moneda::findOne($model->idTipo_moneda);
                            if($moneda == '')
                            return 'moneda no asignada';
                            else
                            return $moneda->descripcion;
                        },
                    ],
                    //'idEntidad_financiera',
                    [
                        'attribute' => 'tbl_entidades_financieras.descripcion',
                        'label' => 'Entidad Financiera',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $entidad = EntidadesFinancieras::findOne($model->idEntidad_financiera);
                            if($entidad == '')
                            return 'Entidad financiera no asignada';
                            else
                            return $entidad->descripcion;
                        },
                    ],
                    'numero_cuenta',
                    'descripcion',
                    // 'tipo_cuenta',
                    // 'cuenta_cliente',
                     //'saldo_libros',
                     //'saldo_bancos',
                     [
                        'attribute' => 'saldo_libros',//'format' => ['decimal',2],
                        'value' => function ($model, $key, $index, $grid) {
                            $mo = Moneda::findOne($model->idTipo_moneda);
                            return $mo->simbolo.number_format($model->saldo_libros,2);
                        },
                        //'options'=>['style'=>'width:15%'],
                     ],
                     [
                        'attribute' => 'saldo_bancos',//'format' => ['decimal',2],
                        'value' => function ($model, $key, $index, $grid) {
                            $mo = Moneda::findOne($model->idTipo_moneda);
                            return $mo->simbolo.number_format($model->saldo_libros,2);//para que muestre lo mismo de libros
                            //return $mo->simbolo.number_format($model->saldo_bancos,2);// se deja pendiente para cuando se programe concileacion bancaria automatica
                        },
                        //'options'=>['style'=>'width:15%'],
                     ],

                    ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}{delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                    'id' => 'activity-link-update',
                                    'data-toggle' => 'modal',
                                    //'data-trigger'=>'hover',
                                    //'data-target' => '#crearmovimientomodal',
                                    'data-url' => Url::to(['update', 'id' => $model->idBancos]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Proceda a modificar la cuenta bancaria'),
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {

                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                    'id' => 'activity-index-link-delete',
                                    'class' => 'activity-delete-link',
                                    'title' => Yii::t('app', 'Eliminar Cuenta bancaria '.$model->numero_cuenta),
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalDelete',
                                    'onclick' => 'javascript:enviaResultado('.$model->idBancos.')',
                                    'href' => Url::to('#'),
                                    'data-pjax' => '0',
                                    ]);
                            },
                    ],
                    ],
                ],
            ]);


            ?>
            </div>
            <div class="tab-pane fade" id="cambio"><br>
                <div id="ref_cambio"><?= $this->render('categoria_tipo_cambio') ?></div>
            </div>
            <div class="tab-pane fade" id="tcambio"><br>
                <div id="ref_t_cambio"><?= $this->render('tipo_cambio') ?></div>
            </div>
            <div class="tab-pane fade" id="documento"><br>
                <?= $this->render('tipo_documento', ['us'=>$us]) ?>
            </div>
        </div>
    </div>
</div>


<?php
//--------------------------------MODAL PARA EL MODULO DE CUENTAS BANCARIAS

//------------------------modal para crear cuenta bancaria
    Modal::begin([
        'id' => 'crearcuentamodal',
        'size'=>'modal-lg',
        'header' => '<center><h4 class="modal-title">Crear cuenta bancaria</h4></center>',
        'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
    ]);
    echo "<div class='panel-body'><div id='well-create-cuenta'></div></div>";

    Modal::end();
//------------------------modal para actualizar cuenta bancaria
    Modal::begin([
        'id' => 'actualizarcuentamodal',
        'size'=>'modal-lg',
        'header' => '<center><h4 class="modal-title">Actualizar cuenta bancaria</h4></center>',
        'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
    ]);
    echo "<div class='panel-body'><div id='well-update-cuenta'></div></div>";

    Modal::end();

//--------------------------------MODAL PARA EL MODULO DE MONEDAS

//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalindexmonedas',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Monedas</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar nuevo tipo de moneda', '#', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modalcreatemonedas',]),
        ]);
    echo '<div class="well">';
    echo '<div id="index-monedas">';
    echo '<div class="table-responsive">';
    echo '<div id="semitransparente">';
    echo '<table class="items table table-striped" id="tabla_lista_monedas" >';
        echo '<thead>';
        printf('<tr><th>%s</th><th>%s</th><th><center>%s</center></th><th class="actions button-column">&nbsp;</th></tr>',
                'ABREVIATURA',
                'DESCRIPCIÓN',
                'SÍMBOLO'
                );
        echo '</thead>';
        $moneda = Moneda::find()->orderBy(['idTipo_moneda' => SORT_DESC])->all();
        echo '<tbody>';
        foreach($moneda as $position => $mon) {
            $eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-1x', 'onclick'=>'javascript:eliminarmoneda('.$mon['idTipo_moneda'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar moneda') ]);
            $actualizar = Html::a('', '#', [
                            'class'=>'fa fa-pencil fa-1x',
                            'id' => 'activity-index-link-moneda',
                            'data-toggle' => 'modal',
                            'onclick'=>'javascript:actualiza_modal_moneda("'.$mon['idTipo_moneda'].'")',
                            'data-target' => '#modalActualizamoneda',
                            //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Actualizar moneda') ]);
            $accion = $actualizar.' '.$eliminar;
            printf('<tr><td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                            $mon['abreviatura'],
                            $mon['descripcion'],
                            $mon['simbolo'],
                            $accion
                        );
        }
        echo '</tbody>';
    echo '</table>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';

    Modal::end();
?>

<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalcreatemonedas',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Crear nueva moneda</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar', '#', ['class' => 'btn btn-success', 'data-dismiss'=>'modal', 'onclick'=>'agrega_moneda()']),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="create-monedas">';
                echo '<div class="col-lg-4">';
                echo '<label>Abreviatura: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'abrevi', 'placeholder' =>' Abreviatura', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-4">';
                echo '<label>Descripción: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'descrip', 'placeholder' =>' Descripción', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-4">';
                echo '<label>Símbolo: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'simbol', 'placeholder' =>' Símbolo', 'readonly' => false, 'onchange' => '', "onkeypress"=>"", "maxlength"=>"1"]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>

<?php
//muestra modal para actualizar moneda cargada
    Modal::begin([
            'id' => 'modalActualizamoneda',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Actualizar moneda</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Actualizar', '#', ['class' => 'btn btn-success', 'onclick'=>'actualizar_moneda()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="update-monedas">';
                echo '<div class="col-lg-4">';
                echo' <input type="hidden" name="position" id="_idTipo_moneda">';
                echo '<label>Abreviatura: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => '_abrevi', 'placeholder' =>' Abreviatura', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-4">';
                echo '<label>Descripción: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => '_descrip', 'placeholder' =>' Descripción', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-4">';
                echo '<label>Símbolo: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => '_simbol', 'placeholder' =>' Símbolo', 'readonly' => false, 'onchange' => '', "onkeypress"=>"", "maxlength"=>"1"]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>

<?php


//--------------------------------MODAL PARA EL MODULO DE ENTIDADES FINANCIERAS


//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalindexentidadesf',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Entidades financieras</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar nueva entidad financiera', '#', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modalcreateentidad',]),
        ]);
    echo '<div class="well">';
    echo '<div id="index-entidades">';
    echo '<div class="table-responsive">';
    echo '<div id="semitransparente">';
    echo '<table class="items table table-striped" id="tabla_lista_entidades" >';
        echo '<thead>';
        printf('<tr><th>%s</th><th>%s</th><th><center>%s</center></th><th class="actions button-column">&nbsp;</th></tr>',
                'ABREVIATURA',
                'DESCRIPCIÓN',
                'TELÉFONO(S)'
                );
        echo '</thead>';
        $entidadesfinancieras = EntidadesFinancieras::find()->orderBy(['idEntidad_financiera' => SORT_DESC])->all();
        echo '<tbody>';
        foreach($entidadesfinancieras as $position => $entidad) {
            $eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-1x', 'onclick'=>'javascript:eliminarentidad('.$entidad['idEntidad_financiera'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar entidad') ]);
            $actualizar = Html::a('', '#', [
                            'class'=>'fa fa-pencil fa-1x',
                            'id' => 'activity-index-link-entidad',
                            'data-toggle' => 'modal',
                            'onclick'=>'javascript:actualiza_modal_entidad("'.$entidad['idEntidad_financiera'].'")',
                            'data-target' => '#modalActualizaentidad',
                            //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Actualizar entidad') ]);
            $accion = $actualizar.' '.$eliminar;
            printf('<tr><td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                            $entidad['abreviatura'],
                            $entidad['descripcion'],
                            $entidad['telefono1'].' / '.$entidad['telefono2'],
                            $accion
                        );
        }
        echo '</tbody>';
    echo '</table>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';

    Modal::end();
?>
</div>
</div>
<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalcreateentidad',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Crear nueva entidad financiera</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar', '#', ['class' => 'btn btn-success', 'onclick'=>'agrega_entidad()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="create-monedas">';
                echo '<div class="col-lg-2">';
                echo '<label>Abreviatura: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'abrevi2', 'placeholder' =>' Abreviatura', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-4">';
                echo '<label>Descripción: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'descrip2', 'placeholder' =>' Descripción', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo '<label>1er Teléfono: </label>';
                echo \yii\widgets\MaskedInput::widget([
                    'name' => 'telefono1',
                    'id' => 'telefono1',
                    'mask' => '9999-99-99',
                ]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo '<label>2do Teléfono: </label>';
                echo \yii\widgets\MaskedInput::widget([
                    'name' => 'telefono2',
                    'id' => 'telefono2',
                    'mask' => '9999-99-99',
                ]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>

<?php
//muestra modal para actualizar entidad financiera cargada
    Modal::begin([
            'id' => 'modalActualizaentidad',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Actualizar entidad financiera</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Actualizar', '#', ['class' => 'btn btn-success', 'onclick'=>'actualizar_entidad()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="update-monedas">';
                echo '<div class="col-lg-2">';
                 echo' <input type="hidden" name="position" id="_idEntidad_financiera">';
                echo '<label>Abreviatura: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'abrevi2_', 'placeholder' =>' Abreviatura', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-4">';
                echo '<label>Descripción: </label>';
                echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'descrip2_', 'placeholder' =>' Descripción', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo '<label>1er Teléfono: </label>';
                echo \yii\widgets\MaskedInput::widget([
                    'name' => 'telefono1_',
                    'id' => 'telefono1_',
                    'mask' => '9999-99-99',
                ]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo '<label>2do Teléfono: </label>';
                echo \yii\widgets\MaskedInput::widget([
                    'name' => 'telefono2_',
                    'id' => 'telefono2_',
                    'mask' => '9999-99-99',
                ]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>

<?php
//------Eliminar cuenta bancaria modal
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar una cuenta bancaria necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "idBancos" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Cuenta Bancaria', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar esta cuenta bancaria?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
