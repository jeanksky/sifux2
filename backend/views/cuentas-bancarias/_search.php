<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CuentasBancariasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuentas-bancarias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idBancos') ?>

    <?= $form->field($model, 'idTipo_moneda') ?>

    <?= $form->field($model, 'idEntidad_financiera') ?>

    <?= $form->field($model, 'numero_cuenta') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'tipo_cuenta') ?>

    <?php // echo $form->field($model, 'cuenta_cliente') ?>

    <?php // echo $form->field($model, 'saldo_libros') ?>

    <?php // echo $form->field($model, 'saldo_bancos') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
