<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\CuentasBancarias */

$this->title = 'Crear nueva cuenta bancaria';
$this->params['breadcrumbs'][] = ['label' => 'Cuentas Bancarias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<p>NOTA: Para asignar un saldo en libros; luego de escribir el saldo en bancos seleccione Establecer saldo!</p>
<div class="cuentas-bancarias-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
