<?php
use yii\helpers\Html;
use backend\models\TipoDocumento;
use yii\bootstrap\Modal;
//use backend\models\Moneda;
//use backend\models\EntidadesFinancieras;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//me envia los datos al controlador que procede a almacenarlos
    function agrega_documento(){
        var descripcion = document.getElementById('descripcion').value;
    	var tipo = $("input[name='tipo']:checked").val();
        var estado = $("input[name='estado']:checked").val();

        if (descripcion =='' || tipo==null || estado==null ) {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/agrega_documento') ?>",
                type:"post",
                data: { 'descripcion': descripcion, 'tipo' : tipo, 'estado': estado },
                success: function(data){
                    $("#index-tipo_documento").load(location.href+' #index-tipo_documento','');
                    $('#descripcion').val('');
                    $("input:radio").attr("checked",false);//desmarca todos los radios buttons
                    $('#modalcreatedocumento').hide("slow");
                    $('#modalcreatedocumento').modal('toggle');
                },
                error: function(msg, status,err){
                 alert('No pasa 33');
                }
            });
        }
    }
    //envia la categoria que selecciono para obtener los datos de dicha categoria
    function actualiza_modal_documento(idTipo_documento) {
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualiza_modal_documento') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idTipo_documento': idTipo_documento },
                success: function(data){
                	$('#idTipo_documento_').val(data.idTipo_documento);
                    $('#descripcion_').val(data.descripcion);
                    $("input[name=tipo_][value='"+data.tipo_movimiento+"']").prop("checked",true);
                    $("input[name=estado_][value='"+data.estado+"']").prop("checked",true);
                    //$("#update-monedas").load(location.href+' #update-monedas','');
                },
                error: function(msg, status,err){
                 alert('No pasa - linea 52');
                }
            });
    }

    //envia al controlador la actualizacionde la categoria
    function actualiza_documento() {
    	var idTipo_documento = document.getElementById('idTipo_documento_').value;
        var descripcion = document.getElementById('descripcion_').value;
    	var tipo = $("input[name='tipo_']:checked").val();
        var estado = $("input[name='estado_']:checked").val();
        if (descripcion =='' || tipo == null || estado ==null) {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualizar_documento') ?>",
                type:"post",
                data: { 'idTipo_documento': idTipo_documento, 'descripcion': descripcion, 'tipo' : tipo, 'estado': estado },
                success: function(data){
                    $("#index-tipo_documento").load(location.href+' #index-tipo_documento','');
                    $('#modalactualizadocumento').hide("slow");
                    $('#modalactualizadocumento').modal('toggle');
                },
                error: function(msg, status,err){
                 //alert('No pasa 33');
                }
            });
        }
    }

    //Esta funcion me carga el idTipo_documento para eliminar su respectivo tipo de documento
    function enviaResultado(id) {
        $('#org').val(id);
        }

    //envia al controlador la orden de eliminar la categoria seleccionada
    function eliminardocumento(idTipo_documento) {
        if(confirm("Está seguro de eliminar este tipo de documento")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/elimina_documento') ?>",
                type:"post",
                data: { 'idTipo_documento': idTipo_documento },
                success: function(data){
                    $("#index-tipo_documento").load(location.href+' #index-tipo_documento','');
                },
                error: function(msg, status,err){
                 //alert('No pasa 94');
                }
            });
        }
    }
</script>
<style>
    #modalcreatedocumento .modal-dialog{
    width: 40%!important;
    }

    #modalactualizadocumento .modal-dialog{
    width: 40%!important;
    }
</style>
<center><h2>Tipo de documento</h2></center>
<p style="text-align:right">
    <?= Html::a('Crear nuevo tipo de documento', '#', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modalcreatedocumento']) ?>
</p>

<?php
echo '<div id="index-tipo_documento">';
    echo '<table class="items table table-striped" id="tabla_lista_entidades" >';
        echo '<thead>';
        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th class="actions button-column">&nbsp;</th></tr>',
                'ID',
                'DESCRIPCIÓN',
                'TIPO DE MOVIMIENTO',
                'ESTADO'
                );
        echo '</thead>';
        $documentos = TipoDocumento::find()->orderBy(['idTipo_documento' => SORT_DESC])->all();
        echo '<tbody>';
        foreach($documentos as $position => $documento) {
        	//$moneda = Moneda::find()->where(['idTipo_moneda'=>$categoria['idTipo_moneda']])->one();
        	//$entidad = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$categoria['idEntidad_financiera']])->one();
            //$eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-1x', 'onclick'=>'javascript:eliminardocumento('.$documento['idTipo_documento'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar tipo de documento') ]);
            $eliminar = Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                'id' => 'activity-index-link-delete',
                'class' => 'activity-delete-link',
                'title' => Yii::t('app', 'Eliminar tipo de documento'),
                'data-toggle' => 'modal',
                'data-target' => '#modalDeleteTD',
                'onclick' => 'javascript:enviaResultado('.$documento['idTipo_documento'].')',
                'href' => Url::to('#'),
                'data-pjax' => '0',
                ]);
            $actualizar = Html::a('', '#', [
                            'class'=>'fa fa-pencil fa-1x',
                            'id' => 'activity-index-link-documento',
                            'data-toggle' => 'modal',
                            'onclick'=>'javascript:actualiza_modal_documento("'.$documento['idTipo_documento'].'")',
                            'data-target' => '#modalactualizadocumento',
                            //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Actualizar tipo de documento') ]);
            $accion = $documento['idTipo_documento'] <= 2 ? '' : $actualizar.' '.$eliminar;
            printf('<tr><td><font face="arial" size=2>%s</font></td>
                <td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                            $documento['idTipo_documento'],
                            $documento['descripcion'],
                            $documento['tipo_movimiento'],
                            $documento['estado'],
                            $accion
                        );
        }
        echo '</tbody>';
    echo '</table>';
    echo '</div>';
?>
<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalcreatedocumento',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Crear nuevo tipo de documento</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar', '#', ['class' => 'btn btn-success', 'onclick'=>'agrega_documento()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">';
            echo '<div id="create-documento">';
                echo '<div class="col-lg-4">';
                echo '<label>Descripción: </label>';
                echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'descripcion', 'placeholder' =>' Descripción', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo '<label>Tipo de movimiento: </label>';
                $new = ['Débito' => 'Débito', 'Crédito' => 'Crédito'];
                echo Html::radioList('tipo', null, $new, ['class' => 'radio', 'id' => 'tipo']);
                echo '</div>';
                echo '<div class="col-lg-5">';
                echo '<label>Estado: </label>';
                $new = ['Activo' => 'Activo', 'Inactivo' => 'Inactivo'];
                echo Html::radioList('estado', null, $new, ['class' => 'radio', 'id' => 'estado']);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>

<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalactualizadocumento',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Actualizar categoría de tipo de cambio</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Actualizar', '#', ['class' => 'btn btn-success', 'onclick'=>'actualiza_documento()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">';
            echo '<div id="update-documento">';
                echo '<div class="col-lg-4">';
                echo '<label>Descripción: </label>';
                echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'descripcion_', 'placeholder' =>' Descripción', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo' <input type="hidden" name="position" id="idTipo_documento_">';
                echo '<label>Tipo de movimiento: </label>';
                $new = ['Débito' => 'Débito', 'Crédito' => 'Crédito'];
                echo Html::radioList('tipo_', null, $new, ['class' => 'radio', 'id' => 'tipo_']);
                echo '</div>';
                echo '<div class="col-lg-5">';
                echo '<label>Estado: </label>';
                $new = ['Activo' => 'Activo', 'Inactivo' => 'Inactivo'];
                echo Html::radioList('estado_', null, $new, ['class' => 'radio', 'id' => 'estado_']);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>
<?php
//------Eliminar cuenta bancaria modal
        Modal::begin([
            'id' => 'modalDeleteTD',
            'header' => '<h4 class="modal-title">Para eliminar un tipo de documento necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "idTipo_documento" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar tipo de documento', ['elimina_documento'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este tipo de documento?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
