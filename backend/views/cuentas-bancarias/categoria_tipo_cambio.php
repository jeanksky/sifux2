<?php
use yii\helpers\Html;
use backend\models\CategoriaTipoCambio;
use yii\bootstrap\Modal;
use backend\models\Moneda;
use backend\models\EntidadesFinancieras;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

    function agrega_categoria(){
    	var catego = $("input[name='catego']:checked").val();
        var ddl_moneda = document.getElementById('ddl-moneda').value;
        var ddl_entidadfina = document.getElementById('ddl-entidadfina').value;
        
        if (catego ==null || ddl_moneda == '' || ddl_entidadfina =='') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/agrega_categoria') ?>",
                type:"post",
                data: { 'catego': catego, 'ddl_moneda' : ddl_moneda, 'ddl_entidadfina': ddl_entidadfina },
                success: function(data){
                    $("#index-categoria_tipo_cambio").load(location.href+' #index-categoria_tipo_cambio','');
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                    $("input:radio").attr("checked",false);
                    $('#ddl-moneda').val('').change();
                    $('#ddl-entidadfina').val('').change();
                    $('#modalcreatecategoria').hide("slow");
                    $('#modalcreatecategoria').modal('toggle');
                },
                error: function(msg, status,err){
                 alert('No pasa 33');
                }
            }); 
        }
    }
    //envia la categoria que selecciono para obtener los datos de dicha categoria
    function actualiza_modal_categoria(idCategoria) {
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualiza_modal_categoria') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idCategoria': idCategoria },
                success: function(data){
                	$('#idCategoria_').val(data.idCategoria);
                    //$('#catego_').val(data.categoria);
                    $("input[name=catego_][value='"+data.categoria+"']").prop("checked",true);
                    $('#ddl-moneda_').val(data.idTipo_moneda).change();
                    $('#ddl-entidadfina_').val(data.idEntidad_financiera).change();
                    //$("#update-monedas").load(location.href+' #update-monedas','');
                },
                error: function(msg, status,err){
                 alert('No pasa - linea 55');
                }
            }); 
    }

    //envia al controlador la actualizacionde la categoria
    function actualiza_categoria() {
    	var idCategoria = document.getElementById('idCategoria_').value;
    	var catego = $("input[name='catego_']:checked").val();
        var ddl_moneda = document.getElementById('ddl-moneda_').value;
        var ddl_entidadfina = document.getElementById('ddl-entidadfina_').value;
        if (catego ==null || ddl_moneda == '' || ddl_entidadfina =='') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualizar_categoria') ?>",
                type:"post",
                data: { 'idCategoria': idCategoria, 'catego': catego, 'ddl_moneda' : ddl_moneda, 'ddl_entidadfina': ddl_entidadfina },
                success: function(data){
                    $("#index-categoria_tipo_cambio").load(location.href+' #index-categoria_tipo_cambio','');
                    $('#modalactualizacategoria').hide("slow");
                    $('#modalactualizacategoria').modal('toggle');
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    });
                },
                error: function(msg, status,err){
                 alert('No pasa 33');
                }
            }); 
        }
    }

    //envia al controlador la orden de eliminar la categoria seleccionada
    function eliminarcategoria(idCategoria) {
        if(confirm("Está seguro de eliminar esta categoría")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/elimina_categoria') ?>",
                type:"post",
                data: { 'idCategoria': idCategoria },
                success: function(alert){
                    $("#index-categoria_tipo_cambio").load(location.href+' #index-categoria_tipo_cambio','');     
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/tipo_cambio') ?>", function( data ) {
                      $('#ref_t_cambio').html(data);
                    }); 
                    $('#alert').html(alert);              
                },
                error: function(msg, status,err){
                 alert('No pasa 94');
                }
            });
        } 
    }
</script>
<style>
    #modalcreatecategoria .modal-dialog{
    width: 40%!important;
    }

    #modalactualizacategoria .modal-dialog{
    width: 40%!important;
    }
</style>
<div id="alert"></div>

<center><h2>Categoría tipo de cambio</h2></center>

<div class="col-xs-3">
</div>
<div class="col-xs-5">
</div>
<div class="col-xs-4">
<p style="text-align:right">
    <?= Html::a('Crear nueva categoría', '#', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modalcreatecategoria']) ?>  
</p>
</div>


<?php
echo '<div id="index-categoria_tipo_cambio">';
    echo '<table class="items table table-striped" id="tabla_lista_entidades" >';
        echo '<thead>';
        printf('<tr><th>%s</th><th>%s</th><th><center>%s</center></th><th class="actions button-column">&nbsp;</th></tr>',
                'CATEGORÍA',
                'MONEDA',
                'ENTIDAD FINANCIERA'
                );
        echo '</thead>';
        $categoriatipocambio = CategoriaTipoCambio::find()->orderBy(['idCategoria' => SORT_DESC])->all();
        echo '<tbody>';
        foreach($categoriatipocambio as $position => $categoria) { 
        	$moneda = Moneda::find()->where(['idTipo_moneda'=>$categoria['idTipo_moneda']])->one();
        	$entidad = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$categoria['idEntidad_financiera']])->one();
            $eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-1x', 'onclick'=>'javascript:eliminarcategoria('.$categoria['idCategoria'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar categoría') ]);
            $actualizar = Html::a('', '#', [
                            'class'=>'fa fa-pencil fa-1x',
                            'id' => 'activity-index-link-categoria-cambio',
                            'data-toggle' => 'modal',
                            'onclick'=>'javascript:actualiza_modal_categoria("'.$categoria['idCategoria'].'")',
                            'data-target' => '#modalactualizacategoria',
                            //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Actualizar categoría') ]);
            $accion = $actualizar.' '.$eliminar;
            printf('<tr><td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                            $categoria['categoria'],
                            $moneda->descripcion,
                            $entidad->descripcion,
                            $accion
                        );
        }
        echo '</tbody>';
    echo '</table>';
    echo '</div>';
?>
<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalcreatecategoria',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Crear nueva categoría de tipo de cambio</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar', '#', ['class' => 'btn btn-success', 'onclick'=>'agrega_categoria()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="create-categoria">';
                echo '<div class="col-lg-4">';
                echo '<label>Categoría: </label>';
                $new = ['Compra' => 'Compra', 'Venta' => 'Venta'];
                echo Html::radioList('catego', null, $new, ['class' => 'radio', 'id' => 'catego']);
                //echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'catego', 'placeholder' =>' Categoría', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo '<label>Moneda: </label>';
                echo Select2::widget([ 
                                'name' => '',
                                'data' => ArrayHelper::map(Moneda::find()->all(), 'idTipo_moneda', 
                                    function($element) {
                                    return $element['simbolo'].' - '.$element['descripcion'];
                                }),
                                'options' => [
                                    'id'=>'ddl-moneda',
                                    'placeholder'=>'- Seleccione -',
                                    //'onchange'=>'javascript:',
                                    //'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                            ]);
                echo '</div>';
                echo '<div class="col-lg-5">';
                echo '<label>Entidad financiera: </label>';
                echo Select2::widget([ 
                                'name' => '',
                                'data' => ArrayHelper::map(EntidadesFinancieras::find()->all(), 'idEntidad_financiera','descripcion'),
                                'options' => [
                                    'id'=>'ddl-entidadfina',
                                    'placeholder'=>'- Seleccione -',
                                    //'onchange'=>'javascript:',
                                    //'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                            ]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end(); 
?>

<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalactualizacategoria',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Actualizar categoría de tipo de cambio</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Actualizar', '#', ['class' => 'btn btn-success', 'onclick'=>'actualiza_categoria()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="update-categoria">';
                echo '<div class="col-lg-4">';
                echo '<label>Categoría: </label>';
                $new = ['Compra' => 'Compra', 'Venta' => 'Venta'];
                echo Html::radioList('catego_', null, $new, ['class' => 'radio', 'id' => 'catego_']);
                //echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'catego_', 'placeholder' =>' Categoría', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo '</div>';
                echo '<div class="col-lg-3">';
                echo' <input type="hidden" name="position" id="idCategoria_">';
                echo '<label>Moneda: </label>';
                echo Select2::widget([ 
                                'name' => '',
                                'data' => ArrayHelper::map(Moneda::find()->all(), 'idTipo_moneda', 
                                    function($element) {
                                    return $element['simbolo'].' - '.$element['descripcion'];
                                }),
                                'options' => [
                                    'id'=>'ddl-moneda_',
                                    'placeholder'=>'- Selecccione -',
                                    //'onchange'=>'javascript:',
                                    //'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                            ]);
                echo '</div>';
                echo '<div class="col-lg-5">';
                echo '<label>Entidad financiera: </label>';
                echo Select2::widget([ 
                                'name' => '',
                                'data' => ArrayHelper::map(EntidadesFinancieras::find()->all(), 'idEntidad_financiera','descripcion'),
                                'options' => [
                                    'id'=>'ddl-entidadfina_',
                                    'placeholder'=>'- Selecccione -',
                                    //'onchange'=>'javascript:',
                                    //'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                            ]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end(); 
?>