<?php
use yii\helpers\Html;
use backend\models\CategoriaTipoCambio;
use backend\models\TipoCambio;
use yii\bootstrap\Modal;
use backend\models\Moneda;
use backend\models\EntidadesFinancieras;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
    jQuery.noConflict();
    jQuery(document).ready( function () {
            $('.table tbody tr').click(function(event) {
              let idTipo_cabio = $('input[name="radios"]:checked').val();
                $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/valorcambiousar') ?>",
                    type:"post",
                    data: { 'idTipo_cabio': idTipo_cabio },
                    success: function(data){
                    },
                    error: function(msg, status,err){
                     //alert('Error, consulte linea 23 (informar a Nelux)');
                    }
                });
            });
        });

    function agrega_tcambio(){
        var ddl_idCategoria = document.getElementById('ddl-idCategoria');
        var valorcambio = document.getElementById('valorcambio').value;
        if (ddl_idCategoria == null || valorcambio == '') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/agrega_tcambio') ?>",
                type:"post",
                data: { 'ddl_idCategoria': ddl_idCategoria.value, 'valorcambio' : valorcambio },
                success: function(data){
                    $("#index-tipo_cambio").empty().load(location.href+' #index-tipo_cambio','');
                    $('#ddl-idCategoria').val('').change();
                    $('#valorcambio').val('');
                    $('#modalcreatecambio').hide("slow");
                    $('#modalcreatecambio').modal('toggle');
                },
                error: function(msg, status,err){
                 //alert('Error, consulte linea 32 (informar a Nelux)');
                }
            });
        }
    }
    //envia la categoria que selecciono para obtener los datos de dicha categoria
    function actualiza_modal_tcambio(idTipo_cabio) {
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualiza_modal_tcambio') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idTipo_cabio': idTipo_cabio },
                success: function(data){
                	$('#idtcambio').val(data.idTipo_cabio);
                    $('#ddl-idCategoria_').val(data.idCategoria).change();
                    $('#valorcambio_').val(data.valor_tipo_cambio);
                    //$("#update-monedas").load(location.href+' #update-monedas','');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 51 (informar a Nelux)');
                }
            });
    }

    //envia al controlador la actualizacionde la categoria
    function actualiza_tcambio() {
    	var idtcambio = document.getElementById('idtcambio').value;
        var ddl_idCategoria_ = document.getElementById('ddl-idCategoria_').value;
        var valorcambio_ = document.getElementById('valorcambio_').value;
        if (ddl_idCategoria_ ==null || idtcambio == '' || valorcambio_ =='') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualizar_tcambio') ?>",
                type:"post",
                data: { 'idtcambio': idtcambio, 'ddl_idCategoria_': ddl_idCategoria_, 'valorcambio_' : valorcambio_ },
                success: function(data){
                    $("#index-tipo_cambio").load(location.href+' #index-tipo_cambio','');
                    $('#modalactualizacambio').hide("slow");
                    $('#modalactualizacambio').modal('toggle');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 74 (informar a Nelux)');
                }
            });
        }
    }

    //envia al controlador la orden de eliminar la categoria seleccionada
    function eliminartcambio(idTipo_cabio) {
        if(confirm("Está seguro de eliminar esta categoría")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/elimina_tcambio') ?>",
                type:"post",
                data: { 'idTipo_cabio': idTipo_cabio },
                success: function(data){
                    $("#index-tipo_cambio").load(location.href+' #index-tipo_cambio','');
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 91 (informar a Nelux)');
                }
            });
        }
    }
</script>
<style>
    #modalcreatecambio .modal-dialog{
    width: 40%!important;
    }

    #modalactualizacambio .modal-dialog{
    width: 40%!important;
    }
</style>
<?php //me permite obtener de todas las monedas cual es la moneda local
$moneda_local = 0;
$monedas = Moneda::find()->all();
foreach($monedas as $moneda) {
    if ($moneda['monedalocal']=='x') {
        $moneda_local = $moneda['idTipo_moneda'];
    }
}
?>
<center><h2>Tipo de cambio</h2></center>
<?php //consulta tipo de cambio del dolar al banco central
    $fecha = date('d/m/Y');
    if (@tipo_cambio($fecha)) {
    $valor_tipo_cambio_banco_central = tipo_cambio($fecha);
    echo "<pre><center>";
    echo 'COSTA RICA > BANCO CENTRAL (Dolar) - COMPRA: ¢'.number_format($valor_tipo_cambio_banco_central['compra'],2) . ' - VENTA: ¢'.number_format($valor_tipo_cambio_banco_central['venta'],2);
    echo "</center></pre>";
} else{
    echo "<pre><center>";
    echo 'COSTA RICA > BANCO CENTRAL (Dolar) - "NO DISPONIBLE TEMPORALMENTE"';
    echo "</center></pre>";
}
?>
<div class="col-xs-3">
    <label>Seleccione su moneda local: </label>
    <?= Select2::widget([
                    'name' => '',
                    'value' => $moneda_local,
                    'data' => ArrayHelper::map(Moneda::find()->all(), 'idTipo_moneda',
                        function($element) {
                        return $element['simbolo'].' - '.$element['descripcion'];
                    }),
                    'options' => [
                        'id'=>'local_moneda',
                        'placeholder'=>'- Seleccione su moneda local -',
                        'onchange'=>'javascript:local_moneda(this.value)',
                        //'multiple' => false //esto me ayuda a que solo se obtenga uno
                    ],
                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                ]) ?>
</div>
<div class="col-xs-5">
</div>
<div class="col-xs-4">
<p style="text-align:right">
    <?= Html::a('Crear un nuevo tipo de cambio', '#', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modalcreatecambio']) ?>
</p>
</div>

<?php
echo '<div id="index-tipo_cambio">';
    echo '<table class="items table table-striped" id="tabla_lista_entidades" >';
        echo '<thead>';
        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center><th>%s</th><th>%s</th></th><th class="actions button-column">&nbsp;</th></tr>',
                'VALOR A USAR',
                'ABREVIATURA',
                'MONEDA',
                'CATEGORÍA TIPO DE CAMBIO',
                'FECHA - HORA',
                'VALOR TIPO DE CAMBIO'
                );
        echo '</thead>';
        $tipocambio = TipoCambio::find()->orderBy(['idTipo_Cabio' => SORT_DESC])->all();
        echo '<tbody>';
        $checked = '';
        foreach($tipocambio as $position => $tcambio) {
            if ($tcambio['usar']=='x') {
                $checked = 'checked';
            } else $checked = '';
            //$categoriatipocambio = CategoriaTipoCambio::find()->orderBy(['idCategoria' => SORT_DESC])->all();
        	$categoriatipocambio = CategoriaTipoCambio::find()->where(['idCategoria'=>$tcambio['idCategoria']])->one();
            $moneda = Moneda::find()->where(['idTipo_moneda'=>$categoriatipocambio->idTipo_moneda])->one();
            $EntidadesFinancieras = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$categoriatipocambio->idEntidad_financiera])->one();
            $fecha = date('d-m-Y ** H:i:s', strtotime( $tcambio['fecha_hora'] ));
            $eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-1x', 'onclick'=>'javascript:eliminartcambio('.$tcambio['idTipo_cabio'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar categoría') ]);
            $actualizar = Html::a('', '#', [
                            'class'=>'fa fa-pencil fa-1x',
                            'id' => 'activity-index-link-categoria-cambio',
                            'data-toggle' => 'modal',
                            'onclick'=>'javascript:actualiza_modal_tcambio("'.$tcambio['idTipo_cabio'].'")',
                            'data-target' => '#modalactualizacambio',
                            //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Actualizar categoría') ]);
            $accion = $actualizar.' '.$eliminar;
            printf('<tr>    <td class="actions button-column">%s</td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                            '<input type="radio" value="'.$tcambio['idTipo_cabio'].'" name="radios" '.$checked.'/>',
                            $moneda->abreviatura,
                            $moneda->descripcion,
                            $categoriatipocambio->categoria.' ('.$EntidadesFinancieras->descripcion.')',
                            $fecha,
                            $moneda->simbolo.number_format($tcambio['valor_tipo_cambio'],2),
                            $accion
                        );
        }
        echo '</tbody>';
    echo '</table>';
    echo '</div>';
?>
<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalcreatecambio',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Crear nuevo tipo de cambio para hoy</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar', '#', ['class' => 'btn btn-success', 'onclick'=>'agrega_tcambio()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="create-tcambio">';
                echo '<div class="col-lg-7">';
                echo '<label>Categoría tipo de cambio: </label>';
                echo Select2::widget([
                                'name' => '',
                                'data' => ArrayHelper::map(CategoriaTipoCambio::find()->all(), 'idCategoria',
                                    function($element) {
                                    $mon = Moneda::find()->where(['idTipo_moneda'=>$element['idTipo_moneda']])->one();
                                    $Entifina = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$element['idEntidad_financiera']])->one();
                                    return $element['categoria'].' - '.$mon->abreviatura.' - '.$mon->descripcion.' del '.$Entifina->descripcion;
                                }),
                                'options' => [
                                    'id'=>'ddl-idCategoria',
                                    'placeholder'=>'- Seleccione -',
                                    //'onchange'=>'javascript:',
                                    //'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                            ]);
                echo '</div>';
                echo '<div class="col-lg-5">';
                echo '<label>Valor del tipo de cambio para hoy: </label>';
                //echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'valorcambio', 'placeholder' =>' Valor tipo cambio', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo \yii\widgets\MaskedInput::widget([
                        'id' => 'valorcambio',
                        'name' => '',
                        'clientOptions' => [
                        'alias' => 'numeric',
                        'allowMinus'=>false,
                        'groupSize'=>3,
                        'radixPoint'=> ".",
                        'groupSeparator' => '',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true
                        //'unmaskAsNumber'=>true//dont know what this does
                        ]
                    ]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>

<?php
//muestra modal para actualizar nota de credito pendiente
    Modal::begin([
            'id' => 'modalactualizacambio',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Actualizar tipo de cambio</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Actualizar', '#', ['class' => 'btn btn-success', 'onclick'=>'actualiza_tcambio()',]),
        ]);
    echo '<div class="well">';
        echo '<div style="height: 80px;width: 100%; overflow-y: auto; ">';
            echo '<div id="update-tcambio">';
                echo '<div class="col-lg-7">';
                echo' <input type="hidden" name="idtcambio" id="idtcambio">';
                echo '<label>Categoría tipo de cambio: </label>';
                echo Select2::widget([
                    'name' => '',
                    'data' => ArrayHelper::map(CategoriaTipoCambio::find()->all(), 'idCategoria',
                        function($element) {
                        $mon = Moneda::find()->where(['idTipo_moneda'=>$element['idTipo_moneda']])->one();
                        $Entifina = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$element['idEntidad_financiera']])->one();
                        return $element['categoria'].' - '.$mon->abreviatura.' - '.$mon->descripcion.' del '.$Entifina->descripcion;
                    }),
                    'options' => [
                        'id'=>'ddl-idCategoria_',
                        'placeholder'=>'- Seleccione -',
                        //'onchange'=>'javascript:',
                        //'multiple' => false //esto me ayuda a que solo se obtenga uno
                    ],
                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                ]);
                echo '</div>';
                echo '<div class="col-lg-5">';
                echo '<label>Valor del tipo de cambio para hoy: </label>';
                //echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'valorcambio', 'placeholder' =>' Valor tipo cambio', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
                echo \yii\widgets\MaskedInput::widget([
                        'id' => 'valorcambio_',
                        'name' => '',
                        'clientOptions' => [
                        'alias' => 'numeric',
                        'allowMinus'=>false,
                        'groupSize'=>3,
                        'radixPoint'=> ".",
                        'groupSeparator' => '',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true
                        //'unmaskAsNumber'=>true//dont know what this does
                        ]
                    ]);
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

    Modal::end();
?>
<?php
function etiqueta_final($parser, $name) {
    global $info,$datos,$contador;
    $info[$name][] = $datos;
}
function extractor_datos($parser,$data){
                global $datos;
                $datos = $data;
}
function extractor_datos_tags($parser,$data){
                global $datos;
                $datos .= $data;
}
function parser_extractor($cadena,$tags=true){

    // Definiendo variables
    global $info,$datos,$contador;
    $info = array();
    $datos = "";
    $contador = 0;

    // Creando el parser
    $xml_parser = xml_parser_create();

    // Definiendo la funciones controladoras
    xml_set_character_data_handler($xml_parser,($tags?"extractor_datos":"extractor_datos_tags"));
    xml_set_element_handler($xml_parser, "", "etiqueta_final");

    // Procesando el archivo
    if (!xml_parse($xml_parser, $cadena)) {
                    die(sprintf("XML error: %s at line %d",
                                                                    xml_error_string(xml_get_error_code($xml_parser)),
                                                                    xml_get_current_line_number($xml_parser)));
    }

    // Liberando recursos
    xml_parser_free($xml_parser);
    return $info;
}


/*
La siguiente Funcion debe recibir por parametro la fecha en formato dd/mm/YYYY
*/
function tipo_cambio($fecha){
    // Rutas de los archivos XML con el tipo de cambio
    $file["compra"] = "http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=317&tcFechaInicio=$fecha&tcFechaFinal=$fecha&tcNombre=dmm&tnSubNiveles=N"; // Archivo XML
    $file["venta"] = "http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=318&tcFechaInicio=$fecha&tcFechaFinal=$fecha&tcNombre=dmm&tnSubNiveles=N"; // Archivo XML

    // Extrae el tipo cambio con el valor de COMPRA
    $data_file = file_get_contents($file["compra"]);
    $ainfo = parser_extractor($data_file,false);
    $fuente = parser_extractor($ainfo["STRING"][0]);
    $tipo["compra"] = $fuente["NUM_VALOR"][0];

    // Extrae el tipo cambio con el valor de VENTA
    $data_file = file_get_contents($file["venta"]);
    $ainfo = parser_extractor($data_file,false);
    $fuente = parser_extractor($ainfo["STRING"][0]);
    $tipo["venta"] = $fuente["NUM_VALOR"][0];

    // Retornando valor de compra y venta del dolar
    if ( $tipo["compra"] == "" or $tipo["venta"] == "" ){
        return false;
    }else{
        return $tipo;
    }

}


?>
