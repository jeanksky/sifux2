<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use backend\models\EntidadesFinancieras;
use backend\models\Moneda;
//$maskedinput = \Yii::$app->request->BaseUrl.'/js/jquery.maskedinput.min.js';
//$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
/* @var $this yii\web\View */
/* @var $model backend\models\CuentasBancarias */
/* @var $form yii\widgets\ActiveForm */
?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->
<!--script type="text/javascript" src="<?php //echo $jquery_min_js; ?>"></script-->
<script type="text/javascript">
    function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58 ) )
                return true;
            return false;
        }
    function isNumberDe(evt) {
            var nav4 = window.Event ? true : false; 
            var key = nav4 ? evt.which : evt.keyCode; 
            return (key <= 13 || key==46 || (key >= 48 && key <= 57)); 
        }
    $(document).ready(function(){//con esto me permite poner saldo en banco con formato desimal y separadores
        $("#saldo_bancos").on({
            "focus": function (event) {
                $(event.target).select();
            },
            "keyup": function (event) {
                $(event.target).val(function (index, value ) {
                    return value.replace(/\D/g, "")
                                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                });
            }
        }); 
        $("#saldo_libros").on({
            "focus": function (event) {
                $(event.target).select();
            },
            "keyup": function (event) {
                $(event.target).val(function (index, value ) {
                    return value.replace(/\D/g, "")
                                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                });
            }
        }); 

        
    });


    //calculadora me devuelve el monto en moneda local multiplicando por la tasa de cambio
    function copiarsaldo(){
        var saldo_bancos = document.getElementById("saldo_bancos").value;
        
        $('#saldo_libros').val(saldo_bancos);

        if (saldo_bancos!='') {
            document.getElementById('btn_ac').disabled=false;
        } else document.getElementById('btn_ac').disabled=true;
        
    }
    
</script>
<div class="cuentas-bancarias-form">
    <br>
    <?php $form = ActiveForm::begin([
        'id' => 'cuentasbancarias-form',
    ]); ?>
    <div class="col-lg-2">
    <?= $form->field($model, 'idTipo_moneda')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Moneda::find()->asArray()->all(), 'idTipo_moneda', 'descripcion'),
                                'options'=>['id'=>'ddl-moneda-cb', 'placeholder'=>'- Seleccione -'],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true],
                            ]);?>
    </div>
    <div class="col-lg-3">
    <?= $form->field($model, 'idEntidad_financiera')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(EntidadesFinancieras::find()->asArray()->all(), 'idEntidad_financiera', 'descripcion'),
                                'options'=>['id'=>'ddl-entidadfina-cb', 'placeholder'=>'- Seleccione -'],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true],
                            ]);?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'numero_cuenta')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-4">
    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-2">
    <?= $form->field($model, 'tipo_cuenta')->radioList(['Ahorro' => 'Ahorro', 'Corriente' => 'Corriente']); ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'cuenta_cliente')->textInput(['maxlength' => true, "onkeypress"=>"return isNumber(event)"]) ?>
    </div>
    <div class="col-xs-2">
    <?php 
    if ($model->isNewRecord) {
        echo $form->field($model, 'saldo_libros')->textInput(['maxlength' => true, 'readonly' => true, 'id' => 'saldo_libros']);
    } else {
     $model->saldo_libros = number_format($model->saldo_libros,2);
     //tiene que ir el id para que el $saldo_libros sea un monto con separrador reconocido por el modelo
     echo $form->field($model, 'saldo_libros')->textInput(['maxlength' => true, 'readonly' => true, 'id' => 'saldo_libros']);   
    }
    

     ?>
    </div>
    

    <?php
    if ($model->isNewRecord) {
        /*echo $form->field($model, 'saldo_bancos')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                'alias' => 'numeric',
                'allowMinus'=>false,
                'groupSize'=>3,
                'radixPoint'=> ".",
                'groupSeparator' => '',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true
                //'unmaskAsNumber'=>true//dont know what this does
                ]
        ]);*/
        echo '<div class="col-xs-3">';
        echo '<label>Saldo Bancos</label>';
        echo $form->field($model, 'saldo_bancos', ['template' => '<div class="form-group input-group">{input}<span class="input-group-btn"><div id="simbolo"><button class="btn btn-default" type="button" onclick="copiarsaldo()">Establecer saldo!</button></div></span></div>'])->textInput(['maxlength' => true, 'id' => 'saldo_bancos', 'onkeyup'=>'']);
        echo '</div>';
    }else{
        echo '<div class="col-xs-2">';
        $model->saldo_bancos = number_format($model->saldo_bancos,2);
        //tiene que ir el id para que el $saldo_bancos sea un monto con separrador reconocido por el modelo
        echo $form->field($model, 'saldo_bancos')->textInput(['maxlength' => true, 'readonly' => true, 'id' => 'saldo_bancos']);
        echo '</div>';
    } ?>
    
    

    <div class="col-xs-2"><br>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Crear cuenta' : 'Actualizar cuenta', ['id'=>'btn_ac', 'disabled'=>$model->isNewRecord ? true : false, 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
