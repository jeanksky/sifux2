<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Mecanicos */

$this->title = 'Crear mecánicos';
$this->params['breadcrumbs'][] = ['label' => 'Mecánicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mecanicos-create">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
