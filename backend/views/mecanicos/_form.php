<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Mecanicos */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ingrese los datos que se le solicitan en el siguiente formulario.
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<div class="mecanicos-form">

				<?php $form = ActiveForm::begin(); ?>
				<div class="col-lg-6">
				<div class="col-lg-10">
				<?= $form->field($model, 'codigo')->textInput(['readonly'=>true]) ?>

				<?= $form->field($model, 'nombre')->textInput(['maxlength' => 80 , 'readonly'=>true]) ?>

				<?= $form->field($model, 'telefono')->
                    widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9999-99-99',

                ])->textInput(['readonly'=>true]) ?>  

                </div>
		                           
				</div><div class="col-lg-3">
				<?= $form->field($model, 'porcentComision')->textInput() ?>
				</div><div class="col-lg-6">
				<?= $form->field($model, 'direccion')->textarea(['maxlength' => 130]) ?>

				
				<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>
				</div>
				<?php ActiveForm::end(); ?>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>