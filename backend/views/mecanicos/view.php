<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Mecanicos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Mecánicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mecanicos-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idMecanico], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idMecanico], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres borrar este mecánico?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div id="semitransparente">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            'idMecanico',
            'codigo',
            'nombre',
            'telefono',
            'porcentComision',
            'direccion',
            
            
        ],
    ]) ?>
</div>
</div>
