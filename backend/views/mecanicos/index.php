<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MecanicosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mecánicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mecanicos-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

     <p style="text-align:right">
        <!--?= /*Html::a('Crear mecánicos', ['create'], ['class' => 'btn btn-success'])*/ ?-->
    </p><br>
    <div class="table-responsive">
    <div id="semitransparente">
              <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
<!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'idMecanico',
            'codigo',
            'nombre',
            'telefono',
            'porcentComision',
            'direccion',


            ['class' => 'yii\grid\ActionColumn','template' => '{update}'],
        ],
    ]); ?>
</div>
</div>
</div>
