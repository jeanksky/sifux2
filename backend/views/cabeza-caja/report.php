<?php

//use yii\helpers\Html;
use backend\models\Empresa;
use yii\helpers\Url;
use backend\models\MovimientoCobrar;
use backend\models\NumberToLetterConverter;
use backend\models\Clientes;//para obtener el cliente
use backend\models\Funcionario;
use backend\models\EncabezadoCaja;
use backend\models\FormasPago;
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;

/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */

//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
    $urlParts = parse_url($_SERVER['REQUEST_URI']);

    if (isset($urlParts['query'])){
        //
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){

            $empresaimagen = new Empresa();//Se obtienen datos de la empresa
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//busco la empresa para el encabezado
            $modelestado = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$id])->one();
            //$products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->all();//Se obtienen datos del detalle de compra
            if ($modelestado->estadoFactura == 'Crédito Caja') {

                    $cliente = Clientes::find()->where(['idCliente'=>$modelestado->idCliente])->one();
                    $nuevafecha = strtotime ( '+'.$cliente->diasCredito.' day' , strtotime ( $modelestado->fecha_inicio ) ) ;
                    $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
                    $fechavencimiento = ($modelestado->tipoFacturacion == 4) ? $nuevafecha : '';
                    $credi_saldo = $modelestado->total_a_pagar;

                $nuevoestado = 'Crédito';
                        $sql = "UPDATE tbl_encabezado_factura SET estadoFactura = :estado, fecha_vencimiento = :fechavencimiento, credi_saldo = :credi_saldo WHERE idCabeza_Factura = :idC";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindParam(":estado", $nuevoestado);
                        $command->bindParam(":fechavencimiento", $fechavencimiento);
                        $command->bindParam(":credi_saldo", $credi_saldo);
                        $command->bindValue(":idC", $id);
                        $command->execute();
            }

            $printerName = urldecode($qs['printerName']);
            //Create ESC/POS commands for sample receipt
            $esc = '0x1B'; //ESC byte in hex notation
            $newLine = '0x0A'; //LF byte in hex notation
            //$derecha = $esc . '!' . '0x00';//Establece el margen izquierdo en la columna n (donde n está entre 0 y 255) en el tono de carácter actual.
            $cajon = $esc ."0x700x0";//abrir cajon
            $cmds = $esc . "@"; //Initializes the printer (ESC @)
            //Encabezado empresa-------------------------------------------------------------------
            $cmds .= $newLine . $newLine;
            $cmds .= $esc . '!' . '0x00'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
            $cmds .= $empresa->nombre.$newLine; //Nombre de la empresa
            $cmds .= $empresa->localidad.' - '.$empresa->direccion.$newLine; //Nombre de la empresa
            $cmds .= $empresa->sitioWeb.' / '. $empresa->email.$newLine; //Nombre de la empresa
            $cmds .= $empresa->telefono.' / '. $empresa->fax; //Nombre de la empresa

            $cmds .= $newLine . $newLine.$newLine;

            //Encabezado factura-------------------------------------------------------------------
            $modelcaja = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$id])->one();//vuelvo a llamar al modelo actual pero esta ves con las modificaciones que se aplicaron
            $estado_de_fac = $modelcaja->estadoFactura=='Crédito' ? 'Credito' : $modelcaja->estadoFactura;
            $cmds .= $esc . '!' . '0x00'; //Fuente de caracteres A seleccionado (ESC! 0)
            $cmds .= 'Factura No: ' . $id.$newLine;
            $cmds .= 'Fecha: '. $modelcaja->fecha_inicio.$newLine.$newLine;
            $cmds .= "CLIENTE:----------------------------------".$newLine;
            if (@$cliente = Clientes::find()->where(['idCliente'=>$modelcaja->idCliente])->one()) {
                $cmds .= "Nombre: ".$cliente->nombreCompleto . $newLine;
                $cmds .= "Telefono: ".$cliente->telefono . $newLine;
                $cmds .= "Direccion: ".$cliente->direccion . $newLine;
            } else {
                $cmds .= $modelcaja->idCliente . $newLine;
            }
            $cmds .= "------------------------------------------". $newLine . $newLine;

            $cmds .= 'Estado de la factura: ' . $estado_de_fac . $newLine;

            if(@$modelP = FormasPago::find()->where(['id_forma'=>$modelcaja->tipoFacturacion])->one()){
            if ($modelcaja->estadoFactura!='Crédito') {
                $cmds .= 'Tipo de pago: ' . $modelP->desc_forma . $newLine;
            } else {
                    $cmds .= 'Fecha Vencimiento: ' . $modelcaja->fecha_vencimiento . $newLine;
                }
            }

            $cmds .= 'Observacion: ' . $modelcaja->observacion . $newLine . $newLine . $newLine;

            //Detalle de factura--------------------------------------------------------------------
            $cmds .= 'CANT      DESCRIPCION                 PRECIO' . $newLine;
            $products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $modelcaja->idCabeza_Factura])->all();
            $detallefactura = '';
            foreach($products as $product) {
                $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
                $detallefactura .= new item($product['cantidad']. ' ' . substr($descrip,0,22), number_format(($product['precio_unitario']*$product['cantidad']),2));
            }

            //Cierre de impresión-------------------------------------------------------------------
            $cmds .= $detallefactura ."---------------------------------------------" . $newLine;
            $cmds .= new item("                 SUBTOTAL:", number_format($modelcaja->subtotal,2));
            $cmds .= new item("                 TOTAL DESCUENTO:", number_format($modelcaja->porc_descuento,2));
            $cmds .= new item("                 TOTAL IV:", number_format($modelcaja->iva,2));
            $cmds .= new item("                 TOTAL A PAGAR:",number_format($modelcaja->total_a_pagar,2)). $newLine . $newLine;

            $modelF = Funcionario::find()->where(['idFuncionario'=>$modelcaja->codigoVendedor])->one();
            $cmds .= "Facturado por: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2;
            $cmds .= $newLine . $newLine . $newLine;
            $cmds .= "autorizado mediante resolucion 11-97 del 05/09/1997 de la D.G.T.D";
            $cmds .= $newLine . $newLine . $newLine;
            $cmds .= "GRACIAS POR PREFERIRNOS, ES UN GUSTO ATENDERLE.";
            $cmds .= $newLine . $newLine . $newLine . $newLine;
            $cmds .= $esc . "0x69";//cortar papel

            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cajon.$cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente
            if ($printerName === 'null'){
                $cpj->clientPrinter = new DefaultPrinter();
            }else{
                $cpj->clientPrinter = new InstalledPrinter($printerName);
            }

            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();
            echo $cpj->sendToClient();

            ob_end_flush();
            exit();
        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de etiqueta-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
        //ejecuta la llamada de impresión
        javascript:jsWebClientPrint.print("useDefaultPrinter=" + $("#useDefaultPrinter").attr("checked") + "&printerName=FACTURAS");
        self.close();

    });
</script>
<?php //otrs script
//Especifique la URL ABSOLUTA al archivo php que creará el objeto ClientPrintJob
//En este caso, esta misma página
echo WebClientPrint::createScript(Utils::getRoot().Url::home().'?r=cabeza-caja/report&id='.$id);

class item
{
    private $nombre;
    private $precio;
    private $signo;

    public function __construct($nombre = '', $precio = '', $signo = false)
    {
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> signo = $signo;
    }

    public function __toString()
    {
        $rightCols = 13;
        $leftCols = 35;
        if ($this -> signo) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> nombre, $leftCols) ;

        $sign = ($this -> signo ? '$ ' : '');
        $right = str_pad($sign . $this -> precio, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}
?>
