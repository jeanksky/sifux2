<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\EncabezadoCaja */

$this->title = 'Update Encabezado Caja: ' . ' ' . $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Encabezado Cajas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idCabeza_Factura, 'url' => ['view', 'id' => $model->idCabeza_Factura]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="encabezado-caja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
