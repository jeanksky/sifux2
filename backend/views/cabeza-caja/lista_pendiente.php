<?php
//use yii\widgets\Pjax;
//Pjax::begin();
use yii\helpers\Html;
use backend\models\Clientes;//para obtener el cliente
use backend\models\EncabezadoCaja;

$load = "'".Yii::$app->request->BaseUrl.'/img/load.gif'."'";
$condicion_venta = "'".Yii::$app->getUrlManager()->createUrl('cabeza-caja/condicion_venta')."'";
?>



<table class="items table table-striped" id="tabla_prefacturas_caja">
  <thead>
    <th style="text-align:left">ID.FAC</th>
    <th style="text-align:center">FECHA INICIO</th>
    <th style="text-align:center">CLIENTE</th>
    <th style="text-align:center">CONDICIÓN VENTA</th>
    <th style="text-align:right">DESCUENTO</th>
    <th style="text-align:right">TOTAL</th>
  </thead>
  <tbody>
    <?php
    $facturas_pendientes = EncabezadoCaja::find()->Where("estadoFactura IN ('Caja','Crédito Caja')", [])->all();
    foreach ($facturas_pendientes as $key => $value) {
      $cliente = $value->idCliente;
      if(@$cliente = Clientes::findOne($value->idCliente))
      {
          $cliente = Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$value->idCliente);
      }else{
          if ($value->idCliente=="")
          $cliente = 'Cliente no asignado';
          else
          $cliente = $value->idCliente;
      }
      $id_forma = $value->tipoFacturacion;
      if ($id_forma==null) {
          $id_forma = 'Cambiando...';
      } else {
        if ($id_forma == '6') {
          $id_forma = 'Contado';
        } else {
          $id_forma = 'Crédito';
        }
          /*$sql = "SELECT desc_forma from tbl_formas_pago where id_forma = :$id_forma";
          $command = \Yii::$app->db->createCommand($sql);
          $command->bindValue(":$id_forma", $id_forma);
          $result = $command->queryOne();
          $id_forma = $result['desc_forma'];*/
      }
      if (Yii::$app->params['servicioRestaurante']==true && $value->ignorar_porc_ser != 'checked') {
        $servicio = ($value->subtotal - $value->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
        $total_a_pagar = $value->total_a_pagar+$servicio;
      } else {
        $total_a_pagar = $value->total_a_pagar;
      }
        echo '<tr style="cursor:pointer;" onclick="seleccionar_factura(this, '.$value->idCabeza_Factura.', '.$condicion_venta.', '.$load.')">
                <td><font face="arial" size=2>'.$value->idCabeza_Factura.'</font></td>
                <td align="center"><font face="arial" size=2>'.$value->fecha_inicio.'</font></td>
                <td><font face="arial" size=2>'.$cliente.'</font></td>
                <td align="center"><font face="arial" size=2>'.$id_forma.'</font></td>
                <td align="right"><font face="arial" size=2>'.number_format($value->porc_descuento,2).'</font></td>
                <td align="right"><font face="arial" size=2>'.number_format($total_a_pagar,2).'</font></td>
                <td><font face="arial" size=2></font></td>
              </tr>';
    }
    ?>
  </tbody>
</table>
<?php //Pjax::end(); ?>
<script type="text/javascript">
  $(document).ready(function(){
    //setInterval($("#lista_pendiente_table").load(location.href+' #lista_pendiente_table',''),5000);
  });
</script>
