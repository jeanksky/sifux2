<?php

use yii\helpers\Html;
use yii\grid\GridView;

use backend\models\OrdenServicio;//para obtener el id del mecanico
use backend\models\Mecanicos;//para obtener el nombre del mecanico
use backend\models\EncabezadoPrefactura; //para obtener los atributos

use yii\helpers\Url;
use kartik\widgets\AlertBlock;
use yii\bootstrap\Modal;
use backend\models\Compra_update;//Prueba


use backend\models\Empresa;
use backend\models\MovimientoCobrar;
use backend\models\NumberToLetterConverter;
use backend\models\EncabezadoCaja;
$caja = \Yii::$app->request->BaseUrl.'/js/vistas/cabeza_caja/caja.js';
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
$empresa = Empresa::findOne(1);
/*use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;*/

$factura_electronica_logo = \Yii::$app->request->BaseUrl.'/img/factura_electronica.png';
 if (isset($_REQUEST['id'])){
      $id = $_GET['id'];
      if ($_GET['mail'] != '') {
        $factura_caja = new EncabezadoCaja();
        //$factura_caja->enviar_correo_factura($id, $_GET['mail'], $_GET['factun']);
        //Yii::$app->getSession()->setFlash('success', '<span class="glyphicon glyphicon-ok-sign"></span> <strong>Éxito!</strong> Factura enviada por correo.');
      }
      echo $this->render('ticket', [//aqui llama tiquet que imprime la factura correspondiente a este id
              'id' => $id,
          ]);
    }


//use backend\models\EntidadesFinancieras;
//use backend\models\CuentasBancarias;
//use yii\helpers\ArrayHelper;
//use kartik\widgets\Select2;
//use kartik\widgets\DepDrop;
//use yii\helpers\Json;
//use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EncabezadoCajaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Caja';
$this->params['breadcrumbs'][] = $this->title;

$factura_electronica = '<br><i class="fa fa-chain-broken fa-2x" aria-hidden="true"> Sin contacto con el servicio de Facturación electrónica</i>';
if ($empresa->factun_conect == 1 && $empresa->user_factun != '' && $empresa->pass_factun != '' &&
    $empresa->compania_id != null && $empresa->sucursal_id != null) {
  $factura_electronica = '<img src="'.$factura_electronica_logo.'" alt="Banner9" width="60%" height="80">';
}
 ?>
 <style>
     #modalFactura .modal-dialog{
     width: 60%!important;
     /*margin: 0 auto;*/
     }
 </style>
<div class="encabezado-caja-index">
  <div class='col-lg-8'>
    <div class="well">
      <div class="col-lg-6">
        <span style="font-size: 8.0em; color:#757777;"><?= Html::encode($this->title) ?> #1</span>
      </div>
      <div class="col-lg-6">
        <center>
          <?= $factura_electronica ?>
          <?php if ($empresa->factun_conect == 1 && $empresa->user_factun != '' && $empresa->pass_factun != '' &&
              $empresa->compania_id != null && $empresa->sucursal_id != null): ?>
          <h3 ><span id="estadoHaciendoGometa" class="label label-warning" style="font-size:12pt;">Estado Hacienda: Verificando...</span></h3>

          <h3 id="ultimaRevisionHaciendoGometa">
          </h3>
          <input type="hidden" id="estado_h" value="NULLO">
          <?php endif; ?>
        </center>
      </div>
      <div class="col-lg-12" id='notificaciones'>
      <?= AlertBlock::widget([
              'type' => AlertBlock::TYPE_ALERT,
              'useSessionFlash' => true,
              'delay' => 35000
          ]);?>
      </div>

      <div class="col-lg-12">
        <center>
          <span style="font-size: 1.5em; color:#757777; ">Lista de pre-facturas pendientes de facturar</span>
          <br> <button onclick="refrescarGridView()">Cargar tabla</button>
        </center><br><br>
      </div>
      <div style="height: 330px;width: 100%; overflow-y: auto; ">
      <div id="GridView">
        <center><h1><small>Cargando facturas, espere...</small></h1></center>
      </div>
      </div>
    </div>
  </div>
  <div class='col-lg-4 panel panel-primary'>
    <div style="height: 650px;width: 100%; overflow-y: auto; ">

        <center><span style="font-size: 1.5em; color:#757777;">Condicion de venta y medio de pago</span></center>
        <div id="condicion_venta"><br><br><center><span style="font-size: 2em; color:#E49E53;">(Seleccione la factura a emitir)</span></center><br><br>
          <strong>Nota:</strong> Si hacienda no esta en linea igualmente puede emitir faturas.
        </div>

    </div>
  </div>


        <?php
       //------Modal para Cancelar factura
           Modal::begin([
                'id' => 'modalCancelar',
                'size'=>'modal-lg',
                'header' => '<h4 class="modal-title">Cancelar factura <span style="float:right">Habilitar casilla de e-mail: <input type="checkbox" class="checkcorreo" name="checkcorreo" />&emsp;</span></h4>',
                'footer' => '<div id="footer"></div>',
            ]);
            echo "<div id='modal_'></div>";
            Modal::end();

          Modal::begin([//modal que me muestra la factura seleccionada
               'id' => 'modalFactura',
               'size'=>'modal-lg',
               'header' => '<center><h4 class="modal-title">Factura</h4></center>',
               'footer' => '<div id="footer_modal"></div>',
           ]);
           echo "<div id='well-factura'></div>";
          Modal::end();
        ?>

</div>

<script type="text/javascript" src="<?= $caja ?>"></script>
<script type="text/javascript">
//espacio que solo permite numero y decimal (.)
function isNumberDe(evt) {
    var nav4 = window.Event ? true : false;
    var key = nav4 ? evt.which : evt.keyCode;
    return (key <= 13 || key==46 || (key >= 48 && key <= 57));
}

//confirma el estado de factura en hacienda en segundo plano, o reenvia la factura si está estancada
function confirmar_facturas_hacienda() {
  var estado_hacienda =  document.getElementById("estado_h").value;
  if (estado_hacienda == 'OK') {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/confirmar_facturas_hacienda') ?>",
        type:"post",
        data: { },
        beforeSend: function() { //document.getElementById("estado_comprobacion").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Sincronizando estados con Hacienda...</span></center>';
        },
        success: function(data){ confirmar_movimientos_hacienda(); },
        error: function(msg, status,err){ /*alert('Algo no esta bien');*/ }
    });
  }
}

function confirmar_movimientos_hacienda(){
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/consultar_estados_movimiento') ?>",
      type:"post",
      data: { },
      beforeSend: function() {},
      success: function(data){},
      error: function(msg, status,err){ /*alert('Algo no esta bien');*/ }
  });
}

function refrescarGridView(){
    //$("#GridView").load(location.href+' #GridView',''); //refrescar el div
    //$("#emitidas_hoy").load(location.href+' #emitidas_hoy',''); //refrescar el div
    $.get(
      '<?= Yii::$app->getUrlManager()->createUrl('cabeza-caja/lista_pendiente') ?>',
      function (data) {
          //document.getElementById("well-factura").innerHTML=data;
          $('#GridView').html(data);
      }
    );
}

//setTimeout($('.modal').hide(),500);

function cargando(){
    $('#cargar').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
}
window.onload=cargando;




/*vehiel 05-12-2018
metodo que verificacion del estado del tarro de haciendo con el api de gometa*/
function verificarEstadoHAciendoGometa(){
  factun_conect = '<?= $empresa->factun_conect ?>';
  $.ajax({
    'url': "<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/consulta_estado_hacienda') ?>",
    'dataType':'json',
    success: function(data){
      $('#estadoHaciendoGometa').removeClass('label-danger');
      $('#estadoHaciendoGometa').removeClass('label-warning');
      estado = data.status;
      document.getElementById("estado_h").value = estado;
      /*los estados que podemos obtener son OK, BAD*/
      if(estado == "OK"){
        $('#estadoHaciendoGometa').addClass('label-success');
        $('#estadoHaciendoGometa').text("Estado Hacienda: En línea");
        var GridView = document.getElementById('GridView');
        GridView.style.background='white';
      }else if(estado == "BAD" && factun_conect == 1){
        $('#estadoHaciendoGometa').addClass('label-danger');
        $('#estadoHaciendoGometa').text("Estado Hacienda: Fuera de servicio");
        var GridView = document.getElementById('GridView');
        GridView.style.background='#E39E97';
      }

      /*creamos un objeto de tipo Date*/
      lastUpdate = data.last_update;
      // fecha = lastUpdate.format('dd-M-yyyy h:mm:ss a');
      $('#ultimaRevisionHaciendoGometa').html("<small>Última Actualización: "+lastUpdate+"</small>");
      // alert(fecha);

    },
    error:function(){
      $('#estadoHaciendoGometa').removeClass('label-success');
      $('#estadoHaciendoGometa').removeClass('label-danger');
      $('#estadoHaciendoGometa').addClass('label-warning');
      $('#estadoHaciendoGometa').text("Estado Hacienda: Sin verificar");
    }
  });
}

//-----------------------------------------------------------------------------------------------
$(document).ready(function(){
  /*vehiel 5-12-2018*/
  /*apenas el documento se cargue verificamos el estado de haciendo con el api de gometa*/
  verificarEstadoHAciendoGometa();
  //confirmar_facturas_hacienda();
  refrescarGridView();
  /*se ejecuta la verificación cada 10 min*/
  setInterval('verificarEstadoHAciendoGometa()',60000); /*1 min*/
  setTimeout("confirmar_facturas_hacienda()", 10000 ); /*luego de dos segundos*/
  setInterval('confirmar_facturas_hacienda()',120000); /*120 segundos*/
  setTimeout("refrescarGridView()", 900 );
  window.setInterval('refrescarGridView()',10000); /* diez segundos */
  /*fin vehiel 5-12-2018*/
        //$('#boton-cancelar').addClass('disabled');
        //$("#correo_cliente").prop('disabled', true);//mantengo inhavilidado el espacio de correo (direccion que se usa para enviar la factura por correo)
    });

/*setInterval('compruebatipopago()');*///Recorro a un segundo la funcion de comprobación
//_---------------------------------------------------------------------------------------------

//funciones para la cancelacion de facturas--------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------


//carga en la modal la vista de la factura consultada
$(document).on('click', '#activity-index-link-factura', (function() {
  document.getElementById("well-factura").innerHTML="";//tengo que destruir el div de well-create-movimiento para que well-update-movimiento esté limpio para la modal
  $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
      $.get(
        $(this).data('url'),
        function (data) {
            //document.getElementById("well-factura").innerHTML=data;
            $('#well-factura').html(data);
            //$('.well-factura').html(data);
            $('#modalFactura').modal();
        }
    );
}));

//funcion para obtener el footer de la modal que necesita enviar el valor id de la factura a imprimir
function agrega_footer(id, url_footer){
    $.ajax({
        url: url_footer,
        type: "post",
        data: { 'id' : id },
        success: function(data){
            $("#footer_modal").html(data);
        },
        error: function(msg, status,err){
         alert('No pasa74');
        }
    });
}

//enviar por correo la factura
function enviar_factura_correo(id) {
  var confirm_pago = confirm("¿Está seguro de enviar esta factura?");
  if (confirm_pago == true) {
  var email_cliente = document.getElementById("email_cliente").value;
  if (validateEmail(email_cliente)) {
  document.getElementById("notificacion_envio_correo").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Espere, puede tardar un minuto, envío de factura en proceso...</span></center>';
  $.get("<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/enviar_correo_factura_movimiento') ?>",
        { 'tipo' : 'FA', 'id' : id, 'email' : email_cliente, 'id_factun_mov' : '' },
         function(notif){
                  document.getElementById("notificacion_envio_correo").innerHTML = notif;
                  setTimeout(function() {//aparece la alerta en un milisegundo
                      $("#notificacion_envio_correo").fadeIn();
                  });
                  setTimeout(function() {//se oculta la alerta luego de 4 segundos
                      $("#notificacion_envio_correo").fadeOut();
                  },5000);
              }
          );
        } else {
          document.getElementById("notificacion_envio_correo").innerHTML = '<br><center><span class="label label-danger" style="font-size:15pt;">Verifique que la dirección sea correcta.</span></center>';
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#notificacion_envio_correo").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 4 segundos
              $("#notificacion_envio_correo").fadeOut();
          },5000);
        }
  }
}

//valida la sintacis de la direccion de correo electronico
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
</script>
