<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use backend\models\ProductoServicios;
use backend\models\TipoMovimientos;
use backend\models\MovimientosSesion;
use unclead\widgets\MultipleInput;
use backend\models\DetalleMovimientos;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Movimientos */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $tipo = MovimientosSesion::getTipo(); ?>
<?php $estado = MovimientosSesion::getEstado(); ?>
<?php $products = MovimientosSesion::getContenidoMovimiento(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
  $('.typeahead').typeahead();
</script>

<div class="panel panel-default">
    <div class="panel-heading">
        Ingrese los datos que se le solicitan en el siguiente formulario.
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="movimientos-form">

        <!--?php $form = ActiveForm::begin([
        ]);?-->
        <?php $fecha = date('d-m-Y'); // En formato ISO porque es compatible con casi todos los motores y los tipos de fecha
          $usuario = Yii::$app->user->identity->nombre;
        ?>
        <div class="col-lg-12">
            <div id="yw3" class="col-lg-7">
                <div class="form-actions" >
                <?php
                        MovimientosSesion::setFecha($fecha);
                        if(@$fech = MovimientosSesion::getFecha()){
                        echo '<h4><strong>Fecha ingreso</strong>: '.$fech;
                        echo "</h4>";
                       // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                        }
                    ?>

                </div> </div>
                <div id="yw3" class="col-lg-5">
                <div class="form-actions" >
                <?php
                        MovimientosSesion::setUsuario($usuario);
                        if(@$usu = MovimientosSesion::getUsuario()){
                        echo '<h4><strong>Usuario</strong>: '.$usu;
                        echo "</h4>";
                       // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                        }
                    ?>
                </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-actions" id="conten_tipo">
                       <?php
                        if(@$tipo = MovimientosSesion::getTipo()){
                        if(@$model = TipoMovimientos::find()->where(['codigoTipo'=>$tipo])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                            {

                                echo "<h4><strong>Tipo de movimiento</strong><br> ".$model->descripcion." ";
                                   if(!$products) {
                                echo Html::a('', ['/movimientos/delete-tipo'],['class'=>'glyphicon glyphicon-pencil']);
                              }
                                echo "</h4>";

                            }
                        }else{
                             echo '<strong>Tipo de movimiento</strong>';
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                               'name' => '',
                               'data' => ArrayHelper::map(TipoMovimientos::find()->all(), 'codigoTipo',
                                function($element) {
                                    return $element['codigoTipo'].' - '.$element['descripcion'].' - '.$element['acciones'];
                                }),
                               'options' => [
                               'placeholder' => '- Selecccione -',
                               'id'=>'ddl-codigoTipo',
                               'onchange'=>'javascript:obtenerTipo()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                    ],
                                    ]);
                        }
                      ?>
             <!--?= $form->field($model, 'codigoTipo')->widget(Select2::classname(), [
               /** 'data' => ArrayHelper::map(TipoMovimientos::find()->all(),'codigoTipo','descripcion'),
                'language'=>'es',
                'options' => ['placeholder' => '- Selecccione -'],
                'pluginOptions' => [
                'allowClear' => true,
                ],]);**/
                ?-->
            </div>
        </div>
        <div class="col-lg-3">
        <div class="form-actions" id="yw3">
          <?php
          if(@$referencia = MovimientosSesion::getReferencia()){
                                echo "<h4><strong>Doc. Referencia</strong><br> ".$referencia." ";
                                echo Html::a('', ['/movimientos/delete-referencia'],['class'=>'glyphicon glyphicon-pencil']);
                                echo "</h4>";
                        }
                        else{
            $form = ActiveForm::begin([
            'id' => 'clienteN-form',
                                //'formConfig' => ['style' => 'POSITION: absolute', 'labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
            'action'=>['/movimientos/referencia'],
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>false,
            ]);

          echo '<strong>Doc. Referencia</strong>';
          echo '<div class="input-group">';
          echo Html::input('text', 'new-referencia', '', ['id' => '', 'class' => 'form-control', 'onkeypress'=>'return isNumberOrLetter(event)']);
          echo '<div class="input-group-btn">';
          echo '<input type="submit" value="Agregar" class="btn">';
                                //echo Html::submitButton('Submit', ['style' => 'clear:both;', 'class' => 'submit']);
          echo '</div>';
          echo '</div>';
          ActiveForm::end();
      }
          ?>

          <?php  ?>
          <!--?= $form->field($model, 'documentoRef')->textInput(['maxlength' => true]) ?-->
      </div>
      </div>

                  <div class="form-actions col-lg-4" id="yw3">
                       <?php
                       MovimientosSesion::setEstado('Pendiente');
                       if(@$estado = MovimientosSesion::getEstado()){
                                echo "<h4><strong>Estado</strong><br> ".$estado." ";
                                //echo Html::a('', ['/movimientos/delete-estado'],['class'=>'glyphicon glyphicon-pencil']);
                                echo "</h4>";
                        }else{
                             echo '<strong>Estado</strong>';
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                               'name' => '',
                               'data' => ["Aplicado" => "Aplicado", "Pendiente" => "Pendiente"],
                               'options' => [
                               'placeholder' => '- Selecccione -',
                               'id'=>'estado',
                               'onchange'=>'javascript:obtenerEstado()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                    ],
                                    ]);
                        }
                      ?>
            </div>

</div>
 <div class="col-lg-12">
           <div class="col-lg-7">
        <div class="form-actions" id="yw3">
          <?php
          if(@$observaciones = MovimientosSesion::getObservaciones()){
                                echo "<h4><strong>Observaciones</strong><br> ".$observaciones." ";
                                echo Html::a('', ['/movimientos/delete-observaciones'],['class'=>'glyphicon glyphicon-pencil']);
                                echo "</h4>";
                        }
                        else{
            $form = ActiveForm::begin([
            'id' => 'clienteN-form',
                                //'formConfig' => ['style' => 'POSITION: absolute', 'labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
            'action'=>['/movimientos/observaciones'],
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>false,
            ]);

          echo '<strong>Observaciones</strong>';
          echo '<div class="input-group">';
          // echo '<textarea class="form-control" rows="3"></textarea>';
          echo Html::input('textarea', 'new-observaciones', '', ['id' => '','rows'=>'3' , 'class' => 'form-control']);
          echo '<div class="input-group-btn">';
          echo '<input type="submit" value="Agregar" class="btn">';
                                //echo Html::submitButton('Submit', ['style' => 'clear:both;', 'class' => 'submit']);
          echo '</div>';
          echo '</div>';
          ActiveForm::end();
      }
          ?>

          <?php  ?>
          <!--?= $form->field($model, 'documentoRef')->textInput(['maxlength' => true]) ?-->
      </div>
      </div>
    </div>
 <?php if($tipo && $estado) : ?>
<div  id="formid" class="col-lg-12">
<br>
<legend></legend>
<div class="">
        <div class="col-lg-3">
          <div class="form-actions" id="yw3">
              <?php
              echo '<strong>Cantidad</strong>';
            echo Html::input('text', 'new-cantidad', '', ['autofocus' => 'autofocus', 'id' => 'cantidad', 'class' => 'form-control' ,'onkeypress'=>'return isNumberDe(event)']);
              ?>
          </div>

          <!--?= /* $form->field($model, 'cantidad')->textInput(['maxlength' => true]) */?-->
      </div>

  <div class="col-lg-4">
      <div class="form-actions" id="yw3">
          <?php
          echo '<strong>Producto</strong> ';
          echo Html::a('', '#', [
                                'class' => 'glyphicon glyphicon-barcode',
                                'id' => 'activity-index-link-inventario',
                                'data-toggle' => 'modal',
                                'data-trigger'=>'hover', 'data-content'=>'Selecciona para mostrar los productos en el inventario',
                                'data-target' => '#modalinventario',
                                'data-url' => Url::to(['#']),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Inventario'),
                                ]);
                           /* echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                               'name' => '',
                               'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',

                                function($element) {
                                    return $element['codProdServicio'].' - '.$element['nombreProductoServicio'];
                                }),
                               'options' => [
                               'placeholder' => '- Seleccione -',
                               'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                               'id'=>'producto',
                                    'onchange'=>'javascript:agrega_productoi()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                    ],
                                    ]);*/
          echo Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '30', 'id' => 'pro_in', 'class' => 'form-control', /*'data-provide' => 'typeahead',*/ 'placeholder' =>'Ingrese código de producto aqui', 'onkeyup'=>'javascript:this.value=Mayuculas(this.value)', 'onchange' => 'javascript:agrega_productoi(this.value)']);

         ?>
      </div>

    </div>

                </div> </div>
                <?php endif ?>
                <!--?php ActiveForm::end(); ?-->

            <div class="col-lg-12" id="conten_prom">
            <br>
            <?php
            //http://www.programacion.com.py/tag/yii
                //echo '<div class="grid-view">';
                echo '<div class="">';
                echo '<table class="items table table-striped">';
                echo '<thead>';
                printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th class="actions button-column">&nbsp;</th></tr>',
                        'CÓDIGO',
                        'PRODUCTO',
                        'CANT. ANT',
                        'CANT. MOV',
                        'CANT. INV',
                        'PRECIO'
                        // 'ESTADO'
                        );
                echo '</thead>';
            if($products) {
            ?>
            <?php
            echo '<tbody>';
                foreach($products as $position => $product) {
                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td class="actions button-column">%s</td></tr>',
                            $product['cod'],
                            $product['desc'],
                            $product['can_ant'],
                            $product['cant'],
                            $product['cantinv'],
                            '₡'.number_format($product['prec'],2),
                            // $product['est'],

                               Html::a('', ['/movimientos/deleteitem', 'id' => $position,'id0' => $product['cod'], 'can'=>$product['cant']], ['class'=>'glyphicon glyphicon-trash','data' => [
                                    'confirm' => '¿Está seguro de quitar este producto del Movimiento a Inventario?',],])
                            );
                        //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/


                }
            echo '</div>';
            echo '</tbody>';
            ?>
            <?php
            }
                echo '</table>';
            ?>
            </div>

        </div>
   </div>
