<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\MovimientosSesionUpdate;
use yii\bootstrap\Modal;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $model backend\models\Movimientos */

$this->title = 'Actualizar Movimiento: ' . ' ' . $model->idMovimiento;
$this->params['breadcrumbs'][] = ['label' => 'Movimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idMovimiento, 'url' => ['view', 'id' => $model->idMovimiento]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<?php $tipo = MovimientosSesionUpdate::getTipo(); ?>
<?php $referencia = MovimientosSesionUpdate::getReferencia(); ?>
<?php $producto = MovimientosSesionUpdate::getContenidoMovimiento();
      MovimientosSesionUpdate::setId_movimiento($model->idMovimiento);
?>
<script type="text/javascript">
window.onkeydown = tecla;

	function tecla(event) {
		//event.preventDefault();
		num = event.keyCode;

		if(num==113)
			$('#modalinventario').modal('show');//muestro la modal

	}
$(document).ready(function(){
       // $('[data-toggle="modal"]').tooltip();
        $('[data-toggle="modal"]').popover();
    });

  function obtenerTipo(){

    var idTipo = document.getElementById("ddl-codigoTipo").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/tipou') ?>",
        type:"post",
        data: {'idTipo': idTipo},
    }) ;
}

  function obtenerObservaciones(){

    var observaciones = document.getElementById("observaciones").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/observacionesu') ?>",
        type:"post",
        data: {'observaciones': observaciones},
    }) ;
}
  function obtenerReferencia(){

    var referencia = document.getElementById("referencia").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/referenciau') ?>",
        type:"post",
        data: {'referencia': referencia},
    }) ;
}

//activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
    });
//me permite buscar por like los productos a BD
function buscar() {
  var textoBusqueda_cod = $("input#busqueda_cod").val();
  var textoBusqueda_des = $("input#busqueda_des").val();
  var textoBusqueda_fam = $("input#busqueda_fam").val();
  var textoBusqueda_cat = $("input#busqueda_cat").val();
  var textoBusqueda_cpr = $("input#busqueda_cpr").val();
  var textoBusqueda_ubi = $("input#busqueda_ubi").val();
  if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
      $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');

  } else {
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/busqueda_producto') ?>",
            type:"post",
            data: {
                'valorBusqueda_cod' : textoBusqueda_cod,
                'valorBusqueda_des' : textoBusqueda_des,
                'valorBusqueda_fam' : textoBusqueda_fam,
                'valorBusqueda_cat' : textoBusqueda_cat,
                'valorBusqueda_cpr' : textoBusqueda_cpr,
                'valorBusqueda_ubi' : textoBusqueda_ubi },
            beforeSend : function() {
              $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
            },
            success: function(data){
                $("#resultadoBusqueda").html(data);
            },
            error: function(msg, status,err){
             //alert('No pasa 286');
            }
        });
    }
}

function agrega_productoi(producto){
    //var producto = document.getElementById("producto").value;
    var observaciones = $("#observaciones").val();
    var estado = $("#estadoi").val();
    var cantidad = $("#cantidad").val();

    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/additemu') ?>",
        type:"post",
        data: { 'estado': estado, 'observaciones' : observaciones, 'producto' : producto, 'cantidad': cantidad, 'idMovimiento': "<?php echo $model->idMovimiento ?>" },
        success: function(){
          $('#modalinventario').modal('hide');
           $("#conten_prom").load(location.href+' #conten_prom',''); //refrescar el div

            var $form = $('#formid');
            $form.find('input, textarea,select').val('');
             $form.find('select2').select2('val', '');

             $('#estadoi').select2('val','');
             // $("#product").select2("val", "");

        },
        error: function(msg, status,err){
         //alert('No pasa 105');
        }
    });

}

//Transformar a mayúsculas todos los caracteres
function Mayuculas(tx){
  //Retornar valor convertido a mayusculas
  return tx.toUpperCase();
}

//espacio que solo permite numero y decimal (.)
function isNumberDe(evt) {
    var nav4 = window.Event ? true : false;
    var key = nav4 ? evt.which : evt.keyCode;
    return (key <= 13 || key==46 || (key >= 48 && key <= 57));
}

function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function refrescarbotones(){
    $("#rebotons").load(location.href+' #rebotons',''); //refrescar el div
    $("#content").load(location.href+' #content',''); //refrescar el div
    $("#content1").load(location.href+' #content1',''); //refrescar el div
}
setTimeout('refrescarbotones()',1);

function imprimir() {
    if (window.print) {
        window.print();
    } else {
        alert("La función de impresion no esta soportada por su navegador.");
    }
}

</script>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="movimientos-update">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
        <div id="rebotons">
         <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>

            <?php if($tipo && $referencia && MovimientosSesionUpdate::getEstado()=='Pendiente') : ?>

                <?= Html::a('Eliminar', ['delete', 'id' => $model->idMovimiento], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Seguro que deseas eliminar los datos de este movimiento?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Aplicar cambios', ['aplicarmovimiento'], [
                            'class' => 'btn btn-success',
                            'data' => [
                                'confirm' => 'Luego de aplicar los cambios no podrá volver a editarlos ¿Seguro que quiere aplicar los cambios?',
                                'method' => 'post', ],]) ?>
            <?php endif ?>
            <?php
            if (MovimientosSesionUpdate::getEstado()=='Aplicado') {
                    echo Html::a('Imprimir', ['report', 'id'=>$model->idMovimiento], ['class' => 'btn btn-info','target'=>'_blank']);
                }
            ?>
            </p>
        </div>


    <?= $this->render('form', [
        'model' => $model,
    ]) ?>

</div>
<?php
//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        //http://blog.collectivecloudperu.com/buscar-en-tiempo-real-con-jquery-y-bootstrap/
        echo '<div class="well"><div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
                                </div>

                <table class="table table-hover" id="tabla_lista_productos">';
        echo '<thead>';
        printf('<tr><th width="150">%s</th>
                    <th width="380">%s</th>
                    <th width="100">%s</th>
                    <th width="130">%s</th>
                    <th width="120">%s</th>
                    <th width="50">%s</th>
                    <th width="140">%s</th>
                    <th class="actions button-column" width="60">&nbsp;</th>
                </tr>',
                        'CÓDIGO LOCAL',
                        'DESCRIPCIÓN',
                        'CANT.INV',
                        'COD.ALTER',
                        'CÓD.PROV',
                        'UBICACIÓN',
                        'PRECIO + IV'
                        );
                echo '</thead>';
                echo '</table>
                        <div id="resultadoBusqueda" style="height: 400px;width: 100%; overflow-y: auto; ">
                </div></div>';

        Modal::end();
