<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\models\ProductoServicios;
use backend\models\TipoMovimientos;
use backend\models\Movimientos;
use backend\models\DetalleMovimientos;

/* @var $this yii\web\View */
/* @var $model backend\models\Movimientos */


?>
<div class="movimientos-view">
<div class="panel panel-default">
<div class="panel-body">
<div class="col-lg-12">
<?php
    $products = DetalleMovimientos::find()->where(["=",'idMovimiento', $model->idMovimiento])->all();
    $TipoMovimientos = TipoMovimientos::findOne($model->codigoTipo);
 ?>
<div class="col-lg-12">
    <?php
        echo '<table style="width: 100%;">
          <tr>
            <th class="tg-031e"><h4><strong>Usuario</strong>: '.$model->usuario.'</h4></th>
            <th class="tg-031e"><h4><strong>Doc. Referencia</strong>: '.$model->documentoRef.'</h4></th>
          </tr>
          <tr>
            <td class="tg-031e"><h4><strong>Tipo de movimiento</strong>: '.$TipoMovimientos->descripcion.'</h4></td>
            <td class="tg-031e"><h4><strong>Estado</strong>: '.$model->estado.'</h4></td>
          </tr>
          <tr>
            <td class="tg-031e" colspan="2"><h4><strong>Observaciones</strong>: '.$model->observaciones.'</h4></td>
          </tr>
        </table>';
    ?>

</div>

<div class="col-lg-12" id="conten_prom">
            <br>
            <?php
            //http://www.programacion.com.py/tag/yii
                //echo '<div class="grid-view">';
                echo '<table class="items table table-striped">';
                echo '<thead>';
                printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr>',
                        'CÓDIGO',
                        'PRODUCTO',
                        'CANT. ANT',
                        'CANT. MOV',
                        'AHORA',
                        'PRECIO'
                        );
                echo '</thead>';
            //if($products) {
            ?>
            <?php
            if($products) {
            echo '<tbody>';
                foreach($products as $position => $product) {
                $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                if ($modelpro) {
                    # code...

                $precio = '₡'.number_format($modelpro->precioVentaImpuesto,2);
                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                            //$product['noEntrada'],
                            $product['codProdServicio'],
                            $modelpro->nombreProductoServicio,
                            $product['cantidadinvant'],
                            $product['cantidad'],
                            $product['cantidadinv'],
                            //number_format($product['precioCompra'],2),
                            //$product['descuento'],
                            //$product['porcentUtilidad'],
                            //number_format($product['preciosinImp'],2),
                            //number_format($product['precioconImp'],2),
                            $precio
                            );
                        //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
                    }else{
                        printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                            //$product['noEntrada'],
                            'Producto inexistente',
                            'Producto inexistente',
                            'Producto inexistente',
                            'Producto inexistente',
                            'Producto inexistente',
                            'Producto inexistente'
                            );
                    }

                }
            echo '</tbody>';
        }
            ?>
            <?php
            //}
                echo '</table>';
            ?>
</div>
</div>
</div>
</div>
</div>
