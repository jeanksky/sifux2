<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\MovimientosSesion;
use yii\bootstrap\Modal;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $model backend\models\Movimientos */

$this->title = 'Crear Movimientos';
$this->params['breadcrumbs'][] = ['label' => 'Movimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $tipo = MovimientosSesion::getTipo(); ?>
<?php $referencia = MovimientosSesion::getReferencia(); ?>
<?php $producto = MovimientosSesion::getContenidoMovimiento(); ?>
<?php $observaciones = MovimientosSesion::getObservaciones(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
window.onkeydown = tecla;

	function tecla(event) {
		//event.preventDefault();
		num = event.keyCode;

		if(num==113)
			$('#modalinventario').modal('show');//muestro la modal

	}

    $(document).ready(function(){
       // $('[data-toggle="modal"]').tooltip();
        $('[data-toggle="modal"]').popover();
        $('#tabla_lista_productos').DataTable();
    });

  function load(page){
        var parametros = {"action":"ajax","page":page};
        $("#loader").fadeIn('slow');
        $.ajax({
            url:'paises_ajax.php',
            data: parametros,
             beforeSend: function(objeto){
            $("#loader").html("<img src='loader.gif'>");
            },
            success:function(data){
                $(".outer_div").html(data).fadeIn('slow');
                $("#loader").html("");
            }
        })
    }

  function obtenerTipo(){

    var idTipo = document.getElementById("ddl-codigoTipo").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/tipo') ?>",
        type:"post",
        data: {'idTipo': idTipo},
    }) ;
}

//Transformar a mayúsculas todos los caracteres
function Mayuculas(tx){
	//Retornar valor convertido a mayusculas
	return tx.toUpperCase();
}

  function obtenerObservaciones(){

    var observaciones = document.getElementById("observaciones").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/observaciones') ?>",
        type:"post",
        data: {'observaciones': observaciones},
    }) ;
}

  function obtenerEstado(){

    var estado = document.getElementById("estado").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/estado') ?>",
        type:"post",
        data: {'estado': estado},
    }) ;
}

  function obtenerReferencia(){

    var referencia = document.getElementById("referencia").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/referencia') ?>",
        type:"post",
        data: {'referencia': referencia},
    }) ;
}
//activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
    });
//me permite buscar por like los productos a BD
    function buscar() {
			var textoBusqueda_cod = $("input#busqueda_cod").val();
			var textoBusqueda_des = $("input#busqueda_des").val();
			var textoBusqueda_fam = $("input#busqueda_fam").val();
			var textoBusqueda_cat = $("input#busqueda_cat").val();
			var textoBusqueda_cpr = $("input#busqueda_cpr").val();
			var textoBusqueda_ubi = $("input#busqueda_ubi").val();
			if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
					$("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');

			} else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/busqueda_producto') ?>",
                type:"post",
								data: {
                    'valorBusqueda_cod' : textoBusqueda_cod,
                    'valorBusqueda_des' : textoBusqueda_des,
										'valorBusqueda_fam' : textoBusqueda_fam,
                    'valorBusqueda_cat' : textoBusqueda_cat,
                    'valorBusqueda_cpr' : textoBusqueda_cpr,
                    'valorBusqueda_ubi' : textoBusqueda_ubi },
                beforeSend : function() {
                  $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                },
								success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 286');
                }
            });
        }
    }

function agrega_productoi(producto){
    //var producto = document.getElementById("producto").value;
    var observaciones = $("#observaciones").val();
    var estado = $("#estadoi").val();
    var cantidad = $("#cantidad").val();

    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimientos/additemi') ?>",
        type:"post",
        data: { 'estado': estado, 'observaciones' : observaciones, 'producto' : producto, 'cantidad': cantidad },
        success: function(){
					 $('#modalinventario').modal('hide');
           $("#conten_prom").load(location.href+' #conten_prom',''); //refrescar el div
           $("#conten_tipo").load(location.href+' #conten_tipo',''); //refrescar el div
           $("#conten_boton").load(location.href+' #conten_boton',''); //refrescar el div


            var $form = $('#formid');
            $form.find('input, textarea').val('');
             $form.find(':selected').val();

        },
        error: function(msg, status,err){
         // alert('Error');
        }
    });

}


function limpiar(){
         $("#observaciones").val("");
         $("#cantidad").val("");
         $("#estadoi").select2("val", "");
         $("#producto").select2("val", "");


}

function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function alfanumerico(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    // patron =/[A-Za-z\s]/;
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

 function isNumberOrLetter(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           if ((charCode > 65 && charCode < 91) || (charCode > 97 && charCode < 123) || (charCode > 47 && charCode < 58))
               return true;
           return false;
           }

 	//espacio que solo permite numero y decimal (.)
 	function isNumberDe(evt) {
 	    var nav4 = window.Event ? true : false;
 	    var key = nav4 ? evt.which : evt.keyCode;
 	    return (key <= 13 || key==46 || (key >= 48 && key <= 57));
 	}
</script>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="movimientos-create">

    <?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <div id="conten_boton">  <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>

            <?= Html::a('Borrar', ['deleteall'], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '¿Seguro que quieres desechar los datos de este movimiento?',
                            'method' => 'post', ],]) ?>


        <?php if($tipo && $referencia && $observaciones && $producto) : ?>
            <?= Html::a('Guardar', ['complete'], ['class' => 'btn btn-success']) ?>
        <?php endif ?>
    </p>
</div>
    <?= $this->render('_form', [
        // 'model' => $model,
    ]) ?>

</div>
<?php
//echo $this->render('busca_productos', []);
//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        //http://blog.collectivecloudperu.com/buscar-en-tiempo-real-con-jquery-y-bootstrap/
        echo '<div class="well"><div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
                                </div>
																<div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
                                </div>

                <table class="table table-hover" id="tabla_lista_productos">';
          echo '<thead>';
					printf('<tr><th width="150">%s</th>
											<th width="380">%s</th>
											<th width="100">%s</th>
											<th width="130">%s</th>
											<th width="120">%s</th>
											<th width="50">%s</th>
											<th width="140">%s</th>
											<th class="actions button-column" width="60">&nbsp;</th>
									</tr>',
													'CÓDIGO LOCAL',
													'DESCRIPCIÓN',
													'CANT.INV',
													'COD.ALTER',
													'CÓD.PROV',
													'UBICACIÓN',
													'PRECIO + IV'
													);
          echo '</thead>';
          echo '</table>

                <div id="resultadoBusqueda" style="height: 400px;width: 100%; overflow-y: auto; ">
                </div>

              </div>';

        Modal::end();
