<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\ProductoServicios;
use backend\models\TipoMovimientos;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MovimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Movimientos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movimientos-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <p style="text-align:right">
        <?= Html::a('+ Nuevo Movimiento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
<!--?php  $dataProvider->pagination->pageSize = 10; ?-->
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

             [
                'attribute' => 'idMovimiento',
                'options'=>['style'=>'width:90px'],
            ],
            [
                'attribute' => 'codigoTipo',
                'label' => 'Cod',
                'options'=>['style'=>'width:60px'],
            ],
            [
            'attribute' =>'codigoTipo',
             'value' => 'codigoTipo0.descripcion',
            ],
            [
                'attribute' => 'documentoRef',
                'options'=>['style'=>'width:100px'],
            ],
            [
                'attribute' => 'observaciones',
                'options'=>['style'=>'width:35%'],
            ],
            // 'observaciones',
            // [
            // 'attribute' =>'codProdServicio',
            //  'value' => 'codProdServicio0.nombreProductoServicio',
            // ],
            // [
            //     'attribute' => 'cantidad',
            //     'options'=>['style'=>'width:90px'],
            // ],
             [
                'attribute' => 'estado',
                'options'=>['style'=>'width:90px'],
            ],
            [
                'attribute' => 'fecha',
                'options'=>['style'=>'width:90px'],
            ],
                        [
                'attribute' => 'usuario',
                'options'=>['style'=>'width:90px'],
            ],
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'options'=>['style'=>'width:30px'],],
        ],
    ]); ?>
</div>
</div>
