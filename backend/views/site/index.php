<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use backend\models\Empresa;
use backend\models\Sistema_empresa;
use backend\models\TipoCambio;
use backend\models\CategoriaTipoCambio;
use yii\helpers\Url;
use kartik\widgets\AlertBlock;
// use kartik\widgets\SideNav;
use kartik\sidenav\SideNav;
use yii\bootstrap\Modal;
$empresa = new Empresa();
$this->title = $empresa->getEmpresa();
if (Yii::$app->params['tipo_sistema']=='lutux') {
  $sistema = 'LUTUX';
} else {
  $sistema = 'SIFUX';
}
$admin = (isset(Yii::$app->user->identity->tipo_usuario) and Yii::$app->user->identity->tipo_usuario == 'Administrador') ? true : false ;
$sys_emp = new Sistema_empresa();
//consulta tipo de cambio del dolar al banco central
    $fecha = date('d/m/Y');
    if (@tipo_cambio($fecha)) {
    $valor_tipo_cambio_banco_central = tipo_cambio($fecha);
    $tipocambio = TipoCambio::find()->where(['usar'=>'x'])->one();
    if (@$tipocambio) {
      $cat_tip_cam = CategoriaTipoCambio::findOne($tipocambio->idCategoria);
      $tipocambio->valor_tipo_cambio = $cat_tip_cam->categoria=="Venta" ? $valor_tipo_cambio_banco_central['venta'] : $valor_tipo_cambio_banco_central['compra'];
      $tipocambio->save();
    }
} else{

}

?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->
<script type="text/javascript">

    /*Permite agregar el popover del button importante!*/
    $(document).ready(function(){
      // $('[data-toggle="popover"]').popover();
       $('[data-toggle="popover"]').popover({ html : true });
       $(".panel").show('slow');
   });


//funcion de las teclas f
//http://www.comocreartuweb.com/consultas/showthread.php/57122-Como-usar-las-teclas-con-javascript
//window.onkeydown = tecla;

  /*function tecla(event) {
    event.preventDefault();
    num = event.keyCode;

    if(num==112)
      alert("Pulsaste F1");

    if(num==123) {
     alert("Pulsaste F12");
     window.close();
    }
  }*/

function contactenos() {
                $('#contactenos').modal('show');//muestro la modal
       }
function condiciones() {
                $('#condiciones').modal('show');//muestro la modal
       }
</script>
<style type="text/css">
.panel{
  display: none;
}
</style>
<div class="site-index" style="">
<?= AlertBlock::widget([
                'type' => AlertBlock::TYPE_ALERT,
                'useSessionFlash' => true,
                'delay' => 5000
            ]);?>
<div class="col-lg-4">
    <br>
        <?php
        echo SideNav::widget([
        'type' => SideNav::TYPE_DEFAULT,
        'encodeLabels' => false,
        'heading' => 'Acciones',
        'items' => [
            [
                'visible'=>Yii::$app->params['tipo_sistema'] == 'lutux' ? true : false,
                'url' => '../web/index.php?r=orden-servicio/create',
                'label' => '<span class="glyphicon glyphicon-copy"></span>&nbsp;Crear nueva orden de servicio',
                // 'icon' => 'home'
            ],
            [
                'visible'=>Yii::$app->params['tipo_sistema'] == 'lutux' ? true : false,
                'url' => '../web/index.php?r=orden-servicio/create_proforma',
                'label' => '<span class="glyphicon glyphicon-copy"></span>&nbsp;Crear nueva orden de servicio <strong>"Proforma"</strong>',
                // 'icon' => 'home'
            ],
            [
                'url' => '../web/index.php?r=proforma/index',
                'label' => '<span class="fa fa-clipboard"></span>&nbsp;Lista de Proformas',
                // 'icon' => 'home'
            ],
            [
                'url' => '../web/index.php?r=cabeza-prefactura/index',
                'label' => '<span class="fa fa-clipboard"></span>&nbsp;Listas de pre-facturas',
                // 'icon' => 'home'
            ],
            [
                'url' => '../web/index.php?r=cabeza-prefactura/create',
                'label' => '<span class="glyphicon glyphicon-open-file"></span>&nbsp;Crear nueva factura',
                // 'icon' => 'home'
            ],
            [
                'url' => '../web/index.php?r=compras-inventario/create&carga=0',
                'label' => '<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Ingresar nueva <strong>COMPRA</strong>',
                // 'icon' => 'home'
            ],
            [
                'url' => '../web/index.php?r=cabeza-caja/index',
                'label' => '<span class="glyphicon glyphicon-credit-card"></span>&nbsp;Caja',
                // 'icon' => 'home'
            ],
            [
                'label' => 'Ayuda - Otros - Términos de <font face="batman" size=4>'.$sistema.'</font>',
                'icon' => 'question-sign',
                'items' => [
                    ['label' => '<span class="glyphicon glyphicon-cog" ></span>&nbsp;Configuración datos de Empresa y activación de factura electronica', 'url'=>'../web/index.php?r=empresa%2Fupdate&id=1', 'visible' => $admin],
                    ['label' => '<span class="glyphicon glyphicon-save" ></span>&nbsp;Respaldo Base de Datos', 'url'=>'../web/index.php?r=backup', 'visible' => $admin],
                    //['label' => '<span class="fa fa-qrcode" ></span>&nbsp;Facturación Electrónica', 'url'=>'../web/index.php?r=empresa%2Fcreate', 'visible' => $admin],
                    ['label' => '<i class="" onclick="contactenos()">Contáctenos para más información</i>', 'icon'=>'phone', 'url'=>'#'],
                    ['label' => '<i class="" onclick="condiciones()">Términos y condiciones de <font face="batman">'.$sistema.'</font></i>', 'icon'=>'alert', 'url'=>'#'],
                ],
            ],
        ],
    ]); ?>


  </div>
  <div class="col-lg-8">
    <center>
    <?php $empresa = new Empresa();?>
        <h2><?php echo $empresa->getEmpresa()?></h2>
        <?php if ( $empresa->getImageurl('html') != Url::base(true).'/' ) { ?>
        <img src="<?php echo $empresa->getImageurl('html') ?>" style="height: 100%; width: 50%">
      <?php } else {
        echo 'Para modificar el nombre y el logo de su empresa valla a: <strong>"Ayuda - Otros - Terminos > Configuración datos de Empresa"</strong> <br><i class="fa fa-picture-o fa-5x" aria-hidden="true"></i>';
      } ?>
            <br>
        <p class="lead">Bienvenido a <font face='batman'><?= $sistema ?></font></p>

        <p>
            <!--a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a-->
            <?php
            if((isset(Yii::$app->user->identity->tipo_usuario) and Yii::$app->user->identity->tipo_usuario == 'Administrador')){
                // echo Html::a('Actualizar información del Negocio',  '../web/index.php?r=empresa%2Fupdate&id=1', ['class'=>'btn btn-success']);
            } else {
                echo "";
            }

            ?>
        </p>
    </center>
  </div>
  <div class="col-lg-12">
<center>
    <button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="<center><strong><span style='font-size: 14pt;'>¡IMPORTANTE!</span></strong></center>" data-content="Su usuario ESTÁ PROTEGIDO por lo tanto CUANDO NO VAYA a usar el sistema en este navegador cierre sesión para un mejor manejo de la integridad del sistema.
       Cualquier proceso realizado en el sistema sin haberse guardado adecuadamente podria perderse ya que el sistema <strong>elimina</strong> todo lo que contiene su sesión sin aplicarse.
        <br><br>
        <center>Cualquier problema para ingresar a la aplicacion se deberá comunicar a <span style='font-size: 14pt;'><strong><span style='color: #9cc44d;'>&lt;</span><span style='color: #41739b;'>&gt;nel</span><span style='color: #9cc44d;'>ux</span></strong></span> para que le habilitemos una nueva sesión a su respectivo usuario.
        </center>">Importante!</button>
        </center>
    </div>
    <?php
/*
$mysqli = new mysqli("localhost", "root", "jsky1987", "nelux_frontend");
if ($mysqli->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}
echo $mysqli->host_info . "\n";

$mysqli = new mysqli("127.0.0.1", "root", "jsky1987", "nelux_frontend", 3306);
if ($mysqli->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

echo $mysqli->host_info . "\n";*/
?>
  </div>

    <!--div class="body-content">


        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2-->
                <!--button type="button" class="btn btn-default btn-circle btn-lg"><i href="#tramado" data-toggle="tab" class="fa fa-gears"></i></button-->
                <!--p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div-->
            <!--div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div-->
</div>
<?php
//------------------------modal para contactenos
        Modal::begin([
            'id' => 'contactenos',
            'size'=>'',
            'header' => '<center><h4 class="modal-title">Contáctenos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'>
        Para proceder a contactarnos por motivo de soporte y solución de algún problema del sistema, por favor,
        diríjase a la opción inferior de la barra de accesos directos y seleccione <strong>Notificar un problema...</strong>;
        esta opción le permitirá enviarnos su solicitud de inmediato a nuestro buzón de emergencia. <br><br>
        <h4>nelux Professional Services.<br>
        Nicoya, Guanacaste. Costa Rica.<br>
        Tel: 8380-1336 / 8980-7574<br>
        Mail: soporte@neluxps.com<br></h4>
        </div>";

        Modal::end();

//------------------------modal para contactenos
        Modal::begin([
            'id' => 'condiciones',
            'size'=>'',
            'header' => '<center><h4 class="modal-title">Términos y condiciones de <font face="batman">'. $sistema .'</font></h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'>
        <strong>Para los efectos del presente Acuerdo, pedimos que lea detenidamente la importancia del los términos
        y concisiones que usted como usuario o empresa tiene al adquirir nuestro producto.</strong>
        <br><br>
        Nelux le ofrece el producto con la ultima versión liberada del sistema a la fecha.
        Por lo tanto si hay una nueva versión con mejoras del mismo su producto serán actualizado solo si
        su licencia está al día.
        <br><br>
        El valor de las licencias de <font face='batman'>". $sistema ."</font> significa que usted como cliente puede usar el sistema segun el contrato de servicios adquiridos contando con la garantía de Nelux pero siendo excluido de la comercialización de este producto como si fuera propio.
        <br><br>
        El costo mensual de las licencias corresponde al soporte y el derecho de adquirir las mejoras del sistema (+ cantidad de usuarios).
        <br><br>
        Nelux es el propietario de <font face='batman'>". $sistema ."</font>, por lo tanto es el único que puede comercializar el producto.
        <br><br>
        <font face='batman'>". $sistema ."</font> esta desarrollado con tecnologías OpenSouce, por lo tanto nuestro cliente solo tendrá que pagar
        el derecho de usar el producto, el alojamiento donde tiene que correr el sistema y el hardware que el
        sistema necesita para su completa manipulación.
        <br><br>
        <font face='batman'>". $sistema ."</font> cuenta con código encriptado proporcionando así seguridad a la hora de algún ataque de copyright.
        <br><br>
        <span style='font-size: 14pt;'><strong><span style='color: #9cc44d;'>&lt;</span><span style='color: #41739b;'>&gt;nel</span><span style='color: #9cc44d;'>ux</span></strong></span> no se hará cargo de la perdida de información o desorden de información provocado por el mal uso
        o descuido del sistema por los usuarios.
           </div>";

        Modal::end();

        function etiqueta_final($parser, $name) {
            global $info,$datos,$contador;
            $info[$name][] = $datos;
        }
        function extractor_datos($parser,$data){
                        global $datos;
                        $datos = $data;
        }
        function extractor_datos_tags($parser,$data){
                        global $datos;
                        $datos .= $data;
        }
        function parser_extractor($cadena,$tags=true){

            // Definiendo variables
            global $info,$datos,$contador;
            $info = array();
            $datos = "";
            $contador = 0;

            // Creando el parser
            $xml_parser = xml_parser_create();

            // Definiendo la funciones controladoras
            xml_set_character_data_handler($xml_parser,($tags?"extractor_datos":"extractor_datos_tags"));
            xml_set_element_handler($xml_parser, "", "etiqueta_final");

            // Procesando el archivo
            if (!xml_parse($xml_parser, $cadena)) {
                            die(sprintf("XML error: %s at line %d",
                                                                            xml_error_string(xml_get_error_code($xml_parser)),
                                                                            xml_get_current_line_number($xml_parser)));
            }

            // Liberando recursos
            xml_parser_free($xml_parser);
            return $info;
        }


        /*
        La siguiente Funcion debe recibir por parametro la fecha en formato dd/mm/YYYY
        */
        function tipo_cambio($fecha){
            // Rutas de los archivos XML con el tipo de cambio
            $file["compra"] = "http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=317&tcFechaInicio=$fecha&tcFechaFinal=$fecha&tcNombre=dmm&tnSubNiveles=N"; // Archivo XML
            $file["venta"] = "http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=318&tcFechaInicio=$fecha&tcFechaFinal=$fecha&tcNombre=dmm&tnSubNiveles=N"; // Archivo XML

            // Extrae el tipo cambio con el valor de COMPRA
            $data_file = @file_get_contents($file["compra"]);
            $ainfo = parser_extractor($data_file,false);
            $fuente = parser_extractor($ainfo["STRING"][0]);
            $tipo["compra"] = $fuente["NUM_VALOR"][0];

            // Extrae el tipo cambio con el valor de VENTA
            $data_file = @file_get_contents($file["venta"]);
            $ainfo = parser_extractor($data_file,false);
            $fuente = parser_extractor($ainfo["STRING"][0]);
            $tipo["venta"] = $fuente["NUM_VALOR"][0];

            // Retornando valor de compra y venta del dolar
            if ( $tipo["compra"] == "" or $tipo["venta"] == "" ){
                return false;
            }else{
                return $tipo;
            }

        }


        ?>
