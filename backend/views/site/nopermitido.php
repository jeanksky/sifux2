<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Atención!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
    <h4>No tiene permiso para acceder a esta página. Si usted necesita el permiso, pongase en contacto con el administrador</h4>
    <img src="../images/denegado.png" margin="-4 sspx" style="text-align:right" height="" width="70%">
    </div>
</div>
