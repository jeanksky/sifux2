<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\AlertBlock;
?>
 
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_ALERT,
            'useSessionFlash' => true,
            'delay' => 3000,
        ]);?>
 
<h1>Restaurar contraseña</h1>
<legend></legend>
<p class="text-justify">Para restaurar tu contraseña, ingrese los datos que se le solicitan en el siguiente formulario.</p>
<br>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>

<div class="col-md-6">
<div class="form-group">
 <?= $form->field($model, "email")->
 widget(\yii\widgets\MaskedInput::className(), [
    'name' => 'input-36',
    'clientOptions' => [
    'alias' =>  'email'
    ],
])->label("Correo electrónico") ?>  
</div>
 
<div class="form-group">
 <?= $form->field($model, "password")->input("password")->label("Nueva contraseña")?>  
</div>
 
<div class="form-group">
 <?= $form->field($model, "password_repeat")->input("password")->label("Repetir nueva contraseña")?>  
</div>

<div class="form-group">
 <?= $form->field($model, "verification_code")->input("text")->label("Código de verificación")?>  
</div>

<div class="form-group">
 <?= $form->field($model, "recover")->input("hidden")->label(false) ?>  
</div>

<?= Html::submitButton("Restaurar contraseña", ["class" => "btn btn-primary"]) ?>
</div>
<?php $form->end() ?>
