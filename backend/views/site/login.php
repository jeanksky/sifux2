<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\AlertBlock;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Iniciar sesión';
//$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
  'type' => AlertBlock::TYPE_GROWL,
  'useSessionFlash' => true,
  'delay' => 1000,
]);?>

<style>

   #center{
       margin: auto;
       width: 400px;
       background-color: rgba(256, 256, 256, 0.83);
       border-radius: 5px;
       box-shadow: 0px 3px 7px #A5A3A3;
       padding: 10px;
   }

   .well {
   min-height: 20px;
   padding: 19px;
   margin-bottom: 20px;
   background-color: #f5f5f5;
   border: 1px solid #e3e3e3;
   border-radius: 4px;
   -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
   box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}
#bg{
   position:fixed;
   top:0;
   left:0;
   z-index:-1;
   width: 100%;
   height: 100%;
}



   h2 {
       font-family: 'Segoe UI Light';
       src: url('/surface/Assets/css/fonts/all/light/segoeuil.eot');
       src: url('/surface/Assets/css/fonts/all/light/segoeuil.eot?#iefix') format('embedded-opentype'),
       url('/surface/Assets/css/fonts/all/light/segoeuil.woff') format('woff'),
       url('/surface/Assets/css/fonts/all/light/segoeuil.svg#SegoeUILight') format('svg');
       font-weight: normal;
       font-style: normal;
   }
   .container-login100-form-btn {
     width: 100%;
     display: flex;
     justify-content: center;
   }
   /*se le ponen bordes redondos al btn que muestra las opciones de contraseña*/
   /* .btn-mas-contrasena{
     border-radius: 15px;
   }
   .btn-menos-contrasena{

   } */
   /*le damos estilo al btn de expander y reducir el area de cambiar y restablecer la contraseña */
   .btn-on-form{
     border-radius: 15px;
     background: transparent;
     border: none;
     font-family: Poppins-Medium;
     font-size: 18px;
     color: #000;
   }
   /*cuando se pone el puntore sobre el boton
   da un color al fondo y cambia el color de la letra*/
   .btn-on-form:hover{
     background:#002d40;
     color:#fff;
   }
</style>
<br><br><br>
<body>
<?php
if (Yii::$app->params['tipo_sistema']=='lutux') {
  $logo_sistema = '<img src="../images/LogoMenu.png" height="100%" width="100%" >';
  ?>
  <img id="bg" src="../images/background.jpg"  alt="background" />
<?php } else {
  $logo_sistema = '<img src="../images/LogoMenu2.png" height="100%" width="100%" >';
  ?>
  <img id="bg" src="../images/background2.jpg"  alt="background" />
<?php } ?>
<div class="row">
       <div class="panel panel-default" id="center">
           <div class="legend">
               <center><?= $logo_sistema ?></center> <!--height="130" width="330" -->
               <legend>
                   <center><h2>Iniciar sesión</h2></center>
               </legend>
           </div>
           <!-- /.panel-heading -->
           <div>
            <div class="form-group">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <!--?= $form->field($model, 'username') ?-->
                <!--input type="text" class="form-control" placeholder="Usuario"-->
                <?= $form->field($model, 'username', ['errorOptions' => ['encode' => false],
                   'inputTemplate' => '<div class="form-group input-group"><span class="input-group-addon">
                   <a data-toggle="tab" class="glyphicon glyphicon-user"></a></span>{input}</div>',
                   ])->label('Usuario'); ?>

                   <!--?= $form->field($model, 'password')->passwordInput() ?-->
                   <?= $form->field($model, 'password', ['errorOptions' => ['encode' => false],
                      'inputTemplate' => '<div class="form-group input-group"><span class="input-group-addon"><a data-toggle="tab" class="glyphicon glyphicon-lock"></a></span>{input}</div>',
                       ])->passwordInput()->label('Contraseña'); ?>

                   <!--?= $form->field($model, 'rememberMe')->checkbox()->label('Recuérdame') ?-->
               </div>
               <br>

                   <div class="form-group">
                       <?= Html::submitButton('Ingresar', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                   <div style="text-align:right">
                       <!--a href="/site/recoverpass">¿Olvidaste tu contraseña?</a-->
                       <?php // Html::a('¿Olvidaste tu contraseña?', ['/site/recoverpass-correo'], ['class'=>'']) ?>
                   </div>
                   </div>
               </div>


               <?php ActiveForm::end(); ?>
               <div class="container-login100-form-btn">
                 <!-- <button class="btn btn-default"><span class="glyphicon glyphicon-asterisk"></span>vehiel</button> -->
                 <?= Html::button(Yii::t('app','<span class="glyphicon glyphicon-plus-sign" style="margin-top: 3px;"></span>'),
                 ['class'=>'btn-mas-contrasena btn-on-form']) ?>
                 <?= Html::button(Yii::t('app','<span class="glyphicon glyphicon-remove-sign" style="margin-top: 3px;"></span>'),
                 ['class'=>'btn-menos-contrasena btn-on-form','style'=>'display:none']) ?>
               </div>
               <div id="divBtnContrasena" style="display:none;">
                 <div class="container-login100-form-btn">
                   <?= Html::a(Yii::t('app','Cambiar Contraseña'), ['cambiar-contrasena'],[]) ?>
                 </div>
                 <br />
                 <div class="container-login100-form-btn">

                   <?= Html::a(Yii::t('app','Olvido la Contraseña?'), ['recoverpass-correo'],[]) ?>
                 </div>
               </div>
               <!-- /.panel-body -->
           </div>
           <!-- /.panel -->
   </div></body>
   <script>
   $('.btn-mas-contrasena').on('click', function(event){
     $('#divBtnContrasena').fadeIn('slow');
       $('.btn-mas-contrasena').fadeOut('slow', function(){
         $('.btn-menos-contrasena').fadeIn('slow');
       });
   });
   $('.btn-menos-contrasena').on('click', function(event){
     $('#divBtnContrasena').fadeOut('slow');
       $('.btn-menos-contrasena').fadeOut('slow', function(){
         $('.btn-mas-contrasena').fadeIn('slow');
       });
   });
 </script>
