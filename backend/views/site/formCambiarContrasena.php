<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\Growl;
use kartik\widgets\AlertBlock;
$this->title = Yii::t('app', 'Cambiar Contraseña');
?>

<?php AlertBlock::widget([
  'type' => AlertBlock::TYPE_GROWL,
  'useSessionFlash' => true,
  'delay' => 1000,
]);?>
<div class="panel panel-default">
  <div class="panel-body">
    <!-- <div class="col-md-12 col-lg-12"> -->
    <div class="col-md-12 col-lg-12">
      <h1><?= Html::encode($this->title) ?></h1>
      <!-- <legend></legend> -->

        <br>
        <?php $form = ActiveForm::begin();
        ?>
        <div class="col-md-6">
          <?= $form->field($model, "username")->textInput() ?>
          <?= $form->field($model, "password")->passwordInput() ?>
        </div>
        <div class="col-md-6">
          <?= $form->field($model, "new_password")->passwordInput() ?>
          <?= $form->field($model, "repeat")->passwordInput() ?>

          <?= Html::submitButton(Yii::t('app',"Cambiar"), ["class" => "btn btn-primary"]) ?>
        </div>
        <?php $form->end() ?>

    </div>
  </div>
</div>
