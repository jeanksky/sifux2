<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\Growl;
use kartik\widgets\AlertBlock;
$this->title = Yii::t('app', 'Restaurar la Contraseña');
?>

<?= AlertBlock::widget([
  'type' => AlertBlock::TYPE_GROWL,
  'useSessionFlash' => true,
  'delay' => 1000,
]);?>


<div class="panel panel-default">
  <div class="panel-body">
    <!-- <div class="col-md-12 col-lg-12"> -->
      <div class="col-md-6 col-lg-6">
        <h1><?= Html::encode($this->title) ?></h1>
        <!-- <legend></legend> -->
        <p class="text-justify">Para restablecer la contraseña, ingrese usuario y dirección de correo electrónico asociado a su cuenta.</p>
        <br>
        <?php $form = ActiveForm::begin([
          'method' => 'post',
          'enableClientValidation' => true,
        ]);
        ?>
        <!-- <div class="form-group"> -->
          <?= $form->field($model, "username")->textInput() ?>
          <?= $form->field($model, "email")->
          widget(\yii\widgets\MaskedInput::className(), [
            'name' => 'input-36',
            'clientOptions' => [
              'alias' =>  'email'
            ],
            ]) ?>
          <!-- </div> -->

          <?= Html::submitButton("Enviar Correo", ["class" => "btn btn-primary"]) ?>

          <?php $form->end() ?>
          <br />
          <?= Html::a(Yii::t('app',"Ya tiene un codigo?"),['recoverpass']) ?>
        </div>
      <!-- </div> -->
    </div>
  </div>
