<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <h1><!--?= //Html::encode($this->title) ?-->Error -  #404 - estas extraviado</h1>

    <div class="alert alert-danger">
        <h4>
            <?= nl2br(Html::encode($message)) ?><br>
        
            Se ha producido un error, mientras que el servidor Web procesaba su solicitud, es probable que esta intentando entrar en una página que no existe.
        </h4>
        
    </div>
    <center>
        <img src="../images/notfound.png" margin="-4 sspx" style="text-align:right" height="450" width="">
    </center>

    
    <p>
        Póngase en contacto con nosotros si usted piensa que esto es un error en el servidor. Gracias.
    </p>

</div>
