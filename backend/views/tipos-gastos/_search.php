<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\TiposGastosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipos-gastos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idTipoGastos') ?>

    <?= $form->field($model, 'descripcionTipoGastos') ?>

    <?= $form->field($model, 'idCategoriaGasto') ?>

    <?= $form->field($model, 'estadoTipoGastos') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
