<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TiposGastos */

$this->title = 'Tipo Gasto: ' . ' ' .$model->descripcionTipoGastos;
$this->params['breadcrumbs'][] = ['label' => 'Tipos Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipos-gastos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idTipoGastos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idTipoGastos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTipoGastos',
            'descripcionTipoGastos',
            'idCategoriaGasto',
            'estadoTipoGastos',
        ],
    ]) ?>

</div>
