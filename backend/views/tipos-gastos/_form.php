<?php
use backend\models\CategoriaGastos;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\TiposGastos */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="col-lg-6">
	<div class="panel panel-default">
		<div class="panel-heading">
				Ingrese los datos que se le solicitan en el siguiente formulario.
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">

					<div class="tipos-gastos-form">

					    <?php $form = ActiveForm::begin(); ?>

					    <?= $form->field($model, 'descripcionTipoGastos')->textInput(['maxlength' => true]) ?>

					    <!--?= $form->field($model, 'idCategoriaGasto')->textInput() ?-->

					    	<?= $form->field($model, 'idCategoriaGasto')->widget(Select2::classname(), [
				    			'data' => ArrayHelper::map($model->getCategoriaGastos(),'idCategoriaGastos', function($element){
										return $element->descripcionCategoriaGastos;
									}),
							    'language'=>'es',
							    'options' => ['placeholder' => '- Seleccione -'],
							    'pluginOptions' => [
							        'allowClear' => true,
							    ],
							]);
							    ?>

					    <!--?= $form->field($model, 'estadoTipoGastos')->textInput(['maxlength' => true]) ?-->
					    <?= $form->field($model, 'estadoTipoGastos')->radioList(array('activo'=>'Activo','inactivo'=>'Inactivo')); ?>

					    <div class="form-group">
					        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					    </div>

					    <?php ActiveForm::end(); ?>

					</div>

			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>