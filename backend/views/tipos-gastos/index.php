<?php
use backend\models\CategoriaGastos;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock; //para mostrar la alerta

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TiposGastosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipos Gastos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipos-gastos-index">

<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align: right;">
        <?= Html::a('Crear Tipos Gastos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idTipoGastos',
            'descripcionTipoGastos',
            //'idCategoriaGasto',
            [
                    'attribute' => 'tbl_categoria_gastos.descripcionCategoriaGastos',
                        'label' => 'Categoria Gasto',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $catgasto = CategoriaGastos::findOne($model->idCategoriaGasto);
                            if (@$catgasto)
                              return $catgasto->descripcionCategoriaGastos;
                            else {
                              return 'Sin Categoria Gasto';
                            }


                        },
            ],
            'estadoTipoGastos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
