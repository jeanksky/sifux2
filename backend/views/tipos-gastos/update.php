<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TiposGastos */

$this->title = 'Actualizar Tipo Gasto: ' . ' ' . $model->descripcionTipoGastos;
$this->params['breadcrumbs'][] = ['label' => 'Tipos Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTipoGastos, 'url' => ['view', 'id' => $model->idTipoGastos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipos-gastos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
