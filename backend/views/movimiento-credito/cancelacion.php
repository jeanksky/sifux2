<?php

use yii\helpers\Html;
use backend\models\MovimientoCredito;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use backend\models\CreditoSession;
use backend\models\Empresa;
use backend\models\MovimientoCobrar;
use backend\models\NcPendientes;
use yii\helpers\Json;
use kartik\widgets\Select2;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use backend\models\Clientes;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\web\JsExpression;

$load = \Yii::$app->request->BaseUrl.'/img/load.gif';
$loading3 = \Yii::$app->request->BaseUrl.'/img/loading3.gif';
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
$empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
//PARA ABONO POR DEPOSITO --------------------------------------------------------------------------------------------

$data = ArrayHelper::map(EntidadesFinancieras::find()->all(), 'idEntidad_financiera', 'descripcion');
$data2 = ArrayHelper::map(CuentasBancarias::find()->all(), 'numero_cuenta', function($element) {
		return $element['numero_cuenta'].' - '.$element['descripcion'];
});
$medio = Select2::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data,
						'options' => ['id'=>'ddl-entidad', 'onchange'=>'obtenMedio(this.value,2)','placeholder' => 'Seleccione entidad financiera...'],
						'pluginOptions' => [
								'initialize'=> true,
								'allowClear' => true
						],
				]).'<br>';
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$cuenta = DepDrop::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data2,
						'type' => DepDrop::TYPE_SELECT2,
						'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
						'options' => ['id'=>'tipoCuentaBancaria', 'onchange'=>'obtenCuenta(this.value,2)','placeholder' => 'Seleccione...'],
						'pluginOptions' => [
								'depends'=>['ddl-entidad'],
								'initialize'=> true, //esto inicializa instantaniamente junto a entidad financiera
								'placeholder' => 'Seleccione cuenta...',
								'url' => Url::to(['cabeza-prefactura/cuentas']),
								'loadingText' => 'Cargando...',
								//'allowClear' => true
						],
				]);
$fechadepo = DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName','value'=>date("d-m-Y"), 'options' => ['class'=>'form-control', 'readonly' => true, 'placeholder'=>'Fecha depósito', 'id'=>'fechadepo', 'onchange'=>'obtenFecha(this.value)']]);
$comprobacion = '<br>'.Html::input('text', '', '', ['size' => '20', 'onkeypress'=>'return isNumberDe(event)', 'id' => 'comprobacion', 'disabled'=>false, 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value,2);']);
//--------------FIN ABONO POR DEPOSITO

//--------------PARA ABONO CON TARJETA
$medio_tarjeta = Html::input('text', '', '', ['size' => '19', 'id' => 'medio_tarjeta', 'class' => 'form-control', 'placeholder' =>' Medio ( MasterCard, VISA, otra )', 'onkeyup' => 'obtenMedio(this.value,3);']);
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$comprobacion_tarjeta = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_tarjeta', 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value,3);']);
//-------------FIN ABONO POR TARJETA

//--------------PARA ABONO POR CHEQUE
$medio_cheque = Select2::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data,
						'options' => ['id'=>'ddl-entidad-cheque', 'onchange'=>'obtenMedio(this.value,4)','placeholder' => 'Seleccione entidad financiera...'],
						'pluginOptions' => [
								'allowClear' => true
						],
				]);
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$comprobacion_cheque = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_cheque', 'class' => 'form-control', 'placeholder' =>' # Cheque', 'onkeyup' => 'obtenCom(this.value,4);']);
//--------------FIN ABONO POR CHEQUE
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
	$('[data-toggle=popover_pago_abono]').popover();//me dice el tipo de popover que quiero correspondiente al id

	//espacio que solo permite numero y decimal (.)
	function isNumberDe(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

    //muestro una animacion de cargando mientras se hace el proceso
	/*function cargar_load_buton(){
		$('#load_buton_nc').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
	}*/





	//obtengo la o las facturas que voy a cancelar
	function obtener_facturas_marcadas(){
		var checkboxValues = "";//
		//var checkboxValues = new Array();//
		$('#load_buton').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
		$('input[name="checkboxRow[]"]:checked').each(function() {//
			checkboxValues += $(this).val() + ",";//
			//checkboxValues.push($(this).val());//
		});//
		//eliminamos la última coma.
		checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//

		//alert(checkboxValues);
		$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/obtener_facturas_marcadas') ?>",
                type:"post",
                data: { 'checkboxValues' : checkboxValues },
                success: function(data){
                    /*$("#not_cr_pen").load(location.href+' #not_cr_pen','');
                    $('#concepto').val('');
                    $('#monto_nc').val('');*/
                    //alert(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 69');
                }
            });
	}

	//obtengo la o las facturas a cancelar que selecciono para no cancelarlas, enviarlas de regreso a Facturas pendientes
	function obtener_facturas_marcadas_regreso(){
		var checkboxValues = "";//
		//var checkboxValues = new Array();//
		$('#load_buton2').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
		$('input[name="checkboxRowinv[]"]:checked').each(function() {//
			checkboxValues += $(this).val() + ",";//
			//checkboxValues.push($(this).val());//
		});//
		//eliminamos la última coma.
		checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//

		//alert(checkboxValues);
		$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/obtener_facturas_marcadas_regreso') ?>",
                type:"post",
                data: { 'checkboxValues' : checkboxValues },
                success: function(data){
                },
                error: function(msg, status,err){
                 //alert('No pasa 98');
                }
            });
	}

	//Marcador de facturas para obtener datos a manipular
	function marcarfactura(x, value, total_a_pagar, saldo) {
		//http://foro.elhacker.net/desarrollo_web/evento_onfocus_en_fila_de_una_tabla-t381665.0.html
        // Obtenemos todos los TR de la tabla con id "tabla_facturas_pendientes"
        // despues del tbody.
        var elementos = document.getElementById('tabla_facturas_pendientes').
        getElementsByTagName('tbody')[0].getElementsByTagName('tr');

        // Por cada TR empezando por el segundo, ponemos fondo blanco.
        for (var i = 0; i < elementos.length; i++) {
            elementos[i].style.background='white';
        }
        // Al elemento clickeado le ponemos fondo amarillo.
        x.style.background="yellow";
        document.getElementById("btn_movimiento_factura").innerHTML='';
        inyectarpagoparcial(value, total_a_pagar, saldo);

        //$("#nuevo_saldo_div").load(location.href+" #nuevo_saldo_div","");
        //$("#refr_nc_div").load(location.href+" #refr_nc_div","");
        //$("#movi_credit").load(location.href+" #movi_credit","");
        $('#pagoparcial').val('');
        $('#nuevo_saldo').val('');
        $('#referencia').val('ABONO A FACTURA');
    }


    //inyecto los datos del cliente que necesito para manipular un movimiento de credito
    function inyectarpagoparcial(value, total_a_pagar, saldo){
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/inyectardatosamanipular') ?>",
                type:"post",
								dataType: 'json',
                data: { 'factura' : value, 'total_a_pagar': total_a_pagar/*, 'saldo_de_la_factura': saldo*/ },
                beforeSend: function() {
	            	$('#movi_credit').html('<center><img src="<?php echo $loading3; ?>" style="height: 200px;"></center>');
	            },
                success: function(data){
                    //$("#resultadoBusqueda").html(data);
										document.getElementById("btn_movimiento_factura").innerHTML = data.btn_movimiento_factura;
										document.getElementById("movi_credit").innerHTML = data.movi_credit;
                    $('#numero_factura').val(' '+value);
    								$('#saldo_de_la_factura').val(data.saldo_de_la_factura);
										$('#saldo_de_la_factura_masc').val(data.saldo_de_la_factura_masc);
										$('#suma_monto_nc').val(data.suma_monto_nc);
                },
                error: function(msg, status,err){
                 alert('No pasa 64');
                }
            });

    }

    //agrego nota de credito usando ajax
    function agrega_nota_credito(){
    	var concepto = document.getElementById('concepto').value;
    	var monto_nc = document.getElementById('monto_nc').value;
        var idCliente = '<?php echo $idCliente; ?>';
    	if (concepto =='' || monto_nc == '') {
    		alert('Por favor no deje campos vacios');
    	} else {
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/agrega_nota_credito') ?>",
                type:"post",
                data: { 'concepto' : concepto, 'monto_nc': monto_nc, 'idCliente': idCliente },
                success: function(data){
                    $("#not_cr_pen").load(location.href+' #not_cr_pen','');
                    $('#concepto').val('');
                    $('#monto_nc').val('');
                },
                error: function(msg, status,err){
                 alert('No pasa 87');
                }
            });
    	}
    }

    //Actualizo nota de credito usado ajax una ves esten cargados
    function actualizar_nota_credito(){
        var concepto = document.getElementById("concepto_u").value;
        var monto_nc = document.getElementById("monto_nc_u").value;
        var position = document.getElementById("position_u").value;
        if (concepto =="" || monto_nc == "") {
            alert("Por favor no deje campos vacios");
        } else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl("movimiento-credito/actualizar_nota_credito") ?>",
                type:"post",
                data: { "concepto" : concepto, "monto_nc": monto_nc, "position": position },
                success: function(data){
                    /*$('#concepto_u').val('');
                    $('#monto_nc_u').val('');*/
                    //setTimeout($("#not_cr_pen").load(location.href+" #not_cr_pen",""),50);
                    $("#not_cr_pen").load(location.href+" #not_cr_pen","");
                },
                error: function(msg, status,err){
                 alert("No pasa 111");
                }
            });
        }
    }

    //Carga en la modal modal_update_notasc_pen los datos a ser actualizados
    function actualiza_modal_credpen(position){

        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/actualiza_modal_credpen') ?>",
            type:"post",
            data: { 'position': position },
            success: function(data){
                $( "#modal_update_notasc_pen" ).html( data );
            },
            error: function(msg, status,err){
             alert('No pasa 128');
            }
        });
    }

    //calculadora me devuelve el nuevo saldo restando el saldo anterior menos el monto actual
    function obtenerPagoparcial(){
    	pago = document.getElementById("pagoparcial");
        saldo = document.getElementById("saldo_de_la_factura");
        pago = Number(pago.value);
        saldo = Number(saldo.value);
        nuevosaldo = saldo-pago;
				$.ajax({
						url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/calcu_nuevo_saldo') ?>",
						type:"post",
						data: { 'nuevosaldo' : nuevosaldo },
						success: function(data){
							$('#nuevo_saldo').val(data);
						}
					});
        //$('#nuevo_saldo').val(nuevosaldo.toLocaleString('hi-IN', { style: 'decimal', decimal: '2' }));
    }

		//obtener el medio de pago
		function obtenMedio(valor, tipo)
		{
		   // document.getElementById("resultado1").innerHTML=valor;
		    $.ajax({
		        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addmedio-u') ?>",
		        type:"post",
		        data: {'medio': valor},
		    }) ;
					compruebatipopago(tipo);

		}

		//envia datos a controlador para cargar cuenta bancaria en session y
		//desabilita el campo de comprovacion si la entidad y la cuenta estan completas
		function obtenCuenta(valor, tipo) {
		        $.ajax({
		            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addcuenta-u') ?>",
		            type:"post",
		            data: {'cuenta': valor},
		        });
		        compruebatipopago(tipo);
		}

		//Obtiene el comprobante para enviarlo al controlador y almacenarlo ensession
		function obtenCom(valor, tipo)
		{
		        $.ajax({
		            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addcom-u') ?>",
		            type:"post",
		            data: {'comprobacion': valor},
		        });
		        compruebatipopago(tipo)
		   // document.getElementById("resultado1").innerHTML=valor;
		}

		//compruebo que los datos estan llenos a la hora de cancelas de acuerdo al tipo de pago para abilitar o no el boton
		function compruebatipopago(tipo) {
				var entidadFinanc = '';
				var cuenta = '0';
				var comprobacion = '';
				//abono
				if (tipo==2) {
				  entidadFinanc = document.getElementById("ddl-entidad").value;
					cuenta = document.getElementById("tipoCuentaBancaria").value;
					comprobacion = document.getElementById("comprobacion").value;
				} else if (tipo==3) {
					entidadFinanc = document.getElementById("medio_tarjeta").value;
					comprobacion = document.getElementById("comprobacion_tarjeta").value;
				} else if (tipo==4) {
				  entidadFinanc = document.getElementById("ddl-entidad-cheque").value;
					comprobacion = document.getElementById("comprobacion_cheque").value;
				}
				//cancelacion grupal
				else if (tipo==6) {
				  entidadFinanc = document.getElementById("ddl-entidad_c").value;
					cuenta = document.getElementById("tipoCuentaBancaria_c").value;
					comprobacion = document.getElementById("comprobacion_c").value;
				} else if (tipo==7) {
					entidadFinanc = document.getElementById("medio_tarjeta_c").value;
					comprobacion = document.getElementById("comprobacion_tarjeta_c").value;
				} else if (tipo==8) {
				  entidadFinanc = document.getElementById("ddl-entidad-cheque_c").value;
					comprobacion = document.getElementById("comprobacion_cheque_c").value;
				}

		    if (entidadFinanc == '' ||  comprobacion == '' || cuenta == '') {
		        if (tipo==2) $('#boton-abonar-deposito').addClass('disabled');
						else if (tipo==3) $('#boton-abonar-tarjeta').addClass('disabled');
						else if (tipo==4) $('#boton-abonar-cheque').addClass('disabled');
						else if (tipo==6) $('#boton-abonar-deposito_c').addClass('disabled');
						else if (tipo==7) $('#boton-abonar-tarjeta_c').addClass('disabled');
						else if (tipo==8) $('#boton-abonar-cheque_c').addClass('disabled');
		    }
		    else {
		        $.ajax({
		            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/compruebatipopago') ?>",
		            type:"post",
		            data: {'comprobacion': comprobacion, 'cuenta_deposito': cuenta, 'entidadFinanc': entidadFinanc, 'tipo': tipo},
		            success: function(data){
		                if (data=='repetido') {
											if (tipo==2) {
		                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación rechasada, ya esta en registro';
		                    $('#boton-abonar-deposito').addClass('disabled');
											} else if (tipo==3) {
												document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación rechasada, ya esta en registro';
		                    $('#boton-abonar-tarjeta').addClass('disabled');
											} else if (tipo==4) {
												document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación rechasada, ya esta en registro';
		                    $('#boton-abonar-cheque').addClass('disabled');
											}
											else if (tipo==6) {
		                    document.getElementById("nota_cancelacion_c").innerHTML = 'Comprobación rechasada, ya esta en registro';
		                    $('#boton-abonar-deposito_c').addClass('disabled');
											} else if (tipo==7) {
												document.getElementById("nota_cancelacion_2_c").innerHTML = 'Comprobación rechasada, ya esta en registro';
		                    $('#boton-abonar-tarjeta_c').addClass('disabled');
											} else if (tipo==8) {
												document.getElementById("nota_cancelacion_3_c").innerHTML = 'Comprobación rechasada, ya esta en registro';
		                    $('#boton-abonar-cheque_c').addClass('disabled');
											}
		                } else {
											if (tipo==2) {
												$('#boton-abonar-deposito').removeClass('disabled');
		                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación aceptada';
											} else if (tipo==3) {
												$('#boton-abonar-tarjeta').removeClass('disabled');
		                    document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación aceptada';
											} else if (tipo==4) {
												$('#boton-abonar-cheque').removeClass('disabled');
		                    document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación aceptada';
											}
											else if (tipo==6) {
												$('#boton-abonar-deposito_c').removeClass('disabled');
		                    document.getElementById("nota_cancelacion_c").innerHTML = 'Comprobación aceptada';
											} else if (tipo==7) {
												$('#boton-abonar-tarjeta_c').removeClass('disabled');
		                    document.getElementById("nota_cancelacion_2_c").innerHTML = 'Comprobación aceptada';
											} else if (tipo==8) {
												$('#boton-abonar-cheque_c').removeClass('disabled');
		                    document.getElementById("nota_cancelacion_3_c").innerHTML = 'Comprobación aceptada';
											}
		                }

		            },
		            error: function(msg, status,err){
		                            alert('error en linea 328, consulte a nelux');
		                        }
		        });
		        //$('#boton-abonar-deposito').removeClass('disabled');
		    }

		}

		//tooltip para pago de abono
		function accion_pago_abono() {
			$('#accion_pago_abono').popover('show');
			var tipo = document.getElementById("tipo_pago_abono").value;
			if (tipo==1) {
				$('#accion_pago_abono').popover('hide');
			} else {

			}

		}

    //Agrega un nuevo abono a la factura seleccionada
    function agregar_abono(tipo_pago_abono){

    	var pago = document.getElementById("pagoparcial");
      var referencia = document.getElementById("referencia");
			if (tipo_pago_abono == 1) { //estos deporsitos som para cuando el tipo de pago de abono es con efectivo
				if (pago.value=='' || referencia.value=='') {
        	pago.style.border = "3px solid red";
        	referencia.style.border = "3px solid red";
        } else {
        	var r = confirm("Está seguro de realizar este pago!");
        	if (r == true) {
        		$('#botones_abono').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
					    $.ajax({
				            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/agregar_abono') ?>",
				            type:"post",
				            data: { 'pago': pago.value, 'referencia': referencia.value, 'tipo_pago_abono': tipo_pago_abono },
				            success: function(data){
				                $( "#botones_abono" ).html( data );
				            },
				            error: function(msg, status,err){
				            }
			        	});
					} else { }

        }
			} else { //esta opcion sería para los abonos que son con deposoto  cheque o tarjeta
				if (pago.value=='' || referencia.value=='' ) {
        	pago.style.border = "3px solid red";
        	referencia.style.border = "3px solid red";
        } else {//*****asegurar que diferencie cuando es un abono de deposito con uno de tarjeta y con uno de cheque*++++
        	var r = confirm("Está seguro de realizar este pago!");
					ddl_entidad = ''; tipoCuentaBancaria = '0'; fechadepo = ''; comprobacion = '';
					if (tipo_pago_abono==4) {
						ddl_entidad = document.getElementById("ddl-entidad").value;
						tipoCuentaBancaria = document.getElementById("tipoCuentaBancaria").value;
						fechadepo = document.getElementById("fechadepo").value;
						comprobacion = document.getElementById("comprobacion").value;
					} else if (tipo_pago_abono==2) {
						ddl_entidad = document.getElementById("medio_tarjeta").value;
						comprobacion = document.getElementById("comprobacion_tarjeta").value;
					} else if (tipo_pago_abono==3) {
						ddl_entidad = document.getElementById("ddl-entidad-cheque").value;
						comprobacion = document.getElementById("comprobacion_cheque").value;
					}
        	if (r == true) {
        		$('#botones_abono').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
						//alert(ddl_entidad + ' ' + tipoCuentaBancaria + ' ' + fechadepo + ' ' + comprobacion);
					  $.ajax({
				            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/agregar_abono') ?>",
				            type:"post",
				            data: { 'pago': pago.value , 'referencia': referencia.value, 'tipo_pago_abono': tipo_pago_abono,
									  'ddl_entidad':	ddl_entidad, 'tipoCuentaBancaria': tipoCuentaBancaria, 'fechadepo': fechadepo, 'comprobacion': comprobacion},
				            success: function(data){
				               // $( "#modal_update_notasc_pen" ).html( data );
				            },
				            error: function(msg, status,err){
				             //alert('No pasa 150');
				            }
			        	});

					} else { }

        }
			}


    }

    //para enviar referencia de cancelacion a session
    function obtenerReferenciacanelacion(){
    	referenciacance = document.getElementById("referenciacance").value;
    	$.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/obtenereferenciacancelacion') ?>",
            type:"post",
            data: { 'referenciacance': referenciacance },
            success: function(data){
            },
            error: function(msg, status,err){
            }
        });
    }

    //con esta modal consulto los datos de los movimientos ya sean de AB o NC usando solo el id del movimiento
    function consulta_modal_movi(idMov){

    	$.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/consulta_modal_movi') ?>",
            type:"post",
            data: { 'idMov': idMov },
            success: function(data){
                $( "#mostrar_movimiento_modal" ).html( data );
            },
            error: function(msg, status,err){
             alert('No pasa 144');
            }
        });

    }

    //Me permite enviar el tipo de fech que quiero mostrar ya sea vencidas, por vencer...
    function facturafecha(tipofecha){

    	$.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/facturafecha') ?>",
            type:"post",
            data: { 'tipofecha': tipofecha },
            beforeSend: function() {
            	$('#facturas-pendientes').html('<center><img src="<?php echo $load; ?>" style="height: 300px;"></center>');
            },
            success: function(data){
                $("#facturas-pendientes").load(location.href+" #facturas-pendientes","");
            },
            error: function(msg, status,err){
             alert('No pasa 160');
            }
        });
    }

    //cancelar varias facturas
    function cancelarvariasfacturas(tipo, idCl,suma_saldo_total_can,totlcanc,totlnc) {
        var r = confirm("¿Está seguro de cancelar las facturas?");
        if (r == true) {
					ddl_entidad = ''; tipoCuentaBancaria = '0'; fechadepo = ''; comprobacion = '';
					if (tipo==4) {
						ddl_entidad = document.getElementById("ddl-entidad_c").value;
						tipoCuentaBancaria = document.getElementById("tipoCuentaBancaria_c").value;
						fechadepo = document.getElementById("fechadepo_c").value;
						comprobacion = document.getElementById("comprobacion_c").value;
					} else if (tipo==2) {
						ddl_entidad = document.getElementById("medio_tarjeta_c").value;
						comprobacion = document.getElementById("comprobacion_tarjeta_c").value;
					} else if (tipo==3) {
						ddl_entidad = document.getElementById("ddl-entidad-cheque_c").value;
						comprobacion = document.getElementById("comprobacion_cheque_c").value;
					}
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/cancelarvariasfacturas') ?>",
                type:"post",
                data: { 'idCl': idCl, 'suma_saldo_total_can': suma_saldo_total_can, 'totlcanc': totlcanc, 'totlnc': totlnc,
								'tipo' : tipo, 'ddl_entidad' : ddl_entidad, 'tipoCuentaBancaria' : tipoCuentaBancaria, 'fechadepo' : fechadepo, 'comprobacion' : comprobacion	},
                success: function(data){
                    /*var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = '<!--?= //Url::toRoute("movimiento-credito/imprimir_facturas") ?-->&idRecibo='+data;
                    a.click();
                    location.reload();*/

                },
                error: function(msg, status,err){
                 //alert('No pasa 355');
                }
            });

        } else {

        }
    }

		//agrego nota de credito usando ajax
    function agrega_movimiento(tipo, idCa, btn_this){
      if(confirm("Está seguro de crear este movimiento")){
        var concepto = '';
      	//var monto = '';
        if (tipo=='nc') {
          concepto = document.getElementById('concepto_nc').value;
        	//monto = document.getElementById('monto_nc').value;
        } else {
          //concepto = document.getElementById('concepto_nd').value;
        	//monto = document.getElementById('monto_nd').value;
        }
      	if (concepto == ''/* || monto == ''*/) {
      		alert('Por favor no deje campos vacios');
      	} else {
          var $btn = $(btn_this).button('loading')
          // simulating a timeout
          setTimeout(function () {
              $btn.button('reset');
          }, 60000);
      	$.ajax({
                  url: "<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/agrega_movimiento') ?>",
                  type: "post",
                  dataType: 'json',
                  data: { 'tipo' : tipo, 'idCa' : idCa, 'concepto' : concepto/*, 'monto': monto*/ },
                  success: function(data_mov){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('facturas-dia/movimientos') ?>" ,
                    { 'id' : idCa },
                    function( data_movimiento ) { $('#movimientos_rev').html(data_movimiento); });
                    if (data_mov.email != '') {
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('facturas-dia/enviar_correo_factura_movimiento') ?>",
                      {'tipo' : tipo, 'id' : idCa, 'email' : data_mov.email , 'id_factun_mov' : data_mov.id_factun},
                        function( data ) {  });
                    }
                    //muestro un mensaje
                    document.getElementById("notificacion_movimiento").innerHTML = data_mov.notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notificacion_movimiento").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notificacion_movimiento").fadeOut();
												var url = "<?php echo Url::toRoute('movimiento-credito/index') ?>";
												window.location.href = url;
                    },2000);
                  },
                  error: function(msg, status,err){
                   alert('No pasa 87');
                  }
              });
      	}
      }
    }
</script>
<div class="movimiento-credito-cancelacion">
	<div class="col-lg-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				Facturas Pendientes (Seleccione la factura a la que desea hacerle un movimiento) <?= ' ' . Html::a('Refrescar', '', ['class' => 'btn btn-default btn-xs']) ?>
				<?= '<div class="pull-right" id="load_buton">' .Html::a('', '#', ['class' => 'fa fa-arrow-circle-o-down fa-2x', 'onclick' => 'obtener_facturas_marcadas()', 'data-toggle' => 'titulo', 'data-placement' => 'left', 'title' => Yii::t('app', 'Agregar facturas seleccionadas a "Facturas a cancelar"')]) . '</div>'; ?>
			</div>
			<div style="height: 350px;width: 100%; overflow-y: auto; ">
				<div class="panel-body">
					<div id="facturas-pendientes">
					<?php //Muestro las facturas pendientes del cliente seleccionado
					$contador_vencidas = 0;
					$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
					@$conditionfecha = CreditoSession::getFechafactura();
					if ($conditionfecha == 'hoy') {
						$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("estadoFactura = :estadoFactura AND fecha_vencimiento = :tipofecha", [':estadoFactura'=>'Crédito', ':tipofecha' => date('Y-m-d')])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
					}
					if ($conditionfecha == 'vencidas') {
						$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("estadoFactura = :estadoFactura AND fecha_vencimiento < :tipofecha", [':estadoFactura'=>'Crédito', ':tipofecha' => date('Y-m-d')])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
					}
					if ($conditionfecha == 'porvencer') {
						$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $idCliente])
						->andWhere("estadoFactura = :estadoFactura AND
							fecha_vencimiento = :tipofecha + INTERVAL 5 DAY OR
							fecha_vencimiento = :tipofecha + INTERVAL 4 DAY OR
							fecha_vencimiento = :tipofecha + INTERVAL 3 DAY OR
							fecha_vencimiento = :tipofecha + INTERVAL 2 DAY OR
							fecha_vencimiento = :tipofecha + INTERVAL 1 DAY", [':estadoFactura'=>'Crédito', ':tipofecha' => date('Y-m-d')])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
					}

					echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
	                echo '<thead>';
	                printf('<tr>
	                	<th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th class="actions button-column">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="checkAll" /></th></tr>',
	                        'FACTURA',
	                        'MONTO',
	                        'FECH.REG',
	                        'FECH.VEN',
	                        'SALDO'
	                        );
	                echo '</thead>';
	                echo '<tbody class="buscar">';
	                foreach($facturasCredito as $position => $factura) {
	                	$fecha_actual = date('d-m-Y');
	                	list ($dia_hoy, $mes_hoy, $anio_hoy) = explode("-", $fecha_actual);
	                	$fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
	                	$fecha_vencimiento = date('d-m-Y', strtotime( $factura['fecha_vencimiento'] ));
	                	list ($dia_ven, $mes_ven, $anio_ven) = explode("-", $fecha_vencimiento);
	                	$fecha_antes_vencimiento5 = date ( 'd-m-Y' , strtotime ( '-5 day' , strtotime ( $fecha_vencimiento ) ) );
	                	$fecha_antes_vencimiento4 = date ( 'd-m-Y' , strtotime ( '-4 day' , strtotime ( $fecha_vencimiento ) ) );
	                	$fecha_antes_vencimiento3 = date ( 'd-m-Y' , strtotime ( '-3 day' , strtotime ( $fecha_vencimiento ) ) );
	                	$fecha_antes_vencimiento2 = date ( 'd-m-Y' , strtotime ( '-2 day' , strtotime ( $fecha_vencimiento ) ) );
	                	$fecha_antes_vencimiento1 = date ( 'd-m-Y' , strtotime ( '-1 day' , strtotime ( $fecha_vencimiento ) ) );
	                	$mostrarFactura = Html::a('', ['#'], [
	                                'class'=>'glyphicon glyphicon-search',
	                                'id' => 'activity-factura',
	                                'data-toggle' => 'modal',
	                                'data-target' => '#modalFactura',
	                                'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
	                                'data-pjax' => '0',
	                                'title' => Yii::t('app', 'Procede a mostrar la factura')]);

	                	$stilo = '';
	                	$border = '';
	                	if(strtotime($fecha_vencimiento) < strtotime($fecha_actual)){
	                		$stilo = 'red';
	                		$border = 'red 10px solid;';
											$contador_vencidas += 1;
	                	}
	                	if($fecha_actual == $fecha_vencimiento){
	                		$stilo = 'orange';
	                		$border = 'orange 10px solid;';
	                	}
	                	if(($fecha_actual == $fecha_antes_vencimiento5) || ($fecha_actual == $fecha_antes_vencimiento4) || ($fecha_actual == $fecha_antes_vencimiento3) || ($fecha_actual == $fecha_antes_vencimiento2) || ($fecha_actual == $fecha_antes_vencimiento1)){
	                		$stilo = 'blue';
	                		$border = 'blue 10px solid;';
	                	}
										if ($factura['factun_id'] > 0) {
											$n_factura = $factura['idCabeza_Factura'] .' | '. $factura['fe'];
										} else {
											$n_factura = $factura['idCabeza_Factura'];
										}
	                	$idCabeza_Factura_activa = $factura['idCabeza_Factura'];
	                	$checkbox = '<input id="striped" type="checkbox" name="checkboxRow[]" value="'.$idCabeza_Factura_activa.'">';
	                	printf('<tr style = "cursor:pointer; border-left: '.$border.'; border-right: '.$border.';" onclick="marcarfactura(this, '.$factura['idCabeza_Factura'].',
	                		'.$factura['total_a_pagar'].','.$factura['credi_saldo'].')"><td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
	                		<td align="right"><font face="arial" size=2>%s</font></td><td align="center"><font face="arial" size=2>%s</font></td>
	                		<td align="center"><font face="arial" size=2>%s</font></td><td align="right"><font face="arial" size=2>%s</font></td><td class="actions button-column">%s</td></tr>',
															$n_factura,
	                            number_format($factura['total_a_pagar'],2),
	                            $fecha_inicio,
	                            $fecha_vencimiento,
	                            number_format($factura['credi_saldo'],2),
	                            $mostrarFactura . '&nbsp;' . $checkbox
	                            );

	                }
	                echo '</tbody>';
	                echo '</table>';
									if ($contador_vencidas == 0) {
										$cliente_credito = Clientes::findOne($idCliente);
										$cliente_credito->estadoCuenta = 'Abierta';
										$cliente_credito->save();
									}
					?>
					</div>
				</div>
			</div>
			<div class="panel-footer">
			<a class="btn btn-primary btn-xs" onclick="facturafecha('porvencer')" >Por vencer</a>
			<a class="btn btn-warning btn-xs" onclick="facturafecha('hoy')" >Vencidas hoy</a>
			<a class="btn btn-danger btn-xs" onclick="facturafecha('vencidas')" >+1 dia Vencidas</a>
			<a class="btn btn-default btn-xs" onclick="facturafecha('todas')" >Todas</a>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				Pago Parcial <?= '<font size=4> - Factura No.</font> '. Html::input('text', '', ' '.CreditoSession::getFactura(), ['class' => '', 'id' => 'numero_factura', 'size' => '10', 'placeholder' => ' Factura ', 'readonly' => true, 'onchange' => '']) ?>
				<!--?= //'<div class="pull-right">' .Html::a('', ['#'], ['class' => 'fa fa-print fa-2x', 'data-toggle' => 'titulo', 'data-placement' => 'left', 'title' => Yii::t('app', 'Imprimir')]) . '</div>'; ?-->
				<div class="pull-right" id="btn_movimiento_factura">

				</div>
			</div>
			<div style="height: 100%;width: 100%; overflow-y: auto; ">
			<div class="panel-body">
				<div class="col-lg-4">
					<?php //Este modulo permite al usuario crear un pago y mostrar los movimientos por pagos o notas de credito
					$saldo_de_la_factura = $saldo_de_la_factura_masc = $suma_monto_nc = 0.00;
					$model_m_cobrar = new MovimientoCobrar();
					if (@CreditoSession::getFactura()) {
						$suma_monto_movimiento = $model_m_cobrar->find()->where(['idCabeza_Factura'=>CreditoSession::getFactura()])->sum('monto_movimiento');
						$saldo_de_la_factura = CreditoSession::getMontofactura() - $suma_monto_movimiento;
						$saldo_de_la_factura_masc = number_format($saldo_de_la_factura,2);
						$suma_monto_nc = $model_m_cobrar->find()->where(['idCabeza_Factura'=>CreditoSession::getFactura()])->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])->sum('monto_movimiento');
					}
					echo '<label>Saldo de la factura</label>';
					echo '<div id="nuevo_saldo_div">';

					echo '<input type="hidden" name="saldo_de_la_factura" id="saldo_de_la_factura" value="'.$saldo_de_la_factura.'" >';
					echo Html::input('text', '', $saldo_de_la_factura_masc, ['class' => 'form-control', 'id' => 'saldo_de_la_factura_masc', 'placeholder' =>'Saldo de la factura', 'readonly' => true, 'onchange' => '']).'</div>';

					echo '<label>Notas de Crédito</label>';
					echo '<div id="refr_nc_div">';

					echo Html::input('text', '', $suma_monto_nc, ['id' =>'suma_monto_nc', 'class' => 'form-control', 'placeholder' =>'Notas de Crédito', 'readonly' => true, 'onchange' => '']).'</div>';

					echo '<label>Pago Parcial</label>';
					echo Html::input('text', '', '', ['class' => 'form-control', 'placeholder' =>'Pago Parcial', 'readonly' => false, 'id' => 'pagoparcial', 'onkeyup'=>'javascript:obtenerPagoparcial()', "onkeypress"=>"return isNumberDe(event)"]);
					echo '<label>Nuevo Saldo</label>';
					echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'nuevo_saldo', 'placeholder' =>'Nuevo Saldo', 'readonly' => true, 'onchange' => '']);
					echo '<label>Referencia</label>';
					echo Html::input('text', '', 'ABONO A FACTURA', ['class' => 'form-control', 'placeholder' =>'Referencia', 'readonly' => false, 'id' => 'referencia']);
					?>
				</div>
				<div class="col-lg-8" id="botones_abono" >
					<div class="btn-group btn-group-justified">
						<a onclick="agregar_abono(1)" title="Click para completar el abono en efectivo" class="btn btn-primary">Efectivo</a>
					  <a href="#deposito" title="Click para crear un abono mediante un depósito bancario" data-toggle="tab" class="btn btn-default">Depósito</a>
					  <a href="#tarjeta" title="Click para crear un abono pagando con tarjeta" data-toggle="tab" class="btn btn-default">Tarjeta</a>
						<a href="#cheque" title="Click para crear un abono pagando con cheque" data-toggle="tab" class="btn btn-default">Cheque</a>
						<a href="#lista" title="Click para listar los movimientos de la factura seleccionada" data-toggle="tab" class="btn">Listar <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>
					</div>
					<div class="well table-responsive tab-content">

		        <div class="tab-pane fade" id="deposito">
							<p>
								 <center><label>Abono por depósito bancario</label><br>(Con movimiento en libro automatizado)<hr style="background-color: black; height: 1px; border: 0;"></center>
								 <div class='col-xs-12'><?= $medio ?></div>
								 <div class='col-xs-7'><?= $cuenta ?></div>
								 <div class='col-xs-5'><?= $fechadepo ?></div>
								 <div class='col-xs-12'><?= $comprobacion ?></div>
								 <div class='col-xs-12'>
								 	<div id='nota_cancelacion'></div><br>
									<div class="pull-right">
									 <?= Html::a('Abonar con Depósito', '#', ['id'=>'boton-abonar-deposito','class' => 'btn btn-primary', 'onclick'=>'agregar_abono(4)']) ?>
									</div>
								</div>

							</p>

						</div>
						<div class="tab-pane fade" id="tarjeta">
							<p>
								<center><label>Abono por tarjeta</label><hr style="background-color: black; height: 1px; border: 0;"></center>

								<div class='col-xs-12'><?= $medio_tarjeta ?></div>
								<div class='col-xs-12'><?= $comprobacion_tarjeta ?></div>
								<div class='col-xs-12'>
								 <div id='nota_cancelacion_2'></div><br>
								 <div class="pull-right">
									<?= Html::a('Abonar con Tarjeta', '#', ['id'=>'boton-abonar-tarjeta','class' => 'btn btn-primary', 'onclick'=>'agregar_abono(2)']) ?>
								 </div>
							 </div>
							 <center><img src="../images/tarjetas.png" height="70" width=""><br>
								 <!--i class="fa fa-cc-visa fa-3x" aria-hidden="true"></i>
								 <i class="fa fa-cc-mastercard fa-3x" aria-hidden="true"></i>
								 <i class="fa fa-cc-amex fa-3x" aria-hidden="true"></i>
								 <i class="fa fa-cc-discover fa-3x" aria-hidden="true"></i-->
							 </center>
							</p>
						</div>
						<div class="tab-pane fade" id="cheque">
							<p>
								<center><label>Abono por cheque</label><hr style="background-color: black; height: 1px; border: 0;"></center>
								<div class='col-xs-12'><?= $medio_cheque ?></div>
								<div class='col-xs-12'><?= $comprobacion_cheque ?></div>
								<div class='col-xs-12'>
								 <div id='nota_cancelacion_3'></div><br>
								 <div class="pull-right">
									<?= Html::a('Abonar con Cheque', '#', ['id'=>'boton-abonar-cheque','class' => 'btn btn-primary', 'onclick'=>'agregar_abono(3)']) ?>
								 </div>
							 </div>
							</p>
						</div>
						<div class="tab-pane fade in active" id="lista">
							<p>
								<center><label>Movimientos (AB: Abono / NC: Nota de crédito)</label></center>
								<div style="height: 200px;width: 100%; overflow-y: auto; ">
									<div id="movi_credit">
										<?php
										if (@$idCabeza_Factura = CreditoSession::getFactura()) {
			                $movimientofactura = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$idCabeza_Factura])->orderBy(['idMov' => SORT_DESC])->all();
			                $lista_movi = '<table class="items table table-striped">';
			                if($movimientofactura) {
			                $lista_movi .= '<tbody>';
			                  foreach($movimientofactura as $position => $movi) {
			                    $consultar = Html::a('', ['', 'id' => $position], [
			                                            'class'=>'glyphicon glyphicon-search',
			                                            'id' => 'activity-index-link-agregacredpen',
			                                            'data-toggle' => 'modal',
			                                            'onclick'=>'javascript:consulta_modal_movi("'.$movi['idMov'].'")',
			                                            'data-target' => '#modalconsultamovi',
			                                            //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
			                                            'data-pjax' => '0',
			                                            'title' => Yii::t('app', 'Mostrar movimiento de crédito') ]);
			                    $imprimir = Html::a('<span class=""></span>', ['movimiento-credito/index', 'id'=>$movi['idMov']], [
			                                            'class'=>'fa fa-print',
			                                            'id' => 'activity-index-link-report',
			                                            //'target'=>'_blank',
			                                            'data-toggle' => 'titulo',
			                                            'onclick' => 'jsWebClientPrint.print()',
			                                            'data-pjax' => '0',
			                                            'title' => Yii::t('app', 'Imprimir movimiento'),
			                                            ]);
			                    $accion_mov = $consultar . ' ' . $imprimir;

			                  $lista_movi .= '<tr>
			                                    <td><font face="arial" size=2>'.$movi['idMov'].'</font></td>
			                                    <td><font face="arial" size=1>'.$movi['fecmov'].'</font></td>
			                                    <td><font face="arial" size=2>'.$movi['tipmov'].'</font></td>
			                                    <td align="right"><font face="arial" size=2>'.number_format($movi['monto_movimiento'],2).'</font></td>
			                                    <td class="actions button-column">'.$accion_mov.'</td></tr>';
			                  }
			                $lista_movi .= '</tbody>';
			                }
			                $lista_movi .= '</table>';
											echo $lista_movi;
			              }
										?>
									</div>
								</div>
							</p>
						</div>
					</div><br>
				</div>


			</div>
			</div>
			<div class="panel-footer">

					<?= 'Usuario: '.Html::input('text', '', ' '.Yii::$app->user->identity->username , ['class' => '', 'id' => 'usuario_reg', 'value' => Yii::$app->user->identity->username, 'size' => '20', 'placeholder' =>' Usuario', 'readonly' => true, 'onchange' => '']) ?>

					<?= '&nbsp;&nbsp;&nbsp;&nbsp;Fecha: '.Html::input('text', '', ' '.date('d-m-Y'), ['class' => '', 'id' => 'fecha_reg', 'value' => date('Y-m-d'), 'size' => '10', 'placeholder' =>' Fecha', 'readonly' => true, 'onchange' => '']) ?>

					<?php
					if ($empresa->factun_conect!=1)
						echo '<div class="pull-right" >' .Html::a('', ['movimiento-credito/aplicarnotasdecredito', 'id'=>$idCliente ], ['class' => 'fa fa-plus-circle fa-2x', 'id' => 'load_buton_nc', 'data-loading-text'=>' Espere...', 'onclick'=>'', 'data-toggle' => 'titulo', 'data-placement' => 'top',
						'title' => Yii::t('app', 'Aplicar notas de credito a factura, tenga muy presente que NO APLICA PARA FACTURA ELECTRÓNICA'), 'data' => [
                                    'confirm' => '¿Está seguro de aplicar las notas de crédito?']]) . '</div>';

																		?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				Facturas a Cancelar
				<?= '<div class="pull-right" id="load_buton2">' .Html::a('', '', ['class' => 'fa fa-arrow-circle-o-up fa-2x', 'data-toggle' => 'titulo', 'onclick' => 'obtener_facturas_marcadas_regreso()', 'data-placement' => 'left', 'title' => Yii::t('app', 'Devolver facturas seleccionadas a "Facturas Pendientes"')]) . '</div>'; ?>
			</div>
			<div style="height: 265px;width: 100%; overflow-y: auto; ">
				<div class="panel-body">
					<?php
					$facturasporatender = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("credi_atencion = :credi_atencion", [":credi_atencion"=>'1'])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
					echo '<table class="items table table-striped">';
	                echo '<thead>';
	                printf('<tr><th>%s</th><th>%s</th><th>%s</th><th class="actions button-column">&nbsp;</th></tr>',
	                        'FACTURA',
	                        'MONTO',
	                        'FECHA REG'
	                        );
	                echo '</thead>';
	                echo '<tbody>';
	                	foreach($facturasporatender as $position => $factura) {
						$fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
						$checkboxinverso = '<input id="stripedin" type="checkbox" name="checkboxRowinv[]" value="'.$factura['idCabeza_Factura'].'">';
							printf('<tr style = "cursor:pointer;" onclick=""><td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
							                		<td align="right"><font face="arial" size=2>%s</font></td><td align="center"><font face="arial" size=2>%s</font></td>
							                		<td class="actions button-column">%s</td></tr>',

	                            $factura['idCabeza_Factura'],
	                            number_format($factura['total_a_pagar'],2),
	                            $fecha_inicio,
	                            $checkboxinverso
	                            );
						}
	                echo '</tbody>';
	                echo '</table>';
					?>


				</div>
			</div>
			<div class="panel-footer">
				<?php
					$facturas_por_atender = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("credi_atencion = :credi_atencion", [":credi_atencion"=>'1'])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
					$longitud = count($facturas_por_atender);
					$suma_monto_nc = 0;
					$suma_monto_ab = 0;
					foreach ($facturas_por_atender as $key => $value) {
						$m_cobrar = new MovimientoCobrar();
						$suma_monto_ab += $m_cobrar->find()->where(['idCabeza_Factura'=>$value['idCabeza_Factura']])->andWhere("tipmov = :tipmov", [":tipmov"=>'AB'])->sum('monto_movimiento');
						$suma_monto_nc += $m_cobrar->find()->where(['idCabeza_Factura'=>$value['idCabeza_Factura']])->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])->sum('monto_movimiento');
					}

				 ?>
				<?= 'NC Ejecut: '.Html::input('text', '', ' '.number_format($suma_monto_nc,2) ,
				['class' => '', 'id' => 'usuario_reg', 'size' => '8', 'placeholder' =>' Monto NC', 'readonly' => true, 'onchange' => '']).
				' ' . 'AB Ejecut: '.Html::input('text', '', ' '.number_format($suma_monto_ab,2) ,
				['class' => '', 'id' => 'usuario_reg', 'size' => '9', 'placeholder' =>' Monto NC', 'readonly' => true, 'onchange' => '']) ?>
			</div>
		</div>
	</div>
	<div class="well col-lg-8">
		<div class="col-lg-6">
			<div class="panel panel-info">
				<div class="panel-heading">
					Cancelar facturas
				</div>
				<div style="height: 100%;width: 100%; overflow-y: auto; ">
					<div class="panel-body">
						<?php
							$suma_monto_total_fact_can = MovimientoCredito::find()->where(['idCliente'=>$idCliente])->andWhere("credi_atencion = :credi_atencion", [":credi_atencion"=>'1'])->sum('total_a_pagar');
						 	$suma_saldo_total_fact_can = $suma_monto_total_fact_can - ($suma_monto_nc + $suma_monto_ab);
						 	$suma_nc_total_fact_can = CreditoSession::getMontoNC() ? CreditoSession::getMontoNC() : 0;
						 	$total_a_cancelar = $suma_saldo_total_fact_can - $suma_nc_total_fact_can;
						 ?>
						<div class="input-group input-group-sm">
						  <span class="input-group-addon" id="sizing-addon3">Total Facturas</span>
						  <input type="text" class="form-control" value=<?= number_format($suma_saldo_total_fact_can,2) ?> readonly="readonly" placeholder="" aria-describedby="sizing-addon3" id="total_facturas">
						</div><br>
						<div class="input-group input-group-sm">
						  <span class="input-group-addon" id="sizing-addon3">Notas de Crédito</span>
						  <input type="text" class="form-control" value=<?= number_format($suma_nc_total_fact_can,2) ?> readonly="readonly" placeholder=" 0" aria-describedby="sizing-addon3">
						</div><br>
						<div class="input-group input-group-sm">
						  <span class="input-group-addon" id="sizing-addon3">Total a Cancelar</span>
						  <input type="text" class="form-control" value=<?= number_format($total_a_cancelar,2) ?> readonly="readonly" placeholder=" 0" aria-describedby="sizing-addon3">
						</div><br>
						<div class="input-group input-group-sm">
						  <span class="input-group-addon" id="sizing-addon3">Referencia</span>
						  <input type="text" class="form-control" placeholder="" value = "<?= CreditoSession::getReferencia() ?>" id="referenciacance" aria-describedby="sizing-addon3" onkeyup='obtenerReferenciacanelacion()'>
						</div><br>
						<?php
						if ($empresa->factun_conect!=1) {
							echo '<div class="pull-right">'
								. Html::a('<span class="fa fa-briefcase"></span> <span class="fa fa-angle-double-left"></span> NC',
									['movimiento-credito/aplicarnotasdecredito_fact_grupal', 'idCl'=>$idCliente, 'totlcanc'=>$total_a_cancelar],
									['class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'titulo',
									'data-placement' => 'top', 'title' => Yii::t('app', 'Agregar Nota de Crédito a pago grupal, tenga muy presente que NO APLICA PARA FACTURA ELECTRÓNICA')/*, 'data' => [
		                                    'confirm' => '¿Está seguro de cargar las notas de crédito?']*/]) . ' '
								. Html::a('<span class="fa fa-briefcase"></span> <span class="fa fa-angle-double-right"></span> NC',
									['movimiento-credito/desaplicarnotasdecredito_fact_grupal', 'idCl'=>$idCliente, 'sum_nc'=>$suma_nc_total_fact_can],
									['class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'titulo',
									'data-placement' => 'top', 'title' => Yii::t('app', 'Devolver Notas cargadas para editarlas')])
								.'<br><br></div>';
						}
						 ?>
						 <?= Html::a('<span class="fa fa-check"></span> Cancelar con efectivo', '#'
 							/*['movimiento-credito/cancelarvariasfacturas', 'idCl'=>$idCliente, 'suma_saldo_total_can'=>$suma_saldo_total_fact_can, 'totlcanc'=>$total_a_cancelar, 'totlnc'=>$suma_nc_total_fact_can]*/,
 							['class' => 'btn btn-success btn-lg dropdown-toggle', //'target'=>'_blank',
                             'id' => 'cancelarButton',
                             'onclick'=>'javascript:cancelarvariasfacturas(1, '.$idCliente.','.$suma_saldo_total_fact_can.','.$total_a_cancelar.','.$suma_nc_total_fact_can.')',
                             'data-loading-text'=>'Espere...', 'data-toggle' => 'titulo',
 							'data-placement' => 'top', 'autocomplete'=>'off', 'title' => Yii::t('app', 'Cancela todas las facturas listadas en Facturas a Cancelar')/*, 'data' => [
                                     'confirm' => '¿Está seguro de cancelar las facturas?']*/]) ?><br><br>
						<div class="btn-group btn-group-justified">
							<a href="#cancel_deposito" title="Click para crear una cancelación mediante un depósito bancario" data-toggle="tab" class="btn btn-default">Depósito</a>
							<a href="#cancel_tarjeta" title="Click para crear una cancelación pagando con tarjeta" data-toggle="tab" class="btn btn-default">Tarjeta</a>
							<a href="#cancel_cheque" title="Click para crear una cancelación pagando con cheque" data-toggle="tab" class="btn btn-default">Cheque</a>
						</div>
<?php
//PARA CANCELCIÓN POR DEPOSITO --------------------------------------------------------------------------------------------

$medio_c = Select2::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data,
						'options' => ['id'=>'ddl-entidad_c', 'onchange'=>'obtenMedio(this.value,6)','placeholder' => 'Seleccione entidad financiera...'],
						'pluginOptions' => [
								'initialize'=> true,
								'allowClear' => true
						],
				]).'<br>';
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$cuenta_c = DepDrop::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data2,
						'type' => DepDrop::TYPE_SELECT2,
						'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
						'options' => ['id'=>'tipoCuentaBancaria_c', 'onchange'=>'obtenCuenta(this.value,6)','placeholder' => 'Seleccione...'],
						'pluginOptions' => [
								'depends'=>['ddl-entidad_c'],
								'initialize'=> true, //esto inicializa instantaniamente junto a entidad financiera
								'placeholder' => 'Seleccione cuenta...',
								'url' => Url::to(['cabeza-prefactura/cuentas']),
								'loadingText' => 'Cargando...',
								//'allowClear' => true
						],
				]);
$fechadepo_c = DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName','value'=>date("d-m-Y"), 'options' => ['class'=>'form-control', 'readonly' => true, 'placeholder'=>'Fecha depósito', 'id'=>'fechadepo_c', 'onchange'=>'obtenFecha(this.value)']]);
$comprobacion_c = '<br>'.Html::input('text', '', '', ['size' => '20', 'onkeypress'=>'return isNumberDe(event)', 'id' => 'comprobacion_c', 'disabled'=>false, 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value,6);']);
//--------------FIN CANCELCIÓN POR DEPOSITO

//--------------PARA CANCELCIÓN CON TARJETA
$medio_tarjeta_c = Html::input('text', '', '', ['size' => '19', 'id' => 'medio_tarjeta_c', 'class' => 'form-control', 'placeholder' =>' Medio ( MasterCard, VISA, otra )', 'onkeyup' => 'obtenMedio(this.value,7);']);
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$comprobacion_tarjeta_c = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_tarjeta_c', 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value,7);']);
//-------------FIN CANCELCIÓN POR TARJETA

//--------------PARA CANCELCIÓN POR CHEQUE
$medio_cheque_c = Select2::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data,
						'options' => ['id'=>'ddl-entidad-cheque_c', 'onchange'=>'obtenMedio(this.value,8)','placeholder' => 'Seleccione entidad financiera...'],
						'pluginOptions' => [
								'allowClear' => true
						],
				]);
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$comprobacion_cheque_c = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_cheque_c', 'class' => 'form-control', 'placeholder' =>' # Cheque', 'onkeyup' => 'obtenCom(this.value,8);']);
//--------------FIN CANCELACIÓN POR CHEQUE
?>
						<div class="well table-responsive tab-content">
			        <div class="tab-pane fade" id="cancel_deposito">
								<p>
									 <center><label>Cancelación por depósito bancario</label><br>(Con movimiento en libro automatizado)<hr style="background-color: black; height: 1px; border: 0;"></center>
									 <div class='col-xs-12'><?= $medio_c ?></div>
									 <div class='col-xs-7'><?= $cuenta_c ?></div>
									 <div class='col-xs-5'><?= $fechadepo_c ?></div>
									 <div class='col-xs-12'><?= $comprobacion_c ?></div>
									 <div class='col-xs-12'>
									 	<div id='nota_cancelacion_c'></div><br>
										<div class="pull-right">
										 <?= Html::a('Cancelar con Depósito', '#', ['id'=>'boton-abonar-deposito_c','class' => 'btn btn-primary',
										 'onclick'=>'cancelarvariasfacturas(4, '.$idCliente.','.$suma_saldo_total_fact_can.','.$total_a_cancelar.','.$suma_nc_total_fact_can.')']) ?>
										</div>
									</div>
								</p>
							</div>
							<div class="tab-pane fade" id="cancel_tarjeta">
								<p>
									<center><label>Cancelación por tarjeta</label><hr style="background-color: black; height: 1px; border: 0;"></center>
									<div class='col-xs-12'><?= $medio_tarjeta_c ?></div>
									<div class='col-xs-12'><?= $comprobacion_tarjeta_c ?></div>
									<div class='col-xs-12'>
									 <div id='nota_cancelacion_2_c'></div><br>
									 <div class="pull-right">
										<?= Html::a('Cancelar con Tarjeta', '#', ['id'=>'boton-abonar-tarjeta_c','class' => 'btn btn-primary',
										'onclick'=>'cancelarvariasfacturas(2, '.$idCliente.','.$suma_saldo_total_fact_can.','.$total_a_cancelar.','.$suma_nc_total_fact_can.')']) ?>
									 </div>
								 </div>
								 <center><img src="../images/tarjetas.png" height="70" width=""><br>
								 </center>
								</p>
							</div>
							<div class="tab-pane fade" id="cancel_cheque">
								<p>
									<center><label>Cancelación por cheque</label><hr style="background-color: black; height: 1px; border: 0;"></center>
									<div class='col-xs-12'><?= $medio_cheque_c ?></div>
									<div class='col-xs-12'><?= $comprobacion_cheque_c ?></div>
									<div class='col-xs-12'>
									 <div id='nota_cancelacion_3_c'></div><br>
									 <div class="pull-right">
										<?= Html::a('Cancelar con Cheque', '#', ['id'=>'boton-abonar-cheque_c','class' => 'btn btn-primary',
										'onclick'=>'cancelarvariasfacturas(3, '.$idCliente.','.$suma_saldo_total_fact_can.','.$total_a_cancelar.','.$suma_nc_total_fact_can.')']) ?>
									 </div>
								 </div>
								</p>
							</div>
						</div>



					</div>
				</div>

			</div>
		</div>
		<div class="col-lg-6">
			<div class="panel panel-info">
				<div class="panel-heading">
					Notas de Crédito Pendientes <br>(SOLO PARA REGIMEN SIMPLIFICADO - 25514-H)
					<?= '<div class="pull-right">' .Html::a('', '#', ['class' => 'fa fa-folder-o fa-2x', 'data-toggle' => 'modal', 'data-target' => '#modalnuevanotacred', 'data-placement' => 'left', 'title' => Yii::t('app', 'Crear nueva Nota de Crédito')]) . '</div>'; ?>
				</div>
				<div style="height: 280px;width: 100%; overflow-y: auto; ">
					<div class="panel-body">
					<div id="not_cr_pen">
					<?php
					    //$notascr_pen = CreditoSession::getNotasCreditoPendientes();
                    $notascr_pen = NcPendientes::find()->where(['idCliente'=>$idCliente])->orderBy(['idNCPendiente' => SORT_DESC])->all();
					    echo '<table class="items table table-striped">';
						if($notascr_pen) {
						echo '<tbody>';
							foreach($notascr_pen as $position => $notascr) {
								$eliminar = Html::a('', ['/movimiento-credito/deleteitem_cr_pen', 'id' => $notascr['idNCPendiente']], ['class'=>'glyphicon glyphicon-remove', 'data-toggle' => 'titulo', 'data' => [
                                    'confirm' => '¿Está seguro de quitar esta nota de crédito?'], 'title' => Yii::t('app', 'Eliminar nota de crédito') ]);
				                $actualizar = Html::a('', ['', 'id' => $notascr['idNCPendiente']], [
				                                'class'=>'glyphicon glyphicon-pencil',
				                                'id' => 'activity-index-link-agregacredpen',
				                                'data-toggle' => 'modal',
				                                'onclick'=>'javascript:actualiza_modal_credpen("'.$notascr['idNCPendiente'].'")',
				                                'data-target' => '#modalActualizacredpen',
				                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
				                                'data-pjax' => '0',
				                                'title' => Yii::t('app', 'Actualizar nota de crédito') ]);
				                $accion = $actualizar.' '.$eliminar;
								printf('<tr><td><font face="arial" size=2>%s</font></td><td align="right"><font face="arial" size=2>%s</font></td><td class="actions button-column">%s</td></tr>',
										$notascr['fecha'],
										number_format($notascr['monto'],2),
										$accion
									);
							}
						echo '</tbody>';
						}
						echo '</table>';
					?>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php
	//-------------------------------------------------Pantallas modales-----------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------
	//Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalFactura',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Factura</h4></center>',
                'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='well'><div id='mostrar_factura_modal'></div></div>";

            Modal::end();
    ?>
    <?php
	//Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevanotacred',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Nueva Nota de Crédito</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '', [
                    'class'=>'btn btn-success',
                    'onclick'=>'agrega_nota_credito()',
                    //'target'=>'_blank',
                    //'data-toggle'=>'tooltip',
                    'data-dismiss'=>'modal',
                    'title'=>'Agrega una nueva nota pendiente'
                ]),
            ]);

            $notascr_pen = NcPendientes::find()->where(['idCliente'=>$idCliente])->orderBy(['idNCPendiente' => SORT_DESC])->all();
         	$monto = 0;
         	if ($notascr_pen) {
         		foreach($notascr_pen as $not_pen) {
         		$monto += $not_pen['monto'];
         	}
         	}

         	echo 'Monto total en NC Pendientes: ₡'.number_format($monto,2);
            echo '<div class="well">';
            echo '<label>Cliente:</label><br>';
            echo Html::input('text', '', ' '.CreditoSession::getIDcliente() , ['size' => '3', 'class' => '', 'id' => 'idCliente', 'placeholder' =>' ID Cliente', 'readonly' => true, 'onchange' => '']).
            ' '.Html::input('text', '', ' '.CreditoSession::getCliente() , ['class' => '', 'id' => 'Cliente', 'placeholder' =>' Cliente', 'readonly' => true, 'onchange' => '']);
            echo '<br><br><label>Concepto:</label><br>';
            echo '<textarea class="form-control" rows="5" id="concepto" required></textarea>';
            echo '<br><label>Monto: </label>';
            echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'monto_nc', 'placeholder' =>' Monto', 'readonly' => false, 'onchange' => '', "onkeypress"=>"return isNumberDe(event)"]);
            echo '</div>';

            Modal::end();
    ?>
    <?php
    //muestra modal para actualizar nota de credito pendiente
        Modal::begin([
                'id' => 'modalActualizacredpen',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Detalle de Entrada</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Actualizar', '', [
                    'class'=>'btn btn-success',
                    'onclick'=>'actualizar_nota_credito()',
                    //'target'=>'_blank',
                    //'data-toggle'=>'tooltip',
                    'data-dismiss'=>'modal',
                    'title'=>'Actualizar nota de credito'
                ]),
            ]);
        echo '<div class="well">';
        echo '<div id="modal_update_notasc_pen"></div>';
        echo '</div>';

        Modal::end();
    ?>
    <?php
	//Muestra una modal del movimiento seleccionado
         Modal::begin([
                'id' => 'modalconsultamovi',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Movimiento</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='well'><div id='mostrar_movimiento_modal'></div></div>";

            Modal::end();
    ?>
		<?php
         Modal::begin([//modal que me muestra la factura seleccionada
                'id' => 'modalNotacredito',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Movimientos (ND: Notas de débito / NC: Nota de crédito)</h4></center>',
                'footer' => '<div id="notificacion_movimiento"></div><a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
            ]);
            echo "
            <div id='movimientos_rev'></div>
            ";
            Modal::end();
    ?>
</div>
