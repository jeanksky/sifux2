<?php

use yii\helpers\Html;
use backend\models\MovimientoCredito;
use backend\models\MovimientoCobrar;
use backend\models\Clientes;
use backend\models\NcPendientes;

$css_checkbox = \Yii::$app->request->BaseUrl.'/jquery.tzCheckbox/jquery.tzCheckbox.css';
$jquery_checkbox = \Yii::$app->request->BaseUrl.'/jquery.tzCheckbox/jquery.tzCheckbox.js';
$script = \Yii::$app->request->BaseUrl.'/js/script.js';
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $css_checkbox; ?>" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="<?php echo $jquery_checkbox; ?>"></script>
<script src="<?php echo $script; ?>"></script>
<script type="text/javascript">
function notificar_estado_cuenta(){
	var notif = document.getElementById("ch_emails").checked;
	var idCliente = "<?php echo $idCliente; ?>";
	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/notificar_estado_cuenta') ?>",
                type:"post",
                data: { 'notif' : notif, 'idCliente' : idCliente },
                beforeSend: function() {
	            	//$('#recibo_detalle_div').html('<center><img src="http://www.reforma.com/libre/acceso/imgdiseno/loading3.gif" style="height: 200px;"></center>');
	            },
                success: function(data){
                    //$("#resultadoBusqueda").html(data);

                },
                error: function(msg, status,err){
                 alert('No pasa 43');
                }
            });
}
//muestro una animacion de cargando mientras se hace el proceso
function cargar_load_buton(){
	$('#load_buton_estado_cuenta').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
}
</script>

<div class="movimiento-credito-estado_cuenta">
	<div class="col-lg-12">
		<div class="well">
			<?php
			echo '<table class="items table table-striped" width="100%">';
	        echo '<thead>';
	        printf('<tr>
										<th align="left" width="400">%s</th>
										<th align="left">%s</th>
										<th align="center" width="150">%s</th>
										<th align="center" width="340">%s</th>
										<th align="right" width="150">%s</th>
										<th align="right" width="160">%s</th>
										<th align="right">%s</th>
										<th align="right">%s</th>
									</tr>',
			                'DOCUMENTO',
			                'TIPO',
			                'FECHA REGISTRO',
			                'FECHA VENCIMIENTO',
			                'MONTO (+)',
			                'MONTO (-)',
			                'SALDO',
			                'DÍAS VENCIDOS'
	                );
	        echo '</thead>';
	        echo '</table>';
			?>
			<div style="height: 400px;width: 100%; overflow-y: auto; ">
			<?php
				$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])->orderBy(['idCabeza_Factura' => SORT_ASC])->all();//SORT_ASC asendente SORT_DESC desendente
				echo '<table class="items table table-striped" id="tabla_estados_cuenta">';
	        	echo '<tbody>';
	        	$monto_fact = $monto_nc_ab = $saldo_fact = 0;
	        	$vencido_uno = $vencido_dos = $vencido_tres = $vencido_cuatro = 0;
	        	foreach($facturasCredito as $position => $factura) {
	        		$fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
	        		$fecha_vencimiento = date('d-m-Y', strtotime( $factura['fecha_vencimiento'] ));

	        		$montomenos = 0;
	        		$diasvencidas = 0;

	        		//condicion para obtener los días de vencimiento de la factura recorrida
	        		if (strtotime($factura['fecha_vencimiento']) < strtotime(date('Y-m-d'))) {
	        			$mv = new MovimientoCredito();
	        			$diasvencidas = $mv->getMovimiento_de_dias($factura['fecha_vencimiento'], date('Y-m-d'));

	        			$vencido_uno += $diasvencidas <= 30 ? $factura['credi_saldo'] : 0;
	        			$vencido_dos += ($diasvencidas <= 60) && ($diasvencidas > 30) ? $factura['credi_saldo'] : 0;
	        			$vencido_tres += ($diasvencidas <= 90) && ($diasvencidas > 60) ? $factura['credi_saldo'] : 0;
	        			$vencido_cuatro += $diasvencidas > 90 ? $factura['credi_saldo'] : 0;
	        		}

	        		$movimientofactura = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$factura['idCabeza_Factura']])->orderBy(['idMov' => SORT_DESC])->all();
	        		foreach($movimientofactura as $movimiento) {
	        			$montomenos += $movimiento['monto_movimiento'];
	        		}
	        		$monto_fact += $factura['total_a_pagar'];//Suma el monto de todas las factuas pendientes
	        		$monto_nc_ab += $montomenos;//Suma el monto de las notas de Credito y abonos
	        		$saldo_fact += $factura['credi_saldo'];//Suma del saldo pendiente de todas las facturas
	        		 printf('<tr style="color:#231BB8">
	        		 				<td align="left" ><font face="arial" size=4 >%s</font></td>
	                		<td align="left"><font size=4>%s</font></td>
	                		<td align="center"><font size=4>%s</font></td>
											<td align="left"><font size=4>%s</font></td>
	                		<td align="right"><font size=4>%s</font></td>
	                		<td align="right"><font size=4>%s</font></td>
	                		<td align="right"><font size=4>%s</font></td>
	                		<td align="center"><font size=4>%s</font></td></tr>',

	                            $factura['idCabeza_Factura'] .' | '. $factura['fe'],
	                            'FA',
	                            $fecha_inicio,
	                            'Vence '.$fecha_vencimiento,
	                            number_format($factura['total_a_pagar'],2),
	                            number_format($montomenos,2),
	                            number_format($factura['credi_saldo'],2),

	                            $diasvencidas
	                            );

	        		foreach($movimientofactura as $position => $movimiento) {

	        			$aplicada = 'Aplica FA - '.$movimiento['idCabeza_Factura'];
	        			printf('<tr>
													<td align="center" style="background: ;"><font face="arial" size=3>%s</font></td>
		                			<td align="left" style="background: ;"><font face="arial" size=3>%s</font></td>
													<td align="center"><font face="arial" size=3>%s</font></td>
													<td align="left"><font face="arial" size=3>%s</font></td>
		                			<td align="right"><font face="arial" size=3>%s</font></td>
													<td align="right"><font face="arial" size=3>%s</font></td>
													<td align="right">%s</td>
													<td align="center">%s</td>
												</tr>',

	                            $movimiento['idMov'],
	                            $movimiento['tipmov'],
	                            date('d-m-Y', strtotime( $movimiento['fecmov'] )),
	                            $aplicada,
	                            '',
	                            number_format($movimiento['monto_movimiento'],2),
	                            '',
	                            ''
	                            );
	        		}

	        	}
	        	echo '</tbody>';
	        	echo '</table>';
	        	$no_vencido = $saldo_fact - ($vencido_uno+$vencido_dos+$vencido_tres+$vencido_cuatro);
	        ?>
			</div>
		</div>

		<div class="col-lg-4">
		    <div class="input-group">
          <span class="input-group-addon">
            Monto por facturas
          </span>
          <?php
			      echo Html::input('text', '', number_format($monto_fact,2), ['class' => 'form-control', 'placeholder' =>'', 'readonly' => true, 'onchange' => '']);
			    ?>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="input-group">
                <span class="input-group-addon">
                    NC y AB aplicados
                </span>
                <?php
			        echo Html::input('text', '', number_format($monto_nc_ab,2), ['class' => 'form-control', 'placeholder' =>'0.00', 'readonly' => true, 'onchange' => '']);
			    ?>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="input-group">
                <span class="input-group-addon">
                    Saldo pendiente
                </span>
                <?php
			        echo Html::input('text', '', number_format($saldo_fact,2), ['class' => 'form-control', 'placeholder' =>'0.00', 'readonly' => true, 'onchange' => '']);
			    ?>
			</div>
		</div>
		<div class="col-lg-4"><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					Detalle de vencimientos
					<div class="pull-right"> <?= Html::a('Refrescar', '', ['class' => 'btn btn-default btn-xs']) ?> </div>
				</div>
				<div style="height: 180px;width: 100%; overflow-y: auto; ">
					<div class="panel-body">
						<p><strong>No vencido:</strong> <?= number_format($no_vencido,2) ?></p>
						<p><strong>De 1 a 30 días:</strong> <?= number_format($vencido_uno,2) ?></p>
						<p><strong>De 31 a 60 días:</strong> <?= number_format($vencido_dos,2) ?></p>
						<p><strong>De 61 a 90 días:</strong> <?= number_format($vencido_tres,2) ?></p>
						<p><strong>Mas de 90 días:</strong> <?= number_format($vencido_cuatro,2) ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4"><br>
			<div class="input-group">
                <span class="input-group-addon">
                    NC Pendientes
                </span>
                <?php
                	$notascr_pen = NcPendientes::find()->where(['idCliente'=>$idCliente])->orderBy(['idNCPendiente' => SORT_DESC])->all();
		         	$monto_nc_pen = 0;
		         	if ($notascr_pen) {
		         		foreach($notascr_pen as $not_pen) {
			         		$monto_nc_pen += $not_pen['monto'];
			         	}
		         	}
			        echo Html::input('text', '', number_format($monto_nc_pen,2), ['class' => 'form-control', 'placeholder' =>'0.00', 'readonly' => true, 'onchange' => '']);
			    ?>
			</div>
		</div>
		<div class="col-lg-4"><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					Notificaciones al cliente
					<div class="pull-right" id="load_buton_estado_cuenta"> <?= Html::a('<span class=""></span>', ['movimiento-credito/imprimir_estado_cuenta', 'idCliente'=>$idCliente], [
		                        'class'=>'fa fa-print fa-2x',
		                        'id' => 'activity-index-link-report',
		                        'target'=>'_blank',
		                        'data-toggle' => 'titulo',
		                        //'data-url' => Url::to(['create']),
		                        'data-pjax' => '0',
		                        'title' => Yii::t('app', 'Imprimir estado de cuenta'),
		                        ]) ?>&nbsp;&nbsp;<?= Html::a('<span class=""></span>', ['movimiento-credito/enviar_estado_cuenta', 'idCliente'=>$idCliente], [
		                        'class'=>'fa fa-share-alt fa-2x',
		                        'onclick' => 'cargar_load_buton()',
		                        'data-toggle' => 'titulo',
		                        'title' => Yii::t('app', 'Enviar estado de cuenta al correo'),
		                        ]) ?> </div>

				</div>
				<div style="height: 180px;width: 100%; overflow-y: auto; ">
					<div class="panel-body">
						<div class="input-group">
			                <span class="input-group-addon">
			                    Correo
			                </span>
			                <?php
			                	$cliente = Clientes::find()->where(['idCliente'=>$idCliente])->one();
						        echo Html::input('text', '', $cliente->email, ['class' => 'form-control', 'placeholder' =>'Correo del cliente', 'readonly' => true, 'onchange' => '']);
						        $checked = $cliente->notificaciones == 'true' ? 'checked' : '';
						    ?>
						    <br>

						</div>
					</div>
					<center>
						<input class="tzCheckBox" onclick="notificar_estado_cuenta()" type="checkbox" id="ch_emails" name="ch_emails" data-on="ON" data-off="OFF" <?= $checked ?> >
						<p>Manten activada la casilla para que este cliente reciba semanalmente en su correo el estado de cuenta de forma automática</p>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
