<?php

use yii\helpers\Html;
use backend\models\Empresa;
use yii\helpers\Url;
use backend\models\RecibosDetalle;
use backend\models\NumberToLetterConverter;
use backend\models\MovimientoCredito;
use backend\models\MovimientoCobrar;
use backend\models\Recibos;
use backend\models\Clientes;
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
$empresa_funcion = new Empresa();
/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */

//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
    $urlParts = parse_url($_SERVER['REQUEST_URI']);

    if (isset($urlParts['query'])){
        //
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){

            $recibos = Recibos::find()->where(['=','idRecibo', $idRecibo])->one();
            $modelclient = Clientes::find()->where(['=','idCliente', $recibos->cliente])->one();

            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
            $detalles_doc = RecibosDetalle::find()->where(['idRecibo'=>$idRecibo])->orderBy(['idDetallerecibo' => SORT_DESC])->all();
            $content = '';

            $saldo_fact = 0;
            $facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $recibos->cliente])->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])->all();
            foreach($facturasCredito as $position => $facs) {
              $saldo_fact += $facs['credi_saldo'];//Suma del saldo pendiente de todas las facturas
            }

            //$products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->all();//Se obtienen datos del detalle de compra

            //$etiquetas_print = Etiquetas::find()->where(["=",'estado', 'act'])->all();//Se obtienen datos del detalle de compra
            $nombre_impresora = 'FACTURASNPS';
            //Create ESC/POS commands for sample receipt
            $esc = '0x1B'; //ESC byte in hex notation
            $newLine = '0x0A'; //LF byte in hex notation
            $derecha = $esc . '!' . '0x00';//Establece el margen izquierdo en la columna n (donde n está entre 0 y 255) en el tono de carácter actual.

            $cmds = $esc . "@"; //Initializes the printer (ESC @)
            $cmds .= $newLine . $newLine;
            $cmds .= $esc . '0x00'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
            //encabezado
            $cmds .= $empresa->nombre.$newLine; //Nombre de la empresa
            $cmds .= $empresa->direccion.$newLine; //Nombre de la empresa
            $cmds .= $empresa->telefono.' / '. $empresa->fax; //Nombre de la empresa

            $cmds .= $newLine . $newLine.$newLine;
            if ($recibos->tipoRecibo == 'CF') {
                //inicializamos las varibles glovales que se requieren para generar montos totales por facturas o NC
                //como tabien los diferentes tbody que vamos a ocupar para mostrar el conjunto de facturas y NC
                $tbodyfacturas = $tbodynotascredito = '';
                $montoFA = $montoNC = 0;
                foreach ($detalles_doc as $position => $detalle) {//Recorremos todo el detelle de dicho recibo
                //Si el detalle consta de Facturas lo recorremos con esta condicion
                        if ($detalle['tipo']=='FA') {
                            //obtenemos solo datos de facturas
                            $modelfa = MovimientoCredito::find()->where(['idCabeza_Factura'=>$detalle['idDocumento']])->one();
                            //$tbodyfacturas .= $modelfa->idCabeza_Factura."         ".date('d-m-Y', strtotime( $detalle['fecha'] ))."   ".number_format($detalle['monto'],2)."\n";
                            $tbodyfacturas .= new item($modelfa->idCabeza_Factura."                 ".date('d-m-Y', strtotime( $detalle['fecha'] )), number_format($detalle['monto'],2));
                            $montoFA += $detalle['monto'];//sumamos el monto total de las facturas recorridas

                        } //De lo contrario recorreriamos las notas de credito que se le aplicaron a la(s) factura(s) recorrida(s) anteriormente
                        else if ($detalle['tipo']=='NC') {
                            //obtenemos solo datos del movimiento por cobrar NC
                            $modelmovi = MovimientoCobrar::find()->where(['idMov'=>$detalle['idDocumento']])->one();
                            //$tbodynotascredito .= $modelmovi->idMov."         ".date('d-m-Y', strtotime( $detalle['fecha'] ))."   ".number_format($modelmovi->monto_movimiento,2)."\n";
                            $tbodynotascredito .= new item($modelmovi->idMov."                 ".date('d-m-Y', strtotime( $detalle['fecha'] )), number_format($modelmovi->monto_movimiento,2));
                            $montoNC += $modelmovi->monto_movimiento;//sumamos el monto total de las NC
                        }
                }//fin foeach
            $cmds .= $esc . '!' . '0x00'; //Fuente de caracteres A seleccionado (ESC! 0)
            $cmds .= 'RECIBO DE DINERO'.$newLine.'# '.$recibos->idRecibo. $newLine.$newLine.$newLine.$newLine;
            $cmds .= date('d-m-Y h:i:s A', strtotime( $recibos->fechaRecibo )).$newLine.$newLine.$newLine;
            $cmds .= "Hemos recibido de:".$newLine. $modelclient->nombreCompleto.$newLine.$newLine.$newLine;
            $cmds .= "La suma de:".$newLine. NumberToLetterConverter::ValorEnLetras($recibos->monto,"COLONES").$newLine.$newLine.$newLine;
            $cmds .= "Por Concepto de:".$newLine."CANCELACION DE FACTURAS".$newLine.$newLine.$newLine;
            $cmds .= "----------------------------------".$newLine."Facturas Canceladas".$newLine.
                     "----------------------------------".$newLine.$newLine;
            $cmds .= 'DOCUMENTO              FECHA             MONTO' . $newLine . $tbodyfacturas .$newLine."----------------------------------------------";
            $cmds .= new item("",'(+) '.number_format($montoFA,2) ).$newLine.$newLine.$newLine;
            $cmds .= "----------------------------------".$newLine."Notas de Credito Aplicadas".$newLine.'----------------------------------'.$newLine.$newLine;
            $cmds .= "DOCUMENTO              FECHA             MONTO" . $newLine . $tbodynotascredito . $newLine . "---------------------------------------------";
            $cmds .= new item("",'(-) '.number_format($montoNC,2)).$newLine.$newLine.$newLine.$newLine;
            $cmds .= 'MONTO: '. $esc . '!' . '0x31' . number_format($recibos->monto,2) . $newLine . $newLine . $newLine . $newLine;
            $cmds .= $esc . '!' . '0x00';
            $cmds .= 'Saldo total de la cuenta: '.number_format($recibos->saldo_cuenta,2) . $newLine . $newLine . $newLine . $newLine . $newLine;
            $cmds .= "_______________________". $newLine ."Autoriza" . $newLine . $newLine . $newLine . $newLine . $newLine;
            $cmds .= "Us-Reg:".$recibos->usuario;

            } else {
                $diferencia_tipo = $recibos->tipoRecibo == 'NC' ? 'NOTA DE CREDITO' : 'RECIBO DE DINERO';
                //$concepto = $recibos->tipoRecibo == 'NC' ? 'NOTA DE CRÉDITO' : 'ABONO A FACTURA';
                $muestra = $recibos->tipoRecibo == 'NC' ? 'Al cliente:' : 'Hemos recibido de:';
                $numerorecibo = $recibos->tipoRecibo == 'NC' ? '' : '# '.$recibos->idRecibo;
                $detalle_doc_indi = RecibosDetalle::find()->where(['idRecibo'=>$idRecibo])->one();
                $modelmovi = MovimientoCobrar::find()->where([ 'idMov'=>$detalle_doc_indi->idDocumento ])->one();
                //$modelmovi->
                $factura = MovimientoCredito::find()->where(['idCabeza_Factura'=>$modelmovi->idCabeza_Factura])->one();
                $cmds .= $diferencia_tipo . $newLine . $numerorecibo . $newLine . $newLine . $newLine ;
                $cmds .= date('d-m-Y h:i:s A', strtotime( $recibos->fechaRecibo )) . $newLine . $newLine ;
                $cmds .= $muestra . $newLine . $modelclient->nombreCompleto . $newLine . $newLine ;
                $cmds .= "La suma de:" . $newLine . NumberToLetterConverter::ValorEnLetras($recibos->monto,"COLONES") . $newLine . $newLine ;
                $cmds .= "Por Concepto de:" . $newLine . $modelmovi->concepto . $newLine . $newLine . $newLine ;
                $cmds .= "+-------------------------------+". $newLine . "| Factura que se le aplico      |" . $newLine . "+-------------------------------+" . $newLine ;
                $cmds .= "DOCUMENTO        FECHA             MONTO" . $newLine ;
                $cmds .= $factura->idCabeza_Factura.'            '.$factura->fecha_inicio.'       '.number_format($factura->total_a_pagar,2) . $newLine ;
                $cmds .= "---------------------------------------------" . $newLine . $newLine ;
                $cmds .= "+-----------------------+". $newLine . "| Movimiento #: " . $modelmovi->idMov . $newLine . "+-----------------------+" . $newLine . $newLine ;
                $cmds .= "Saldo Anterior: " . number_format($modelmovi->monto_anterior,2) . $newLine ;
                $cmds .= "Monto Aplicado: " . number_format($modelmovi->monto_movimiento,2) . $newLine ;
                $cmds .= "Saldo Documento: " . number_format($modelmovi->saldo_pendiente,2) . $newLine . $newLine . $newLine ;
                $cmds .= "MONTO: " . $esc . '!' . '0x31' . number_format($recibos->monto,2) . $newLine . $newLine;
                $cmds .= $esc . '!' . '0x00';
                $cmds .= "Saldo total de la cuenta: " . number_format($saldo_fact,2) . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine; ;
                $cmds .= "_______________________". $newLine ."Autoriza" . $newLine . $newLine . $newLine . $newLine . $newLine;
                $cmds .= "Us-Reg:".$recibos->usuario;
            }
            $cmds .= $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine;

            $cmds .= $esc . '0x69';//con esto corta el papel al final

            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente
            $cpj->clientPrinter = new InstalledPrinter($nombre_impresora);


            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();
            echo $cpj->sendToClient();

            ob_end_flush();
            exit();

        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de etiqueta-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

?>

<script type="text/javascript">
$(document).ready(function () {
        //ejecuta la llamada de impresión
        jsWebClientPrint.print();
        var url_ = "<?php echo Url::toRoute('movimiento-credito/index') ?>";
        window.location.href = url_;
        //self.close();
    });
</script>
<?php //otrs script
//Especifique la URL ABSOLUTA al archivo php que creará el objeto ClientPrintJob
//En este caso, esta misma página
$webClientPrintControllerAbsoluteURL = Utils::getRoot().$empresa_funcion->getWCP();
$demoPrintCommandsProcessAbsoluteURL = Utils::getRoot().Url::home().'?r=movimiento-credito/imprimir_recibo&idRecibo='.$idRecibo;
echo WebClientPrint::createScript($webClientPrintControllerAbsoluteURL, $demoPrintCommandsProcessAbsoluteURL, Yii::$app->user->identity->last_session_id);
echo '<meta http-equiv="refresh" content="1;url='.Yii::$app->getUrlManager()->createUrl('movimiento-credito/index').'" />';
//clase para agrupar numeros en fatura
class item
{
    private $nombre;
    private $precio;
    private $signo;

    public function __construct($nombre = '', $precio = '', $signo = false)
    {
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> signo = $signo;
    }

    public function __toString()
    {
        $rightCols = 13;
        $leftCols = 35;
        if ($this -> signo) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> nombre, $leftCols) ;

        $sign = ($this -> signo ? '$ ' : '');
        $right = str_pad($sign . $this -> precio, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}

?>
