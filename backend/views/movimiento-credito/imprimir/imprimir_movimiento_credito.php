<?php

use yii\helpers\Html;
use backend\models\Empresa;
use yii\helpers\Url;
use backend\models\MovimientoCobrar;
use backend\models\NumberToLetterConverter;
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
$empresa_funcion = new Empresa();
/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */

//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
    $urlParts = parse_url($_SERVER['REQUEST_URI']);

    if (isset($urlParts['query'])){
        //
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){

            $MovimientoCobrar = MovimientoCobrar::find()->where(['idMov'=>$id])->one();//busco el movimiento a cobrar
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//busco la empresa para el encabezado
            $tipo_movimiento = $MovimientoCobrar->tipmov == 'NC' ? 'NOTA DE CREDITO' : 'ABONO';
            //$products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->all();//Se obtienen datos del detalle de compra

            //$etiquetas_print = Etiquetas::find()->where(["=",'estado', 'act'])->all();//Se obtienen datos del detalle de compra
            $nombre_impresora = 'FACTURASNPS';
           //http://www.star-m.jp/eng/service/usermanual/linemode_cm_en.pdf
            //http://www.starmicronics.com/support/mannualfolder/tsp200pm.pdf
            //https://www.sparkfun.com/datasheets/Components/General/Driver%20board.pdf
            //http://www.nordenlogic.com/downloads/zlgprinter/DevGuide.pdf
            //http://www.sumiquel.com/ftp_todotpv/escpos%20ePSON.pdf
            //http://www.mabingenieros.com/index.php?name=/cabalo/aseinf/epson_escp2.html
            //http://ascii.cl/es/
            //http://starmicronics.com/support/mannualfolder/comemu_starline_pm.pdf
            //http://www.starmicronics.com/support/mannualfolder/escpos_cm_en.pdf
            //http://www.neodynamic.com/articles/How-to-print-raw-ESC-POS-commands-from-PHP-directly-to-the-client-printer/

            //Create ESC/POS commands for sample receipt
            $esc = '0x1B'; //ESC byte in hex notation
            $newLine = '0x0A'; //LF byte in hex notation
            $derecha = $esc . '!' . '0x00';//Establece el margen izquierdo en la columna n (donde n está entre 0 y 255) en el tono de carácter actual.

            $cmds = $esc . "@"; //Initializes the printer (ESC @)
            $cmds .= $newLine . $newLine;
            $cmds .= $esc . '0x00'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
            $cmds .= $empresa->nombre.$newLine; //Nombre de la empresa
            $cmds .= $empresa->direccion.$newLine; //Nombre de la empresa
            $cmds .= $empresa->telefono.' / '. $empresa->fax; //Nombre de la empresa

            $cmds .= $newLine . $newLine.$newLine;

            $cmds .= $esc . '!' . '0x00'; //Fuente de caracteres A seleccionado (ESC! 0)
            $cmds .= $tipo_movimiento.$newLine;
            $cmds .= '# '. $MovimientoCobrar->idMov.$newLine.$newLine;
            $cmds .= $MovimientoCobrar->fecmov.$newLine.$newLine;
            $cmds .= "Cliente:".$newLine. $MovimientoCobrar->idCliente.$newLine.$newLine;
            $cmds .= "Por Concepto de:".$newLine. $MovimientoCobrar->concepto.$newLine.$newLine;
            $cmds .= "----------------------------------------------".$newLine;
            $cmds .= "Aplicada al Documento:".$MovimientoCobrar->idCabeza_Factura.$newLine;
            $cmds .= "----------------------------------------------".$newLine;
            $cmds .= "MONTO: ". $esc . '!' . '0x31' .number_format($MovimientoCobrar->monto_movimiento,2).$newLine.$newLine.$newLine;
            $cmds .= $esc . '!' . '0x00';
            $cmds .= NumberToLetterConverter::ValorEnLetras($MovimientoCobrar->monto_movimiento,"COLONES");
            $cmds .= $newLine . $newLine . $newLine . $newLine . $newLine . $newLine;
            $cmds .= "_______________________". $newLine ."Autoriza" . $newLine . $newLine . $newLine . $newLine . $newLine;
            $cmds .= "Us-Reg:".$MovimientoCobrar->usuario;
            $cmds .= $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine;

            $cmds .= $esc . '0x69';//con esto corta el papel al final

            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente

                $cpj->clientPrinter = new InstalledPrinter($nombre_impresora);


            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();
            echo $cpj->sendToClient();

            ob_end_flush();
            exit();
        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de etiqueta-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
        //ejecuta la llamada de impresión
        jsWebClientPrint.print();
        var url_ = "<?php echo Url::toRoute('movimiento-credito/index') ?>";
        window.location.href = url_;
    });
</script>
<?php //otrs script
//Especifique la URL ABSOLUTA al archivo php que creará el objeto ClientPrintJob
//En este caso, esta misma página
$webClientPrintControllerAbsoluteURL = Utils::getRoot().$empresa_funcion->getWCP();
$demoPrintCommandsProcessAbsoluteURL = Utils::getRoot().Url::home().'?r=movimiento-credito/imprimir_movimiento_credito&id='.$id;
echo WebClientPrint::createScript($webClientPrintControllerAbsoluteURL, $demoPrintCommandsProcessAbsoluteURL, Yii::$app->user->identity->last_session_id);
echo '<meta http-equiv="refresh" content="1;url='.Yii::$app->getUrlManager()->createUrl('movimiento-credito/index').'" />';
?>
