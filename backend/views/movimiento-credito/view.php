<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MovimientoCredito */

$this->title = $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Movimiento Creditos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movimiento-credito-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idCabeza_Factura], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idCabeza_Factura], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCabeza_Factura',
            'fecha_inicio',
            'fecha_final',
            'fecha_vencimiento',
            'idCliente',
            'idOrdenServicio',
            'porc_descuento',
            'iva',
            'total_a_pagar',
            'estadoFactura',
            'tipoFacturacion',
            'codigoVendedor',
            'subtotal',
            'observacion',
        ],
    ]) ?>

</div>
