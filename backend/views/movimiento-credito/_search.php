<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MovimientoCreditoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movimiento-credito-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idCabeza_Factura') ?>

    <?= $form->field($model, 'fecha_inicio') ?>

    <?= $form->field($model, 'fecha_final') ?>

    <?= $form->field($model, 'fecha_vencimiento') ?>

    <?= $form->field($model, 'idCliente') ?>

    <?php // echo $form->field($model, 'idOrdenServicio') ?>

    <?php // echo $form->field($model, 'porc_descuento') ?>

    <?php // echo $form->field($model, 'iva') ?>

    <?php // echo $form->field($model, 'total_a_pagar') ?>

    <?php // echo $form->field($model, 'estadoFactura') ?>

    <?php // echo $form->field($model, 'tipoFacturacion') ?>

    <?php // echo $form->field($model, 'codigoVendedor') ?>

    <?php // echo $form->field($model, 'subtotal') ?>

    <?php // echo $form->field($model, 'observacion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
