<?php

use yii\helpers\Html;
use backend\models\MovimientoCredito;
use backend\models\MovimientoCobrar;
use backend\models\RecibosDetalle;
use kartik\widgets\DatePicker;
use backend\models\CreditoSession;
use yii\helpers\Url;
use yii\bootstrap\Modal;
$loading3 = \Yii::$app->request->BaseUrl.'/img/loading3.gif';

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//Funcion para obtener la suma de 30 dias a la fecha de documento
    function agrega_fecha(idCliente){
    	//var feast = $("#feast").val();

    	var fedes = document.getElementById("fedes").value;
    	var feast = document.getElementById("feast").value;

    	$.ajax({
	        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/filtro_facturas_canceladas') ?>",
	        type:"post",
	        data: { 'fedes': fedes, 'feast': feast, 'idCliente': idCliente },
	        beforeSend: function() {
	        	$('#factura-venci-div').html('<center><img src="<?php echo $loading3; ?>" style="height: 200px;"></center>');
	        },
	        success: function(data){
	        	//alert(data);
            $('#factura-venci-div').html(data);
	        	//$("#factura-venci-div").load(location.href+' #factura-venci-div','');
        		$("#nc_detalle_div").html('');
        		$("#recibos_detalle_div").html('');
	        },
	        error: function(msg, status,err){
	            alert('error 32');
	        }
	    });
    }

    function obtener_fact_canc(x, value){
    	//http://foro.elhacker.net/desarrollo_web/evento_onfocus_en_fila_de_una_tabla-t381665.0.html
        // Obtenemos todos los TR de la tabla con id "tabla_facturas_pendientes"
        // despues del tbody.
        var elementos = document.getElementById('tabla_facturas_canceladas').
        getElementsByTagName('tbody')[0].getElementsByTagName('tr');

        // Por cada TR empezando por el segundo, ponemos fondo blanco.
        for (var i = 0; i < elementos.length; i++) {
            elementos[i].style.background='white';
        }
        // Al elemento clickeado le ponemos fondo amarillo.
        x.style.background="yellow";
        //alert(value);
        inyectarfactura(value);

        //$("#nc_detalle_div").load(location.href+" #nc_detalle_div","");
        //$("#recibos_detalle_div").load(location.href+" #recibos_detalle_div","");

    }

    function inyectarfactura(value){
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/inyectarfactura') ?>",
                type:"post",
                dataType:"json",
                data: { 'idCabeza_Factura' : value },
                beforeSend: function() {
	            	$('#recibos_detalle_div').html('<center><img src="<?php echo $loading3; ?>" style="height: 100px;"></center>');
	            	$('#nc_detalle_div').html('<center><img src="<?php echo $loading3; ?>" style="height: 100px;"></center>');
	            },
                success: function(data){
                  $('#recibos_detalle_div').html(data.recibos_detalle_div);
  	            	$('#nc_detalle_div').html(data.nc_detalle_div);
                },
                error: function(msg, status,err){
                 alert('No pasa 43');
                }
            });
    }

    //Modal que me muestra la factura de facturas pendientes
	$(document).on('click', '#activity-index-link-facturas_canceladas', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $( "#mostrar_factura_modal_cancel" ).html( data );
                        $('#modalFacturacancel').modal();
                    }
                );
            }));
	//con esta modal consulto los datos de los movimientos ya sean de AB o NC usando solo el id del documento seleccionado
    function consulta_modal_recibo(idDoc){

    	$.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/consulta_modal_movi') ?>",
            type:"post",
            data: { 'idMov': idDoc },
            success: function(data){
                $( "#mostrar_recibo" ).html( data );

            },
            error: function(msg, status,err){
             alert('No pasa 111');
            }
        });

    }
</script>
<div class="movimiento-credito-facturas_canceladas">

	<div class="col-lg-12">

		<div class="col-lg-6">
      <label for="">Busqueda por creación de la factura.</label>
		<div class="form-group">
			<div class="input-group date id='datetimepicker1">
			  <span class="input-group-addon" id="basic-addon1"> Desde:</span>
			  <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName', 'options' => ['class'=>'form-control', 'placeholder'=>'Desde la fecha', 'id'=>'fedes', 'onchange'=>'javascript:agrega_fecha('.$idCliente.')']]) ?>
			  <span class="input-group-addon" id="basic-addon1"> Hasta:</span>
			  <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName', 'options' => ['class'=>'form-control', 'placeholder'=>'Hasta la fecha', 'id'=>'feast', 'onchange'=>'javascript:agrega_fecha('.$idCliente.')']]) ?>
			</div><br>
		</div>
		</div>
	</div>

	<div class="col-lg-7">
	<div class="well">
		<?php
			echo '<table class="items table table-striped">';
	        echo '<thead>';
	        printf('<tr><th width="350">%s</th><th>%s</th><th>%s</th><th>%s</th></tr>',
	                'DOCUMENTO',
	                'FECHA INICIO',
	                'FECHA FINAL',
	                'MONTO'
	                );
	        echo '</thead>';
	        echo '</table>';
			?>
		<div style="height: 500px;width: 100%; overflow-y: auto; ">
		<div id="factura-venci-div">
		</div>
		</div>
	</div>
	</div>
	<div class="col-lg-5">
	<div class="panel panel-info">
		<div class="panel-heading">
					Abonos parciales
		</div>
		<div class="panel-body">
			<?php
			echo '<table class="items">';
	        echo '<thead>';
	        printf('<tr><th width="150">%s</th><th width="160">%s</th><th>%s</th><th class="actions button-column">&nbsp;</th></tr>',
	                'DOCUMENTO',
	                'FECHA REG',
	                'MONTO'
	                );
	        echo '</thead>';
	        echo '</table>';
			?>
			<div style="height: 190px;width: 100%; overflow-y: auto; ">
				<div id="recibos_detalle_div">
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="col-lg-5">
	<div class="panel panel-info">
		<div class="panel-heading">
					Notas de Crédito
		</div>
		<div class="panel-body">
			<?php
			echo '<table class="items">';
	        echo '<thead>';
	        printf('<tr><th width="150">%s</th><th width="160">%s</th><th>%s</th><th class="actions button-column">&nbsp;</th></tr>',
	                'DOCUMENTO',
	                'FECHA REG',
	                'MONTO'
	                );
	        echo '</thead>';
	        echo '</table>';
			?>
			<div style="height: 190px;width: 100%; overflow-y: auto; ">
				<div id="nc_detalle_div">
				</div>
			</div>
		</div>
	</div>
	</div>

	<?php
	//-------------------------------------------------Pantallas modales-----------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------
	//Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalFacturacancel',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Factura</h4></center>',
                'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='well'><div id='mostrar_factura_modal_cancel'></div></div>";

            Modal::end();
    ?>
    <?php
	//Muestra una modal del recibo
         Modal::begin([
                'id' => 'modalconsultarecibo',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Movimiento</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='well'><div id='mostrar_recibo'></div></div>";

            Modal::end();
    ?>
</div>
