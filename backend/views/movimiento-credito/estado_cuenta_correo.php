<?php
//%2Fenviar_estado_cuenta&idCliente=2
use yii\helpers\Html;
use backend\models\MovimientoCredito;
use backend\models\MovimientoCobrar;
use backend\models\Clientes;
$css_checkbox = \Yii::$app->request->BaseUrl.'/jquery.tzCheckbox/jquery.tzCheckbox.css';
$jquery_checkbox = \Yii::$app->request->BaseUrl.'/jquery.tzCheckbox/jquery.tzCheckbox.js';
$script = \Yii::$app->request->BaseUrl.'/js/script.js';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $css_checkbox; ?>" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script src="<?php echo $jquery_checkbox; ?>"></script>
<script src="<?php echo $script; ?>"></script>
<script type="text/javascript">
function notificar_estado_cuenta(){
	var notif = document.getElementById("ch_emails").checked;
	var idCliente = "<?php echo $idCliente; ?>";
	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/notificar_estado_cuenta') ?>",
                type:"post",
                data: { 'notif' : notif, 'idCliente' : idCliente },
                beforeSend: function() {
	            	$('#recibo_detalle_div').html('<center><img src="http://www.reforma.com/libre/acceso/imgdiseno/loading3.gif" style="height: 200px;"></center>');
	            },
                success: function(data){
                    //$("#resultadoBusqueda").html(data);

                },
                error: function(msg, status,err){
                 alert('No pasa 43');
                }
            });
}
</script>

<div class="movimiento-credito-estado_cuenta">
	<div class="col-lg-12">

			<?php
			echo '<table class="items table table-striped">';
	        echo '<thead>';
	        printf('<tr>
										<th align="center"><font size=1>%s</font></th>
										<th align="center"><font size=1>%s</font></th>
										<th align="center"><font size=1>%s</font></th>
										<th align="left"><font size=1>%s</font></th>
										<th align="right"><font size=1>%s</font></th>
										<th align="right"><font size=1>%s</font></th>
										<th align="right"><font size=1>%s</font></th>
										<th align="center"><font size=1>%s</font></th></tr>',
	                'DOCUMENTO',
	                'TIPO',
	                'FECHA REGISTRO',
	                'FECHA VENCIMIENTO',
	                'MONTO (+)',
	                'MONTO (-)',
	                'SALDO',
	                'DÍAS.VENC'
	                );
	        echo '</thead>';


				$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();

	        	echo '<tbody>';
	        	$monto_fact = $monto_nc_ab = $saldo_fact = 0;
	        	$vencido_uno = $vencido_dos = $vencido_tres = $vencido_cuatro = 0;
	        	foreach($facturasCredito as $position => $factura) {
	        		$fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
	        		$fecha_vencimiento = date('d-m-Y', strtotime( $factura['fecha_vencimiento'] ));

	        		$montomenos = 0;
	        		$diasvencidas = 0;

	        		//condicion para obtener los días de vencimiento de la factura recorrida
	        		if (strtotime($factura['fecha_vencimiento']) < strtotime(date('Y-m-d'))) {
	        			$mv = new MovimientoCredito();
	        			$diasvencidas = $mv->getMovimiento_de_dias($factura['fecha_vencimiento'], date('Y-m-d'));

	        			$vencido_uno += $diasvencidas <= 30 ? $factura['credi_saldo'] : 0;
	        			$vencido_dos += ($diasvencidas <= 60) && ($diasvencidas > 30) ? $factura['credi_saldo'] : 0;
	        			$vencido_tres += ($diasvencidas <= 90) && ($diasvencidas > 60) ? $factura['credi_saldo'] : 0;
	        			$vencido_cuatro += $diasvencidas > 90 ? $factura['credi_saldo'] : 0;
	        		}

	        		$movimientofactura = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$factura['idCabeza_Factura']])->orderBy(['idMov' => SORT_DESC])->all();
	        		foreach($movimientofactura as $movimiento) {
	        			$montomenos += $movimiento['monto_movimiento'];
	        		}
	        		$monto_fact += $factura['total_a_pagar'];//Suma el monto de todas las factuas pendientes
	        		$monto_nc_ab += $montomenos;//Suma el monto de las notas de Credito y abonos
	        		$saldo_fact += $factura['credi_saldo'];//Suma del saldo pendiente de todas las facturas
							if ($factura['factun_id'] > 0) {
								$n_factura = $factura['fe'];
							} else {
								$n_factura = $factura['idCabeza_Factura'];
							}
	        		 printf('<tr>
	        		 			  <td align="center" ><font face="arial" size=2 >%s</font></td>
	                		<td align="center" ><font size=2>%s</font></td>
	                		<td align="center" ><font size=2>%s</font></td>
											<td align="left"><font size=2>%s</font></td>
	                		<td align="right"><font size=2>%s</font></td>
	                		<td align="right"><font size=2>%s</font></td>
	                		<td align="right"><font size=2>%s</font></td>
	                		<td align="center"><font size=2>%s</font></td>
										</tr>',
                      $factura['fe'],
                      'FA',
                      $fecha_inicio,
                      'Vence '.$fecha_vencimiento,
                      number_format($factura['total_a_pagar'],2),
                      number_format($montomenos,2),
                      number_format($factura['credi_saldo'],2),
                      $diasvencidas
                      );

	        		foreach($movimientofactura as $position => $movimiento) {

	        			$aplicada = 'Aplica FA - '.$movimiento['idCabeza_Factura'];
	        			printf('<tr style="color:#29AB1F"><td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
	                		<td align="center" style="background: ;"><font face="arial" size=2>%s</font></td><td align="center"><font face="arial" size=2>%s</font></td><td align="left"><font face="arial" size=2>%s</font></td>
	                		<td align="right"><font face="arial" size=2>%s</font></td><td align="right"><font face="arial" size=2>%s</font></td><td>%s</td><td>%s</td></tr>',

	                            $movimiento['idMov'],
	                            $movimiento['tipmov'],
	                            date('d-m-Y', strtotime( $movimiento['fecmov'] )),
	                            $aplicada,
	                            '',
	                            number_format($movimiento['monto_movimiento'],2),
	                            '',
	                            ''
	                            );
	        		}

	        	}
	        	echo '</tbody>';
	        	echo '</table><hr>';
	        	$no_vencido = $saldo_fact - ($vencido_uno+$vencido_dos+$vencido_tres+$vencido_cuatro);
	        ?>


		<p style="text-align:right;">
			<font size=2>Monto por facturas:</font> <font size=2><?= number_format($monto_fact,2) ?></font>&nbsp;&nbsp;&nbsp;&nbsp;
			<font size=2>NC y AB aplicados:</font> <font size=2><?= number_format($monto_nc_ab,2) ?></font>&nbsp;&nbsp;&nbsp;&nbsp;
			<font size=2>Saldo pendiente:</font> <font size=2><?= number_format($saldo_fact,2) ?></font>
		</p>
		<div class="col-lg-4"><br>
			<font size=3>Detalle de Vencimiento</font>
			<table>
				<tr>
					<td align="left" width="100"><font size=2><strong>No vencido</strong></font></td><td align="right"><font size=2><?= number_format($no_vencido,2) ?></font></td>
				</tr>
				<tr>
					<td align="left" width="150"><font size=2><strong>De 2 a 30 días</strong></font></td><td align="right"><font size=2><?= number_format($vencido_uno,2) ?></font></td>
				</tr>
				<tr>
					<td align="left" width="150"><font size=2><strong>De 31 a 60 días</strong></font></td><td align="right"><font size=2><?= number_format($vencido_dos,2) ?></font></td>
				</tr>
				<tr>
					<td align="left" width="150"><font size=2><strong>De 61 a 90 días</strong></font></td><td align="right"><font size=2><?= number_format($vencido_tres,2) ?></font></td>
				</tr>
				<tr>
					<td align="left" width="150"><font size=2><strong>Mas de 90 días</strong></font></td><td align="right"><font size=2><?= number_format($vencido_cuatro,2) ?></font></td>
				</tr>
			</table>
		</div>

	</div>
</div>
