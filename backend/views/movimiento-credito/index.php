<?php
//http://bootsnipp.com/tags/navbar
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock;
use backend\models\CreditoSession;
use backend\models\MovimientoCredito;
use backend\models\Clientes;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';

//si un parametro GET entra al index esta condicion lo detecta y llama la impresora
//este me imprime de recibos_generados (todo) y de facturas_canceladas (recibos de abonos)
if (isset($_REQUEST['idRecibo'])){
      $idRecibo = $_GET['idRecibo'];
      echo $this->render('imprimir/imprimir_recibo', [//aqui llama imprimir_recibo que imprime la factura correspondiente a este id
            'idRecibo' => $idRecibo,
        ]);

    }
//este me imprime la cancerlacion de las facturas
if (isset($_REQUEST['idRecibof'])){
      $idRecibof = $_GET['idRecibof'];
      echo $this->render('imprimir/imprimir_facturas', [//aqui llama imprimir_facturas que imprime la factura correspondiente a este id
            'idRecibof' => $idRecibof,
        ]);

    }
//este me imprime de facturas_canceladas (Notas de credito)
if (isset($_REQUEST['id'])){
      $id = $_GET['id'];
      echo $this->render('imprimir/imprimir_movimiento_credito', [//aqui llama imprimir_movimiento_credito que imprime la factura correspondiente a este id
            'id' => $id,
        ]);
    }
//Para pintar las filas de las tablas http://getbootstrap.com/css/

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MovimientoCreditoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuentas por Cobrar';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//Funcion que hace abrir modal desde la tecla[F2]
window.onkeydown = tecla;
  function tecla(event) {
    //event.preventDefault();
    num = event.keyCode;

    if(num==113)
      $('#modalcliente').modal('show');//muestro la modal

  }

//Carga un cliente
function agrega_cliente(codCliente){
    $('#datos_cliente').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/addcliente') ?>",
        type:"post",
        data: { 'codCliente': codCliente },
        success: function(data){
         //alert(data);
         //$("#conten_pro").html('index.php?r=cabeza-prefactura%2Ftabla');

        //$("#conten_pro").load(location.href+' #conten_pro','');
        //$("#codProdServicio").select2("val", ""); //vaciar contenido de select2
        //$("#descuento").select2("val", "");
         //location.reload();
        },
        error: function(msg, status,err){
         //alert('No pasa');
         //$("#conten_pro").load('cabeza-prefactura/create');
         //$.update('#texto-msg');

        }
    });

}

//Me pinta el titulo con negro transparente
    $(document).ready(function(){
        $('[data-toggle="modal"]').tooltip();
    });

//activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
        $('#boton-abonar-deposito').addClass('disabled');//desabilito el boton de abonar, desde el index para que me afecte a todo hasta que el form esté lleno
        $('#boton-abonar-tarjeta').addClass('disabled');// ''
        $('#boton-abonar-cheque').addClass('disabled');// ''

        $('#boton-abonar-deposito_c').addClass('disabled');//desabilito el boton de abonar, desde el index para que me afecte a todo hasta que el form esté lleno
        $('#boton-abonar-tarjeta_c').addClass('disabled');// ''
        $('#boton-abonar-cheque_c').addClass('disabled');// ''
    });

    $(document).ready(function () {
  		//me checa todas las facturas
  	    $('#checkAll').on('click', function() {
  	        if (this.checked == true)
  	            $('#tabla_facturas_pendientes').find('input[name="checkboxRow[]"]').prop('checked', true);
  	        else
  	            $('#tabla_facturas_pendientes').find('input[name="checkboxRow[]"]').prop('checked', false);
  	    });

  	    $('#cancelarButton').on('click', function () {
  		    var $btn = $(this).button('loading')
  		    // simulating a timeout
  		    setTimeout(function () {
  		        $btn.button('reset');
  		    }, 2000);
  		  });

  	    $('#load_buton_nc').on('click', function () {
  		    var $btn = $(this).button('loading')
  		    // simulating a timeout
  		    setTimeout(function () {
  		        $btn.button('reset');
  		    }, 2000);
  		  });

  	});

    //Modal que me muestra la factura de facturas pendientes
  	$(document).on('click', '#activity-factura', (function() {
                  $.get(
                      $(this).data('url'),
                      function (data) {
                          //$('.modal-body').html(data);
                          $( "#mostrar_factura_modal" ).html( data );
                          $('#modalFactura').modal();
                      }
                  );
              }));

    //me permite buscar por like los clientes a BD
    function buscar() {
        var textoBusqueda = $("input#busqueda").val();
        if (textoBusqueda != "") {

            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/busqueda_cliente') ?>",
                type:"post",
                data: { 'valorBusqueda' : textoBusqueda },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 alert('No pasa 286');
                }
            });
        } else {
            ("#resultadoBusqueda").html('');
        }
    }

    //esta funcion me mantiene el boton de Cancelar inestable mientras no hayan facturas a cancelar
    function compruebafacturasacancelar() {
        var total_facturas = document.getElementById("total_facturas").value;
        if (total_facturas == '0.00') {
            $('#cancelarButton').addClass('disabled');
        }
        else {
            $('#cancelarButton').removeClass('disabled');
        }
    }
    setInterval('compruebafacturasacancelar()');

    //muestro la modal por la que puedo revocar una factura por NC y ver los movimientos
		$(document).on('click', '#activity-nota_credito_rev', (function() {
		  document.getElementById("movimientos_rev").innerHTML = '';
		                $.get(
		                    $(this).data('url'),
		                    function (data) {
		                        //$('#movimientos').html(data);
		                        document.getElementById("movimientos_rev").innerHTML = data;
		                        //$('#modalNotacredito').modal();
		                    }
		                );
		            }));
</script>
<style>
    #modalFactura .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
    #modalFactura2 .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
    #modalFacturacancel .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="movimiento-credito-index">

    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <div class="col-lg-12">
        <div class="panel panel-warning">
            <div class="panel-heading">
                 Seleccione el Cliente.
                 <?php
                 if (@$idcliente = CreditoSession::getIDcliente()) {
                       echo '<div class="pull-right">' .Html::a('Actualizar datos de este Cliente', ['clientes/update', 'id'=>$idcliente], ['class' => 'btn btn-primary btn-xs', 'target'=>'_blank']) .
                       ' ' . Html::a('Limpiar para agregar otro cliente', ['limpiar'], ['class' => 'btn btn-warning btn-xs']) . '</div>';
                 } ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body" id="datos_cliente">
                <div class="col-lg-1">
                <label>ID Cliente</label>
                <?php
                    if (@$idcliente = CreditoSession::getIDcliente()) {
                        echo '<h4>'.$idcliente.'</h4>';
                    } else
                         echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'id' => 'medio', 'class' => 'form-control', 'placeholder' =>'ID Cliente', 'onchange' => 'javascript:agrega_cliente(this.value)']);
                ?>
                </div>
                <div class="col-lg-3">
                <label>Cliente</label>
                <div class="input-group">
                <span class="input-group-btn">
                    <button class="btn btn-default" data-toggle="modal" data-placement="top" title="Buscar Cliente con crédito pendiente" type="button" data-target="#modalcliente"><span class="glyphicon glyphicon-search"></span></button>
                </span>
                <?php
                if (@$cliente = CreditoSession::getCliente()) {
                    echo Html::input('text', '', $cliente, ['class' => 'form-control', 'placeholder' => $cliente, 'readonly' => true, 'onchange' => '']);
                } else
                    echo Html::input('text', '', '', ['class' => 'form-control', 'placeholder' =>'Nombre del cliente', 'readonly' => true, 'onchange' => '']);
                ?>
                </div>
                </div>
                <div class="col-lg-2">
                <label>Crédito Aprobado</label>
                    <div id="aprobado" style="background-color:#FFC; padding:12px">
                        <?php
                            if (/*@$aprobado = CreditoSession::getAprobado()*/@$idcliente = CreditoSession::getIDcliente()) {
                                $modelcliente = Clientes::find()->where(['idCliente'=>$idcliente])->one();
                                echo '₡ '.number_format($modelcliente->montoMaximoCredito,2);
                            }
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                <label>Saldo a favor</label>
                    <div id="aprobado" style="background-color:#FFC; padding:12px">
                        <?php
                            if (/*@$aprobado = CreditoSession::getAprobado()*/@$idcliente = CreditoSession::getIDcliente()) {
                                $modelcliente = Clientes::find()->where(['idCliente'=>$idcliente])->one();
                                echo '₡ '.number_format($modelcliente->saldo_a_favor,2);
                            }
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                <label>Crédito Ejecutado</label>
                    <div id="ejecutado" style="background-color:#FFC; padding:12px">
                        <?php
                          $saldo_fact = 0;
                            if (/*@$ejecutado = CreditoSession::getEjecutado()*/@$idcliente = CreditoSession::getIDcliente()) {
                              $facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $idcliente])->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])->all();

                              foreach($facturasCredito as $position => $factura) {
                                $saldo_fact += $factura['credi_saldo'];//Suma del saldo pendiente de todas las facturas
                  						}
                                //$modelcliente = Clientes::find()->where(['idCliente'=>$idcliente])->one();
                                echo '₡ '.number_format($saldo_fact/*$modelcliente->montoTotalEjecutado*/,2);
                            }
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                <label>Disponible para crédito</label>
                    <div id="disponible" style="background-color:#FFC; padding:12px">
                        <?php
                            if (/*@$disponible = CreditoSession::getDisponible()*/@$idcliente = CreditoSession::getIDcliente()) {
                                $modelcliente = Clientes::find()->where(['idCliente'=>$idcliente])->one();
                                $montodisponible = $modelcliente->montoMaximoCredito - $saldo_fact/*$modelcliente->montoTotalEjecutado*/;
                                echo '₡ '.number_format($montodisponible,2);
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?= AlertBlock::widget([
                'type' => AlertBlock::TYPE_ALERT,
                'useSessionFlash' => true,
                'delay' => 5000
            ]);?>
        <ul class="nav nav-tabs nav-justified">
                <li class="active" role="presentation" ><a href="#cance" data-toggle="tab">Cancelación</a></li>
                <li role="presentation"><a href="#reci_gene" data-toggle="tab">Recibos Generados</a></li>
                <li role="presentation"><a href="#estad_cuenta" data-toggle="tab">Estado de Cuenta</a></li>
                <li role="presentation"><a href="#fac_can" data-toggle="tab">Facturas Canceladas</a></li>
        </ul>
        <div class="table-responsive tab-content">
            <div class="tab-pane fade in active" id="cance"><br>
                <?php
                    if (@$idcliente = CreditoSession::getIDcliente()) {
                        echo $this->render('cancelacion', [
                        'idCliente' => $idcliente,
                       ]);
                    } else
                        echo '<div class="well col-lg-12"><center>Esperando un cliente para cancelación</center></div>';
                ?>
            </div>
            <div class="tab-pane fade" id="reci_gene"><br>
                <?php
                    if (@$idcliente = CreditoSession::getIDcliente()) {
                        echo $this->render('recibos_generados', [
                        'idCliente' => $idcliente,
                       ]);
                    } else
                        echo '<div class="well col-lg-12"><center>Esperando un cliente para Recibos Generados</center></div>';
                ?>
            </div>
            <div class="tab-pane fade" id="estad_cuenta"><br>
                <?php
                    if (@$idcliente = CreditoSession::getIDcliente()) {
                        echo $this->render('estado_cuenta', [
                        'idCliente' => $idcliente,
                       ]);
                    } else
                        echo '<div class="well col-lg-12"><center>Esperando un cliente para Estado de Cuenta</center></div>';
                ?>
            </div>
            <div class="tab-pane fade" id="fac_can"><br>
                <?php
                    if (@$idcliente = CreditoSession::getIDcliente()) {
                        echo $this->render('facturas_canceladas', [
                        'idCliente' => $idcliente,
                       ]);
                    } else
                        echo '<div class="well col-lg-12"><center>Esperando un cliente para Facturas Canceladas</center></div>';
                ?>
            </div>
        </div>
    </div>
    <!--?php echo $this->render('_search', ['model' => $searchModel]); ?-->


    <!--?= /*GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idCabeza_Factura',
            'fecha_inicio',
            'fecha_final',
            'fecha_vencimiento',
            'idCliente',
            // 'idOrdenServicio',
            // 'porc_descuento',
            // 'iva',
            // 'total_a_pagar',
            // 'estadoFactura',
            // 'tipoFacturacion',
            // 'codigoVendedor',
            // 'subtotal',
            // 'observacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);*/ ?-->
    <div class="modal fade" id="modalcliente" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Lista de Clientes</h4>
        </div>
        <div class="modal-body">
            <?php
                echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '80', 'id' => 'busqueda', 'class' => 'form-control', 'placeholder' =>'Busque el cliente que desea agregar', 'onKeyUp' => 'javascript:buscar()']);
            ?>
            <br>
            <div id="resultadoBusqueda" style="height: 200px;width: 100%; overflow-y: auto; "></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>
      </div>
    </div>
  </div>

</div>
