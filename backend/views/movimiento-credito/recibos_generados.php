<?php

use yii\helpers\Html;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\MovimientoCobrar;
use backend\models\MovimientoCredito;
use backend\models\CreditoSession;
use yii\helpers\Url;
use yii\bootstrap\Modal;
//use yii\helpers\Json;
$loading3 = \Yii::$app->request->BaseUrl.'/img/loading3.gif';

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
	//Modal que me muestra la factura de facturas pendientes
	$(document).on('click', '#activity-index-link-factura-2', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $( "#mostrar_factura_modal2" ).html( data );
                        $('#modalFactura2').modal();
                    }
                );
            }));
	//Marcador de facturas para obtener datos a manipular
	function marcarecibos(x, value) {
		//http://foro.elhacker.net/desarrollo_web/evento_onfocus_en_fila_de_una_tabla-t381665.0.html
        // Obtenemos todos los TR de la tabla con id "tabla_facturas_pendientes"
        // despues del tbody.
        var elementos = document.getElementById('tabla_recibos').
        getElementsByTagName('tbody')[0].getElementsByTagName('tr');

        // Por cada TR empezando por el segundo, ponemos fondo blanco.
        for (var i = 0; i < elementos.length; i++) {
            elementos[i].style.background='white';
        }
        // Al elemento clickeado le ponemos fondo amarillo.
        x.style.background="yellow";
        //alert(value);
        inyectarrecibo(value);

        //$("#recibo_detalle_div").load(location.href+" #recibo_detalle_div","");
    }

    function inyectarrecibo(value){
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/inyectarrecibo') ?>",
                type:"post",
                data: { 'idRecibo' : value },
                beforeSend: function() {
	            	$('#recibo_detalle_div').html('<center><img src="<?php echo $loading3; ?>" style="height: 200px;"></center>');
	            },
                success: function(data){
                    $("#recibo_detalle_div").html(data);
                },
                error: function(msg, status,err){
                 alert('No pasa 43');
                }
            });
    }
    function consulta_recibo(idCliente){
    	idRecibo_buscar = document.getElementById('idRecibo_buscar').value;
    	$.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/consulta_recibo') ?>",
            type:"post",
            data: { 'idRecibo_buscar': idRecibo_buscar, 'idCliente' : idCliente },
            beforeSend: function() {
            	$('#recibo-div').html('<center><img src="<?php echo $loading3; ?>" style="height: 200px;"></center>');
            },
            success: function(data){
                //$("#recibo-div").load(location.href+" #recibo-div","");
								$( "#recibo-div" ).html( data );
                $('#idRecibo_buscar').val('');
            },
            error: function(msg, status,err){
             alert('Dato de entrada incorrecto');
            }
        });
    }

    function consultar_todos_recibos(idCliente){
    	$.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/consultar_todos_recibos') ?>",
            type:"post",
            data: { 'idCliente': idCliente },
            beforeSend: function() {
            	$('#recibo-div').html('<center><img src="<?php echo $loading3; ?>" style="height: 200px;"></center>');
            },
            success: function(data){
                //$("#recibo-div").load(location.href+" #recibo-div","");
								$( "#recibo-div" ).html( data );
                $('#idRecibo_buscar').val('');
            },
            error: function(msg, status,err){
             alert('96');
            }
        });
    }
    //con esta modal consulto los datos de los movimientos ya sean de AB o NC usando solo el id del documento seleccionado
    function consulta_modal_movimiento(idDoc){

    	$.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-credito/consulta_modal_movi') ?>",
            type:"post",
            data: { 'idMov': idDoc },
            success: function(data){
                $( "#mostrar_movimiento_modal2" ).html( data );

            },
            error: function(msg, status,err){
             alert('No pasa 111');
            }
        });

    }
</script>
<div class="movimiento-credito-recibos_generados">
	<div class="col-lg-12">
		<div class="col-lg-4">
			<div class="input-group">
                <span class="input-group-btn">
                	<button class="btn btn-default" data-toggle="titulo" data-placement="right" title="Cargar todos los recibos" type="button" onclick="consultar_todos_recibos(<?= $idCliente ?>)"><span class="glyphicon glyphicon-refresh"></span></button>
                </span>
                <?php
                echo Html::input('text', '', '', ['class' => 'form-control', 'id'=>'idRecibo_buscar','placeholder' =>'No. Recibo', 'readonly' => false, 'onchange' => 'consulta_recibo(<?= $idCliente ?>)']);
                ?>
                <span class="input-group-btn">
                	<button class="btn btn-default" data-toggle="titulo" data-placement="right" title="Buscar recibo" type="button" onclick="consulta_recibo(<?= $idCliente ?>)"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
			<br>
	    </div>

	</div>
	<div class="well col-lg-5">
		<?php
		echo '<table class="items table table-striped">';
        echo '<thead>';
        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th class="actions button-column">&nbsp;</th></tr>',
                'NO.<br>RECIBO',
                'MONTO',
                'FECHA REG',
                'USUARIO'
                );
        echo '</thead>';
        echo '</table>';
		?>
		<div style="height: 350px;width: 100%; overflow-y: auto; ">
			<div id="recibo-div">
			</div>
		</div>
	</div>
	<div class="col-lg-7">
	<div class="well">
		<?php
		echo '<table class="items table table-striped">';
        echo '<thead>';
        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th class="actions button-column">&nbsp;</th></tr>',
                'DOCUMENTO',
                'TIPO',
                'FECHA',
                'MONTO',
                'REFERENCIA'
                );
        echo '</thead>';
        echo '</table>';
		?>
		<div style="height: 200px;width: 100%; overflow-y: auto; ">
			<div id="recibo_detalle_div">
			</div>
		</div>
	</div>
	</div>
	<?php
	//-------------------------------------------------Pantallas modales-----------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------
	//Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalFactura2',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Factura</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='well'><div id='mostrar_factura_modal2'></div></div>";

            Modal::end();
    ?>
    <?php
	//Muestra una modal del movimiento seleccionado
         Modal::begin([
                'id' => 'modalconsultadocu',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Movimiento</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='well'><div id='mostrar_movimiento_modal2'></div></div>";

            Modal::end();
    ?>
</div>
