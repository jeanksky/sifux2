<?php
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\AgentesComisiones;

$empresaimagen = new Empresa();

$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';
$agenteCoMisiones = AgentesComisiones::find()->all();
if ($lista_clientes != '0') {
  $agenteCoMisiones = AgentesComisiones::find()->where("idAgenteComision IN (".$lista_clientes.")", [])->all();
}

$sqlAGNT = "SELECT SUM(subtotal-porc_descuento) AS sumD151AGNT, idAgenteComision FROM `tbl_encabezado_factura`
WHERE idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
(SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC' GROUP BY idCabeza_Factura) fact_not
ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
WHERE fact.total_a_pagar = fact_not.suma_nc)
AND fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura IN ('Cancelada') GROUP BY idAgenteComision DESC;";

  $command = \Yii::$app->db->createCommand($sqlAGNT);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $ventasAGNT = $command->queryAll();


$comisiones = Yii::$app->params['comisiones'];
if ($comisiones == true) {
/*Fechas en el formato para personas*/
$f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
$f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
      </span>';

echo '<table class="items table table-striped" id="tabla_d151_agente_comiciones"  >';
echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
                        'N° Cedula',
                        'Numero Agente',
                        'Nombre Agente',
                        'Telefono',
                        'Monto Facturado',
                        '% Comisión',
                        'Monto Comisión'
                        );
                echo '</thead>';
echo '<tbody class="">';
$monto_total_AGNT = 0;
$limiteMontoAGNT = Yii::$app->params['limite_monto_compra_MIN']; //Limite impuesto por el MIN en comision >= ¢50,000
foreach($ventasAGNT as $facturaAGNT) {

    /*Datos Clientes*/
  $nCedulaAgente = '';
  $numAgente = '';
  $nombre_agente = '';
  $agenteTelefono = '';
  $porcentComisionXagnt = '';
  $montoComisionXagnt = 0;

  //if($agenteComi = AgentesComisiones::findOne($facturaAGNT['idAgenteComision']))
  foreach ($agenteCoMisiones as $key => $agenteCoMisi)
  {
    $porcentComisionXagnt = $agenteCoMisi->porcentComision;
    $montoComisionXagnt = $facturaAGNT['sumD151AGNT'] * ($porcentComisionXagnt / 100);

    if($facturaAGNT['idAgenteComision'] == $agenteCoMisi->idAgenteComision){
            $nCedulaAgente = $agenteCoMisi->cedula;
            $numAgente =  $agenteCoMisi->idAgenteComision;
            $nombre_agente = $agenteCoMisi->nombre;
            $agenteTelefono = $agenteCoMisi->telefono;

            $monto_total_AGNT += $montoComisionXagnt;

            printf('<tr style="color:#231BB8">
                  <td align="center" ><font face="arial" size=2 >%s</font></td>
                  <td align="center"  ><font size=2>%s</font></td>
                  <td align="center"><font size=2>%s</font></td>
                  <td align="center"><font size=2>%s</font></td>
                  <td align="center" ><font face="arial" size=2 >%s</font></td>
                  <td align="center"><font size=2>%s</font></td>
                  <td align="right" ><font size=2>%s</font></td>
                </tr>',
                      $nCedulaAgente,
                      $numAgente,
                      $nombre_agente,
                      $agenteTelefono,
                      number_format($facturaAGNT['sumD151AGNT'] ,2),
                      $porcentComisionXagnt,
                      number_format($montoComisionXagnt ,2)
                  );
    }//fin if limiteMonto ley Tributaria
  }//fin del if BusquedaClienteRegistrado -> foreach REMPLAZO
}//fin foreach


printf('<tr>
          <td style="text-align:center" ><font size=3>%s</font></td>
          <td style="text-align:right" width="190"><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="190"><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="190"><font size=3>%s</font></td>
        </tr>',
            '',
            '',
            '',
            '',
            '',
            '',
            '<h4>Total: <strong>'.number_format($monto_total_AGNT,2).'</strong></h4>'
        );
echo '</tbody>';
echo '</table>';
} // fin if comisiones VALIDA PARAMS
?>
