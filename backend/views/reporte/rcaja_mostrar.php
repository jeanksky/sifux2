<?php
use kartik\widgets\DatePicker;
use backend\models\Clientes;
use backend\models\ClientesApartados;
use backend\models\AgentesComisiones;
use backend\models\Funcionario;
//use app\models\Proveedores;
//use app\models\CuentasBancarias;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';
?>

<?php
//>>>>>>>>>>>>>>>>>>>>>>>>>BALANCE DE CAJA
if ($tipo=='balance') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Balance de caja</h3>';
  echo '<div class="col-lg-3 col-md-3">
  <labe for="fecha_desde9">Fecha Inicio</labe>
  '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
  'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
  'id'=>'fecha_desde9','autocomplete'=>'off']]).'
  </div>
  <div class="col-lg-3 col-md-3">
  <labe for="fecha_hasta9">Fecha Fin</labe>
  '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
  'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
  'id'=>'fecha_hasta9','autocomplete'=>'off']]).'
  </div>
  <div class="col-lg-3 col-md-3" style="margin-top:25px;">
  <a class="btn btn-default btn-sm" onclick="buscar_reporte_venta_balance_caja()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Mostrar</a>
  <a class="btn btn-default btn-sm" id="imp_re_ve_ba_ca" onclick="imprimir_reporte_venta_balance_caja()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
  </div>
  <div class="col-lg-12 col-md-12">
<br />
  </div>
  ';
  echo '<div class=" col-lg-12 col-md-12" >
<div class="well col-lg-12 col-md-12" id="mostrar_venta_bal_ca" >
</div>
  </div>';
}
//>>>>>>>>>>>>>>>>>>>>>>>>>REPORTE DE ABONOS
elseif($tipo=='abonos') {
  /*SELECT `tbl_clientes`.`idCliente`, `tbl_clientes`.`nombreCompleto` FROM `tbl_clientes` INNER JOIN `tbl_encabezado_factura` `ef`
ON ef.`idCliente` = tbl_clientes.`idCliente` WHERE (ef.estadoFactura IN ("Crédito","Cancelada")) AND (`ef`.`tipoFacturacion`='4')
GROUP BY `tbl_clientes`.`idCliente`;*/
  $nombre_clientes = Clientes::find()
  ->innerJoin(['ef'=>'tbl_encabezado_factura'],'ef.idCliente = tbl_clientes.idCliente')
  ->where(['IN','ef.estadoFactura',["Crédito","Cancelada"]])
  ->andWhere(['ef.tipoFacturacion'=>'4'])
  ->groupBy('tbl_clientes.idCliente')
  ->all();
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de abonos y cancelaciones a facturas de crédito</h3>';
  echo '<div class="col-lg-6 col-md-6">
  <label for="filtro_cliente_ab">Clientes</label>'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
    'name' => '',
    'data' => ArrayHelper::map($nombre_clientes,'idCliente',function($element) {
      return $element['idCliente'].' - '.$element['nombreCompleto'];
    }),
    'options' => [
      'placeholder' => '- Seleccione -',
      'id'=>'filtro_cliente_ab',
      'multiple' => true //esto me ayuda a que la busqueda sea multiple
    ],
    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
    ]).'</div>';
    echo '<div class="col-lg-3 col-md-3">
    <label for="fecha_desde6">Fecha Inicio</label>
    '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
    'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
    'id'=>'fecha_desde6','autocomplete'=>'off']]).'
    </div>
    <div class="col-lg-3 col-md-3">
    <label for="fecha_hasta6">Fecha Fin</label>
    '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
    'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
    'id'=>'fecha_hasta6','autocomplete'=>'off']]).'
    </div>
    <div class="col-lg-6 col-md-6" style="margin-top:25px;">
    <a class="btn btn-default btn-sm" onclick="buscar_reporte_abonos()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
    <a class="btn btn-default btn-sm" id="imp_re_abon" onclick="imprimir_reporte_abonos()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
    </div>
    ';
    echo '<div class="col-lg-12"><br>
    <div class="well" id="mostrar_abonos"></div>
    </div>';
  }
  //>>>>>>>>>>>>>>>>>>>>>>>>>CUENTAS POR COBRAR
  elseif ($tipo=='cuent_x_cobra') {
    $nombre_clientes = Clientes::find()
    ->where(['>','montoTotalEjecutado', 0])->all();
    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Cuentas por cobrar</h3>';
    echo '<div class="col-lg-6">
    <label for="filtro_cliente_venta_cuen_cobrar">Clientes</label>'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
      'name' => '',
      'data' => ArrayHelper::map($nombre_clientes,'idCliente',function($element) {
        return $element['idCliente'].' - '.$element['nombreCompleto'];
      }),
      'options' => [
        'placeholder' => '- Seleccione -',
        'id'=>'filtro_cliente_venta_cuen_cobrar',
        'multiple' => true //esto me ayuda a que la busqueda sea multiple
      ],
      'pluginOptions' => ['initialize'=> true,'allowClear' => true]
      ]).'</div>';
      echo '<div class="col-lg-3" style="margin-top:25px;"><input id="facturas_vencidas" type="checkbox" name="" > Incluir sólo facturas vencidas </div>';
      echo '<div class="col-lg-3" style="margin-top:25px;">
      <a class="btn btn-default btn-sm" onclick="buscar_reporte_venta_cuen_cobrar()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
      <a class="btn btn-default btn-sm" id="imp_re_cuen_cobrar" onclick="imprimir_reporte_venta_cuen_cobrar()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
      </div>
      ';
      echo '<div class="col-lg-12"><br>
      <div class="well" id="mostrar_cuen_cobrar"></div>
      </div>';
    // $clientes = Clientes::find()->where(['>','montoTotalEjecutado', 0])->all();
    // $rcaja_m_venta_cuen_cobrar = '<center><h2><small>No se encontraron registros</small></h2></center>';
    // if (@$clientes) {
    //   $rcaja_m_venta_cuen_cobrar = $this->renderAjax('rcaja_m_venta_cuen_cobrar', [ ]);
    // }
    // echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Cuentas por cobrar</h3>';
    // echo '<div class="col-lg-12"><br>'.$rcaja_m_venta_cuen_cobrar.'</div>';
  }
  //>>>>>>>>>>>>>>>>>>>>>>>>>NOTAS DE DÉBITO A CLIENTES
  elseif ($tipo=='not_deb_cli'){
    $nombre_clientes = Clientes::find()
    ->innerJoin(['r'=>'tbl_recibos'],'r.cliente = tbl_clientes.idCliente')
    ->where(['tipoRecibo'=>'ND'])->all();
    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Notas de débito a Clientes</h3>';
    echo '<div class="col-lg-6 col-md-6">
    <label for="filtro_cliente_nd">Clientes Nota Débito</label>'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
      'name' => '',
      'data' => ArrayHelper::map($nombre_clientes,'idCliente',function($element) {
        return $element['idCliente'].' - '.$element['nombreCompleto'];
      }),
      'options' => [
        'placeholder' => '- Seleccione -',
        'id'=>'filtro_cliente_nd',
        'multiple' => true //esto me ayuda a que la busqueda sea multiple
      ],
      'pluginOptions' => ['initialize'=> true,'allowClear' => true]
      ]).'</div>';
      echo '<div class="col-lg-3 col-md-3">
      <label for="fecha_desde7">Fecha Inicio</label>
      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
      'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
      'id'=>'fecha_desde7','autocomplete'=>'off']]).'
      </div>
      <div class="col-lg-3 col-md-3">
      <label for="fecha_hasta7">Fecha Fin</label>
      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
      'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
      'id'=>'fecha_hasta7','autocomplete'=>'off']]).'
      </div>
      <div class="col-lg-6 col-md-6" style="margin-top:25px;">
      <a class="btn btn-default btn-sm" onclick="buscar_reporte_ndebito()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
      <a class="btn btn-default btn-sm" id="imp_re_nd" onclick="imprimir_reporte_ndebito()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
      </div>
      ';
      echo '<div class="col-lg-12 col-md-12"><br>
      <div class="well" id="mostrar_ndebito"></div>
      </div>';
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>NOTAS DE CRÉDITO A CLIENTES
    elseif ($tipo=='not_cre_cli') {
      echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Notas de Crédito a clientes</h3>';
      $nombre_clientes = Clientes::find()
      ->innerJoin(['r'=>'tbl_recibos'],'r.cliente = tbl_clientes.idCliente')
      ->where(['tipoRecibo'=>'NC'])->all();
      echo '<div class="col-lg-6 col-md-6">
      <label for="filtro_cliente_nc">Cliente Nota Crédito</label>'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
        'name' => '',
        'data' => ArrayHelper::map($nombre_clientes,'idCliente',function($element) {
          return $element['idCliente'].' - '.$element['nombreCompleto'];
        }),
        'options' => [
          'placeholder' => '- Seleccione los clientes a filtrar -',
          'id'=>'filtro_cliente_nc',
          'multiple' => true //esto me ayuda a que la busqueda sea multiple
        ],
        'pluginOptions' => ['initialize'=> true,'allowClear' => true]
        ]).'</div>';
        echo '<div class="col-lg-3 col-md-3">
        <label for="fecha_desde8">Fecha Inicio</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_desde8','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3">
        <label for="fecha_hasta8">Fecha Fin</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_hasta8','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-6 col-md-6" style="margin-top:25px;">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_ncredito()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_nc" onclick="imprimir_reporte_ncredito()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
        </div>
        ';
        echo '<div class="col-lg-12 col-md-12"><br>
        <div class="well" id="mostrar_ncredito"></div>
        </div>';
        echo '<div id="chart_div" ></div>';
      }
      //>>>>>>>>>>>>>>>>>>>>>>>>>PAGOS POR CAJA
      elseif ($tipo=='pago_x_caja') {
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Gastos</h3>';
        echo '<div class="col-lg-3 col-md-3">
        <label for="fecha_desde14">Fecha Inicio</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_desde14','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3">
        <label for="fecha_desde14">Fecha Fin</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_hasta14','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3" style="margin-top:25px;">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_pago_x_caja()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_ve_d" onclick="imprimir_reporte_pago_x_caja()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
        </div>
        ';
        echo '<div class="col-lg-12"><br>
        <div class="well" id="mostrar_pago_x_caja"></div>
        </div>';
      }
      //
      elseif ($tipo=='recepcion_general') {
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Por recepción general</h3>';
        echo '<div class="col-lg-3 col-md-3">
        <label for="fecha_desde_rec_gen">Fecha Inicio</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_desde_rec_gen','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3">
        <label for="fecha_hasta_rec_gen">Fecha Fin</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_hasta_rec_gen','autocomplete'=>'off']]).'
        </div>

        ';
        echo '<div class="col-lg-3 col-md-3">
        <label for="filtro_documento_electronico">Tipo de documento</label>'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
          'name' => '',
          'data' => ["'Factura electrónica'"=>'Factura electrónica',"'Tiquete Electrónico'"=>'Tiquete electrónico',"'Nota de crédito electrónica'"=>'Nota de crédito electronica',"'Nota de débito electrónica'"=>'Nota de débito electrónica'],
          'options' => [
            'placeholder' => '- Seleccione el tipo de documento -',
            'id'=>'filtro_documento_electronico',
            'multiple' => true //esto me ayuda a que la busqueda sea multiple
          ],
          'pluginOptions' => ['initialize'=> true,'allowClear' => true]
          ]).'</div>
          <div class="col-lg-3 col-md-3">
          <label for="filtro_tipo_resolucion">Tipo de resolución</label>'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
            'name' => '',
            'data' => ["'Aceptado'"=>'Aceptado',"'Parcialmente aceptado'"=>'Parcialmente aceptado',"'Rechazado'"=>'Rechazado'],
            'options' => [
              'placeholder' => '- Seleccione el tipo de resolución -',
              'id'=>'filtro_tipo_resolucion',
              'multiple' => true //esto me ayuda a que la busqueda sea multiple
            ],
            'pluginOptions' => ['initialize'=> true,'allowClear' => true]
            ]).'</div>
            <div class="col-lg-3 col-md-3"><br>
            <label for="filtro_respuesta_hacienda">Elija la respuesta de hacienda</label>'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
              'name' => '',
              'data' => ['01'=>'Aceptadas por hacienda','03'=>'Rechasadas por hacienda'],
              'options' => [
                'placeholder' => '- Seleccione respuesta de hacienda -',
                'id'=>'filtro_respuesta_hacienda',
                'multiple' => true //esto me ayuda a que la busqueda sea multiple
              ],
              'pluginOptions' => ['initialize'=> true,'allowClear' => true]
              ]).'</div>
          <div class="col-lg-3 col-md-3" style="margin-top:25px;">
          <a class="btn btn-default btn-sm" onclick="buscar_reporte_recepcion_general()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
          <a class="btn btn-default btn-sm" id="imp_re_re_ge" onclick="imprimir_reporte_recepcion_general()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
          </div>';
        echo '<div class="col-lg-12"><br>
        <div class="well" id="mostrar_recepcion_general"></div>
        </div>';
      }
      //>>>>>>>>>>>>>>>>>>>>>>>>>RESUMEN VENTAS GRAVADAS / EXENTAS
      elseif ($tipo=='res_vent_gr_ex') {
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen ventas Gravadas / Exentas</h3>';
        echo '<div class="col-lg-3 col-md-3">
        <label for="fecha_desde4">Fecha Inicio</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_desde4','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3">
        <label for="fecha_hasta4">Fecha Fin</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_hasta4','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3" style="margin-top:25px;">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_venta_gra_ex()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_ve_gr_ex" onclick="imprimir_reporte_venta_gra_ex()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
        </div>
        ';
        echo '<div class="col-lg-12"><br>
        <div class="well" id="mostrar_venta_gra_ex"></div>
        </div>';
      }
      //>>>>>>>>>>>>>>>>>>>>>>>>>VENTAS DETALLE
      elseif ($tipo=='vent_detalle') {
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Ventas Detalle</h3>';
        echo '<div class="col-lg-3 col-md-3">
        <label for="fecha_desde3">Fecha Inicio</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_desde3','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3">
        <label for="fecha_hasta3">Fecha fin</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_hasta3','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3" style="margin-top:25px;">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_venta_detalle()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_ve_d" onclick="imprimir_reporte_venta_detalle()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
        </div>
        ';
        echo '<div class="col-lg-12"><br>
        <div class="well" id="mostrar_venta_detalle"></div>
        </div>';
      }
      //>>>>>>>>>>>>>>>>>>>>>>>>>FACTURAS DESECHADAS
      elseif ($tipo=='desechadas') {
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Pre-facturas desechadas</h3>';
        echo '<div class="col-lg-3 col-md-3">
        <label for="fecha_desde10">Fecha Inicio</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_desde10','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3">
        <label for="fecha_hasta10">Fecha Inicio</label>
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
        'id'=>'fecha_hasta10','autocomplete'=>'off']]).'
        </div>
        <div class="col-lg-3 col-md-3" style="margin-top:25px;">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_desechadas()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_desech" onclick="imprimir_reporte_desechadas()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
        </div>
        ';
        echo '<div class="col-lg-12"><br>
        <div class="well" id="mostrar_desechadas"></div>
        </div>';
      }
      //>>>>>>>>>>>>>>>>>>>>>>>>>O.T
      elseif ($tipo=='ot_resumen') {
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Facturas O.T resumen</h3>';
        $clientes = Clientes::find()->where(['ot'=>'Si'])->all();
        //$rcaja_m_ot_resumen = '<center><h2><small>No se encontraron registros</small></h2></center>';
        echo '<div class="col-lg-10"> </div><div class="col-lg-2">
        <a class="btn btn-default btn-sm" id="imp_re_ot_resumen" onclick="imprimir_reporte_ot_resumen()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
        </div>';
        $rcaja_m_ot_resumen = $this->renderAjax('rcaja_m_ot_resumen', ['clientes'=>$clientes]);
        echo '<div class="col-lg-12"><br>'.$rcaja_m_ot_resumen.'</div>';
      }
      elseif ($tipo=='ot_detalle') {
        $clientesOt = Clientes::find()->where(['ot'=>'Si'])->all();
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Facturas O.T detalle</h3>';
        echo '<div class="col-lg-6 col-md-6">
        <label for="filtro_cliente_ot">Clientes O.T</label>
        '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
          'name' => '',
          'data' => ArrayHelper::map($clientesOt,'idCliente',function($element) {
            return $element['idCliente'].' - '.$element['nombreCompleto'];
          }),
          'options' => [
            'placeholder' => '- Seleccione -',
            'id'=>'filtro_cliente_ot',
            'multiple' => true //esto me ayuda a que la busqueda sea multiple
          ],
          'pluginOptions' => ['initialize'=> true,'allowClear' => true]
          ]).'</div>';
          echo '<div class="col-lg-3 col-md-3">
          <label for="fecha_desde11">Fecha Inicio</label>
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy','autocomplete'=>'off',
          'id'=>'fecha_desde11','autocomplete'=>'off']]).'
          </div>
          <div class="col-lg-3 col-md-3">
          <label for="fecha_desde11">Fecha Fin</label>
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy', 'autocomplete'=>'off',
          'id'=>'fecha_hasta11','autocomplete'=>'off']]).'
          </div>
          <div class="col-lg-6 col-md-6" style="margin-top:25px;">
          <a class="btn btn-default btn-sm" onclick="buscar_reporte_ot_detalle()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
          <a class="btn btn-default btn-sm" id="imp_re_ot_detalle" onclick="imprimir_reporte_ot_detalle()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
          </div>
          ';
          echo '<div class="col-lg-12"><br>
          <div class="well" id="mostrar_ot_detalle"></div>
          </div>';
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>D151
        elseif ($tipo=='D151') {
          echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Reporte D-151 Clientes</h3>';
          $anoActual = date('Y');
          $arrayAnnos = null;
          for($i=$anoActual;$i>=($anoActual-10);$i--){
            $arrayAnnos[$i]=$i;
            // $slec .= "<option value='".$i."'>".$i."</option>";
          }
          echo '<div class="col-lg-6 col-md-6">
          <label for="fecha_desde5">Selecione el año del periodo Fiscal a consultar: </label>
          '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
            'name' => '',
            'data' => $arrayAnnos,
            'options' => [
              'placeholder' => '- Seleccione -',
              'id'=>'fecha_desde5',
              'name'=>'fecha_desde5'
            ],
            'pluginOptions' => ['initialize'=> true,'allowClear' => true]
            ]).'</div>';

            echo '</div>
            <div class="col-lg-6 col-md-6" style="margin-top:25px;">
            <a class="btn btn-default btn-sm" onclick="buscar_reporte_d151()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
            <a class="btn btn-default btn-sm" id="imp_re_ve_d" onclick="imprimir_reporte_d151()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
            </div>
            ';
            echo '<div class="col-lg-12"><br>
            <div class="well" id="mostrar_d151"></div>
            </div>';
          }
          //>>>>>>>>>>>>>>>>>>>>>>>>>D151_detalle
          elseif ($tipo=='D151_detalle') {
            echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Reporte D-151 Detalle de Clientes</h3>';

            $anoActual = date('Y');
            $arrayAnnos = null;
            for($i=$anoActual;$i>=($anoActual-10);$i--){
              /*[2018=>2018,2017=>2017....2008=>2008]*/
              $arrayAnnos[$i]=$i;
            }
            echo '<div class="col-lg-6 col-md-6">
            <label for="filtro_anno_fiscal_d151_detalle">Año Fiscal</label>
            '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
              'name' => '',
              'data' => $arrayAnnos,
              'options' => [
                'placeholder' => '- Seleccione -',
                'id'=>'filtro_anno_fiscal_d151_detalle',
                'onchange'=>'javascript:cargarClientesPerido()',
                'name'=>'filtro_anno_fiscal_d151_detalle'
              ],
              'pluginOptions' => ['initialize'=> true,'allowClear' => true]
              ]).'</div>';
              /*vehiel 01-12-2018
              para obtimizar el select2 de clientes, mostraremos solo los posibles clientes a consultar,
              para esto hacemos uno consulta previa de e iremos sacando los clientes donde el monto a declarar sea mayor
              o igual a monto minimo impuesto por Hacienda*/
              /*la consulta es la misma que el reporte D151, nos trae el monto a declarar y el id del cliente*/

              $sql = "SELECT SUM(ef.subtotal-ef.porc_descuento) AS sumD151, ef.idCliente FROM `tbl_encabezado_factura` ef
              WHERE ef.idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
                (SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC'
                GROUP BY idCabeza_Factura) fact_not
                ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
                WHERE fact.total_a_pagar = fact_not.suma_nc)
                AND ef.fecha_inicio BETWEEN :fechaFactura1 AND :fechaFactura2 AND ef.estadoFactura !='Anulada'  GROUP BY ef.idCliente DESC
                HAVING sumD151 >=".Yii::$app->params['limite_monto_venta_MIN'].";";

                $anoPasado = date('Y', strtotime('-1 year'));
                $anoActual = date('Y');
                $fechaFactura1 = $anoPasado.'-10-01';
                $fechaFactura2 = $anoActual.'-09-30';
                $command = \Yii::$app->db->createCommand($sql);
                $command->bindParam(":fechaFactura1", $fechaFactura1);
                $command->bindParam(":fechaFactura2", $fechaFactura2);
                $facturas = $command->queryAll();
                $clientesArray = [];
                foreach ($facturas as $key => $factura) {
                  // if($factura['sumD151'] >= Yii::$app->params['limite_monto_venta_MIN']){
                  $clientesArray[]=$factura['idCliente'];
                  // }
                }
                $clientes = Clientes::find()->where(['IN','idCliente',$clientesArray])->all();
                echo '<div class="col-lg-6">
                <label id="label_filtro_cliente_agnt_d151_detalle" for="filtro_cliente_agnt_d151_detalle">Clientes 2018</label>
                '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                  'name' => '',
                  /*se cargan los clientes para el ultimo periodo fiscal*/
                  'data' => ArrayHelper::map($clientes,'idCliente',function($element) {
                    return $element['idCliente'].' - '.$element['nombreCompleto'];
                  }),
                  'options' => [
                    'placeholder' => '- Seleccione -',
                    'id'=>'filtro_cliente_agnt_d151_detalle',
                    'multiple' => true //esto me ayuda a que la busqueda sea multiple
                  ],
                  'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                  ]).'</div>';
                  echo '</div>
                  <div class="col-lg-6 col-md-6" style="margin-top:25px;">
                  <a class="btn btn-default btn-sm" onclick="buscar_reporte_d151_detalle()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                  <a class="btn btn-default btn-sm" id="imp_re_d151_detalle" onclick="imprimir_reporte_d151_detalle()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                  </div>
                  ';
                  echo '<div class="col-lg-12"><br>
                  <div class="well" id="mostrar_d151_detalle"></div>
                  </div>';
                }
                //>>>>>>>>>>>>>>>>>>>>>>>>>REPORTE DE RESUMEN DE COMICIONES
                elseif($tipo=='comisiones_resumen') {
                  $agenteComi = AgentesComisiones::find()->all();
                  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte Resumen de Comiciones</h3>';
                  echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                    'name' => '',
                    'data' => ArrayHelper::map($agenteComi,'idAgenteComision',function($element) {
                      return $element['idAgenteComision'].' - '.$element['nombre'];
                    }),
                    'options' => [
                      'placeholder' => '- Seleccione los clientes a filtrar -',
                      'id'=>'filtro_cliente_agnt',
                      'multiple' => true //esto me ayuda a que la busqueda sea multiple
                    ],
                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                    ]).'</div>';
                    echo '<div class="col-lg-2">
                    '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                    'options' => ['class'=>'form-control', 'placeholder'=>'FECH.DESDE dd-mm-yyyy',
                    'id'=>'fecha_desde12']]).'
                    </div>
                    <div class="col-lg-2">
                    '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                    'options' => ['class'=>'form-control', 'placeholder'=>'FECH.HASTA dd-mm-yyyy',
                    'id'=>'fecha_hasta12']]).'
                    </div>
                    <div class="col-lg-2">
                    <a class="btn btn-default btn-sm" onclick="buscar_reporte_resumen_comisiones()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                    <a class="btn btn-default btn-sm" id="imp_re_resumen_comisiones" onclick="imprimir_reporte_resumen_comisiones()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                    </div>
                    ';
                    echo '<div class="col-lg-12"><br>
                    <div class="well" id="mostrar_resumen_comisiones"></div>
                    </div>';
                  }

                  //>>>>>>>>>>>>>>>>>>>>>>>>>REPORTE DE DETALLE DE COMICIONES AGENTE
                  elseif($tipo=='comisiones_detalle') {
                    $agenteComi = AgentesComisiones::find()->all();
                    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte Detalle de Comiciones por Agente</h3>';
                    echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                      'name' => '',
                      'data' => ArrayHelper::map($agenteComi,'idAgenteComision',function($element) {
                        return $element['idAgenteComision'].' - '.$element['nombre'];
                      }),
                      'options' => [
                        'placeholder' => '- Seleccione los clientes a filtrar -',
                        'id'=>'filtro_cliente_agnt',
                        'multiple' => true //esto me ayuda a que la busqueda sea multiple
                      ],
                      'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                      ]).'</div>';
                      echo '<div class="col-lg-2">
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'FECH.DESDE dd-mm-yyyy',
                      'id'=>'fecha_desde13']]).'
                      </div>
                      <div class="col-lg-2">
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'FECH.HASTA dd-mm-yyyy',
                      'id'=>'fecha_hasta13']]).'
                      </div>
                      <div class="col-lg-2">
                      <a class="btn btn-default btn-sm" onclick="buscar_reporte_detalle_comisiones()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                      <a class="btn btn-default btn-sm" id="imp_re_detalle_comisiones" onclick="imprimir_reporte_detalle_comisiones()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                      </div>
                      ';
                      echo '<div class="col-lg-12"><br>
                      <div class="well" id="mostrar_detalle_comisiones"></div>
                      </div>';
                    }elseif($tipo=='apartado_lista') {
                      $clientes_apart = Clientes::find()
                      ->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
                      ->where(['estadoFactura'=>'Apartado'])
                      ->all();
                      $vendedor = Funcionario::find()
                      ->innerJoin(['tbl_encabezado_factura'], 'tbl_funcionario.idFuncionario = tbl_encabezado_factura.codigoVendedor')
                      ->where(['estadoFactura'=>'Apartado'])
                      ->all();
                      echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de apartados pendientes</h3>';
                      $ap_v = "$('#apart_al_dia').prop('checked', false)";
                      $ap_d = "$('#apart_vencidas').prop('checked', false)";
                      echo '<div class="col-lg-6">
                              <div class="col-lg-8">'.
                                Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                  'name' => '',
                                  'data' => ArrayHelper::map($clientes_apart,'idCliente',function($element) {
                                    return $element['idCliente'].' - '.$element['nombreCompleto'];
                                  }),
                                  'options' => [
                                    'placeholder' => '- Seleccione los clientes a filtrar -',
                                    'id'=>'filtro_cliente_apartado',
                                    'multiple' => true //esto me ayuda a que la busqueda sea multiple
                                  ],
                                  'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                                ]).
                              '</div>
                              <div class="col-lg-4"><br>
                                <input id="incluir_abono_apart" type="checkbox" name="" > Incluir detalle abonos
                              </div>
                              <div class="col-lg-7"><br>
                              '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                  'name' => '',
                                  'data' => ArrayHelper::map($vendedor,'idFuncionario',function($element) {
                                    return $element['idFuncionario'].' - '.$element['nombre'].' '.$element['apellido1'].' '.$element['apellido2'];
                                  }),
                                  'options' => [
                                    'placeholder' => '- Seleccione el Vendedor que efectuó -',
                                    'id'=>'filtro_vendedor_apartado',
                                    'multiple' => false //esto me ayuda a que la busqueda sea multiple
                                  ],
                                  'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                                ]).
                             '</div>
                              <div class="col-lg-5"><br>
                                <input id="apart_vencidas" type="checkbox" name="" onClick="'.$ap_v.'"> Incluir sólo apartados vencidos <br>
                                <input id="apart_al_dia" type="checkbox" name="" onClick="'.$ap_d.'"> Incluir sólo apartados al día
                              </div>
                            </div>';

                      echo '<div class="col-lg-3">Fecha de APARTADO desde:
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'DESDE dd-mm-yyyy',
                      'id'=>'fecha_desde_com_detalle']]).'
                      </div>
                      <div class="col-lg-3">Fecha de APARTADO hasta:
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'HASTA dd-mm-yyyy',
                      'id'=>'fecha_hasta_com_detalle']]).'
                      </div>
                      <div class="col-lg-3"><br>Fecha de ABONO DE APARTADO desde:
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'DESDE dd-mm-yyyy',
                      'id'=>'fecha_desde_com_detalle_ab']]).'
                      </div>
                      <div class="col-lg-3"><br>Fecha de ABONO DE APARTADO hasta:
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'HASTA dd-mm-yyyy',
                      'id'=>'fecha_hasta_com_detalle_ab']]).'
                      </div>
                      <div class="col-lg-12"><br>
                        <div class="pull-right">
                          <a class="btn btn-default btn-sm" onclick="buscar_reporte_apartado_lista()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                          <a class="btn btn-default btn-sm" id="imp_re_apartado_lista" onclick="imprimir_reporte_apartado_lista()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                				</div>
                      </div>
                      ';
                      echo '<div class="col-lg-12"><br>
                      <div class="well" id="mostrar_apartado_lista"></div>
                      </div>';
                    } elseif ($tipo=='apartado_cancelados') {
                      $clientes_apart = Clientes::find()
                      ->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
                      ->where(['apartado'=>'x'])
                      ->all();
                      echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de apartados cancelados</h3>';
                      echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                  'name' => '',
                                  'data' => ArrayHelper::map($clientes_apart,'idCliente',function($element) {
                                    return $element['idCliente'].' - '.$element['nombreCompleto'];
                                  }),
                                  'options' => [
                                    'placeholder' => '- Seleccione los clientes a filtrar -',
                                    'id'=>'filtro_cliente_cancelado',
                                    'multiple' => true //esto me ayuda a que la busqueda sea multiple
                                  ],
                                  'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                                ]).'</div>';
                      echo '<div class="col-lg-2">
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'DESDE dd-mm-yyyy',
                      'id'=>'fecha_desde_apart_cancel']]).'
                      </div>
                      <div class="col-lg-2">
                      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                      'options' => ['class'=>'form-control', 'placeholder'=>'HASTA dd-mm-yyyy',
                      'id'=>'fecha_hasta_apart_cancel']]).'
                      </div>
                      <div class="col-lg-2">
                      <a class="btn btn-default btn-sm" onclick="buscar_reporte_apartado_cancelado()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                      <a class="btn btn-default btn-sm" id="imp_re_apartado_cancelado" onclick="imprimir_reporte_apartado_cancelado()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                      </div>
                      ';
                      echo '<div class="col-lg-12"><br>
                      <div class="well" id="mostrar_apartado_cancelado"></div>
                      </div>';
                    }

                    ?>
                    <script type="text/javascript">

                    $(document).ready(function () {
                      //mantenemos todos los boton de imprimir desabilitado
                      $('#imp_re_ve_gr_ex').addClass('disabled');
                      $('#imp_re_ve_d').addClass('disabled');
                      $('#imp_re_abon').addClass('disabled');
                      $('#imp_re_nd').addClass('disabled');
                      $('#imp_re_nc').addClass('disabled');
                      $('#imp_re_ve_ba_ca').addClass('disabled');
                      $('#imp_re_desech').addClass('disabled');
                      $('#imp_re_ot_detalle').addClass('disabled');
                      $('#imp_re_resumen_comisiones').addClass('disabled');
                      $('#imp_re_detalle_comisiones').addClass('disabled');
                      $('#imp_re_d151_detalle').addClass('disabled');
                      $('#imp_re_cuen_cobrar').addClass('disabled');
                      $('#imp_re_apartado_lista').addClass('disabled');
                      $('#imp_re_apartado_cancelado').addClass('disabled');
                      $('#imp_re_re_ge').addClass('disabled');
                    });
                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR BALANCE DE CAJA
                    function buscar_reporte_venta_balance_caja() {
                      fecha_desde = document.getElementById('fecha_desde9').value;
                      fecha_hasta = document.getElementById('fecha_hasta9').value;
                      document.getElementById("mostrar_venta_bal_ca").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_venta_balance_caja') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_venta_bal_ca').html(data);
                        $('#imp_re_ve_ba_ca').removeClass('disabled');//abilitamos boton de imprimir
                      }).fail(function() {
                        document.getElementById("mostrar_venta_bal_ca").innerHTML = '<center><h2><small>Rango de fechas incorrecto, o balance inexistente.</small></h2></center>';
                      });
                    }

                    function imprimir_reporte_venta_balance_caja() {
                      fecha_desde = document.getElementById('fecha_desde9').value;
                      fecha_hasta = document.getElementById('fecha_hasta9').value;
                      $('#imp_re_ve_ba_ca').addClass('disabled');
                      $('#imp_re_ve_ba_ca').text('Por favor espere...');
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_venta_balance_caja') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#imp_re_ve_ba_ca').html('<span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir');
                        $('#imp_re_ve_ba_ca').removeClass('disabled');
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR ABONOS
                    function buscar_reporte_abonos() {
                      fecha_desde = document.getElementById('fecha_desde6').value;
                      fecha_hasta = document.getElementById('fecha_hasta6').value;
                      var filtro_cliente = $("#filtro_cliente_ab").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      document.getElementById("mostrar_abonos").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_venta_abonos') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_abonos').html(data);
                        $('#imp_re_abon').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    function imprimir_reporte_abonos() {
                      fecha_desde = document.getElementById('fecha_desde6').value;
                      fecha_hasta = document.getElementById('fecha_hasta6').value;
                      var filtro_cliente = $("#filtro_cliente_ab").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_venta_abonos') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }

                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR NOTAS DE DEBITO
                    function buscar_reporte_ndebito() {
                      fecha_desde = document.getElementById('fecha_desde7').value;
                      fecha_hasta = document.getElementById('fecha_hasta7').value;
                      var filtro_cliente = $("#filtro_cliente_nd").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      document.getElementById("mostrar_ndebito").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_venta_ndebito') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_ndebito').html(data);
                        $('#imp_re_nd').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    function imprimir_reporte_ndebito() {
                      fecha_desde = document.getElementById('fecha_desde7').value;
                      fecha_hasta = document.getElementById('fecha_hasta7').value;
                      var filtro_cliente = $("#filtro_cliente_nd").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_venta_ndebito') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }

                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR NOTAS DE CREDITO
                    function buscar_reporte_ncredito() {
                      fecha_desde = document.getElementById('fecha_desde8').value;
                      fecha_hasta = document.getElementById('fecha_hasta8').value;
                      var filtro_cliente = $("#filtro_cliente_nc").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      document.getElementById("mostrar_ncredito").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_venta_ncredito') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_ncredito').html(data);
                        $('#imp_re_nc').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    function imprimir_reporte_ncredito() {
                      fecha_desde = document.getElementById('fecha_desde8').value;
                      fecha_hasta = document.getElementById('fecha_hasta8').value;
                      var filtro_cliente = $("#filtro_cliente_nc").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_venta_ncredito') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR PAGOS POR CAJA
                    /*Este reporte muestra los pagos por caja*/
                    function buscar_reporte_pago_x_caja() {
                      fecha_desde = document.getElementById('fecha_desde14').value;
                      fecha_hasta = document.getElementById('fecha_hasta14').value;
                      document.getElementById("mostrar_pago_x_caja").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_pagos_x_caja') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_pago_x_caja').html(data);
                        $('#imp_re_ve_d').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }
                    /*Este reporte imprime los pagos por caja*/
                    function imprimir_reporte_pago_x_caja() {
                      fecha_desde = document.getElementById('fecha_desde14').value;
                      fecha_hasta = document.getElementById('fecha_hasta14').value;
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_pagos_x_caja') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR LAS RECEPCIONES GENERALES
                    function buscar_reporte_recepcion_general() {
                      fecha_desde = document.getElementById('fecha_desde_rec_gen').value;
                      fecha_hasta = document.getElementById('fecha_hasta_rec_gen').value;
                      var filtro_documento = $("#filtro_documento_electronico").val();
                      var filtro_resolucion = $("#filtro_tipo_resolucion").val();
                      var filtro_respuesta = $("#filtro_respuesta_hacienda").val();

                      var lista_documento_electronico = lista_resolucion = lista_respuesta = '0';
                      if (filtro_documento==null) { lista_documento_electronico = '0'; }
                      else { lista_documento_electronico = filtro_documento.toString(); }
                      if (filtro_resolucion==null) { lista_resolucion = '0'; }
                      else { lista_resolucion = filtro_resolucion.toString(); }
                      if (filtro_respuesta==null) { lista_respuesta = '0'; }
                      else { lista_respuesta = filtro_respuesta.toString(); }
                      document.getElementById("mostrar_recepcion_general").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_recepcion_hacienda_ge') ?>" ,
                      {
                        'lista_documento_electronico' : lista_documento_electronico,
                        'fecha_desde' : fecha_desde,
                        'fecha_hasta' : fecha_hasta,
                        'lista_resolucion' : lista_resolucion,
                        'lista_respuesta' : lista_respuesta
                      } ,
                      function( data ) {
                        $('#mostrar_recepcion_general').html(data);
                        $('#imp_re_re_ge').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    function imprimir_reporte_recepcion_general() {
                      fecha_desde = document.getElementById('fecha_desde_rec_gen').value;
                      fecha_hasta = document.getElementById('fecha_hasta_rec_gen').value;
                      var filtro_documento = $("#filtro_documento_electronico").val();
                      var filtro_resolucion = $("#filtro_tipo_resolucion").val();
                      var filtro_respuesta = $("#filtro_respuesta_hacienda").val();

                      var lista_documento_electronico = lista_resolucion = lista_respuesta = '0';
                      if (filtro_documento==null) { lista_documento_electronico = '0'; }
                      else { lista_documento_electronico = filtro_documento.toString(); }
                      if (filtro_resolucion==null) { lista_resolucion = '0'; }
                      else { lista_resolucion = filtro_resolucion.toString(); }
                      if (filtro_respuesta==null) { lista_respuesta = '0'; }
                      else { lista_respuesta = filtro_respuesta.toString(); }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_recepcion_hacienda_ge') ?>" ,
                      {
                        'lista_documento_electronico' : lista_documento_electronico,
                        'fecha_desde' : fecha_desde,
                        'fecha_hasta' : fecha_hasta,
                        'lista_resolucion' : lista_resolucion,
                        'lista_respuesta' : lista_respuesta
                      } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA BUSCAR E IMPRIMIR CUENTAS POR COBRAR DE TODOS LOS CLIENTES
                    function buscar_reporte_venta_cuen_cobrar() {
                      var filtro_clientes = $("#filtro_cliente_venta_cuen_cobrar").val();
                      var facturas_vencidas = 'no';
                      if( $('#facturas_vencidas').is(':checked') ) { facturas_vencidas = 'si'; }
                      var lista_clientes = '0';
                      if (filtro_clientes==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_clientes.toString();
                      }
                      document.getElementById("mostrar_cuen_cobrar").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_venta_cuen_cobrar') ?>" ,
                      {'lista_clientes':lista_clientes, 'facturas_vencidas':facturas_vencidas},
                      function( data ) {
                        $('#mostrar_cuen_cobrar').html(data);
                        $('#imp_re_cuen_cobrar').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }
                    function imprimir_reporte_venta_cuen_cobrar() {
                      var filtro_clientes = $("#filtro_cliente_venta_cuen_cobrar").val();
                      var facturas_vencidas = 'no';
                      if( $('#facturas_vencidas').is(':checked') ) { facturas_vencidas = 'si'; }
                      var lista_clientes = '0';
                      if (filtro_clientes==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_clientes.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_venta_cuen_cobrar') ?>" ,
                      {'lista_clientes':lista_clientes, 'facturas_vencidas':facturas_vencidas},
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }

                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR RESUMEN VENTA GRAVADAS / EXENTAS
                    function buscar_reporte_venta_gra_ex() {
                      fecha_desde = document.getElementById('fecha_desde4').value;
                      fecha_hasta = document.getElementById('fecha_hasta4').value;
                      document.getElementById("mostrar_venta_gra_ex").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_venta_gra_ex') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_venta_gra_ex').html(data);
                        $('#imp_re_ve_gr_ex').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    function imprimir_reporte_venta_gra_ex() {
                      fecha_desde = document.getElementById('fecha_desde4').value;
                      fecha_hasta = document.getElementById('fecha_hasta4').value;
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_venta_gra_ex') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }

                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR VENTAS DETALLE
                    /*Este reporte muestra las ventas detalle*/
                    function buscar_reporte_venta_detalle() {
                      fecha_desde = document.getElementById('fecha_desde3').value;
                      fecha_hasta = document.getElementById('fecha_hasta3').value;
                      document.getElementById("mostrar_venta_detalle").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_venta_detalle') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_venta_detalle').html(data);
                        $('#imp_re_ve_d').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }
                    /*Este reporte imprime las venta detalles*/
                    function imprimir_reporte_venta_detalle() {
                      fecha_desde = document.getElementById('fecha_desde3').value;
                      fecha_hasta = document.getElementById('fecha_hasta3').value;
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_venta_detalle') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR FACTURAS DESECHADAS




                    /*Este reporte muestra prefacturas desechadas*/
                    function buscar_reporte_desechadas() {
                      fecha_desde = document.getElementById('fecha_desde10').value;
                      fecha_hasta = document.getElementById('fecha_hasta10').value;
                      document.getElementById("mostrar_desechadas").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_desechadas') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_desechadas').html(data);
                        $('#imp_re_desech').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }
                    /*Este reporte imprime prefacturas desechadas*/
                    function imprimir_reporte_desechadas() {
                      fecha_desde = document.getElementById('fecha_desde10').value;
                      fecha_hasta = document.getElementById('fecha_hasta10').value;
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_desechadas') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR FACTURAS OT RESUMEN
                    function imprimir_reporte_ot_resumen() {
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_ot_resumen') ?>" ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR FACTURAS OT DETALLE

                    /*Este reporte muestra prefacturas desechadas*/
                    function buscar_reporte_ot_detalle() {
                      fecha_desde = document.getElementById('fecha_desde11').value;
                      fecha_hasta = document.getElementById('fecha_hasta11').value;
                      var filtro_cliente_ot = $("#filtro_cliente_ot").val();
                      var lista_cliente_ot = '0';
                      if (filtro_cliente_ot==null) {
                        lista_cliente_ot = '0';
                      } else {
                        lista_cliente_ot = filtro_cliente_ot.toString();
                      }
                      document.getElementById("mostrar_ot_detalle").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_ot_detalle') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta,'lista_cliente_ot':lista_cliente_ot },
                      function( data ) {
                        $('#mostrar_ot_detalle').html(data);
                        $('#imp_re_ot_detalle').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }
                    /*Este reporte imprime prefacturas desechadas*/
                    function imprimir_reporte_ot_detalle() {
                      fecha_desde = document.getElementById('fecha_desde11').value;
                      fecha_hasta = document.getElementById('fecha_hasta11').value;
                      var filtro_cliente_ot = $("#filtro_cliente_ot").val();
                      var lista_cliente_ot = '0';
                      if (filtro_cliente_ot==null) {
                        lista_cliente_ot = '0';
                      } else {
                        lista_cliente_ot = filtro_cliente_ot.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_ot_detalle') ?>" ,
                      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta,'lista_cliente_ot':lista_cliente_ot },
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }

                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR REPORTE D151
                    /*Este reporte muestra D151*/
                    function buscar_reporte_d151() {
                      anoSeleccionado = document.getElementById('fecha_desde5').value;
                      /*vehiel 30-11-2018
                      se hace esta validación por el cambio que se hizo del select a select2
                      si no se selecciona nada, el valor sera '0', con esto se valida donde hace la consulta*/
                      if (anoSeleccionado=='') {
                        anoSeleccionado = '0';
                      }
                      document.getElementById("mostrar_d151").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_d151') ?>" ,
                      {   'anoSeleccionado' : anoSeleccionado} ,
                      function( data ) {
                        $('#mostrar_d151').html(data);
                        $('#imp_re_ve_d').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    /*Este reporte imprime las venta detalles*/
                    function imprimir_reporte_d151() {
                      anoSeleccionado = document.getElementById('fecha_desde5').value;
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_d151') ?>" ,
                      {   'anoSeleccionado' : anoSeleccionado} ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }


                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR REPORTE D151 DETALLE
                    /*Este reporte muestra D151 detalle*/
                    function buscar_reporte_d151_detalle() {
                      anoSeleccionado = document.getElementById('filtro_anno_fiscal_d151_detalle').value;
                      if (anoSeleccionado=='') {
                        anoSeleccionado = '0';
                      }
                      var filtro_clientes = $("#filtro_cliente_agnt_d151_detalle").val();
                      var lista_clientes = '0';
                      if (filtro_clientes==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_clientes.toString();
                      }
                      document.getElementById("mostrar_d151_detalle").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_d151_detalle') ?>" ,
                      {'anoSeleccionado' : anoSeleccionado,'lista_clientes':lista_clientes},
                      function( data ) {
                        $('#mostrar_d151_detalle').html(data);
                        $('#imp_re_d151_detalle').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    /*Este reporte imprime las venta detalles*/
                    function imprimir_reporte_d151_detalle() {
                      $('#imp_re_d151_detalle').addClass('disabled');
                      $('#imp_re_d151_detalle').text("Por favor espere...");
                      anoSeleccionado = document.getElementById('filtro_anno_fiscal_d151_detalle').value;
                      if (anoSeleccionado=='') {
                        anoSeleccionado = '0';
                      }
                      var filtro_clientes = $("#filtro_cliente_agnt_d151_detalle").val();
                      var lista_clientes = '0';
                      if (filtro_clientes==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_clientes.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_d151_detalle') ?>" ,
                      {'anoSeleccionado' : anoSeleccionado,'lista_clientes':lista_clientes},
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                        $('#imp_re_d151_detalle').removeClass('disabled');
                        $('#imp_re_d151_detalle').html('<span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir');
                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }
                    //==============FUNCIONES PARA MOSTRAR E RESUMEN COMISIONES
                    function buscar_reporte_resumen_comisiones() {
                      fecha_desde = document.getElementById('fecha_desde12').value;
                      fecha_hasta = document.getElementById('fecha_hasta12').value;
                      var filtro_cliente = $("#filtro_cliente_agnt").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      document.getElementById("mostrar_resumen_comisiones").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_resumen_comisiones') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_resumen_comisiones').html(data);
                        $('#imp_re_resumen_comisiones').removeClass('disabled');//abilitamos boton de imprimir
                      }).fail(function() {
                        document.getElementById("mostrar_resumen_comisiones").innerHTML = '<center><h2><small>Rango de fechas incorrecto, o comisiones inexistentes.</small></h2></center>';
                      });
                    }

                    function imprimir_reporte_resumen_comisiones() {
                      fecha_desde = document.getElementById('fecha_desde12').value;
                      fecha_hasta = document.getElementById('fecha_hasta12').value;
                      var filtro_cliente = $("#filtro_cliente_agnt").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_resumen_comisiones') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }


                    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR DETALLE COMISIONES
                    function buscar_reporte_detalle_comisiones() {
                      fecha_desde = document.getElementById('fecha_desde13').value;
                      fecha_hasta = document.getElementById('fecha_hasta13').value;
                      var filtro_cliente = $("#filtro_cliente_agnt").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      document.getElementById("mostrar_detalle_comisiones").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_detalle_comisiones') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        $('#mostrar_detalle_comisiones').html(data);
                        $('#imp_re_detalle_comisiones').removeClass('disabled');//abilitamos boton de imprimir
                      }).fail(function() {
                        document.getElementById("mostrar_detalle_comisiones").innerHTML = '<center><h2><small>Rango de fechas incorrecto, o comisiones inexistentes.</small></h2></center>';
                      });
                    }

                    function imprimir_reporte_detalle_comisiones() {
                      fecha_desde = document.getElementById('fecha_desde13').value;
                      fecha_hasta = document.getElementById('fecha_hasta13').value;
                      var filtro_cliente = $("#filtro_cliente_agnt").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_detalle_comisiones') ?>" ,
                      { 'lista_clientes' : lista_clientes, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }

                    function cargarClientesPerido() {
                      anno = document.getElementById('filtro_anno_fiscal_d151_detalle').value;
                      if(anno == ''){
                        anno = <?= date('Y')?>
                      }
                      $('#label_filtro_cliente_agnt_d151_detalle').text("Clientes "+anno);
                      $('#filtro_cliente_agnt_d151_detalle').html('<option value="0">Por favor espere...</option>');
                      $.ajax({
                        url:"?r=reporte/cargar-cliente-periodo",
                        type:"get",
                        data: { 'anno' : anno},
                        success: function(valores){
                          // alert("success");
                          $('#filtro_cliente_agnt_d151_detalle').html(valores).fadeIn();
                        },
                        error:function(){
                          $('#filtro_cliente_agnt_d151_detalle').html('<option value="0">Ocurrio un error</option>');
                        }
                      });
                    }

                    /*Este reporte muestra prefacturas desechadas*/
                    function buscar_reporte_apartado_lista() {
                      var filtro_cliente = $("#filtro_cliente_apartado").val();
                      var filtro_vendedor_apartado = $("#filtro_vendedor_apartado").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      incluir_abono_apart = 'no';
                      apart_vencidas = 'no';
                      apart_al_dia = 'no';
                      if( $('#incluir_abono_apart').is(':checked') ) { incluir_abono_apart = 'si'; }
                      if( $('#apart_vencidas').is(':checked') ) { apart_vencidas = 'si'; }
                      if( $('#apart_al_dia').is(':checked') ) { apart_al_dia = 'si'; }
                      fecha_desde = document.getElementById('fecha_desde_com_detalle').value;
                      fecha_hasta = document.getElementById('fecha_hasta_com_detalle').value;
                      fecha_desde_ab = document.getElementById('fecha_desde_com_detalle_ab').value;
                      fecha_hasta_ab = document.getElementById('fecha_hasta_com_detalle_ab').value;
                      document.getElementById("mostrar_apartado_lista").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_apartado_lista') ?>" ,
                      {
                        'filtro_vendedor_apartado' : filtro_vendedor_apartado,
                        'incluir_abono_apart' : incluir_abono_apart,
                        'apart_vencidas' : apart_vencidas,
                        'apart_al_dia' : apart_al_dia,
                        'lista_clientes' : lista_clientes,
                        'fecha_desde' : fecha_desde,
                        'fecha_hasta' : fecha_hasta,
                        'fecha_desde_ab' : fecha_desde_ab,
                        'fecha_hasta_ab' : fecha_hasta_ab
                      } ,
                      function( data ) {
                        $('#mostrar_apartado_lista').html(data);
                        $('#imp_re_apartado_lista').removeClass('disabled');//abilitamos boton de imprimir
                      });
                    }

                    function imprimir_reporte_apartado_lista() {
                      var filtro_cliente = $("#filtro_cliente_apartado").val();
                      var filtro_vendedor_apartado = $("#filtro_vendedor_apartado").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      incluir_abono_apart = 'no';
                      apart_vencidas = 'no';
                      apart_al_dia = 'no';
                      if( $('#incluir_abono_apart').is(':checked') ) { incluir_abono_apart = 'si'; }
                      if( $('#apart_vencidas').is(':checked') ) { apart_vencidas = 'si'; }
                      if( $('#apart_al_dia').is(':checked') ) { apart_al_dia = 'si'; }
                      fecha_desde = document.getElementById('fecha_desde_com_detalle').value;
                      fecha_hasta = document.getElementById('fecha_hasta_com_detalle').value;
                      fecha_desde_ab = document.getElementById('fecha_desde_com_detalle_ab').value;
                      fecha_hasta_ab = document.getElementById('fecha_hasta_com_detalle_ab').value;
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icaja_m_apartado_lista') ?>" ,
                      {
                        'filtro_vendedor_apartado' : filtro_vendedor_apartado,
                        'incluir_abono_apart' : incluir_abono_apart,
                        'apart_vencidas' : apart_vencidas,
                        'apart_al_dia' : apart_al_dia,
                        'lista_clientes' : lista_clientes,
                        'fecha_desde' : fecha_desde,
                        'fecha_hasta' : fecha_hasta,
                        'fecha_desde_ab' : fecha_desde_ab,
                        'fecha_hasta_ab' : fecha_hasta_ab
                      } ,
                      function( data ) {
                        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                        ventimp.document.write(data);
                        ventimp.document.close();
                        ventimp.print();
                        ventimp.close();
                      });
                    }

                    function buscar_reporte_apartado_cancelado() {
                      var filtro_cliente = $("#filtro_cliente_cancelado").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      fecha_desde = document.getElementById('fecha_desde_apart_cancel').value;
                      fecha_hasta = document.getElementById('fecha_hasta_apart_cancel').value;
                      document.getElementById("mostrar_apartado_cancelado").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_apartado_cancelado') ?>" ,
                        {
                          'lista_clientes' : lista_clientes,
                          'fecha_desde' : fecha_desde,
                          'fecha_hasta' : fecha_hasta
                        },
                        function( data ) {
                          $('#mostrar_apartado_cancelado').html(data);
                          $('#imp_re_apartado_cancelado').removeClass('disabled');//abilitamos boton de imprimir
                        });
                    }

                    function imprimir_reporte_apartado_cancelado() {
                      var filtro_cliente = $("#filtro_cliente_cancelado").val();
                      var lista_clientes = '0';
                      if (filtro_cliente==null) {
                        lista_clientes = '0';
                      } else {
                        lista_clientes = filtro_cliente.toString();
                      }
                      fecha_desde = document.getElementById('fecha_desde_apart_cancel').value;
                      fecha_hasta = document.getElementById('fecha_hasta_apart_cancel').value;
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_m_apartado_cancelado') ?>" ,
                      {
                        'lista_clientes' : lista_clientes,
                        'fecha_desde' : fecha_desde,
                        'fecha_hasta' : fecha_hasta
                        } ,
                        function( data ) {
                          var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                          ventimp.document.write(data);
                          ventimp.document.close();
                          ventimp.print();
                          ventimp.close();
                      });
                    }
                    </script>
                    <!--script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <script type="text/javascript">
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawVisualization);

                    function drawVisualization() {
                    // Some raw data (not necessarily accurate)
                    var data = google.visualization.arrayToDataTable([
                    ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
                    ['2004/05',  165,      938,         522,             998,           450,      614.6],
                    ['2005/06',  135,      1120,        599,             1268,          288,      682],
                    ['2006/07',  157,      1167,        587,             807,           397,      623],
                    ['2007/08',  139,      1110,        615,             968,           215,      609.4],
                    ['2008/09',  136,      691,         629,             1026,          366,      569.6]
                    ]);

                    var options = {
                    title : 'Monthly Coffee Production by Country',
                    vAxis: {title: 'Cups'},
                    hAxis: {title: 'Month'},
                    seriesType: 'bars',
                    series: {5: {type: 'line'}}
                  };

                  var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                  chart.draw(data, options);
                }
              </script-->
