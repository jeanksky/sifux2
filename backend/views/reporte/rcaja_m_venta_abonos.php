<?php
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\MovimientoCobrar;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\MedioPago;
use backend\models\FormasPago;

$empresaimagen = new Empresa();
$fechaAbonos1 = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
$fechaAbonos2 = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';
$clientes = Clientes::find()->all();
if ($lista_clientes != '0') {
  $clientes = Clientes::find()->where("idCliente IN (".$lista_clientes.")", [])->all();

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y h:i:s A',strtotime($fechaAbonos1))." <strong>Hasta: </strong>".Date('d-m-Y h:i:s A',strtotime($fechaAbonos2)).'</p>
      </span>';

      $abono_efectivo = $abono_deposito = $abono_tarjeta = $abono_cheque = $cancel_efectivo = $cancel_deposito = $cancel_tarjeta = $cancel_cheque = 0;
echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';

echo '<thead class="thead-inverse">
        <tr>
          <th style="text-align:center"><font face="arial" size=3>Cliente</font></th>
          <th style="text-align:center"><font face="arial" size=2>Fecha / Hora</font></th>
          <th style="text-align:center"><font face="arial" size=2>ABO - REC</font></th>
          <th style="text-align:center"><font face="arial" size=2>Factura</font></th>
          <th style="text-align:right"><font face="arial" size=2>Monto anterior</font></th>
          <th style="text-align:right"><font face="arial" size=2>Monto abono</font></th>
          <th style="text-align:right"><font face="arial" size=2>Saldo</font></th>
          <th style="text-align:right"><font face="arial" size=2>Método de pago</font></th>
        </tr>
      </thead>
      <tbody class="">';
    foreach ($clientes as $key => $cliente) {
      $rowspan = 2; $monto_abono = 0;
      $ab_efectivo = $ab_deposito = $ab_tarjeta = $ab_cheque = $ca_efectivo = $ca_deposito = $ca_tarjeta = $ca_cheque = 0;
      $abonos = Recibos::find()->where(['IN','tipoRecibo',['AB','CF']])
      ->andWhere(['=','cliente',$cliente->idCliente])
      ->andWhere("fechaRecibo BETWEEN :fechaAbonos1 AND :fechaAbonos2", [':fechaAbonos1'=>$fechaAbonos1, ':fechaAbonos2'=>$fechaAbonos2])
      ->orderBy(['idRecibo' => SORT_ASC])->all();
      foreach ($abonos as $key => $abono) {

          // if ($cliente->idCliente==$abono->cliente) {
            $rowspan += 1; $monto_abono += $abono->monto;
            if ($abono->tipoRecibo == 'AB') {//recorremos los abojos parciales
            /*$rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$abono->idRecibo])->one();
            $movimiento = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();*/
            $movimiento = RecibosDetalle::find()->select(['mc.idMetPago','mc.monto_movimiento'])
            ->innerJoin(['mc'=>'tbl_movi_cobrar'],'idMov = idDocumento')
            ->where(['idRecibo'=>$abono->idRecibo])->asArray()->one();
            if($movimiento){
              $forma_pago_ab = '';
            if ($movimiento['idMetPago'] == null) {
              $forma_pago_ab = 'Efectivo';
            } else {
              $mediopago = MedioPago::find()->where(['idMetPago'=>$movimiento['idMetPago']])->one();
              $forma_pago = FormasPago::find()->where(['id_forma'=>$mediopago->idForma_Pago])->one();
              $forma_pago_ab = $forma_pago->desc_forma;
            }
            if ($forma_pago_ab == 'Efectivo') {
              $ab_efectivo += $movimiento['monto_movimiento'];
            } elseif ($forma_pago_ab == 'Depósito') {
              $ab_deposito += $movimiento['monto_movimiento'];
            } elseif ($forma_pago_ab == 'Tarjeta') {
              $ab_tarjeta += $movimiento['monto_movimiento'];
            } elseif ($forma_pago_ab == 'Cheque') {
              $ab_cheque += $movimiento['monto_movimiento'];
            }
          }
          }
          if($abono->tipoRecibo == 'CF') {//recorremos las cancelaciones
            /*$mediopago = MedioPago::find()->where(['idCabeza_factura'=>$abono->idRecibo])->one();
            $forma_pago = FormasPago::find()->where(['id_forma'=>$mediopago->idForma_Pago])->one();
            $forma_pago_ab = $forma_pago->desc_forma;*/
            $mediopago = MedioPago::find()->select(['fp.desc_forma'])
            ->innerJoin(['fp'=>'tbl_formas_pago'],'fp.id_forma = tbl_medio_pago.idForma_Pago')
            ->where(['idCabeza_factura'=>$abono->idRecibo])->asArray()->one();
            if($mediopago){
            $forma_pago_ab = $mediopago['desc_forma'];
            if ($forma_pago_ab == 'Efectivo') {
              $ca_efectivo += $abono->monto;
            } elseif ($forma_pago_ab == 'Depósito') {
              $ca_deposito += $abono->monto;
            } elseif ($forma_pago_ab == 'Tarjeta') {
              $ca_tarjeta += $abono->monto;
            } elseif ($forma_pago_ab == 'Cheque') {
              $ca_cheque += $abono->monto;
            }
          }/* fin if if($mediopago)*/
        } /* fin if($abono->tipoRecibo == 'CF')*/
        // }/* fin cliente == cliente abono*/
      }
        if ($monto_abono > 0) {
        $total_ab = $ab_efectivo + $ab_deposito + $ab_tarjeta + $ab_cheque;
        $total_ca = $ca_efectivo + $ca_deposito + $ca_tarjeta + $ca_cheque;
        $cliente_turno = '<strong>'.$cliente->nombreCompleto .  '</strong>
        <p style="border-bottom: 1px solid Turquoise;">Efectivo:<span style="float:right">'.number_format($ab_efectivo+$ca_efectivo,2).'</span></p>
        <p style="border-bottom: 1px solid Turquoise;">Depósito:<span style="float:right">'.number_format($ab_deposito+$ca_deposito,2).'</span></p>
        <p style="border-bottom: 1px solid Turquoise;">Tarjetas:<span style="float:right">'.number_format($ab_tarjeta+$ca_tarjeta,2).'</span></p>
        <p style="border-bottom: 2px solid #708090;">Cheques:<span style="float:right">'.number_format($ab_cheque+$ca_cheque,2).'</span></p>
        <span style="float:right">Total: '.number_format($total_ab+$total_ca,2).'</span>';

        // echo '<tr><td colspan="9" bgcolor="#7FFFD4"></td></tr>';

        echo '<tr><td rowspan='.$rowspan.' ><font size=4>'.$cliente_turno.'</font></td><tr>';
        foreach ($abonos as $key => $abono) {

          // if ($cliente->idCliente==$abono->cliente) {/*2do if de cliente*/
          if ($abono->tipoRecibo == 'AB') {/* 2do if ab*/
          /*$rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$abono->idRecibo])->one();
          $movimiento = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();*/
          $movimiento = RecibosDetalle::find()->select(['mc.idMetPago','mc.monto_movimiento','mc.fecmov',
          'mc.idMov','mc.idCabeza_Factura','mc.monto_anterior','mc.monto_movimiento','mc.saldo_pendiente'])
          ->innerJoin(['mc'=>'tbl_movi_cobrar'],'idMov = idDocumento')
          ->where(['idRecibo'=>$abono->idRecibo])->asArray()->one();
          if($movimiento){
          $forma_pago_ab = '';
          if ($movimiento['idMetPago'] == null) {
            $forma_pago_ab = 'Efectivo';
          } else {
            $mediopago = MedioPago::find()->where(['idMetPago'=>$movimiento['idMetPago']])->one();
            $forma_pago = FormasPago::find()->where(['id_forma'=>$mediopago->idForma_Pago])->one();
            $forma_pago_ab = $forma_pago->desc_forma;
          }
          if ($forma_pago_ab == 'Efectivo') {
            $abono_efectivo += $movimiento['monto_movimiento'];
          } elseif ($forma_pago_ab == 'Depósito') {
            $abono_deposito += $movimiento['monto_movimiento'];
          } elseif ($forma_pago_ab == 'Tarjeta') {
            $abono_tarjeta += $movimiento['monto_movimiento'];
          } elseif ($forma_pago_ab == 'Cheque') {
            $abono_cheque += $movimiento['monto_movimiento'];
          }
          $fecha = date('d-m-Y / h:i:s A', strtotime( $movimiento['fecmov'] ));
          printf('<tr>
                    <td align="center" ><font size=2>%s</font></td>
          		 		  <td align="center" ><font size=2>%s</font></td>
          	    		<td align="center"><font size=2>%s</font></td>
          	    		<td align="right"><font size=2>%s</font></td>
          	    		<td align="right"><font size=2>%s</font></td>
          	    		<td align="right"><font size=2>%s</font></td>
                    <td align="center" ><font size=2>%s</font></td>
              		</tr>',
                        $fecha,
                        $movimiento['idMov'].' - '.$abono->idRecibo,
                        $movimiento['idCabeza_Factura'],
                        number_format($movimiento['monto_anterior'],2),
                        '<strong>'.number_format($movimiento['monto_movimiento'],2).'</strong>',
                        number_format($movimiento['saldo_pendiente'],2),
                        $forma_pago_ab
                    );
                  }/*if($movimiento)*/
          } /*fin 2do if ($abono->tipoRecibo == 'AB')*/
          if($abono->tipoRecibo == 'CF') {
            $fecha = date('d-m-Y / h:i:s A', strtotime( $abono->fechaRecibo ));
            /*$mediopago = MedioPago::find()->where(['idCabeza_factura'=>$abono->idRecibo])->one();
            $forma_pago = FormasPago::find()->where(['id_forma'=>$mediopago->idForma_Pago])->one();*/
            $mediopago = MedioPago::find()->select(['fp.desc_forma'])
            ->innerJoin(['fp'=>'tbl_formas_pago'],'fp.id_forma = tbl_medio_pago.idForma_Pago')
            ->where(['idCabeza_factura'=>$abono->idRecibo])->asArray()->one();
            if($mediopago){
            $forma_pago_ab = $mediopago['desc_forma'];
            if ($forma_pago_ab == 'Efectivo') {
              $cancel_efectivo += $abono->monto;
            } elseif ($forma_pago_ab == 'Depósito') {
              $cancel_deposito += $abono->monto;
            } elseif ($forma_pago_ab == 'Tarjeta') {
              $cancel_tarjeta += $abono->monto;
            } elseif ($forma_pago_ab == 'Cheque') {
              $cancel_cheque += $abono->monto;
            }
            printf('<tr>
                      <td align="center" ><font size=2>%s</font></td>
            		 		  <td align="center" ><font size=2>%s</font></td>
            	    		<td align="center"><font size=2>%s</font></td>
            	    		<td align="right"><font size=2>%s</font></td>
            	    		<td align="right"><font size=2>%s</font></td>
            	    		<td align="right"><font size=2>%s</font></td>
                      <td align="center" ><font size=2>%s</font></td>
                		</tr>',
                          $fecha,
                          '**** - '.$abono->idRecibo,
                          '',//$movimiento->idCabeza_Factura,
                          '(CANCELACIÓN)',//number_format($movimiento->monto_anterior,2),
                          '<strong>'.number_format($abono->monto,2).'</strong>',
                          '',//number_format($movimiento->saldo_pendiente,2),
                          $forma_pago_ab
                      );
                    }/*fin if($mediopago)*/
          }
        // }/* 2do if ($cliente->idCliente==$abono->cliente)*/
        }
      }
    }
echo "</tbody> ";
$suma_abono_total = $abono_efectivo + $abono_deposito + $abono_tarjeta + $abono_cheque + $cancel_efectivo + $cancel_deposito + $cancel_tarjeta + $cancel_cheque;
echo '<tfoot>
        <tr>
          <td style="text-align:right" ><font size=5>Desglose total de ingresos por abonos:</font></td>
          <td style="text-align:left" >
            <font size=4>Efectivo:<br>Depósito:<br>Tarjetas:<br>Cheques:</font>
          </td>
          <td style="text-align:right">
          <font size=4>
          '.number_format($abono_efectivo + $cancel_efectivo,2).'<br>'.number_format($abono_deposito + $cancel_deposito,2).'<br>'.number_format($abono_tarjeta + $cancel_tarjeta,2).'<br>'.number_format($abono_cheque + $cancel_cheque,2).'
          </font>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td style="text-align:left"><font size=5>TOTAL:</font></td>
          <td style="text-align:right"><font size=5>'.number_format($suma_abono_total,2).'</font></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>

        </tr>
      </tfoot> ';
echo "</table>";
}else{
  echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos un cliente...!</small></h1>';
}
 ?>
