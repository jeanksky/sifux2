<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
h3{
	color: black;
}
</style>
<?php
use backend\models\Empresa;
use backend\models\OrdenServicio;
use backend\models\Mecanicos;
use backend\models\Clientes;
use backend\models\Vehiculos;
use backend\models\Modelo;
use backend\models\Marcas;

$empresaimagen = new Empresa();
echo '<img src="'. $empresaimagen->getImageurl('html') .'" style="height: 100px;">';
$mecanicos = Mecanicos::find()->all();
if ($lista_filtro_mecanico != '0') {
  $mecanicos = Mecanicos::find()->where("idMecanico IN (".$lista_filtro_mecanico.")", [])->all();
}
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
echo '<BR><span style="float:right">
        <p>RANGO DESDE: '.$fecha_desde.' HASTA: '.$fecha_hasta.'</p>
      </span><br><br>';
$fecha_desde = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fecha_hasta = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de Detalle de Ordenen de servicio por mecánico</h3>';
      echo '<table class="items table table-striped">';
          echo '<tbody>';
          $antidad_total = 0;
          $monto_total = 0;
            foreach ($mecanicos as $key => $mecanico) {
              echo '<thead><tr><th colspan="7" style="text-align:center"><h3>'.$mecanico->nombre.'</h3></th></tr>';
              printf('<tr>
                       <th style="text-align:center">%s</th>
                       <th style="text-align:center">%s</th>
                       <th style="text-align:center">%s</th>
                       <th style="text-align:left">%s</th>
                       <th style="text-align:left">%s</th>
                       <th style="text-align:left">%s</th>
                       <th style="text-align:right">%s</th>
                      </tr>',
                         '#ORDEN',
                         'FECH.INICIO',
                         'FECH.FINAL',
                         'PLACA',
                         'MARCA / MODELO',
                         'CLIENTE',
                         'MONTO'
                         );
              $orden_servicios = OrdenServicio::find()->where(['idMecanico'=>$mecanico->idMecanico])
              ->andWhere("estado = '0' AND fecha BETWEEN :fecha_desde AND :fecha_hasta",
              [':fecha_desde'=>$fecha_desde, ':fecha_hasta'=>$fecha_hasta])->all();
              $cantidad = $monto = 0;
              $imp = 13/100;
              foreach ($orden_servicios as $key => $orden_servicio) {
                $cliente = Clientes::find()->where(['idCliente'=>$orden_servicio->idCliente])->one();
                $vehiculo = Vehiculos::find()->where(['placa'=>$orden_servicio->placa])->one();
                $modelo = Modelo::find()->where(['codModelo'=>$vehiculo->modelo])->one();
                $marca = Marcas::find()->where(['codMarca'=>$vehiculo->marca])->one();
                $cantidad += 1;
                $monto += $orden_servicio->montoServicio;
                $antidad_total += 1;
                $monto_total += $orden_servicio->montoServicio;

                printf('<tr>
                         <td style="text-align:center">%s</td>
                         <td style="text-align:center">%s</td>
                         <td style="text-align:center">%s</td>
                         <td style="text-align:left">%s</td>
                         <td style="text-align:left">%s</td>
                         <td style="text-align:left">%s</td>
                         <td style="text-align:right">%s</td>
                        </tr>',
                           $orden_servicio->idOrdenServicio,
                           $orden_servicio->fecha,
                           $orden_servicio->fechaCierre,
                           $orden_servicio->placa,
                           $marca->nombre_marca.' / '.$modelo->nombre_modelo,
                           $cliente->nombreCompleto,
                           number_format($orden_servicio->montoServicio + ($orden_servicio->montoServicio*$imp),2)
                           );
              }
            }
            printf('<tr>
                     <th style="text-align:center">%s</th>
                     <th style="text-align:center">%s</th>
                     <th style="text-align:center">%s</th>
                     <th style="text-align:left">%s</th>
                     <th style="text-align:left">%s</th>
                     <th style="text-align:right">%s</th>
                     <th style="text-align:right">%s</th>
                    </tr>',
                       '',
                       '',
                       '',
                       '',
                       '',
                       'TOTAL:',
                       number_format($monto_total + ($monto_total*$imp),2)
                       );
          echo '</tbody>';
      echo '</table>'
 ?>
