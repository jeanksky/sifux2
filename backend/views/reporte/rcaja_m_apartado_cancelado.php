<?php
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\Clientes;

$empresaimagen = new Empresa();
$todos_clientes = Clientes::find()
->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
->where(['apartado'=>'x'])
->all();
 $todos_idCliente = '';
 foreach ($todos_clientes as $key => $value) { $todos_idCliente .= $value->idCliente.','; }
 $fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
 $fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
 $lista_clientes = $lista_clientes == '0' ? substr($todos_idCliente, 0, -1) : $lista_clientes;
/*$mediopagoproveedor = MedioPagoProveedor::find()->where(['=','cuenta_bancaria_local', $cuenta->idBancos])->andWhere("fecha_documento >= :fechaFactura1 AND fecha_documento <= :fechaFactura2", [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2])->orderBy(['idMedio_pago' => SORT_ASC])->all();*/


  $ventas = EncabezadoPrefactura::find()->where(['apartado'=>'x'])
  ->andWhere("estadoFactura = 'Cancelada' AND idCliente IN (".$lista_clientes.") AND fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2",
  [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2 ])->orderBy(['fecha_emision' => SORT_ASC])->all();
  echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
  echo '<span style="float:right">
          <p>RANGO: <strong>Desde: </strong>'.$fecha_desde." <strong>Hasta: </strong>".$fecha_hasta.'</p>
        </span>';
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Lista de apartados cancelados</h3>';
  echo '<table class="items table table-striped" id="tabla_facturas_pendientes" width="100%" >';
  echo '<thead class="thead-inverse">';
                  printf('<tr>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
                          'ID Documento',
                          'Registro',
                          'Emitida',
                          'Cliente',
                          'Estado',
                          'Monto'
                          );
                  echo '</thead>';
  echo '<tbody class="">';
  $monto_total_apartado = 0;
  foreach ($ventas as $factura) {
    $monto_total_apartado += $factura['total_a_pagar'];
    $nombre_cliente = '';
    if($cliente = Clientes::findOne($factura['idCliente']))
    {
        $nombre_cliente = $cliente->nombreCompleto;

    } else {
        $nombre_cliente = $factura['idCliente'];
    }
    echo '<tr style="color:#231BB8">
              <td align="center" width="100"><font face="arial" size=2 >'.$factura['idCabeza_Factura'].'</font></td>
              <td align="center" width="100"><font size=2>'.date('d-m-Y', strtotime($factura['fecha_inicio']) ).'</font></td>
              <td align="center" width="100"><font size=2>'.date('d-m-Y', strtotime($factura['fecha_emision']) ).'</font></td>
              <td align="left" width="400"><font size=2>'.$nombre_cliente.'</font></td>
              <td align="center" width="150"><font size=2>'.$factura['estadoFactura'].'</font></td>
              <td align="right" width="80"><font size=2>'.number_format($factura['total_a_pagar'],2).'</font></td>
            </tr>';
  }
  printf('<tr>
            <td style="text-align:center" ><font size=3>%s</font></td>
            <td style="text-align:right"><font size=3>%s</font></td>
            <td style="text-align:right"><font size=3>%s</font></td>
            <td style="text-align:left"><font size=3>%s</font></td>
            <td style="text-align:left"><font size=3>%s</font></td>
            <td style="text-align:right" width="190"><font size=3>%s</font></td>
          </tr>',
              '',
              '',
              '',
              '',
              '<h4>Total:</h4>',
              '<h4> <strong>'.number_format($monto_total_apartado,2).'</strong></h4>'
          );
  echo '</tbody>';
  echo '</table>';
 ?>
