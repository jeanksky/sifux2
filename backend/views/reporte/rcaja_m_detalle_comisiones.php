<?php
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\AgentesComisiones;
//prueba hotfix
$empresaimagen = new Empresa();

$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
$agenteCoMisiones = AgentesComisiones::find()->orderBy(['idAgenteComision' => SORT_DESC])->all();
if ($lista_clientes != '0') {
  $agenteCoMisiones = AgentesComisiones::find()->where("idAgenteComision IN (".$lista_clientes.")", [])->orderBy(['idAgenteComision' => SORT_DESC])->all();
}
/*Verifica si PARAMETROS DE COMISIONES ESTA ACTIVO*/
$comisiones = Yii::$app->params['comisiones'];
	if ($comisiones == true) {

	/*Fechas en el formato para personas*/
	$f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
	$f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

	echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
	echo '<span style="float:right">
	        <p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
	      </span>';

		  $sumaToTalSUBTOTAL = 0;
		  $sumaToTalDESCUENTO = 0;
		  $montoToTalComisionXagnt = 0;

		foreach ($agenteCoMisiones as $key => $agntComi) {
		  echo '
		    <table class="items table table-striped">';
		      echo '<thead>
		      <tr><th colspan="8" style="text-align:center"><h3>'.$agntComi->idAgenteComision.' - '.$agntComi->nombre.'<br><small>Tel: '.$agntComi->telefono.' / % '.$agntComi->porcentComision.'</small></h3></th></tr>';
		      printf('<tr>
		        <th width="150">%s</th>
		        <th width="100">%s</th>
		        <th width="200">%s</th>
		        <th width="280">%s</th>
		        <th width="140" style="text-align:right">%s</th>
		        <th width="130" style="text-align:right">%s</th>
		        <th style="text-align:right">%s</th>
		        <th style="text-align:center">%s</th>
		        </tr>',
		              'DOCUMENTO',
		              'FECHA EMISION',
		              'ESTADO',
		              'CLIENTE',
		              'SUBTOTAL',
		              'DESCUENTO',
		              'IMPUESTO',
		              'TOTAL'
		              );
		      echo '</thead>';
		$NumeroAgenteComision = $agntComi['idAgenteComision'];

		$sqlAGNT = "SELECT  idAgenteComision, idCabeza_Factura, fecha_emision, estadoFactura, idCliente, subtotal, porc_descuento, iva, total_a_pagar FROM `tbl_encabezado_factura`
		WHERE idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
		(SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC' GROUP BY idCabeza_Factura) fact_not
		ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
		WHERE fact.total_a_pagar = fact_not.suma_nc)
		AND fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura IN ('Cancelada') AND idAgenteComision = :idAgenteComision ORDER BY idCabeza_Factura DESC;";

		  $command = \Yii::$app->db->createCommand($sqlAGNT);
		  $command->bindParam(":fechaFactura1", $fechaFactura1);
		  $command->bindParam(":fechaFactura2", $fechaFactura2);
		  $command->bindParam(":idAgenteComision",$NumeroAgenteComision);
		  $ventasAGNT = $command->queryAll();

		  $sumaSUBTOTAL = 0;
		  $sumaDESCUENTO = 0;
		  $montoSUBTOTALsinDescuento = 0;
		  $porcentComisionXagnt = 0;
		  $montoComisionXagnt = 0;

		  /*$sumaToTalSUBTOTAL = 0;
		  $sumaToTalDESCUENTO = 0;
		  $montoToTalComisionXagnt = 0;*/

			foreach($ventasAGNT as $facturaAGNT) {

				$fecha_EMISION = date('d-m-Y', strtotime( $facturaAGNT['fecha_emision'] ));

				/*Se establecen los datos de las facturas en las variables*/
				$sumaSUBTOTAL += $facturaAGNT['subtotal'];  //suma de todos los subtotales
				$sumaDESCUENTO += $facturaAGNT['porc_descuento']; // suma de todos los descuentos de la factuas
				$montoSUBTOTALsinDescuento = $sumaSUBTOTAL - $sumaDESCUENTO; // se les quita los descuento por si se les hicieron a las facturas
   	            $porcentComisionXagnt = $agntComi['porcentComision']; //porcentaje de comision con el que cuenta el agente
    			$montoComisionXagnt = $montoSUBTOTALsinDescuento * ($porcentComisionXagnt / 100); // se le calcula el monto de comision

				echo '<tr style="color:#231BB8">
			      		 		<td align="center" ><font face="arial" size=4 >'.$facturaAGNT['idCabeza_Factura'].'</font></td>
			              		<td align="center" width="180" ><font size=4>'.$fecha_EMISION.'</font></td>
			              		<td width="190"><font size=4>'.$facturaAGNT['estadoFactura'].'</font></td>
			              		<td align="left"><font size=4>'.$facturaAGNT['idCliente'].'</font></td>
			              		<td align="right"><font size=4>'.number_format($facturaAGNT['subtotal'],2).'</font></td>
			              		<td align="right"><font size=4>'.number_format($facturaAGNT['porc_descuento'],2).'</font></td>
			              		<td align="center"><font size=4>'.number_format($facturaAGNT['iva'],2).'</font></td>
			              		<td align="center"><font size=4>'.number_format($facturaAGNT['total_a_pagar'],2).'</font></td></tr>';

				/*$sumaToTalSUBTOTAL += $sumaSUBTOTAL;
		  		$sumaToTalDESCUENTO += $sumaDESCUENTO;
		 		$montoToTalComisionXagnt += $montoComisionXagnt;*/

			}//fin foreach de ventasAGNT

				$sumaToTalSUBTOTAL += $sumaSUBTOTAL;
		  		$sumaToTalDESCUENTO += $sumaDESCUENTO;
		 		$montoToTalComisionXagnt += $montoComisionXagnt;

			echo '
                </tr>
                  <td align="center" ></td>
                  <td align="center" width="180" ></td>
                  <td width="190"><font size=4></font></td>
                  <td align="left"><font size=4></font></td>
                  <td align="right"><font size=4><strong>'.number_format($sumaSUBTOTAL,2).'</strong></font></td>
                  <td align="right"><font size=4><strong>'.number_format($sumaDESCUENTO,2).'</strong></font></td>
                  <td align="right"><font size=4><strong>Comision:</strong></font></td>
                  <td align="center"><font size=4><strong>'.number_format($montoComisionXagnt,2).'</strong></font></td>
                </tr>';
		}// foreach agenteCoMisiones

		echo '<tfoot>
                </tr>
                  <td align="center" ></td>
                  <td align="center" width="180" ></td>
                  <td width="190"><font size=4></font></td>
                  <td align="left"><font size=4><strong></strong></font></td>
                  <td align="right"><font size=4><strong></strong></font></td>
                  <td align="right"><font size=4><strong></strong></font></td>
                  <td align="right"><font size=4><strong></strong></font></td>
                  <td align="center"><font size=4><strong></strong></font></td>
                </tr>
              </tfoot>
            </table>';
// este imprime con totales lo comente porque no se si solo sera emitido para un Agente puede confundir.
/*echo '<tfoot>
                </tr>
                  <td align="center" ></td>
                  <td align="center" width="180" ></td>
                  <td width="190"><font size=4></font></td>
                  <td align="left"><font size=4><strong>TOTALES:</strong></font></td>
                  <td align="right"><font size=4><strong>'.number_format($sumaToTalSUBTOTAL,2).'</strong></font></td>
                  <td align="right"><font size=4><strong>'.number_format($sumaToTalDESCUENTO,2).'</strong></font></td>
                  <td align="right"><font size=4><strong>Comision:</strong></font></td>
                  <td align="center"><font size=4><strong>'.number_format($montoToTalComisionXagnt,2).'</strong></font></td>
                </tr>
              </tfoot>
            </table>';*/

	}// fin if comisiones VALIDA PARAMS

?>
