<?php
use kartik\widgets\DatePicker;
use backend\models\Clientes;
use backend\models\Familia;
//use app\models\Proveedores;
//use app\models\CuentasBancarias;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';
$categorias = Familia::find()->where(['tipo'=>'Producto'])->all();

  //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>REPORTE DE INVENTARIO
  if ($tipo=='costo_inventario') {//==================COSTO INVENTARIO
    	echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Costo inventario</h3>';
      echo '
      <div class="col-lg-11">
      </div>
      <div class="col-lg-1">
        <a class="btn btn-default btn-sm" onclick="imprimir_inventario_costo()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a><br><br>
      </div>';
      echo '<div class="col-lg-12">
              '.$this->renderAjax('rinventario_m_costo', [ ]).'
            </div>';
  } elseif ($tipo=='resumen_costo_inventario') {//==================RESUMEN DE COSTO INVENTARIO POR CATEGORIA
    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen Costo inventario Categorías</h3>';

    echo '<div class="col-lg-8">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
        'name' => '',
        'data' => ArrayHelper::map($categorias,'codFamilia', function($element) {
                    return $element['codFamilia'].' - '.$element['descripcion'];
                }),
        'options' => [
            'placeholder' => '- Seleccione las categoría a filtrar -',
            'id'=>'filtro_cagtegoria_inven',
            'multiple' => true //esto me ayuda a que la busqueda sea multiple
        ],
        'pluginOptions' => ['initialize'=> true,'allowClear' => true]
    ]).'</div>
    <div class="col-lg-2">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_in_costo_categoria()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_in_costo_categoria" onclick="imprimir_reporte_in_costo_categoria()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
    </div>
    <div class="col-lg-12"><br>
      <div class="well" id="mostrar_re_inv_costo_cat"></div>
    </div>';

  } elseif ($tipo=='producto_x_categoria') {//==================PRODUCTO POR CATEGORIA
    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Productos por Categoría</h3>';
    echo '<div class="col-lg-8">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
        'name' => '',
        'data' => ArrayHelper::map($categorias,'codFamilia', function($element) {
                    return $element['codFamilia'].' - '.$element['descripcion'];
                }),
        'options' => [
            'placeholder' => '- Seleccione las categoría a filtrar -',
            'id'=>'filtro_cagtegoria_inven2',
            'multiple' => true //esto me ayuda a que la busqueda sea multiple
        ],
        'pluginOptions' => ['initialize'=> true,'allowClear' => true]
    ]).'</div>
    <div class="col-lg-2">
      '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
          'name' => '',
          'data' => ['todos' => "Todos", 'escasos' => "Escasos", 'suficiente' => "Suficientes"],
          'options' => [
              'value' => 'todos',
              'id'=>'filtro_categoria_inven_cantidad',
              'multiple' => false //esto me ayuda a que solo se obtenga uno
          ],
      ]).'
    </div>
    <div class="col-lg-2">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_in_x_categoria()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_in_x_categoria" onclick="imprimir_reporte_in_x_categoria()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
    </div>
    <div class="col-lg-12"><br>
      <div class="well" id="mostrar_re_inv_x_cat"></div>
    </div>';
  } elseif ($tipo=='producto_x_debajo_minimo') {
    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Productos por debajo del mínimo (Categoría)</h3>';
    echo '<div class="col-lg-8">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
        'name' => '',
        'data' => ArrayHelper::map($categorias,'codFamilia', function($element) {
                    return $element['codFamilia'].' - '.$element['descripcion'];
                }),
        'options' => [
            'placeholder' => '- Seleccione las categoría a filtrar -',
            'id'=>'filtro_cagtegoria_inven3',
            'multiple' => true //esto me ayuda a que la busqueda sea multiple
        ],
        'pluginOptions' => ['initialize'=> true,'allowClear' => true]
    ]).'</div>
    <div class="col-lg-2">
        <a class="btn btn-default btn-sm" onclick="buscar_reporte_in_x_debajo_minimo()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_in_x_debajo_minimo" onclick="imprimir_reporte_in_x_debajo_minimo()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
    </div>
    <div class="col-lg-12"><br>
      <div class="well" id="mostrar_re_inv_x_debajo_minimo"></div>
    </div>';
  }elseif ($tipo=='producto_no_vendido') {
    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Productos no vendidos</h3>';
    echo '<div class="col-lg-3">
                  '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                  'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy','autocomplete'=>'off',
                  'id'=>'fecha_desde1']]).'
          </div>
          <div class="col-lg-3">
                  '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                  'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy','autocomplete'=>'off',
                  'id'=>'fecha_hasta1']]).'
          </div>
    <div class="col-lg-2">
        <a class="btn btn-default btn-sm" onclick="buscar_producto_no_vendido()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_re_producto_no_vendido" onclick="imprimir_producto_no_vendido()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
    </div>
    <div class="col-lg-12"><br>
      <div class="well" id="mostrar_re_inv_producto_no_vendido"></div>
    </div>';
  } /* fin if producto_no_vendido*/
?>
<script type="text/javascript">
    $(document).ready(function () {
        //mantenemos todos los boton de imprimir desabilitado
        $('#imp_re_in_costo_categoria').addClass('disabled');
        $('#imp_re_in_x_categoria').addClass('disabled');
        $('#imp_re_in_x_debajo_minimo').addClass('disabled');
        $('#imp_re_producto_no_vendido').addClass('disabled');
    });
    //==============FUNCIONES PARA IMPRIMIR COSTO INVENTARIO
    function imprimir_inventario_costo() {
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iinventario_m_costo') ?>" ,
          function( data ) {
            var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
              ventimp.document.write(data);
              ventimp.document.close();
              ventimp.print();
              ventimp.close();
          });
    }

    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR RESUMEN COSTO INVENTARIO CATEGORIA
    function buscar_reporte_in_costo_categoria() {
      var filtro_cagtegoria_inven = $("#filtro_cagtegoria_inven").val();
      var lista_filtro_cagtegoria_inven = '0';
      if (filtro_cagtegoria_inven==null) {
        lista_filtro_cagtegoria_inven = '0';
      } else {
        lista_filtro_cagtegoria_inven = filtro_cagtegoria_inven.toString();
      }
      document.getElementById("mostrar_re_inv_costo_cat").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rinventario_m_costo_categoria') ?>" ,
          { 'lista_filtro_cagtegoria_inven' : lista_filtro_cagtegoria_inven } ,
          function( data ) {
            $('#mostrar_re_inv_costo_cat').html(data);
            $('#imp_re_in_costo_categoria').removeClass('disabled');//abilitamos boton de imprimir
          });
    }
    function imprimir_reporte_in_costo_categoria() {
      var filtro_cagtegoria_inven = $("#filtro_cagtegoria_inven").val();
      var lista_filtro_cagtegoria_inven = '0';
      if (filtro_cagtegoria_inven==null) {
        lista_filtro_cagtegoria_inven = '0';
      } else {
        lista_filtro_cagtegoria_inven = filtro_cagtegoria_inven.toString();
      }
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iinventario_m_costo_categoria') ?>" ,
          { 'lista_filtro_cagtegoria_inven' : lista_filtro_cagtegoria_inven } ,
              function( data ) {
                var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
              });
    }

    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR PRODUCTOS POR CATEGORIA
    function buscar_reporte_in_x_categoria() {
      var filtro_cagtegoria_inven = $("#filtro_cagtegoria_inven2").val();
      var filtro_cantidad = $("#filtro_categoria_inven_cantidad").val();
      var lista_filtro_cagtegoria_inven = '0';
      if (filtro_cagtegoria_inven==null) {
        lista_filtro_cagtegoria_inven = '0';
      } else {
        lista_filtro_cagtegoria_inven = filtro_cagtegoria_inven.toString();
      }
      document.getElementById("mostrar_re_inv_x_cat").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rinventario_m_x_categoria') ?>" ,
          { 'lista_filtro_cagtegoria_inven' : lista_filtro_cagtegoria_inven, 'filtro_cantidad' : filtro_cantidad } ,
          function( data ) {
            $('#mostrar_re_inv_x_cat').html(data);
            $('#imp_re_in_x_categoria').removeClass('disabled');//abilitamos boton de imprimir
          });
    }

    function imprimir_reporte_in_x_categoria() {
      var filtro_cagtegoria_inven = $("#filtro_cagtegoria_inven2").val();
      var filtro_cantidad = $("#filtro_categoria_inven_cantidad").val();
      var lista_filtro_cagtegoria_inven = '0';
      if (filtro_cagtegoria_inven==null) {
        lista_filtro_cagtegoria_inven = '0';
      } else {
        lista_filtro_cagtegoria_inven = filtro_cagtegoria_inven.toString();
      }
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iinventario_m_x_categoria') ?>" ,
          { 'lista_filtro_cagtegoria_inven' : lista_filtro_cagtegoria_inven, 'filtro_cantidad' : filtro_cantidad } ,
              function( data ) {
                var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
              });
    }
    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR PRODUCTOS POR DEBAJO DEL MINIMO
    function buscar_reporte_in_x_debajo_minimo() {
      var filtro_cagtegoria_inven = $("#filtro_cagtegoria_inven3").val();
      var lista_filtro_cagtegoria_inven = '0';
      if (filtro_cagtegoria_inven==null) {
        lista_filtro_cagtegoria_inven = '0';
      } else {
        lista_filtro_cagtegoria_inven = filtro_cagtegoria_inven.toString();
      }
      document.getElementById("mostrar_re_inv_x_debajo_minimo").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rinventario_m_x_debajo_minimo') ?>" ,
          { 'lista_filtro_cagtegoria_inven' : lista_filtro_cagtegoria_inven } ,
          function( data ) {
            $('#mostrar_re_inv_x_debajo_minimo').html(data);
            $('#imp_re_in_x_debajo_minimo').removeClass('disabled');//abilitamos boton de imprimir
          });
    }
    function imprimir_reporte_in_x_debajo_minimo() {
      var filtro_cagtegoria_inven = $("#filtro_cagtegoria_inven3").val();
      var lista_filtro_cagtegoria_inven = '0';
      if (filtro_cagtegoria_inven==null) {
        lista_filtro_cagtegoria_inven = '0';
      } else {
        lista_filtro_cagtegoria_inven = filtro_cagtegoria_inven.toString();
      }

      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iinventario_m_x_debajo_minimo') ?>" ,
          { 'lista_filtro_cagtegoria_inven' : lista_filtro_cagtegoria_inven } ,
              function( data ) {
                var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
              });
    }

    //==============FUNCIONES PARA MOSTRAR E IMPRIMIR PRODUCTOS NO VENDIDOS
    function buscar_producto_no_vendido() {
      fecha_desde = document.getElementById('fecha_desde1').value;
      fecha_hasta = document.getElementById('fecha_hasta1').value;
      document.getElementById("mostrar_re_inv_producto_no_vendido").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rinventario_m_producto_no_vendido') ?>" ,
          { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
          function( data ) {
            $('#mostrar_re_inv_producto_no_vendido').html(data);
            $('#imp_re_producto_no_vendido').removeClass('disabled');//abilitamos boton de imprimir
          });
    }
    function imprimir_producto_no_vendido() {
      fecha_desde = document.getElementById('fecha_desde1').value;
      fecha_hasta = document.getElementById('fecha_hasta1').value;
      $('#imp_re_producto_no_vendido').addClass('disabled');
      $('#imp_re_producto_no_vendido').text('Por favor espere...');
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iinventario_m_producto_no_vendido') ?>" ,
          { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
          function( data ) {
            $('#imp_re_producto_no_vendido').removeClass('disabled');
            $('#imp_re_producto_no_vendido').html('<span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir');
            var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
              ventimp.document.write(data);
              ventimp.document.close();
              ventimp.print();
              ventimp.close();
          });
    }
</script>
