<?php
use backend\models\Proveedores;
use backend\models\Empresa;
use backend\models\TramitePago;
use backend\models\ComprasInventario;
use backend\models\MovimientoPagar;
use backend\models\Moneda;

$empresaimagen = new Empresa();
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
echo '<BR><span style="float:right">
        <p>RANGO DESDE: '.$fecha_desde.' HASTA: '.$fecha_hasta.'</p>
      </span>';
      $fecha_cancela_or = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
      $fecha_cancrela_fi = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';
      $proveedores = Proveedores::find()->all();
      if ($lista_filtro_proveedores != '0') {
        $proveedores = Proveedores::find()->where("codProveedores IN (".$lista_filtro_proveedores.")", [])->all();
      }
      echo '<table class="items table table-striped">';
          echo '<tbody>';

          $monto_total_pagar[] = null;
          $monto_total_compra[] = null;
          $monto_nd_total[] = null;
          $monto_nc_total[] = null;
          $monto_ab_total[] = null;
          $monto_saldo_total_pagar[] = null;

            foreach ($proveedores as $key => $proveedor) {
              $tramites = TramitePago::find()->where(['idProveedor'=>$proveedor->codProveedores])
              ->andWhere("fecha_aplica BETWEEN :fecha_cancela_or AND :fecha_cancrela_fi",
              [':fecha_cancela_or'=>$fecha_cancela_or, ':fecha_cancrela_fi'=>$fecha_cancrela_fi])->all();
              if (@$tramites) {
                  echo '<thead><tr><th colspan="8" style="text-align:center"><h3>'.$proveedor->codProveedores.' - '.$proveedor->nombreEmpresa.'<br><small>Tel: '.$proveedor->telefono.' / '.$proveedor->telefono2.'</small></h3></th></tr>';
                  printf('<tr>
                               <th style="text-align:left">%s</th>
                               <th style="text-align:center">%s</th>
                               <th style="text-align:left">%s</th>
                               <th style="text-align:right">%s</th>
                               <th style="text-align:right">%s</th>
                               <th style="text-align:right">%s</th>
                               <th style="text-align:right">%s</th>
                               <th style="text-align:right">%s</th>
                              </tr>',
                                 'No. TRÁMITE',
                                 'FECHA CANCEL / ID COMPRA',
                                 'No. FACTURA PROV',
                                 'MONTO FACTURA',
                                 'NOT.DÉBITO',
                                 'NOT.CRÉDITO',
                                 'ABONOS',
                                 'SALDO CANCELADO'
                                 );
                      echo '</thead>';
                  $total_tramite_pagar = $total_compra = $nd_total = $nc_total = $ab_total = $saldo_total_pagar = 0;//
                  foreach ($tramites as $ubi => $tramite) {
                    $compras = ComprasInventario::find()->where(['idTramite_pago'=>$tramite->idTramite_pago])->all();
                    if (@$compras) {
                        $moneda_pro = Moneda::findOne($proveedor->idMoneda);
                        $rowspan = 0; $td = '';
                        foreach ($compras as $posi => $compra) {
                          $rowspan += 1;
                        }
                        $suma_movimiento = $tramite->cantidad_monto_documento;
                        $suma_saldo = $tramite->monto_saldo_pago;
                        $porc_desc = $tramite->porcentaje_descuento;
                        $monto_pagar = $tramite->monto_tramite_pagar;

                        $total_tramite_pagar += $tramite->monto_tramite_pagar;

                        $resumen = '<br>Suma de montos:<span style="float:right"><strong>'.$moneda_pro->simbolo.' '.number_format($suma_movimiento,2).'</strong></span>
                        <br><br>Suma saldo:<span style="float:right"><strong>'.$moneda_pro->simbolo.' '.number_format($suma_saldo,2).'</strong></span>
                        <br><br>% Desc:<span style="float:right"><strong>%'.number_format($porc_desc,2).'</strong></span>
                        <br><h3>Total cancelado:<span style="float:right"><strong>'.$moneda_pro->simbolo.' '.number_format($monto_pagar,2).'</strong></span><h3>';
                        $resumen = $rowspan > 1 ? $resumen : '';

                          echo '<tr><td width=20% rowspan='.$rowspan.' style="border-right-width: 3px; border-right-style: solid; border-right-color: #ddd;" >
                          <font size=6>'.$tramite->idTramite_pago.'</font>'.$resumen.'</td>';


                        //recorre las compras

                        foreach ($compras as $posi => $compra) {
                          $nd = $nc = $ab = 0;
                          $movimiento_pagar = MovimientoPagar::find()->where(['idCompra'=>$compra->idCompra])->all();
                          //recorre los movimientos por pagar

                          foreach ($movimiento_pagar as $key => $mov_pag) {

                            if ($mov_pag->tipo_mov == 'nd') {
                              $nd += $mov_pag->monto_movimiento;
                              $nd_total += $mov_pag->monto_movimiento;
                            } elseif ($mov_pag->tipo_mov == 'nc') {
                              $nc += $mov_pag->monto_movimiento;
                              $nc_total += $mov_pag->monto_movimiento;
                            } elseif ($mov_pag->tipo_mov == 'ab') {
                              $ab += $mov_pag->monto_movimiento;
                              $ab_total += $mov_pag->monto_movimiento;
                            }

                          }//fin de foreach que rtecorre los movimientos (NC ND AB)

                          $saldo_pagar = ($compra->total + $nd) - ($nc+$ab);
                          $total_compra += $compra->total;
                          $saldo_total_pagar += $saldo_pagar;
                          echo '
                                 <td style="text-align:center">'.date('d-m-Y', strtotime( $tramite->fecha_aplica )).' / '.$compra->idCompra.'</td>
                                 <td style="text-align:center"><font size=4>'.$compra->numFactura.'</font></td>
                                 <td style="text-align:right">'.$moneda_pro->simbolo.' '.number_format($compra->total,2).'</td>
                                 <td style="text-align:right">'.$moneda_pro->simbolo.' '.number_format($nd,2).'</td>
                                 <td style="text-align:right">'.$moneda_pro->simbolo.' '.number_format($nc,2).'</td>
                                 <td style="text-align:right">'.$moneda_pro->simbolo.' '.number_format($ab,2).'</td>
                                 <td style="text-align:right">'.$moneda_pro->simbolo.' '.number_format($saldo_pagar,2).'</td>
                                </tr>';
                          }//fin foreach compras
                          echo '<td colspan="8"></td>';
                        }
                  }//fin foreach tramite
                  $monedas = Moneda::find()->all();
                  $monto_total_pagar[] = null;
                  $monto_total_compra[] = null;
                  $monto_nd_total[] = null;
                  $monto_nc_total[] = null;
                  $monto_ab_total[] = null;
                  $monto_saldo_total_pagar[] = null;
                  foreach ($monedas as $r => $moneda) {
                    if ($moneda->idTipo_moneda == $proveedor->idMoneda) {
                      $monto_total_pagar[$r] += $total_tramite_pagar;
                      $monto_total_compra[$r] += $total_compra;
                      $monto_nd_total[$r] += $nd_total;
                      $monto_nc_total[$r] += $nc_total;
                      $monto_ab_total[$r] += $ab_total;
                      $monto_saldo_total_pagar[$r] += $saldo_total_pagar;
                    }
                  }//fin foreach monedas
              }//fin if que comprueba tramites

            }//fin foreach proveedor

            $resultado_monto_total_pagar = $resultado_monto_total_compra = $resultado_monto_nd_total = $resultado_monto_nc_total = $resultado_monto_ab_total = $resultado_monto_saldo_total_pagar = '';
            $monedas = Moneda::find()->all();
            foreach ($monedas as $r => $moneda) {
              $resultado_monto_total_pagar .= $moneda->simbolo.number_format($monto_total_pagar[$r],2).' <br> ';

              $resultado_monto_total_compra .= $moneda->simbolo.number_format($monto_total_compra[$r],2).' <br> ';
              $resultado_monto_nd_total .= $moneda->simbolo.number_format($monto_nd_total[$r],2).' <br> ';
              $resultado_monto_nc_total .= $moneda->simbolo.number_format($monto_nc_total[$r],2).' <br> ';
              $resultado_monto_ab_total .= $moneda->simbolo.number_format($monto_ab_total[$r],2).' <br> ';
              $resultado_monto_saldo_total_pagar .= $moneda->simbolo.number_format($monto_saldo_total_pagar[$r],2).' <br> ';
            }
          echo '</tbody>';
          echo '<tfoot>
                      <tr>
                       <th style="text-align:right"><h1>'.$resultado_monto_total_pagar.'</h1></th>
                       <th style="text-align:center"></th>
                       <th style="text-align:right"><font size=5>TOTAL GLOBAL (Por moneda):</font></th>
                       <th style="text-align:right"><font size=4>'.$resultado_monto_total_compra.'</font></th>
                       <th style="text-align:right"><font size=4>'.$resultado_monto_nd_total.'</font></th>
                       <th style="text-align:right"><font size=4>'.$resultado_monto_nc_total.'</font></th>
                       <th style="text-align:right"><font size=4>'.$resultado_monto_ab_total.'</font></th>
                       <th style="text-align:right"><font size=5>'.$resultado_monto_saldo_total_pagar.'</font></th>
                      </tr>';
              echo '</tfoot>';
          echo '</table>';
 ?>
