<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
</style>
<?php
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\AgentesComisiones;

$empresaimagen = new Empresa();

/*Consultas de años*/
$anoConsultar = $anoSeleccionado;
$anoConsultar_desde = $anoConsultar - 1;
$anoPasado = date('Y', strtotime('-1 year'));
$anoActual = $anoPasado + 1;

$fechaFactura1 = $anoConsultar != '0' ? $anoConsultar_desde.'-10-01' : $anoPasado.'-10-01';
$fechaFactura2 = $anoConsultar != '0' ? $anoConsultar.'-09-30' : $anoActual.'-09-30';


 /*$sql =" SELECT SUM(subtotal-porc_descuento) AS sumD151, idCliente FROM `tbl_encabezado_factura` WHERE fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2 GROUP BY idCliente DESC";*/

 $sql = "SELECT SUM(subtotal) AS sumD151, idCliente FROM `tbl_encabezado_factura`
WHERE idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
(SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC' GROUP BY idCabeza_Factura) fact_not
ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
WHERE fact.total_a_pagar = fact_not.suma_nc)
AND fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura !='Anulada'  GROUP BY idCliente DESC;";

  $command = \Yii::$app->db->createCommand($sql);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $ventas = $command->queryAll();

/*Fechas en el formato para personas*/
$f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
$f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
      </span>';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte D-151 Clientes</h3>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th></tr>',
                        'N° Cedula',
                        'Tipo Cedula',
                        'Nombre Cliente',
                        'Telefono / Celular',
                        'Codigo MIN',
                        'Monto Vendido'
                        );
                echo '</thead>';
echo '<tbody class="">';
$monto_total = 0;
$limiteMonto = Yii::$app->params['limite_monto_venta_MIN']; //Limite impuesto por el MIN en venta de articulos. ¢2,500,000
foreach($ventas as $factura) {

    /*Datos Clientes*/
  $nCedulaCliente = '';
  $tipoIdentidadCliente = '';
  $nombre_cliente = '';
  $clienteTelefono = '';
  $clienteCelular = '';
  $codigoMIN = 'V';
  if($cliente = Clientes::findOne($factura['idCliente']))
  {
    /*Verificacion de suma de monto con el limite de venta tributacion*/
    if($factura['sumD151'] >= $limiteMonto){
            $nCedulaCliente = $cliente->identificacion;
            $tipoIdentidadCliente =  $cliente->tipoIdentidad;
            $nombre_cliente = $cliente->nombreCompleto;
            $clienteTelefono = $cliente->telefono;
            $clienteCelular = $cliente->celular;

            $monto_total += $factura['sumD151'];

              printf('<tr style="color:#231BB8">
                  <td align="right" ><font face="arial" size=2 >%s</font></td>
                  <td style="text-align:center" ><font size=2>%s</font></td>
                  <td align="right" width="130"><font size=2>%s</font></td>
                  <td align="right"><font size=2>%s</font></td>
                  <td style="text-align:center" width="80"><font size=2>%s</font></td>
                  <td style="text-align:right" width="150"><font size=2>%s</font></td>
                </tr>',
                      $nCedulaCliente,
                      $tipoIdentidadCliente,
                      $nombre_cliente,
                      $clienteTelefono.' / '.$clienteCelular,
                      $codigoMIN,
                      number_format($factura['sumD151'] ,2)
                  );
    }//fin if limiteMonto ley Tributaria
  }//fin del if BusquedaClienteRegistrado
}//fin foreach
//$monto_total = $monto_total_credito + $monto_total_contado;

printf('<tr>
          <td style="text-align:center" ><font size=3>%s</font></td>
          <td style="text-align:right" ><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="50"><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="150"><font size=3>%s</font></td>
        </tr>',
            '',
            '',
            '',
            '',
            '',
            '<h4>Total: <strong>'.number_format($monto_total,2).'</strong></h4>'
        );
echo '</tbody>';
echo '</table>';

 $sqlAGNT = "SELECT SUM(subtotal) AS sumD151AGNT, idAgenteComision FROM `tbl_encabezado_factura`
WHERE idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
(SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC' GROUP BY idCabeza_Factura) fact_not
ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
WHERE fact.total_a_pagar = fact_not.suma_nc)
AND fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura !='Anulada' GROUP BY idAgenteComision DESC;";

  $command = \Yii::$app->db->createCommand($sqlAGNT);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $ventasAGNT = $command->queryAll();


$comisiones = Yii::$app->params['comisiones'];
if ($comisiones == true) {
echo '<br><center><strong><font face="arial" size=4 color="#891313">D-151 Comisiones Agentes</font></strong></center>';
echo '<table class="items table table-striped" id="tabla_d151_agente_comiciones"  >';
echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
                        'N° Cedula',
                        'Tipo Cedula',
                        'Nombre Cliente',
                        'Telefono / Celular',
                        'Codigo MIN',
                        'Monto Comisión'
                        );
                echo '</thead>';
echo '<tbody class="">';
$monto_total_AGNT = 0;
$limiteMontoAGNT = Yii::$app->params['limite_monto_comision_MIN']; //Limite impuesto por el MIN en venta de articulos. ¢2,500,000
foreach($ventasAGNT as $facturaAGNT) {

    /*Datos Clientes*/
  $nCedulaAgente = '';
  $tipoIdentidadAgente = '';
  $nombre_agente = '';
  $agenteTelefono = '';
  $porcentComisionXagnt = '';
  $codigoMIN = 'M';
  $montoComisionXagnt = 0;

  if($agenteComi = AgentesComisiones::findOne($facturaAGNT['idAgenteComision']))
  {
    $porcentComisionXagnt = $agenteComi->porcentComision;
    $montoComisionXagnt = $facturaAGNT['sumD151AGNT'] * ($porcentComisionXagnt / 100);
    /*Verificacion de suma de monto con el limite de venta tributacion*/
    //if($facturaAGNT['sumD151AGNT'] >= $limiteMontoAGNT){
    if($montoComisionXagnt >= $limiteMontoAGNT){
            $nCedulaAgente = $agenteComi->cedula;
            $tipoIdentidadAgente =  $agenteComi->tipoIdentidad;
            $nombre_agente = $agenteComi->nombre;
            $agenteTelefono = $agenteComi->telefono;
            /*$porcentComisionXagnt = $agenteComi->porcentComision;
            $montoComisionXagnt = $facturaAGNT['sumD151AGNT'] * ($porcentComisionXagnt / 100);*/

            $monto_total_AGNT += $montoComisionXagnt;

           printf('<tr style="color:#231BB8">
                  <td align="right" ><font face="arial" size=2 >%s</font></td>
                  <td style="text-align:center" ><font size=2>%s</font></td>
                  <td align="right" width="130"><font size=2>%s</font></td>
                  <td align="right"><font size=2>%s</font></td>
                  <td style="text-align:center" width="80"><font size=2>%s</font></td>
                  <td style="text-align:right" width="150"><font size=2>%s</font></td>
                </tr>',
                      $nCedulaAgente,
                      $tipoIdentidadAgente,
                      $nombre_agente,
                      $agenteTelefono,
                      $codigoMIN,
                      number_format($montoComisionXagnt ,2)
                  );
    }//fin if limiteMonto ley Tributaria
  }//fin del if BusquedaClienteRegistrado
}//fin foreach
//$monto_total_AGNT = $monto_total_AGNT_credito + $monto_total_AGNT_contado;

printf('<tr>
          <td style="text-align:center" ><font size=3>%s</font></td>
          <td style="text-align:right" ><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="50"><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="150"><font size=3>%s</font></td>
        </tr>',
            '',
            '',
            '',
            '',
            '',
            '<h4>Total: <strong>'.number_format($monto_total_AGNT,2).'</strong></h4>'
        );
echo '</tbody>';
echo '</table>';
} // fin if comisiones
?>
