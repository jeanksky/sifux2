<?php
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use backend\models\Mecanicos;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';
$mecanicos = Mecanicos::find()->all();
if ($tipo == 'pendiente') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de Orden de Servicio pendiente</h3>';
  echo '<div class="col-lg-11"></div><div class="col-lg-1">
            <a class="btn btn-default btn-sm" onclick="imprimir_orden_servicio_pendiente()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
            <br><br>
        </div>';
  echo '<div class="col-lg-12">
          '.$this->renderAjax('rorden_servicio_m_pendiente', [ ]).'
        </div>';
} elseif ($tipo == 'resumen_meca') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de Orden de servicio por mecanico</h3>';
  echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
          'name' => '',
          'data' => ArrayHelper::map($mecanicos,'idMecanico', function($element) {
                      return $element['idMecanico'].' - '.$element['nombre'];
                  }),
          'options' => [
              'placeholder' => '- Seleccione el mecánico a filtrar -',
              'id'=>'filtro_mecanico',
              'multiple' => true //esto me ayuda a que la busqueda sea multiple
          ],
          'pluginOptions' => ['initialize'=> true,'allowClear' => true]
      ]).'
      </div>
      <div class="col-lg-2">
              '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
              'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
              'id'=>'fecha_desde']]).'
      </div>
      <div class="col-lg-2">
              '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
              'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
              'id'=>'fecha_hasta']]).'
      </div>
      <div class="col-lg-2">
          <a class="btn btn-default btn-sm" onclick="buscar_orden_serv_x_mecanico()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
          <a class="btn btn-default btn-sm" id="imp_re_orden_serv_x_mecanico" onclick="imprim_orden_serv_x_mecanico()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
      </div>
      <div class="col-lg-12"><br>
        <div class="well" id="mostrar_re_x_mecanicos"></div>
      </div>';
}elseif ($tipo == 'detalle_meca') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de Detalle de Ordenen de servicio por mecánico</h3>';
  echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
          'name' => '',
          'data' => ArrayHelper::map($mecanicos,'idMecanico', function($element) {
                      return $element['idMecanico'].' - '.$element['nombre'];
                  }),
          'options' => [
              'placeholder' => '- Seleccione el mecánico a filtrar -',
              'id'=>'filtro_mecanico2',
              'multiple' => true //esto me ayuda a que la busqueda sea multiple
          ],
          'pluginOptions' => ['initialize'=> true,'allowClear' => true]
      ]).'
      </div>
      <div class="col-lg-2">
              '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
              'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
              'id'=>'fecha_desde2']]).'
      </div>
      <div class="col-lg-2">
              '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
              'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
              'id'=>'fecha_hasta2']]).'
      </div>
      <div class="col-lg-2">
          <a class="btn btn-default btn-sm" onclick="buscar_orden_serv_x_mecanico_det()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
          <a class="btn btn-default btn-sm" id="imp_re_orden_serv_x_mecanico_det" onclick="imprim_orden_serv_x_mecanico_det()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
      </div>
      <div class="col-lg-12"><br>
        <div class="well" id="mostrar_re_x_mecanicos_det"></div>
      </div>';
}
 ?>
<script type="text/javascript">
  $(document).ready(function () {
      //mantenemos todos los boton de imprimir desabilitado
      $('#imp_re_orden_serv_x_mecanico').addClass('disabled');
      $('#imp_re_orden_serv_x_mecanico_det').addClass('disabled');
  });
  //==============FUNCIONES PARA IMPRIMIR ORDEN DE SERVICIOS PENDIENTES
    function imprimir_orden_servicio_pendiente() {
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iorden_servicio_m_pendiente') ?>" ,
              function( data ) {
                var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
              });
    }
  //==============FUNCIONES PARA MOSTRAR E IMPRIMIR ORDEN DE SERVICIOS POR MECANICOS
  function buscar_orden_serv_x_mecanico() {
    var filtro_mecanico = $("#filtro_mecanico").val();
    fecha_desde = document.getElementById('fecha_desde').value;
    fecha_hasta = document.getElementById('fecha_hasta').value;
    var lista_filtro_mecanico = '0';
    if (filtro_mecanico==null) {
      lista_filtro_mecanico = '0';
    } else {
      lista_filtro_mecanico = filtro_mecanico.toString();
    }
    document.getElementById("mostrar_re_x_mecanicos").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rorden_servicio_m_resumen_meca') ?>" ,
        { 'lista_filtro_mecanico' : lista_filtro_mecanico, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
        function( data ) {
          $('#mostrar_re_x_mecanicos').html(data);
          $('#imp_re_orden_serv_x_mecanico').removeClass('disabled');//abilitamos boton de imprimir
        });
  }
  function imprim_orden_serv_x_mecanico() {
    var filtro_mecanico = $("#filtro_mecanico").val();
    fecha_desde = document.getElementById('fecha_desde').value;
    fecha_hasta = document.getElementById('fecha_hasta').value;
    var lista_filtro_mecanico = '0';
    if (filtro_mecanico==null) {
      lista_filtro_mecanico = '0';
    } else {
      lista_filtro_mecanico = filtro_mecanico.toString();
    }
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iorden_servicio_m_resumen_meca') ?>" ,
        { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_mecanico' : lista_filtro_mecanico } ,
            function( data ) {
              var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                ventimp.document.write(data);
                ventimp.document.close();
                ventimp.print();
                ventimp.close();
            });
  }

  //==============FUNCIONES PARA MOSTRAR E IMPRIMIR ORDEN DE SERVICIOS POR MECANICOS
  function buscar_orden_serv_x_mecanico_det() {
    var filtro_mecanico = $("#filtro_mecanico2").val();
    fecha_desde = document.getElementById('fecha_desde2').value;
    fecha_hasta = document.getElementById('fecha_hasta2').value;
    var lista_filtro_mecanico = '0';
    if (filtro_mecanico==null) {
      lista_filtro_mecanico = '0';
    } else {
      lista_filtro_mecanico = filtro_mecanico.toString();
    }
    document.getElementById("mostrar_re_x_mecanicos_det").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rorden_servicio_m_detalle_meca') ?>" ,
        { 'lista_filtro_mecanico' : lista_filtro_mecanico, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
        function( data ) {
          $('#mostrar_re_x_mecanicos_det').html(data);
          $('#imp_re_orden_serv_x_mecanico_det').removeClass('disabled');//abilitamos boton de imprimir
        });
  }
  function imprim_orden_serv_x_mecanico_det() {
    var filtro_mecanico = $("#filtro_mecanico2").val();
    fecha_desde = document.getElementById('fecha_desde2').value;
    fecha_hasta = document.getElementById('fecha_hasta2').value;
    var lista_filtro_mecanico = '0';
    if (filtro_mecanico==null) {
      lista_filtro_mecanico = '0';
    } else {
      lista_filtro_mecanico = filtro_mecanico.toString();
    }
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iorden_servicio_m_detalle_meca') ?>" ,
        { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_mecanico' : lista_filtro_mecanico } ,
            function( data ) {
              var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                ventimp.document.write(data);
                ventimp.document.close();
                ventimp.print();
                ventimp.close();
            });
  }
</script>
