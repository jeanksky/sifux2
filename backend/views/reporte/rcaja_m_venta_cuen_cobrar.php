<?php
use backend\models\MovimientoCredito;
use backend\models\MovimientoCobrar;
use backend\models\Clientes;
use backend\models\NcPendientes;
use backend\models\Empresa;

$empresaimagen = new Empresa();
?>
<!-- <div class="col-lg-12">
<span style="float:right">
<a class="btn btn-default btn-sm"  onclick="imprimir_reporte_venta_cuen_cobrar()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
</span><br><br>
</div> -->

<!-- <div class="well col-lg-12 col-md-12"> -->
<?php
//if($lista_clientes != '0'){

	echo '<img src="'.$empresaimagen->getImageurl('html').'" style="height: 100px;">';
	$clientes = '';
	if ($lista_clientes == '0') {//si no existe seleccion de clientes
		if ($facturas_vencidas == 'si') {//si solo se requiere facturas vencidas
			$clientes = Clientes::find()
			->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
			->where(['>','credi_saldo', 0])->andWhere('fecha_vencimiento BETWEEN fecha_vencimiento AND :hoy AND fecha_vencimiento < :hoy',[':hoy' => date('Y-m-d')])->all();
		} else {
			$clientes = Clientes::find()
			->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
			->where(['>','credi_saldo', 0])->all();
		}
	} else {//si existe seleccion de clientes (fltro por clientes)
		if ($facturas_vencidas == 'si') {//si solo se requiere facturas vencidas
			$clientes = Clientes::find()
			->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
			->where("tbl_clientes.idCliente IN (".$lista_clientes.")", [])->andWhere('credi_saldo > 0 AND fecha_vencimiento BETWEEN fecha_vencimiento AND :hoy AND fecha_vencimiento < :hoy',[':hoy' => date('Y-m-d')])->all();
		} else {
			$clientes = Clientes::find()
			->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
			->where("tbl_clientes.idCliente IN (".$lista_clientes.")", [])->andWhere(['>','credi_saldo', 0])->all();
		}
	}
	$monto_total_fact = $monto_total_nc_ab = $saldo_total_fact = 0;
	foreach ($clientes as $key => $cliente) {
		echo '
		<table class="items table table-striped">';
		echo '<thead>
		<tr><th colspan="8" style="text-align:center"><h3>'.$cliente->nombreCompleto.'<br><small>Tel: '.$cliente->telefono.' / '.$cliente->celular.'</small></h3></th></tr>';
		printf('<tr>
		<th width="150">%s</th>
		<th width="100">%s</th>
		<th width="200">%s</th>
		<th width="280">%s</th>
		<th width="140" style="text-align:right">%s</th>
		<th width="130" style="text-align:right">%s</th>
		<th style="text-align:right">%s</th>
		<th style="text-align:center">%s</th>
		</tr>',
		'DOCUMENTO',
		'TIPO',
		'FECHA REGISTRO',
		'FECHA VENCIMIENTO',
		'MONTO (+)',
		'MONTO (-)',
		'SALDO',
		'DÍAS VENCIDOS'
	);
	echo '</thead>';
	if ($facturas_vencidas == 'si') {
		$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $cliente->idCliente])
		->andWhere("estadoFactura = :estadoFactura AND fecha_vencimiento < :hoy", [":estadoFactura"=>'Crédito', ':hoy' => date('Y-m-d')])
		->orderBy(['idCabeza_Factura' => SORT_ASC])->all();
	} else {
		$facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $cliente->idCliente])
		->andWhere("estadoFactura = :estadoFactura", [":estadoFactura"=>'Crédito'])
		->orderBy(['idCabeza_Factura' => SORT_ASC])->all();//SORT_ASC asendente SORT_DESC desendente
	}

	echo '<tbody>';
	$monto_fact = $monto_nc_ab = $saldo_fact = 0;
	$vencido_uno = $vencido_dos = $vencido_tres = $vencido_cuatro = 0;
	foreach($facturasCredito as $position => $factura) {
		$fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
		$fecha_vencimiento = date('d-m-Y', strtotime( $factura['fecha_vencimiento'] ));

		$montomenos = 0;
		$diasvencidas = 0;

		//condicion para obtener los días de vencimiento de la factura recorrida
		if (strtotime($factura['fecha_vencimiento']) < strtotime(date('Y-m-d'))) {
			$mv = new MovimientoCredito();
			$diasvencidas = $mv->getMovimiento_de_dias($factura['fecha_vencimiento'], date('Y-m-d'));

			$vencido_uno += $diasvencidas <= 30 ? $factura['credi_saldo'] : 0;
			$vencido_dos += ($diasvencidas <= 60) && ($diasvencidas > 30) ? $factura['credi_saldo'] : 0;
			$vencido_tres += ($diasvencidas <= 90) && ($diasvencidas > 60) ? $factura['credi_saldo'] : 0;
			$vencido_cuatro += $diasvencidas > 90 ? $factura['credi_saldo'] : 0;
		}

		$movimientofactura = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$factura['idCabeza_Factura']])
		->orderBy(['idMov' => SORT_DESC])->all();
		foreach($movimientofactura as $movimiento) {
			$montomenos += $movimiento['monto_movimiento'];
			$monto_total_nc_ab += $movimiento['monto_movimiento'];
		}
		$monto_fact += $factura['total_a_pagar'];//Suma el monto de todas las factuas pendientes
		$monto_nc_ab += $montomenos;//Suma el monto de las notas de Credito y abonos
		$saldo_fact += $factura['credi_saldo'];//Suma del saldo pendiente de todas las facturas
		$monto_total_fact += $factura['total_a_pagar'];
		$saldo_total_fact += $factura['credi_saldo'];
		printf('<tr style="color:#231BB8">
		<td align="center" ><font face="arial" size=4 >%s</font></td>
		<td align="center" width="180" ><font size=4>%s</font></td>
		<td width="190"><font size=4>%s</font></td><td align="left"><font size=4>%s</font></td>
		<td align="right"><font size=4>%s</font></td>
		<td align="right"><font size=4>%s</font></td>
		<td align="right"><font size=4>%s</font></td>
		<td align="center"><font size=4>%s</font></td></tr>',

		$factura['idCabeza_Factura'],
		'FA',
		$fecha_inicio,
		'Vence '.$fecha_vencimiento,
		number_format($factura['total_a_pagar'],2),
		number_format($montomenos,2),
		number_format($factura['credi_saldo'],2),
		$diasvencidas
	);

	foreach($movimientofactura as $position => $movimiento) {

		$aplicada = 'Aplica FA - '.$movimiento['idCabeza_Factura'];
		printf('<tr>
		<td align="center" style="background: ;"><font face="arial" size=3>%s</font></td>
		<td align="center" style="background: ;"><font face="arial" size=3>%s</font></td>
		<td><font face="arial" size=3>%s</font></td>
		<td align="left"><font face="arial" size=3>%s</font></td>
		<td align="right"><font face="arial" size=3>%s</font></td>
		<td align="right"><font face="arial" size=3>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		</tr>',
		$movimiento['idMov'],
		$movimiento['tipmov'],
		date('d-m-Y', strtotime( $movimiento['fecmov'] )),
		$aplicada,
		'',
		number_format($movimiento['monto_movimiento'],2),
		'',
		''
	);
}

}//fin foreach de facturas de crédito
// echo '</tbody>';
// echo '<tbody>';
echo '</tr>
<td align="center" ></td>
<td align="center" width="180" ></td>
<td width="190"><font size=4></font></td>
<td align="left"><font size=4></font></td>
<td align="right"><font size=4><strong>'.number_format($monto_fact,2).'</strong></font></td>
<td align="right"><font size=4><strong>'.number_format($monto_nc_ab,2).'</strong></font></td>
<td align="right"><font size=4><strong>'.number_format($saldo_fact,2).'</strong></font></td>
<td align="center"><font size=4></font></td>
</tr>
</tbody>
</table>';
}//fin foreach clientes
echo '<table width="100%">
<thead>

<tr>
<th></th>
<th></th>
<th style="text-align:right">MONTO (+)</th>
<th style="text-align:right">MONTO (-)</th>
<th style="text-align:right">SALDO</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:right" colspan="2"><font size=5><strong>Totales:</strong></font></td>
<td style="text-align:right"><font size=5>'.number_format($monto_total_fact,2).'</font></td>
<td style="text-align:right"><font size=5>'.number_format($monto_total_nc_ab,2).'</font></td>
<td style="text-align:right"><font size=5>'.number_format($saldo_total_fact,2).'</font></td>
</tr>
</tbody>
</table>';
$no_vencido = $saldo_fact - ($vencido_uno+$vencido_dos+$vencido_tres+$vencido_cuatro);
/*}else{
	echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos un cliente...!</small></h1>';
}*/
?>
<!-- </div> -->
