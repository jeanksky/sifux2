<?php  
use backend\models\Empresa;
use backend\models\OrdenCompraProv;
use backend\models\Proveedores;
use backend\models\ContactosPedidos;

$empresaimagen = new Empresa();
$fechaOrdCompra1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaOrdCompra2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';

echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.$fecha_desde." <strong>Hasta: </strong>".$fecha_hasta.'</p>
      </span>';

//Obtengo todos los proveedores y lo almaceno en el objero $proveedor ademas de que almaceno los que trae en la lista del selec2
$proveedores = Proveedores::find()->all();
if ($lista_proveedores != '0') {
  $proveedores = Proveedores::find()->where("codProveedores IN (".$lista_proveedores.")", [])->all();
}

echo '<table class="items table table-striped">';
foreach ($proveedores as $position => $proveedor) {
  //Obtengo las ordenes de compra a mostrar de los proveedores seleccionados
$ordenCompraProveedores = OrdenCompraProv::find()->where(['=','idProveedor', $proveedor->codProveedores])->andWhere("fecha_registro >= :fechaOrdCompra1 AND fecha_registro <= :fechaOrdCompra2", [':fechaOrdCompra1'=>$fechaOrdCompra1, ':fechaOrdCompra2'=>$fechaOrdCompra2])->orderBy(['idOrdenCompra' => SORT_ASC])->all();//SORT_ASC asendente SORT_DESC desendente

//if que verifica si los proveedores tienen orden de compra si no tienen no los muestra
if (@$ordenCompraProveedores) {

       echo '<thead>
          <tr><th colspan="8" style="text-align:center"><h3><strong>'.$proveedor->nombreEmpresa.'<br><small>Tel: '.$proveedor->telefono.' / '.$proveedor->telefono2.'</small></strong></h3></th></tr>';
          printf('<tr>
    		      	<th style="text-align:center">%s</th>
    		      	<th style="text-align:center">%s</th>
    		      	<th style="text-align:center">%s</th>
    		      	<th style="text-align:center">%s</th>
    		      	<th style="text-align:center">%s</th>
    		      	<th style="text-align:center">%s</th>
          		 </tr>',
                  'N° ORDEN',
                  'N° COMPRA',
                  'FECHA REGISTRO',
                  'FECHA INGRESO',
                  'CONTACTO',
                  'ESTADO'
                  );
          echo '</thead>';
    
echo '<tbody>';
    foreach ($ordenCompraProveedores as $key => $odenCompra) {
      $contactoPedido = ContactosPedidos::find()->where(['=','idContacto_pedido', $odenCompra->idContacto_pedido])->one();
      $fechaR = date('d-m-Y', strtotime( $odenCompra->fecha_registro ));
         
      if ($odenCompra->idCompra == '') {
            $numeroCompra ='<strong><font color="red">(PENDIENTE INGRESO)</font></strong>';
            $fechaI = '<strong><font color="red">(PENDIENTE INGRESO)</font></strong>';
      }else{
          $numeroCompra = $odenCompra->idCompra;
          $fechaI = date('d-m-Y', strtotime( $odenCompra->fecha_ingreso_mercaderia ));
      }
      printf('<tr>
                    <td align="center" ><font size=2>%s</font></td>
                    <td align="center" ><font size=2>%s</font></td>
                    <td align="center"><font size=2>%s</font></td>
                    <td align="center"><font size=2>%s</font></td>
                    <td align="center"><font size=2>%s</font></td>
                    <td align="center"><font size=2>%s</font></td>
                  </tr>',
                       $odenCompra->idOrdenCompra,
                        $numeroCompra,
                        $fechaR,
                        $fechaI,
                        $contactoPedido->nombre,
                        $odenCompra->estado
                    );
    }//fin foreach de ordenCompraProveedores
  }//fin del if que verifica si los proveedores tienen orden de compra
  echo '</tbody>';
}//fin foreach proveedores
echo '</table>';
?>