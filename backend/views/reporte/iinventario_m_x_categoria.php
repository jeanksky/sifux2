<?php
use backend\models\Empresa;
use backend\models\Familia;
use backend\models\ProductoServicios;

$empresaimagen = new Empresa();

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Productos por Categoría</h3>';
  if ($lista_filtro_cagtegoria_inven != '0') {
    $categorias = Familia::find()->where("codFamilia IN (".$lista_filtro_cagtegoria_inven.")", [])->all();
    echo '<table WIDTH="100%">
            <thead>';
        printf('<tr>
                 <th style="text-align:left"><font size=1>%s</font></th>
                 <th style="text-align:left"><font size=1>%s</font></th>
                 <th style="text-align:center"><font size=1>%s</font></th>
                 <th style="text-align:center"><font size=1>%s</font></th>
                 <th style="text-align:right"><font size=1>%s</font></th>
                 <th style="text-align:right"><font size=1>%s</font></th>
                </tr>',
                   'CÓDIGO PRODUCTO',
                   'DESCRIPCIÓN',
                   'UBICACIÓN',
                   'CANTIDAD',
                   'PRECIO VENTA',
                   'PRECIO I.V.I'
                   );
        echo '</thead>';
        echo '<tbody>';
    $suma_total_precio_venta_iv = $suma_total_precio_venta = 0;
    foreach ($categorias as $key => $categoria) {
      if ($filtro_cantidad == 'suficiente') {
        $inventario = ProductoServicios::find()->where(['codFamilia'=>$categoria->codFamilia])
        ->andWhere('cantidadInventario > 0',[])->all();
      } elseif ($filtro_cantidad == 'escasos') {
        $inventario = ProductoServicios::find()->where(['codFamilia'=>$categoria->codFamilia])
        ->andWhere('cantidadInventario = 0',[])->all();
      } else {
        $inventario = ProductoServicios::find()->where(['codFamilia'=>$categoria->codFamilia])->all();
      }
      $total_precio_venta = $total_precio_venta_iv = 0;

      echo '<tr><td colspan="6" align="center"><font size=4>'.$categoria->descripcion.'</font></td></tr>';
      foreach ($inventario as $key => $producto) {
        $suma_total_precio_venta += $producto->precioVenta;
        $suma_total_precio_venta_iv += $producto->precioVentaImpuesto;
        $total_precio_venta += $producto->precioVenta;
        $total_precio_venta_iv += $producto->precioVentaImpuesto;
        printf('<tr>
                  <td align="left" ><font size=1>%s</font></td>
                  <td align="left" ><font size=1>%s</font></td>
                  <td align="center"><font size=1>%s</font></td>
                  <td align="center" ><font size=1>%s</font></td>
                  <td align="right" ><font size=1>%s</font></td>
                  <td align="right"><font size=1>%s</font></td>
                </tr>',
                    $producto->codProdServicio,
                    $producto->nombreProductoServicio,
                    $producto->ubicacion,
                    $producto->cantidadInventario,
                    number_format($producto->precioVenta,2),
                    number_format($producto->precioVentaImpuesto,2)
                  );
      }
      echo '<tr>
        <th></th>
        <th></th>
        <th></th>
        <th style="text-align:right">Total categoría:</th>
        <th style="text-align:right">'.number_format($total_precio_venta,2).'</th>
        <th style="text-align:right">'.number_format($total_precio_venta_iv,2).'</th>
      </tr>';
      echo '<tr><td colspan="6" bgcolor="#7FFFD4" height="70px"></td></tr>';
    }
    echo '</tbody>';
    echo '<tbody>
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th style="text-align:right"><font size=4><strong>Total global:</strong></font></th>
              <th style="text-align:right"><font size=4><strong>'.number_format($suma_total_precio_venta,2).'</strong></font></th>
              <th style="text-align:right"><font size=4><strong>'.number_format($suma_total_precio_venta_iv,2).'</strong></font></th>
            </tr>
          </tbody> ';
echo '</table>';
  }else {
    echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos una categoría</small></h1>';
  }
 ?>
