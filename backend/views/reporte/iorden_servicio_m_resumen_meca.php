<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
h3{
	color: black;
}
</style>
<?php
use backend\models\Empresa;
use backend\models\OrdenServicio;
use backend\models\Mecanicos;

$empresaimagen = new Empresa();
echo '<img src="'. $empresaimagen->getImageurl('html') .'" style="height: 100px;">';
$mecanicos = Mecanicos::find()->all();
if ($lista_filtro_mecanico != '0') {
  $mecanicos = Mecanicos::find()->where("idMecanico IN (".$lista_filtro_mecanico.")", [])->all();
}
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
echo '<br><span style="float:right">
        <p>RANGO DESDE: '.$fecha_desde.' HASTA: '.$fecha_hasta.'</p>
      </span><br><br>';
$fecha_desde = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fecha_hasta = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de Orden de servicio por mecanico</h3>';
      echo '<table class="items table table-striped">';
          echo '<thead>';
          printf('<tr>
                   <th style="text-align:center">%s</th>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:center">%s</th>
                   <th style="text-align:right">%s</th>
                  </tr>',
                     'ID MECÁNICO',
                     'NOMBRE MECÁNICO',
                     'CANT.ORDENES.SERV',
                     'MONTO'
                     );
          echo '</thead>';
          echo '<tbody>';
          $antidad_total = 0;
          $monto_total = 0;
          $imp = 13/100;
            foreach ($mecanicos as $key => $mecanico) {
              $orden_servicios = OrdenServicio::find()->where(['idMecanico'=>$mecanico->idMecanico])
              ->andWhere("estado = '0' AND fecha BETWEEN :fecha_desde AND :fecha_hasta",
              [':fecha_desde'=>$fecha_desde, ':fecha_hasta'=>$fecha_hasta])->all();
              $cantidad = $monto = 0;
              foreach ($orden_servicios as $key => $orden_servicio) {
                $cantidad += 1;
                $monto += $orden_servicio->montoServicio;
                $antidad_total += 1;
                $monto_total += $orden_servicio->montoServicio;
              }
              printf('<tr>
                        <td style="text-align:center">%s</td>
                        <td style="text-align:left">%s</td>
                        <td style="text-align:center">%s</td>
                        <td style="text-align:right">%s</td>
                      </tr>',
                         $mecanico->idMecanico,
                         $mecanico->nombre,
                         $cantidad,
                         number_format($monto + ($monto*$imp),2)
                         );
            }
            printf('<tr>
                     <th style="text-align:center">%s</th>
                     <th style="text-align:right">%s</th>
                     <th style="text-align:center">%s</th>
                     <th style="text-align:right">%s</th>
                    </tr>',
                       '',
                       'TOTAL',
                       $antidad_total,
                       number_format($monto_total + ($monto_total*$imp),2)
                       );
          echo '</tbody>';
      echo '</table>'
 ?>
