<?php
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\CuentasBancarias;
use yii\helpers\ArrayHelper;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\models\EntidadesFinancieras;


$empresaimagen = new Empresa();

$cuentas = CuentasBancarias::find()->all();

$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';
?>
<?php
if ($tipo=='cuentas_bancarias') {
  echo '<div class="col-lg-1">';

  echo '</div>';
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Cuentas bancarias (Saldos)</h3>';
  echo '<div class="helper pull-right col-lg-2">

  <a class="btn btn-default btn-sm"  onclick="imprimir_reporte_index_cuenta_bancaria()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>

  </div>';
  echo '<div class="col-lg-12 col-md-12">
  <br />
  <div class="well">';

  echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
  echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
  echo '<thead class="thead-inverse">';
  printf('<tr>
  <th style="text-align:center"><font face="arial" size=2>%s</font></th>
  <th style="text-align:center"><font face="arial" size=2>%s</font></th>
  <th style="text-align:center"><font face="arial" size=2>%s</font></th>
  <th style="text-align:center"><font face="arial" size=2>%s</font></th>
  <th style="text-align:right"><font face="arial" size=2>%s</font></th>
  <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
  'MONEDA',
  'ENTIDAD FINANCIERA',
  'N° CUENTA',
  'DESCRIPCION',
  'SALDO LIBROS',
  'SALDO BANCOS'
  //'SALDO'
);
echo '</thead>';
echo '<tbody class="">';

foreach($cuentas as $position => $cuenta) {
  $moneda = Moneda::find()->where(['idTipo_moneda'=>$cuenta['idTipo_moneda']])->one();
  $entidades_financieras = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$cuenta['idEntidad_financiera']])->one();

  printf('<tr style="color:#231BB8">
  <td align="center" ><font face="arial" size=2 >%s</font></td>
  <td align="center" width="300" ><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td></td>
  </tr>',
  $moneda->descripcion,
  $entidades_financieras->descripcion,
  $cuenta['numero_cuenta'],
  $cuenta['descripcion'],
  $moneda->simbolo.number_format($cuenta['saldo_libros'],2),
  $moneda->simbolo.number_format($cuenta['saldo_bancos'],2)

);
}//fin del foreach

echo '</tbody>';
echo '</table>';
echo '</div>
</div>';

} else if ($tipo=='movimientos_libros') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Movimientos en libros (Cuentas)</h3>';
  echo '<div class="col-lg-3 col-md-3">
  <label for="idBancos">Cuentas</label>
  '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
    'name' => '',
    'data' => ArrayHelper::map(CuentasBancarias::find()->all(), 'idBancos',
    function($element) {
      return $element['idBancos'].' - '.$element['numero_cuenta'];
    }),
    'options' => [
      'placeholder' => '- Seleccione -',
      'id'=>'idBancos',
      //'onchange' => 'javascript:buscar_factura_cancelada()',
      'multiple' => false //esto me ayuda a que solo se obtenga uno
    ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
    ]).'
    </div>
    <div class="col-lg-3 col-md-3">
    <label for="fecha_desde2">Fecha Inicio</label>
    '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
    'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
    'id'=>'fecha_desde2','autocomplete'=>'off']]).'
    </div>
    <div class="col-lg-3 col-md-3">
    <label for="fecha_hasta2">Fecha Fin</label>
    '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
    'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
    'id'=>'fecha_hasta2','autocomplete'=>'off']]).'
    </div>
    <div class="col-lg-3 col-md-30" style="margin-top:25px;">
    <a class="btn btn-default btn-sm" onclick="buscar_reporte_cuenta_bancaria()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
    <a class="btn btn-default btn-sm" id="imp_re_cb" onclick="imprimir_reporte_cuenta_bancaria()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
    </div>
    ';
    echo '<div class="col-lg-12"><br>
    <div class="well" id="mostrar_estado_cuenta_por_cuenta_bancaria"></div>
    </div>';
  }
  ?>

  <script type="text/javascript">
  $(document).ready(function () {
    $('#imp_re_cb').addClass('disabled');//mantenemos el boton de imprimir desabilitado
  });
  function buscar_reporte_cuenta_bancaria() {
    idBancos = document.getElementById('idBancos').value;
    fecha_desde2 = document.getElementById('fecha_desde2').value;
    fecha_hasta2 = document.getElementById('fecha_hasta2').value;
    document.getElementById("mostrar_estado_cuenta_por_cuenta_bancaria").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rbancos_m_movimiento_libros') ?>" ,
    { 'idBancos' : idBancos, 'fecha_desde2' : fecha_desde2, 'fecha_hasta2' : fecha_hasta2 } ,
    function( data ) {
      $('#mostrar_estado_cuenta_por_cuenta_bancaria').html(data);
      $('#imp_re_cb').removeClass('disabled');
    }).fail(function() {
      document.getElementById("mostrar_estado_cuenta_por_cuenta_bancaria").innerHTML = '<center><h2><small>Seleccione al menos una cuenta...!</small></h2></center>';
    });
  }
  /*Este reporte imprime los Movimientos en libros*/
  function imprimir_reporte_cuenta_bancaria() {
    idBancos = document.getElementById('idBancos').value;
    fecha_desde2 = document.getElementById('fecha_desde2').value;
    fecha_hasta2 = document.getElementById('fecha_hasta2').value;
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/ibancos_m_movimiento_libros') ?>" ,
    { 'idBancos' : idBancos, 'fecha_desde2' : fecha_desde2, 'fecha_hasta2' : fecha_hasta2 } ,
    function( data ) {
      var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

      ventimp.document.write(data);
      ventimp.document.close();
      ventimp.print();
      ventimp.close();
    });
  }

  function imprimir_reporte_index_cuenta_bancaria() {
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/ibancos_m_cuentas_bancarias') ?>" ,
    function( data ) {
      var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

      ventimp.document.write(data);
      ventimp.document.close();
      ventimp.print();
      ventimp.close();
    });
  }
</script>
