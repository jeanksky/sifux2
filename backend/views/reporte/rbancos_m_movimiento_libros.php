<?php
use backend\models\CuentasBancarias;
use backend\models\MovimientoLibros;
use backend\models\MedioPagoProveedor;
use backend\models\TramitePago;
use backend\models\Moneda;
use backend\models\EntidadesFinancieras;
use backend\models\TipoDocumento;
use backend\models\Empresa;

$empresaimagen = new Empresa();

$cuenta = CuentasBancarias::find()->where(['idBancos'=>$idBancos])->one();
$moneda = Moneda::find()->where(['idTipo_moneda'=>$cuenta->idTipo_moneda])->one();
$entidades_financieras = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$cuenta->idEntidad_financiera])->one();

$fechaFactura1 = $fecha_desde2 != '' ? date('Y-m-d', strtotime( $fecha_desde2 )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta2 != '' ? date('Y-m-d', strtotime( $fecha_hasta2 )) : '3890-01-01';

/*$mediopagoproveedor = MedioPagoProveedor::find()->where(['=','cuenta_bancaria_local', $cuenta->idBancos])->andWhere("fecha_documento >= :fechaFactura1 AND fecha_documento <= :fechaFactura2", [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2])->orderBy(['idMedio_pago' => SORT_ASC])->all();*/

$libros = MovimientoLibros::find()->where(['=','idCuenta_bancaria', $cuenta->idBancos])->andWhere("fecha_documento >= :fechaFactura1 AND fecha_documento <= :fechaFactura2", [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2])->orderBy(['idMovimiento_libro' => SORT_ASC])->all();

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y',strtotime($fechaFactura1))." <strong>Hasta: </strong>".Date('d-m-Y',strtotime($fechaFactura2)).'</p>
( TR ) = Transferencia bancaria.  ( CH ) = Cheque.</span>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
echo '<thead class="thead-inverse">';
echo '<tr>
<th colspan="7" style="text-align:center">
<h3>CUENTA BANCARIA: '.$cuenta->numero_cuenta.' - '.$moneda->descripcion.'</h3>
ENTIDAD FINANCIERA: '.$entidades_financieras->descripcion.'
</th>
</tr>';
printf('<tr>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
//'POS FECHA',
'FECHA',
//'MEDIO PAGO',
'BENEFICIARIO',
'DETALLE',
'N° TRANSACCION',
'DÉBITO',
'CRÉDITO',
'SALDO'
);
echo '</thead>';
echo '<tbody class="">';
$simb = '';
$monto_total_debito = $monto_total_credito = 0;
foreach($libros as $position => $libro) {
  $fecha = date('d-m-Y', strtotime( $libro['fecha_documento'] ));
  $montodebito = ''; $montocredito = '';
  $posFecha = '';
  $medio_pago = '';
  $mediopagoproveedor = MedioPagoProveedor::find()->where(['=','idMedio_pago', $libro['idMedio_pago']])->one();
  if (@$mediopagoproveedor) {
    $medio_pago = $mediopagoproveedor->medio_pago == 'cheque' ? ' - CH' : ' - TR';
    if ($mediopagoproveedor->posFecha == 'Posfecha') {
      $posFecha = date('d-m-Y', strtotime( $mediopagoproveedor->fecha_documento ));
    }
  }
  if($td = TipoDocumento::findOne($libro['idTipodocumento']))
  {
    $mo = Moneda::findOne($libro['idTipoMoneda']);
    $montodebito = $td->tipo_movimiento == 'Débito' ? $mo->simbolo.number_format($libro['monto'],2) : '';
    $montocredito = $td->tipo_movimiento == 'Crédito' ? $mo->simbolo.number_format($libro['monto'],2) : '';
    $simb = $mo->simbolo;
    $monto_total_debito += $td->tipo_movimiento == 'Débito' ? $libro['monto'] : 0;
    $monto_total_credito += $td->tipo_movimiento == 'Crédito' ? $libro['monto'] : 0;
  }

  //$posFecha = $mediopagoproveedor->posFecha;
  printf('<tr style="color:#231BB8">
  <td align="center" ><font face="arial" size=2 >%s</font></td>
  <td align="center" width="180" ><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  </tr>',
  //$posFecha,
  $fecha,
  //strtoupper($medio_pago),
  $libro['beneficiario'],
  $libro['detalle'],
  $libro['numero_documento'].$medio_pago,
  $montodebito,
  $montocredito,
  $simb.number_format($libro['saldo'],2)
);
}
$monto_total_saldo = $monto_total_credito - $monto_total_debito;

printf('<tr>
<td colspan="4" style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
</tr>',
'<h4>Totales:</h4>',
'<h4><strong>'.$simb.number_format($monto_total_debito,2).'</strong></h4>',
'<h4><strong>'.$simb.number_format($monto_total_credito,2).'</strong></h4>',
''
);
printf('<tr>
<td colspan="6" style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
</tr>',
'<h4>Diferiencia de créditos - débitos:</h4>',
'<h4><strong>'.$simb.number_format($monto_total_saldo,2).'</strong></h4>'
);
echo '</tbody>';
echo '</table>';
?>
