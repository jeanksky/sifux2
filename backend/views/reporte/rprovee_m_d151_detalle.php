<?php
use backend\models\Compra;
use backend\models\Empresa;
use backend\models\Proveedores;

$empresaimagen = new Empresa();

/*Consultas de años*/
// $anoSeleccionado = $anoSeleccionado;
/*vehiel 04-12-2018
si no se selecciono ningun año, entonces no se podra restar nada*/
if ($anoSeleccionado != '0'){
  $anoConsultar_desde = $anoSeleccionado - 1;
}

$anoPasado = date('Y', strtotime('-1 year'));
$anoActual = $anoPasado + 1;

$fechaFactura1 = $anoSeleccionado != '0' ? $anoConsultar_desde.'-10-01' : $anoPasado.'-10-01';
$fechaFactura2 = $anoSeleccionado != '0' ? $anoSeleccionado.'-09-30' : $anoActual.'-09-30';

/*debemos obtener los clientes*/
if($lista_proveedores != '0'){
  $proveedores = Proveedores::find()->where('codProveedores IN ('.$lista_proveedores.')',[])->all();


  /*Fechas en el formato para personas*/
  $f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
  $f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

  echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
  echo '<span style="float:right">
  <p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
  </span>';
  /*para cada proveedor se va a mostrar un table, recorremos los proveedores y validamos el sum d151 para mostrar los datos*/
  foreach ($proveedores as $key => $proveedor) {
    /*si se le pasa el valor de codProveedores directo en el create comman produce un error*/
    $codProveedores = $proveedor['codProveedores'];
    /*buscamos el monto total para validar que sea mayor o igual al parametro de monto minimo de MIN*/
    $sql = "SELECT SUM(subTotal) AS sumD151 FROM `tbl_compras`
    WHERE idProveedor = :proveedor AND idCompra NOT IN (SELECT fact.idCompra FROM tbl_compras fact INNER JOIN
      (SELECT idCompra, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_pagar` WHERE tipo_mov = 'nc' GROUP BY idCompra) fact_not
      ON fact.idCompra = fact_not.idCompra
      WHERE fact.total = fact_not.suma_nc)
      AND fechaDocumento BETWEEN :fechaFactura1 AND :fechaFactura2  GROUP BY idProveedor DESC;";

      $command = \Yii::$app->db->createCommand($sql);
      $command->bindParam(":fechaFactura1", $fechaFactura1);
      $command->bindParam(":fechaFactura2", $fechaFactura2);
      $command->bindParam(":proveedor", $codProveedores);
      $montoD151 = $command->queryOne();
      /*al hacer un queryOne nos trae un array simple*/
      if($montoD151['sumD151'] >= Yii::$app->params['limite_monto_compra_MIN']){/*Limite impuesto por el MIN en compra de articulos. ¢2,500,000*/

        echo '<table class="items table table-striped" id="">';
        echo '<thead class="thead-inverse">
        <tr><th colspan="6" style="text-align:center">
        <h3>'.$proveedor['codProveedores'].' - '.$proveedor['nombreEmpresa'].'<br>
        <small>N° Cédula: '.$proveedor['cedula'].' / Tel: '.$proveedor['telefono'].'</small></h3>
        </th></tr>';
        printf('<tr>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
        'N° Interno',
        'N° Factura Electrónica',
        'Fecha Documento',
        'Subtotal',
        'Descuento',
        'Impuesto',
        'Total'
      );
      echo '<tbody class="">';
      echo '</thead>';
      echo '<tbody class="">';

      $sql = "SELECT n_interno_prov AS n_interno, numFactura AS n_fe, subTotal AS subtotal,
      descuento, impuesto, total, idProveedor, fechaDocumento FROM `tbl_compras` WHERE
      idProveedor = :proveedor AND idCompra NOT IN (SELECT fact.idCompra FROM tbl_compras fact
        INNER JOIN (SELECT idCompra, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_pagar`
        WHERE tipo_mov = 'nc' GROUP BY idCompra) fact_not ON fact.idCompra = fact_not.idCompra
        WHERE fact.total = fact_not.suma_nc) AND fechaDocumento BETWEEN :fechaFactura1 AND
        :fechaFactura2 ORDER BY fechaDocumento ASC;";

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindParam(":fechaFactura1", $fechaFactura1);
        $command->bindParam(":fechaFactura2", $fechaFactura2);
        $command->bindParam(":proveedor", $codProveedores);
        $compras = $command->queryAll();
        $declarar = $mt_subtotal = $mt_descuento = 0;
      foreach($compras as $key => $compra) {

            $mt_subtotal += $compra['subtotal'];
            $mt_descuento += $compra['descuento'];

           printf('<tr style="color:#231BB8">
            <td align="center" ><font face="arial" size=2 >%s</font></td>
            <td align="center"  ><font size=2>%s</font></td>
            <td align="center"  ><font size=2>%s</font></td>
            <td align="right"><font size=2>%s</font></td>
            <td align="right"><font size=2>%s</font></td>
            <td align="right" ><font face="arial" size=2 >%s</font></td>
            <td align="right" ><font size=2>%s</font></td>
            </tr>',
            $compra['n_interno'],
            $compra['n_fe'],
            Date('d-m-Y',strtotime($compra['fechaDocumento'])),
            number_format($compra['subtotal'],2),
            number_format($compra['descuento'],2),
            number_format($compra['impuesto'],2),
            number_format($compra['total'],2)
          );
    }//fin foreach

    printf('<tr>
    <td colspan="3" style="text-align:right"><font size=3>%s</font></td>
    <td style="text-align:right"><font size=3>%s</font></td>
    <td style="text-align:right"><font size=3>%s</font></td>
    <td colspan="2" style="text-align:right"><font size=3>%s</font></td>
    </tr>',
    '<h4>Totales: </h4>',
    '<h4><strong>'.number_format($mt_subtotal,2).'</strong></h4>',
    '<h4><strong>'.number_format($mt_descuento,2).'</strong></h4>',
    '<h4>Declarar: <strong>'.number_format(($mt_subtotal),2).'</strong></h4>'
  );
  echo '</tbody>';
  echo '</table>';
}/*fin if sumD151 del cliente es mayor que el minimo impuesto por MIN*/
}/*fin foreach clientes*/
}// fin if del if lista_clientes
else {
  echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos un proveedor...!</small></h1>';
}
?>
