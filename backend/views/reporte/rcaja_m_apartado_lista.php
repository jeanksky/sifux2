<?php
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\ClientesApartados;
use backend\models\MovimientoApartado;

$empresaimagen = new Empresa();
$todos_clientes = Clientes::find()
->innerJoin(['tbl_encabezado_factura'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
->where(['estadoFactura'=>'Apartado'])
->all();
 $todos_idCliente = '';
 foreach ($todos_clientes as $key => $value) { $todos_idCliente .= $value->idCliente.','; }
 $fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
 $fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
 $fechaAbonos1 = $fecha_desde_ab != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde_ab )) : '1890-01-01 00:00:00';
 $fechaAbonos2 = $fecha_hasta_ab != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta_ab )) : '3890-01-01 23:59:59';
 $lista_clientes = $lista_clientes == '0' ? substr($todos_idCliente, 0, -1) : $lista_clientes;
 $codigoVendedor = $filtro_vendedor_apartado == '' ? " LIKE '%%'" : "= ".$filtro_vendedor_apartado;
/*$mediopagoproveedor = MedioPagoProveedor::find()->where(['=','cuenta_bancaria_local', $cuenta->idBancos])->andWhere("fecha_documento >= :fechaFactura1 AND fecha_documento <= :fechaFactura2", [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2])->orderBy(['idMedio_pago' => SORT_ASC])->all();*/


  $ventas = EncabezadoPrefactura::find()->where(['estadoFactura'=>'Apartado'])
  ->andWhere("idCliente IN (".$lista_clientes.") AND fecha_inicio BETWEEN :fechaFactura1 AND :fechaFactura2 AND codigoVendedor ".$codigoVendedor,
  [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2 ])->all();
  echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
  echo '<span style="float:right">
          <p>RANGO: <strong>Desde: </strong>'.$fecha_desde." <strong>Hasta: </strong>".$fecha_hasta.'</p>
        </span>';
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Lista de apartados pendientes</h3>';
  echo '<table class="items table table-striped" id="tabla_facturas_pendientes" width="100%" >';
  echo '<thead class="thead-inverse">';
                  printf('<tr>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                      <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
                          'ID Documento',
                          'Registro',
                          'Vencimiento',
                          'Cliente',
                          'Estado',
                          'Monto',
                          'Abono',
                          'Saldo'
                          );
                  echo '</thead>';
  echo '<tbody class="">';
  $monto_total_apartado = $monto_total_abono = $monto_total_saldo = 0;
  foreach($ventas as $factura) {

  	$fecha = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
    $nombre_cliente = $fecha_vencimiento = '';
    if($cliente = Clientes::findOne($factura['idCliente']))
    {
        $nombre_cliente = $cliente->nombreCompleto;
        $cliente_apartado = ClientesApartados::find()->where(['idCliente'=>$cliente->idCliente])->one();
        $fecha_vencimiento = strtotime ( '+'.$cliente_apartado->dias_apartado.' day' , strtotime ( $factura['fecha_inicio'] ) ) ;
    }else{
        $nombre_cliente = $factura['idCliente'];
    }
    if ($fecha_vencimiento < strtotime(date('Y-m-d'))) {
      $vencido = ' Vencido';
    }else {
      $vencido = ' al día';
    }
    $abono = 0;
    $movi_adelanto = MovimientoApartado::find()
    ->where(['idCabeza_Factura'=>$factura['idCabeza_Factura']])
    ->andWhere("fecmov BETWEEN :fechaAbonos1 AND :fechaAbonos2",
      [':fechaAbonos1'=>$fechaAbonos1, ':fechaAbonos2'=>$fechaAbonos2 ])
    ->all();
    $abonos = '';
    $abonos_html = '';
    if (@$movi_adelanto) {
      foreach ($movi_adelanto as $key => $adelanto) {
        $abono += $adelanto->monto_movimiento;
        $abonos_html .= '
        <tr>
          <td align="center" width="100"><font face="arial" size=2 >'.$adelanto->idMov.'</font></td>
          <td colspan="2" align="left" width="100"><font size=2>'.date('d-m-Y // h:m:s A', strtotime( $adelanto->fecmov )).'</font></td>
          <td align="left" width="400"><font size=2></font></td>
          <td align="center" width="150"><font size=2></font></td>
          <td align="right" width="80"><font size=2>'.number_format($adelanto->monto_anterior,2).'</font></td>
          <td align="right" width="80"><font size=2>'.number_format($adelanto->monto_movimiento,2).'</font></td>
          <td align="right" width="80"><font size=2>'.number_format($adelanto->saldo_pendiente,2).'</font></td>
        </tr>';
      }
    }
    $saldo = $factura['total_a_pagar'] - $abono;
    $apartados_html = '<tr style="color:#231BB8">
              <td align="center" width="100"><font face="arial" size=2 >'.$factura['idCabeza_Factura'].'</font></td>
              <td align="center" width="100"><font size=2>'.$fecha.'</font></td>
              <td align="center" width="100"><font size=2>'.date('d-m-Y', $fecha_vencimiento ).'</font></td>
              <td align="left" width="400"><font size=2>'.$nombre_cliente.'</font></td>
              <td align="center" width="150"><font size=2>'.'Apartado'.$vencido.'</font></td>
              <td align="right" width="80"><font size=2>'.number_format($factura['total_a_pagar'],2).'</font></td>
              <td align="right" width="80"><font size=2>'.number_format($abono,2).'</font></td>
              <td align="right" width="80"><font size=2>'.number_format($saldo,2).'</font></td>
            </tr>';
    if ($apart_vencidas == 'si') {
      if ($fecha_vencimiento < strtotime(date('Y-m-d'))) {
        foreach ($movi_adelanto as $key => $adelanto) { $monto_total_abono += $adelanto->monto_movimiento; }
        if ($incluir_abono_apart == 'si') {
          if ($abono > 0) {
            $monto_total_apartado +=  $factura['total_a_pagar'];
            echo $apartados_html;
          } else {
            // code...
          }
          echo $abonos_html;
        } else {
          $monto_total_apartado +=  $factura['total_a_pagar'];
          echo $apartados_html;
        }
      }
    } else if ($apart_al_dia == 'si') {
      if ($fecha_vencimiento >= strtotime(date('Y-m-d'))) {
        foreach ($movi_adelanto as $key => $adelanto) { $monto_total_abono += $adelanto->monto_movimiento; }
        if ($incluir_abono_apart == 'si') {
          if ($abono > 0) {
            $monto_total_apartado +=  $factura['total_a_pagar'];
            echo $apartados_html;
          } else {
            // code...
          }
          echo $abonos_html;
        } else {
          $monto_total_apartado +=  $factura['total_a_pagar'];
          echo $apartados_html;
        }
      }
    } else {


      foreach ($movi_adelanto as $key => $adelanto) { $monto_total_abono += $adelanto->monto_movimiento; }
      if ($incluir_abono_apart == 'si') {
        if ($abono > 0) {
          $monto_total_apartado +=  $factura['total_a_pagar'];
          echo $apartados_html;
        } else {
          // code...
        }
        echo $abonos_html;
      } else {
        $monto_total_apartado +=  $factura['total_a_pagar'];
        echo $apartados_html;
      }
    }

  }
  $monto_total_saldo = $monto_total_apartado - $monto_total_abono;
  printf('<tr>
            <td style="text-align:center" ><font size=3>%s</font></td>
            <td style="text-align:right"><font size=3>%s</font></td>
            <td style="text-align:right"><font size=3>%s</font></td>
            <td style="text-align:left"><font size=3>%s</font></td>
            <td style="text-align:left"><font size=3>%s</font></td>
            <td style="text-align:right" width="190"><font size=3>%s</font></td>
            <td style="text-align:right" width="190"><font size=3>%s</font></td>
            <td style="text-align:right" width="190"><font size=3>%s</font></td>
          </tr>',
              '',
              '',
              '',
              '',
              '<h4>Total:</h4>',
              '<h4> <strong>'.number_format($monto_total_apartado,2).'</strong></h4>',
              '<h4> <strong>'.number_format($monto_total_abono,2).'</strong></h4>',
              '<h4> <strong>'.number_format($monto_total_saldo,2).'</strong></h4>'
          );
  echo '</tbody>';
  echo '</table>';
  ?>
