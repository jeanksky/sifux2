<style type="text/css">
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 8px;
  text-align: center;
  border-bottom: 1px solid #ddd;
}
th {
  background-color: #4CAF50;
  color: black;
}
</style>
<?php
/*vehiel 30-11-2018*/
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\AgentesComisiones;

$empresaimagen = new Empresa();

/*Consultas de años*/
$anoConsultar = $anoSeleccionado;
/*vehiel 30-11-2018
si no se selecciono ningun año, entonces no se podra restar nada*/
if ($anoConsultar != '0'){
  $anoConsultar_desde = $anoConsultar - 1;
}

$anoPasado = date('Y', strtotime('-1 year'));
$anoActual = $anoPasado + 1;

$fechaFactura1 = $anoConsultar != '0' ? $anoConsultar_desde.'-10-01' : $anoPasado.'-10-01';
$fechaFactura2 = $anoConsultar != '0' ? $anoConsultar.'-09-30' : $anoActual.'-09-30';

/*debemos obtener los clientes*/
if($lista_clientes != '0'){
  $clientes = Clientes::find()->where('idCliente IN ('.$lista_clientes.')',[])->all();


  /*Fechas en el formato para personas*/
  $f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
  $f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

  echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
  echo '<span style="float:right">
  <p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
  <p>Emitido: '.Date('d-m-Y h:i:s A').'</p>
  </span>';
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> D-151 Detalle de Clientes</h3>';
  /*para cada cliente se va a mostrar un table, recorremos los clientes y validamos el sum d151 para mostrar los datos*/
  foreach ($clientes as $key => $cliente) {
    /*si se le pasa el valor de idCliente directo en el create comman produce un error*/
    $idCliente = $cliente['idCliente'];
    /*buscamos el monto total para validar que sea mayor o igual al parametro de monto minimo de MIN*/
    $sql = "SELECT SUM(subtotal) AS sumD151 FROM `tbl_encabezado_factura`
    WHERE idCliente = :cliente AND idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
      (SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC'
      GROUP BY idCabeza_Factura) fact_not
      ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
      WHERE fact.total_a_pagar = fact_not.suma_nc)
      AND fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura !='Anulada'  GROUP BY idCliente DESC;";

      $command = \Yii::$app->db->createCommand($sql);
      $command->bindParam(":fechaFactura1", $fechaFactura1);
      $command->bindParam(":fechaFactura2", $fechaFactura2);
      $command->bindParam(":cliente", $idCliente);
      $montoD151 = $command->queryOne();/*al hacer un queryOne nos trae un array simple*/

      if($montoD151['sumD151'] >= Yii::$app->params['limite_monto_venta_MIN']){/*Limite impuesto por el MIN en venta de articulos. ¢2,500,000*/

        echo '<table class="items table table-striped" id="">';
        echo '<thead class="thead-inverse">
        <tr><th align="center" colspan="6">
        <h3>'.$cliente['idCliente'].' - '.$cliente['nombreCompleto'].'<br>
        <small>N° Cédula: '.$cliente['identificacion'].'/ Tel: '.$cliente['telefono'].' '.$cliente['celular'].'</small></h3>
        </th></tr>';
        printf('<tr>
        <th ><font face="arial" size=2>%s</font></th>
        <th ><font face="arial" size=2>%s</font></th>
        <th ><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
        'N° Interno',
        'N° Factura Electrónica',
        'Fecha Emisión',
        'Subtotal',
        'Descuento',
        'Impuesto',
        'Total'
      );
      echo '<tbody class="">';
      echo '</thead>';
      echo '<tbody class="">';

      $sql = "SELECT idCabeza_Factura AS n_interno, fe AS n_fe, subtotal, porc_descuento AS descuento,
      iva AS impuesto, total_a_pagar AS total, idCliente, fecha_emision FROM `tbl_encabezado_factura`
      WHERE idCliente = :cliente AND idCabeza_Factura NOT IN (SELECT fact.idCabeza_Factura FROM tbl_encabezado_factura fact INNER JOIN
        (SELECT idCabeza_Factura, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_cobrar` WHERE tipmov = 'NC'
        GROUP BY idCabeza_Factura) fact_not
        ON fact.idCabeza_Factura = fact_not.idCabeza_Factura
        WHERE fact.total_a_pagar = fact_not.suma_nc)
        AND fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2  AND estadoFactura !='Anulada'";

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindParam(":fechaFactura1", $fechaFactura1);
        $command->bindParam(":fechaFactura2", $fechaFactura2);
        $command->bindParam(":cliente", $idCliente);
        $facturas = $command->queryAll();
        $declarar = $mt_subtotal = $mt_descuento = 0;
        foreach($facturas as $key => $factura) {

          /*Datos Clientes*/
          $nCedulaCliente = '';
          $tipoIdentidadCliente = '';
          $nombre_cliente = '';
          $clienteTelefono = '';
          $clienteCelular = '';
          $codigoMIN = 'V';

          $nCedulaCliente = $cliente->identificacion;
          $tipoIdentidadCliente =  $cliente->tipoIdentidad;
          $nombre_cliente = $cliente->nombreCompleto;
          $clienteTelefono = $cliente->telefono;
          $clienteCelular = $cliente->celular;

          $mt_subtotal += $factura['subtotal'];
          $mt_descuento += $factura['descuento'];

          printf('<tr style="color:#231BB8">
          <td ><font face="arial" size=2 >%s</font></td>
          <td ><font size=2>%s</font></td>
          <td ><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="right" ><font face="arial" size=2 >%s</font></td>
          <td align="right" ><font size=2>%s</font></td>
          </tr>',
          $factura['n_interno'],
          $factura['n_fe'],
          Date('d-m-Y',strtotime($factura['fecha_emision'])),
          number_format($factura['subtotal'],2),
          number_format($factura['descuento'],2),
          number_format($factura['impuesto'],2),
          number_format($factura['total'],2)
        );
      }//fin foreach

      printf('<tr>
      <td colspan="3" style="text-align:right"><font size=3>%s</font></td>
      <td style="text-align:right"><font size=3>%s</font></td>
      <td style="text-align:right"><font size=3>%s</font></td>
      <td colspan="2" style="text-align:right"><font size=3>%s</font></td>
      </tr>',
      '<h4>Totales: </h4>',
      '<h4><strong>'.number_format($mt_subtotal,2).'</strong></h4>',
      '<h4><strong>'.number_format($mt_descuento,2).'</strong></h4>',
      '<h4>Declarar: <strong>'.number_format(($mt_subtotal),2).'</strong></h4>'
    );
    echo '</tbody>';
    echo '</table>';
  }/*fin if sumD151 del cliente es mayor que el minimo impuesto por MIN*/
}/*fin foreach clientes*/
}// fin if del if lista_clientes
else {
  echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos un cliente...!</small></h1>';
}
?>
