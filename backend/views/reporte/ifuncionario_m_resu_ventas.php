<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
</style>
<?php
use backend\models\Funcionario;
use backend\models\Empresa;
use backend\models\FacturasDia;

$empresaimagen = new Empresa();
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO DESDE: '.$fecha_desde.' HASTA: '.$fecha_hasta.'</p>
      </span>';
      $fechaInicio1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
      $fechaInicio2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
      $funcionarios = Funcionario::find()->all();
      if ($lista_filtro_funcionarios != '0') {
        $funcionarios = Funcionario::find()->where("idFuncionario IN (".$lista_filtro_funcionarios.")", [])->all();
      }
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen de ventas por Funcionario</h3>';
echo '<table class="items table table-striped">
        <thead>
        <th style="text-align:center">ID FUNCIONARIO</th>
        <th style="text-align:left">NOMBRE COMPLETO</th>
        <th style="text-align:center">CANTIDAD DE VENTAS</th>
        <th style="text-align:right">MONTO VENTA</th>
        </thead>
        <tbody>';
        $total_cantidad = $total_venta = 0;
        foreach ($funcionarios as $key => $funcionario) {
          $ventas = FacturasDia::find()->where(['codigoVendedor'=>$funcionario->idFuncionario])
          ->andWhere("estadoFactura <> 'Anulada' AND fecha_inicio BETWEEN :fechaInicio1 AND :fechaInicio2", [':fechaInicio1'=>$fechaInicio1, ':fechaInicio2'=>$fechaInicio2])->all();
          if (@$ventas) {
            $monto_venta = 0; $cantidad = 0;
            foreach ($ventas as $turn => $venta) {
              $monto_venta += $venta->total_a_pagar;
              $cantidad +=1;
              $total_cantidad +=1;
              $total_venta += $venta->total_a_pagar;
            }
            printf('<tr>
                     <td style="text-align:center">%s</td>
                     <td style="text-align:left">%s</td>
                     <td style="text-align:center">%s</td>
                     <td style="text-align:right">%s</td>
                    </tr>',
                       $funcionario->idFuncionario,
                       $funcionario->nombre.' '.$funcionario->apellido1.' '.$funcionario->apellido2,
                       $cantidad,
                       number_format($monto_venta,2)
                       );
          }
        }
echo '    <tr>
           <td style="text-align:center"></td>
           <td style="text-align:right"><strong>TOTAL:</strong></td>
           <td style="text-align:center"><strong>'.$total_cantidad.'</strong></td>
           <td style="text-align:right"><strong>'.number_format($total_venta,2).'</strong></td>
          </tr>
        <tbody>
      </table>';
