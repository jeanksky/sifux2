<?php
use backend\models\MovimientoCobrar;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\Gastos;
use backend\models\MedioPago;
use backend\models\Empresa;
use backend\models\FacturasDia;
use backend\models\MovimientoApartado;

$empresaimagen = new Empresa();

$contado = 0;             $efectivo = 0;
$credito = 0;             $deposito = 0;
$total_ventas = 0;        $tarjetas = 0;
$abono_credito = 0;       $cheques = 0;
$total_ingresos = 0;      $dollar = 0;
$gastos = 0;              $total = 0;
$notas_credito = 0;       $apartado_cancelado = 0;
$total_caja = 0;

$efectivo_nc = 0;
$deposito_nc = 0;
$tarjetas_nc = 0;
$cheques_nc = 0;

$fecha_d = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fecha_h = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
$fecha_hora_d = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
$fecha_hora_h = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';

//JEANK SQL que suma todas las ventas de credito pero aquellas son anuladas
$sql_credito = "SELECT SUM(total_a_pagar) AS sum_ventas, tipoFacturacion FROM `tbl_encabezado_factura`
WHERE fecha_emision BETWEEN :fecha_d AND :fecha_h AND estadoFactura != 'Anulada'
AND tipoFacturacion = '5' GROUP BY tipoFacturacion DESC;";
$command_cr = \Yii::$app->db->createCommand($sql_credito);
$command_cr->bindParam(":fecha_d", $fecha_d);
$command_cr->bindParam(":fecha_h", $fecha_h);
$ventas_cr = $command_cr->queryOne();
// foreach($ventas_cr as $factura) {
//     if ($factura['tipoFacturacion'] == '4') {
      $credito = $ventas_cr['sum_ventas'];
//     }//fin del if
// }//fin del foreach

/*Version credito NelGamb
$sql_credito = "SELECT * FROM `tbl_encabezado_factura`
WHERE fecha_final BETWEEN :fecha_d AND :fecha_h;";
$command_cr = \Yii::$app->db->createCommand($sql_credito);
$command_cr->bindParam(":fecha_d", $fecha_d);
$command_cr->bindParam(":fecha_h", $fecha_h);
$ventas_cr = $command_cr->queryAll();
foreach($ventas_cr as $factura) {
  if ($factura['tipoFacturacion'] != 'Ninguno') {
    if ($factura['tipoFacturacion'] == '4') {
      if ($factura['estadoFactura'] != 'Anulada') {
        $credito += $factura['total_a_pagar'];
      }
    }
  }
}//fin foreach  */

$sql_contado_y_cancelacion = "SELECT SUM(total_a_pagar) as sumContado FROM `tbl_encabezado_factura`
WHERE estadoFactura <> 'Anulada' AND fecha_emision BETWEEN :fecha_d AND :fecha_h
AND  tipoFacturacion NOT IN ('Ninguno','5');";
$command_co_ca = \Yii::$app->db->createCommand($sql_contado_y_cancelacion);
$command_co_ca->bindParam(":fecha_d", $fecha_d);
$command_co_ca->bindParam(":fecha_h", $fecha_h);
$ventas_co_ca = $command_co_ca->queryOne();
// foreach($ventas_co_ca as $factura) {
  // if ($factura['tipoFacturacion'] != 'Ninguno') {
  //   if ($factura['tipoFacturacion'] != '4') {
      $contado += $ventas_co_ca['sumContado'];
  //   }
  // }
// }
$total_ventas = $contado + $credito;
//------------------------------------me suma las facturas canceladas que fueron apartados
$sql_apartado_y_cancelacion = "SELECT SUM(total_a_pagar) as sumApartado FROM `tbl_encabezado_factura`
WHERE estadoFactura <> 'Anulada' AND apartado = 'x' AND fecha_emision BETWEEN :fecha_d AND :fecha_h
AND  tipoFacturacion NOT IN ('Ninguno','5');";
$command_apa_ca = \Yii::$app->db->createCommand($sql_apartado_y_cancelacion);
$command_apa_ca->bindParam(":fecha_d", $fecha_d);
$command_apa_ca->bindParam(":fecha_h", $fecha_h);
$ventas_apa_ca = $command_apa_ca->queryOne();

$apartado_cancelado += $ventas_apa_ca['sumApartado'];

//OBTENER MONTO DE ABONOS Y CANCELACIONES
$montoAbonos = Recibos::find()->select('SUM(monto) as monto')
->where([ 'between', 'fechaRecibo', $fecha_hora_d, $fecha_hora_h ])
->andWhere(['tipoRecibo'=>'AB'])
->asArray()->One();
$abono_credito += $montoAbonos['monto'];

$cancelacion_credito = 0;
$montoCancelaciones = Recibos::find()->select('SUM(monto) as monto')
->where([ 'between', 'fechaRecibo', $fecha_hora_d, $fecha_hora_h ])
->andWhere(['tipoRecibo'=>'CF'])
->asArray()->One();
$cancelacion_credito += $montoCancelaciones['monto'];


$abono_apartados = 0;
$montoAbono_apartado = MovimientoApartado::find()->select('SUM(monto_movimiento) as monto')
->where([ 'between', 'fecmov', $fecha_hora_d, $fecha_hora_h ])
->asArray()->One();
$abono_apartados += $montoAbono_apartado['monto'];
/*SELECT SUM(r.monto) FROM tbl_recibos r INNER JOIN tbl_recibos_detalle rd ON rd.`idRecibo` = r.`idRecibo`
INNER JOIN tbl_movi_cobrar mc ON mc.`idMov` = rd.`idDocumento`
INNER JOIN tbl_encabezado_factura ef ON ef.`idCabeza_Factura` = mc.`idCabeza_Factura`
 WHERE r.tipoRecibo = 'NC' AND r.fechaRecibo BETWEEN '2018-11-01 00:00:00' AND '2018-11-06 23:59:59'
 AND ef.`tipoFacturacion` != 4;*/
$montoNotasCredito = Recibos::find()->select('SUM(tbl_recibos.monto) as monto')
->innerJoin(['rd'=>'tbl_recibos_detalle'],'rd.idRecibo = tbl_recibos.idRecibo')
->innerJoin(['mc'=>'tbl_movi_cobrar'],'mc.idMov = rd.idDocumento')
->innerJoin(['ef'=>'tbl_encabezado_factura'],'ef.idCabeza_Factura = mc.idCabeza_Factura')
->where([ 'between', 'fechaRecibo', $fecha_hora_d, $fecha_hora_h ])
->andWhere(['tipoRecibo'=>'NC'])
->andWhere(['!=','tipoFacturacion','5'])
->asArray()->One();
$notas_credito += $montoNotasCredito['monto'];

// $recibos = Recibos::find()->where([ 'between', 'fechaRecibo', $fecha_hora_d, $fecha_hora_h ])->all();
// $cancelacion_credito = 0;
// if (@$recibos) {
//   foreach ($recibos as $key => $recibo) {
//     /*if ($recibo->tipoRecibo == 'AB') {
//       $abono_credito += $recibo->monto;
//     }*/
//    if ($recibo->tipoRecibo == 'NC') {
//       $rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$recibo->idRecibo])->one();
//       $movim = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();
//       $fact_filtro = FacturasDia::findOne(['idCabeza_Factura'=>$movim->idCabeza_Factura]);
//       if ($fact_filtro->tipoFacturacion != '4') {
//         $notas_credito += $recibo->monto;
//       }
//     }
//     /*if ($recibo->tipoRecibo == 'CF') {
//       $cancelacion_credito += $recibo->monto;
//     }*/
//
//   }//fin foreach abono y cancelacion
// }

$total_ingresos = $contado + $abono_credito + $cancelacion_credito + $abono_apartados;

//OBTENER DESGLOSE DE INGRESOS POR COMPRA
$medio_pagos = MedioPago::find()->where([ 'between', 'fecha_deposito', $fecha_d, $fecha_h ])->all();
foreach ($medio_pagos as $key => $medio_pago) {
  //$fact_filtro = FacturasDia::findOne($medio_pago->idCabeza_factura);
  $movimiento = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$medio_pago->idCabeza_factura])->one();

  //if (@$movimiento->tipmov != 'NC') {
    if ($medio_pago->idForma_Pago == 1) {//efectivo
      $efectivo += $medio_pago->monto;
    } elseif ($medio_pago->idForma_Pago == 4){//deposito
      $deposito += $medio_pago->monto;
    } elseif ($medio_pago->idForma_Pago == 2) {//tarjeta
      $tarjetas += $medio_pago->monto;
    } elseif ($medio_pago->idForma_Pago == 3) {//cheque
      $cheques += $medio_pago->monto;
    }
  //}
}//fin medio pago
//OBTENER DESGLOCE DE DEVOLUCIONES
$movimiento_cobrar_nc = MovimientoCobrar::find()->where([ 'between', 'fecmov', $fecha_hora_d, $fecha_hora_h ])->all();
$saldo_x_nc_e = $saldo_x_nc_d = $saldo_x_nc_t = $saldo_x_nc_c = 0;
foreach ($movimiento_cobrar_nc as $movim_nc) {
  //esto me comprueba que es una NC de una factura a contado.
  if ($movim_nc->tipmov == 'NC' && $movim_nc->monto_anterior == 0.00 && $movim_nc->idCabeza_Factura > 0) {
    $monto_factura = FacturasDia::findOne($movim_nc->idCabeza_Factura);//obtengo datos de la factura
    if ($monto_factura->tipoFacturacion == 1) {//efectivo
      $saldo_x_nc_e += $movim_nc->monto_movimiento;//recorremos los movimientos de esta factura si esta fue pagada con este medio de pago
      $efectivo_nc = $monto_factura->total_a_pagar - $saldo_x_nc_e;//restamos la suma de todos los movimientos NC de este tipo de pago y lo restamos al monto de la factura
    } elseif ($monto_factura->tipoFacturacion == 2){//deposito
      $saldo_x_nc_d += $movim_nc->monto_movimiento;//recorremos los movimientos...
      $deposito_nc = $monto_factura->total_a_pagar - $saldo_x_nc_d;//restamos la suma de todos los movimientos...
    } elseif ($monto_factura->tipoFacturacion == 3) {//tarjeta
      $saldo_x_nc_t += $movim_nc->monto_movimiento;//recorremos los movimientos...
      $tarjetas_nc = $monto_factura->total_a_pagar - $saldo_x_nc_t;//restamos la suma de todos los movimientos...
    } elseif ($monto_factura->tipoFacturacion == 5) {//cheque
      $saldo_x_nc_c += $movim_nc->monto_movimiento;//recorremos los movimientos...
      $cheques_nc = $monto_factura->total_a_pagar - $saldo_x_nc_c;//restamos la suma de todos los movimientos...
    }
  }
}

//OBTENER DESGLOSE DE GASTOS APLICADOS
$gastos_dia = Gastos::find()->select('SUM(montoGasto) as montoGasto')
->where([ 'between', 'fechaAplicada', $fecha_d, $fecha_h ])
->andWhere(['estadoGasto'=>"Aplicado"])->asArray()->One();
  $gastos += $gastos_dia['montoGasto'];

$total = ($efectivo + $efectivo_nc + $deposito + $deposito_nc + $tarjetas + $tarjetas_nc + $cheques + $cheques_nc);
$total_caja = $total_ingresos - ( $gastos + $notas_credito );
?>

  <div class="col-lg-12 col-md-12">
  <img src="<?=$empresaimagen->getImageurl('html')?>" style="height: 100px;">
  <span style="float:right">
  <p>RANGO: <strong>Desde: </strong><?=Date('d-m-Y',strtotime($fecha_d))?> <strong> Hasta: </strong><?=Date('d-m-Y',strtotime($fecha_h))?></p>
  </span>
</div>
<div class="col-lg-6 col-md-6">
  <table class="items table table-striped" id="tabla_facturas_pendientes">
    <thead class="thead-inverse">
      <tr>
        <th style="text-align:left" colspan="2"><font face="arial" size=6>Ventas</font></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:right" ><font size=3>CONTADO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($contado,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>CRÉDITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($credito,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=5>TOTAL VENTAS:</font></td>
        <td style="text-align:right" ><font size=5><?= number_format($total_ventas,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>CANCL & ABONO CRÉDITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($abono_credito + $cancelacion_credito,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>ABONO APARTADO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($abono_apartados,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=5>TOTAL INGRESOS:</font></td>
        <td style="text-align:right" ><font size=5><?= number_format($total_ingresos,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>GASTOS:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($gastos,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>NOTAS DE CREDITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($notas_credito,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>APARTADOS CANCELADOS:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($apartado_cancelado,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=5>TOTAL EN CAJA:</font></td>
        <td style="text-align:right" ><font size=6><strong><?= number_format($total_caja - $apartado_cancelado,2) ?></strong></font></td>
      </tr>
    </tbody>
  </table>
</div>
<div class="col-lg-6 col-md-6">
  <table class="items table table-striped" id="tabla_facturas_pendientes">
    <thead class="thead-inverse">
      <tr>
        <th style="text-align:left" colspan="2"><font face="arial" size=6>Desgloce de Ingresos</font></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:right" ><font size=3>EFECTIVO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($efectivo+$efectivo_nc,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>DEPOSITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($deposito+$deposito_nc,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>TARJETAS:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($tarjetas+$tarjetas_nc,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>CHEQUES:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($cheques+$cheques_nc,2) ?></font></td>
      </tr>
      <!--tr>
        <td style="text-align:right" ><font size=3>**DOLLAR:</font></td>
        <td style="text-align:right" ><font size=4--><!--?= number_format($dollar,2) ?></font></td>
      </tr-->

      <tr>
        <td style="text-align:right" ><font size=3 color="RED">( - )&nbsp;&nbsp;&nbsp;&nbsp; GASTOS:</font></td>
        <td style="text-align:right" ><font size=4 color="RED"><?= number_format($gastos,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3 color="RED">( - )&nbsp;&nbsp;&nbsp;&nbsp; NOTAS DE CREDITO:</font></td>
        <td style="text-align:right" ><font size=4 color="RED"><?= number_format($notas_credito,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=6>TOTAL:</font></td>
        <td style="text-align:right" ><font size=6><?= number_format($total-($gastos+$notas_credito),2) ?></font></td>
      </tr>
    </tbody>
  </table>
</div>
