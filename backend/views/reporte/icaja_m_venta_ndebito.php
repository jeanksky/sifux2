<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
</style>
<?php
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\MovimientoCobrar;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\MedioPago;
use backend\models\FormasPago;

$empresaimagen = new Empresa();
$fechaAbonos1 = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
$fechaAbonos2 = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';
$clientes = Clientes::find()->all();
if ($lista_clientes != '0') {
  $clientes = Clientes::find()->where("idCliente IN (".$lista_clientes.")", [])->all();

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y h:i:s A',strtotime($fechaAbonos1))." <strong>Hasta: </strong>".Date('d-m-Y h:i:s A',strtotime($fechaAbonos2)).'</p>
        <p>Emitido: '.Date('d-m-Y h:i:s A').'</p>
      </span>';
echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';

echo '<thead class="thead-inverse">
        <tr>
          <th style="text-align:center"><font face="arial" size=3>Cliente</font></th>
          <th style="text-align:center"><font face="arial" size=2>Fecha / Hora</font></th>
          <th style="text-align:center"><font face="arial" size=2>ID N.Débito</font></th>
          <th style="text-align:center"><font face="arial" size=2>ID Factura</font></th>
          <th style="text-align:right"><font face="arial" size=2>Monto anterior</font></th>
          <th style="text-align:right"><font face="arial" size=2>Monto ND</font></th>
          <th style="text-align:right"><font face="arial" size=2>Saldo</font></th>
          <th style="text-align:right"><font face="arial" size=2>Detalle</font></th>
        </tr>
      </thead>
      <tbody class="">';
    $monto_not_deb_total = 0;
    foreach ($clientes as $key => $cliente) {
      $rowspan = 2; $monto_not_deb = 0;
      $total_nd = 0;
      $notas_de_debito = Recibos::find()->where(['tipoRecibo'=>'ND','cliente'=>$cliente->idCliente])
      ->andWhere("fechaRecibo >= :fechaAbonos1 AND fechaRecibo <= :fechaAbonos2",
      [':fechaAbonos1'=>$fechaAbonos1, ':fechaAbonos2'=>$fechaAbonos2])
      ->orderBy(['idRecibo' => SORT_ASC])->all();
      foreach ($notas_de_debito as $key => $nota_debito) {
        // if ($cliente->idCliente==$nota_debito->cliente) {
          $rowspan += 1;
          $monto_not_deb += $nota_debito->monto;
          $rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$nota_debito->idRecibo])->one();
          $movimiento = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();
          $monto_not_deb_total += $movimiento->monto_movimiento;//Me suma el total de notas de debito de todos los clientes seleccionado
          $total_nd += $movimiento->monto_movimiento;
         // }
      }

        if ($monto_not_deb > 0) {
        $cliente_turno = '<strong>'.$cliente->nombreCompleto .  '</strong><br>
        <span style="float:right">Total: '.number_format($total_nd,2).'</span>';

        echo '
        <tr><td colspan="9" bgcolor="#7FFFD4"></td></tr>

        <tr><td rowspan='.$rowspan.' style="border-left-width: 3px; border-left-style: solid; border-left-color: #708090;"><font size=4>'.$cliente_turno.'</font></td><tr>';
        foreach ($notas_de_debito as $key => $nota_debito) {
          if ($cliente->idCliente==$nota_debito->cliente) {
          $rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$nota_debito->idRecibo])->one();
          $movimiento = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();

          $fecha = date('d-m-Y / h:i:s A', strtotime( $movimiento->fecmov ));
          printf('<tr>
                    <td align="center" ><font size=2>%s</font></td>
          		 		  <td align="center" ><font size=2>%s</font></td>
          	    		<td align="center"><font size=2>%s</font></td>
          	    		<td align="right"><font size=2>%s</font></td>
          	    		<td align="right"><font size=2>%s</font></td>
          	    		<td align="right"><font size=2>%s</font></td>
                    <td align="center" style="border-right-width: 3px; border-right-style: solid; border-left-color: #708090;"><font size=2>%s</font></td>
              		</tr>',
                        $fecha,
                        $movimiento->idMov,
                        $movimiento->idCabeza_Factura,
                        number_format($movimiento->monto_anterior,2),
                        number_format($movimiento->monto_movimiento,2),
                        number_format($movimiento->saldo_pendiente,2),
                        $movimiento->concepto
                    );
          }
        }
      }
    }
echo "</tbody> ";
echo '<tfoot>
        <tr>
          <td></td>
          <td style="text-align:left"><font size=5>TOTAL:</font></td>
          <td style="text-align:right"><font size=5>'.number_format($monto_not_deb_total,2).'</font></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>

        </tr>
      </tfoot> ';
echo "</table>";
}// fin if del if lista_clientes
else {
  echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos un cliente...!</small></h1>';
}
 ?>
