<?php
use backend\models\Empresa;
use backend\models\Familia;
use backend\models\ProductoServicios;

$empresaimagen = new Empresa();

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Productos por debajo del mínimo (Categoría)</h3>';
  $categorias = Familia::find()->all();
  if ($lista_filtro_cagtegoria_inven != '0') {
      $categorias = Familia::find()->where("codFamilia IN (".$lista_filtro_cagtegoria_inven.")", [])->all();
    }
      echo '<table class="items table table-striped">
            <thead>';
            printf('<tr>
                     <th style="text-align:left">%s</th>
                     <th style="text-align:left">%s</th>
                     <th style="text-align:right">%s</th>
                     <th style="text-align:center">%s</th>
                     <th style="text-align:center">%s</th>
                     <th style="text-align:center">%s</th>
                    </tr>',
                       'CÓDIGO PRODUCTO',
                       'DESCRIPCIÓN',
                       'PRECIO UNIT I.V.I',
                       'CANTIDAD',
                       'CANT.MÍNIMA',
                       'UBICACIÓN'
                       );
            echo '</thead>';
            echo '<tbody>';
        $suma_total_precio_venta_iv = $suma_total_precio_venta = 0;
        foreach ($categorias as $key => $categoria) {
          $inventario = ProductoServicios::find()->where(['codFamilia'=>$categoria->codFamilia])->andWhere('cantidadInventario <= cantidadMinima',[])->all();
          if (@$inventario) {
            $total_precio_venta = $total_precio_venta_iv = 0;
            echo '<tr><td colspan="6" align="center"><font size=4>'.$categoria->descripcion.'</font></td></tr>';
            foreach ($inventario as $key => $producto) {
              $suma_total_precio_venta += $producto->precioVenta;
              $suma_total_precio_venta_iv += $producto->precioVentaImpuesto;
              $total_precio_venta += $producto->precioVenta;
              $total_precio_venta_iv += $producto->precioVentaImpuesto;
              printf('<tr>
                        <td align="left" ><font size=2>%s</font></td>
                        <td align="left" ><font size=2>%s</font></td>
                        <td align="right"><font size=2>%s</font></td>
                        <td align="center" ><font size=2>%s</font></td>
                        <td align="center" ><font size=2>%s</font></td>
                        <td align="center"><font size=2>%s</font></td>
                      </tr>',
                          $producto->codProdServicio,
                          $producto->nombreProductoServicio,
                          number_format($producto->precioVentaImpuesto,2),
                          $producto->cantidadInventario,
                          $producto->cantidadMinima,
                          $producto->ubicacion
                        );
            }//fin foreach de $inventario
            echo '<tr>
              <th></th>
              <th style="text-align:right">Total categoría:</th>
              <th style="text-align:right">'.number_format($total_precio_venta_iv,2).'</th>
              <th></th>
              <th></th>
              <th></th>
            </tr>';
            echo '<tr><td colspan="6" bgcolor="#7FFFD4" height="70px"></td></tr>';
          }//fin if $inventario (que esten en el rango que se cumple)

        }
        echo '</tbody>';
        echo '<tfoot>
                <tr>
                  <th></th>
                  <th style="text-align:right"><font size=4>Total global:</font></th>
                  <th style="text-align:right"><font size=4>'.number_format($suma_total_precio_venta_iv,2).'</font></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot> ';
    echo '</table>';
