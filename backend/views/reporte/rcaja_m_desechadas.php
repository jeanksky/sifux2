<?php
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\Clientes;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

/*$mediopagoproveedor = MedioPagoProveedor::find()->where(['=','cuenta_bancaria_local', $cuenta->idBancos])->andWhere("fecha_documento >= :fechaFactura1 AND fecha_documento <= :fechaFactura2", [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2])->orderBy(['idMedio_pago' => SORT_ASC])->all();*/


echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y',strtotime($fechaFactura1))." <strong>Hasta: </strong>".Date('d-m-Y',strtotime($fechaFactura2)).'</p>
</span>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
                        'ID Documento',
                        'Fecha',
                        'Cliente',
                        'Estado Pre-factura',
                        'Monto'
                        );
                echo '</thead>';
echo '<tbody class="">';
$monto_total_anulado = 0;
$sql = "SELECT * FROM tbl_encabezado_factura WHERE estadoFactura IN ('Anulada') AND fecha_inicio BETWEEN :fechaFactura1 AND :fechaFactura2";
$command = \Yii::$app->db->createCommand($sql);
$command->bindParam(":fechaFactura1", $fechaFactura1);
$command->bindParam(":fechaFactura2", $fechaFactura2);
$ventas = $command->queryAll();
foreach($ventas as $factura) {
  $monto_total_anulado += $factura['estadoFactura']=='Anulada' ?  $factura['total_a_pagar'] : 0;
	$fecha = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
  $nombre_cliente = '';
  if($cliente = Clientes::findOne($factura['idCliente']))
  {
      $nombre_cliente = $cliente->nombreCompleto;
  }else{
      $nombre_cliente = $factura['idCliente'];
  }
	//$posFecha = $mediopagoproveedor->posFecha;
	printf('<tr style="color:#231BB8">
  		 		  <td align="center" ><font face="arial" size=2 >%s</font></td>
  	    		<td align="center" width="180" ><font size=2>%s</font></td>
  	    		<td align="center"><font size=2>%s</font></td>
  	    		<td align="center"><font size=2>%s</font></td>
  	    		<td align="right"><font size=2>%s</font></td>
      		</tr>',
                $factura['idCabeza_Factura'],
                $fecha,
                $nombre_cliente,
                'Desechada',//$factura['estadoFactura'],
                number_format($factura['total_a_pagar'],2)
            );
}

printf('<tr>
          <td colspan="5" style="text-align:right" width="190"><font size=3>%s</font></td>
        </tr>',
            '<h4>Total: <strong>'.number_format($monto_total_anulado,2).'</strong></h4>'
        );
echo '</tbody>';
echo '</table>';
?>
