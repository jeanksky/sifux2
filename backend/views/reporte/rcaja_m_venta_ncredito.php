<?php
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\MovimientoCobrar;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\MedioPago;
use backend\models\FormasPago;

$filtro = false;
$clientes = Clientes::find()
->innerJoin(['r'=>'tbl_recibos'],'r.cliente = tbl_clientes.idCliente')
->where(['tipoRecibo'=>'NC'])->all();
if ($lista_clientes != '0') {
  $clientes = Clientes::find()->where("idCliente IN (".$lista_clientes.")", [])->all();
  $filtro = true;
}
  $empresaimagen = new Empresa();
  $fechaAbonos1 = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
  $fechaAbonos2 = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';


  echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
  echo '<span style="float:right">
    <p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y H:i:s A',strtotime($fechaAbonos1))." <strong>Hasta: </strong>".Date('d-m-Y H:i:s A',strtotime($fechaAbonos2)).'</p>
  </span>';
  $notas_de_credito = Recibos::find()->where(['tipoRecibo'=>'NC'])->andWhere("fechaRecibo >= :fechaAbonos1 AND fechaRecibo <= :fechaAbonos2", [':fechaAbonos1'=>$fechaAbonos1, ':fechaAbonos2'=>$fechaAbonos2])->orderBy(['idRecibo' => SORT_ASC])->all();

  echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';

  echo '<thead class="thead-inverse">
  <tr>
  <th style="text-align:center"><font face="arial" size=3>Cliente</font></th>
  <th style="text-align:center"><font face="arial" size=2>Fecha / Hora</font></th>
  <th style="text-align:center"><font face="arial" size=2>ID N.Crédito</font></th>
  <th style="text-align:center"><font face="arial" size=2>ID Factura</font></th>
  <th style="text-align:right"><font face="arial" size=2>Monto anterior</font></th>
  <th style="text-align:right"><font face="arial" size=2>Monto NC</font></th>
  <th style="text-align:right"><font face="arial" size=2>Saldo</font></th>
  <th style="text-align:center"><font face="arial" size=2>Detalle</font></th>
  </tr>
  </thead>
  <tbody class="">';
  $monto_not_cre_total = 0;
  if ($filtro==false) {
    //Para clientes que no estan en el sistema==============================================
    foreach ($notas_de_credito as $key => $nota_credito) {
      $movimiento = RecibosDetalle::find()->select(['mc.fecmov','mc.idMov','mc.idCabeza_Factura',
      'mc.monto_anterior','mc.monto_movimiento','mc.saldo_pendiente','mc.concepto'])
      ->innerJoin(['mc'=>'tbl_movi_cobrar'],'idMov = idDocumento')
      ->where(['idRecibo'=>$nota_credito->idRecibo])
      ->asArray()->one();
      /*$rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$nota_credito->idRecibo])->one();
      $movimiento = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();*/
      $clientes_sistema = Clientes::findOne($nota_credito->cliente);
      if ($clientes_sistema) { } else {
        //---------------------------------------------
        $rowspan_cliente_no_reg = 2;
        foreach ($notas_de_credito as $key => $nota_credito_) {
          $clientes_sistema = Clientes::findOne($nota_credito_->cliente);
          if ($clientes_sistema) { } else {
            $rowspan_cliente_no_reg += 1;
          }
        }
        //---------------------------------------------
        $monto_not_cre_total += $movimiento['monto_movimiento'];
        $fecha = date('d-m-Y / h:i:s A', strtotime( $movimiento['fecmov'] ));
        printf('<tr><td colspan="9" bgcolor="#7FFFD4"></td></tr>
          <tr>
          <td style="border-left-width: 3px;"><font size=4><strong>%s</strong><br>(Cliente no registrado)</font></td>
          <td align="center" ><font size=2>%s</font></td>
          <td align="center" ><font size=2>%s</font></td>
          <td align="center"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="center" style="border-right-width: 3px;"><font size=2>%s</font></td>
          </tr>',
          $nota_credito->cliente,
          $fecha,
          $movimiento['idMov'],
          $movimiento['idCabeza_Factura'],
          number_format($movimiento['monto_anterior'],2),
          number_format($movimiento['monto_movimiento'],2),
          number_format($movimiento['saldo_pendiente'],2),
          $movimiento['concepto']
        );
      }
    }
  }//fin filtro false
//para clientes en el sistema============================================================
foreach ($clientes as $key => $cliente) {
  $rowspan = 2; $monto_not_cre = 0;
  $total_nd = 0;
  foreach ($notas_de_credito as $key => $nota_credito) {
    if ($cliente->idCliente==$nota_credito->cliente) {
      $movimiento = RecibosDetalle::find()->select(['mc.idMetPago','mc.monto_movimiento'])
      ->innerJoin(['mc'=>'tbl_movi_cobrar'],'idMov = idDocumento')
      ->innerJoin(['ef'=>'tbl_encabezado_factura'],'ef.idCabeza_Factura = mc.idCabeza_Factura')
      ->where(['idRecibo'=>$nota_credito->idRecibo])
      ->andWhere(['!=','ef.tipoFacturacion','5'])
      ->asArray()->one();
      if($movimiento != NULL){
        $rowspan += 1;
        $monto_not_cre += $nota_credito->monto;
        /*$rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$nota_credito->idRecibo])->one();
        $movimiento = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();*/

        $monto_not_cre_total += $movimiento['monto_movimiento'];//Me suma el total de notas de debito de todos los clientes seleccionado
        $total_nd += $movimiento['monto_movimiento'];
      }
    }
  }

  if ($monto_not_cre > 0) {
    $cliente_turno = '<strong>'.$cliente->nombreCompleto .  '</strong><br>
    Total: '.number_format($total_nd,2);

    // echo '<tr><td colspan="9" bgcolor="#7FFFD4"></td></tr>';

    echo '<tr><td rowspan='.$rowspan.' style="border-left-width: 3px;"><font size=4>'.$cliente_turno.'</font></td><tr>';
    foreach ($notas_de_credito as $key => $nota_credito) {
      if ($cliente->idCliente==$nota_credito->cliente) {
        $movimiento = RecibosDetalle::find()->select(['mc.fecmov','mc.idMov','mc.idCabeza_Factura',
        'mc.monto_anterior','mc.monto_movimiento','mc.saldo_pendiente','mc.concepto'])
        ->innerJoin(['mc'=>'tbl_movi_cobrar'],'idMov = idDocumento')
        ->where(['idRecibo'=>$nota_credito->idRecibo])->asArray()->one();
        if($movimiento != NULL){
          // $rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$nota_credito->idRecibo])->one();
          // $movimiento = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();

          $fecha = date('d-m-Y / h:i:s A', strtotime( $movimiento['fecmov'] ));
          printf('<tr>
          <td align="center" ><font size=2>%s</font></td>
          <td align="center" ><font size=2>%s</font></td>
          <td align="center"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          <td align="center" style="border-right-width: 3px;"><font size=2>%s</font></td>
          </tr>',
          $fecha,
          $movimiento['idMov'],
          $movimiento['idCabeza_Factura'],
          number_format($movimiento['monto_anterior'],2),
          number_format($movimiento['monto_movimiento'],2),
          number_format($movimiento['saldo_pendiente'],2),
          $movimiento['concepto']
        );
      }
    }
  }
}
}

echo "</tbody> ";
echo '<tfoot>
<tr>
<td colspan="6" style="text-align:right"><font size=5>TOTAL:</font></td>
<td colspan="2" style="text-align:right"><font size=5>'.number_format($monto_not_cre_total,2).'</font></td>
</tr>
</tfoot> ';
echo "</table>";
/*}// fin if del if lista_clientes
else {
  echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos un cliente...!</small></h1>';
}*/
?>
