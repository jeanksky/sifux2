<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
h3{
	color: black;
}
</style>
<?php
use backend\models\Empresa;
use backend\models\OrdenServicio;
use backend\models\Mecanicos;
use backend\models\Clientes;
use backend\models\Vehiculos;
use backend\models\Modelo;
use backend\models\Marcas;

$empresaimagen = new Empresa();
$orden_servicios = OrdenServicio::find()->where(['estado'=>'1'])->all();
echo '<img src="'. $empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
      echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de Orden de Servicio pendiente</h3>';
      echo '<table class="items table table-striped">';
          echo '<thead>';
          printf('<tr>
                   <th style="text-align:center">%s</th>
                   <th style="text-align:center">%s</th>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:right">%s</th>
                  </tr>',
                     'ID ORDEN',
                     'FECH INICIO',
                     'PLCA VEHÍCULO',
                     'MARCA / MODELO',
                     'CLIENTE',
                     'MECÁNICO ASIGN',
                     'MONTO'
                     );
          echo '</thead>';
          echo '<tbody>';
          $total_monto = 0; $imp = 13/100;
            foreach ($orden_servicios as $key => $orden_servicio) {
              $mecanico = Mecanicos::find()->where(['idMecanico'=>$orden_servicio->idMecanico])->one();
              $cliente = Clientes::find()->where(['idCliente'=>$orden_servicio->idCliente])->one();
              $vehiculo = Vehiculos::find()->where(['placa'=>$orden_servicio->placa])->one();
              $modelo = Modelo::find()->where(['codModelo'=>$vehiculo->modelo])->one();
              $marca = Marcas::find()->where(['codMarca'=>$vehiculo->marca])->one();
              printf('<tr>
                       <td style="text-align:center"><font face="arial" size=1>%s</font></td>
                       <td style="text-align:center"><font face="arial" size=1>%s</font></td>
                       <td style="text-align:left"><font face="arial" size=1>%s</font></td>
                       <td style="text-align:left"><font face="arial" size=1>%s</font></td>
                       <td style="text-align:left"><font face="arial" size=1>%s</font></td>
                       <td style="text-align:left"><font face="arial" size=1>%s</font></td>
                       <td style="text-align:right"><font face="arial" size=1>%s</font></td>
                      </tr>',
                         $orden_servicio->idOrdenServicio,
                         $orden_servicio->fecha,
                         $orden_servicio->placa,
                         $marca->nombre_marca.' / '.$modelo->nombre_modelo,
                         $cliente->nombreCompleto,
                         $mecanico->nombre,
                         number_format($orden_servicio->montoServicio + ($orden_servicio->montoServicio*$imp),2)
                         );
                         $total_monto += $orden_servicio->montoServicio;
            }
            printf('<tr>
                     <td style="text-align:center">%s</td>
                     <td style="text-align:center">%s</td>
                     <td style="text-align:left">%s</td>
                     <td style="text-align:left">%s</td>
                     <td style="text-align:left">%s</td>
                     <th style="text-align:right">%s</th>
                     <th style="text-align:right">%s</th>
                    </tr>',
                       '',
                       '',
                       '',
                       '',
                       '',
                       'TOTAL:',
                       number_format($total_monto + ($total_monto*$imp),2)
                       );
          echo '</tbody>';
      echo '</table>'
 ?>
