<?php
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use backend\models\Funcionario;

$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';
$funcionarios = Funcionario::find()->all();
if ($tipo=='resumen_venta') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen de ventas por Funcionario</h3>';
  echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
      'name' => '',
      'data' => ArrayHelper::map($funcionarios,'idFuncionario', function($element) {
                  return $element['idFuncionario'].' - '.$element['nombre'].' '.$element['apellido1'].' '.$element['apellido2'];
              }),
      'options' => [
          'placeholder' => '- Seleccione los funcionarios a filtrar -',
          'id'=>'filtro_funcionario',
          'multiple' => true //esto me ayuda a que la busqueda sea multiple
      ],
      'pluginOptions' => ['initialize'=> true,'allowClear' => true]
  ]).'</div>
  <div class="col-lg-2">
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
          'id'=>'fecha_desde']]).'
  </div>
  <div class="col-lg-2">
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
          'id'=>'fecha_hasta']]).'
  </div>
  <div class="col-lg-2">
      <a class="btn btn-default btn-sm" onclick="buscar_resumen_venta_fu()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
      <a class="btn btn-default btn-sm" id="imp_resumen_venta_fu" onclick="imprimir_resumen_venta_fu()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
  </div>
  <div class="col-lg-12"><br>
    <div class="well" id="mostrar_resumen_venta_fu"></div>
  </div>';
} elseif ($tipo=='resumen_notas_credito') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen de notas de crédito por Funcionario</h3>';
  echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
      'name' => '',
      'data' => ArrayHelper::map($funcionarios,'idFuncionario', function($element) {
                  return $element['idFuncionario'].' - '.$element['nombre'].' '.$element['apellido1'].' '.$element['apellido2'];
              }),
      'options' => [
          'placeholder' => '- Seleccione los funcionarios a filtrar -',
          'id'=>'filtro_funcionario2',
          'multiple' => true //esto me ayuda a que la busqueda sea multiple
      ],
      'pluginOptions' => ['initialize'=> true,'allowClear' => true]
  ]).'</div>
  <div class="col-lg-2">
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
          'id'=>'fecha_desde2']]).'
  </div>
  <div class="col-lg-2">
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
          'id'=>'fecha_hasta2']]).'
  </div>
  <div class="col-lg-2">
      <a class="btn btn-default btn-sm" onclick="buscar_resumen_nota_credito_fu()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
      <a class="btn btn-default btn-sm" id="imp_resumen_nota_credito_fu" onclick="imprimir_resumen_nota_credito_fu()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
  </div>
  <div class="col-lg-12"><br>
    <div class="well" id="mostrar_resumen_nota_credito_fu"></div>
  </div>';
}

 ?>
<script type="text/javascript">
$(document).ready(function () {
    //mantenemos todos los boton de imprimir desabilitado
    $('#imp_resumen_venta_fu').addClass('disabled');
    $('#imp_resumen_nota_credito_fu').addClass('disabled');
});
//=====================RESUMEN DE VENTA FUNCIONARIO
  function buscar_resumen_venta_fu() {
    var filtro_funcionarios = $("#filtro_funcionario").val();
    fecha_desde = document.getElementById('fecha_desde').value;
    fecha_hasta = document.getElementById('fecha_hasta').value;
    var lista_filtro_funcionarios = '0';
    if (filtro_funcionarios==null) {
      lista_filtro_funcionarios = '0';
    } else {
      lista_filtro_funcionarios = filtro_funcionarios.toString();
    }
    document.getElementById("mostrar_resumen_venta_fu").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rfuncionario_m_resu_ventas') ?>" ,
        { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_funcionarios' : lista_filtro_funcionarios } ,
        function( data ) {
          $('#mostrar_resumen_venta_fu').html(data);
          $('#imp_resumen_venta_fu').removeClass('disabled');//abilitamos boton de imprimir
        }).fail(function() {
          document.getElementById("mostrar_resumen_venta_fu").innerHTML = '<center><h2><small>No se encontraron registros</small></h2></center>';
        });
  }
  function imprimir_resumen_venta_fu() {
  var filtro_funcionarios = $("#filtro_funcionario").val();
  fecha_desde = document.getElementById('fecha_desde').value;
  fecha_hasta = document.getElementById('fecha_hasta').value;
  var lista_filtro_funcionarios = '0';
  if (filtro_funcionarios==null) {
    lista_filtro_funcionarios = '0';
  } else {
    lista_filtro_funcionarios = filtro_funcionarios.toString();
  }
  $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/ifuncionario_m_resu_ventas') ?>" ,
      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_funcionarios' : lista_filtro_funcionarios } ,
          function( data ) {
            var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
              ventimp.document.write(data);
              ventimp.document.close();
              ventimp.print();
              ventimp.close();
          });
  }
//=====================RESUMEN DE NOTAS DE CREDITOS FUNCIONARIO
function buscar_resumen_nota_credito_fu() {
  var filtro_funcionarios = $("#filtro_funcionario2").val();
  fecha_desde = document.getElementById('fecha_desde2').value;
  fecha_hasta = document.getElementById('fecha_hasta2').value;
  var lista_filtro_funcionarios = '0';
  if (filtro_funcionarios==null) {
    lista_filtro_funcionarios = '0';
  } else {
    lista_filtro_funcionarios = filtro_funcionarios.toString();
  }
  document.getElementById("mostrar_resumen_nota_credito_fu").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
  $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rfuncionario_m_resu_notas_credito') ?>" ,
      { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_funcionarios' : lista_filtro_funcionarios } ,
      function( data ) {
        $('#mostrar_resumen_nota_credito_fu').html(data);
        $('#imp_resumen_nota_credito_fu').removeClass('disabled');//abilitamos boton de imprimir
      }).fail(function() {
        document.getElementById("mostrar_resumen_nota_credito_fu").innerHTML = '<center><h2><small>No se encontraron registros</small></h2></center>';
      });
}
function imprimir_resumen_nota_credito_fu() {
var filtro_funcionarios = $("#filtro_funcionario2").val();
fecha_desde = document.getElementById('fecha_desde2').value;
fecha_hasta = document.getElementById('fecha_hasta2').value;
var lista_filtro_funcionarios = '0';
if (filtro_funcionarios==null) {
  lista_filtro_funcionarios = '0';
} else {
  lista_filtro_funcionarios = filtro_funcionarios.toString();
}
$.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/ifuncionario_m_resu_notas_credito') ?>" ,
    { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_funcionarios' : lista_filtro_funcionarios } ,
        function( data ) {
          var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
            ventimp.document.write(data);
            ventimp.document.close();
            ventimp.print();
            ventimp.close();
        });
}
</script>
