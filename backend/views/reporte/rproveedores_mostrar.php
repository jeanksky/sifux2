<?php
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use backend\models\Proveedores;

$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';
$proveedores = Proveedores::find()->all();
if ($tipo=='cuentas_x_pagar') {
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen Cuentas por Pagar (Proveedores)</h3>';
  echo '<div class="col-lg-8">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
    'name' => '',
    'data' => ArrayHelper::map($proveedores,'codProveedores', function($element) {
      return $element['codProveedores'].' - '.$element['nombreEmpresa'];
    }),
    'options' => [
      'placeholder' => '- Seleccione los proveedores a filtrar -',
      'id'=>'filtro_proveedores',
      'multiple' => true //esto me ayuda a que la busqueda sea multiple
    ],
    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
    ]).'</div>
    <div class="col-lg-2">
    <a class="btn btn-default btn-sm" onclick="buscar_resumen_cuenta_pagar()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
    <a class="btn btn-default btn-sm" id="imp_resumen_cuenta_pagar" onclick="imprimir_resumen_cuenta_pagar()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
    </div>
    <div class="col-lg-12"><br>
    <div class="well" id="mostrar_resumen_cuenta_pagar"></div>
    </div>';
  }  elseif ($tipo=='tramite_pagos') {
    echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Trámites de pagos a proveedores</h3>';
    echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
      'name' => '',
      'data' => ArrayHelper::map($proveedores,'codProveedores', function($element) {
        return $element['codProveedores'].' - '.$element['nombreEmpresa'];
      }),
      'options' => [
        'placeholder' => '- Seleccione los proveedores a filtrar -',
        'id'=>'filtro_proveedores_tr',
        'multiple' => true //esto me ayuda a que la busqueda sea multiple
      ],
      'pluginOptions' => ['initialize'=> true,'allowClear' => true]
      ]).'</div>
      <div class="col-lg-2">
      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
      'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
      'id'=>'fecha_desde_tr']]).'
      </div>
      <div class="col-lg-2">
      '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
      'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
      'id'=>'fecha_hasta_tr']]).'
      </div>
      <div class="col-lg-2">
      <a class="btn btn-default btn-sm" onclick="buscar_tramites()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
      <a class="btn btn-default btn-sm" id="imp_tramites" onclick="imprimir_tramites()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
      </div>
      <div class="col-lg-12"><br>
      <div class="well" id="mostrar_tramites"></div>
      </div>';
    } elseif ($tipo=='notas_debito') {
      echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Notas de Débito a Proveedores</h3>';
      echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
        'name' => '',
        'data' => ArrayHelper::map($proveedores,'codProveedores', function($element) {
          return $element['codProveedores'].' - '.$element['nombreEmpresa'];
        }),
        'options' => [
          'placeholder' => '- Seleccione los proveedores a filtrar -',
          'id'=>'filtro_proveedores2',
          'multiple' => true //esto me ayuda a que la busqueda sea multiple
        ],
        'pluginOptions' => ['initialize'=> true,'allowClear' => true]
        ]).'</div>
        <div class="col-lg-2">
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
        'id'=>'fecha_desde']]).'
        </div>
        <div class="col-lg-2">
        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
        'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
        'id'=>'fecha_hasta']]).'
        </div>
        <div class="col-lg-2">
        <a class="btn btn-default btn-sm" onclick="buscar_notas_debito()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
        <a class="btn btn-default btn-sm" id="imp_notas_debito" onclick="imprimir_notas_debito()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
        </div>
        <div class="col-lg-12"><br>
        <div class="well" id="mostrar_notas_debito"></div>
        </div>';
      } elseif ($tipo=='notas_credito') {
        echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Notas de Crédito a Proveedores</h3>';
        echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
          'name' => '',
          'data' => ArrayHelper::map($proveedores,'codProveedores', function($element) {
            return $element['codProveedores'].' - '.$element['nombreEmpresa'];
          }),
          'options' => [
            'placeholder' => '- Seleccione los proveedores a filtrar -',
            'id'=>'filtro_proveedores3',
            'multiple' => true //esto me ayuda a que la busqueda sea multiple
          ],
          'pluginOptions' => ['initialize'=> true,'allowClear' => true]
          ]).'</div>
          <div class="col-lg-2">
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
          'id'=>'fecha_desde2']]).'
          </div>
          <div class="col-lg-2">
          '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
          'id'=>'fecha_hasta2']]).'
          </div>
          <div class="col-lg-2">
          <a class="btn btn-default btn-sm" onclick="buscar_notas_credito()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
          <a class="btn btn-default btn-sm" id="imp_notas_credito" onclick="imprimir_notas_credito()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
          </div>
          <div class="col-lg-12"><br>
          <div class="well" id="mostrar_notas_credito"></div>
          </div>';
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>D151
        elseif ($tipo=='proveedores_D151') {
          echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Reporte D-151 Proveedores</h3>';
          /*$anoActual = date('Y');
          $slec = '';
          for($i=$anoActual-10;$i<=$anoActual;$i++) { $slec .= "<option value='".$i."'>".$i."</option>"; }
          echo '<div class="col-lg-4">
          <label>Selecione el año del periodo Fiscal a consultar: </label>
          <select name="fecha_desde3" id="fecha_desde3">
          <option value="0">Año</option>
          ' .$slec.'
          </select>
          </div>
          <div class="col-lg-2">

          </div>';*/
          $anoActual = date('Y');
          $arrayAnnos = null;
          for($i=$anoActual;$i>=($anoActual-10);$i--){
            $arrayAnnos[$i]=$i;
            // $slec .= "<option value='".$i."'>".$i."</option>";
          }
          echo '<div class="col-lg-6 col-md-6">
          <label for="annoFiscalD151Proveedor">Selecione el año del periodo Fiscal a consultar: </label>
          '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
            'name' => '',
            'data' => $arrayAnnos,
            'options' => [
              'placeholder' => '- Seleccione -',
              'id'=>'annoFiscalD151Proveedor',
              'name'=>'annoFiscalD151Proveedor'
            ],
            'pluginOptions' => ['initialize'=> true,'allowClear' => true]
            ]).'</div>';

          echo '<div class="col-lg-6 col-md-6" style="margin-top:25px;">
          <a class="btn btn-default btn-sm" onclick="buscar_reporte_proveedor_d151()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
          <a class="btn btn-default btn-sm" id="imp_proveedor_d151" onclick="imprimir_reporte_proveedor_d151()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
          </div>
          ';
          echo '<div class="col-lg-12"><br>
          <div class="well" id="mostrar_provedor_d151"></div>
          </div>';
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>D151 DETALLE
        elseif ($tipo=='proveedores_D151_detalle') {
          /*vehiel 04-12-2018*/
          echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"> Reporte D-151 Detalle Proveedores</h3>';
          $anoActual = date('Y');
          $arrayAnnos = null;
          for($i=$anoActual;$i>=($anoActual-10);$i--){
            /*[2018=>2018,2017=>2017....2008=>2008]*/
            $arrayAnnos[$i]=$i;
          }
          echo '<div class="col-lg-6 col-md-6">
          <label for="filtro_anno_fiscal_d151_detalle_proveedor">Año Fiscal</label>
          '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
            'name' => '',
            'data' => $arrayAnnos,
            'options' => [
              'placeholder' => '- Seleccione -',
              'id'=>'filtro_anno_fiscal_d151_detalle_proveedor',
              'onchange'=>'javascript:cargarProveedoresPerido()',
              'name'=>'filtro_anno_fiscal_d151_detalle_proveedor'
            ],
            'pluginOptions' => ['initialize'=> true,'allowClear' => true]
            ]).'</div>';
            /*obtenemos los id de los proveedores de las compras que entran el ultimo periodo fiscal*/
            $sql = "SELECT SUM(subTotal-descuento) AS sumD151, idProveedor FROM `tbl_compras`
            WHERE idCompra NOT IN (SELECT fact.idCompra FROM tbl_compras fact INNER JOIN
              (SELECT idCompra, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_pagar` WHERE tipo_mov = 'nc' GROUP BY idCompra) fact_not
              ON fact.idCompra = fact_not.idCompra
              WHERE fact.total = fact_not.suma_nc)
              AND fechaDocumento BETWEEN :fechaFactura1 AND :fechaFactura2  GROUP BY idProveedor DESC
              HAVING sumD151 >=".Yii::$app->params['limite_monto_compra_MIN'].";"; /*Limite impuesto por el MIN en compra de articulos. ¢2,500,000*/

              $anoPasado = date('Y', strtotime('-1 year'));
              $anoActual = date('Y');
              $fechaFactura1 = $anoPasado.'-10-01';
              $fechaFactura2 = $anoActual.'-09-30';
              $command = \Yii::$app->db->createCommand($sql);
              $command->bindParam(":fechaFactura1", $fechaFactura1);
              $command->bindParam(":fechaFactura2", $fechaFactura2);
              $facturas = $command->queryAll();
              /*creamos el array que nos va a ayudar a encontrar los proveedores mas adelante*/
              $proveedoresArray = [];
              /*para cada factura, sacmos el id de proveedor*/
              foreach ($facturas as $key => $factura) {
                $proveedoresArray[]=$factura['idProveedor'];
              }
              /*obtemos un objeto con los proveedores*/
              $proveedores = Proveedores::find()->where(['IN','codProveedores',$proveedoresArray])->all();
              echo '<div class="col-lg-6">
              <label id="label_filtro_proveedores_d151_detalle" for="filtro_proveedores_d151_detalle">Proveedores 2018</label>
              '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                'name' => '',
                /*se cargan los clientes para el ultimo periodo fiscal*/
                'data' => ArrayHelper::map($proveedores,'codProveedores',function($element) {
                  return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                }),
                'options' => [
                  'placeholder' => '- Seleccione -',
                  'id'=>'filtro_proveedores_d151_detalle',
                  'multiple' => true //esto me ayuda a que la busqueda sea multiple
                ],
                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                ]).'</div>';

                echo '<div class="col-lg-6 col-md-6" style="margin-top:25px;">
                <a class="btn btn-default btn-sm" onclick="buscar_reporte_proveedor_d151_detalle()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                <a class="btn btn-default btn-sm" id="imp_D151_detalle" onclick="imprimir_reporte_proveedor_d151_detalle()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                </div>
                ';
                echo '<div class="col-lg-12"><br>
                <div class="well" id="mostrar_d151_detalle"></div>
                </div>';
              } /* fin if ($tipo=='proveedores_D151_detalle')*/

              ?>
              <script type="text/javascript">
              $(document).ready(function () {
                //mantenemos todos los boton de imprimir desabilitado
                $('#imp_resumen_cuenta_pagar').addClass('disabled');
                $('#imp_notas_debito').addClass('disabled');
                $('#imp_notas_credito').addClass('disabled');
                $('#imp_tramites').addClass('disabled');
                $('#imp_proveedor_d151').addClass('disabled');
                $('#imp_D151_detalle').addClass('disabled');
              });
              //REPORTES PARA cuentas_x_pagar
              function buscar_resumen_cuenta_pagar() {//MOSTRAR EN PANTALLA
                var filtro_proveedores = $("#filtro_proveedores").val();
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                document.getElementById("mostrar_resumen_cuenta_pagar").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rprovee_m_resumen_cuenta_pagar') ?>" ,
                { 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  $('#mostrar_resumen_cuenta_pagar').html(data);
                  $('#imp_resumen_cuenta_pagar').removeClass('disabled');//abilitamos boton de imprimir
                }).fail(function() {
                  document.getElementById("mostrar_resumen_cuenta_pagar").innerHTML = '<center><h2><small>No se encontraron registros</small></h2></center>';
                });
              }
              function imprimir_resumen_cuenta_pagar() {//IMPRIMIR
                var filtro_proveedores = $("#filtro_proveedores").val();
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iprovee_m_resumen_cuenta_pagar') ?>" ,
                { 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
                });
              }

              //reportes de tramite_pagos
              function buscar_tramites() {
                var filtro_proveedores = $("#filtro_proveedores_tr").val();
                fecha_desde = document.getElementById('fecha_desde_tr').value;
                fecha_hasta = document.getElementById('fecha_hasta_tr').value;
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                document.getElementById("mostrar_tramites").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rprovee_m_tramites') ?>" ,
                { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  $('#mostrar_tramites').html(data);
                  $('#imp_tramites').removeClass('disabled');//abilitamos boton de imprimir
                }).fail(function() {
                  document.getElementById("mostrar_tramites").innerHTML = '<center><h2><small>No se encontraron registros</small></h2></center>';
                });
              }
              function imprimir_tramites() {
                var filtro_proveedores = $("#filtro_proveedores_tr").val();
                fecha_desde = document.getElementById('fecha_desde_tr').value;
                fecha_hasta = document.getElementById('fecha_hasta_tr').value;
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iprovee_m_tramites') ?>" ,
                { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
                });
              }

              //REPORTES PARA notas_debito
              function buscar_notas_debito() {
                var filtro_proveedores = $("#filtro_proveedores2").val();
                fecha_desde = document.getElementById('fecha_desde').value;
                fecha_hasta = document.getElementById('fecha_hasta').value;
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                document.getElementById("mostrar_notas_debito").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rprovee_m_notas_debito') ?>" ,
                { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  $('#mostrar_notas_debito').html(data);
                  $('#imp_notas_debito').removeClass('disabled');//abilitamos boton de imprimir
                }).fail(function() {
                  document.getElementById("mostrar_notas_debito").innerHTML = '<center><h2><small>No se encontraron registros</small></h2></center>';
                });
              }
              function imprimir_notas_debito() {
                var filtro_proveedores = $("#filtro_proveedores2").val();
                fecha_desde = document.getElementById('fecha_desde').value;
                fecha_hasta = document.getElementById('fecha_hasta').value;
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iprovee_m_notas_debito') ?>" ,
                { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
                });
              }

              //REPORTES PARA notas_credito
              function buscar_notas_credito() {//MOSTRAR EN PANTALLA
                var filtro_proveedores = $("#filtro_proveedores3").val();
                fecha_desde = document.getElementById('fecha_desde2').value;
                fecha_hasta = document.getElementById('fecha_hasta2').value;
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                document.getElementById("mostrar_notas_credito").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rprovee_m_notas_credito') ?>" ,
                { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  $('#mostrar_notas_credito').html(data);
                  $('#imp_notas_credito').removeClass('disabled');//abilitamos boton de imprimir
                }).fail(function() {
                  document.getElementById("mostrar_notas_credito").innerHTML = '<center><h2><small>No se encontraron registros</small></h2></center>';
                });
              }
              function imprimir_notas_credito() {//imprimir
                var filtro_proveedores = $("#filtro_proveedores3").val();
                fecha_desde = document.getElementById('fecha_desde2').value;
                fecha_hasta = document.getElementById('fecha_hasta2').value;
                var lista_filtro_proveedores = '0';
                if (filtro_proveedores==null) {
                  lista_filtro_proveedores = '0';
                } else {
                  lista_filtro_proveedores = filtro_proveedores.toString();
                }
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iprovee_m_notas_credito') ?>" ,
                { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'lista_filtro_proveedores' : lista_filtro_proveedores } ,
                function( data ) {
                  var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
                });
              }
              //==============FUNCIONES PARA MOSTRAR E IMPRIMIR REPORTE D151
              /*Este reporte muestra D151*/
              function buscar_reporte_proveedor_d151() {
                anoSeleccionado = document.getElementById('annoFiscalD151Proveedor').value;
                /*vehiel 04-12-2018
                se hace esta validación por el cambio que se hizo del select a select2
                si no se selecciona nada, el valor sera '0', con esto se valida donde hace la consulta*/
                if (anoSeleccionado=='') {
                  anoSeleccionado = '0';
                }
                document.getElementById("mostrar_provedor_d151").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rprovee_m_d151') ?>" ,
                {   'anoSeleccionado' : anoSeleccionado} ,
                function( data ) {
                  $('#mostrar_provedor_d151').html(data);
                  $('#imp_proveedor_d151').removeClass('disabled');//abilitamos boton de imprimir
                });
              }

              /*Este reporte imprime las venta detalles*/
              function imprimir_reporte_proveedor_d151() {
                anoSeleccionado = document.getElementById('annoFiscalD151Proveedor').value;
                /*vehiel 04-12-2018
                se hace esta validación por el cambio que se hizo del select a select2
                si no se selecciona nada, el valor sera '0', con esto se valida donde hace la consulta*/
                if (anoSeleccionado=='') {
                  anoSeleccionado = '0';
                }
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iprovee_m_d151') ?>" ,
                {   'anoSeleccionado' : anoSeleccionado} ,
                function( data ) {
                  var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
                });
              }
              /*vehiel 04-12-2018*/
              //==============FUNCIONES PARA MOSTRAR E IMPRIMIR REPORTE D151
              /*Este reporte muestra D151*/
              function buscar_reporte_proveedor_d151_detalle() {
                anoSeleccionado = document.getElementById('filtro_anno_fiscal_d151_detalle_proveedor').value;
                if (anoSeleccionado=='') {
                  anoSeleccionado = '0';
                }
                var filtro_proveedores = $("#filtro_proveedores_d151_detalle").val();
                var lista_proveedores = '0';
                if (filtro_proveedores !=null) {
                  lista_proveedores = filtro_proveedores.toString();
                }
                document.getElementById("mostrar_d151_detalle").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rprovee_m_d151_detalle') ?>" ,
                {'anoSeleccionado' : anoSeleccionado,'lista_proveedores':lista_proveedores},
                function(data){
                  $('#mostrar_d151_detalle').html(data);
                  $('#imp_D151_detalle').removeClass('disabled');//abilitamos boton de imprimir
                });
              }

              /*Este reporte imprime las venta detalles*/
              function imprimir_reporte_proveedor_d151_detalle() {
                anoSeleccionado = document.getElementById('filtro_anno_fiscal_d151_detalle_proveedor').value;
                if (anoSeleccionado=='') {
                  anoSeleccionado = '0';
                }
                var filtro_proveedores = $("#filtro_proveedores_d151_detalle").val();
                var lista_proveedores = '0';
                if (filtro_proveedores !=null) {
                  lista_proveedores = filtro_proveedores.toString();
                }
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/iprovee_m_d151_detalle') ?>" ,
                {'anoSeleccionado' : anoSeleccionado,'lista_proveedores':lista_proveedores},
                function( data ) {
                  var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
                });
              }
              function cargarProveedoresPerido() {
                anno = document.getElementById('filtro_anno_fiscal_d151_detalle_proveedor').value;
                if(anno == ''){
                  anno = <?= date('Y')?>
                }
                $('#label_filtro_proveedores_d151_detalle').text("Proveedores "+anno);
                $('#filtro_proveedores_d151_detalle').html('<option value="0">Por favor espere...</option>');
                $.ajax({
                url:"?r=reporte/cargar-proveedor-periodo",
                type:"get",
                data: { 'anno' : anno},
                success: function(valores){
                // alert("success");
                $('#filtro_proveedores_d151_detalle').html(valores).fadeIn();
              },
              error:function(){
              $('#filtro_proveedores_d151_detalle').html('<option value="0">Ocurrio un error</option>');
            }
          });
        }
        /*fin vehiel 04-12-2018*/
        </script>
