<?php
use backend\models\ProductoServicios;
use backend\models\Empresa;
$empresaimagen = new Empresa();

//$inventario = ProductoServicios::find()->where(['tipo'=>'Producto'])->orderBy(['codFamilia' => SORT_ASC])->limit(50)->all();
$sql = 'SELECT codProdServicio, nombreProductoServicio, cantidadInventario, precioCompra,
(cantidadInventario*precioCompra) AS total_costo
FROM tbl_producto_servicios WHERE tipo = "Producto" ORDER BY codFamilia';
$command = \Yii::$app->db->createCommand($sql);
$inventario = $command->queryAll();
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
echo '<table WIDTH="100%" >
        <thead class="thead-inverse">
          <tr>
            <th style="text-align:left"><font face="arial" size=3>Código producto</font></th>
            <th style="text-align:left"><font face="arial" size=3>Descripción del producto</font></th>
            <th style="text-align:center"><font face="arial" size=3>Cantidad inventario</font></th>
            <th style="text-align:right"><font face="arial" size=3>Precio costo</font></th>
            <th style="text-align:right"><font face="arial" size=3>Total de costo</font></th>
          </tr>
        </thead>
        <tbody class="">
        </tbody>
          ';
          $total_generar_costo = 0;
          foreach ($inventario as $key => $producto) {
            $total_generar_costo += $producto['total_costo'];
            printf('<tr>
                      <td style="border-left-width: 3px; border-left-style: solid; border-left-color: #708090;"><font size=4><strong>%s</strong></font></td>
                      <td align="left" ><font size=2>%s</font></td>
            	    		<td align="center"><font size=2>%s</font></td>
            	    		<td align="right"><font size=2>%s</font></td>
            	    		<td align="right"><font size=2>%s</font></td>
                		</tr>',
                          $producto['codProdServicio'],
                          $producto['nombreProductoServicio'],
                          $producto['cantidadInventario'],
                          number_format($producto['precioCompra'],2),
                          number_format($producto['total_costo'],2)
                      );
          }
echo '
        <tfoot>
        <tr>
          <th style="text-align:left"><font face="arial" size=3></font></th>
          <th style="text-align:left"><font face="arial" size=3></font></th>
          <th style="text-align:center"><font face="arial" size=3></font></th>
          <th style="text-align:right"><font face="arial" size=3>Total costo:</font></th>
          <th style="text-align:right"><font face="arial" size=3>'.number_format($total_generar_costo,2).'</font></th>
        </tr>
        </tfoot>
      </table>';

 ?>
