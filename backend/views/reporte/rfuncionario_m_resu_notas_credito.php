<?php
use backend\models\Funcionario;
use backend\models\Empresa;
use backend\models\MovimientoCobrar;

$empresaimagen = new Empresa();
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO DESDE: '.$fecha_desde.' HASTA: '.$fecha_hasta.'</p>
      </span>';
      $fechaInicio1 = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
      $fechaInicio2 = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';
      $funcionarios = Funcionario::find()->all();
      if ($lista_filtro_funcionarios != '0') {
        $funcionarios = Funcionario::find()->where("idFuncionario IN (".$lista_filtro_funcionarios.")", [])->all();
      }
echo '<table class="items table table-striped">
        <thead>
        <th style="text-align:left"><font size=4>ID FUNCIONARIO</font></th>
        <th style="text-align:left"><font size=4>NOMBRE COMPLETO</font></th>
        <th></th>
        <th style="text-align:center"><font size=4>CANTIDAD DE NOT.CREDIT</font></th>
        <th style="text-align:right"><font size=4>MONTO POR NOT.CREDIT</font></th>
        </thead>
        <tbody>';
        $total_cantidad = $total_movimiento = 0;
        foreach ($funcionarios as $key => $funcionario) {
          $movimientos = MovimientoCobrar::find()->where(['usuario'=>$funcionario->username])
          ->andWhere("tipmov = 'NC' AND fecmov BETWEEN :fechaInicio1 AND :fechaInicio2", [':fechaInicio1'=>$fechaInicio1, ':fechaInicio2'=>$fechaInicio2])->all();
          if (@$movimientos) {
            $monto_movimiento = 0; $cantidad = 0;
            foreach ($movimientos as $turn => $movimiento) {
              $monto_movimiento += $movimiento->monto_movimiento;
              $cantidad +=1;
              $total_cantidad +=1;
              $total_movimiento += $movimiento->monto_movimiento;
            }
            printf('<tr>
                       <td style="text-align:left" bgcolor="#7FFFD4"><font size=5>%s</font></td>
                       <td style="text-align:left" bgcolor="#7FFFD4" colspan="2"><font size=5>%s</font></td>
                       <td style="text-align:center" bgcolor="#7FFFD4"><font size=5>%s</font></td>
                       <td style="text-align:right" bgcolor="#7FFFD4"><font size=5>%s</font></td>
                    <tr>',
                       'US: '.$funcionario->idFuncionario,
                       $funcionario->nombre.' '.$funcionario->apellido1.' '.$funcionario->apellido2,
                       $cantidad.' Not.Cred',
                       number_format($monto_movimiento,2)
                       );
         echo '<tr>
                  <th style="text-align:center">ID</th>
                  <th style="text-align:left">APLIC.FACTURA</th>
                  <th style="text-align:left">CLIENTE</th>
                  <th style="text-align:right">MONTO</th>
               </tr>';
          foreach ($movimientos as $turn => $movimiento) {
          echo '<tr>
                   <td style="text-align:center">'.$movimiento->idMov.'</td>
                   <td style="text-align:left">'.$movimiento->idCabeza_Factura.'</td>
                   <td style="text-align:left">'.$movimiento->idCliente.'</td>
                   <td style="text-align:right">'.number_format($movimiento->monto_movimiento,2).'</td>
                </tr>';
           }

          }
        }
echo '    <tr>
           <td style="text-align:center" colspan="2"></td>
           <td style="text-align:right"><font size=5><strong>TOTAL:</strong></font></td>
           <td style="text-align:center"><font size=5><strong>'.$total_cantidad.' Not.Creds</strong></font></td>
           <td style="text-align:right"><font size=5><strong>'.number_format($total_movimiento,2).'</strong></font></td>
          </tr>
        <tbody>
      </table>';
