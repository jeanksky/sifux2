<?php
use backend\models\Empresa;
use backend\models\RecepcionHaciendaGeneral;

$empresaimagen = new Empresa();
$fecha_hora_d = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
$fecha_hora_h = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y',strtotime($fecha_hora_d))." <strong>Hasta: </strong>".Date('d-m-Y',strtotime($fecha_hora_h)).'</p>
</span>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
echo '<thead class="thead-inverse">';
printf('<tr>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
'Nombre del emisior',
'Tipo de documento',
'Consecutivo electrónico',
'Fecha hora documento',
'Fecha hora resolución',
'Subtotal',
'Descuento',
'IV',
'Total',
'Usuario registro'
);
echo '</thead>';
echo '<tbody class="">';


$subtotal = $descuento = $impuesto = $monto_total = 0;
$lista_respuesta = $lista_respuesta == '0' ? "'01'": $lista_respuesta;
$lista_documento_electronico = $lista_documento_electronico == '0' ? "'Factura electrónica','Tiquete Electrónico','Nota de crédito electrónica','Nota de débito electrónica'": $lista_documento_electronico;
$lista_resolucion = $lista_resolucion == '0' ? "'Aceptado','Parcialmente aceptado','Rechazado'" : $lista_resolucion;
$recepcion_general = RecepcionHaciendaGeneral::find()
->where([ 'between', 'reha_fecha_hora_documento', $fecha_hora_d, $fecha_hora_h ])
->andWhere("reha_tipo_documento IN (".$lista_documento_electronico.")",[])
->andWhere("reha_resolucion IN (".$lista_resolucion.")",[])
->andWhere("reha_respuesta_hacienda IN (".$lista_respuesta.")",[])
//->andWhere(['tipoRecibo'=>'NC'])
//->andWhere(['!=','tipoFacturacion','5'])
->all();
foreach($recepcion_general as $recepcion) {

  $fecha_resolucion = date('d-m-Y / h:m:s A', strtotime( $recepcion['reha_fecha_hora'] ));
  $fecha_documento = date('d-m-Y / h:m:s A', strtotime( $recepcion['reha_fecha_hora_documento'] ));
  printf('<tr style="color:#231BB8">
  <td align="center"><font face="arial" size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  </tr>',
  $recepcion['reha_nombre_emisor'],
  $recepcion['reha_tipo_documento'],
  $recepcion['reha_consecutivo_electronico'],
  $fecha_documento,
  $fecha_resolucion,
  number_format($recepcion['reha_subtotal'],2),
  number_format($recepcion['reha_descuento'],2),
  number_format($recepcion['reha_impuesto'],2),
  number_format($recepcion['reha_total'],2),
  $recepcion['reha_usuario_registra']
);
$subtotal += $recepcion['reha_subtotal'];
$descuento += $recepcion['reha_descuento'];
$impuesto += $recepcion['reha_impuesto'];
$monto_total += $recepcion['reha_total'];
} /* fin foreach $gastos*/

printf('<tr>
<td colspan="5" style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
</tr>',
'<h4>Total: </h4>',
'<h4><strong>'.number_format($subtotal,2).'</strong></h4>',
'<h4><strong>'.number_format($descuento,2).'</strong></h4>',
'<h4><strong>'.number_format($impuesto,2).'</strong></h4>',
'<h4><strong>'.number_format($monto_total,2).'</strong></h4>',
''
);
echo '</tbody>';

echo '</table>';

?>
