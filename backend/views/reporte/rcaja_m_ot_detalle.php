<?php
use backend\models\EncabezadoPrefactura;
use backend\models\FacturaOt;
use backend\models\Empresa;
use backend\models\Ot;
use backend\models\Clientes;
use backend\models\MovimientoCredito;
$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('d-m-Y', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('d-m-Y', strtotime( $fecha_hasta )) : '3890-01-01';

if($lista_cliente_ot != '0'){

  $clientes = Clientes::find()->where(['ot'=>'Si'])->andWhere("idCliente IN (".$lista_cliente_ot.")", [])->all();

  // echo '<div class="well">';
  echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
  echo '<span style="float:right">
  <p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y',strtotime($fechaFactura1))." <strong>Hasta: </strong>".Date('d-m-Y',strtotime($fechaFactura2)).'</p>
  </span>';

  $cantidad_ot_tot = $monto_ot_tot = 0;
  foreach ($clientes as $key => $cliente) {
    $cantidad_ot = $monto_ot = 0;
    $facturaPendiente = false;
    $get_ot = FacturaOt::find()->where(['idCliente'=>$cliente->idCliente])->all();

    foreach ($get_ot as $key => $value) {
      $ot_resumen = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$value->idCabeza_Factura])
      ->andWhere(['=','estadoFactura','Pendiente'])
      ->andWhere("fecha_inicio BETWEEN :fechaAbonos1 AND :fechaAbonos2", [':fechaAbonos1'=>$fechaFactura1, ':fechaAbonos2'=>$fechaFactura2])
      ->one();
      if($ot_resumen){
        // if ($ot_resumen->estadoFactura=='Pendiente' && strtotime($ot_resumen->fecha_inicio) >= strtotime($fechaFactura1) && strtotime($ot_resumen->fecha_inicio) <= strtotime($fechaFactura2)) {
        $cantidad_ot += 1;
        $monto_ot += $ot_resumen->total_a_pagar;
        $cantidad_ot_tot += 1;
        $monto_ot_tot += $ot_resumen->total_a_pagar;
        $facturaPendiente = true;
        // }
      }
    }

    if ($facturaPendiente==true) {
      $cliente_ot = Ot::find()->where(['idCliente'=>$cliente->idCliente])->one();
      echo '<table class="items table table-striped" width="100%">
      <thead>
      <tr>
      <th colspan="5" style="text-align:center">
      <h3>ID O.T: '.$cliente_ot->idOT.' - '.$cliente->nombreCompleto.'</h3>
      </th>
      </tr>
      <tr>
      <th colspan="5" style="text-align:center">
      <h3><small style="margin:25px;"> Tel: '.$cliente->celular.' '.$cliente->telefono.'</small>
      <small style="margin:25px;">Factura(s) O.T: '.$cantidad_ot.' </small>
      <small style="margin:25px;">Monto total: '.number_format($monto_ot,2).' </small></h3>
      </th>
      </tr>
      <tr>
      <th style="text-align:center">Documento / Cliente</th>
      <th style="text-align:center">Fecha Reg.</th>
      <th style="text-align:center">Fecha Ven</th>
      <th style="text-align:center">Dias Vencido</th>
      <th style="text-align:right">Monto</th>
      </tr>
      </thead>
      <tbody>';
      foreach ($get_ot as $key => $value) {
        $ot_resumen = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$value->idCabeza_Factura])->one();
        if ($ot_resumen->estadoFactura=='Pendiente' && strtotime($ot_resumen->fecha_inicio) >= strtotime($fechaFactura1) && strtotime($ot_resumen->fecha_inicio) <= strtotime($fechaFactura2)) {
          $fecha_vencimiento = date ( 'Y-m-d' , strtotime ( '+'.$cliente_ot->dias_ot.' day' , strtotime ( $ot_resumen->fecha_inicio ) ) );
          $mv = new MovimientoCredito();
          $dias_vencidos = strtotime($fecha_vencimiento) < strtotime(date('Y-m-d')) ? $mv->getMovimiento_de_dias( $fecha_vencimiento , date('Y-m-d')) : 0;
          if($cliente = Clientes::findOne($ot_resumen->idCliente))
          {
            $cli = $cliente->nombreCompleto;
          }else{
            if ($ot_resumen->idCliente=="")
            $cli = 'Cliente no asignado';
            else
            $cli = $ot_resumen->idCliente;
          }
          printf('<tr style="color:#231BB8">
          <td align="center" ><font face="arial" size=2 >%s</font></td>
          <td align="center"><font size=2>%s</font></td>
          <td align="center"><font size=2>%s</font></td>
          <td align="center"><font size=2>%s</font></td>
          <td align="right"><font size=2>%s</font></td>
          </tr>',
          $ot_resumen->idCabeza_Factura.' / '.$cli,
          $ot_resumen->fecha_inicio,
          date('d-m-Y', strtotime( $fecha_vencimiento )),
          $dias_vencidos,
          number_format($ot_resumen->total_a_pagar,2)
        );
      }
    }/*fin foreach $get_ot*/
    echo "</tbody>
    </table>";
  }/* fin if $facturaPendiente*/
}/* fin foreach $clientes*/
echo '<table class="items table table-striped" width="100%">
<tbody>
<tr>
<td style="text-align:center"><font size=4>Factura(s) O.T Pendientes: '.$cantidad_ot_tot.'</font></td>
<td style="text-align:right"><font size=4>Total: '.number_format($monto_ot_tot,2).'</font></td>
</tr>
</tbody>
</table>';

// echo "</div>"; /*fin class well*/
}// fin if del if $lista_cliente_ot
else {
  echo '<h1 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;"><small>Seleccione al menos un cliente...!</small></h1>';
}
?>
