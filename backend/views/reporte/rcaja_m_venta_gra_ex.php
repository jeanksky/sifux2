<?php
use backend\models\EncabezadoPrefactura;
use backend\models\DetalleFacturas;
use backend\models\Empresa;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

/*$mediopagoproveedor = MedioPagoProveedor::find()->where(['=','cuenta_bancaria_local', $cuenta->idBancos])->andWhere("fecha_documento >= :fechaFactura1 AND fecha_documento <= :fechaFactura2", [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2])->orderBy(['idMedio_pago' => SORT_ASC])->all();*/
  $sql = "SELECT * FROM tbl_encabezado_factura WHERE fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2";
  $command = \Yii::$app->db->createCommand($sql);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $ventas = $command->queryAll();
  $cont_subto = $cont_descu = $cont_impue = $cont_total = 0;
  $cred_subto = $cred_descu = $cred_impue = $cred_total = 0;
  $vent_ex = $vent_gra = 0;
  foreach($ventas as $factura) {
    $cont_subto += $factura['tipoFacturacion']!='5' ?  $factura['subtotal'] : 0;
    $cred_subto += $factura['tipoFacturacion']=='5' ?  $factura['subtotal'] : 0;
    $cont_descu += $factura['tipoFacturacion']!='5' ?  $factura['porc_descuento'] : 0;
    $cred_descu += $factura['tipoFacturacion']=='5' ?  $factura['porc_descuento'] : 0;
    $cont_impue += $factura['tipoFacturacion']!='5' ?  $factura['iva'] : 0;
    $cred_impue += $factura['tipoFacturacion']=='5' ?  $factura['iva'] : 0;
    $cont_total += $factura['tipoFacturacion']!='5' ?  $factura['total_a_pagar'] : 0;
    $cred_total += $factura['tipoFacturacion']=='5' ?  $factura['total_a_pagar'] : 0;
    $detalle_venta = DetalleFacturas::find()->where(['idCabeza_factura'=>$factura['idCabeza_Factura']])->all();
    foreach ($detalle_venta as $key => $value) {
      $dcto = $value->subtotal_iva != 0.00 ? 0 : $value->descuento_producto*@$value->cantidad;
      $dcto_imp = $value->subtotal_iva == 0.00 ? 0 : $value->descuento_producto*@$value->cantidad;
      if ($value->subtotal_iva == 0.00) {
        $vent_ex += ($value->precio_unitario * $value->cantidad) - $dcto;
      } else {
        $vent_gra += ($value->precio_unitario * $value->cantidad) - $dcto_imp;
      }
    }/*  fin foreach$detalle_venta*/
  } /* fin foreach $ventas*/
  $suma_subto = $cont_subto + $cred_subto;
  $suma_descu = $cont_descu + $cred_descu;
  $suma_impue = $cont_impue + $cred_impue;
  $suma_total = $cont_total + $cred_total;//
  $imp_vent = $suma_impue;
  $total_vent = $vent_ex + $vent_gra + $imp_vent;//$suma_total;
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y',strtotime($fechaFactura1))." <strong>Hasta: </strong>".Date('d-m-Y',strtotime($fechaFactura2)).'</p>
</span>';
echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >
      <thead class="thead-inverse">
        <tr>
          <th></th>
          <th style="text-align:right"><font face="arial" size=2>Sub Total</font></th>
          <th style="text-align:right"><font face="arial" size=2>Descuento</font></th>
          <th style="text-align:right"><font face="arial" size=2>Impuesto</font></th>
          <th style="text-align:right"><font face="arial" size=2>Total</font></th>
        </tr>
      </thead>
      <tbody class="">
        <tr style="color:#231BB8">
          <td><font face="arial" size=3 >CONTADO</font></td>
          <td align="right"><font size=2>'.number_format($cont_subto,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cont_descu,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cont_impue,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cont_total,2).'</font></td>
        </tr>
        <tr style="color:#231BB8">
          <td><font face="arial" size=3 >CRÉDITO</font></td>
          <td align="right"><font size=2>'.number_format($cred_subto,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cred_descu,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cred_impue,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cred_total,2).'</font></td>
        </tr>
      </tbody>
      <tbody>
        <tr style="color:#231BB8">
          <td><font face="arial" size=2 ></font></td>
          <td align="right"><font size=3>'.number_format($suma_subto,2).'</font></td>
          <td align="right"><font size=3>'.number_format($suma_descu,2).'</font></td>
          <td align="right"><font size=3>'.number_format($suma_impue,2).'</font></td>
          <td align="right"><font size=3>'.number_format($suma_total,2).'</font></td>
        </tr>
        <tr style="color:#231BB8">
          <td colspan="4" align="right">
            <font size=4>
              Total Ventas Exentas:<br>
              Total Ventas Gravadas:<br>
              Impuesto de Ventas:<br>
              Total Ventas:
            </font>
          </td>
          <td align="right"><font size=4>'.number_format($vent_ex,2).'<br>'.number_format($vent_gra,2).'<br>'.number_format($imp_vent,2).'<br>'.number_format($total_vent,2).'</font></td>
        </tr>
      </tbody>
      </table>';
?>
