<?php
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use backend\models\Empresa;
use backend\models\OrdenCompraProv;
use backend\models\Proveedores;

$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';


	//>>>>>>>>>>>>>>>>>>>>>>>>>ORDENES DE COMPRA X PROVEEDOR
	if ($tipo == 'ordenesxproveedor') {
		 $ProveedorP = Proveedores::find()->all();
    			echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de ordenes de Compra por proveedor</h3>';
          echo '<div class="col-lg-6">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
              'name' => '',
              'data' => ArrayHelper::map($ProveedorP,'codProveedores',function($element) {
                          return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                      }),
              'options' => [
                  'placeholder' => '- Seleccione los proveedores a filtrar -',
                  'id'=>'filtro_proveedor',
                  'multiple' => true //esto me ayuda a que la busqueda sea multiple
              ],
              'pluginOptions' => ['initialize'=> true,'allowClear' => true]
          ]).'</div>';
          echo '<div class="col-lg-2">
                        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                        'options' => ['class'=>'form-control', 'placeholder'=>'FECH.DESDE dd-mm-yyyy',
                        'id'=>'fecha_desde1']]).'
                </div>
                <div class="col-lg-2">
                        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                        'options' => ['class'=>'form-control', 'placeholder'=>'FECH.HASTA dd-mm-yyyy',
                        'id'=>'fecha_hasta1']]).'
                </div>
                <div class="col-lg-2">
                    <a class="btn btn-default btn-sm" onclick="buscar_reporte_ordenes_compro_x_proveedor()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                    <a class="btn btn-default btn-sm" id="imp_re_ordenes_compras_x_proveedor" onclick="imprimir_reporte_ordenes_compro_x_proveedor()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                </div>
                ';
          echo '<div class="col-lg-12"><br>
                      <div class="well" id="mostrar_ordenes_compras_x_proveedor"></div>
                </div>';
	}//fin de $tipo == 'ordenesxproveedor'
  //>>>>>>>>>>>>>>>>>>>>>>>>>RESUMEN DE COMPRAS
  elseif ($tipo == 'resumen_compras') {
             echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen de Compras(Proveedores)</h3>';
          echo '<div class="col-lg-3">
                        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                        'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
                        'id'=>'fecha_desde2']]).'
                </div>
                <div class="col-lg-3">
                        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                        'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
                        'id'=>'fecha_hasta2']]).'
                </div>
                <div class="col-lg-3">
                    <a class="btn btn-default btn-sm" onclick="buscar_reporte_resumen_compras()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                    <a class="btn btn-default btn-sm" id="imp_re_resumen_compras" onclick="imprimir_reporte_resumen_compras()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                </div>
                ';
          echo '<div class="col-lg-12"><br>
                      <div class="well" id="mostrar_resumen_compras"></div>
                </div>';
  }//fin de $tipo == 'resumen_compras'
	elseif ($tipo == 'detalle_compras') {
						 echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Detalle de Compras(Proveedores)</h3>';
					echo '<div class="col-lg-3">
												'.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
												'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy','autocomplete'=>'off',
												'id'=>'fecha_desde4']]).'
								</div>
								<div class="col-lg-3">
												'.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
												'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy','autocomplete'=>'off',
												'id'=>'fecha_hasta4']]).'
								</div>
								<div class="col-lg-3">
										<a class="btn btn-default btn-sm" onclick="buscar_reporte_detalle_compras()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
										<a class="btn btn-default btn-sm" id="imp_re_detalle_compras" onclick="imprimir_reporte_detalle_compras()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
								</div>
								';
					echo '<div class="col-lg-12"><br>
											<div class="well" id="mostrar_detalle_compras"></div>
								</div>';
	}//fin de $tipo == 'detalle_compras'
  //>>>>>>>>>>>>>>>>>>>>>>>>>RESUMEN COMPRAS GRAVADAS / EXENTAS
  elseif ($tipo=='resumen_compras_grabada_excenta') {
          echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen Compras Gravadas / Exentas</h3>';
          echo '<div class="col-lg-3">
                        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                        'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy',
                        'id'=>'fecha_desde3']]).'
                </div>
                <div class="col-lg-3">
                        '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                        'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy',
                        'id'=>'fecha_hasta3']]).'
                </div>
                <div class="col-lg-3">
                    <a class="btn btn-default btn-sm" onclick="buscar_reporte_compra_gra_ex()" ><span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</a>
                    <a class="btn btn-default btn-sm" id="imp_re_resumen_compras_gra_ex" onclick="imprimir_reporte_compra_gra_ex()" ><span class="glyphicon glyphicon-print" aria-hidden="true"> Imprimir</a>
                </div>
                ';
          echo '<div class="col-lg-12"><br>
                      <div class="well" id="mostrar_resumen_compras_gra_ex"></div>
                </div>';
        }
 ?>

 <script type="text/javascript">

       $(document).ready(function () {
              //mantenemos todos los boton de imprimir desabilitado
              $('#imp_re_ordenes_compras_x_proveedor').addClass('disabled');
              $('#imp_re_resumen_compras').addClass('disabled');
              $('#imp_re_resumen_compras_gra_ex').addClass('disabled');
							$('#imp_re_detalle_compras').addClass('disabled');

          });

 	//==============FUNCIONES PARA MOSTRAR E IMPRIMIR ABONOS
      function buscar_reporte_ordenes_compro_x_proveedor() {
        fecha_desde = document.getElementById('fecha_desde1').value;
        fecha_hasta = document.getElementById('fecha_hasta1').value;
        var filtro_proveedor = $("#filtro_proveedor").val();
        var lista_proveedores = '0';
        if (filtro_proveedor==null) {
          lista_proveedores = '0';
        } else {
          lista_proveedores = filtro_proveedor.toString();
        }
        document.getElementById("mostrar_ordenes_compras_x_proveedor").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcompras_m_orden_compra_x_proveedor') ?>" ,
            { 'lista_proveedores' : lista_proveedores, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
            function( data ) {
              $('#mostrar_ordenes_compras_x_proveedor').html(data);
              $('#imp_re_ordenes_compras_x_proveedor').removeClass('disabled');//abilitamos boton de imprimir
            });
      }

      function imprimir_reporte_ordenes_compro_x_proveedor() {
        fecha_desde = document.getElementById('fecha_desde1').value;
        fecha_hasta = document.getElementById('fecha_hasta1').value;
        var filtro_proveedor = $("#filtro_proveedor").val();
        var lista_proveedores = '0';
        if (filtro_proveedor==null) {
          lista_proveedores = '0';
        } else {
          lista_proveedores = filtro_proveedor.toString();
        }
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icompras_m_orden_compra_x_proveedor') ?>" ,
            { 'lista_proveedores' : lista_proveedores, 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
            function( data ) {
              var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                ventimp.document.write(data);
                ventimp.document.close();
                ventimp.print();
                ventimp.close();
            });
      }//fin de funcion


      //==============FUNCIONES PARA MOSTRAR E IMPRIMIR RESUMEN COMPRAS
      /*Este reporte muestra el Resumen Compras*/
      function buscar_reporte_resumen_compras() {
          fecha_desde = document.getElementById('fecha_desde2').value;
          fecha_hasta = document.getElementById('fecha_hasta2').value;
          document.getElementById("mostrar_resumen_compras").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
          $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcompras_m_resumen_compras') ?>" ,
              { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
              function( data ) {
                $('#mostrar_resumen_compras').html(data);
                $('#imp_re_resumen_compras').removeClass('disabled');//abilitamos boton de imprimir
              });
      }
      /*Este reporte imprime el Resumen Compras*/
      function imprimir_reporte_resumen_compras() {
          fecha_desde = document.getElementById('fecha_desde2').value;
          fecha_hasta = document.getElementById('fecha_hasta2').value;
          $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icompras_m_resumen_compras') ?>" ,
              { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
              function( data ) {
                var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                  ventimp.document.write(data);
                  ventimp.document.close();
                  ventimp.print();
                  ventimp.close();
              });
      }//fin de funcion

			//==============FUNCIONES PARA MOSTRAR E IMPRIMIR DETALLE COMPRAS
			/*Este reporte muestra el Resumen Compras*/
			function buscar_reporte_detalle_compras() {
					fecha_desde = document.getElementById('fecha_desde4').value;
					fecha_hasta = document.getElementById('fecha_hasta4').value;
					document.getElementById("mostrar_detalle_compras").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
					$.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcompras_m_detalle_compras') ?>" ,
							{ 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
							function( data ) {
								$('#mostrar_detalle_compras').html(data);
								$('#imp_re_detalle_compras').removeClass('disabled');//abilitamos boton de imprimir
							});
			}
			/*Este reporte imprime el Resumen Compras*/
			function imprimir_reporte_detalle_compras() {
					fecha_desde = document.getElementById('fecha_desde4').value;
					fecha_hasta = document.getElementById('fecha_hasta4').value;
					$.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icompras_m_detalle_compras') ?>" ,
							{ 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
							function( data ) {
								var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

									ventimp.document.write(data);
									ventimp.document.close();
									ventimp.print();
									ventimp.close();
							});
			}//fin de funcion

      //==============FUNCIONES PARA MOSTRAR E IMPRIMIR RESUMEN COMPRA GRAVADAS / EXENTAS
      function buscar_reporte_compra_gra_ex() {
        fecha_desde = document.getElementById('fecha_desde3').value;
        fecha_hasta = document.getElementById('fecha_hasta3').value;
        document.getElementById("mostrar_resumen_compras_gra_ex").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcompras_m_compras_gra_ex') ?>" ,
            { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
            function( data ) {
              $('#mostrar_resumen_compras_gra_ex').html(data);
              $('#imp_re_resumen_compras_gra_ex').removeClass('disabled');//abilitamos boton de imprimir
            });
      }

      function imprimir_reporte_compra_gra_ex() {
        fecha_desde = document.getElementById('fecha_desde3').value;
        fecha_hasta = document.getElementById('fecha_hasta3').value;
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/icompras_m_compras_gra_ex') ?>" ,
            { 'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta } ,
            function( data ) {
              var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'

                ventimp.document.write(data);
                ventimp.document.close();
                ventimp.print();
                ventimp.close();
            });
      }//fin de funcion
 </script>
