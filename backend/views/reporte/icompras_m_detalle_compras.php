<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: center;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
h3{
	color: black;
}
</style>
<?php
use backend\models\Empresa;
use backend\models\Comrpas;
use backend\models\Proveedores;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

//$sql = "SELECT * FROM `tbl_compras` WHERE `estado` = 'Aplicado' AND `fechaRegistro` BETWEEN :fechaFactura1 AND :fechaFactura2";

$sql = "SELECT  comp.`idProveedor` ,provee.`nombreEmpresa`,comp.`n_interno_prov` AS N_Interno,
comp.`numFactura` AS N_Factura,comp.`fechaRegistro`,comp.`formaPago`,
comp.`subTotal`,comp.`descuento`,comp.`impuesto`,comp.`total`,comp.`estado`
FROM `tbl_compras`  AS comp INNER JOIN `tbl_proveedores` AS provee
ON provee.`codProveedores` = comp.`idProveedor`
WHERE `fechaRegistro` BETWEEN :fechaFactura1 AND :fechaFactura2 ORDER BY `fechaRegistro` ASC";
$command = \Yii::$app->db->createCommand($sql);
$command->bindParam(":fechaFactura1", $fechaFactura1);
$command->bindParam(":fechaFactura2", $fechaFactura2);
$compras = $command->queryAll();


$f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
$f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
</span>';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Detalle de Compras(Proveedores)</h3>';
//Variables para almacenar los datos de totales
$comprasContado = 0;
$comprasCredito = 0;
$totalCompras = 0;
echo '<div class="table-responsive">';
echo '<table class="items table table-striped" id=""  >';



echo '<thead>';
printf('<tr>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
<th style="text-align:right" ><font face="arial" size=2>%s</font></th>
<th style="text-align:right" ><font face="arial" size=2>%s</font></th>
<th style="text-align:right" ><font face="arial" size=2>%s</font></th>
<th style="text-align:right" ><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
</tr>',
'ID Pro.',
'Nombre Empresa',
'N° Interno',
'N° Factura',
'Fecha Registro',
'Forma Pago',
'Subtotal',
'Descuento',
'Impuesto',
'Total',
'Estado'
);
echo '</thead>';
echo '<tbody class="">';
foreach ($compras as $key => $CompraContado) {
  if($CompraContado['formaPago'] === "Contado"){
    $comprasContado += $CompraContado['total'];
  }else  if($CompraContado['formaPago'] === "Crédito"){
    $comprasCredito += $CompraContado['total'];
  }

  $totalCompras += $CompraContado['total'];

  printf('<tr style="color:#231BB8">
  <td width="10"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  <td style="text-align:right" width="40"><font size=2>%s</font></td>
  <td style="text-align:right" width="40"><font size=2>%s</font></td>
  <td style="text-align:right" width="40"><font size=2>%s</font></td>
  <td style="text-align:right" width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  </tr>',
  $CompraContado['idProveedor'],
  $CompraContado['nombreEmpresa'],
  $CompraContado['N_Interno'],
  $CompraContado['N_Factura'],
  Date('d-m-Y',strtotime($CompraContado['fechaRegistro'])),
  $CompraContado['formaPago'],
  number_format($CompraContado['subTotal'],2,'.',','),
  number_format($CompraContado['descuento'],2,'.',','),
  number_format($CompraContado['impuesto'],2,'.',','),
  number_format($CompraContado['total'],2,'.',','),
  $CompraContado['estado']
);

}//fin foreach
printf('<tr>
<td  width="20"><font size=2>%s</font></td>
<td colspan="3" width="40"><font size=2>%s</font></td>
<td colspan="4" width="40"><font size=2>%s</font></td>
<td colspan="3" width="40"><font size=2>%s</font></td>
</tr>',
'',
'<h4>Total Compra Contado: <strong>'.number_format($comprasContado,2,'.',',').'</strong></h4>',
'<h4>Total Compra Crédito: <strong>'.number_format($comprasCredito,2,'.',',').'</strong></h4>',
'<h4>Total Compra: <strong>'.number_format($totalCompras,2,'.',',').'</strong></h4>'
);
echo '</tbody>';
echo '</table>';
echo '</div>'; /*fin table-responsive*/

?>
