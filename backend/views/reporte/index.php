<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReporteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton2.gif';
$load_buton_ = \Yii::$app->request->BaseUrl.'/img/load.gif';
$this->title = 'Reportes del sistema';
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }

    /* custom inclusion of right, left and below tabs */
    .tabs-below > .nav-tabs,
    .tabs-right > .nav-tabs,
    .tabs-left > .nav-tabs {
      border-bottom: 0;
    }

    .tab-content > .tab-pane,
    .pill-content > .pill-pane {
      display: none;
    }

    .tab-content > .active,
    .pill-content > .active {
      display: block;
    }

    .tabs-below > .nav-tabs {
      border-top: 1px solid #ddd;
    }

    .tabs-below > .nav-tabs > li {
      margin-top: -1px;
      margin-bottom: 0;
    }

    .tabs-below > .nav-tabs > li > a {
      -webkit-border-radius: 0 0 4px 4px;
         -moz-border-radius: 0 0 4px 4px;
              border-radius: 0 0 4px 4px;
    }

    .tabs-below > .nav-tabs > li > a:hover,
    .tabs-below > .nav-tabs > li > a:focus {
      border-top-color: #ddd;
      border-bottom-color: transparent;
    }

    .tabs-below > .nav-tabs > .active > a,
    .tabs-below > .nav-tabs > .active > a:hover,
    .tabs-below > .nav-tabs > .active > a:focus {
      border-color: transparent #ddd #ddd #ddd;
    }

    .tabs-left > .nav-tabs > li,
    .tabs-right > .nav-tabs > li {
      float: none;
    }

    .tabs-left > .nav-tabs > li > a,
    .tabs-right > .nav-tabs > li > a {
      min-width: 74px;
      margin-right: 0;
      margin-bottom: 3px;
    }

    .tabs-left > .nav-tabs {
      float: left;
      margin-right: 19px;
      border-right: 1px solid #ddd;
    }

    .tabs-left > .nav-tabs > li > a {
      margin-right: -1px;
      -webkit-border-radius: 4px 0 0 4px;
         -moz-border-radius: 4px 0 0 4px;
              border-radius: 4px 0 0 4px;
    }

    .tabs-left > .nav-tabs > li > a:hover,
    .tabs-left > .nav-tabs > li > a:focus {
      border-color: #eeeeee #dddddd #eeeeee #eeeeee;
    }

    .tabs-left > .nav-tabs .active > a,
    .tabs-left > .nav-tabs .active > a:hover,
    .tabs-left > .nav-tabs .active > a:focus {
      border-color: #ddd transparent #ddd #ddd;
      *border-right-color: #ffffff;
    }

    .tabs-right > .nav-tabs {
      float: right;
      margin-left: 19px;
      border-left: 1px solid #ddd;
    }

    .tabs-right > .nav-tabs > li > a {
      margin-left: -1px;
      -webkit-border-radius: 0 4px 4px 0;
         -moz-border-radius: 0 4px 4px 0;
              border-radius: 0 4px 4px 0;
    }

    .tabs-right > .nav-tabs > li > a:hover,
    .tabs-right > .nav-tabs > li > a:focus {
      border-color: #eeeeee #eeeeee #eeeeee #dddddd;
    }

    .tabs-right > .nav-tabs .active > a,
    .tabs-right > .nav-tabs .active > a:hover,
    .tabs-right > .nav-tabs .active > a:focus {
      border-color: #ddd #ddd #ddd transparent;
      *border-left-color: #ffffff;
    }

    #verticalLine {
    border-left: thick solid #044e7d;
    border-right: thick solid #044e7d;
    }
</style>
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    });
    //Me muestra en el entorno del div ver_listado los listados de bancos, bancos y cuentas, guias, proveedores
    function rbancos_mostrar(tipo) {
      $('#rbancos_cuentas_bancarias').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rbancos_movimientos_libros').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rbancos_mostrar') ?>" ,
                { 'tipo' : tipo } ,
                function( data ) {
                  if (tipo=='cuentas_bancarias') {
                    $('#rbancos_cuentas_bancarias').html(data);
                  } else if (tipo=='movimientos_libros') {
                    $('#rbancos_movimientos_libros').html(data);
                  }

                });
    }

    function rcaja_mostrar(tipo) {
      $('#rcaja_balance').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_cuentasxcobrar').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_abonos').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_notasdebitocli').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_notascreditocli').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_recepciongeneral').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_pagosxcaja').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_resumvent_gravadaexenta').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_ventasdetalle').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_desechadas').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_ot_resumen').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_ot_detalle').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_D151').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_comisiones_resumen').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_comisiones_detalle').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_D151_detalle').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_apartado_lista').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
      $('#rcaja_apartado_cancelados').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');

      $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcaja_mostrar') ?>" ,
              { 'tipo' : tipo } ,
              function( data ) {
                if (tipo=='balance') {
                  $('#rcaja_balance').html(data);
                } else if (tipo=='cuent_x_cobra') {
                  $('#rcaja_cuentasxcobrar').html(data);
                } else if (tipo=='abonos') {
                  $('#rcaja_abonos').html(data);
                } else if (tipo=='not_deb_cli') {
                  $('#rcaja_notasdebitocli').html(data);
                } else if (tipo=='not_cre_cli') {
                  $('#rcaja_notascreditocli').html(data);
                } else if (tipo=='recepcion_general') {
                  $('#rcaja_recepciongeneral').html(data);
                } else if (tipo=='pago_x_caja') {
                  $('#rcaja_pagosxcaja').html(data);
                } else if (tipo=='res_vent_gr_ex') {
                  $('#rcaja_resumvent_gravadaexenta').html(data);
                } else if (tipo=='vent_detalle') {
                  $('#rcaja_ventasdetalle').html(data);
                } else if (tipo=='desechadas') {
                  $('#rcaja_desechadas').html(data);
                } else if (tipo=='ot_resumen') {
                  $('#rcaja_ot_resumen').html(data);
                } else if (tipo=='ot_detalle') {
                  $('#rcaja_ot_detalle').html(data);
                } else if (tipo=='D151') {
                  $('#rcaja_D151').html(data);
                } else if (tipo=='comisiones_resumen') {
                  $('#rcaja_comisiones_resumen').html(data);
                } else if (tipo=='comisiones_detalle') {
                  $('#rcaja_comisiones_detalle').html(data);
                } else if(tipo == 'D151_detalle'){
                  $('#rcaja_D151_detalle').html(data);
                } else if(tipo == 'apartado_lista'){
                  $('#rcaja_apartado_lista').html(data);
                } else if (tipo=='apartado_cancelados') {
                  $('#rcaja_apartado_cancelados').html(data);
                }
              });
    }//fin funcion

      function rcompras_mostrar(tipo) {
        $('#rcompras_ordenesxproveedor').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rcompras_resumencompras').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rcompras_detallecompras').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rcompras_resumencompra_grabada_excenta').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rcompras_mostrar') ?>" ,
                  { 'tipo' : tipo } ,
                  function( data ) {
                    if (tipo == 'ordenesxproveedor') {
                      $('#rcompras_ordenesxproveedor').html(data);
                    } else if (tipo == 'resumen_compras') {
                      $('#rcompras_resumencompras').html(data);
                    }else if (tipo == 'resumen_compras_grabada_excenta') {
                      $('#rcompras_resumencompra_grabada_excenta').html(data);
                    }else if (tipo == 'detalle_compras') {
                      $('#rcompras_detallecompras').html(data);
                    }
                  });
      }//fin funcion rcompras_mostrar

      //mostrar inventarios por categoria y por todos
      function rinventaio_mostrar(tipo) {
        $('#rinventario_resumen_costo').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rinventario_producto_categoria').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rinventario_producto_debajo_minimo').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rinventario_producto_no_vendido').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $.ajax({
            url:"<?= Yii::$app->getUrlManager()->createUrl('reporte/rinventaio_mostrar') ?>",
            type:"post",
            data: {'tipo': tipo},
            beforeSend: function() {
              if (tipo == 'costo_inventario') {
                document.getElementById("rinventario_costo").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"><h2>Espere, esto puede tardar hasta 5 minutos segun su inventario y ancho de banda de su internet.</h2></center>';
              }
            },
            success: function(data){
              if (tipo == 'costo_inventario') {
                $('#rinventario_costo').html(data);
              } else if (tipo == 'resumen_costo_inventario') {
                $('#rinventario_resumen_costo').html(data);
              }else if (tipo == 'producto_x_categoria') {
                $('#rinventario_producto_categoria').html(data);
              }else if (tipo == 'producto_x_debajo_minimo') {
                $('#rinventario_producto_debajo_minimo').html(data);
              }else if (tipo == 'producto_no_vendido') {
                $('#rinventario_producto_no_vendido').html(data);
              }
            },
            error: function(msg, status,err){
                          //  alert('error en linea 204, consulte a nelux');
                        }
        });
      }
      //mostrar reporte de proveedores
      function rproveedores_mostrar(tipo) {
        $('#rproveedores_cuentas_x_pagar').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rproveedores_tramite_pagos').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rproveedores_notas_debito').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rproveedores_notas_credito').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rproveedores_D151').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rproveedores_D151_detalle').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');

        $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rproveedores_mostrar') ?>" ,
                { 'tipo' : tipo } ,
                function( data ) {
                  if (tipo == 'cuentas_x_pagar') {
                    $('#rproveedores_cuentas_x_pagar').html(data);
                  } else if (tipo=='tramite_pagos') {
                    $('#rproveedores_tramite_pagos').html(data);
                  } else if (tipo == 'notas_debito') {
                    $('#rproveedores_notas_debito').html(data);
                  }else if (tipo == 'notas_credito') {
                    $('#rproveedores_notas_credito').html(data);
                  }else if (tipo == 'proveedores_D151') {
                    $('#rproveedores_D151').html(data);
                  }else if (tipo == 'proveedores_D151_detalle') {
                    $('#rproveedores_D151_detalle').html(data);
                  }
                });
      }

      //mostrar reporte de USUARIOS
      function rusuarios_mostrar(tipo) {
        $('#rusuarios_resumen_venta').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $('#rusuarios_resumen_notas_credito').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rusuarios_mostrar') ?>" ,
                { 'tipo' : tipo } ,
                function( data ) {
                  if (tipo == 'resumen_venta') {
                    $('#rusuarios_resumen_venta').html(data);
                  } else if (tipo == 'resumen_notas_credito') {
                    $('#rusuarios_resumen_notas_credito').html(data);
                  }
                });
      }
      //mostrar reporte de ORDEN DE SERVICIO
            function rorden_servicio_mostrar(tipo) {
              $('#rorden_servi_pendiente').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
              $('#rorden_servi_resumen_meca').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
              $('#rorden_servi_detalle_meca').html('<center><img src="<?= $load_buton_ ?>" style="height: 600px;"></center>');
              $.get( "<?= Yii::$app->getUrlManager()->createUrl('reporte/rorden_servicio_mostrar') ?>" ,
                      { 'tipo' : tipo } ,
                      function( data ) {
                        if (tipo == 'pendiente') {
                          $('#rorden_servi_pendiente').html(data);
                        } else if (tipo == 'resumen_meca') {
                          $('#rorden_servi_resumen_meca').html(data);
                        }else if (tipo == 'detalle_meca') {
                          $('#rorden_servi_detalle_meca').html(data);
                        }
                      });
            }

</script>
<div class="facturas-index">
  <center><h1><?= Html::encode($this->title) ?></h1></center>
  <div class="bs-example">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <span class="glyphicon glyphicon-plus"></span> REPORTE BANCOS
                  </a>
              </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">

                <!--div class="tabbable tabs-right"--><!--https://www.bootply.com/74926-->
                  <ul class="nav nav-tabs">
                    <li><a href="#rbancos_cuentas_bancarias" data-toggle="tab" onclick="rbancos_mostrar('cuentas_bancarias')">Cuentas bancarias (Saldos):</a></li>
                    <li><a href="#rbancos_movimientos_libros" data-toggle="tab" onclick="rbancos_mostrar('movimientos_libros')">Mov. en libros (Cuentas):</a></li>
                  </ul>
                  <div id="verticalLine">
                    <div style="height: 600px;width: 100%; overflow-y: auto; ">
                      <div class="tab-content">
                        <div class="tab-pane active"><center><h1><small>Seleccione una opción del menú superior</small></h1></center></div>
                        <div class="tab-pane" id="rbancos_cuentas_bancarias"></div>
                        <div class="tab-pane" id="rbancos_movimientos_libros"></div>
                      </div>
                    </div>
                  </div>

                <!--/div-->
              </div>
          </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span> REPORTE CAJA</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                      <li><a href="#rcaja_balance" data-toggle="tab" onclick="rcaja_mostrar('balance')">Balance de Caja</a></li>
                      <li><a href="#rcaja_abonos" data-toggle="tab" onclick="rcaja_mostrar('abonos')">Abonos & Cancelaciones</a></li>
                      <li><a href="#rcaja_cuentasxcobrar" data-toggle="tab" onclick="rcaja_mostrar('cuent_x_cobra')">Cuentas por Cobrar</a></li>
                      <li><a href="#rcaja_notasdebitocli" data-toggle="tab" onclick="rcaja_mostrar('not_deb_cli')">Notas de Débito a Clientes</a></li>
                      <li><a href="#rcaja_notascreditocli" data-toggle="tab" onclick="rcaja_mostrar('not_cre_cli')">Notas de Crédito a Clientes</a></li>
                      <li><a href="#rcaja_pagosxcaja" data-toggle="tab" onclick="rcaja_mostrar('pago_x_caja')">Gastos</a></li>
                      <li><a href="#rcaja_recepciongeneral" data-toggle="tab" onclick="rcaja_mostrar('recepcion_general')">Por recepción general</a></li>
                      <li><a href="#rcaja_resumvent_gravadaexenta" data-toggle="tab" onclick="rcaja_mostrar('res_vent_gr_ex')">Ventas Gravadas & Exentas</a></li>
                    </ul>
                  <div class="tabbable tabs-right"><!--https://www.bootply.com/74926-->
                    <ul class="nav nav-tabs">


                      <li><a href="#rcaja_ventasdetalle" data-toggle="tab" onclick="rcaja_mostrar('vent_detalle')">Ventas Detalle</a></li>
                      <li><a href="#rcaja_desechadas" data-toggle="tab" onclick="rcaja_mostrar('desechadas')">Pre-facturas Desechadas</a></li>

                      <?php
                      $ot = Yii::$app->params['ot'];
                      if ($ot == true) {
                        ?> <li><a href="#rcaja_ot_resumen" data-toggle="tab" onclick="rcaja_mostrar('ot_resumen')">Facturas O.T Resumen</a></li>
                           <li><a href="#rcaja_ot_detalle" data-toggle="tab" onclick="rcaja_mostrar('ot_detalle')">Factura O.T Detalle</a></li> <?php
                      }
                      ?>
                      <li><a href="#rcaja_D151" data-toggle="tab" onclick="rcaja_mostrar('D151')">D-151</a></li>
                      <li><a href="#rcaja_D151_detalle" data-toggle="tab" onclick="rcaja_mostrar('D151_detalle')">D-151 Detalle</a></li>
                      <?php
                        $comisiones = Yii::$app->params['comisiones'];
                        if ($comisiones == true) { ?>
                        <li><a href="#rcaja_comisiones_resumen" data-toggle="tab" onclick="rcaja_mostrar('comisiones_resumen')">Resumen de Comisiones</a></li>
                        <li><a href="#rcaja_comisiones_detalle" data-toggle="tab" onclick="rcaja_mostrar('comisiones_detalle')">Detalle de Comisiones / Agente</a></li>
                         <?php
                      }
                      ?>
                      <?php
                        if (Yii::$app->params['apartados'] == true ) { ?>
                        <li><a href="#rcaja_apartado_lista" data-toggle="tab" onclick="rcaja_mostrar('apartado_lista')">Lista de apartados</a></li>
                        <li><a href="#rcaja_apartado_cancelados" data-toggle="tab" onclick="rcaja_mostrar('apartado_cancelados')">Apartados cancelados</a></li>
                         <?php
                      }
                      ?>
                    </ul>

                    <div id="verticalLine" class="col-lg-10">
                      <div style="height: 600px;width: 100%; overflow-y: auto; ">
                        <div class="tab-content">
                          <div class="tab-pane active"><center><h1><small>Seleccione una opción del menú que está en la derecha del marco</small></h1></center></div>
                          <div class="tab-pane" id="rcaja_balance"></div>
                          <div class="tab-pane" id="rcaja_abonos"></div>
                          <div class="tab-pane" id="rcaja_cuentasxcobrar"></div>
                          <div class="tab-pane" id="rcaja_notasdebitocli"></div>
                          <div class="tab-pane" id="rcaja_notascreditocli"></div>
                          <div class="tab-pane" id="rcaja_pagosxcaja"></div>
                          <div class="tab-pane" id="rcaja_recepciongeneral"></div>
                          <div class="tab-pane" id="rcaja_resumvent_gravadaexenta"></div>
                          <div class="tab-pane" id="rcaja_ventasdetalle"></div>
                          <div class="tab-pane" id="rcaja_desechadas"></div>
                          <div class="tab-pane" id="rcaja_ot_resumen"></div>
                          <div class="tab-pane" id="rcaja_ot_detalle"></div>
                          <div class="tab-pane" id="rcaja_D151"></div>
                          <div class="tab-pane" id="rcaja_D151_detalle"></div>
                          <div class="tab-pane" id="rcaja_comisiones_resumen"></div>
                          <div class="tab-pane" id="rcaja_comisiones_detalle"></div>
                          <div class="tab-pane" id="rcaja_apartado_lista"></div>
                          <div class="tab-pane" id="rcaja_apartado_cancelados"></div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> REPORTE DE COMPRAS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                  <!--div class="tabbable tabs-right"--><!--https://www.bootply.com/74926-->
                  <ul class="nav nav-tabs">
                    <li><a href="#rcompras_ordenesxproveedor" data-toggle="tab" onclick="rcompras_mostrar('ordenesxproveedor')">Órdenes de compra x Proveedor</a></li>
                    <li><a href="#rcompras_resumencompras" data-toggle="tab" onclick="rcompras_mostrar('resumen_compras')">Resumen de compras</a></li>
                    <li><a href="#rcompras_detallecompras" data-toggle="tab" onclick="rcompras_mostrar('detalle_compras')">Detalle de compras</a></li>
                    <li><a href="#rcompras_resumencompra_grabada_excenta" data-toggle="tab" onclick="rcompras_mostrar('resumen_compras_grabada_excenta')">Resumen compras Grabadas / Exentas</a></li>
                  </ul>
                    <div id="verticalLine" >
                      <div style="height: 600px;width: 100%; overflow-y: auto; ">
                        <div class="tab-content">
                          <div class="tab-pane active"><center><h1><small>Seleccione una opción del menú superior</small></h1></center></div>
                          <div class="tab-pane" id="rcompras_ordenesxproveedor"></div>
                          <div class="tab-pane" id="rcompras_resumencompras"></div>
                          <div class="tab-pane" id="rcompras_detallecompras"></div>
                          <div class="tab-pane" id="rcompras_resumencompra_grabada_excenta"></div>
                        </div>
                      </div>
                    </div>
                  <!--/div-->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-plus"></span> REPORTE DE INVENTARIO</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                  <!--div class="tabbable tabs-below"--><!--https://www.bootply.com/74926-->
                    <ul class="nav nav-tabs">
                      <li style="float:right;"><a href="#rinventario_costo" data-toggle="tab" onclick="rinventaio_mostrar('costo_inventario')">Costo inventario <strong>(Consulta requiere tiempo)</strong></a></li>
                      <li><a href="#rinventario_resumen_costo" data-toggle="tab" onclick="rinventaio_mostrar('resumen_costo_inventario')">Resumen Costo inventario Categorías</a></li>
                      <li><a href="#rinventario_producto_categoria" data-toggle="tab" onclick="rinventaio_mostrar('producto_x_categoria')">Productos por Categoría</a></li>
                      <li><a href="#rinventario_producto_debajo_minimo" data-toggle="tab" onclick="rinventaio_mostrar('producto_x_debajo_minimo')">Productos por debajo del mínimo (Categoría)</a></li>
                      <li><a href="#rinventario_producto_no_vendido" data-toggle="tab" onclick="rinventaio_mostrar('producto_no_vendido')">Productos no vendidos</a></li>
                    </ul>
                    <div id="verticalLine">
                      <div style="height: 600px;width: 100%; overflow-y: auto; ">
                        <div class="tab-content">
                          <div class="tab-pane active"><center><h1><small>Seleccione una opción del menú superior</small></h1></center></div>
                          <div class="tab-pane" id="rinventario_costo"></div>
                          <div class="tab-pane" id="rinventario_resumen_costo"></div>
                          <div class="tab-pane" id="rinventario_producto_categoria"></div>
                          <div class="tab-pane" id="rinventario_producto_debajo_minimo"></div>
                          <div class="tab-pane" id="rinventario_producto_no_vendido"></div>
                        </div>
                      </div>
                    </div>
                  <!--/div-->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-plus"></span> REPORTE DE ORDEN DE SERVICIO</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                  <!--div class="tabbable tabs-below"--><!--https://www.bootply.com/74926-->
                    <ul class="nav nav-tabs">
                      <li><a href="#rorden_servi_pendiente" data-toggle="tab" onclick="rorden_servicio_mostrar('pendiente')">Orden de servicio pendiente</a></li>
                      <li><a href="#rorden_servi_resumen_meca" data-toggle="tab" onclick="rorden_servicio_mostrar('resumen_meca')">Resumen Orden de servicio por Mecánico</a></li>
                      <li><a href="#rorden_servi_detalle_meca" data-toggle="tab" onclick="rorden_servicio_mostrar('detalle_meca')">Detalle orden de servicio por Mecánico</a></li>
                    </ul>
                    <div id="verticalLine">
                      <div style="height: 600px;width: 100%; overflow-y: auto; ">
                        <div class="tab-content">
                          <div class="tab-pane active"><center><h1><small>Seleccione una opción del menú que está en el pie del marco</small></h1></center></div>
                          <div class="tab-pane" id="rorden_servi_pendiente"></div>
                          <div class="tab-pane" id="rorden_servi_resumen_meca"></div>
                          <div class="tab-pane" id="rorden_servi_detalle_meca"></div>
                        </div>
                      </div>
                    </div>
                  <!--/div-->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-plus"></span> REPORTE DE PROVEEDORES</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                  <!--div class="tabbable tabs-below"--><!--https://www.bootply.com/74926-->
                    <ul class="nav nav-tabs">
                      <li><a href="#rproveedores_cuentas_x_pagar" data-toggle="tab" onclick="rproveedores_mostrar('cuentas_x_pagar')">Resumen cuentas por pagar (Proveedores)</a></li>
                      <li><a href="#rproveedores_tramite_pagos" data-toggle="tab" onclick="rproveedores_mostrar('tramite_pagos')">Resumen de trámites de pagos</a></li>
                      <li><a href="#rproveedores_notas_debito" data-toggle="tab" onclick="rproveedores_mostrar('notas_debito')">Notas de débito a proveedores</a></li>
                      <li><a href="#rproveedores_notas_credito" data-toggle="tab" onclick="rproveedores_mostrar('notas_credito')">Notas de Crédito a proveedores</a></li>
                      <li><a href="#rproveedores_D151" data-toggle="tab" onclick="rproveedores_mostrar('proveedores_D151')">D-151</a></li>
                      <li><a href="#rproveedores_D151_detalle" data-toggle="tab" onclick="rproveedores_mostrar('proveedores_D151_detalle')">D-151 Detalle</a></li>
                    </ul>
                    <div id="verticalLine">
                      <div style="height: 600px;width: 100%; overflow-y: auto; ">
                        <div class="tab-content">
                          <div class="tab-pane active"><center><h1><small>Seleccione una opción del menú que está en el pie del marco</small></h1></center></div>
                          <div class="tab-pane" id="rproveedores_cuentas_x_pagar"></div>
                          <div class="tab-pane" id="rproveedores_tramite_pagos"></div>
                          <div class="tab-pane" id="rproveedores_notas_debito"></div>
                          <div class="tab-pane" id="rproveedores_notas_credito"></div>
                          <div class="tab-pane" id="rproveedores_D151"></div>
                          <div class="tab-pane" id="rproveedores_D151_detalle"></div>
                        </div>
                      </div>
                    </div>
                  <!--/div-->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><span class="glyphicon glyphicon-plus"></span> REPORTE DE USUARIOS</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">
                  <!--div class="tabbable tabs-below"--><!--https://www.bootply.com/74926-->
                    <ul class="nav nav-tabs">
                      <li><a href="#rusuarios_resumen_venta" data-toggle="tab" onclick="rusuarios_mostrar('resumen_venta')">Resumen de ventas por funcionario</a></li>
                      <li><a href="#rusuarios_resumen_notas_credito" data-toggle="tab" onclick="rusuarios_mostrar('resumen_notas_credito')">Resumen de notas de crédito por funcionario</a></li>
                    </ul>
                    <div id="verticalLine">
                      <div style="height: 600px;width: 100%; overflow-y: auto; ">
                        <div class="tab-content">
                          <div class="tab-pane active"><center><h1><small>Seleccione una opción del menú que está en el pie del marco</small></h1></center></div>
                          <div class="tab-pane" id="rusuarios_resumen_venta"></div>
                          <div class="tab-pane" id="rusuarios_resumen_notas_credito"></div>
                        </div>
                      </div>
                    </div>
                  <!--/div-->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><span class="glyphicon glyphicon-plus"></span> REPORTE DE VEHICULOS</a>
                </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse">
                <div class="panel-body">
                    <h3 align="center" style="color:red" ><span class="glyphicon glyphicon-warning-sign"></span> En mantenimiento. <span class="glyphicon glyphicon-warning-sign"></span></h3>
                </div>
            </div>
        </div>
    </div>
  	<p><strong>Nota:</strong> Desprenda del acordeón el tipo de reporte que desea consultar.</p>
  </div>
</div>
