<?php
use backend\models\MovimientoCobrar;
use backend\models\Recibos;
use backend\models\RecibosDetalle;
use backend\models\Gastos;
use backend\models\MedioPago;
use backend\models\Empresa;
use backend\models\FacturasDia;

$empresaimagen = new Empresa();
echo '<img src="'.$empresaimagen->getImageurl() .'" style="height: 100px;">';
$contado = 0;             $efectivo = 0;
$credito = 0;             $deposito = 0;
$total_ventas = 0;        $tarjetas = 0;
$abono_credito = 0;       $cheques = 0;
$total_ingresos = 0;      $dollar = 0;
$gastos = 0;              $total = 0;
$notas_credito = 0;
$total_caja = 0;

$fecha_d = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fecha_h = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
$fecha_hora_d = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
$fecha_hora_h = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';

//JEANK SQL que suma todas las ventas de credito pero aquellas son anuladas
$sql_credito = "SELECT SUM(total_a_pagar) AS sum_ventas, tipoFacturacion FROM `tbl_encabezado_factura`
WHERE fecha_emision BETWEEN :fecha_d AND :fecha_h AND estadoFactura != 'Anulada' GROUP BY tipoFacturacion DESC;";
$command_cr = \Yii::$app->db->createCommand($sql_credito);
$command_cr->bindParam(":fecha_d", $fecha_d);
$command_cr->bindParam(":fecha_h", $fecha_h);
$ventas_cr = $command_cr->queryAll();
foreach($ventas_cr as $factura) {
    if ($factura['tipoFacturacion'] == '4') {
      $credito = $factura['sum_ventas'];
    }//fin del if
}//fin de l foreach

/*Version credito NelGamb
$sql_credito = "SELECT * FROM `tbl_encabezado_factura`
WHERE fecha_final BETWEEN :fecha_d AND :fecha_h;";
$command_cr = \Yii::$app->db->createCommand($sql_credito);
$command_cr->bindParam(":fecha_d", $fecha_d);
$command_cr->bindParam(":fecha_h", $fecha_h);
$ventas_cr = $command_cr->queryAll();
foreach($ventas_cr as $factura) {
  if ($factura['tipoFacturacion'] != 'Ninguno') {
    if ($factura['tipoFacturacion'] == '4') {
      if ($factura['estadoFactura'] != 'Anulada') {
        $credito += $factura['total_a_pagar'];
      }
    }
  }
}//fin foreach  */

$sql_contado_y_cancelacion = "SELECT * FROM `tbl_encabezado_factura`
WHERE estadoFactura <> 'Anulada' AND fecha_emision BETWEEN :fecha_d AND :fecha_h;";
$command_co_ca = \Yii::$app->db->createCommand($sql_contado_y_cancelacion);
$command_co_ca->bindParam(":fecha_d", $fecha_d);
$command_co_ca->bindParam(":fecha_h", $fecha_h);
$ventas_co_ca = $command_co_ca->queryAll();
foreach($ventas_co_ca as $factura) {
  if ($factura['tipoFacturacion'] != 'Ninguno') {
    if ($factura['tipoFacturacion'] != '4') {
      $contado += $factura['total_a_pagar'];
    }
  }
}
$total_ventas = $contado + $credito;

//OBTENER MONTO DE ABONOS Y CANCELACIONES
$recibos = Recibos::find()->where([ 'between', 'fechaRecibo', $fecha_hora_d, $fecha_hora_h ])->all();
$cancelacion_credito = 0;
if (@$recibos) {
  foreach ($recibos as $key => $recibo) {
    if ($recibo->tipoRecibo == 'AB') {
      $abono_credito += $recibo->monto;
    }
   /* if ($recibo->tipoRecibo == 'NC') {
      $rec_detalle = RecibosDetalle::find()->where(['idRecibo'=>$recibo->idRecibo])->one();
      $movim = MovimientoCobrar::find()->where(['idMov'=>$rec_detalle->idDocumento])->one();
      $fact_filtro = FacturasDia::findOne($movim->idCabeza_Factura);
      if ($fact_filtro->tipoFacturacion != '4') {
        $notas_credito += $recibo->monto;
      }
    }*/
    if ($recibo->tipoRecibo == 'CF') {
      $cancelacion_credito += $recibo->monto;
    }

  }//fin foreach abono y cancelacion
}

$total_ingresos = $contado + $abono_credito + $cancelacion_credito;

//OBTENER MONTO DE GASTOS APLICADOS
$gastos_dia = Gastos::find()->where([ 'between', 'fechaAplicada', $fecha_d, $fecha_h ])->andWhere('estadoGasto = "Aplicado"',[])->all();
foreach ($gastos_dia as $key => $gasto) {
  $gastos += $gasto->montoGasto;
}

//OBTENER DESGLOSE DE INGRESOS
$medio_pagos = MedioPago::find()->where([ 'between', 'fecha_deposito', $fecha_d, $fecha_h ])->all();
foreach ($medio_pagos as $key => $medio_pago) {
  //$fact_filtro = FacturasDia::findOne($medio_pago->idCabeza_factura);
  $movimiento = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$medio_pago->idCabeza_factura])->one();

  if (@$movimiento->tipmov != 'NC') {
    if ($medio_pago->idForma_Pago == 1) {//efectivo
      $efectivo += $medio_pago->monto;
    } elseif ($medio_pago->idForma_Pago == 2){//deposito
      $deposito += $medio_pago->monto;
    } elseif ($medio_pago->idForma_Pago == 3) {//tarjeta
      $tarjetas += $medio_pago->monto;
    } elseif ($medio_pago->idForma_Pago == 5) {//cheque
      $cheques += $medio_pago->monto;
    }
  }
  //$efectivo =
}
$total = $efectivo + $deposito + $tarjetas + $cheques;
$total_caja = $total_ingresos - ( $gastos + $notas_credito );
?>
<div class="col-lg-2"></div>
<div class="col-lg-4">
  <table class="items table table-striped" id="tabla_facturas_pendientes">
    <thead class="thead-inverse">
      <tr>
        <th style="text-align:left" colspan="2"><font face="arial" size=6>Ventas</font></th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td style="text-align:right" ><font size=3>CONTADO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($contado,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>CRÉDITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($credito,2) ?></font></td>
      </tr>
      <tr>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=5>TOTAL VENTAS:</font></td>
        <td style="text-align:right" ><font size=5><?= number_format($total_ventas,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>CANCL & ABONO CRÉDITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($abono_credito + $cancelacion_credito,2) ?></font></td>
      </tr>
      <tr>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=5>TOTAL INGRESOS:</font></td>
        <td style="text-align:right" ><font size=5><?= number_format($total_ingresos,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>GASTOS:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($gastos,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>NOTAS DE CREDITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($notas_credito,2) ?></font></td>
      </tr>
      <tr>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=5>TOTAL EN CAJA:</font></td>
        <td style="text-align:right" ><font size=6><strong><?= number_format($total_caja,2) ?></strong></font></td>
      </tr>
    </tfoot>
  </table>
</div>
<div class="col-lg-4">
  <table class="items table table-striped" id="tabla_facturas_pendientes">
    <thead class="thead-inverse">
      <tr>
        <th style="text-align:left" colspan="2"><font face="arial" size=6>Desgloce de Ingresos</font></th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td style="text-align:right" ><font size=3>EFECTIVO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($efectivo,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>DEPOSITO:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($deposito,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>TARJETAS:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($tarjetas,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>CHEQUES:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($cheques,2) ?></font></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=3>**DOLLAR:</font></td>
        <td style="text-align:right" ><font size=4><?= number_format($dollar,2) ?></font></td>
      </tr>
      <tr>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td style="text-align:right" ><font size=6>TOTAL:</font></td>
        <td style="text-align:right" ><font size=6><?= number_format($total,2) ?></font></td>
      </tr>
    </tfoot>
  </table>
</div>
