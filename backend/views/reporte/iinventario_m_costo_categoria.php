<?php
use backend\models\Empresa;
use backend\models\OrdenCompraProv;
use backend\models\Familia;
use backend\models\ContactosPedidos;
use backend\models\ProductoServicios;

$empresaimagen = new Empresa();

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';

echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen Costo inventario Categorías</h3>';
      //Obtengo todos los proveedores y lo almaceno en el objero $proveedor ademas de que almaceno los que trae en la lista del selec2
      $sql = 'SELECT f.codFamilia AS id, f.descripcion AS des, mf.suma AS sum_can FROM tbl_familia f INNER JOIN
      (SELECT SUM(cantidadInventario*precioCompra) AS suma, codFamilia FROM tbl_producto_servicios
      WHERE tipo = "Producto" GROUP BY codFamilia) mf
      ON f.codFamilia = mf.codFamilia';
      $command = \Yii::$app->db->createCommand($sql);
      $categorias = $command->queryAll();
      //$categorias = Familia::find()->all();
      if ($lista_filtro_cagtegoria_inven != '0') {
        $categorias = Familia::find()->where("codFamilia IN (".$lista_filtro_cagtegoria_inven.")", [])->all();

      }
      echo '<table WIDTH="100%">
              <thead>';
          printf('<tr>
                   <th style="text-align:center">%s</th>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:right">%s</th>
                  </tr>',
                     'CÓDIGO CATEGORÍA',
                     'DESCRIPCIÓN DE CATEGORÍA',
                     'TOTAL DE COSTO'
                     );
          echo '</thead>';
          echo '<tbody>';
          $total_categoria_costo_grobal = 0;
          if ($lista_filtro_cagtegoria_inven != '0') {//con filtro
            foreach ($categorias as $key => $categoria) {
              $inventario = ProductoServicios::find()->where(['codFamilia'=>$categoria->codFamilia])->all();
              $total_categoria_costo = 0;
              foreach ($inventario as $key => $producto) {
                $total_costo = $producto->cantidadInventario * $producto->precioCompra;
                $total_categoria_costo += $total_costo;
              }
              $total_categoria_costo_grobal += $total_categoria_costo;
              printf('<tr>
                        <td align="center" ><font size=2>%s</font></td>
                        <td align="left" ><font size=2>%s</font></td>
                        <td align="right"><font size=2>%s</font></td>
                      </tr>',
                          $categoria->codFamilia,
                          $categoria->descripcion,
                          number_format($total_categoria_costo,2)
                        );
            }
          }
            else {//sin filtro
              foreach ($categorias as $key => $categoria) {
                $total_categoria_costo_grobal += $categoria['sum_can'];
                printf('<tr>
                          <td align="center" ><font size=2>%s</font></td>
                          <td align="left" ><font size=2>%s</font></td>
                          <td align="right"><font size=2>%s</font></td>
                        </tr>',
                            $categoria['id'],
                            $categoria['des'],
                            number_format($categoria['sum_can'],2)
                          );
              }
            }

          echo '</tbody>';
          echo '<tbody>
                  <tr>
                    <th></th>
                    <th style="text-align:right"><strong>Total:</strong></th>
                    <th style="text-align:right"><strong>'.number_format($total_categoria_costo_grobal,2).'</strong></th>
                  </tr>
                </tbody> ';
      echo '</table>';
 ?>
