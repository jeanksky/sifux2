<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
</style>
<?php
use backend\models\CuentasBancarias;
use yii\helpers\ArrayHelper;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\models\EntidadesFinancieras;


$empresaimagen = new Empresa();

$cuentas = CuentasBancarias::find()->all();

echo '<div class="col-lg-1">';
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>Emitido: '.Date('d-m-Y h:i:s A').'</p>
</span>';
echo '</div>';
      echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Cuentas bancarias (Saldos)</h3>';
      echo '<div class="helper pull-right col-lg-2">

      </div>';
      echo '<div class="col-lg-12"><table class="items table table-striped" id="tabla_facturas_pendientes"  >';
      echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
                        'MONEDA',
                        'ENTIDAD FINANCIERA',
                        'N° CUENTA',
                        'DESCRIPCION',
                        'SALDO LIBROS',
                        'SALDO BANCOS'
                        //'SALDO'
                        );
                echo '</thead>';
echo '<tbody class="">';

            foreach($cuentas as $position => $cuenta) {
          $moneda = Moneda::find()->where(['idTipo_moneda'=>$cuenta['idTipo_moneda']])->one();
        $entidades_financieras = EntidadesFinancieras::find()->where(['idEntidad_financiera'=>$cuenta['idEntidad_financiera']])->one();

            printf('<tr style="color:#231BB8">
                  <td align="center" ><font face="arial" size=2 >%s</font></td>
                    <td align="center" width="200" ><font size=2>%s</font></td>
                    <td align="center"><font size=2>%s</font></td>
                    <td align="right"><font size=2>%s</font></td>
                    <td align="right"><font size=2>%s</font></td>
                    <td align="right"><font size=2>%s</font></td></td>
                  </tr>',
                          $moneda->descripcion,
                          $entidades_financieras->descripcion,
                          $cuenta['numero_cuenta'],
                          $cuenta['descripcion'],
                          $moneda->simbolo.number_format($cuenta['saldo_libros'],2),
                          $moneda->simbolo.number_format($cuenta['saldo_bancos'],2)

                      );
          }//fin del foreach

      echo '</tbody>';
      echo '</table>';
      echo '</div>';
  ?>
