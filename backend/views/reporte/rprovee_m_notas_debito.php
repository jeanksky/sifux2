<?php
use backend\models\Proveedores;
use backend\models\Empresa;
use backend\models\MovimientoPagar;
use backend\models\ComprasInventario;

$empresaimagen = new Empresa();
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
echo '<BR><span style="float:right">
        <p>RANGO DESDE: '.$fecha_desde.' HASTA: '.$fecha_hasta.'</p>
      </span>';
      $fechaOrdCompra1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
      $fechaOrdCompra2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
      $proveedores = Proveedores::find()->all();
      if ($lista_filtro_proveedores != '0') {
        $proveedores = Proveedores::find()->where("codProveedores IN (".$lista_filtro_proveedores.")", [])->all();
      }
      echo '<table class="items table table-striped">';
          echo '<tbody>';
          $suma_total_movimiento = $suma_total_saldo = 0;
            foreach ($proveedores as $key => $proveedor) {
              $compras = ComprasInventario::find()->where(['idProveedor'=>$proveedor->codProveedores])->all();
              if (@$compras) {
                $bandera = 'false';
                foreach ($compras as $ubi => $compra) {
                  $movimientos_compruebo = MovimientoPagar::find()->where(['idCompra'=>$compra->idCompra])
                  ->andWhere("tipo_mov = 'nd' AND fecha_mov >= :fechaOrdCompra1 AND fecha_mov <= :fechaOrdCompra2",
                  [':fechaOrdCompra1'=>$fechaOrdCompra1, ':fechaOrdCompra2'=>$fechaOrdCompra2])->one();
                  if (@$movimientos_compruebo) {
                    $bandera = 'true';
                  }

                }
                if ($bandera=='true') {
                  echo '<thead><tr><th colspan="7" style="text-align:center"><h3>'.$proveedor->nombreEmpresa.'<br><small>Tel: '.$proveedor->telefono.' / '.$proveedor->telefono2.'</small></h3></th></tr>';
                  printf('<tr>
                               <th style="text-align:left">%s</th>
                               <th style="text-align:center">%s</th>
                               <th style="text-align:left">%s</th>
                               <th style="text-align:center">%s</th>
                               <th style="text-align:right">%s</th>
                               <th style="text-align:right">%s</th>
                               <th style="text-align:center">%s</th>
                              </tr>',
                                 'N° FACTURA',
                                 'FECHA',
                                 'N° ND',
                                 'N° ND PROVEEDOR',
                                 'MONTO MOVIMIENTO',
                                 'SALDO',
                                 'DETALLE'
                                 );
                      echo '</thead>';

                  foreach ($compras as $ubi => $compra) {
                    $movimientos = MovimientoPagar::find()->where(['idCompra'=>$compra->idCompra])->andWhere("tipo_mov = 'nd' AND fecha_mov >= :fechaOrdCompra1 AND fecha_mov <= :fechaOrdCompra2", [':fechaOrdCompra1'=>$fechaOrdCompra1, ':fechaOrdCompra2'=>$fechaOrdCompra2])->orderBy(['idMov' => SORT_ASC])->all();
                    if (@$movimientos) {
                        $rowspan = 0; $td = ''; $suma_movimiento = $suma_saldo = 0;
                        foreach ($movimientos as $posi => $movimiento) {
                          $rowspan += 1;
                          $suma_movimiento += $movimiento->monto_movimiento;
                          $suma_saldo += $movimiento->saldo_pendiente;
                          $suma_total_movimiento += $movimiento->monto_movimiento;
                          $suma_total_saldo += $movimiento->saldo_pendiente;
                        }
                        $resumen = '<br>Suma de montos:<span style="float:right"><strong>'.number_format($suma_movimiento,2).'</strong></span>
                        <br>Suma saldos:<span style="float:right"><strong>'.number_format($suma_saldo,2).'</strong></span>';
                        $resumen = $rowspan > 1 ? $resumen : '';

                          echo '<tr><td width=20% rowspan='.$rowspan.' style="border-right-width: 3px; border-right-style: solid; border-right-color: #ddd;" >
                          <font size=6>'.$compra->numFactura.'</font>'.$resumen.'</td>';


                        foreach ($movimientos as $posi => $movimiento) {
                          echo '
                                 <td style="text-align:center">'.date('d-m-Y', strtotime( $movimiento->fecha_mov )).'</td>
                                 <td style="text-align:center"><font size=4>'.$movimiento->idMov.'</font></td>
                                 <td style="text-align:center">'.$movimiento->numero_mov.'</td>
                                 <td style="text-align:right">'.number_format($movimiento->monto_movimiento,2).'</td>
                                 <td style="text-align:right">'.number_format($movimiento->saldo_pendiente,2).'</td>
                                 <td style="text-align:left" width=30%>'.$movimiento->detalle.'</td>
                                </tr>';
                          }
                          echo '<td colspan="7"></td>';
                          }

                  }
                }//fin bandera
              }

            }
          echo '</tbody>';
          echo '<tfoot>
                      <tr>
                       <th style="text-align:left"></th>
                       <th style="text-align:center"></th>
                       <th style="text-align:left"></th>
                       <th style="text-align:right">TOTAL:</th>
                       <th style="text-align:right"><font size=4>'.number_format($suma_total_movimiento,2).'</font></th>
                       <th style="text-align:right"><font size=4>'.number_format($suma_total_saldo,2).'</font></th>
                       <th style="text-align:center"></th>
                      </tr>';
              echo '</tfoot>';
          echo '</table>';
 ?>
