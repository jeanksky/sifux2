<?php
use backend\models\Empresa;
use backend\models\Gastos;
use backend\models\MedioPagoGastos;
use backend\models\TiposGastos;
use backend\models\ProveedoresGastos;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>RANGO: <strong>Desde: </strong>'.Date('d-m-Y',strtotime($fechaFactura1))." <strong>Hasta: </strong>".Date('d-m-Y',strtotime($fechaFactura2)).'</p>
</span>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
echo '<thead class="thead-inverse">';
printf('<tr>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:center"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th>
<th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
'N° Gastos',
'Proveedor',
'Tipo Gasto',
'Medio Pago',
'N° Documento',
'Fecha Aplicado',
'Subtotal',
'Descuento',
'IV',
'Total'
);
echo '</thead>';
echo '<tbody class="">';

/*Variables para la suma total de Gasto por medios de pagos*/
$Efectivo = 0; //1
$Transferencia = 0; //2
$Tarjeta = 0; //3
$Cheque = 0; //4
$subtotal = $descuento = $impuesto = $monto_total = 0;

$sql = "SELECT * FROM tbl_gastos WHERE estadoGasto = 'Aplicado' AND fechaAplicada BETWEEN :fechaFactura1 AND :fechaFactura2";

$command = \Yii::$app->db->createCommand($sql);
$command->bindParam(":fechaFactura1", $fechaFactura1);
$command->bindParam(":fechaFactura2", $fechaFactura2);
$gastos = $command->queryAll();
foreach($gastos as $gst2) {
  $proveedorGst2 = ProveedoresGastos::find()->where(['idProveedorGastos'=>$gst2['idProveedorGastos']])->one();
  $tipoGst2 = TiposGastos::find()->where(['idTipoGastos'=>$gst2['idTipoGastos']])->one();
  $medioPagoGst2 = MedioPagoGastos::find()->where(['idMedioPagoGastos'=>$gst2['idMedioPagoGastos']])->one();

  if($medioPagoGst2->idMedioPagoGastos == 1){/*1 Efectivo*/
    $Efectivo += $gst2['montoGasto'];
  }
  elseif ($medioPagoGst2->idMedioPagoGastos == 2) {/*2 Transferencia*/
    $Transferencia += $gst2['montoGasto'];
  }
  elseif ($medioPagoGst2->idMedioPagoGastos == 3) {/*3 Tarjeta*/
    $Tarjeta += $gst2['montoGasto'];
  }
  elseif ($medioPagoGst2->idMedioPagoGastos == 4) {/*4 Cheque*/
    $Cheque += $gst2['montoGasto'];
  }

  $fechaGasto = date('d-m-Y', strtotime( $gst2['fechaAplicada'] ));

  printf('<tr style="color:#231BB8">
  <td align="center"><font face="arial" size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="center"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  <td align="right"><font size=2>%s</font></td>
  </tr>',
  $gst2['idGastos'],
  $proveedorGst2->nombreEmpresa,
  $tipoGst2->descripcionTipoGastos,
  $medioPagoGst2->descripcionMedioPagoGastos,
  $gst2['numeroDocumento'],
  $fechaGasto,
  number_format($gst2['subtotal'],2),
  number_format($gst2['descuento'],2),
  number_format($gst2['impuesto'],2),
  number_format($gst2['montoGasto'],2)
);
$subtotal += $gst2['subtotal'];
$descuento += $gst2['descuento'];
$impuesto += $gst2['impuesto'];
$monto_total += $gst2['montoGasto'];
} /* fin foreach $gastos*/

printf('<tr>
<td colspan="6" style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
<td style="text-align:right" width=""><font size=3>%s</font></td>
</tr>',
'<h4>Total: </h4>',
'<h4><strong>'.number_format($subtotal,2).'</strong></h4>',
'<h4><strong>'.number_format($descuento,2).'</strong></h4>',
'<h4><strong>'.number_format($impuesto,2).'</strong></h4>',
'<h4><strong>'.number_format($monto_total,2).'</strong></h4>'
);
echo '</tbody>';

echo '</table>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes2"  >';
echo '<tbody class="">
<td style="text-align:left"  width="450"><font size=5>Desglose de Pagos Por Caja:</font></td>
<td style="text-align:left">
<font size=4>Efectivo:<br>Transferencia:<br>Tarjetas:<br>Cheques:</font>
</td>
<td style="text-align:right">
<font size=4>
'.number_format($Efectivo,2).'<br>'.number_format($Transferencia,2).'<br>'.number_format($Tarjeta,2).'<br>'.number_format($Cheque,2).'
</font>
</td>
<td colspan="4"></td>

<tr>
<td colspan="5" style="text-align:right" ><font size=5>TOTAL:</font></td>
<td colspan="2" style="text-align:right" ><font size=5>'.number_format($monto_total,2).'</font></td>
</tr>
</tbody>';
echo '</table>';
?>
