<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: center;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
h3{
	color: black;
}
</style>
<?php
use backend\models\Empresa;
use backend\models\Comrpas;
use backend\models\Proveedores;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';
$hoyMenosDiez = date('Y-m-d', strtotime('-10 year',strtotime('today')));
$hoy = date('Y-m-d', strtotime('today'));
$sql = "SELECT `codProdServicio`,`nombreProductoServicio`,`cantidadInventario` AS CANT,`ubicacion` AS UB FROM `tbl_producto_servicios` p WHERE p.codProdServicio IN
(SELECT dfac.`codProdServicio` FROM `tbl_detalle_facturas` dfac INNER JOIN `tbl_encabezado_factura` fac
ON dfac.`idCabeza_factura` = fac.`idCabeza_Factura`
WHERE fac.fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2 AND dfac.`codProdServicio`
NOT IN (SELECT dfac.`codProdServicio` FROM `tbl_detalle_facturas` dfac INNER JOIN `tbl_encabezado_factura` fac
ON dfac.`idCabeza_factura` = fac.`idCabeza_Factura`
WHERE fac.fecha_emision BETWEEN :fechaFactura3 AND :fechaFactura4)) AND p.cantidadInventario > 0";
$command = \Yii::$app->db->createCommand($sql);
$command->bindParam(":fechaFactura1", $hoyMenosDiez);
$command->bindParam(":fechaFactura2", $hoy);
$command->bindParam(":fechaFactura3", $fechaFactura1);
$command->bindParam(":fechaFactura4", $fechaFactura2);
$productos = $command->queryAll();

$f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
$f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
<p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
</span>';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Productos no vendidos</h3>';
echo '<table class="items table table-striped" id=""  >';
echo '<thead>';
printf('<tr>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
<th><font face="arial" size=2>%s</font></th>
</tr>',
'Código Producto',
'Nombre Producto',
'Cantidad',
'Ubicación'
);
echo '</thead>';
echo '<tbody class="">';
foreach ($productos as $key => $producto) {

  printf('<tr style="color:#231BB8">
  <td width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  <td width="40"><font size=2>%s</font></td>
  </tr>',
  $producto['codProdServicio'],
  $producto['nombreProductoServicio'],
  number_format($producto['CANT'],2,'.',','),
  $producto['UB']
);


}//fin foreach

echo '</tbody>';
echo '</table>';

?>
