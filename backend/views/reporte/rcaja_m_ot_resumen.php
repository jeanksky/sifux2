<?php
use backend\models\EncabezadoPrefactura;
use backend\models\FacturaOt;
use backend\models\Empresa;
use backend\models\Ot;
$empresaimagen = new Empresa();
echo '<div class="well">
        <img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<table class="items table table-striped" width="100%">
        <thead>
        <th style="text-align:left"><font size=4>ID</font></th>
        <th style="text-align:left"><font size=4>NOMBRE O.T</font></th>
        <th style="text-align:center"><font size=4>CELULAR</font></th>
        <th style="text-align:center"><font size=4>TELÉFONO</font></th>
        <th style="text-align:center"><font size=4>CANTIDAD DE OT</font></th>
        <th style="text-align:right"><font size=4>MONTO</font></th>
        </thead>
        <tbody>';
  $cantidad_ot_tot = $monto_ot_tot = 0;
  foreach ($clientes as $key => $cliente) {
    $cantidad_ot = $monto_ot = 0;
    $factutaPendiente = false;
    $get_ot = FacturaOt::find()->where(['idCliente'=>$cliente->idCliente])->all();

      foreach ($get_ot as $key => $value) {
        $ot_resumen = EncabezadoPrefactura::find()->where(['idCabeza_Factura'=>$value->idCabeza_Factura])->one();
        if ($ot_resumen->estadoFactura=='Pendiente') {
          $cantidad_ot += 1;
          $monto_ot += $ot_resumen->total_a_pagar;
          $cantidad_ot_tot += 1;
          $monto_ot_tot += $ot_resumen->total_a_pagar;
          $factutaPendiente = true;
        }
      }/* fin foreach $get_ot*/

    if ($factutaPendiente==true) {
      $cliente_ot = Ot::find()->where(['idCliente'=>$cliente->idCliente])->one();
      printf('<tr style="color:#231BB8">
      		 		  <td align="center" ><font face="arial" size=2 >%s</font></td>
      	    		<td align="left"><font size=2>%s</font></td>
      	    		<td align="center"><font size=2>%s</font></td>
      	    		<td align="center"><font size=2>%s</font></td>
                <td align="center"><font size=2>%s</font></td>
      	    		<td align="right"><font size=2>%s</font></td>
          		</tr>',
                    $cliente_ot->idOT,
                    $cliente->nombreCompleto,
                    $cliente->celular,
                    $cliente->telefono,
                    $cantidad_ot,
                    number_format($monto_ot,2)
                );
    }
  }/* fin foreach $clientes*/
  echo '<tr>
          <td colspan="4" style="text-align:right"><font size=4>Totales:</font></td>
          <td style="text-align:center"><font size=4><strong>'.$cantidad_ot_tot.' Factura(s) O.T Pendientes</strong></font></td>
          <td style="text-align:right"><font size=4><strong>'.number_format($monto_ot_tot,2).'</strong></font></td>
        </tr>';
  echo "</tbody></table></div>";

 ?>
