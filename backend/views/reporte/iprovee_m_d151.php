<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
</style>
<?php
use backend\models\Compra;
use backend\models\Empresa;
use backend\models\Proveedores;

$empresaimagen = new Empresa();

/*Consultas de años*/
$anoConsultar = $anoSeleccionado;
$anoConsultar_desde = $anoConsultar - 1;
$anoPasado = date('Y', strtotime('-1 year'));
$anoActual = $anoPasado + 1;

$fechaFactura1 = $anoConsultar != '0' ? $anoConsultar_desde.'-10-01' : $anoPasado.'-10-01';
$fechaFactura2 = $anoConsultar != '0' ? $anoConsultar.'-09-30' : $anoActual.'-09-30';

 $sql = "SELECT SUM(subTotal) AS sumD151, idProveedor FROM `tbl_compras`
WHERE idCompra NOT IN (SELECT fact.idCompra FROM tbl_compras fact INNER JOIN
(SELECT idCompra, SUM(monto_movimiento) AS suma_nc FROM `tbl_movi_pagar` WHERE tipo_mov = 'nc' GROUP BY idCompra) fact_not
ON fact.idCompra = fact_not.idCompra
WHERE fact.total = fact_not.suma_nc)
AND fechaDocumento BETWEEN :fechaFactura1 AND :fechaFactura2  GROUP BY idProveedor DESC;";

  $command = \Yii::$app->db->createCommand($sql);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $compras = $command->queryAll();

/*Fechas en el formato para personas*/
$f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
$f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte D-151 Proveedores</h3>';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
      </span>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th></tr>',
                        'N° Cedula',
                        'Tipo Cedula',
                        'Nombre Proveedor',
                        'Telefonos',
                        'Codigo MIN',
                        'Monto Comprado'
                        );
                echo '</thead>';
echo '<tbody class="">';
$monto_total = 0;
$limiteMonto = Yii::$app->params['limite_monto_compra_MIN']; //Limite impuesto por el MIN en compra de articulos. ¢2,500,000
foreach($compras as $factura) {

    /*Datos Proveedor*/
  $nCedulaProveedor = '';
  $tipoIdentidadProveedor = '';
  $nombre_proveedor = '';
  $proveedorTelefono = '';
  $proveedorTeledono2 = '';
  $codigoMIN = 'C';

  if($provee2r = Proveedores::findOne($factura['idProveedor']))
  {
    /*Verificacion de suma de monto con el limite de venta tributacion*/
    if($factura['sumD151'] >= $limiteMonto){
            $nCedulaProveedor = $provee2r->cedula;
            //$tipoIdentidadProveedor =  $provee2r->tipoIdentidad;
            $tipoIdentidadProveedor = 'jurídica';
            $nombre_proveedor = $provee2r->nombreEmpresa;
            $proveedorTelefono = $provee2r->telefono;
            $proveedorTeledono2 = $provee2r->telefono2;

            $monto_total += $factura['sumD151'];

              printf('<tr style="color:#231BB8">
                  <td style="text-align:right" ><font face="arial" size=1 >%s</font></td>
                  <td style="text-align:center" width="100"><font size=1>%s</font></td>
                  <td style="text-align:right" width="130"><font size=1>%s</font></td>
                  <td style="text-align:right"><font size=1>%s</font></td>
                  <td style="text-align:center" width="80"><font size=1>%s</font></td>
                  <td style="text-align:right" width="150"><font size=1>%s</font></td>
                </tr>',
                      $nCedulaProveedor,
                      $tipoIdentidadProveedor,
                      $nombre_proveedor,
                      $proveedorTelefono.' / '.$proveedorTeledono2,
                      $codigoMIN,
                      number_format($factura['sumD151'] ,2)
                  );
    }//fin if limiteMonto ley Tributaria
  }//fin del if BusquedaClienteRegistrado
}//fin foreach
//$monto_total = $monto_total_credito + $monto_total_contado;

printf('<tr>
          <td style="text-align:center" ><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="50"><font size=3>%s</font></td>
          <td style="text-align:right"><font size=3>%s</font></td>
          <td style="text-align:right" width="150"><font size=3>%s</font></td>
        </tr>',
            '',
            '',
            '',
            '',
            '',
            '<h4>Total: <strong>'.number_format($monto_total,2).'</strong></h4>'
        );
echo '</tbody>';
echo '</table>';
?>
