<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: center;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
</style>
<?php
use backend\models\EncabezadoPrefactura;
use backend\models\Empresa;
use backend\models\Clientes;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

/*$mediopagoproveedor = MedioPagoProveedor::find()->where(['=','cuenta_bancaria_local', $cuenta->idBancos])->andWhere("fecha_documento >= :fechaFactura1 AND fecha_documento <= :fechaFactura2", [':fechaFactura1'=>$fechaFactura1, ':fechaFactura2'=>$fechaFactura2])->orderBy(['idMedio_pago' => SORT_ASC])->all();*/
  $sql = "SELECT * FROM tbl_encabezado_factura WHERE fecha_emision BETWEEN :fechaFactura1 AND :fechaFactura2";
  $command = \Yii::$app->db->createCommand($sql);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $ventas = $command->queryAll();

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.$fecha_desde." <strong>Hasta: </strong>".$fecha_hasta.'</p>
        <p>Emitido: '.Date('d-m-y h:i:s A').'</p>
      </span>';
echo '<br><h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Ventas detalle</h3>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
echo '<thead class="thead-inverse">';
    printf('<tr>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:center"><font face="arial" size=2>%s</font></th>
        <th style="text-align:right"><font face="arial" size=2>%s</font></th></tr>',
            'ID Documento',
            'Fecha',
            'Cliente',
            'Tipo Factura',
            'Estado Factura',
            'Monto'
            );
                echo '</thead>';
echo '<tbody class="">';
$monto_total_contado = $monto_total_credito = 0;
foreach($ventas as $factura) {
  $monto_total_contado += $factura['tipoFacturacion']!='5' ?  $factura['total_a_pagar'] : 0;
  $monto_total_credito += $factura['tipoFacturacion']=='5' ?  $factura['total_a_pagar'] : 0;
  $fecha = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
  $nombre_cliente = '';
  if($cliente = Clientes::findOne($factura['idCliente']))
  {
      $nombre_cliente = $cliente->nombreCompleto;
  }else{
      $nombre_cliente = $factura['idCliente'];
  }
  $tipo_factura = $factura['tipoFacturacion']!='4' ?  'Contado' : 'Crédito';
	//$posFecha = $mediopagoproveedor->posFecha;
  printf('<tr style="color:#231BB8">
  		 		  <td align="center" ><font face="arial" size=2 >%s</font></td>
  	    		<td align="center" width="180" ><font size=2>%s</font></td>
  	    		<td align="center"><font size=2>%s</font></td>
            <td align="center"><font size=2>%s</font></td>
  	    		<td align="center"><font size=2>%s</font></td>
  	    		<td align="right"><font size=2>%s</font></td>
      		</tr>',
                $factura['idCabeza_Factura'],
                $fecha,
                $nombre_cliente,
                $tipo_factura,
                $factura['estadoFactura'],
                number_format($factura['total_a_pagar'],2)
            );
}
$monto_total = $monto_total_credito + $monto_total_contado;

printf('<tr>
<td colspan="3" style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
</tr>',
'<h4></h4>',
'<h4>Contado</h4>',
'<h4>Crédito</h4>',
'<h4>Total</h4>'
);
printf('<tr>
<td colspan="3" style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
<td style="text-align:right"><font size=3>%s</font></td>
</tr>',
'<strong>Totales:</strong>',
'<strong>'.number_format($monto_total_contado,2).'</strong>',
'<strong>'.number_format($monto_total_credito,2).'</strong>',
'<strong>'.number_format($monto_total,2).'</strong>'
);
echo '</tbody>';
echo '</table>';
?>
