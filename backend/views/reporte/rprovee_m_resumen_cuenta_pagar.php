<?php
use backend\models\Proveedores;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\models\ComprasInventario;

$empresaimagen = new Empresa();
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>EMITIDO EL: '.date('d-m-Y / h:i:s A').'</p>
      </span>';
      $proveedores = Proveedores::find()->all();
      if ($lista_filtro_proveedores != '0') {
        $proveedores = Proveedores::find()->where("codProveedores IN (".$lista_filtro_proveedores.")", [])->all();
      }
      echo '<table class="items table table-striped">
              <thead>';
          printf('<tr>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:left">%s</th>
                   <th style="text-align:right">%s</th>
                  </tr>',
                     'ID PROVEEDOR',
                     'PROVEEDOR',
                     'SALDO ACTUAL'
                     );
          echo '</thead>';
          echo '<tbody>';
          $suma_total_saldo[] = null;

          foreach ($proveedores as $key => $proveedor) {
            $credi_saldo = 0;
            $facturas_compras = ComprasInventario::find()->where(['=','idProveedor', $proveedor->codProveedores])->andWhere("estado_pago IN ('Pendiente','Proceso')", [])->all();
            foreach ($facturas_compras as $key => $value) { $credi_saldo += $value->credi_saldo; }
            if ($credi_saldo > 0.00) {
            $monedas = Moneda::find()->all();
            $moneda_pro = Moneda::findOne($proveedor->idMoneda);
            $suma_total_saldo[] = null;
            foreach ($monedas as $r => $moneda) {
              if ($moneda->idTipo_moneda == $moneda_pro->idTipo_moneda) {
                $suma_total_saldo[$r] += $credi_saldo;
              }
            }
            printf('<tr>
                      <td align="left" ><font size=2>%s</font></td>
                      <td align="left" ><font size=2>%s</font></td>
                      <td align="right"><font size=2>%s</font></td>
                    </tr>',
                        $proveedor->codProveedores,
                        $proveedor->nombreEmpresa,
                        $moneda_pro->simbolo.' '.number_format($credi_saldo,2)
                      );
          }
          }
          $resultado = '';
          $monedas = Moneda::find()->all();
          foreach ($monedas as $r => $moneda) {
            $resultado .= $moneda->simbolo.number_format($suma_total_saldo[$r],2).' / ';
          }
          echo '</tbody>';
          echo '<tfoot>
                  <tr>
                    <th></th>
                    <th style="text-align:right"><font size=4>Saldo Total (por moneda):</font></th>
                    <th style="text-align:right"><font size=4>'.$resultado.'</font></th>
                  </tr>
                </tfoot> ';
      echo '</table>';

 ?>
