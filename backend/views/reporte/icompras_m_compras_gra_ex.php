<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}

h3{
  color: black;
}
</style>
<?php
use backend\models\Compra;
use backend\models\DetalleCompra;
use backend\models\Empresa;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

  $sql = "SELECT * FROM tbl_compras WHERE formaPago IN ('Crédito','Contado') AND estado = 'Aplicado' AND fechaRegistro BETWEEN :fechaFactura1 AND :fechaFactura2";

  $command = \Yii::$app->db->createCommand($sql);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $compras = $command->queryAll();

  $cont_subto = $cont_descu = $cont_impue = $cont_total = 0;
  $cred_subto = $cred_descu = $cred_impue = $cred_total = 0;
  $compra_ext = $compra_gra = 0;

  foreach($compras as $factura) {
    $cont_subto += $factura['formaPago']=='Contado' ?  $factura['subTotal'] : 0;
    $cred_subto += $factura['formaPago']=='Crédito' ?  $factura['subTotal'] : 0;
    $cont_descu += $factura['formaPago']=='Contado' ?  $factura['descuento'] : 0;
    $cred_descu += $factura['formaPago']=='Crédito' ?  $factura['descuento'] : 0;
    $cont_impue += $factura['formaPago']=='Contado' ?  $factura['impuesto'] : 0;
    $cred_impue += $factura['formaPago']=='Crédito' ?  $factura['impuesto'] : 0;
    $cont_total += $factura['formaPago']=='Contado' ?  $factura['total'] : 0;
    $cred_total += $factura['formaPago']=='Crédito' ?  $factura['total'] : 0;
    $detalle_compra = DetalleCompra::find()->where(['idCompra'=>$factura['idCompra']])->andWhere('estado = "Aplicado"',[])->all();
    foreach ($detalle_compra as $key => $value) {
      if ($value->exImpuesto == 'Si') {
        $precioCompra = $value->cantidadEntrada * $value->precioCompra;
        $compra_ext +=  $precioCompra - ($precioCompra * ($value->descuento/100));
      } else {
        $precioCompra = $value->cantidadEntrada * $value->precioCompra;
        $compra_gra +=  $precioCompra - ($precioCompra * ($value->descuento/100));
      }
    }
  }
  $suma_subto = $cont_subto + $cred_subto;
  $suma_descu = $cont_descu + $cred_descu;
  $suma_impue = $cont_impue + $cred_impue;
  $suma_total = $cont_total + $cred_total;
  $imp_vent = $suma_impue;
  $total_vent = $suma_total;
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.$fecha_desde." <strong>Hasta: </strong>".$fecha_hasta.'</p>
      </span>';

echo '<br><h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Reporte de resumen de Compra Gravadas / Exentas</h3>';

echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >
      <thead class="thead-inverse">
        <tr>
          <th></th>
          <th style="text-align:right"><font face="arial" size=2>Sub Total</font></th>
          <th style="text-align:right"><font face="arial" size=2>Descuento</font></th>
          <th style="text-align:right"><font face="arial" size=2>Impuesto</font></th>
          <th style="text-align:right"><font face="arial" size=2>Total</font></th>
        </tr>
      </thead>
      <tbody class="">
        <tr style="color:#231BB8">
          <td><font face="arial" size=3 >CONTADO</font></td>
          <td align="right"><font size=2>'.number_format($cont_subto,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cont_descu,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cont_impue,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cont_total,2).'</font></td>
        </tr>
        <tr style="color:#231BB8">
          <td><font face="arial" size=3 >CRÉDITO</font></td>
          <td align="right"><font size=2>'.number_format($cred_subto,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cred_descu,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cred_impue,2).'</font></td>
          <td align="right"><font size=2>'.number_format($cred_total,2).'</font></td>
        </tr>
      </tbody>
      <tfoot>
        <tr style="color:#231BB8">
          <td><font face="arial" size=2 ></font></td>
          <td align="right"><font size=3>'.number_format($suma_subto,2).'</font></td>
          <td align="right"><font size=3>'.number_format($suma_descu,2).'</font></td>
          <td align="right"><font size=3>'.number_format($suma_impue,2).'</font></td>
          <td align="right"><font size=3>'.number_format($suma_total,2).'</font></td>
        </tr>
        <tr style="color:#231BB8">
          <td></td>
          <td align="right"><font size=3></font></td>
          <td align="right"><font size=3></font></td>
          <td align="right"><font size=3></font></td>
          <td align="right">
            <font size=4>
              Total Compras Exentas:<br>
              Total Compras Gravadas:<br>
              Impuesto de Compras:<br>
              Total Compras:
            </font>
          </td>
          <td align="right"><font size=4>'.number_format($compra_ext,2).'<br>'.number_format($compra_gra,2).'<br>'.number_format($imp_vent,2).'<br>'.number_format($total_vent,2).'</font></td>
        </tr>
      </tfoot>
      </table>';
?>