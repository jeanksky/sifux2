<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
th {
    background-color: #4CAF50;
    color: black;
}
h3{
	color: black;
</style>
<?php
use backend\models\Funcionario;
use backend\models\Empresa;
use backend\models\MovimientoCobrar;

$empresaimagen = new Empresa();
echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO DESDE: '.$fecha_desde.' HASTA: '.$fecha_hasta.'</p>
      </span>';
      $fechaInicio1 = $fecha_desde != '' ? date('Y-m-d 00:00:00', strtotime( $fecha_desde )) : '1890-01-01 00:00:00';
      $fechaInicio2 = $fecha_hasta != '' ? date('Y-m-d 23:59:59', strtotime( $fecha_hasta )) : '3890-01-01 23:59:59';
      $funcionarios = Funcionario::find()->all();
      if ($lista_filtro_funcionarios != '0') {
        $funcionarios = Funcionario::find()->where("idFuncionario IN (".$lista_filtro_funcionarios.")", [])->all();
      }
  echo '<h3 style="text-align:center; color: #891313; font-family: Century Gothic, sans-serif;">Resumen de notas de crédito por Funcionario</h3>';
echo '<table class="items table table-striped">
        <tbody>
        <th style="text-align:left"><font size=2>ID.FUNCIO</font></th>
        <th style="text-align:center" colspan="2"><font size=2>NOMBRE COMPLETO</font></th>
        <th style="text-align:center"><font size=2>CANT. NOT.CREDIT</font></th>
        <th style="text-align:right"><font size=2>MONTO POR NOT.CREDIT</font></th>
        </tbody>
        <tbody>';
        $total_cantidad = $total_movimiento = 0;
        foreach ($funcionarios as $key => $funcionario) {
          $movimientos = MovimientoCobrar::find()->where(['usuario'=>$funcionario->username])
          ->andWhere("tipmov = 'NC' AND fecmov BETWEEN :fechaInicio1 AND :fechaInicio2", [':fechaInicio1'=>$fechaInicio1, ':fechaInicio2'=>$fechaInicio2])->all();
          if (@$movimientos) {
            $monto_movimiento = 0; $cantidad = 0;
            foreach ($movimientos as $turn => $movimiento) {
              $monto_movimiento += $movimiento->monto_movimiento;
              $cantidad +=1;
              $total_cantidad +=1;
              $total_movimiento += $movimiento->monto_movimiento;
            }
            printf('<tr>
                       <td style="text-align:left" bgcolor="#7FFFD4"><font size=3>%s</font></td>
                       <td style="text-align:center" bgcolor="#7FFFD4" colspan="2"><font size=5>%s</font></td>
                       <td style="text-align:center" bgcolor="#7FFFD4"><font size=3>%s</font></td>
                       <td style="text-align:right" bgcolor="#7FFFD4"><font size=3>%s</font></td>
                    <tr>',
                       'US: '.$funcionario->idFuncionario,
                       $funcionario->nombre.' '.$funcionario->apellido1.' '.$funcionario->apellido2,
                       $cantidad.' Not.Cred',
                       number_format($monto_movimiento,2)
                       );
         echo '<tr>
                  <th style="text-align:center">ID</th>
                  <th style="text-align:left">APLIC.FACTURA</th>
                  <th style="text-align:center">CLIENTE</th>
                  <th style="text-align:right">MONTO</th>
               </tr>';
          foreach ($movimientos as $turn => $movimiento) {
          echo '<tr>
                   <td style="text-align:center"><font size=1>'.$movimiento->idMov.'</font></td>
                   <td style="text-align:left"><font size=1>'.$movimiento->idCabeza_Factura.'</font></td>
                   <td style="text-align:left"><font size=1>'.$movimiento->idCliente.'</font></td>
                   <td style="text-align:right"><font size=1>'.number_format($movimiento->monto_movimiento,2).'</font></td>
                </tr>';
           }

          }
        }
echo '    <tr>
           <td style="text-align:center" colspan="2"></td>
           <td style="text-align:right"><font size=3><strong>TOTAL:</strong></font></td>
           <td style="text-align:center"><font size=3><strong>'.$total_cantidad.' Not.Creds</strong></font></td>
           <td style="text-align:right"><font size=3><strong>'.number_format($total_movimiento,2).'</strong></font></td>
          </tr>
        <tbody>
      </table>';
