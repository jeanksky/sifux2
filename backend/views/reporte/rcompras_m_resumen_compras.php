<?php
use backend\models\Empresa;
use backend\models\Comrpas;
use backend\models\Proveedores;

$empresaimagen = new Empresa();
$fechaFactura1 = $fecha_desde != '' ? date('Y-m-d', strtotime( $fecha_desde )) : '1890-01-01';
$fechaFactura2 = $fecha_hasta != '' ? date('Y-m-d', strtotime( $fecha_hasta )) : '3890-01-01';

 //$sql = "SELECT * FROM `tbl_compras` WHERE `estado` = 'Aplicado' AND `fechaRegistro` BETWEEN :fechaFactura1 AND :fechaFactura2";

$sql = "SELECT SUM(`total`) AS sumCompras, `idProveedor`, `formaPago` FROM `tbl_compras` WHERE `fechaRegistro` BETWEEN :fechaFactura1 AND :fechaFactura2 GROUP BY `idProveedor`,`formaPago` ORDER BY `formaPago`,`idProveedor`";
  $command = \Yii::$app->db->createCommand($sql);
  $command->bindParam(":fechaFactura1", $fechaFactura1);
  $command->bindParam(":fechaFactura2", $fechaFactura2);
  $compras = $command->queryAll();


$f1 = date('d-m-Y', strtotime( $fechaFactura1 ));
$f2 = date('d-m-Y', strtotime( $fechaFactura2 ));

echo '<img src="'.$empresaimagen->getImageurl('html') .'" style="height: 100px;">';
echo '<span style="float:right">
        <p>RANGO: <strong>Desde: </strong>'.$f1." <strong>Hasta: </strong>".$f2.'</p>
      </span>';

 //Variables para almacenar los datos de totales
$comprasContado = 0;
$comprasCredito = 0;
$totalCompras = 0;
$nombreProveedor = '';

echo '<table class="items table table-striped" id="tabla_facturas_Contado"  >';
echo '<thead>
   <tr><th colspan="8" style="text-align:center"><h3><strong>Compras Contado</strong></h3></th></tr>';
   printf('<tr>
                 <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                 <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                 <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                 </tr>',
                     'ID Proveedor',
                     'Nombre Proveedor',
                     'Monto'
                     );
   echo '</thead>';
echo '<tbody class="">';
	foreach ($compras as $key => $CompraContado) {
		$proveedor = Proveedores::findOne($CompraContado['idProveedor']);
		$nombreProveedor = $proveedor->nombreEmpresa;

	 if ($CompraContado['formaPago'] == 'Contado') {
	 	$comprasContado += $CompraContado['sumCompras'];


			printf('<tr style="color:#231BB8">
                    <td align="center"  width="20"><font size=2>%s</font></td>
				    <td align="center" width="40"><font size=2>%s</font></td>
				    <td align="right" width="40"><font size=2>%s</font></td>
                </tr>',
                      $CompraContado['idProveedor'],
                      $nombreProveedor,
                      number_format($CompraContado['sumCompras'] ,2)
                  );


		 }//fin del if que es de formaPago es Contado
	}//fin foreach Contado
  echo '</tbody>';
  echo '</table>';
echo '<table class="items table table-striped" id="tabla_facturas_Credito"  >';
		echo '<thead>
          <tr><th colspan="8" style="text-align:center"><h3><strong>Compras Crédito</strong></h3></th></tr>';
          printf('<tr>
		                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
		                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
		                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
		                    </tr>',
		                        'ID Proveedor',
		                        'Nombre Proveedor',
		                        'Monto'
		                        );
          echo '</thead>';
		echo '<tbody class="">';
				foreach ($compras as $key => $CompraCredito) {
				$proveedor = Proveedores::findOne($CompraCredito['idProveedor']);
				$nombreProveedor = $proveedor->nombreEmpresa;
				if ($CompraCredito['formaPago'] == 'Crédito'){

				$comprasCredito += $CompraCredito['sumCompras'];

								printf('<tr style="color:#231BB8">
				                  <td align="center"  width="20"><font size=2>%s</font></td>
				                  <td align="center" width="40"><font size=2>%s</font></td>
				                  <td align="right" width="40"><font size=2>%s</font></td>
				                </tr>',
				                      $CompraCredito['idProveedor'],
				                      $nombreProveedor,
				                      number_format($CompraCredito['sumCompras'] ,2)
				                  );
						}//fin if
					}//fin foreach CompraCredito


					printf('<tr>
          				<td align="center"  width="20"><font size=2>%s</font></td>
				        <td align="center" width="40"><font size=2>%s</font></td>
				        <td style="text-align:right" width="40"><font size=3>%s</font></td>
        </tr>',
            '',
            '<h4>Total Compra Contado: <strong>'.number_format($comprasContado,2).'</strong></h4>',
            '<h4>Total Compra Credito: <strong>'.number_format($comprasCredito,2).'</strong></h4>'
        );
		echo '</tbody>';
	echo '</table>';
?>
