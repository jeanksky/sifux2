<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock;
use yii\helpers\ArrayHelper;
use backend\models\Familia;
use backend\models\ProductoServicios;
//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FamiliaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categorias';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<div class="familia-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear Categoria', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<div class="table-responsive">
    <div id="semitransparente">
         <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <?php Pjax::begin(['id' => 'pjax-gridview']) ?>
    <!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'codFamilia','options'=>['style'=>'width:40px']],
            'descripcion',
            [
              'attribute' => 'tipo',
              'options'=>['style'=>'width:150px'],
              'filter'=>Html::activeDropDownList($searchModel, 'tipo',
                ArrayHelper::map(Familia::find()
                ->asArray()
                ->all(),'tipo','tipo'),['class' => 'form-control','prompt' => 'Todos'])
            ],

            [
              'attribute' => 'estado',
              'options'=>['style'=>'width:150px'],
              'filter'=> Html::activeDropDownList($searchModel, 'estado',
               ['1'=>'Activos',
                '0'=>'Inactivos'],
               ['class' => 'form-control','prompt' => 'Todos'])
            ],
            // [
            // 'attribute' => 'estado',
            // 'options'=>['style'=>'width:150px'],
            // 'filter'=>Html::activeDropDownList($searchModel, 'estado',
            //     ArrayHelper::map(Familia::find()
            //     ->orderBy('estado ASC')
            //     ->asArray()
            //     ->all(),'estado','estado'),['class' => 'form-control','prompt' => 'Todos'])
            // ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:80px'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                                'delete' => function ($url, $model, $key) {

                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                        'id' => 'activity-index-link-delete',
                                        'class' => 'activity-delete-link',
                                        'title' => Yii::t('app', 'Eliminar Familia '.$model->descripcion),
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modalDelete',
                                        'onclick' => 'javascript:enviaResultado('.$model->codFamilia.')',
                                        'href' => Url::to('#'),
                                        'data-pjax' => '0',
                                        ]);
                                },
                             ]
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar una Familia necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "codFamilia" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Familia', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar esta Familia?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
</div>
