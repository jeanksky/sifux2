<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $model backend\models\Familia */

$this->title = $model->descripcion;
$this->params['breadcrumbs'][] = ['label' => 'Familias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<div class="col-lg-6">
<div class="familia-view">

    <h3><?= Html::encode($this->title) ?></h3>
<br>
    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codFamilia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['#'], [
            'id' => 'activity-index-link-delete',
            'class' => 'btn btn-danger',
            'data-toggle' => 'modal',
            'data-target' => '#modalDelete',
            'onclick' => 'javascript:enviaResultado('.$model->codFamilia.')',
            'href' => Url::to('#'),
            'data-pjax' => '0',
        ]) ?>
    </p>
<div id="semitransparente">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            'codFamilia',
            'descripcion',
            'tipo',
            'estado',

        ],
    ]) ?>
</div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar una Categoria necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "codProdServicio" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Familia', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar esta Categoria?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
</div>
