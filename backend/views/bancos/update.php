<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
/* @var $this yii\web\View */
/* @var $model backend\models\Bancos */

$this->title = 'Actualizar Banco: ' . ' ' . $model->nombreBanco;
$this->params['breadcrumbs'][] = ['label' => 'Bancos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idBanco, 'url' => ['view', 'id' => $model->idBanco]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="bancos-update">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>
<div class="col-lg-12"> 
    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
