
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Bancos */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="bancos-form"><div class="col-lg-6"> 
<div class="panel panel-default">
            <div class="panel-heading">
                Ingrese los datos que se le solicitan en el siguiente formulario.
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreBanco')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> </div></div> </div>
