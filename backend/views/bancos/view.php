<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Bancos */

$this->title = $model->nombreBanco;
$this->params['breadcrumbs'][] = ['label' => 'Bancos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-6"> 
<div class="bancos-view">

    <h1><?= Html::encode($this->title) ?></h1>
<br>
    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idBanco], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idBanco], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este banco?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'idBanco',
            'nombreBanco',
        ],
    ]) ?>

</div></div>
