<div class="col-lg-6">
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BancosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bancos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bancos-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear Bancos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php  $dataProvider->pagination->pageSize = 10; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'idBanco',
            'nombreBanco',

            ['class' => 'yii\grid\ActionColumn',
            'options'=>['style'=>'width:70px'],
        ], ],
    ]); ?>

</div></div>
