<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Tarjeta */

$this->title = 'Actualizar Tarjeta: ' . ' ' . $model->nombreTarjeta;
$this->params['breadcrumbs'][] = ['label' => 'Tarjetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTarjeta, 'url' => ['view', 'id' => $model->idTarjeta]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="tarjeta-update">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>
<div class="col-lg-12">
    <h3><?= Html::encode($this->title) ?></h3>
      <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p></div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
