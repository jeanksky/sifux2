<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Tarjeta */

$this->title = $model->nombreTarjeta;
$this->params['breadcrumbs'][] = ['label' => 'Tarjetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6">
<div class="tarjeta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <br>
    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actulizar', ['update', 'id' => $model->idTarjeta], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idTarjeta], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar esta tarjeta?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

       <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            // 'idTarjeta',
            ['attribute'=>'nombreTarjeta',  'valueColOptions'=>['style'=>'width:40%'] ],
        ],
    ]) ?>

</div></div>
