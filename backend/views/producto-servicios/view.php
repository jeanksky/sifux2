<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario
use backend\models\HistorialProducto;
use backend\models\Funcionario;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\ProductoServicios */

$this->title = $model->nombreProductoServicio;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$load = \Yii::$app->request->BaseUrl.'/img/load.gif';
//$historial_producto = new HistorialProducto();

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
    //busqueda de historial_producto
    function buscar_historial_producto() {
      codPro = '<?= $model->codProdServicio ?>';
      usuario = document.getElementById('usuario').value;
      origen = document.getElementById('origen').value;
      fecha_desde_historial = document.getElementById('fecha_desde_historial').value;
      fecha_hasta_historial = document.getElementById('fecha_hasta_historial').value;
      document.getElementById("tbl_historial_producto").innerHTML='';//tengo que destruir el contenido del div de
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('producto-servicios/historial_producto') ?>" ,
          { 'cod_prod' : codPro,  'usuario' : usuario,
          'origen' : origen, 'fecha_desde' : fecha_desde_historial,
          'fecha_hasta' : fecha_hasta_historial } ,
          function( data ) {
            $('#tbl_historial_producto').html(data);
          });
    }

    function he_pr(id) { $("#he_pr").html('<span class="label label-warning">'+id+'</span>') }
    $(document).on('click', '#Prefactura_id', (function() {
      document.getElementById("documento_prefactura").innerHTML = '<center><img src="<?= $load ?>" style="height: 300px;"></center>';
                    $.get(
                        $(this).data('url'),
                        function (data) {
                            //$('#movimientos').html(data);
                            document.getElementById("documento_prefactura").innerHTML = data;
                            //$('#modalNotacredito').modal();
                        }
                    );
                }));

    function he_fa(id) { $("#he_fa").html('<span class="label label-success">'+id+'</span>') }
    $(document).on('click', '#Factura_id', (function() {
      document.getElementById("documento_factura").innerHTML = '<center><img src="<?= $load ?>" style="height: 300px;"></center>';
                    $.get(
                        $(this).data('url'),
                        function (data) {
                            //$('#movimientos').html(data);
                            document.getElementById("documento_factura").innerHTML = data;
                            //$('#modalNotacredito').modal();
                        }
                    );
                }));

    function he_mo(id) { $("#he_mo").html('<span class="label label-info">'+id+'</span>') }
    $(document).on('click', '#Movimiento_id', (function() {
      document.getElementById("documento_movimiento").innerHTML = '<center><img src="<?= $load ?>" style="height: 300px;"></center>';
                    $.get(
                        $(this).data('url'),
                        function (data) {
                            //$('#movimientos').html(data);
                            document.getElementById("documento_movimiento").innerHTML = data;
                            //$('#modalNotacredito').modal();
                        }
                    );
                }));
</script>
<?php
    Modal::begin([//modal que me muestra la factura seleccionada
            'id' => 'modal_documento_prefactura',
            'size'=>'modal-lg',
            'header' => '<center><h2 class="modal-title"><div id="he_pr"></div></h2></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "
        <div id='documento_prefactura'></div>
        ";
        Modal::end();
    Modal::begin([//modal que me muestra la factura seleccionada
           'id' => 'modal_documento_factura',
           'size'=>'modal-lg',
           'header' => '<center><h2 class="modal-title"><div id="he_fa"></div></h2></center>',
           'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
       ]);
       echo "
       <div id='documento_factura'></div>
       ";
       Modal::end();
    Modal::begin([//modal que me muestra la factura seleccionada
            'id' => 'modal_movimiento',
            'size'=>'modal-lg',
            'header' => '<center><h2 class="modal-title"><div id="he_mo"></div></h2></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "
        <div id='documento_movimiento'></div>
        ";
        Modal::end();
?>
<div class="producto-servicios-view">
<div class="col-lg-12">
    <h3><?= Html::encode($this->title) ?></h3>

    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codProdServicio], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Eliminar', ['#'], [
            'id' => 'activity-index-link-delete',
            'class' => 'btn btn-danger',
            'data-toggle' => 'modal',
            'data-target' => '#modalDelete',
            'onclick' => 'javascript:enviaResultado("'.$model->codProdServicio.'")',
            'href' => Url::to('#'),
            'data-pjax' => '0',
        ]) ?>
    </p>
</div>
<div id="semitransparente" class="col-lg-6">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            'codProdServicio',
            //'tipo',
            //'numFacturaProv',
            ['attribute'=>'nombreProductoServicio',  'valueColOptions'=>['style'=>'width:30%'] ],
            // 'nombreProductoServicio',
            'cantidadInventario',
            'cantidadMinima',
            'codFamilia',

        ],

    ]) ?>
</div><div id="semitransparente" class="col-lg-6">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            ['attribute'=>'precioCompra',  'valueColOptions'=>['style'=>'width:30%'] ],
            // 'precioCompra',
            'porcentUnidad',
            'precioVenta',
            'exlmpuesto',
            'precioVentaImpuesto',
            'anularDescuento',
           // 'precioMinServicio',
        ],
    ]) ?>

 <!-- GENERADOR DE CODIGO DE BARRA -->

                        <center>
                        <label>Código barra</label><br>
                        <div id="showBarcode"></div>
                        </center>

                        <?php
                        if ($model->isNewRecord) {

                        }// FIN DEL IF
                        else
                        {
                             $optionsArray = array(
                        'elementId'=> 'showBarcode', /* div or canvas id*/
                        'value'=> $model->codProdServicio, /* value for EAN 13 be careful to set right values for each barcode type */
                        'type'=>'code128',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/

                        );
                       echo BarcodeGenerator::widget($optionsArray);
                        }//fin del else del BARCODE
                        ?>

    </div>

    <div class="col-lg-12">
      <h2>HISTORIAL DEL PRODUCTO</h2>
      Reconocimiento desde:
    </div>
      <?php
      echo '<div class="col-lg-12 alert alert-info" role="alert">
              <div class="col-lg-3">
              '.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                  //'model' => $model,
                  //'attribute' => 'codigoVendedor',
                  'name' => '',
                  'data' => ArrayHelper::map(Funcionario::find()->all(), 'idFuncionario',
                      function($element) {
                      return $element['idFuncionario'].' - '.$element['nombre'].' '.$element['apellido1'].' '.$element['apellido2'];
                  }),
                  'options' => [
                      'placeholder' => 'USUARIO DEL SISTEMA',
                      'id'=>'usuario',
                      'onchange'=>'javascript:buscar_historial_producto()',
                      'multiple' => false //esto me ayuda a que solo se obtenga uno
                  ],'pluginOptions' => ['initialize'=> true,'allowClear' => true],
              ]).'
              </div>
              <div class="col-lg-3">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
              'name' => '',
              'data' => [ "Compra" => "EN ................... (Entrada de producto por compra)", "Prefactura" => "PR / PR-OT ..... (Prefactura / Prefctura con O.T)", "Venta" => "FA .................... (Factura por venta)", "Movimiento" => "MV ................... (Movimiento de inventario)", "Devolución" => "DV ................... (Devoluciones)"],
              'options' => [
                  'placeholder' => 'ORIGEN',
                  'id'=>'origen',
                  'onchange' => 'javascript:buscar_historial_producto()',
                  'multiple' => false //esto me ayuda a que solo se obtenga uno
              ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
          ]).'</div>
              <div class="col-lg-3">
              '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
              'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DESDE dd-mm-yyyy', 'onchange'=>'javascript:buscar_historial_producto()',
              'id'=>'fecha_desde_historial']]).'
              </div>
              <div class="col-lg-3">
              '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
              'options' => ['class'=>'form-control', 'placeholder'=>'FECHA HASTA dd-mm-yyyy', 'onchange'=>'javascript:buscar_historial_producto()',
              'id'=>'fecha_hasta_historial']]).'
              </div>
            </div>';

      ?>
      <div id="tbl_historial_producto">
        <?= $this->render('historial_producto', [
          'model' => $model,
          'usuario'=>NULL,
          'fecha_desde' => date('Y-m-d 00:00:00'),
          'origen'=>['Compra', 'Prefactura', 'Venta', 'Movimiento', 'Devolución'],
          'fecha_hasta' => date('Y-m-d 23:59:59')
         ]) ?>
      </div>

    </div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un producto necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "codProdServicio" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Familia', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este producto?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
</div>
