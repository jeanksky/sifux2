<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductoServicios */

$this->title = 'Actualizar producto: ' . ' ' . $model->nombreProductoServicio;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombreProductoServicio, 'url' => ['view', 'id' => $model->codProdServicio]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//Funcion para que me permita ingresar solo numeros
function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58))
                return true;
            return false;
        }
//Funcion para poner mascara de dicimales
function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || [',', "'", '.'];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}


function obtenerPrecioVenta() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            iva=13/100;

            //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
            utilidad=Number(caja["porcentu"].value)/100;

            //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
            subtotal=precioc*utilidad;
            //para luego sumarlo al precio de compra
            total=precioc+subtotal;
            caja["preciov"].value=total.toFixed(2);
            caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
        }
function obtenerPrecioVentau() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            iva=13/100;

            //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
            utilidad=Number(caja["porcentu"].value)/100;

            //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
            subtotal=precioc*utilidad;
            //para luego sumarlo al precio de compra
            total=precioc+subtotal;
            caja["preciov"].value=total.toFixed(2);
            caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
        }
function obtenerUtilidadImpu() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            preciov=Number(caja["preciov"].value);
            iva=13/100;
            //el % del margen sale del el precio de ganacia dividido del precio de compra
            utilidad = preciov/precioc;

            //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
            subtotal=utilidad*100;
            //para luego restarle 100 dejandonos el margen de utilidad
            total=subtotal-100;
            caja["porcentu"].value=currency(total.toFixed(2));
            caja["precioventaimpu"].value=(preciov+(preciov * iva)).toFixed(2);
        }
function obtenerVentaUti() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            precioventaimpu=Number(caja["precioventaimpu"].value);
            //iva=13/100;

            preciov = precioventaimpu/1.13;
            //el % del margen sale del el precio de ganacia dividido del precio de compra
            utilidad = preciov/precioc;

            //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
            subtotalporc=utilidad*100;
            //para luego restarle 100 dejandonos el margen de utilidad
            totalporc=subtotalporc-100;
            caja["porcentu"].value=totalporc.toFixed(2);
            caja["preciov"].value=preciov.toFixed(2);
        }

        //esta funcion me permite asignar el proveedor seleccionado al producto
        function asignar_proveedor(){
            codProdServicio = '<?= $model->codProdServicio ?>';
            var codProveedores = document.getElementById('ddl-codProveedores');
            var codigo_proveedor = document.getElementById('codigo_proveedor').value;
            if (codigo_proveedor == '' || codProveedores.value == '') {
                alert('Por favor no deje campos vacios');
            } else {
                $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/asignar_proveedor') ?>",
                    type:"post",
                    data: {	'codProdServicio': codProdServicio, 'codProveedores': codProveedores.value, 'codigo_proveedor' : codigo_proveedor },
                    success: function(data){
                      if (data=='repetido') {
                        document.getElementById("div_alerta_prov").innerHTML = '<found style="color:red;">Proveedor repetido</found>';
                        setTimeout(function() {//aparece la alerta en un milisegundo
                            $("#div_alerta_prov").fadeIn();
                        });
                        setTimeout(function() {//se oculta la alerta luego de 4 segundos
                            $("#div_alerta_prov").fadeOut();
                        },3000);
                      } else {
                        $("#tabla_proveedores").load(location.href+' #tabla_proveedores','');
                        $('#ddl-codProveedores').val('').change();
                        $('#codigo_proveedor').val('');
                        $('#modalnuevoproveedor').hide("slow");
                        $('#modalnuevoproveedor').modal('toggle');
                      }
                    },
                    error: function(msg, status,err){
                     alert('Error, consulte linea 36 (informar a Nelux)');
                    }
                });
            }
        }

        //elimina proveedor del producto
        function elimina_proveedor(id){
            if(confirm("Está seguro de quitar este proveedor")){
                $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/elimina_proveedor') ?>",
                    type:"post",
                    data: { 'id': id },
                    success: function(data){
                      if (data=='asignado') {
                        document.getElementById("div_alerta_delet_prov").innerHTML = '<br><center><found style="color:red;">Disculpe. No se puede descartar este proveedor ya que cuenta con una orden o pre-orden de este producto.</found></center>';
                        setTimeout(function() {//aparece la alerta en un milisegundo
                            $("#div_alerta_delet_prov").fadeIn();
                        });
                        setTimeout(function() {//se oculta la alerta luego de 4 segundos
                            $("#div_alerta_delet_prov").fadeOut();
                        },5000);
                      } else {
                        $("#tabla_proveedores").load(location.href+' #tabla_proveedores','');
                      }
                    },
                    error: function(msg, status,err){
                     alert('No pasa 101');
                    }
                });
            }
        }

        //muestra la modal con los datos seleccionados del proveedor relacionado
        function actualiza_modal_proveedor(idProd_prov) {
          $.ajax({
              url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/actualiza_modal_proveedor') ?>",
              type:"post",
              dataType: 'json',
              data: { 'idProd_prov': idProd_prov },
              success: function(data){
              	  $('#idProd_prov').val(data.idProd_prov);
                  $('#ddl-codProveedores_').val(data.codProveedores).change();
                  $('#codigo_proveedor_').val(data.codigo_proveedor);
                  //$("#update-monedas").load(location.href+' #update-monedas','');
              },
              error: function(msg, status,err){
               alert('Error, consulte linea 166 (informar a Nelux)');
              }
          });
        }

        //funcion que me actualiza los datos mostrados en la modal luego de la modificacion
        function actualiza_proveedor() {
          var idProd_prov = document.getElementById('idProd_prov').value;
          var codProveedores = document.getElementById('ddl-codProveedores_');
          var codigo_proveedor = document.getElementById('codigo_proveedor_').value;
          if (codigo_proveedor == '' || codProveedores.value == '') {
              alert('Por favor no deje campos vacios');
          } else {
              $.ajax({
                  url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/actualiza_proveedor') ?>",
                  type:"post",
                  data: {	'idProd_prov': idProd_prov, 'codProveedores': codProveedores.value, 'codigo_proveedor': codigo_proveedor },
                  success: function(data){
                    if (data=='repetido') {
                      document.getElementById("div_alerta_prov_u").innerHTML = '<found style="color:red;">Proveedor no actualizable</found>';
                      setTimeout(function() {//aparece la alerta en un milisegundo
                          $("#div_alerta_prov_u").fadeIn();
                      });
                      setTimeout(function() {//se oculta la alerta luego de 4 segundos
                          $("#div_alerta_prov_u").fadeOut();
                      },3000);
                    } else {
                      $("#tabla_proveedores").load(location.href+' #tabla_proveedores','');
                      $('#ddl-codProveedores_').val('').change();
                      $('#codigo_proveedor_').val('');
                      $('#modalActualizaproveedor').hide("slow");
                      $('#modalActualizaproveedor').modal('toggle');
                    }
                  },
                  error: function(msg, status,err){
                   alert('Error, consulte linea 36 (informar a Nelux)');
                  }
              });
          }
        }

        //asigna una marca al producto
        function asignar_marca() {
          codProdServicio = '<?= $model->codProdServicio ?>';
          var id_marcas = document.getElementById('ddl-id_marcas');
          var codigo_marca = document.getElementById('codigo_marca').value;
          if (codigo_marca == '' || id_marcas.value == '') {
              alert('Por favor no deje campos vacios');
          } else {
              $.ajax({
                  url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/asignar_marca') ?>",
                  type:"post",
                  data: {	'codProdServicio': codProdServicio, 'id_marcas': id_marcas.value, 'codigo_marca' : codigo_marca },
                  success: function(data){
                    if (data=='repetido') {
                      document.getElementById("div_alerta_prov").innerHTML = '<found style="color:red;">Proveedor repetido</found>';
                      setTimeout(function() {//aparece la alerta en un milisegundo
                          $("#div_alerta_prov").fadeIn();
                      });
                      setTimeout(function() {//se oculta la alerta luego de 4 segundos
                          $("#div_alerta_prov").fadeOut();
                      },3000);
                    } else {
                      $("#tabla_catalogo").load(location.href+' #tabla_catalogo','');
                      $('#ddl-id_marcas').val('').change();
                      $('#codigo_marca').val('');
                      $('#modalnuevamarca').hide("slow");
                      $('#modalnuevamarca').modal('toggle');
                    }
                  },
                  error: function(msg, status,err){
                   alert('Error, consulte linea 256 (informar a Nelux)');
                  }
              });
          }
        }
        //elimina proveedor del producto
        function elimina_marca(id){
            if(confirm("Está seguro de quitar esta marca")){
                $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/elimina_marca') ?>",
                    type:"post",
                    data: { 'id': id },
                    success: function(data){
                      if (data=='asignado') {
                        document.getElementById("div_alerta_delet_marc").innerHTML = '<br><center><found style="color:red;">Disculpe. No se puede descartar esta marca ya que cuenta con una orden o pre-orden de este producto.</found></center>';
                        setTimeout(function() {//aparece la alerta en un milisegundo
                            $("#div_alerta_delet_marc").fadeIn();
                        });
                        setTimeout(function() {//se oculta la alerta luego de 4 segundos
                            $("#div_alerta_delet_marc").fadeOut();
                        },5000);
                      } else {
                        $("#tabla_catalogo").load(location.href+' #tabla_catalogo','');
                      }
                    },
                    error: function(msg, status,err){
                     alert('No pasa 101');
                    }
                });
            }
        }
      function actualiza_modal_marca(id_Prod_marca) {
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/actualiza_modal_marca') ?>",
            type:"post",
            dataType: 'json',
            data: { 'id_Prod_marca': id_Prod_marca },
            success: function(data){
                $('#id_Prod_marca').val(data.id_Prod_marca);
                $('#ddl-id_marcas_').val(data.id_marcas).change();
                $('#codigo_marca_').val(data.codigo);
                //$("#update-monedas").load(location.href+' #update-monedas','');
            },
            error: function(msg, status,err){
             alert('Error, consulte linea 301 (informar a Nelux)');
            }
        });
      }

      //funcion que me actualiza los datos mostrados en la modal luego de la modificacion
      function actualiza_marca() {
        var id_Prod_marca = document.getElementById('id_Prod_marca').value;
        var id_marcas = document.getElementById('ddl-id_marcas_');
        var codigo = document.getElementById('codigo_marca_').value;
        if (codigo == '' || id_marcas.value == '') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/actualiza_marca') ?>",
                type:"post",
                data: {	'id_Prod_marca': id_Prod_marca, 'id_marcas': id_marcas.value, 'codigo': codigo },
                success: function(data){
                  if (data=='repetido') {
                    document.getElementById("div_alerta_marca_u").innerHTML = '<found style="color:red;">Marca no actualizable</found>';
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#div_alerta_marca_u").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#div_alerta_marca_u").fadeOut();
                    },3000);
                  } else {
                    $("#tabla_catalogo").load(location.href+' #tabla_catalogo','');
                    $('#ddl-id_marcas_').val('').change();
                    $('#codigo_marca_').val('');
                    $('#modal_actualiza_marca').hide("slow");
                    $('#modal_actualiza_marca').modal('toggle');
                  }
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 36 (informar a Nelux)');
                }
            });
        }
      }
</script>
<div class="producto-servicios-update">
  <div class="col-lg-12">
  <?= AlertBlock::widget([
              'type' => AlertBlock::TYPE_GROWL,
              'useSessionFlash' => true
          ]);?>

      <h3><?= Html::encode($this->title) ?></h3>
      <br>
      <p>
          <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
      </p>
  </div>
  <ul class="nav nav-tabs nav-justified">
      <li class="active" role="presentation" ><a href="#productos" data-toggle="tab">Datos del producto</a></li>
      <li role="presentation"><a href="#proveedores" data-toggle="tab">Proveedores asociados</a></li>
      <li role="presentation"><a href="#marcas" data-toggle="tab">Marcas asociadas</a></li>
  </ul>
  <div class="table-responsive tab-content">
    <div class="tab-pane fade in active" id="productos"><br>
      <?= $this->render('_form', [
          'model' => $model,
      ]) ?>
    </div>
    <div class="tab-pane fade" id="proveedores"><br>
      <?= $this->render('proveedores_asociados', [
          'model' => $model,
      ]) ?>
    </div>
    <div class="tab-pane fade" id="marcas"><br>
      <?= $this->render('marcas_asociadas', [
          'model' => $model,
      ]) ?>
    </div>
  </div>
</div>
