<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductoServiciosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventario de productos';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="producto-servicios-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="table-responsive">
<div id="semitransparente">

     <?php  $dataProvider->pagination->pageSize = 10; ?>
     <?php Pjax::begin(['enablePushState'=>FALSE]); ?>
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'codProdServicio',
                'label'=>'Cód',
            ],
            //'tipo',
            [
                'attribute' => 'nombreProductoServicio',
                'label'=>'Producto',
            ],
            [
                'attribute' => 'cantidadInventario',
                'label'=>'Cant Invent',
            ],
            [
                'attribute' => 'cantidadMinima',
                'label'=>'Cantidad Mínima',
            ],
            /*[
                'attribute' => 'precioCompra',
                'label'=>'Pre comp',
            ],*/
            //'porcentUnidad',
            [
                'attribute' => 'precioVenta',
                'label'=>'Pre vent',
            ],
            /*[
                'attribute' => 'exlmpuesto',
                'label'=>'Ext imp',
            ],*/
            [
                'attribute' => 'precioVentaImpuesto',
                'label'=>'Pr vt+imp',
            ],
            /*[
                'attribute' => 'anularDescuento',
                'label'=>'Anul desc',
            ],*/
            // 'precioMinServicio',


        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
 </div>

</div>
