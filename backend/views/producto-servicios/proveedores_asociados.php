<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use backend\models\ProductoProveedores;
use backend\models\Proveedores;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

?>
<div class="col-lg-12">
	<div class="panel panel-info">
		<div class="panel-heading">
			Agregue los proveedores que ofrecen este producto. <br>
			ATENCIÓN! si ya ingresó un proveedor no lo puede actualizar, solo el código correspondiente. Si necesita asignar tal código a otro proveedor entonces vuelva a asignar un proveedor nuevo (diferente) con dicho código.<br>
			No puede quitar un proveedor que ya cuente con al menos una orden de compra de este producto.

            <?= '<div class="pull-right">' .Html::a('', '#', ['class' => 'fa fa-plus fa-2x', 'data-toggle' => 'modal', 'onclick' => 'javascript: $.fn.modal.Constructor.prototype.enforceFocus = function() {};', 'data-target' => '#modalnuevoproveedor', 'data-placement' => 'left', 'title' => Yii::t('app', 'Agregar nuevo proveedor')]) . '</div>'; ?>
						<div id="div_alerta_delet_prov">

						</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
      <div style="height: 280px;width: 100%; overflow-y: auto; ">
        <div id="tabla_proveedores" class="pagos-form">
        <?php
					$proveedores_p = ProductoProveedores::find()->where([ 'codProdServicio'=>$model->codProdServicio ])->orderBy(['idProd_prov' => SORT_DESC])->all();
					echo '<table class="items table table-striped">';
					if($proveedores_p) {
					echo '<tbody>';
						foreach($proveedores_p as $position => $proveedor_p) {
							$proveedor = Proveedores::find()->where(['codProveedores'=>$proveedor_p['codProveedores']])->one();
							$nombreEmpresa = @$proveedor ? $proveedor->nombreEmpresa : $proveedor_p['codProveedores'].' **Este proveedor fue eliminado**';
							$eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-2x', 'onclick'=>'javascript:elimina_proveedor('.$proveedor_p['idProd_prov'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Descartar este proveedor') ]);
							$actualizar = Html::a('', ['', 'id' => $proveedor_p['idProd_prov']], [
															'class'=>'fa fa-pencil fa-2x',
															'id' => 'activity-index-link-agregacredpen',
															'data-toggle' => 'modal',
															'onclick'=>'javascript:actualiza_modal_proveedor('.$proveedor_p['idProd_prov'].')',
															'data-target' => '#modalActualizaproveedor',
															//'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
															'data-pjax' => '0',
															'title' => Yii::t('app', 'Actualizar datos') ]);
							$accion = $actualizar.' '.$eliminar;
							printf('<tr><td><font face="arial" size=4>%s</font></td>
											<td><font face="arial" size=4>%s</font></td>
											<td class="actions button-column">%s</td></tr>',
											'<strong>PROVEEDOR:</strong> '.$nombreEmpresa,
											'<strong>CÓDIGO:</strong> '.$proveedor_p['codigo_proveedor'],
											$accion
									);
						}
					echo '</tbody>';
					}
					echo '</table>';
				?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
    //Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevoproveedor',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Asignar nuevo proveedor</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '#', [
                    'class'=>'btn btn-success',
                    'onclick'=>'asignar_proveedor()',
                    'title'=>'Agregar nuevo proveedor'
                ]),
            ]);
            echo '<div class="well">';
            echo '<label>Proveedor: </label>';
						echo Select2::widget([
														'name' => '',
														'data' => ArrayHelper::map(Proveedores::find()->all(), 'codProveedores',
																function($element) {
																 return $element['codProveedores'].' - '.$element['nombreEmpresa'];
														}),
														'options' => [
																'id'=>'ddl-codProveedores',
																'placeholder'=>'- Seleccione -',
																//'onchange'=>'javascript:',
																//'multiple' => false //esto me ayuda a que solo se obtenga uno
														],
														'pluginOptions' => ['initialize'=> true,'allowClear' => true]
												]);
						echo "<div id='div_alerta_prov'></div>";
            echo '<br><label>Código del producto (PROVEEDOR): </label>';
            echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'codigo_proveedor', 'placeholder' =>' Código', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
						echo '</div>';

            Modal::end();
    ?>

		<?php
    //Muestra una modal para actualizar los datos seleccionados de la relacion con el proveedor
         Modal::begin([
                'id' => 'modalActualizaproveedor',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Actualizar proveedor</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-refresh"></i> Actualizar', '#', [
                    'class'=>'btn btn-success',
                    'onclick'=>'actualiza_proveedor()',
                    'title'=>'Agregar nuevo proveedor'
                ]),
            ]);
            echo '<div class="well">';
						echo' <input type="hidden" name="idProd_prov" id="idProd_prov">';
            echo '<label>Proveedor: </label>';
						echo Select2::widget([
														'name' => '',
														'data' => ArrayHelper::map(Proveedores::find()->all(), 'codProveedores',
																function($element) {
																 return $element['codProveedores'].' - '.$element['nombreEmpresa'];
														}),
														'options' => [
																'id'=>'ddl-codProveedores_',
																'placeholder'=>'- Seleccione -',
																//'onchange'=>'javascript:',
																//'multiple' => false //esto me ayuda a que solo se obtenga uno
														],
														'pluginOptions' => ['initialize'=> true,'allowClear' => true]
												]);
						echo "<div id='div_alerta_prov_u'></div>";
            echo '<br><label>Código del producto (PROVEEDOR): </label>';
            echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'codigo_proveedor_', 'placeholder' =>' Código', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '</div>';

            Modal::end();
    ?>
