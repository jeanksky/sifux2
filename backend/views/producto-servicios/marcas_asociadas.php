<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use backend\models\ProductoCatalogo;
use backend\models\MarcasProducto;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
?>
<div class="col-lg-12">
	<div class="panel panel-info">
		<div class="panel-heading">
			Agregue las marcas que ofrecen este producto.

            <?= '<div class="pull-right">' .Html::a('', '#', ['class' => 'fa fa-plus fa-2x', 'data-toggle' => 'modal', 'onclick' => 'javascript: $.fn.modal.Constructor.prototype.enforceFocus = function() {};', 'data-target' => '#modalnuevamarca', 'data-placement' => 'left', 'title' => Yii::t('app', 'Agregar nuevo proveedor')]) . '</div>'; ?>
						<div id="div_alerta_delet_marc"></div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
      <div style="height: 280px;width: 100%; overflow-y: auto; ">
        <div id="tabla_catalogo" class="pagos-form">
        <?php
					$catalogos = ProductoCatalogo::find()->where([ 'codProdServicio'=>$model->codProdServicio ])->orderBy(['id_marcas' => SORT_DESC])->all();

					if($catalogos) {

						foreach($catalogos as $position => $catalogo) {
							$marca = MarcasProducto::find()->where(['id_marcas'=>$catalogo['id_marcas']])->one();
							$nombre_marca = @$marca ? $marca->nombre_marca : $catalogo['id_marcas'].' **Esta marca fue eliminada**';
							$eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-2x', 'onclick'=>'javascript:elimina_marca('.$catalogo['id_Prod_marca'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Descartar esta marca') ]);
							$actualizar = Html::a('', ['', 'id' => $catalogo['id_Prod_marca']], [
															'class'=>'fa fa-pencil fa-2x',
															'id' => 'activity-index-link-agregacredpen',
															'data-toggle' => 'modal',
															'onclick'=>'javascript:actualiza_modal_marca('.$catalogo['id_Prod_marca'].')',
															'data-target' => '#modal_actualiza_marca',
															//'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
															'data-pjax' => '0',
															'title' => Yii::t('app', 'Actualizar datos') ]);
							$accion = '<div class="pull-right">'.$actualizar.' '.$eliminar.'</div>';
              echo '<div class="col-lg-3"><div class="alert alert-info">'.$accion.'<h4><small>MARCA:</small> '.$nombre_marca.'</h4><h4><small>CÓDIGO:</small> '.$catalogo['codigo'].'</h4></div></div>';
						}

					}

				?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
    //Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevamarca',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Asignar nueva Marca</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '#', [
                    'class'=>'btn btn-success',
                    'onclick'=>'asignar_marca()',
                    'title'=>'Agregar nueva marca'
                ]),
            ]);
            echo '<div class="well">';
            echo '<label>Marca: </label>';
						echo Select2::widget([
														'name' => '',
														'data' => ArrayHelper::map(MarcasProducto::find()->where(['estado'=>'Activo'])->all(), 'id_marcas',
																function($element) {
																 return $element['id_marcas'].' - '.$element['nombre_marca'];
														}),
														'options' => [
																'id'=>'ddl-id_marcas',
																'placeholder'=>'- Seleccione -',
																//'onchange'=>'javascript:',
																//'multiple' => false //esto me ayuda a que solo se obtenga uno
														],
														'pluginOptions' => ['initialize'=> true,'allowClear' => true]
												]);
						echo "<div id='div_alerta_marca'></div>";
            echo '<br><label>Código catálogo (MARCA): </label>';
            echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'codigo_marca', 'placeholder' =>' Código', 'name' => 'codigo_marca', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
						echo '</div>';

            Modal::end();
    ?>
		<?php
		//Muestra una modal para actualizar los datos seleccionados de la relacion con el proveedor
				 Modal::begin([
								'id' => 'modal_actualiza_marca',
								'size'=>'modal-sm',
								'header' => '<center><h4 class="modal-title">Actualizar marca</h4></center>',
								'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-refresh"></i> Actualizar', '#', [
										'class'=>'btn btn-success',
										'onclick'=>'actualiza_marca()',
										'title'=>'actualiza marca'
								]),
						]);
						echo '<div class="well">';
						echo' <input type="hidden" name="id_Prod_marca" id="id_Prod_marca">';
						echo '<label>Proveedor: </label>';
						echo Select2::widget([
														'name' => '',
														'data' => ArrayHelper::map(MarcasProducto::find()->where(['estado'=>'Activo'])->all(), 'id_marcas',
																function($element) {
																 return $element['id_marcas'].' - '.$element['nombre_marca'];
														}),
														'options' => [
																'id'=>'ddl-id_marcas_',
																'placeholder'=>'- Seleccione -',
																//'onchange'=>'javascript:',
																//'multiple' => false //esto me ayuda a que solo se obtenga uno
														],
														'pluginOptions' => ['initialize'=> true,'allowClear' => true]
												]);
						echo "<div id='div_alerta_marca_u'></div>";
						echo '<br><label>Código catálogo (MARCA): </label>';
						echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'codigo_marca_', 'placeholder' =>' Código', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
						echo '</div>';

						Modal::end();
		?>
