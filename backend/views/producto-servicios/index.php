<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock; //para mostrar la alerta
//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario
use yii\widgets\Pjax;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductoServiciosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos - Inventario';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>

<div class="producto-servicios-index">

    <h3 style="text-align: center;"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="table-responsive">
<div id="semitransparente">
   <br>

    <!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?php Pjax::begin(['id' => 'StickerList', 'timeout' => false,'enablePushState'=>FALSE, 'clientOptions' => ['method' => 'GET']]);
    echo '<div class="col-lg-2">
            '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
          </div>
          <div class="col-lg-3">
            '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
          </div>
          <div class="col-lg-2">
            '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
          </div>
          <div class="col-lg-2">
            '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
          </div>
          <div class="col-lg-2">
            '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
          </div>
          <div class="col-lg-1">
            '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
          </div>';
     ?>
      <table class="table table-hover" id="tabla_lista_productos" style="width:100%;">
        <thead>
          <tr>
            <th width="150">CÓDIGO LOCAL</th>
            <th width="450">DESCRIPCIÓN</th>
            <th width="100" style="text-align:center">CANT.INV</th>
            <th width="150" style="text-align:center">COD.ALTE</th>
            <th width="150" style="text-align:center">CÓD.PROV</th>
            <th width="50" style="text-align:center">UBICACIÓN</th>
            <th width="140" style="text-align:right">PRECIO + IV</th>
            <th class="actions button-column" width="60" style="text-align:right">&nbsp;</th>
          </tr>
        </thead>
      </table>
      <div style="height: 400px;width: 100%; overflow-y: auto; ">
        <div id="resultadoBusqueda"><p style="text-align: center;">ESPERANDO BUSQUEDA</p>
        </div>
      </div>
    <?php Pjax::end(); ?>

</div>
 </div>
 <?php //-------------------------------------------------------
 //-------------------------------Pantallas modales-------------
 //------Eliminar
         Modal::begin([
             'id' => 'modalDelete',
             'header' => '<h4 class="modal-title">Para eliminar un producto necesita el permiso del administrador</h4>',
             'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
         ]);

          $form = ActiveForm::begin([
             'method' => 'post',
             'id' => 'delete-pass-form',
             'enableAjaxValidation' => true,
             'enableClientValidation' => false,
         ]);
         ?>
                 <input type="hidden" id="org" name = "codProdServicio" />
                 <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                 <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                 <div class="form-group" style="text-align:center">
                    <p> "Tenga en cuenta que si hay una factura o compra asociada a este producto no se podrá mostrar una ves esté eliminado, aparecería como producto no existente." </p>
                    <?= Html::a('Eliminar Producto', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este producto?',
                            'method' => 'post',
                        ],
                    ]) ?>
                 </div>
         <?php  ActiveForm::end();  ?>
      <?php
         Modal::end();
         //-------------------web/index.php?r=clientes%2Fdelete&id=45
 ?>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
        //me permite buscar por like los productos a BD
        function buscar() {
            var textoBusqueda_cod = $("input#busqueda_cod").val();
            var textoBusqueda_des = $("input#busqueda_des").val();
            var textoBusqueda_fam = $("input#busqueda_fam").val();
            var textoBusqueda_cat = $("input#busqueda_cat").val();
            var textoBusqueda_cpr = $("input#busqueda_cpr").val();
            var textoBusqueda_ubi = $("input#busqueda_ubi").val();
            if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
                $("#resultadoBusqueda").html('<p style="text-align: center;">ESPERANDO BUSQUEDA</p>');

            } else {

                $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/busqueda_producto') ?>",
                    type:"post",
                    data: {
                        'valorBusqueda_cod' : textoBusqueda_cod,
                        'valorBusqueda_des' : textoBusqueda_des,
                        'valorBusqueda_fam' : textoBusqueda_fam,
                        'valorBusqueda_cat' : textoBusqueda_cat,
                        'valorBusqueda_cpr' : textoBusqueda_cpr,
                        'valorBusqueda_ubi' : textoBusqueda_ubi },
                    beforeSend : function() {
                      $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                    },
                    success: function(data){
                        $("#resultadoBusqueda").html(data);
                    },
                    error: function(msg, status,err){
                     //alert('No pasa 286');
                    }
                });
            }
        }
</script>
