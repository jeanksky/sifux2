<?php
use backend\models\HistorialProducto;
use backend\models\Funcionario;
use backend\models\FacturasDia;
use backend\models\FacturaOt;
use yii\helpers\Html;
use yii\helpers\Url;
 ?>
<div class="col-lg-9">
  <div style="height: 400px;width: 100%; overflow-y: auto; ">
  <?php
  echo '<table class="items table table-striped">';
  echo '<thead>';
  printf('<tr><th>%s</th>
          <th>%s</th>
          <th>%s</th>
          <th>%s</th>
          <th>%s</th>
          <th>%s</th>
          <th>%s</th>
          <th>%s</th>
          ',
          'Fecha',
          'Origen',
          'Documento',
          'Cant anterior',
          '',
          'Cant movimiento',
          'Cant nueva',
          'Realizado por'
          );
  echo '</thead>';
  echo '<tbody>';
  if ($usuario == NULL) {
    $historial_producto = HistorialProducto::find()->where(['codProdServicio'=>$model->codProdServicio])->andWhere(['IN', 'origen', $origen])
    ->andWhere('fecha BETWEEN :fecha_desde AND :fecha_hasta', [':fecha_desde' => $fecha_desde, ':fecha_hasta' => $fecha_hasta])->orderBy(['id_historial' => SORT_DESC])->all();
  } else {
    $historial_producto = HistorialProducto::find()->where(['codProdServicio'=>$model->codProdServicio])->andWhere(['IN', 'origen', $origen])
    ->andWhere('usuario = :usuario AND fecha BETWEEN :fecha_desde AND :fecha_hasta', [':usuario' => $usuario, ':fecha_desde' => $fecha_desde, ':fecha_hasta' => $fecha_hasta])->orderBy(['id_historial' => SORT_DESC])->all();
  }
  $total_compra = $total_salidas = $total_devueltas = $total_mov_sum = $total_mov_res = $total_fa = 0.00;
  foreach ($historial_producto as $key => $producto) {
    $color_letra = '#DF0101';
    if ($producto['tipo']=='SUM') {
      $color_letra = '#4B8A08';
    }
    $documento = $producto['id_documento']; $origen = '';
    if ($producto->origen == 'Compra') {
      $origen = 'EN';
      $total_compra += $producto['cant_mov'];
      $documento = Html::a('<i class="glyphicon glyphicon-shopping-cart"></i> ID '.$documento, ['/compras-inventario/update', 'id'=>$documento, 'carga'=>0], [
          'class'=>'btn btn-primary',
          'target'=>'_blank',
          'data-toggle'=>'tooltip',
          'title'=>'Muestra factura de entrada'
          ]);
    } elseif ($producto->origen == 'Prefactura') {
      $prefactura = FacturaOt::find()->where(['idCabeza_Factura'=>$documento])->one();
      $fot = @$prefactura->idCabeza_Factura == $documento ? '-OT' : '';
      $origen = 'PR' . $fot;
      $total_salidas += $producto['tipo'] == 'RES' ? $producto['cant_mov'] : 0;
      $total_devueltas += $producto['tipo'] == 'SUM' ? $producto['cant_mov'] : 0;
      $documento = Html::a('<i class="glyphicon glyphicon-open-file"></i> ID '.$documento, ['#'], [
          'class'=>'btn btn-warning',
          'data-toggle' => 'modal',
          'onClick'=>'he_pr("'.'PREFACTURA ID: '.$documento.$fot.'")',
          'id' => 'Prefactura_id',
          'data-target' => '#modal_documento_prefactura',
          'data-url' => Url::to(['facturas-dia/view', 'id' => $documento]),
          'data-pjax' => '0',
          'title' => Yii::t('app', 'Consulta la Pre-facturas asociada')]);

    } elseif ($producto->origen == 'Venta') {
      $origen = 'FA';
      $total_fa += $producto['cant_mov'];
      $factura = FacturasDia::findOne($documento);
      $documento = Html::a('<i class="glyphicon glyphicon-send"></i> ID '.$documento.' - '.$factura->fe, ['#'], [
          'class'=>'btn btn-success',
          'data-toggle' => 'modal',
          'id' => 'Factura_id',
          'onClick'=>'he_fa("'.'FACTURA: ID: '.$documento.' FE:'.$factura->fe.'")',
          'data-target' => '#modal_documento_factura',
          'data-url' => Url::to(['facturas-dia/view', 'id' => $documento]),
          'data-pjax' => '0',
          'title'=>'Muestra factura de salida'
          ]);
    } elseif ($producto->origen == 'Movimiento') {
      $origen = 'MV';
      $total_mov_sum += $producto['tipo'] == 'SUM' ? $producto['cant_mov'] : 0;
      $total_mov_res += $producto['tipo'] == 'RES' ? $producto['cant_mov'] : 0;
      $documento = Html::a('<i class="glyphicon glyphicon-knight"></i> ID '.$documento, ['#'], [
          'class'=>'btn btn-info',
          'data-toggle' => 'modal',
          'id' => 'Movimiento_id',
          'onClick'=>'he_mo("'.'MOVIMIENTO INVENTARIO: ID: '.$documento.'")',
          'data-target' => '#modal_movimiento',
          'data-url' => Url::to(['movimientos/view', 'id' => $documento]),
          'data-pjax' => '0',
          'title'=>'Muestra movimiento'
      ]);
    } elseif ($producto->origen == 'Devolución') {
      $origen = 'DV';
      $documento = Html::a('<i class="glyphicon glyphicon-log-in"></i> ID '.$documento, ['#'], [
          'class'=>'btn btn-default',
          'data-toggle' => 'modal',
          'id' => 'Movimiento_id',
          'onClick'=>'he_mo("'.'Devolución: ID: '.$documento.'")',
          'data-target' => '#modal_movimiento',
          'data-url' => Url::to(['movimientos/view', 'id' => $documento]),
          'data-pjax' => '0',
          'title'=>'Devolución'
      ]);
    }
    $funcionario = Funcionario::find()->where(['idFuncionario'=>$producto['usuario']])->one();
    printf('<tr style="color:'.$color_letra.';">
              <td><font face="arial" size=2>%s</font></td>
              <td><font face="arial" size=2>%s</font></td>
              <td><font face="arial" size=2>%s</font></td>
              <td align="center"><font face="arial" size=2>%s</font></td>
              <td align="right"><font face="arial" size=2>%s</font></td>
              <td align="center"><font face="arial" size=2>%s</font></td>
              <td align="center"><font face="arial" size=2>%s</font></td>
              <td><font face="arial" size=2>%s</font></td>
            </tr>',
        date('d-m-Y | h:i:s A', strtotime( $producto['fecha'] )),
        $origen,
        $documento,
        $producto['cant_ant'],
        $producto['tipo']=='SUM' ? '( + )' : '( - )',
        $producto['cant_mov'],
        $producto['cant_new'],
        $funcionario->nombre . ' '. $funcionario->apellido1 . ' ' . $funcionario->apellido2
      );
  }
  echo '</tbody>';
  echo '</table>';
   ?>
   </div>
</div>
<div class="well col-lg-3">
  <h3>Compras:<span style="float:right"><?= $total_compra ?></span></h4>
  <h3>PR / (PR-OT) Salidas:<span style="float:right"><?= $total_salidas ?></span></h4>
  <h3>PR / (PR-OT) Devueltas:<span style="float:right"><?= $total_devueltas ?></span></h4>
  <h3>FA:<span style="float:right"><?= $total_fa ?></span></h4>
  <h3>Movimientos ( + ):<span style="float:right"><?= $total_mov_sum ?></span></h4>
  <h3>Movimientos ( - ):<span style="float:right"><?= $total_mov_res ?></span></h4>
</div>
