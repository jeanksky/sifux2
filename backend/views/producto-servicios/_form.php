<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Familia;
use backend\models\UnidadMedidaProducto;
use kartik\widgets\Select2;
use kartik\money\MaskMoney;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
/* @var $this yii\web\View */
/* @var $model backend\models\ProductoServicios */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
function isNumberDe(evt) {
    var nav4 = window.Event ? true : false;
    var key = nav4 ? evt.which : evt.keyCode;
    return (key <= 13 || key==46 || (key >= 48 && key <= 57));
}
//Transformar a mayúsculas todos los caracteres
function Mayuculas(tx){
  //Retornar valor convertido a mayusculas
  return tx.toUpperCase();
}
</script>
<br>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Ingrese los datos que se le solicitan en el siguiente formulario.
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="producto-servicios-form">
                <div class="col-lg-4">

                    <?php $form = ActiveForm::begin(['options' => ['name'=>'caja'],]); ?>

                    <?= $form->field($model, 'tipo')->textInput(['readonly' => true, 'value' => 'Producto']) ?>

                    <?= $form->field($model, 'codProdServicio')->textInput(['onkeyup'=>'javascript:this.value=Mayuculas(this.value)']) ?>

                        <!-- GENERADOR DE CODIGO DE BARRA -->

                        <label>Código barra</label><br>
                        <center>
                        <div id="showBarcode"></div>
                        </center>

                        <?php
                        if ($model->isNewRecord) {
                          $model->exlmpuesto = 'No';
                          $model->anularDescuento = 'No';
                        }// FIN DEL IF
                        else
                        {
                             $optionsArray = array(
                        'elementId'=> 'showBarcode', /* div or canvas id*/
                        'value'=> $model->codProdServicio, /* value for EAN 13 be careful to set right values for each barcode type */
                        'type'=>'code128',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/

                        );
                       echo BarcodeGenerator::widget($optionsArray);
                        }//fin del else del BARCODE
                        ?>

                    <?= $form->field($model, 'nombreProductoServicio')->textarea(['maxlength' => 500]) ?>

                    <?php echo $model->isNewRecord ? '' : '<label>Cantidad en inventario</label><br><h4>'.$model->cantidadInventario.'</h4>'; ?>
        </div>
        <div class="col-lg-4">
               <div class="col-lg-12">
             <?= $form->field($model, 'idUnidadMedida')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($model->getUnidadMedida(),'idUnidadMedida', function($element){
                                        //return $element->numero_cuenta;
                                    return $element['descripcionUnidadMedida'].' / '.$element['simbolo'];
                                    }),
                                'language'=>'es',
                                'options' => ['placeholder' => '- Seleccione -'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]);
                                ?>
        </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'cantidadMinima')->textInput(["onkeypress"=>"return isNumberDe(event)"]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'ubicacion')->textInput() ?>
            </div>
                <!--?= $form->field($model, 'codFamilia')->dropDownList(
                ArrayHelper::map(Familia::find()->all(),'codFamilia','descripcion'),['prompt' => 'Selecccione']
                )?-->
                    <!--?= $form->field($model, 'codFamilia')->dropDownList(
                        ArrayHelper::map($model->getNombreFamilia(),'codFamilia','descripcion'),['prompt' => 'Selecccione']
                        )?-->
            <div class="col-lg-12">
                    <?= $form->field($model, 'codFamilia')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($model->getNombreFamilia(),'codFamilia',function($element) {
                                    return $element['codFamilia'].' - '.$element['descripcion'];
                                }),

                        'language'=>'es',
                        'options' => ['placeholder' => '- Seleccione -'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Categoría');
                        ?>
            </div>
            <div class="col-lg-6">
              <?= $form->field($model, 'cantidad_dias_produccion')->textInput(["onkeypress"=>"return isNumberDe(event)"]) ?>
            </div>
            <div class="col-lg-6">
                    <?= $form->field($model, 'precioCompra')->textInput(['id'=>'precioc', 'onkeyup'=>'javascript:obtenerPrecioVenta()']) ?>
                    <!--?= /*$form->field($model, 'precioCompra')->
                                                     widget(MaskMoney::classname(), [
                                                        'pluginOptions' => [
                                                            'prefix' => '¢',
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ]
                                                    ])*/ ?-->
            </div>
            <div class="col-lg-12">
                    <?= $form->field($model, 'porcentUnidad')->textInput(['id'=>'porcentu', 'onkeyup'=>'javascript:obtenerPrecioVentau()']) ?>
            </div>
        </div>
        <div class="col-lg-4">
                    <?= $form->field($model, 'precioVenta')->textInput(['id'=>'preciov', 'onkeyup'=>'obtenerUtilidadImpu()']) ?>
                    <!--?= /*$form->field($model, 'precioVenta')->
                                                     widget(MaskMoney::classname(), [
                                                        'pluginOptions' => [
                                                            'prefix' => '¢',
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ]
                                                    ])*/ ?-->

                    <?= $form->field($model, 'exlmpuesto')->radioList(['Si'=>'Si','No'=>'No'])?>

                    <?= $form->field($model, 'precioVentaImpuesto')->textInput(['id'=>'precioventaimpu', 'onkeyup'=>'obtenerVentaUti()']) ?>
                    <!--?= /*$form->field($model, 'precioVentaImpuesto')->
                                                     widget(MaskMoney::classname(['name'=>'precioventaimpu']), [
                                                        'pluginOptions' => [
                                                            'prefix' => '¢',
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ],
                                                     ])*/ ?-->
                  <!--?php  echo MaskMoney::widget([
                        'name' => 'precioventaimpu',
                        'pluginOptions' => [
                            'prefix' => '$ ',
                        ],
                    ]); ?-->

                    <?= $form->field($model, 'anularDescuento')->radioList(['Si'=>'Si','No'=>'No'])?>

                    <!--?= $form->field($model, 'precioMinServicio')->textInput() ?-->

                    <div class="form-group" style="text-align:right">
                        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
            <!-- /.panel-body -->
            </div>
        <!-- /.panel -->
        </div>
</div>
