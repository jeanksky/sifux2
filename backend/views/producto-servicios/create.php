<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductoServicios */

$this->title = 'Crear producto';
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//Funcion para que me permita ingresar solo numeros
function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58))
                return true;
            return false;
        }
//Funcion para poner mascara de dicimales
function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || [',', "'", '.'];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

function obtenerPrecioVenta() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            //var precioc = document.getElementById("precioc").value;
            iva=13/100;
            //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
            utilidad=Number(caja["porcentu"].value)/100;
            //var utilidad = $("#porcentu").val();

            //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
            subtotal=precioc*utilidad;
            //para luego sumarlo al precio de compra
            total=precioc+subtotal;
            caja["preciov"].value=total.toFixed(2);
            caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
            //$("#preciov").val() = currency(total.toFixed(2));
            //$("#precioventaimpu").val() = currency((total+(total * iva)).toFixed(2));
        }
function obtenerPrecioVentau() {//
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            iva=13/100;

            //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
            utilidad=Number(caja["porcentu"].value)/100;

            //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
            subtotal=precioc*utilidad;
            //para luego sumarlo al precio de compra
            total=precioc+subtotal;
            caja["preciov"].value=total.toFixed(2);
            caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
        }
function obtenerUtilidadImpu() {//
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            preciov=Number(caja["preciov"].value);
            iva=13/100;
            //el % del margen sale del el precio de ganacia dividido del precio de compra
            utilidad = preciov/precioc;

            //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
            subtotal=utilidad*100;
            //para luego restarle 100 dejandonos el margen de utilidad
            total=subtotal-100;
            caja["porcentu"].value=total.toFixed(2);
            caja["precioventaimpu"].value=(preciov+(preciov * iva)).toFixed(2);
        }
function obtenerVentaUti() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["caja"].elements;
            precioc=Number(caja["precioc"].value);
            precioventaimpu=Number(caja["precioventaimpu"].value);
            //iva=13/100;

            preciov = precioventaimpu/1.13;
            //el % del margen sale del el precio de ganacia dividido del precio de compra
            utilidad = preciov/precioc;

            //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
            subtotalporc=utilidad*100;
            //para luego restarle 100 dejandonos el margen de utilidad
            totalporc=subtotalporc-100;
            caja["porcentu"].value=totalporc.toFixed(2);
            caja["preciov"].value=preciov.toFixed(2);
        }
</script>

<div class="producto-servicios-create">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
