 <div class="col-md-6"> 
<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
/* @var $this yii\web\View */
/* @var $model backend\models\Version */

$this->title = 'Actualizar Versión: ' . ' ' . $model->codVersion;
$this->params['breadcrumbs'][] = ['label' => 'Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codVersion, 'url' => ['view', 'id' => $model->codVersion]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="version-update">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div>
