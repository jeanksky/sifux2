 <div class="col-md-6">
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\VersionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Versiones';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<div class="version-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear Versión', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php  $dataProvider->pagination->pageSize = 10; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codVersion',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
</div>
