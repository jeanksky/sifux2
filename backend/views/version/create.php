 <div class="col-md-6"> 
<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Version */

$this->title = 'Crear Versión';
$this->params['breadcrumbs'][] = ['label' => 'Versiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="version-create">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div>
