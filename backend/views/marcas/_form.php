<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Marcas */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="col-lg-6">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ingrese los datos que se le solicitan en el siguiente formulario.
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<div class="marcas-form">

				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'nombre_marca')->textInput(['maxlength' => 20]) ?>

				<?= $form->field($model, 'slug')->textInput(['maxlength' => 20]) ?>

				<div class="form-group">
					<?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
