<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Marcas */

$this->title = $model->nombre_marca;
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6">
<div class="marcas-view">

    <h1><?= Html::encode($this->title) ?></h1>
<br>
    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codMarca], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codMarca], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres borrar esta marca?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div id="semitransparente">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            'nombre_marca',
            'slug',
        ],
    ]) ?>
</div>
</div></div>
