<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Marcas */

$this->title = 'Actualizar Marca: ' . ' ' . $model->nombre_marca;
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre_marca, 'url' => ['view', 'id' => $model->codMarca]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="marcas-update">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
