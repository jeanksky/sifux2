<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */

$this->title = 'Actualizar cliente: ' . ' ' . $model->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombreCompleto, 'url' => ['view', 'id' => $model->idCliente]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="clientes-update">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h1><?= Html::encode($this->title) ?></h1>

   <!--  <p>
        <//?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p> -->
</div>
    <?= $this->render('_form', [
        'model' => $model,
        'model_ot' => $model_ot,
        'model_cl_apartado' => $model_cl_apartado
    ]) ?>

</div>
