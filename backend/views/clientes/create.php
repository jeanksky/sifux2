<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */

$this->title = 'Crear clientes';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript">


function compruebe_repetido(ident) {
  $.ajax({
    url:"<?= Yii::$app->getUrlManager()->createUrl('clientes/compruebe_repetido') ?>",
    type:"post",
    data: { 'ident' : ident },
    success: function(notif){
      if (notif=='repetido') {
        document.getElementById("compruebe_repetido").innerHTML = '<font size="4">Espera, este cliente ya fue registrado.</font>';
      } else {
        document.getElementById("compruebe_repetido").innerHTML = '';
      }
    },
    error: function(msg, status,err){
     alert('No pasa 30');
    }
  });
}

function compruebe_repetido_mail(email) {
  $.ajax({
    url:"<?= Yii::$app->getUrlManager()->createUrl('clientes/compruebe_repetido_mail') ?>",
    type:"post",
    data: { 'email' : email },
    success: function(notif){
      if (notif=='repetido') {
        document.getElementById("compruebe_repetido").innerHTML = '<font size="4">Espera, este e-mail ya fue registrado. El sistema no permite e-mails repetidos</font>';
      } else {
        document.getElementById("compruebe_repetido").innerHTML = '';
      }
    },
    error: function(msg, status,err){
     alert('No pasa 30');
    }
  });
}
</script>
<div class="clientes-create">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h1><?= Html::encode($this->title) ?></h1>
    <!-- <br>
    <p>
        <//?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p> -->
</div>
    <?= $this->render('_form', [
        'model' => $model,
        'model_ot' => $model_ot,
        'model_cl_apartado' => $model_cl_apartado
    ]) ?>

</div>
