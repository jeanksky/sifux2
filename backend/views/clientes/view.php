<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
//use yii\widgets\DetailView;
//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */

$this->title = $model->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
            function enviaResultado(id) {
                $('#org').val(id);
            }
    </script>
<div class="clientes-view">
<div class="col-lg-12">
    <h1><?= Html::encode($this->title) ?></h1>

    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idCliente], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['#'], [
            'id' => 'activity-index-link-delete',
            'class' => 'btn btn-danger',
            'data-toggle' => 'modal',
            'data-target' => '#modalDelete',
            'onclick' => 'javascript:enviaResultado('.$model->idCliente.')',
            'href' => Url::to('#'),
            'data-pjax' => '0',
        ]) ?>
    </p>
   </div>
<div class="col-lg-6">
    <div id="semitransparente">
        <?= DetailView::widget([
            'model' => $model,
            //'hAlign'=> DetailView::ALIGN_LEFT ,
            'attributes' => [
                'idCliente',
                'tipoIdentidad',
                'identificacion',
                ['attribute'=>'nombreCompleto',  'valueColOptions'=>['style'=>'width:30%'] ],
                ['attribute' => 'genero',
                        'visible' =>$model->tipoIdentidad !== 'Jurídica'],
                ['attribute' => 'fechNacimiento',
                        'visible' =>$model->tipoIdentidad !== 'Jurídica'],
                'telefono',
                'fax',
                'celular',
                'email:email',
            ],
        ]) ?>
    </div>
</div>
<div class="col-lg-6">
    <div id="semitransparente">
        <?= DetailView::widget([
            'model' => $model,
            //'hAlign'=> DetailView::ALIGN_LEFT ,
            'attributes' => [

                ['attribute'=>'direccion',  'valueColOptions'=>['style'=>'width:20%'] ],
                'credito',
                'documentoGarantia',
                'diasCredito',
                'autorizacion',
               // 'montoMaximoCredito',
                [
                'attribute' => 'montoMaximoCredito',
                'format' => ['decimal',2],
                 ],
                 [
                'attribute' => 'montoTotalEjecutado',
                'format' => ['decimal',2],
                 ],
                //'montoTotalEjecutado',
                'estadoCuenta',
            ],
        ]) ?>
    </div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un cliente ocupa una clave administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "idCliente" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Cliente', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este cliente?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
?>
</div>
