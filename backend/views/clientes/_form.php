<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
//use yii\bootstrap\ActiveForm;
use backend\models\Ot;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;
use kartik\money\MaskMoney;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use backend\models\Tribunales;
use backend\models\EncabezadoPrefactura;
use backend\models\MovimientoApartado;
/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
//ancho de celda GridView yii2
//http://www.yiiframework.com/forum/index.php/topic/3760-best-way-to-hideshow-elements-dinamically/
  $tribunales = new Tribunales();
  $cantones = $tribunales->obtener_cantones($model->provincia);
  $distritos = $tribunales->obtener_distritos($model->provincia, $model->canton);
  $barrios = $tribunales->obtener_barrios($model->provincia, $model->canton, $model->distrito);
  $datos_cantones = $datos_distritos = $datos_barrios = null;
  $c = $d = $b = 0;
  foreach ($cantones as $key => $canton) { $c += 1; $datos_cantones[$c] = $canton["nombre_canton"]; }
  foreach ($distritos as $key => $distrito) { $d += 1; $datos_distritos[$d] = $distrito["nombre_distrito"]; }
  foreach ($barrios as $key => $barrio) { $b += 1; $datos_barrios[$b] = $barrio["nombre_barrio"]; }
?>

<script type="text/javascript">
$(function(){
    $('#tipo :radio').change(function(){
    var valor = $(this).val();
    $('#entidad_hidden').val(valor);
    if (valor == 'Jurídica'){
        $('#contenido_a_mostrar').hide("slow");
        $('#contenido_a_mostrar2').hide("slow");
    } else {
      //var net_output = some_variable + 5;
      $('#contenido_a_mostrar').show("slow");
      $('#contenido_a_mostrar2').show("slow");
      // $('#nombre').val("Funciona");
      //alert( "Seleccionó juridica");
      }
    });
    $('#credito :radio').change(function(){
        var valor2 = $(this).val();
        if (valor2 == 'Si')
              {
                $('#panel-body').show("slow");
              }
        else if (valor2 == 'No')
        {
            $('#panel-body').hide("slow");
        }
    });
});

//funcion para buscar el cliente por numero de cedula
function buscar_cliente(id) {
  var entidad = $('#entidad_hidden').val();
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('clientes/buscar_cliente') ?>",
      type:"post",
      dataType: 'json',
      data: { 'id' : id, 'entidad' : entidad },
      beforeSend: function() {
        //$('#genero :radio').removeAttr("checked");
        $('.nombre').val('');
      },
      success: function(data){
        //alert('data.nombreCompleto');
        $('.nombre').val(data.nombreCompleto);
        $('#genero :radio').filter('[value="' + data.genero + '"]').prop('checked', true);
      },
      error: function(msg, status,err){
          //alert('error 32');
      }
  });
}

  function obtener_canton() {
    provincia = $('.provincia').val();
    $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('clientes/obtener_canton') ?>",
      type:"post",
      data: { 'provincia' : provincia },
      beforeSend: function() {
      },
      success: function(valores){
          $('.canton').html(valores).fadeIn();
        }
      });
    }

    function obtener_distrito() {
      provincia = $('.provincia').val();
      canton = $('.canton').val();
      $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('clientes/obtener_distrito') ?>",
        type:"post",
        data: { 'canton' : canton, 'provincia' : provincia },
        beforeSend: function() {
        },
        success: function(valores){
            $('.distrito').html(valores).fadeIn();
          }
        });
    }

    function obtener_barrio() {
      provincia = $('.provincia').val();
      canton = $('.canton').val();
      distrito = $('.distrito').val();
      $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('clientes/obtener_barrio') ?>",
        type:"post",
        data: { 'canton' : canton, 'provincia' : provincia, 'distrito' : distrito },
        beforeSend: function() {
        },
        success: function(valores){
            $('.barrio').html(valores).fadeIn();
          }
        });
    }
</script>

<style media="screen">
.row {
border-bottom: 1px solid #d9edf7;
}
.otrow {
border-bottom: 1px solid #faebcc;
}
</style>

<div class="clientes-form"><br>


<!--Al hace llamado a la función solo tienes que idicar el nombre del DIV entre parentesis -->
<!--p><a style='cursor: pointer;' onclick="muestra_oculta('contenido_a_mostrar')" title="">Mostrar / Ocultar</a></p-->

<!--div id="contenido_a_mostrar">
<p>Este contenido tiene que mostrarse con el link</p>
</div-->



    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'enableClientScript' => true]); ?>
    <!--?php //Para patalla modal______________________
    /* $form = ActiveForm::begin([
    'id' => 'solicitante-form',
    'enableAjaxValidation' => true,
    //'enableClientScript' => true,
   // 'enableClientValidation' => true,
    ]); */ ?-->
    <div class="col-lg-12" style="line-height:3.5;" align="right">
      <?php
        echo Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) .' '. Html::submitButton($model->isNewRecord ? 'Registrar cliente' : 'Actualizar cliente', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
      ?>
    </div>
    <div class="col-lg-7">
    <div class="panel panel-success">
      <div class="panel-heading">
          Datos personales
          <span id="compruebe_repetido" style="float:right" class="label label-warning"></span>
      </div>
      <div class="panel-body">
        <div class="col-lg-4">
          <?php
          if ($model->isNewRecord)
            {
              $model->tipoIdentidad = 'Física';
              $model->iv = 0;
            } ?>
          <!--?=$form->field($model, 'tipoIdentidad')->radioList(['1' => 'Física', '2' => 'Jurídica'], ['item' => function($index, $label, $name, $checked, $value) {$checked = $checked == 1 ? 'checked=""' : 'disabled=""';echo "<label><input tabindex='{$index}' type='radio' {$checked}'name='{$name}'value='{$value}'> {$label}</label>";}]);?-->
          <div id="tipo">
              <?=
              //$model->tipoIdentidad = 'Jurídica';
              $form->field($model, 'tipoIdentidad')->radioList(
              [
              'Física'=>'Física',
              'Jurídica'=>'Jurídica',
              'DIMEX'=>'DIMEX (12)',
              'NITE'=>'NITE (11)'
              ]/*, [
              'onclick'=>"muestra_oculta('contenido_a_mostrar')",
              'item' => function ($index, $label, $name, $checked, $value) {
                  return Html::radio('Física', $checked, ['value' => $value]);
              },
              ]*/)
              ?>
          </div>
          <input type="hidden" id="entidad_hidden" value="Física">
        </div>
        <div class="col-lg-4">
          <?php if ($model->isNewRecord) { ?>
          <?= $form->field($model, 'identificacion')->textInput(['maxlength' => 20, 'onkeyup'=>'javascript:compruebe_repetido(this.value)', 'onchange' => 'javascript:buscar_cliente(this.value)']) ?>
          <?php } else { ?>
          <?= $form->field($model, 'identificacion')->textInput(['readonly'=>false, 'id'=>'identi', 'onkeyup'=>'javascript:compruebe_repetido(this.value)', 'onchange' => 'javascript:buscar_cliente(this.value)']) ?>
          <?php } ?>
        </div>
        <div class="col-lg-4">
          <div id="contenido_a_mostrar">
          <?php
          if ( $model->tipoIdentidad !== 'Jurídica') { ?>
            <div id="genero">
              <?= $form->field($model, 'genero')->radioList(['Masculino'=>'Masculino','Femenino'=>'Femenino']);?>
            </div>
              <?php } ?>
          </div>
        </div>
        <div class="col-lg-7">
          <?= $form->field($model, 'nombreCompleto')->textInput(['class'=>'nombre form-control']) ?>
        </div>
        <div class="col-lg-5">
          <div id="contenido_a_mostrar2">
           <?php
           if ( $model->tipoIdentidad != 'Jurídica') { ?>
           <?= $form->field($model,'fechNacimiento')->
               widget(DatePicker::className(),[
                   'name' => 'check_issue_date',
                    'disabled' => false,
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'autoclose' => true,
                    ]
                ]);?>
           <?php } ?>
          </div>
        </div>
        <div class="col-lg-3">
        <?= $form->field($model, 'iv')->radioList(
                [
                false=>'Gavado',
                true=>'Exento'
              ]) ?>
        </div>
        <div class="col-lg-5">
          <?= $form->field($model, 'email')->
           widget(\yii\widgets\MaskedInput::className(), [
              'name' => 'input-36',
              'options' => ['onkeyup'=>'javascript:compruebe_repetido_mail(this.value)', 'class'=>'form-control', 'aria-describedby'=>"emailHelp"],
              'clientOptions' => [
              'alias' =>  'email'
               ],
          ]) ?>
        </div>
        <div class="col-lg-4">
          <?= $form->field($model, 'celular')->
          widget(\yii\widgets\MaskedInput::className(), [
                  'mask' => '9999-99-99',
          ]) ?>
        </div>
        <div class="col-lg-4">
          <?= $form->field($model, 'telefono')->
          widget(\yii\widgets\MaskedInput::className(), [
                  'mask' => '9999-99-99',
          ]) ?>
        </div>
        <div class="col-lg-4">
          <?= $form->field($model, 'fax')->
          widget(\yii\widgets\MaskedInput::className(), [
                  'mask' => '9999-99-99',
          ]) ?>
        </div>
        <div class="col-lg-3">
          <?= $form->field($model, 'provincia')->widget(Select2::classname(), [
                                          'data' => ['1'=>'San José', '2'=>'Alajuela',
                                                     '3'=>'Cartago', '4'=>'Heredia',
                                                     '5'=>'Guanacaste', '6'=>'Puntarenas', '7'=>'Limón'],
                                          'options'=>['class'=>'provincia', 'placeholder'=>'- Seleccione -', 'onchange'=>'javascript:obtener_canton()'],
                                          'pluginOptions' => ['allowClear' => true]
                                      ]) ?>
        </div>
        <div class="col-lg-4">
          <?= $form->field($model, 'canton')->widget(Select2::classname(), [
                                          'data' => $datos_cantones,
                                          'options'=>['class'=>'canton', 'placeholder'=>'- Seleccione -', 'onchange'=>'javascript:obtener_distrito()'],
                                          'pluginOptions' => ['allowClear' => true]
                                      ]) ?>
        </div>
        <div class="col-lg-5">
          <?= $form->field($model, 'distrito')->widget(Select2::classname(), [
                                          'data' => $datos_distritos,
                                          'options'=>['class'=>'distrito', 'placeholder'=>'- Seleccione -', 'onchange'=>'javascript:obtener_barrio()'],
                                          'pluginOptions' => ['allowClear' => true]
                                      ]) ?>
        </div>
        <div class="col-lg-5">
          <?= $form->field($model, 'barrio')->widget(Select2::classname(), [
                                          'data' => $datos_barrios,
                                          'options'=>['class'=>'barrio', 'placeholder'=>'- Seleccione -'],
                                          'pluginOptions' => ['allowClear' => true]
                                      ]) ?>
        </div>
        <div class="col-lg-7">
          <?= $form->field($model, 'direccion')->textarea(['maxlength' => 130]) ?>
        </div>

      </div>
    </div>
    </div><!-- fin de datos personales -->
    <div class="col-lg-5">
    <div class="panel panel-info">
      <div class="panel-heading">
        Datos de crédito
      </div>
      <?php
      if ($model->isNewRecord)
        {
          $model->credito = 'No';
        } ?>
      <div class="panel-body">
        <div class="col-lg-4">
          <div id="credito">
            <?= $form->field($model, 'credito')->radioList(['Si'=>'Si','No'=>'No'])?>
          </div>
        </div>
        <div class="col-lg-3">
          <?= $form->field($model, 'diasCredito')->textInput(['maxlength' => 2 , 'type'=>"number"]) ?>
        </div>
        <div class="col-lg-5">
            <?php if (!$model->isNewRecord) {
                echo '<label>Monto Crédito ejecutado</label>';
                echo '<div style="background-color:#FFC; padding:5px"><h4>₡'.number_format($model->montoTotalEjecutado,2).'</h4></div><br>';


            } else {
              echo '<br><br><br><br>';
              $model->ot = 'No';
            } ?>
        </div>
        <div class="col-lg-5">
        <?= $form->field($model, 'documentoGarantia')->radioList(['Si'=>'Si','No'=>'No'])?>
        </div>
        <div class="col-lg-5">
          <?= $form->field($model, 'montoMaximoCredito')->
                           widget(MaskMoney::classname(), [
                              'pluginOptions' => [
                                  'prefix' => '¢',
                                  'thousandSeparator' => ',',
                                  'decimalSeparator' => '.',
                                   'precision' => 2,
                                   'allowZero' => false,
                                  'allowNegative' => false,
                              ]
                          ]) ?>
        </div>
        <!--?= $form->field($model, 'montoTotalEjecutado')->
        widget(MaskMoney::classname(), [
                              'pluginOptions' => [
                                  'readonly'=> true,
                                  'prefix' => '¢',
                                  'thousands' => ',',
                                  'decimal' => '.',
                                  'allowNegative' => false
                              ]
                          ]);
                          ?-->
        <div class="well col-lg-12">
          <div class="col-lg-7">
            <?= $form->field($model, 'autorizacion')->radioList(['Si'=>'Si','No'=>'No'])?>
          </div>
          <div class="col-lg-5">
            <?= $form->field($model, 'estadoCuenta')->radioList(['Abierta'=>'Abierta','Cerrada'=>'Cerrada'])?>
          </div>
        </div>
      </div>
    </div>
  </div><!-- fin de datos crédito -->
  <?php
      $ot = Yii::$app->params['ot'];
      if ($ot == true) {
   ?>
  <div class="col-lg-5">
  <div class="panel panel-warning">
    <div class="panel-heading">
      Datos para ejecutar O.T
    </div>
    <div class="panel-body">
      <div class="col-lg-4">
          <?= $form->field($model, 'ot')->radioList(['Si'=>'Si','No'=>'No'])?>
      </div>
      <div class="col-lg-3">
        <?= $form->field($model_ot, 'dias_ot')->textInput(['maxlength' => 2 , 'type'=>"number"]) ?>
      </div>
      <div class="col-lg-5">
          <?php if (!$model->isNewRecord) {
              echo '<label>Monto O.T ejecutado</label>';
              echo '<div style="background-color:#FFC; padding:5px"><h4>₡'.number_format($model_ot->montoTotalEjecutado_ot,2).'</h4></div><br>';
          } else {
            echo '<br><br><br><br>';
          } ?>
      </div>
      <div class="well col-lg-12">
        <div class="col-lg-6">
          <?= $form->field($model_ot, 'autorizacion_ot')->radioList(['Si'=>'Si','No'=>'No'])?>
        </div>
        <div class="col-lg-6">
          <?= $form->field($model_ot, 'estado_ot')->radioList(['Abierta'=>'Abierta','Cerrada'=>'Cerrada'])?>
        </div>
      </div>
  </div>
  </div>
</div>
<?php }
$apartados = Yii::$app->params['apartados'];
if ($apartados == true) { ?>
  <div class="col-lg-7">
  </div>
  <div class="col-lg-5">
  <div class="panel panel-default">
    <div class="panel-heading">
      APARTADOS
    </div>
    <div class="panel-body">
      <div class="col-lg-3">
        <?= $form->field($model_cl_apartado, 'dias_apartado')->textInput(['maxlength' => 2 , 'type'=>"number"]) ?>
      </div>
      <div class="col-lg-4">
        <?= $form->field($model_cl_apartado, 'montoMaximoApartado')->
                         widget(MaskMoney::classname(), [
                            'pluginOptions' => [
                                'prefix' => '¢',
                                'thousandSeparator' => ',',
                                'decimalSeparator' => '.',
                                 'precision' => 2,
                                 'allowZero' => false,
                                'allowNegative' => false,
                            ]
                        ]) ?>
      </div>
      <div class="col-lg-5">
          <?php if (!$model->isNewRecord) {
              $apartados = EncabezadoPrefactura::find()->where(['idCliente'=>$model->idCliente])->andWhere('estadoFactura = "Apartado"',[])->all();
              $monto_disponible_apartado = 0;
              foreach ($apartados as $key => $apartado) {
                $movi_adelanto = MovimientoApartado::find()->where(['idCabeza_Factura'=>$apartado->idCabeza_Factura])->all();
                $adelantos = 0;
                foreach ($movi_adelanto as $key => $adelanto) { $adelantos += $adelanto->monto_movimiento; }
                $monto_disponible_apartado += $apartado->total_a_pagar - $adelantos;
              }
              echo '<label>Monto Apartado ejecutado</label>';
              echo '<div style="background-color:#FFC; padding:5px"><h4>₡'.number_format($monto_disponible_apartado,2).'</h4></div><br>';
          } else {
            echo '<br><br><br><br>';
          } ?>
      </div>
      <div class="well col-lg-12">
        <div class="col-lg-6">
          <?= $form->field($model_cl_apartado, 'estado')->radioList(['Abierta'=>'Abierta','Cerrada'=>'Cerrada'])?>
        </div>
      </div>
    </div>
  </div>
  </div>
<?php } ActiveForm::end(); ?>

    <!--?php /*
    //script para modal
    $this->registerJs('
        // obtener la id del formulario y establecer el manejador de eventos
            $("form#solicitante-form").on("beforeSubmit", function(e) {
                var form = $(this);
                $.post(
                    form.attr("action")+"&submit=true",
                    form.serialize()
                )
                .done(function(result) {
                    form.parent().html(result.message);
                    $.pjax.reload({container:"#solicitante-grid"});
                });
                return false;
            }).on("submit", function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                return false;
            });
        '); */
    ?-->

</div>
