<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use backend\models\Clientes;
use common\models\User; //para confirmar eliminacion de cllientes
//use common\models\LoginForm; //para confirmar eliminacion de cllientes
use common\models\DeletePass; //para confirmar eliminacion de cllientes
use kartik\widgets\AlertBlock;
use kartik\popover\PopoverX; //http://demos.krajee.com/popover-x
//use kartik\widgets\ActiveForm;

//use kartik\grid\GridView;
//https://github.com/kartik-v/yii2-grid
//http://demos.krajee.com/grid
//http://demos.krajee.com/grid-demo
//http://demos.krajee.com/grid#data-column
//http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
//Modal para eliminar https://www.youtube.com/watch?v=ZDnV8aLTPi4

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;

?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
            /*$(document).on('click', '#activity-index-link-create', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalCreate').modal();
                    }
                );
            }));
            $(document).on('click', '#activity-index-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalUpdate').modal();
                    }
                );
            }));*/
            /*Eliminar modal*/
          /*  $(document).on('click', '#activity-index-link-delete', (function() {

                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalDelete').modal();
                    }
                );
            }));*/
            function enviaResultado(id) {
                $('#org').val(id);
            }

             // obtener la id del formulario y establecer el manejador de eventos
        $("form#delete-pass-form").on("beforeSubmit", function(e) {
            var form = $(this);
            $.post(
                form.attr("action")+"&submit=true",
                form.serialize()
            )
            .done(function(result) {
                form.parent().html(result.message);
                $.pjax.reload({container:"#solicitante-grid"});
            });
            return false;
        }).on("submit", function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        });

    </script>


<div class="clientes-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]);
     /*   $content = '<p class="text-justify">' .
        '¿Esta Seguro de eliminar este cliente?' .
        '</p>';
        echo PopoverX::widget([
            'header' => 'Hello world',
            'placement' => PopoverX::ALIGN_LEFT_BOTTOM,
            'content' => $content,
            'footer' => Html::button('Si', ['class'=>'btn btn-sm btn-primary', 'id'=>'activity-index-link-delete']) . Html::resetButton('No', ['class'=>'btn btn-sm btn-default']),
            'toggleButton' => ['label'=>'Clic aqui', 'class'=>'btn btn-default'],
        ]);*/
    ?>

    <p style="text-align:right">
        <?= Html::a('Agregar clientes', ['create'], ['class' => 'btn btn-success']) ?>
        <!--?= /*Html::a('Agregar cliente', '#', [
            'id' => 'activity-index-link-create',
            'class' => 'btn btn-success',
            'data-toggle' => 'modal',
            'data-target' => '#modalCreate',
            'data-url' => Url::to(['create']),
            'data-pjax' => '0',
        ]); */ ?-->
    </p>

    <?php
        $this->registerJs("  ");
    ?>
<div class="table-responsive">
<div id="semitransparente">
          <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
<!--?php  $dataProvider->pagination->pageSize = 10; ?-->

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            //['class' => 'kartik\grid\SerialColumn'],
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'idCliente',
                'options'=>['style'=>'width:15px'],
            ],
            [
                'attribute' => 'tipoIdentidad',
                'options'=>['style'=>'width:20px'],
            ],
            'identificacion',
            'nombreCompleto',

            //'genero',
            // 'fechNacimiento',
            // 'telefono',
            // 'fax',
            [
                'attribute' => 'celular',
                'options'=>['style'=>'width:100px'],
            ],
            'email:email',
            // 'direccion',
            // 'credito',
            // 'documentoGarantia',
            // 'diasCredito',
            // 'autorizacion',
            // 'montoMaximoCredito',
            // 'montoTotalEjecutado',
            // 'estadoCuenta',


          //  ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                  /*  'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'id' => 'activity-index-link-update',
                           // 'title' => Yii::t('app', 'Update'),
                            'data-toggle' => 'modal',
                            'data-target' => '#modalUpdate',
                            'data-url' => Url::to(['update', 'id' => $model->idCliente]),
                            'data-pjax' => '0',
                            ]);
                    },*/
                    'delete' => function ($url, $model, $key) {
                        //$cliente = Clientes::findOne($model->idCliente);

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#'/*['delete', 'id' => $model->idCliente]*/, [
                            'id' => 'activity-index-link-delete',
                            'class' => 'activity-delete-link',
                            'title' => Yii::t('app', 'Eliminar cliente '.$model->nombreCompleto),
                            'data-toggle' => 'modal',
                            'data-target' => '#modalDelete',
                            'data-value' => 'Eliminar',
                           // 'data-id' => $model->idCliente,
                           // 'data-method' => 'post',
                           // 'data-name' => 'so',
                            //'data-type' => 'post',
                            'onclick' => 'javascript:enviaResultado('.$model->idCliente.')',
                            'href' => Url::to('#'),
                           // 'data' => [ 'method' => 'post', 'confirm' => 'Está Seguro de eliminar este cliente?'],
                            'data-pjax' => '0',
                            ]);
                    },

                ]
            ],
        ],
    ]);


     ?>
</div>
</div>

<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un cliente ocupa una clave administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            //'enableClientScript' => true,
        ]);
        ?>
                <input type="hidden" id="org" name = "idCliente" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Cliente', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este cliente?',
                            'method' => 'post',
                        ],
                    ]) ?>
                     <!--?= //Html::submitButton('Ingresar', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?-->
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
    <?php
//------Crear
      /*  Modal::begin([
            'id' => 'modalCreate',
            'size'=>'modal-lg',
            'header' => '<h4 class="modal-title">Crear Cliente</h4>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
        ]);

        echo "<div class='well'></div>";

        Modal::end(); */
    ?>

    <?php
//------Actualizar
     /*   Modal::begin([
            'id' => 'modalUpdate',
            'size'=>'modal-lg',
            'header' => '<h4 class="modal-title">Actualizar Cliente</h4>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
        ]);

        echo "<div class='well'></div>";

        Modal::end();*/ //$model = new Clientes();
    ?>



    <!--?=


$gridColumns = "[
                    ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'nombreCompleto',
                            'pageSummary' => 'Page Total',
                            'vAlign'=>'middle',
                            'headerOptions'=>['class'=>'kv-sticky-column'],
                            'contentOptions'=>['class'=>'kv-sticky-column'],
                            'editableOptions'=>['header'=>'Name', 'size'=>'md']
                        ],
                        ['class' => 'kartik\grid\CheckboxColumn']
                ]";


    GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'nombreCompleto',
                            'pageSummary' => 'Page Total',
                            'vAlign'=>'middle',
                            'headerOptions'=>['class'=>'kv-sticky-column'],
                            'contentOptions'=>['class'=>'kv-sticky-column'],
                            'editableOptions'=>['header'=>'Name', 'size'=>'md']
                        ],
                        ['class' => 'kartik\grid\CheckboxColumn']
                ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            'columns'=>[
                ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']],
                ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']],
                ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
            ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
        ['content'=> Html::a('Agregar clientes', ['create'], ['class' => 'btn btn-success'])
           // Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
           // Html::a('&lt;i class="glyphicon glyphicon-repeat">&lt;/i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('kvgrid', 'Reset Grid')])
        ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY
    ],
]);
    ?-->

</div>
