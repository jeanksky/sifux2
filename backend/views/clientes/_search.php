<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ClientesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idCliente') ?>

    <?= $form->field($model, 'tipoIdentidad') ?>

    <?= $form->field($model, 'nombreCompleto') ?>

    <?= $form->field($model, 'identificacion') ?>

    <?= $form->field($model, 'genero') ?>

    <?php // echo $form->field($model, 'fechNacimiento') ?>

    <?php // echo $form->field($model, 'telefono') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'celular') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'direccion') ?>

    <?php // echo $form->field($model, 'credito') ?>

    <?php // echo $form->field($model, 'documentoGarantia') ?>

    <?php // echo $form->field($model, 'diasCredito') ?>

    <?php // echo $form->field($model, 'autorizacion') ?>

    <?php // echo $form->field($model, 'montoMaximoCredito') ?>

    <?php // echo $form->field($model, 'montoTotalEjecutado') ?>

    <?php // echo $form->field($model, 'estadoCuenta') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
