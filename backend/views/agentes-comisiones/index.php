<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use kartik\widgets\AlertBlock;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\AgentesComisiones;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AgentesComisionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agentes de  Comisiones';
$this->params['breadcrumbs'][] = $this->title;
$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
?>

<script type="text/javascript" src="<?php echo $jquery_min_js; ?>"></script>
    <script type="text/javascript">
     
     $(document).ready(function(){
       // $('[data-toggle="modal"]').tooltip();
        $('[data-toggle="toggle"]').popover();
    });

     $(document).on('click', '#activity-link-create', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        document.getElementById("well-create-agente-comision").innerHTML = '';
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-create-agente-comision').html(data);//muestro en el div los datos del create
                        $('#crearAgenteComisionModal').modal('show');//muestro la modal
                    }
                );
            }));

     $(document).on('click', '#activity-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        document.getElementById("well-update-agente-comision").innerHTML = '';
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-update-agente-comision').html(data);//muestro en el div los datos del create
                        $('#actualizarAgenteComisionModal').modal('show');//muestro la modal
                    }
                );
            }));

</script>
<style>
    #crearAgenteComisionModal .modal-dialog{
    width: 70%!important;
    /*margin: 0 auto;*/
    }

    #actualizarAgenteComisionModal .modal-dialog{
    width: 70%!important;
    /*margin: 0 auto;*/
    }

</style>

<div class="agentes-comisiones-index">

    <h1 align="center"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <p style="text-align:right">
        <?= Html::a('<strong><span class="glyphicon glyphicon-edit"></span> Agregar Agentes Comisiones</strong>', '#', [
            'id' => 'activity-link-create',
            'data-toggle' => 'modal',
            'class' => 'btn btn-success',
            //'data-trigger'=>'hover',
            //'data-target' => '#crearmovimientomodal',
            'data-url' => Url::to(['create']),
            'data-pjax' => '0',
            'title' => Yii::t('app', 'Proceda a crear un nuevo Agentes Comision'),
        ]) ?>
    </p>

<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>


    <p align="right">
        <!--?= Html::a('<strong><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Agentes Comisiones<strong>', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>
<div class="table-responsive">
    <div id="semitransparente">
         <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idAgenteComision',
            'tipoIdentidad',
            'cedula',
            'nombre',
            'telefono',
            //'direccion',
             'porcentComision',
             //'estadoAgenteComision',
            [
              'attribute' => 'estadoAgenteComision',
              'options'=>['style'=>'width:150px'],
              'filter'=>Html::activeDropDownList($searchModel, 'estadoAgenteComision',
                ArrayHelper::map(AgentesComisiones::find()
                ->asArray()
                ->all(),'estadoAgenteComision','estadoAgenteComision'),['class' => 'form-control','prompt' => 'Todos'])
            ],

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'id' => 'activity-link-update',
                            'data-toggle' => 'modal',
                            //'data-trigger'=>'hover',
                            //'data-target' => '#crearmovimientomodal',
                            'data-url' => Url::to(['update', 'id' => $model->idAgenteComision]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Detalles del Agente Comision'),
                        ]);
                    },
            ],//buttun
        ],//class
        ],//columns
    ]); ?>

</div>
</div>
</div>
<?php
//------------------------modal para crear Gastos
        Modal::begin([
            'id' => 'crearAgenteComisionModal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span><strong> Registrar Agentes Comision</strong></h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-create-agente-comision'></div></div>";

        Modal::end();

        //------------------------modal para crear Gastos


//------------------------modal para crear Gastos
        Modal::begin([
            'id' => 'actualizarAgenteComisionModal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span><strong> Actualizar Agentes Comision</strong></h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-update-agente-comision'></div></div>";

        Modal::end();

        //------------------------modal para crear Gastos


?>