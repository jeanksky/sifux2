<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AgentesComisiones */

/*$this->title = 'Create Agentes Comisiones';
$this->params['breadcrumbs'][] = ['label' => 'Agentes Comisiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="agentes-comisiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
