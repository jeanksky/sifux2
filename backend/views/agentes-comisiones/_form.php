<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\models\Tribunales;
/* @var $this yii\web\View */
/* @var $model backend\models\AgentesComisiones */
/* @var $form yii\widgets\ActiveForm */
  $tribunales = new Tribunales();
?>

<script type="text/javascript">
/*Para desabilitar el boton*/
    $(document).ready(function () {
     // $('#crear_movimiento').prop('disabled', true);
    });

    //espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
      var nav4 = window.Event ? true : false;
      var key = nav4 ? evt.which : evt.keyCode;
      return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

    //funcion para buscar el cliente por numero de cedula
function buscar_cliente(id) {
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('clientes/buscar_cliente') ?>",
      type:"post",
      dataType: 'json',
      data: { 'id' : id },
      beforeSend: function() {
        //$('#genero :radio').removeAttr("checked");
        $('.nombreAG').val('');
      },
      success: function(data){
        //alert('data.nombreCompleto');
        $('.nombreAG').val(data.nombreCompleto);
      },
      error: function(msg, status,err){
          //alert('error 32');
      }
  });
}

//habilitar boton
    function habilito(argument) {
        if (argument == true) {
            $('#crear_movimiento').prop('disabled', false);
        }
        else {
            $('#crear_movimiento').prop('disabled', true);
        }
    }

//consulta que dato esta repetido
    function consulta_repetido(){
        numero_documento = $(".numero_documento").val();//document.getElementById("numero_documento").value;
        if (numero_documento != '') {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('agentes-comisiones/consulta_repetido') ?>",
                    type:"post",
                    data: { 'numero_documento' : numero_documento},
                    success: function(data){
                        if (data=='no') {
                            $('#notif_repetido').html('<center><strong><font size=4 color="red">Atención, Agente Comision repetido</font></strong></center>');
                            $('#crear_movimiento').prop('disabled', true);
                        }
                        else {
                            $('#notif_repetido').html('');
                            $('#crear_movimiento').prop('disabled', false);
                        }
                    },
                    error: function(msg, status,err){
                     $('#notif_repetido').html('Error');
                    }
            });
        }
    }
</script>

<div class="agentes-comisiones-form">

    <?php $form = ActiveForm::begin([
         'id' => 'agentes-comisionesForm',
         'enableAjaxValidation' => true,
         'enableClientScript' => true,
         'enableClientValidation' => true
     ]); ?>
     <div class="col-lg-6">
       <?php
       if ($model->isNewRecord)
         {
           $model->tipoIdentidad = 'Física';
         } ?>
       <!--?=$form->field($model, 'tipoIdentidad')->radioList(['1' => 'Física', '2' => 'Jurídica'], ['item' => function($index, $label, $name, $checked, $value) {$checked = $checked == 1 ? 'checked=""' : 'disabled=""';echo "<label><input tabindex='{$index}' type='radio' {$checked}'name='{$name}'value='{$value}'> {$label}</label>";}]);?-->
       <div id="tipo">
           <?=
           //$model->tipoIdentidad = 'Jurídica';
           $form->field($model, 'tipoIdentidad')->radioList(
           [
           'Física'=>'Física',
           'Jurídica'=>'Jurídica'
           ]/*, [
           'onclick'=>"muestra_oculta('contenido_a_mostrar')",
           'item' => function ($index, $label, $name, $checked, $value) {
               return Html::radio('Física', $checked, ['value' => $value]);
           },
           ]*/)
           ?>
       </div>
     </div>
 <div class="col-lg-6">
    <?= $form->field($model, 'cedula')->textInput(['class'=>'numero_documento form-control','maxlength' => true,'onkeypress'=>'return isNumberDe(event)', 'onchange' => 'javascript:buscar_cliente(this.value)', 'onkeyup'=>"consulta_repetido()"]) ?>
    <div id="notif_repetido"></div>
</div>
 <div class="col-lg-6">
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'class'=>'nombreAG form-control']) ?>
</div>
 <div class="col-lg-6">
    <!--?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?-->
          <?= $form->field($model, 'telefono')->
          widget(\yii\widgets\MaskedInput::className(), [
                  'mask' => '9999-99-99',
          ]) ?>
</div>
 <div class="col-lg-6">
    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
</div>
 <div class="col-lg-6">
    <!--?= $form->field($model, 'porcentComision')->textInput() ?-->
    <?= $form->field($model, 'porcentComision')->widget(\yii\widgets\MaskedInput::className(), [
                      'options' => ['class'=>'','class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                          'autoGroup' => true
                      ],
                  ]); ?>

</div>
<div class="col-lg-6">
    <?= $form->field($model, 'estadoAgenteComision')->radioList(array('Activo'=>'Activo','Inactivo'=>'Inactivo')); ?>
</div>
    <div class="col-lg-11">
    <div class="form-group">
        <p style="text-align:right">
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['id'=>'crear_movimiento', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </p>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
