<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AgentesComisiones */

$this->title = $model->idAgenteComision;
$this->params['breadcrumbs'][] = ['label' => 'Agentes Comisiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agentes-comisiones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idAgenteComision], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idAgenteComision], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idAgenteComision',
            'cedula',
            'nombre',
            'telefono',
            'direccion',
            'porcentComision',
        ],
    ]) ?>

</div>
