<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AgentesComisiones */

/*$this->title = 'Update Agentes Comisiones: ' . ' ' . $model->idAgenteComision;
$this->params['breadcrumbs'][] = ['label' => 'Agentes Comisiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAgenteComision, 'url' => ['view', 'id' => $model->idAgenteComision]];
$this->params['breadcrumbs'][] = 'Update';*/
?>
<div class="agentes-comisiones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
