<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Clientes;
use backend\models\Funcionario;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\DevolucionesProducto */
$mostrarFactura = Html::a($model->idCabeza_Factura, ['#'], [
              'id' => 'activity-factura-devol',
              'data-toggle' => 'modal',
              'data-target' => '#modalFacturadev',
              'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $model->idCabeza_Factura]),
              'data-pjax' => '0',
              'title' => Yii::t('app', 'Procede a mostrar la factura')]);
$fe = $model->factun_id > 0  ? 'FE: '.$model->fe : '';
$tipoFacturacion = $model->tipoFacturacion != 4 ? 'Contado' : 'Crédito';
$title = '(ID: '.$mostrarFactura.') '.$fe.' ['.$tipoFacturacion.']['.$model->estadoFactura.']';
$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
$vendedor = Funcionario::find()->where(['idFuncionario'=>$model->codigoVendedor])->one();
if (@$cliente) {
  $nombre_cliente = $cliente->nombreCompleto;
} else {
  $nombre_cliente = $model->idCliente;
}
?>
<div class="devoluciones-producto-view" style="width: 100%; overflow-x: auto; border: 2px solid #333;">

    <h1 style="text-align: center;"><small>Crear devolución para Factura <?= $title ?></small></h1>
    <?php
    if ($model->factun_id > -1) {
      if ($model->estado_hacienda=='01') echo '<center><span class="label label-success" style="font-size:12pt;">Aceptado por Hacienda</span></center><br>';
      elseif ($model->estado_hacienda=='02') echo '<center><span class="label label-primary" style="font-size:12pt;">Recibida por Hacienda</span></center><br>';
      elseif ($model->estado_hacienda=='03') echo '<center><span class="label label-danger" style="font-size:12pt;">Rechazada por Hacienda</span></center><br>';
      elseif ($model->estado_hacienda=='04') echo '<center><span class="label label-info" style="font-size:12pt;">Enviado a Hacienda</span></center><br>';
      elseif ($model->estado_hacienda=='05') {
        echo $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-warning" style="font-size:12pt;">EN ESPERA</span>';
      }
    }
    $fecha_emision = $model->fecha_emision == null ? 'No emitida' : date('d-m-Y', strtotime( $model->fecha_emision ));
    ?>
    <p style="text-align: center;">NOTA: No refresque la página hasta terminar el proceso de devolución, de lo contrario perderá los cambios.</p>
    <div class="col-lg-5" ><h2><small>Cliente: </small><br><?= $nombre_cliente ?></h2></div>
    <div class="col-lg-3" ><h2><small>Vendedor: </small><br><?= $vendedor->nombre.' '.$vendedor->apellido1 ?></h2></div>
    <div class="col-lg-2" ><h2><small>Factura emitida el: </small><br><?= $fecha_emision ?></h2></div>
    <div class="col-lg-2" ><h2><small>Usuario que registra:<br></small><?= Yii::$app->user->identity->username ?></h2></div>
    <div class="col-lg-3" >

    </div>

    <div class="col-lg-6" id="campo_notificacion">
    </div>
    <input type="hidden" id="HiddenScrollbarPosition" runat="server" />
    <div class="col-lg-12">
      <div class="table-responsive tab-content" id="orden_compra_productos_disponibles" style="border: 2px solid #333;">
        <?php
        if ($fecha_emision == 'No emitida') {
          echo '<center><div class="alert alert-info">Documento no emitido</div></center>';
        } else {
          echo $this->render('producto_en_disposicion', [
            'model' => $model]);
        }
         ?>
      </div>
    </div>
    <div class="col-lg-12">
      <br><br>
    </div>
</div>
