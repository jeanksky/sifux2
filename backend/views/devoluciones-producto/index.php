<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historial de factura y devoluciones de productos';
$this->params['breadcrumbs'][] = $this->title;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load.gif';
?>


<div class="devoluciones-producto-index">

    <h1 style="text-align: center;"><?= Html::encode($this->title) ?></h1>
    <div class="col-lg-4" ><br></div>
    <div class="col-lg-4" >
      <!--label for="dias_rendimiento">Días rendimiento</label-->
      <?= Html::a('<i class="fa fa-search" aria-hidden="true"></i> Busqueda avanzada', null, [
          'class'=>'btn',
          'data-toggle'=>'modal',
          'data-target'=>'#modal_facturas',
          'onclick' => 'javascript:modal_busqueda_avanzada()',
          'title'=>'Proceda a buscar una factura.'
        ]).Html::input('text', '', '', [ 'autofocus' => 'autofocus',
                                        'class' => 'form-control input-lg',
                                        'id' => 'factura_a_ejecutar',
                                        'placeholder' =>'FACTURA + [ENTER]',
                                        'style'=>'font-family: Fantasy; font-size: 30pt; text-align: center;',
                                        'onkeyup' => 'javascript:ingresar_factura(event.keyCode,this.value)',
                                        'onkeypress'=>'return isNumber(event)',
                                        'title'=>"Ingrese el ID de la factura o el consecutivo electrónico" ]) ?><br>
    </div>
    <div class="col-lg-4" ><br></div>
    <div class="col-lg-12" >
      <div class="panel panel-success" id="estadistica_producto">
        <center><h1><small>Esperando una factura</small><h1></center><br>
      </div>
    </div>
</div>
<style media="screen">
#modal_facturas .modal-dialog{
width: 60%!important;
/*margin: 0 auto;*/
}
</style>
<div class="modal fade" id="modal_facturas" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Busqueda de Facturas (con un límite máximo de 50 resultados por consulta)</h4>
    </div>
    <div class="modal-body">
        <?php
            echo '<div class="col-lg-3">
            '. yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName', 'options' => ['class'=>'form-control', 'placeholder'=>'FECHA DE EMISIÓN (REQUERIDA)', 'id'=>'busqueda_fec', 'onchange'=>'javascript:buscar()']]) .'
            </div><div class="col-lg-6">'.Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '80', 'id' => 'busqueda_multiple', 'class' => 'form-control', 'placeholder' =>'Busque la factura por ID, consecutivo electronico, cliente o estado', 'onKeyUp' => 'javascript:buscar()']).'</div>
            <div class="col-lg-3">'.Html::input('text', '', '', ['size' => '80', 'id' => 'busqueda_fu', 'class' => 'form-control', 'placeholder' =>'Busqueda por funcionario que registró', 'onKeyUp' => 'javascript:buscar()']).'<br></div>';
        ?>
        <br>
        <div id="resultadoBusqueda" style="height: 200px;width: 100%; overflow-y: auto; "></div>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
    </div>
  </div>
</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//Funcion que hace abrir modal desde la tecla[F2]
window.onkeydown = tecla;
  function tecla(event) {
    //event.preventDefault();
    num = event.keyCode;

    if(num==113)
      $('#modal_facturas').modal('show');//muestro la modal

  }
//Funcion para que me permita ingresar solo numeros
  function isNumber(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if ( (charCode > 47 && charCode < 58))
              return true;
          return false;
      }
      function isNumberDe(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || key==46 || (key >= 48 && key <= 57));
      }

  function buscar() {
    var value = document.getElementById("busqueda_multiple").value;
    var busqueda_fec = document.getElementById("busqueda_fec").value;
    var busqueda_fu = document.getElementById("busqueda_fu").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/cargar-factura') ?>",
        type:"post",
        data: { 'value' : value, 'busqueda_fec' : busqueda_fec, 'busqueda_fu' : busqueda_fu },
        success: function(data){
          $('#resultadoBusqueda').html(data);
        },
        error: function(msg, status,err){
          $('#resultadoBusqueda').html('<center><h1><small>Ese número no corresponde a una factura emitida.</small><h1></center><br>');
        }
    });
  }

  //muestra el historial de la factura desde el input
  function ingresar_factura(key,factura) {
    if(key == 13) {
      $('#estadistica_producto').html('<center><img src="<?php echo $load_buton; ?>" style="height: 320px;"></center>');
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/view') ?>",
          type:"post",
          data: { 'id' : factura },
          success: function(data){
            $('#estadistica_producto').html(data);
          },
          error: function(msg, status,err){
            $('#estadistica_producto').html('<center><h1><small>Ese número no corresponde a una factura emitida.</small><h1></center><br>');
          }
      });
    }
  }

  //muestra el historial de la facrura desde la modal
  //ingresa añ historial de la factura
  function agrega_factura(factura) {
      $('#estadistica_producto').html('<center><img src="<?php echo $load_buton; ?>" style="height: 320px;"></center>');
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/view') ?>",
          type:"post",
          data: { 'id' : factura },
          success: function(data){
            $('#estadistica_producto').html(data);
            $('#factura_a_ejecutar').val(factura);
            $('#modal_facturas').modal('hide');
          },
          error: function(msg, status,err){
            $('#estadistica_producto').html('<center><h1><small>Ese número no corresponde a una factura emitida.</small><h1></center><br>');
          }
      });

  }

  //Modal que me muestra la factura de facturas pendientes
  $(document).on('click', '#activity-factura-devol', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $("#mostrar_factura_modal_devol").html( data );

                    }
                );
            }));
  //carga el producto seleccionado como producto a devolver
  function devolver_producto(key, idDetalleFactura, idCabeza_Factura, codProdServicio, cant_fact) {
    if(key == 13) {
      var cant_devolver = parseFloat(document.getElementById(codProdServicio).value).toFixed(2);
      if (cant_devolver > 0.00) {
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/devolver_producto') ?>",
            type:"post",
            dataType: 'json',
            data: { 'codProdServicio' : codProdServicio, 'idDetalleFactura' : idDetalleFactura, 'idCabeza_Factura' : idCabeza_Factura, 'cant_fact' : cant_fact, 'cant_devolver' : cant_devolver },
            success: function(data){
              $('#estadistica_producto').html(data.estadistica_producto);
              $("#campo_notificacion").html(data.campo_notificacion);
              setTimeout(function() {//aparece la alerta en un milisegundo
                  $("#campo_notificacion").fadeIn();
              });
              setTimeout(function() {//se oculta la alerta luego de 3 segundos
                  $("#campo_notificacion").fadeOut();
              },8000);
            },
            error: function(msg, status,err){
              //$('#estadistica_producto').html('<center><h1><small>Ese número no corresponde a una factura emitida.</small><h1></center><br>');
            }
        });
      }
    }
  }
  //cargo todos los productodisponibles a devolver
  function cargar_todos(idCabeza_Factura) {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/cargar_todos') ?>",
        type:"post",
        dataType: 'json',
        data: { 'idCabeza_Factura' : idCabeza_Factura },
        success: function(data){
          $('#estadistica_producto').html(data.estadistica_producto);
          $("#campo_notificacion").html(data.campo_notificacion);
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#campo_notificacion").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 3 segundos
              $("#campo_notificacion").fadeOut();
          },8000);
        },
        error: function(msg, status,err){
          //$('#estadistica_producto').html('<center><h1><small>Ese número no corresponde a una factura emitida.</small><h1></center><br>');
        }
    });
  }
  //descrata el producto que esta en la lista de devoluciones
  function descartar_producto(idProd_dev, idFactura) {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/descartar_producto') ?>",
        type:"post",
        dataType: 'json',
        data: { 'idProd_dev' : idProd_dev, 'idFactura' : idFactura },
        success: function(data){
          $('#estadistica_producto').html(data.estadistica_producto);
          $("#campo_notificacion").html(data.campo_notificacion);
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#campo_notificacion").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 3 segundos
              $("#campo_notificacion").fadeOut();
          },8000);
        },
        error: function(msg, status,err){
          //$('#estadistica_producto').html('<center><h1><small>Ese número no corresponde a una factura emitida.</small><h1></center><br>');
        }
    });
  }
  function aplicar_devolucion(this_boton, subtotal, descuento, impuesto_venta) {
    var idCabeza_Factura = document.getElementById("idCabeza_Factura").value;
    var monto_nota_credito = document.getElementById("monto_nota_credito").value;
    var detalle_devolucion = document.getElementById("detalle_devolucion").value;
    if (detalle_devolucion!='') {
      var confirm_pago = confirm("¿Está seguro de aplicar esta devolución?");
      if (confirm_pago == true) {
        var $btn = $(this_boton).button('loading'); // simulating a timeout
        $("#campo_notificacion").html('<br><center><div class="alert alert-info">Espere, obteniendo resultados de hacienda...</div></center>');
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/aplicar_devolucion') ?>",
            type:"post",
            dataType: 'json',
            data: { 'subtotal' : subtotal,
                    'descuento' : descuento,
                    'impuesto_venta' : impuesto_venta,
                    'idCabeza_Factura' : idCabeza_Factura,
                    'monto_nota_credito' : monto_nota_credito,
                    'detalle_devolucion' : detalle_devolucion },
            success: function(data){
              $('#estadistica_producto').html(data.estadistica_producto);
              $("#campo_notificacion").html(data.campo_notificacion);
              setTimeout(function() {//aparece la alerta en un milisegundo
                  $("#campo_notificacion").fadeIn();
              });
              setTimeout(function() {//se oculta la alerta luego de 3 segundos
                  $("#campo_notificacion").fadeOut();
              },8000);
            },
            error: function(msg, status,err){
              $("#campo_notificacion").html('<br><center><div class="alert alert-danger">Error, no hay comunicación con Hacienda</div></center>');
              setTimeout(function() {//aparece la alerta en un milisegundo
                  $("#campo_notificacion").fadeIn();
              });
              setTimeout(function() {//se oculta la alerta luego de 3 segundos
                  $("#campo_notificacion").fadeOut();
              },8000);
            }
        });
      }
    } else {
      alert('No puedes aplicar la devolucion sin antes justificar un detalle por devolución.');
    }
  }

  function re_aplicar_devolucion(this_boton, idDevolucion) {
    $("#campo_notificacion").html('<br><center><div class="alert alert-info">Espere, obteniendo resultados de hacienda...</div></center>');
    var $btn = $(this_boton).button('loading');
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/re_aplicar_devolucion') ?>",
        type:"post",
        dataType: 'json',
        data: { 'idDevolucion' : idDevolucion },
        success: function(data){
          $('#estadistica_producto').html(data.estadistica_producto);
          $("#campo_notificacion").html(data.campo_notificacion);
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#campo_notificacion").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 3 segundos
              $("#campo_notificacion").fadeOut();
          },8000);
        },
        error: function(msg, status,err){
          $("#campo_notificacion").html('<br><center><div class="alert alert-danger">Error, no hay comunicación con Hacienda</div></center>');
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#campo_notificacion").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 3 segundos
              $("#campo_notificacion").fadeOut();
          },8000);
        }
    });
  }

  //ver la devolucion generada
  var visual_dev = null;
  function ver_devolucion(id_dev) {
    obj = document.getElementById('contenido_dev'+id_dev);
		obj.style.display = (obj==visual_dev) ? 'none' : 'block';
		if (visual_dev != null)
		visual_dev.style.display = 'none';
		visual_dev = (obj==visual_dev) ? null : obj;
  }

  //enviar por correo la nota de credito
  function enviar_nc_correo(id, idMov) {
    var confirm_pago = confirm("¿Está seguro de enviar este movimiento?");
    if (confirm_pago == true) {
    var email_cliente = document.getElementById("email_cliente_mov_"+idMov).value;
    if (validateEmail(email_cliente)) {
    document.getElementById("notificacion_movimiento").innerHTML = '<br><span class="label label-info" style="font-size:15pt;">Espere, envío de movimiento en proceso...</span>';
    $.get("<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/enviar_correo_factura_movimiento') ?>",
          { 'id' : id, 'email' : email_cliente, 'idMov' : idMov },
           function(notif){
                    document.getElementById("notificacion_movimiento").innerHTML = notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notificacion_movimiento").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notificacion_movimiento").fadeOut();
                        refrescar(id);
                    },1000);
                }
            );
          } else {
            document.getElementById("notificacion_movimiento"+idMov).innerHTML = '<br><center><span class="label label-danger" style="font-size:15pt;">Verifique que la dirección sea correcta.</span></center>';
            setTimeout(function() {//aparece la alerta en un milisegundo
                $("#notificacion_movimiento").fadeIn();
            });
            setTimeout(function() {//se oculta la alerta luego de 4 segundos
                $("#notificacion_movimiento").fadeOut();
            },5000);
          }
    }
  }

  function refrescar(id) {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/refrescar') ?>",
        type:"post",
        data: { 'idCabeza_Factura' : id },
        success: function(data){
          $('#estadistica_producto').html(data);
        }
    });
  }
  //valida la sintacis de la direccion de correo electronico
  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
</script>

<?php
//-------------------------------------------------Pantallas modales-----------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//Muestra una modal de la factura seleccionada
       Modal::begin([
              'id' => 'modalFacturadev',
              'size'=>'modal-lg',
              'header' => '<center><h4 class="modal-title">Factura</h4></center>',
              'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                  'class'=>'btn btn-info',
                  'target'=>'_blank',
                  'data-toggle'=>'tooltip',
                  'title'=>'Genera un PDF de la factura para imprimir'
              ]),*/
          ]);
          echo "<div class='well'><div id='mostrar_factura_modal_devol'></div></div>";

          Modal::end();
  ?>
