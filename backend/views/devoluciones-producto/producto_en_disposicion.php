<?php
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
use backend\models\OrdenCompraSession;
use backend\models\FacturasDia;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Clientes;
use backend\models\HistorialEnvioEmail;
use backend\models\DevProductosDevueltos;
use backend\models\DevolucionesProducto;
use backend\models\MovimientoCredito;
use backend\models\MovimientoCobrar;
use backend\models\Factun;
use backend\models\Empresa;
  $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
  $detalle_factura = DetalleFacturas::find()->where(['idCabeza_factura' => $model->idCabeza_Factura])->all();
  $load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
  $cantidad_sugerida = 1;
  $promedio_sugerido = 0;
?>
<style media="screen">
  .tbody_ {
  display: block; /*obligado*/
  height: 300px; /*la que necesites*/
  /*width : 100%;*/
  overflow: auto; /*obligado*/
  }
  .thead_ {
    display: block; /*obligado*/
    /*width : 100%;*/
  }
  /*#tam_descr { width : 30%; }
  #tam_cantp { width : 20%; }*/
  .text {
  font-size:15px;
  font-family:helvetica;
  font-weight:bold;
  color:#E87B07;
  text-transform:uppercase;
}
.text2 {
font-size:13px;
font-family:helvetica;
font-weight:bold;
color:#FE2E64;
text-transform:uppercase;
}
.parpadea {

  animation-name: parpadeo;
  animation-duration: 0.5s;
  animation-timing-function: linear;
  animation-iteration-count: infinite;

  -webkit-animation-name:parpadeo;
  -webkit-animation-duration: 0.5s;
  -webkit-animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
}

@-moz-keyframes parpadeo{
  15% { opacity: 1.0; }
  20% { opacity: 0.0; }
  25% { opacity: 1.0; }
}

@-webkit-keyframes parpadeo {
  15% { opacity: 1.0; }
  20% { opacity: 0.0; }
   25% { opacity: 1.0; }
}

@keyframes parpadeo {
  15% { opacity: 1.0; }
   20% { opacity: 0.0; }
  25% { opacity: 1.0; }
}

</style>
<div class="col-lg-7" >
<h3 style="text-align:center">
  Productos de la factura
  <span style="float:right">
    <a onclick="cargar_todos(<?= $model->idCabeza_Factura ?>)" title="Cargar todos los productos como devueltos" class="alert-link"  style="cursor:pointer;"><i class="fa fa-angle-double-right" style="font-size:40px"></i></a>
  </span>
</h3>
<table class="items table table-sm table-inverse" id="tabla_productos_disponibles" width="100%">
  <thead class="thead_">
    <tr class="table-warning">
      <th width="10%"><font face="arial" size=4>Cant.</font></th>
      <th width="15%"><font face="arial" size=4>Código</font></th>
      <th width="25%" id="tam_descr"><font face="arial" size=4>Descripción</font></th>
      <th width="10%" style="text-align:right"><font face="arial" size=4>Prec.Unit</font></th>
      <th width="10%" style="text-align:right"><font face="arial" size=4>Descuento</font></th>
      <th width="10%" style="text-align:right"><font face="arial" size=4>IV%</span></th>
      <th width="10%" style="text-align:right"><font face="arial" size=4>Total</span></th>
      <th width="10%" style="text-align:right" id="tam_cantp"><font face="arial" size=4><span class="label label-success">Cant. a devl</span></font></th>
    </tr>
  </thead>
  <tbody class="tbody_" id="tbody_pd">
    <?php
      foreach ($detalle_factura as $key => $value) {
        $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$value->codProdServicio])->one();
        $descrip = $value->codProdServicio != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
        $cantidad_a_devolver = Html::input('text', '', '', [ 'class' => 'form-control input-sm',
                                                                        'id' => $value->codProdServicio,
                                                                        'style'=>'font-family: Fantasy; font-size: 18pt; text-align: center;',
                                                                        'onkeypress'=>'return isNumberDe(event)',
                                                                        'onkeyup'=>'javascript:devolver_producto(event.keyCode, '.$value->idDetalleFactura.', '.$model->idCabeza_Factura.',"'.$value->codProdServicio.'",'.$value->cantidad.');'
                                                                      ]);
        //buscamos en los productos devueltos correspondientes a esta factura que ya hayan sigo agregados en devoluciones
        $cant_fact = 0;
        if (@$devProdDev = DevProductosDevueltos::find()->where(['idDetalleFactura'=>$value->idDetalleFactura])->all()) {
          foreach ($devProdDev as $key => $det_dev) {
            $cant_fact += $det_dev->cantidad_dev;
          }
        }
        if ($cant_fact!=$value->cantidad) {
          $cant_restante = $value->cantidad - $cant_fact;
          $cant_restante = '<span class="label label-success">'.$cant_restante.'</span>';
          echo '<tr  style = "cursor:pointer;" title="'.$descrip.'">
                  <td width="10%"><font face="arial" size=4>'.$value->cantidad.' '.$cant_restante.'</font></td>
                  <td width="15%"><font face="arial" size=2>'.$value->codProdServicio.'</font></td>
                  <td width="25%"><font face="arial" size=2>'.substr($descrip,0,20).'</font></td>
                  <td align="right" width="10%"><font face="arial" size=2>'.number_format($value->precio_unitario,2).'</font></td>
                  <td align="right" width="10%"><font face="arial" size=2>'.number_format($value->descuento_producto*$value->cantidad,2).'</font></td>
                  <td align="right" width="10%"><font face="arial" size=2>'.number_format($value->subtotal_iva,2).'</font></td>
                  <td align="right" width="10%"><font face="arial" size=2>'.number_format(($value->precio_unitario*$value->cantidad),2).'</font></td>
                  <td align="right" width="10%" class="actions button-column">'.$cantidad_a_devolver.'</td>
                </tr>';
        }

      }
    ?>
  </tbody>
</table>
<?php
$servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
  $subtotal = $model->subtotal + $servicio;
  $total_a_pagar = $model->total_a_pagar + $servicio;
} else {
  $subtotal = $model->subtotal;
  $total_a_pagar = $model->total_a_pagar;
}
?>
<div class="col-lg-3"><div class="alert alert-info">SUBTOT: <span style="float:right"><?= number_format($subtotal,2) ?></span></div></div>
<div class="col-lg-3"><div class="alert alert-info">DSC: <span style="float:right"><?= number_format($model->porc_descuento,2) ?></span></div></div>
<div class="col-lg-3"><div class="alert alert-info">IV: <span style="float:right"><?= number_format($model->iva,2) ?></span></div></div>
<div class="col-lg-3"><div class="alert alert-info">TOTAL: <span style="float:right"><?= number_format($total_a_pagar,2) ?></span></div></div>
</div>
<div class="col-lg-5" >
  <center><h3>Productos de devolución</h3></center>
  <table class="items table table-sm table-inverse" id="tabla_productos_devolucion" width="100%">
    <thead class="thead_">
      <tr class="table-warning">
        <th width="10%"><font face="arial" size=4>Cant.</font></th>
        <th width="30%"><font face="arial" size=4>Código</font></th>
        <th width="10%" style="text-align:right"><font face="arial" size=4>Prec.Unit</font></th>
        <th width="10%" style="text-align:right"><font face="arial" size=4>Descuento</font></th>
        <th width="10%" style="text-align:right"><font face="arial" size=4>IV%</font></th>
        <th width="30%" style="text-align:center"><font face="arial" size=4>Total</font></th>
      </tr>
    </thead>
    <tbody class="tbody_" id="tbody_pd">
      <?php
        $subtotal_dev = 0;
        $desc_dev = 0;
        $iv_dev = 0;
        $total_dev = $contador = 0;
        $prod_dev = DevProductosDevueltos::find()->where(['idFactura'=>$model->idCabeza_Factura])->all();
        foreach ($prod_dev as $key => $value) {
          $detall_fac = DetalleFacturas::findOne($value->idDetalleFactura);
          $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$detall_fac->codProdServicio])->one();
          $descrip = $detall_fac->codProdServicio != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
          $descartar = Html::a('', null, ['onClick'=>'descartar_producto('.$value->idProd_dev.','.$value->idFactura.')', 'class'=>'glyphicon glyphicon-trash', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Descartar este producto']);
          $color_letra = '#34495E'; $clock = ''; $fecha = date('d-m-Y | h:i:s A', strtotime( $value->fecha_hora ));
          if ($value->estado=='Aplicado') { $color_letra = '#95A5A6'; $descartar = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>'; $clock = '<span class="glyphicon glyphicon-calendar" title="'.$fecha.'" aria-hidden="true"></span>';
          } else {
            $contador += 1;
            $subtotal_dev += $value->cantidad_dev * $detall_fac->precio_unitario;
            //------------------------------------
            $dcto = $detall_fac->subtotal_iva != 0 ? 0 : ($detall_fac->descuento_producto * $value->cantidad_dev);
            $dcto_imp = $detall_fac->subtotal_iva == 0 ? 0 : ($detall_fac->descuento_producto * $value->cantidad_dev);
            $desc_dev += $dcto + $dcto_imp;
            //------------------------------------
            $prod_sin_impuesto = $value->iv != 0 ? 0 : $value->cantidad_dev * $detall_fac->precio_unitario;
            $prod_con_impuesto = $value->iv == 0 ? 0 : $value->cantidad_dev * $detall_fac->precio_unitario;
            $iv_dev += ($prod_con_impuesto - $dcto_imp) * Yii::$app->params['impuesto'];
            //------------------------------------
            $tp = $prod_sin_impuesto + $prod_con_impuesto;
            $total_dev += ($tp-($dcto+$dcto_imp))+(($prod_con_impuesto - $dcto_imp) * Yii::$app->params['impuesto']);
          }
          echo '<tr style = "cursor:pointer; color:'.$color_letra.';" title="'.$descrip.'">
                  <td width="10%"><font face="arial" size=2>'.$value->cantidad_dev.'</font></td>
                  <td width="30%"><font face="arial" size=2>'.$detall_fac->codProdServicio.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$clock.'</font></td>
                  <td align="right" width="10%"><font face="arial" size=2>'.number_format($detall_fac->precio_unitario,2).'</font></td>
                  <td align="right" width="10%"><font face="arial" size=2>'.number_format($value->descuento*$value->cantidad_dev,2).'</font></td>
                  <td align="right" width="18%"><font face="arial" size=2>'.number_format($value->iv,2).'</font></td>
                  <td align="right" width="22%"><font face="arial" size=2>'.number_format($value->total,2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$descartar.'</font></td>
                </tr>';
        }
      ?>
    </tbody>
  </table>
  <?php
  if (@$prod_dev && $contador > 0)
  if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
    $subtotal_dev = $subtotal_dev + $servicio;
    $total_dev = $total_dev + $servicio;
  }
  ?>
  <div class="alert alert-warning col-lg-3">SUBT: <span style="float:right"><?= number_format($subtotal_dev,2) ?></span></div>
  <div class="alert alert-warning col-lg-3">DSC: <span style="float:right"><?= number_format($desc_dev,2) ?></span></div>
  <div class="alert alert-warning col-lg-3">IV: <span style="float:right"><?= number_format($iv_dev,2) ?></span></div>
  <div class="alert alert-warning col-lg-3">TOTAL: <span style="float:right"><?= number_format($total_dev,2) ?></span></div>
</div>
<div class="col-lg-7" >
  <?php
    $devoluciones = DevolucionesProducto::find()->where(['idFactura'=>$model->idCabeza_Factura])->orderBy(['idDevolucion' => SORT_DESC])->all();
    $array_movimiento = array();
    foreach ($devoluciones as $key => $devolucion) {
      $consultar_factun = new Factun();
      $email = '';//buscamos el correo del cliente si es un cliente registrado
      if (@$cliente = Clientes::findOne($model->idCliente)) {
        $email = $cliente->email;
      }
      $array_movimiento[] = $devolucion->idMov;
      $movimiento_cobrar = MovimientoCobrar::findOne($devolucion->idMov);
      if (@$movimiento_cobrar) {
        $ver_dev = Html::a('<span class=""></span>', null, [
              'class'=>'fa fa-paperclip fa-2x',
              'style'=>"cursor:pointer",
              'onclick'=>"ver_devolucion(".$devolucion->idDevolucion.")",
              'title' => 'Ver devolución',
              ]);
        $print_dev = Html::a('<span class=""></span>', ['devoluciones-producto/imprimir_devoluciones', 'id'=>$devolucion->idDevolucion], [
              'class'=>'fa fa-ticket fa-2x',
              'id' => 'pdf',
              'target'=>'_blank',
              'data-pjax' => '0',
              'title' => 'Imprimir devolución',
              ]);
        $pdf = Html::a('<span class=""></span>', ['devoluciones-producto/pdf', 'idFactura'=>$model->idCabeza_Factura, 'idMov'=>$movimiento_cobrar->idMov], [
                            'class'=>'fa fa-puzzle-piece fa-2x',
                            'id' => 'pdf',
                            'target'=>'_blank',
                            'data-pjax' => '0',
                            'title' => 'PDF Nota de crédito',
                            ]);
        $xml = Html::a('<span class=""></span>', ['facturas-dia/xml', 'clave_fe'=>$movimiento_cobrar->clave_mov, 'tipo'=>'doc', 'docum'=>'NC'], [
                              'class'=>'fa fa-file-code-o fa-2x',
                              'id' => 'xml',
                              'data-pjax' => '0',
                              'title' => Yii::t('app', 'Descargar xml de la nota de crédito'),
                            ]);
        $xml_resp = Html::a('<span class=""></span>', ['facturas-dia/xml', 'clave_fe'=>$movimiento_cobrar->clave_mov, 'tipo'=>'res', 'docum'=>'NC'], [
                              'class'=>'fa fa-code fa-2x',
                              'id' => 'xml',
                              'data-pjax' => '0',
                              'title' => Yii::t('app', 'Descargar xml respuesta de hacienda'),
                            ]);
        $print_nc = Html::a('<span class=""></span>', ['devoluciones-producto/imprimir_movimiento_credito', 'id'=>$movimiento_cobrar->idMov], [
                            'class'=>'fa fa-print fa-2x',
                            'id' => 'activity-index-link-report_nc',
                            'data-toggle' => 'titulo',
                            'target'=>'_blank',
                            //'onclick' => 'jsWebClientPrint.print()',
                            'data-pjax' => '0',
                            'title' => 'Imprimir Nota de crédito',
                            ]);
        $go_email = $movimiento_cobrar->estado_hacienda != '01' ? '' : Html::a('<span class=""></span>', null, [
                            'class'=>'fa fa-paper-plane fa-2x',
                            'data-toggle'=>'tooltip',
                            'style'=>"cursor:pointer",
        										'onclick' => 'javascript:enviar_nc_correo('.$model->idCabeza_Factura.', '.$movimiento_cobrar->idMov.')',
                            'title' => 'Enviar Nota de crédito al correo',
                            ]);
        $productos_devueltos = '<br><table width="100%">
        <tr>
        <th>CÓDIGO</th>
        <th>DESCRIPCIÓN</th>
        <th>CANT.DEV</th>
        <th style="text-align:right">PREC.UNIT</th>
        <th style="text-align:right">DESCTO</th>
        <th style="text-align:right">IV</th>
        <th style="text-align:right">TOTAL</th>
        </tr>';
        $productos_dev = DevProductosDevueltos::find()->where(['idDevolucion'=>$devolucion->idDevolucion])->all();
        foreach ($productos_dev as $key => $productos) {
          $detall_fac = DetalleFacturas::findOne($productos->idDetalleFactura);
          $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$detall_fac->codProdServicio])->one();
          $descrip = $detall_fac->codProdServicio != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
          $movimiento_cobrar = MovimientoCobrar::findOne($devolucion->idMov);
          $productos_devueltos .= '
                                  <tr title="'.$descrip.'">
                                    <td>'.$detall_fac->codProdServicio.'</td>
                                    <td>'.substr($descrip,0,20).'</td>
                                    <td>'.$productos->cantidad_dev.'</td>
                                    <td style="text-align:right">'.number_format($detall_fac->precio_unitario,2).'</td>
                                    <td style="text-align:right">'.number_format($productos->descuento,2).'</td>
                                    <td style="text-align:right">'.number_format($productos->iv,2).'</td>
                                    <td style="text-align:right">'.number_format($productos->total,2).'</td>
                                  </tr>
          ';
        }
        $productos_devueltos .= '</table>';
        if ( $empresa->factun_conect == 1 ) {
          $input_email = '<input type="email" value="'.$email.'" class="form-control" id="email_cliente_mov_'.$movimiento_cobrar->idMov.'" placeholder="Dirección email">';
        } else {
          $input_email = '<br>';
        }
        if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
          $servicio_restaurante = '<td width="20%"><span style="float:right">SERVICIO 10%: '.number_format($servicio,2).'</span></td>';
        } else {
          $servicio_restaurante = '<td width="20%"><span style="float:right"></span></td>';
        }
        $contenido = '
                      <div id="contenido_dev'.$devolucion->idDevolucion.'" style="display:none;">
                        <div class="alert alert-info">
                          <div style="height: 280px;width: 100%; overflow-y: auto; ">
                            <strong>
                            <span style="float:right">
                              Emitida el: '.date('d-m-Y | h:i:s A', strtotime($devolucion->fecha_hora)).' por '.$devolucion->usuario.'
                            </span><br>
                            Descripción:</strong> '.$devolucion->descripcion_devolucion.'<br>
                            <span style="float:right">
                            <form class="form-inline">
                            <div class="form-group mx-sm-6 mb-6">
                              '.$input_email.'
                              '.$go_email.'<br><br>
                            </div>
                            </form>
                            </span>
                            <table width="100%">
                              <tr>
                                '.$servicio_restaurante.'
                                <td width="20%"><span style="float:right">Subtotal: '.number_format($devolucion->subtotal,2).'</span></td>
                                <td width="20%"><span style="float:right">Descuento: '.number_format($devolucion->descuento,2).'</span></td>
                                <td width="20%"><span style="float:right">IV: '.number_format($devolucion->impuesto_venta,2).'</span></td>
                                <td width="20%"><span style="float:right">Total: '.number_format($devolucion->total,2).'</span></td>
                              </tr>
                            </table>
                            '.$productos_devueltos.'
                          </div>
                        </div>
                      </div>
        ';
        if ($movimiento_cobrar->estado_hacienda == '01') {
          echo '<div class="alert alert-success">
                  <span style="font-size: 1em; color:#ff6c00;">Devolucion ID '.$devolucion->idDevolucion.'
                  [ MOV ID: '.$movimiento_cobrar->idMov.' (NC) - '.$movimiento_cobrar->mov_el.' ] [ Aceptada por Hacienda ]</span>
                  <span style="float:right">
                    '.$ver_dev.' &nbsp; '.$print_dev.' &nbsp;&nbsp;
                    '.$xml.' &nbsp; '.$xml_resp.' &nbsp; '.$pdf.' &nbsp; '.$print_nc.'
                  </span>
                </div>'.$contenido;
        } elseif ($movimiento_cobrar->estado_hacienda == '03'){
          $load_aux = "<img src='".$load_buton."' style='height: 15px;'>";
          echo '<div class="alert alert-danger">
                  <span style="font-size: 1em; color:#EB0816;">Devolucion ID '.$devolucion->idDevolucion.' [Rechazada por Hacienda]
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <a onclick="re_aplicar_devolucion(this,'.$devolucion->idDevolucion.')" data-loading-text="Solucionando espere... '.$load_aux.'" title="AUXILIAR PARA REENVIAR ESTA NOTA DE CRÉDITO A HACIENDA, si no se resuelve con este auxiliar y la causa fue por una factura previamente rechazada es porque no ha resolvido los problemas que hacienda le notificó al previo rechazo." class="alert-link" style="cursor:pointer;"><i class="fa fa-life-ring fa-2x"></i></a>
                  </span>
                  <span style="float:right">
                    '.$ver_dev.'
                    &nbsp;
                    '.$print_dev.'
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    '.$pdf.'
                    &nbsp;
                    '.$print_nc.'
                  </span>
                </div>'.$contenido;
        } elseif ($movimiento_cobrar->estado_hacienda == '05' || $movimiento_cobrar->estado_hacienda == '00') {
          if ( $empresa->factun_conect == 1 ) {
            $estado = '[ En proceso ]';
          } else {
            $estado = '';
            $pdf = '';
          }
          echo '<div class="alert alert-info">
                  <span style="font-size: 1em; color:#EB9808;">Devolucion ID '.$devolucion->idDevolucion.'
                  [ MOV ID: '.$movimiento_cobrar->idMov.' (NC) - '.$movimiento_cobrar->mov_el.' ] '.$estado.'</span>
                  <span style="float:right">
                    '.$ver_dev.'
                    &nbsp;
                    '.$print_dev.'
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    '.$pdf.'
                    &nbsp;
                    '.$print_nc.'
                  </span>
                </div>'.$contenido;
        } else {
          echo '<div class="alert alert-info">
                  <span style="font-size: 1em; color:#0820EB;">Devolucion ID '.$devolucion->idDevolucion.'
                  [ MOV ID: '.$movimiento_cobrar->idMov.' (NC) - '.$movimiento_cobrar->mov_el.' ] [ Esperando respuesta de Hacienda ]</span>
                  <span style="float:right">
                    '.$ver_dev.'
                    &nbsp;
                    '.$print_dev.'
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    '.$pdf.'
                    &nbsp;
                    '.$print_nc.'
                  </span>
                </div>'.$contenido;
        }
      }

    }//fin foreach
  ?>



</div>
<?php if ($contador > 0) {
  $load = "<img src='".$load_buton."' style='height: 15px;'>";
  ?>
<div class="col-lg-5" >
  <div class="col-lg-12" >
    <label>Detalle devolución:</label><br>
    <textarea class="form-control" rows="2" id="detalle_devolucion" required></textarea><br>
  </div>
  <div class="col-lg-7" >
    <div class="input-group">
      <span class="input-group-addon">
        Nota de crédito por
      </span>
      <input type="text" style="font-family: Fantasy; font-size: 20pt; text-align: center;" class="form-control" id="monto_nota_credito" value="<?= number_format($total_dev,2) ?>" placeholder="Monto NC" disabled>
    </div>
  </div>
  <div class="col-lg-5" >
    <p style="text-align:right">
      <input type="hidden" id="idCabeza_Factura" value="<?= $model->idCabeza_Factura ?>">
      <?= '<a class="btn btn-primary btn-lg" data-loading-text="Enviando a Hacienda '.$load.'" onclick="aplicar_devolucion(this,'.$subtotal_dev.','.$desc_dev.','.$iv_dev.')" >Aplicar Devolución</a>' ?>
    </p>
  </div>
</div>
<?php } ?>
<div class="col-lg-5">
  <div id="historial_correo">
    <div id="notificacion_movimiento"></div>
    <center><h3>Historial de envios por e-mail</h3></center>
    <table class="items table table-sm table-inverse" id="" width="100%">
      <thead class="">
        <tr class="table-warning">
          <th width="20%"><font face="arial" size=4>ID Docto</font></th>
          <th width="40%"><font face="arial" size=4>Dirección</font></th>
          <th width="40%"><font face="arial" size=4>Fecha | Hora | Usuario</font></th>
        </tr>
      </thead>
      <tbody class="" id="">
        <?php
          foreach ($array_movimiento as $value) {
            $email = HistorialEnvioEmail::find()->where(['tipo_documento'=>'NC'])
            ->andWhere('idDocumento = :idDocumento',[':idDocumento'=>$value])->all();
            foreach ($email as $key => $doc_nc) {
              echo '<tr>
                      <td width="20%"><font face="arial" size=2>'.$doc_nc->tipo_documento.' - '.$doc_nc->idDocumento.'</font></td>
                      <td width="40%"><font face="arial" size=2>'.$doc_nc->email.'</font></td>
                      <td width="40%"><font face="arial" size=2>'.date('d-m-Y | h:i:s A', strtotime($doc_nc->fecha_envio)).' | '.$doc_nc->usuario.'</font></td>
                    </tr>
                  ';
            }
          }
          $emails = HistorialEnvioEmail::find()->where(['tipo_documento'=>'FA'])
          ->andWhere('idDocumento = :idDocumento',[':idDocumento'=>$model->idCabeza_Factura])->all();
          foreach ($emails as $key => $doc_fa) {
            echo '<tr>
                    <td width="20%"><font face="arial" size=2>'.$doc_fa->tipo_documento.' - '.$doc_fa->idDocumento.'</font></td>
                    <td width="40%"><font face="arial" size=2>'.$doc_fa->email.'</font></td>
                    <td width="40%"><font face="arial" size=2>'.date('d-m-Y | h:i:s A', strtotime($doc_fa->fecha_envio)).' | '.$doc_fa->usuario.'</font></td>
                  </tr>
                ';
          }
        ?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
</script>
