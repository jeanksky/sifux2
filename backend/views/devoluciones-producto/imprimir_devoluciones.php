<?php
//Clases para usar librería de etiquetas
use yii\helpers\Url;
use backend\models\Empresa;
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
use backend\models\UserSelectedPrinter;
use backend\models\ParallelPortPrinter;
use backend\models\SerialPortPrinter;
use backend\models\NetworkPrinter;
use backend\models\EncabezadoCaja;
use backend\models\DevolucionesProducto;
use backend\models\DevProductosDevueltos;
use backend\models\Clientes;//para obtener el cliente
use backend\models\FormasPago;
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
use backend\models\Funcionario;
use backend\models\MovimientoCobrar;
//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
    $urlParts = parse_url($_SERVER['REQUEST_URI']);
    $empresa_funcion = new Empresa();//Se obtienen datos de la empresa
    if (isset($urlParts['query'])){
        //
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){


            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//busco la empresa para el encabezado

            $nombre_impresora = 'FACTURASNPS';//ORDENNPS
            //$printerName = urldecode($qs['printerName']);
            //Create ESC/POS commands for sample receipt
            $esc = '0x1B'; //ESC byte in hex notation
            $newLine = '0x0A'; //LF byte in hex notation
            //$derecha = $esc . '!' . '0x00';//Establece el margen izquierdo en la columna n (donde n está entre 0 y 255) en el tono de carácter actual.
            $cajon = $esc ."0x700x0";//abrir cajon
            $cmds = $esc . "@"; //Initializes the printer (ESC @)
            //Encabezado empresa-------------------------------------------------------------------
            $cmds .= $newLine . $newLine;
            $cmds .= $esc . '!' . '0x00'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
            $cmds .= $empresa->nombre.$newLine; //Nombre de la empresa
            $cmds .= $empresa->localidad.' - '.$empresa->direccion.$newLine; //Nombre de la empresa
            $cmds .= $empresa->sitioWeb.' / '. $empresa->email.$newLine; //Nombre de la empresa
            $cmds .= $empresa->telefono.' / '. $empresa->fax; //Nombre de la empresa

            $cmds .= $newLine . $newLine.$newLine;

            //Encabezado factura-------------------------------------------------------------------
            $devolucion = DevolucionesProducto::find()->where(['idDevolucion'=>$id])->one();
            $MovimientoCobrar = MovimientoCobrar::find()->where(['idMov'=>$devolucion->idMov])->one();
            $modelcaja = EncabezadoCaja::find()->where(['idCabeza_Factura'=>$devolucion->idFactura])->one();//vuelvo a llamar al modelo actual pero esta ves con las modificaciones que se aplicaron
            $fecha_hora = date('d-m-Y | h:i:s A', strtotime($devolucion->fecha_hora));
            $cmds .= $esc . '!' . '0x00'; //Fuente de caracteres A seleccionado (ESC! 0)
            $cmds .= $esc . '!' . '0x31' .'BALANCE DE DEVOLUCION'.$newLine.$newLine.$newLine;
            $cmds .= $esc . '!' . '0x00';
            if ( $modelcaja->factun_id > 0 ) {
              $cmds .= 'Devolucion ID: ' . $id.' de Factura ID: '.$devolucion->idFactura.$newLine;
              $cmds .= 'NCE: ' . $MovimientoCobrar->mov_el .$newLine. 'Aplicada a la FE/TE: '.$modelcaja->fe.$newLine.$newLine;
              $autorizado_mediante = "Autorizado mediante ".$newLine." Resolucion No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D";
            } else {
              $cmds .= 'Devolucion ID: ' . $id.' (Nota de Crédito ID: '.$MovimientoCobrar->idMov.').'.$newLine.'Aplicada a la Factura ID: '.$devolucion->idFactura.$newLine.$newLine;
              $autorizado_mediante = "Autorizado mediante ".$newLine." Resolucion 11-97 del 05/09/1997 de la D.G.T.D";
            }
            $cmds .= 'Fecha emision: '. $fecha_hora.$newLine.$newLine;
            $cmds .= "CLIENTE:----------------------------------".$newLine;
            if (@$cliente = Clientes::find()->where(['idCliente'=>$modelcaja->idCliente])->one()) {
                $cmds .= "Nombre: ".$cliente->nombreCompleto . $newLine;
                $cmds .= "Telefono: ".$cliente->telefono . $newLine;
                $cmds .= "Direccion: ".$cliente->direccion . $newLine;
            } else {
                $cmds .= $modelcaja->idCliente . $newLine;
            }
            $cmds .= "------------------------------------------". $newLine . $newLine . $newLine . $newLine;
            $cmds .= "_______________________ _______________________". $newLine ."Firma Cliente           Firma Cajero" . $newLine . $newLine ;
            $cmds .= 'Observacion: ' . $devolucion->descripcion_devolucion . $newLine . $newLine . $newLine;

            //Detalle de factura--------------------------------------------------------------------
            $cmds .= 'CANT      DESCRIPCION          PRECIO' . $newLine;
            $products = DevProductosDevueltos::find()->where(['idDevolucion'=>$devolucion->idDevolucion])->all();
            $detallefactura = '';
            foreach($products as $product) {
                $detalle_fac = DetalleFacturas::find()->where(["=",'idDetalleFactura', $product->idDetalleFactura])->one();
                $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$detalle_fac->codProdServicio])->one();
                $descrip = $detalle_fac->codProdServicio != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
                $detallefactura .= new item2("\n+++++++++++++++++++++++++++++++++++\n".$product->cantidad_dev. ' ' . substr($descrip,0,22).'   '.number_format(($detalle_fac->precio_unitario*$product->cantidad_dev),2)."\n     <".$detalle_fac->codProdServicio . ' ** ' . $modelProd->ubicacion.">" , '');
            }

            //Cierre de impresión-------------------------------------------------------------------
            $cmds .= $detallefactura ."------------------------------------------" . $newLine. $newLine;
            $cmds .= new item2("         SUBTOTAL:", number_format($devolucion->subtotal,2));
            $cmds .= new item2("         TOTAL DESCUENTO:", number_format($devolucion->descuento,2));
            $cmds .= new item2("         TOTAL IV:", number_format($devolucion->impuesto_venta,2));
            $cmds .= new item2("         TOTAL A PAGAR:",number_format($devolucion->total,2)). $newLine . $newLine;

            $modelF = Funcionario::find()->where(['username'=>$devolucion->usuario])->one();
            $cmds .= "Emitido por: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2;
            $cmds .= $newLine . $newLine . $newLine;
            $cmds .= $autorizado_mediante;
            $cmds .= $newLine . $newLine . $newLine;
            $cmds .= "GRACIAS POR PREFERIRNOS, ES UN GUSTO ATENDERLE.";
            $cmds .= $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine;
            $cmds .= $esc . "0x69";//cortar papel

            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente
            $cpj->clientPrinter = new InstalledPrinter($nombre_impresora);

            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();
            header('Content-type: application/octet-stream');
            echo $cpj->sendToClient();

            ob_end_flush();
            exit();
        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de etiqueta-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
$webClientPrintControllerAbsoluteURL = Utils::getRoot().$empresa_funcion->getWCP();
$demoPrintCommandsProcessAbsoluteURL = Utils::getRoot().Url::home().'?r=devoluciones-producto/imprimir_devoluciones&id='.$_GET['id'];
echo WebClientPrint::createScript($webClientPrintControllerAbsoluteURL, $demoPrintCommandsProcessAbsoluteURL, Yii::$app->user->identity->last_session_id);

class item2
{
    private $nombre;
    private $precio;
    private $signo;

    public function __construct($nombre = '', $precio = '', $signo = false)
    {
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> signo = $signo;
    }

    public function __toString()
    {
        $rightCols = 3;
        $leftCols = 35;
        if ($this -> signo) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> nombre, $leftCols) ;

        $sign = ($this -> signo ? '$ ' : '');
        $right = str_pad($sign . $this -> precio, $rightCols, ' ', STR_PAD_LEFT);
        return $left.$right."\n";
    }
}
?>
<center><h1>IMPRIMIENDO DOCUMENTO</h1></center>
 <script type="text/javascript">
 $(document).ready(function () {
         //ejecuta la llamada de impresión
         // Inicie WCPP en el lado del cliente para imprimir ...
         jsWebClientPrint.print('sid=<?php echo session_id(); ?>');
     });
  setTimeout("self.close()", 900 );
 </script>
