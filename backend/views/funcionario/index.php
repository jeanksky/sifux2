<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\alert\Alert;
use yii\helpers\ArrayHelper;
use backend\models\Funcionario;
use kartik\widgets\AlertBlock; //para mostrar la alerta
//----------------------------------------------Para la modal de elimacion por permiso admin
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FuncionarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Funcionarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<div class="funcionario-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p style="text-align:right">
        <?= Html::a('Agregar funcionario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <div id="semitransparente">
              <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'idFuncionario','options'=>['style'=>'width:15px']],
        //'identificacion',
        ['attribute' => 'fullName','label'=>'Nombre'],
        // ['attribute' => 'nombre',
        //     'value' => function ($data) {
        //      return $data->nombre." ".$data->apellido1." ".$data->apellido2;
        //  },],

        // ['attribute' => 'apellido1','options'=>['style'=>'width:15%']],
        // ['attribute' => 'apellido2','options'=>['style'=>'width:15%']],
       // 'apellido1',
        //'apellido2',
            //'genero',
        ['attribute' => 'username','options'=>['style'=>'width:14%']],
        // ['attribute' => 'tipoFuncionario','options'=>['style'=>'width:13%']],
        [
              'attribute' => 'tipoFuncionario',
              'options'=>['style'=>'width:13%'],
              'filter'=>Html::activeDropDownList($searchModel, 'tipoFuncionario',
                ArrayHelper::map(Funcionario::find()
                ->orderBy('tipoFuncionario ASC')
                ->asArray()
                ->all(),'tipoFuncionario','tipoFuncionario'),['class' => 'form-control','prompt' => 'Todos'])
            ],
        //'tipoFuncionario',

        //'username',
            //'email:email',
        [
              'attribute' => 'tipo_usuario',
              'options'=>['style'=>'width:14%'],
              'filter'=>Html::activeDropDownList($searchModel, 'tipo_usuario',
                ArrayHelper::map(Funcionario::find()
                ->orderBy('tipo_usuario ASC')
                ->asArray()
                ->all(),'tipo_usuario','tipo_usuario'),['class' => 'form-control','prompt' => 'Todos'])
            ],
        // ['attribute' => 'tipo_usuario','options'=>['style'=>'width:13%']],
        //'tipo_usuario',
            //'estado',

          ['class' => 'yii\grid\ActionColumn', 'options'=>['style'=>'width:6%'],
            'template' => '{view} {update} {delete}',
            'buttons' => [
                        'delete' => function ($url, $model, $key) {

                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                'id' => 'activity-index-link-delete',
                                'class' => 'activity-delete-link',
                                'title' => Yii::t('app', 'Eliminar Funcionario '.$model->fullName),
                                'data-toggle' => 'modal',
                                'data-target' => '#modalDelete',
                                'onclick' => 'javascript:enviaResultado("'.$model->idFuncionario.'")',
                                'href' => Url::to('#'),
                                'data-pjax' => '0',
                                ]);
                        },
                     ]
          ],
        ],
        ]); ?>

</div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un funcionario necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "idFuncionario" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Funcionario', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este funcionario?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
    </div>
