<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Funcionario */

$this->title = 'Actualizar Funcionario: ' . ' ' . $model->nombre . ' ' . $model->apellido1 . ' ' . $model->apellido2;
$subtitle = 'Código: ' . $model->idFuncionario;
$this->params['breadcrumbs'][] = ['label' => 'Funcionarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre . ' ' . $model->apellido1 . ' ' . $model->apellido2, 'url' => ['view', 'id' => $model->idFuncionario]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="funcionario-update">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <h3><?= Html::encode($subtitle) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>



    <?= $this->render('_form', [
        'model' => $model,
        'model_User' =>$model_User,
    ]) ?>

</div>
