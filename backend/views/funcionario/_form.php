<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Funcionario;
use backend\models\UsuariosDerechos;

//use backend\models\PasswordResetRequestForm;
//use backend\models\ResetPasswordForm;
//use backend\models\SignupForm;
use backend\models\ContactForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Funcionario */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
function buscar_funcionario(id) {
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('funcionario/buscar_funcionario') ?>",
      type:"post",
      dataType: 'json',
      data: { 'id' : id },
      beforeSend: function() {
        //$('#genero :radio').removeAttr("checked");
        $('.nombre').val(''); $('.apellido1').val(''); $('.apellido2').val('');
      },
      success: function(data){
        //alert('data.nombreCompleto');
        $('.nombre').val(data.nombre);
        $('.apellido1').val(data.apellido1);
        $('.apellido2').val(data.apellido2);
        $('#genero :radio').filter('[value="' + data.genero + '"]').prop('checked', true);
      },
      error: function(msg, status,err){
          //alert('error 32');
      }
  });
}
</script>
<div class="funcionario-form">
<div class="site-signup">
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Ingrese los datos que se le solicitan en el siguiente formulario.
        </div>
                        <!-- /.panel-heading -->
        <div class="panel-body">

            <div class="col-md-4">

                <?php $form = ActiveForm::begin(); ?>
                <!--?php $form = ActiveForm::begin(['id' => 'form-signup']);  ?-->

                <?php if ($model->isNewRecord) { ?>
                <?= $form->field($model, 'identificacion')->textInput(['maxlength' => 20, 'onchange' => 'javascript:buscar_funcionario(this.value)']) ?>
                <?php } else { ?>
                <?= $form->field($model, 'identificacion')->textInput(['readonly'=>true]) ?>
                <?php } ?>
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => 30, 'class'=>'nombre form-control']) ?>

                <?= $form->field($model, 'apellido1')->textInput(['maxlength' => 15, 'class'=>'apellido1 form-control']) ?>

                <?= $form->field($model, 'apellido2')->textInput(['maxlength' => 15, 'class'=>'apellido2 form-control']) ?>
            </div>
            <div class="col-md-4">

                <!--?= $form->field($model, 'genero')->textInput(['maxlength' => 10]) ?-->
                <div id="genero">
                  <?= $form->field($model, 'genero')->radioList(['Masculino'=>'Masculino','Femenino'=>'Femenino'])?>
                </div>
                <!--?= $form->field($model, 'tipoFuncionario')->textInput(['maxlength' => 12]) ?-->
                <?= $form->field($model, 'tipoFuncionario')->dropDownList(['Sin definir'=>'Seleccione',
                                                                           'Caja'=>'Caja',
                                                                           'Recepción'=>'Recepción',
                                                                           'Contador'=>'Contador',
                                                                           'Administrador'=>'Administrador',
                                                                           'Mecánico'=>'Mecánico',
                                                                           'Otro'=>'Otro'])?>



                <?= $form->field($model, 'username')->textInput(['maxlength' => 20]) ?>
                <!--?= $form->field($model, 'username')->textInput(['maxlength' => 20]) //?-->


                <?= $form->field($model, 'email')->widget(\yii\widgets\MaskedInput::className(), [
                                                                    'name' => 'input-36',
                                                                    'clientOptions' => [
                                                                    'alias' =>  'email'
                                                                     ],
                                                                ]) ?>
            </div>
            <div class="col-md-4">

                <?= $form->field($model, 'telefono')->
                widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99',
                ]) ?>

                <?= $form->field($model, 'tipo_usuario')->radioList(['Administrador'=>'Administrador', 'Operador'=>'Operador'])?>
                <?php
                if ($model->isNewRecord)
                  {
                    $model->estado = '1';
                  } ?>
                <?= $form->field($model, 'estado')->radioList(['1'=>'Activo', '0'=>'Inactivo'])?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
                                <!-- /.panel-body -->
        </div>
                            <!-- /.panel -->
    </div>

</div>


    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
