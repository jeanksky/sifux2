<?php

use yii\helpers\Html;
use backend\models\Clientes;//para obtener el cliente
use backend\models\FacturasDia;
use backend\models\Factun;
use backend\models\MovimientoCobrar;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use kartik\widgets\AlertBlock;
use yii\widgets\Pjax;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load.gif';
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FacturasDiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facturas del Dia';
$this->params['breadcrumbs'][] = $this->title;
//$facturas_ = FacturasDia::find()->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

function isNumberDe(evt) {
      var nav4 = window.Event ? true : false;
      var key = nav4 ? evt.which : evt.keyCode;
      return (key <= 13 || key==46 || (key >= 48 && key <= 57));
  }

  function refrescarGridView(){
      //confirmar_facturas_hacienda();
      $("#GridView").load(location.href+' #GridView',''); //refrescar el div
  }
  setInterval('refrescarGridView()',4000);

  //funcion para obtener el footer de la modal que necesita enviar el valor id de la factura a imprimir
  function agrega_footer(id){
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/footer') ?>",
          type:"post",
          data: { 'id' : id },
          success: function(data){
              $("#footer_modal").html(data);
          },
          error: function(msg, status,err){
           alert('No pasa74');
          }
      });
  }

//carga en la modal la vista de la factura consultada
$(document).on('click', '#activity-index-link-factura', (function() {
  document.getElementById("well-factura").innerHTML="";//tengo que destruir el div de well-create-movimiento para que well-update-movimiento esté limpio para la modal
  $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
  document.getElementById("well-factura").innerHTML='<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //document.getElementById("well-factura").innerHTML=data;
                        $('#well-factura').html(data);
                        //$('.well-factura').html(data);
                        $('#modalFactura').modal();
                    }
                );
            }));

$(document).on('click', '#activity-nota_credito', (function() {
  document.getElementById("movimientos").innerHTML = '<center><img src="<?= $load_buton ?>" style="height: 300px;"></center>';
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('#movimientos').html(data);
                        document.getElementById("movimientos").innerHTML = data;
                        //$('#modalNotacredito').modal();
                    }
                );
            }));

//tooltip de bootstrap color negro
$(document).ready(function(){
    $('[data-toggle="modal"]').tooltip();
});

//Esta funcion me filtra los datos de la tabla (solo por parte de vista, no por base de datos)
   /* $(document).ready(function () {
            (function ($) {
                $('#filtrar').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));
            $('#tabla_lista_facturas').dataTable({
            "sPaginationType" : "full_numbers"//DAMOS FORMATO A LA PAGINACION(NUMEROS)
            });
            $('[data-toggle="modal"]').tooltip();
        });*/
//activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<center><p>ESPERANDO BUSQUEDA</p></center>');
        $("#resultadoBusqueda2").html('<center><p>ESPERANDO BUSQUEDA</p></center>');
    });
//me permite buscar por like los productos a BD la cantidad que sea
    function buscar() {
        var textoBusqueda_cod = document.getElementById("busqueda_cod").value;//$("input#busqueda_cod").val();
        var textoBusqueda_fec = document.getElementById("busqueda_fec").value;//$("input#busqueda_fec").val();
        var textoBusqueda_cli = document.getElementById("busqueda_cli").value;//$("input#busqueda_cli").val();
        var textoBusqueda_est = document.getElementById("busqueda_est").value;  //$("input#busqueda_est").val();Ninguna
        buscar2(textoBusqueda_cod,textoBusqueda_fec,textoBusqueda_cli,textoBusqueda_est);
        if (textoBusqueda_est == 'nin') { textoBusqueda_est = '';}
        if (textoBusqueda_cod == "" && textoBusqueda_fec == "" && textoBusqueda_cli == "" && textoBusqueda_est == "") {
            $("#resultadoBusqueda").html('<center><p>ESPERANDO BUSQUEDA</p></center>');
            $("#resultadoBusqueda2").html('<center><p>ESPERANDO BUSQUEDA</p></center>');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/busqueda_factura') ?>",
                type:"post",
                data: { 'valorBusqueda_cod' : textoBusqueda_cod, 'valorBusqueda_fec' : textoBusqueda_fec, 'valorBusqueda_cli': textoBusqueda_cli, 'valorBusqueda_est': textoBusqueda_est },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 alert('No pasa74');
                }
            });
        }
    }
    //permite hacer una segunda busqueda a la base de datos clasificando los clientes que no no estan registrados
    function buscar2(textoBusqueda_cod,textoBusqueda_fec,textoBusqueda_cli,textoBusqueda_est) {
        if (textoBusqueda_est == 'nin') { textoBusqueda_est = '';}
        if (textoBusqueda_cod == "" && textoBusqueda_fec == "" && textoBusqueda_cli == "" && textoBusqueda_est == "") {
            $("#resultadoBusqueda").html('<center><p>ESPERANDO BUSQUEDA</p></center>');

        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/busqueda_factura_simple') ?>",
                type:"post",
                data: { 'valorBusqueda_cod' : textoBusqueda_cod, 'valorBusqueda_fec' : textoBusqueda_fec, 'valorBusqueda_cli': textoBusqueda_cli, 'valorBusqueda_est': textoBusqueda_est },
                success: function(data){
                    $("#resultadoBusqueda2").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 115');
                }
            });
        }
    }

    //valida la sintacis de la direccion de correo electronico
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    //confirmar facturas a hacienda
    function confirmar_facturas_hacienda() {

      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/confirmar_facturas_hacienda') ?>",
          type:"post",
          data: { },
          beforeSend: function() {
            document.getElementById("estado_comprobacion").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Sincronizando estados de factura con Hacienda...</span></center>';
          },
          success: function(data){
            document.getElementById("estado_comprobacion").innerHTML = data;
            setTimeout(function() {//aparece la alerta en un milisegundo
                $("#estado_comprobacion").fadeIn();
            });
            setTimeout(function() {//se oculta la alerta luego de 4 segundos
                $("#estado_comprobacion").fadeOut();
                confirmar_movimientos_hacienda();
            },5000);
          },
          error: function(msg, status,err){
           alert('Algo no esta bien');
          }
      });
    }

    function confirmar_movimientos_hacienda(){
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('devoluciones-producto/consultar_estados_movimiento') ?>",
          type:"post",
          data: { },
          beforeSend: function() {
            document.getElementById("estado_comprobacion").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Sincronizando estados de movimientos con Hacienda...</span></center>';
          },
          success: function(data){
            document.getElementById("estado_comprobacion").innerHTML = data;
            setTimeout(function() {//aparece la alerta en un milisegundo
                $("#estado_comprobacion").fadeIn();
            });
            setTimeout(function() {//se oculta la alerta luego de 4 segundos
                $("#estado_comprobacion").fadeOut();
            },5000);
          },
          error: function(msg, status,err){ /*alert('Algo no esta bien');*/ }
      });
    }

    //enviar por correo la factura
    function enviar_factura_correo(id) {
    	var confirm_pago = confirm("¿Está seguro de enviar esta factura?");
    	if (confirm_pago == true) {
    	var email_cliente = document.getElementById("email_cliente").value;
    	if (validateEmail(email_cliente)) {
      document.getElementById("notificacion_envio_correo").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Espere, puede tardar un minuto, envío de factura en proceso...</span></center>';
    	$.get("<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/enviar_correo_factura_movimiento') ?>",
    				{ 'tipo' : 'FA', 'id' : id, 'email' : email_cliente, 'id_factun_mov' : '' },
    				 function(notif){
    									document.getElementById("notificacion_envio_correo").innerHTML = notif;
    									setTimeout(function() {//aparece la alerta en un milisegundo
    											$("#notificacion_envio_correo").fadeIn();
    									});
    									setTimeout(function() {//se oculta la alerta luego de 4 segundos
    											$("#notificacion_envio_correo").fadeOut();
    									},5000);
    							}
    					);
    				} else {
    					document.getElementById("notificacion_envio_correo").innerHTML = '<br><center><span class="label label-danger" style="font-size:15pt;">Verifique que la dirección sea correcta.</span></center>';
    					setTimeout(function() {//aparece la alerta en un milisegundo
    							$("#notificacion_envio_correo").fadeIn();
    					});
    					setTimeout(function() {//se oculta la alerta luego de 4 segundos
    							$("#notificacion_envio_correo").fadeOut();
    					},5000);
    				}
    	}
    }

    //enviar por correo la nota de credito
    function enviar_factura_correo_mov(id, factun_id_mov, tipo) {
    	var confirm_pago = confirm("¿Está seguro de enviar este movimiento?");
    	if (confirm_pago == true) {
    	var email_cliente = document.getElementById("email_cliente_mov").value;
    	if (validateEmail(email_cliente)) {
      document.getElementById("notificacion_movimiento").innerHTML = '<br><br><center><span class="label label-info" style="font-size:15pt;">Espere, puede tardar un minuto, envío de movimiento en proceso...</span></center>';
    	$.get("<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/enviar_correo_factura_movimiento') ?>",
    				{ 'tipo' : tipo, 'id' : id, 'email' : email_cliente, 'id_factun_mov' : factun_id_mov },
    				 function(notif){
    									document.getElementById("notificacion_movimiento").innerHTML = notif;
    									setTimeout(function() {//aparece la alerta en un milisegundo
    											$("#notificacion_movimiento").fadeIn();
    									});
    									setTimeout(function() {//se oculta la alerta luego de 4 segundos
    											$("#notificacion_movimiento").fadeOut();
    									},5000);
    							}
    					);
    				} else {
    					document.getElementById("notificacion_movimiento").innerHTML = '<br><center><span class="label label-danger" style="font-size:15pt;">Verifique que la dirección sea correcta.</span></center>';
    					setTimeout(function() {//aparece la alerta en un milisegundo
    							$("#notificacion_movimiento").fadeIn();
    					});
    					setTimeout(function() {//se oculta la alerta luego de 4 segundos
    							$("#notificacion_movimiento").fadeOut();
    					},5000);
    				}
    	}
    }

    //agrego un movimiento nota de credito o debito usando ajax
    function agrega_nc(tipo, idCa, btn_this){
      if(confirm("Está seguro de crear este movimiento")){
        var concepto = '';
      	var monto = '';
        if (tipo=='nc') {
          concepto = document.getElementById('concepto_nc').value;
        	monto = document.getElementById('monto_nc').value;
        } else {
          //concepto = document.getElementById('concepto_nd').value;
        	//monto = document.getElementById('monto_nd').value;
        }
      	if (concepto == '' || monto == '') {
      		alert('Por favor no deje campos vacios');
      	} else {
          var $btn = $(btn_this).button('loading')
          // simulating a timeout
          setTimeout(function () {
              $btn.button('reset');
          }, 5000);
      	$.ajax({
                  url: "<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/agrega_nota') ?>",
                  type: "post",
                  dataType: 'json',
                  data: { 'tipo' : tipo, 'idCa' : idCa, 'concepto' : concepto, 'monto': monto },
                  success: function(data_mov){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('facturas-dia/movimientos') ?>" ,
                    { 'id' : idCa },
                    function( data_movimiento ) { $('#movimientos').html(data_movimiento); });

                    //muestro un mensaje
                    document.getElementById("notificacion_movimiento").innerHTML = data_mov.notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notificacion_movimiento").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notificacion_movimiento").fadeOut();
                    },5000);
                  },
                  error: function(msg, status,err){
                   alert('No pasa 87');
                  }
              });
      	}
      }
    }

    //agrego nota de credito electronica usando ajax
    function agrega_movimiento(tipo, idCa, btn_this){
      if(confirm("Está seguro de crear este movimiento")){
        var concepto = '';
      	//var monto = '';
        if (tipo=='nc') {
          concepto = document.getElementById('concepto_nc').value;
        	//monto = document.getElementById('monto_nc').value;
        } else {
          //concepto = document.getElementById('concepto_nd').value;
        	//monto = document.getElementById('monto_nd').value;
        }
      	if (concepto == ''/* || monto == ''*/) {
      		alert('Por favor no deje campos vacios');
      	} else {
          var $btn = $(btn_this).button('loading')
          // simulating a timeout
          setTimeout(function () {
              $btn.button('reset');
          }, 60000);
      	$.ajax({
                  url: "<?php echo Yii::$app->getUrlManager()->createUrl('facturas-dia/agrega_movimiento') ?>",
                  type: "post",
                  dataType: 'json',
                  data: { 'tipo' : tipo, 'idCa' : idCa, 'concepto' : concepto/*, 'monto': monto*/ },
                  success: function(data_mov){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('facturas-dia/movimientos') ?>" ,
                    { 'id' : idCa },
                    function( data_movimiento ) { $('#movimientos').html(data_movimiento); });
                    if (data_mov.email != '') {
                      $.get( "<?= Yii::$app->getUrlManager()->createUrl('facturas-dia/enviar_correo_factura_movimiento') ?>",
                      {'tipo' : tipo, 'id' : idCa, 'email' : data_mov.email , 'id_factun_mov' : data_mov.id_factun},
                        function( data ) {  });
                    }
                    //muestro un mensaje
                    document.getElementById("notificacion_movimiento").innerHTML = data_mov.notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notificacion_movimiento").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notificacion_movimiento").fadeOut();
                    },5000);
                  },
                  error: function(msg, status,err){
                   alert('No pasa 87');
                  }
              });
      	}
      }
    }

</script>
<style>
    #modalFactura .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="facturas-dia-index">
  <?= AlertBlock::widget([
          'type' => AlertBlock::TYPE_ALERT,
          'useSessionFlash' => true,
          'delay' => 35000
      ]);?>
  <ul class="nav nav-tabs nav-justified">
    <li class="active" role="presentation" ><a href="#dia" data-toggle="tab">Consulta de facturas del día</a></li>
    <li role="presentation"><a href="#todas" data-toggle="tab">Consulta de todas las facturas</a></li>
  </ul>
  <div class="table-responsive tab-content">
    <div class="tab-pane fade in active" id="dia">
    <center><h1>Facturas Emitidas Hoy</h1></center>
    <div class="well col-lg-12">
      <center>
        <span class="label label-warning" style="font-size:15pt;">Atento! Las facturas electrónicas rechazadas por Hacienda deben ser rebocadas con una nota de crédito.</span>
        <p><br>IMPORTANTE: Antes de hacer una nota de crédito por rechazo de Hacienda considere la nota de error consultando la factura y resuelva primero el problema por rechazo, de lo contrario la nota de crédito será rechazada igualmente.</p>
      </center>
      <?php
      echo Html::a('<i class="fa fa-mixcloud" aria-hidden="true"></i> Confirmar estados de factura en hacienda', null, [
          'class'=>'btn',
          'data-toggle'=>'tooltip',
          'onclick' => 'javascript:confirmar_facturas_hacienda()',
          'title'=>'Esta opción recorre todas las facturas electrónicas para comprobar el estado de la factura en hacienda'
        ]).'<div id="estado_comprobacion"></div>';
      ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]);

    ?>

    <p>
        <!-- ?= Html::a('Create Facturas Dia', ['create'], ['class' => 'btn btn-success']) ? -->
    </p>
    <div style="height: 1000px;width: 100%; overflow-y: auto; ">
    <div id="GridView">
            <div align="right">
                    <label>Registro por pantalla: </label>
                    <?php echo \nterms\pagesize\PageSize::widget(); ?>
                </div>
    <?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <!--?php  $dataProvider->pagination->defaultPageSize = 8; ?-->
    <?php Pjax::begin(); ?>
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
          /*'rowOptions' => function($model){
            if ($model->factun_id > -1) {
              $consultar_factun = new Factun();
              $api_consultar = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Consultar/'.$model->factun_id;
              $result_conslt = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_consultar, 'PUT'));
              foreach ($result_conslt->data as $key => $value) {
                if ($key == 'estado_hacienda') { $estado_hacienda = $value; }//con esto obtenemos el numero de factura de hacienda para atribuircelo al archivo
                }
              if ($estado_hacienda=='01') return ['style'=>'background-color:#8FF3BA'];
              if ($estado_hacienda=='02') return ['style'=>'background-color:#94D8F3'];
              if ($estado_hacienda=='03') return ['style'=>'background-color:#F7819F'];
            }
          },*/
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],

              //'idCabeza_Factura',
              [
                'attribute' => 'idCabeza_Factura',
                'options'=>['style'=>'width:7%'],
              ],
              [
                'attribute'=>'fe',
                'options'=>['style'=>'width:25%'],
                'label' => 'Factura/tiquete electrónico',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                  if ($model->factun_id > -1) {
                    if ($model->estado_hacienda=='10'){
                      if($model->estado_detalle == '-'){
                        return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-default" style="font-size:12pt;">En cola...</span>';
                      } else {
                        return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-danger" style="font-size:12pt;">Presenta un error</span><br>'.$model->estado_detalle.'<br><br><center>(Cuando halla solucionado el problema seleccione "Confirmar estados de factura en hacienda" para comprovar si ya no presenta errores)</center>';
                      }
                    }
                    elseif ($model->estado_hacienda=='00') return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-info" style="font-size:12pt;">Creada</span>';
                    elseif ($model->estado_hacienda=='01') return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-success" style="font-size:12pt;">Aceptado por Hacienda</span>';
                    elseif ($model->estado_hacienda=='02') return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-primary" style="font-size:12pt;">Recibida por Hacienda</span>';
                    elseif ($model->estado_hacienda=='03') return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-danger" style="font-size:12pt;">Rechazada por Hacienda</span>';
                    elseif ($model->estado_hacienda=='04') return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-info" style="font-size:12pt;">Enviada a Hacienda</span>';
                    elseif ($model->estado_hacienda=='05') {
                      return $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-warning" style="font-size:12pt;">EN ESPERA</span>';
                    }
                  } else {
                    return ' &nbsp;&nbsp;&nbsp;<span class="label label-default" style="font-size:12pt;">NO ES FACTURA ELECTRÓNICA</span>';
                  }
                },
              ],
              //'fecha_inicio',
              //'fecha_final',
                      [
                          'attribute' => 'tbl_clientes.nombreCompleto',
                          'label' => 'Clientes',
                          'format' => 'raw',
                          'value' => function ($model, $key, $index, $grid) {
                            $suma_monto_ab = MovimientoCobrar::find()
                            ->where(['idCabeza_Factura'=>$model->idCabeza_Factura])
                            ->andWhere("tipmov = :tipmov", [":tipmov"=>'AB'])
                            ->sum('monto_movimiento');
                            $suma_monto_nc = MovimientoCobrar::find()
                            ->where(['idCabeza_Factura'=>$model->idCabeza_Factura])
                            ->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])
                            ->sum('monto_movimiento');
                            $suma_monto_nd = MovimientoCobrar::find()
                            ->where(['idCabeza_Factura'=>$model->idCabeza_Factura])
                            ->andWhere("tipmov = :tipmov", [":tipmov"=>'ND'])
                            ->sum('monto_movimiento');
                            $nd = $fr = $nc = '';
                            $saldo_fac = $model->total_a_pagar + $suma_monto_nd - /*$suma_monto_ab -*/ $suma_monto_nc;
                            if ($suma_monto_nc > 0) {
                              $nc = ' <span class="label label-warning" style="font-size:12pt;">Nota de Crédito</span> ';
                            }
                            if ($suma_monto_nd > 0) {
                              $nd = ' <span class="label label-info" style="font-size:12pt;">Nota de Débito</span> ';
                            }
                            if ($saldo_fac <= 0) {
                              $fr = ' <span class="label label-danger" style="font-size:12pt;">Factura revocada</span> ';
                            }
                              if($cliente = Clientes::findOne($model->idCliente))
                              {
                                  return Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$model->idCliente).$nc.$nd.$fr;
                              }else{
                                  if ($model->idCliente=="")
                                  return 'Cliente no asignado'.$nc.$nd.$fr;
                                  else
                                  return $model->idCliente.$nc.$nd.$fr;
                              }
                          },
              ],
              //'idOrdenServicio',
              // 'porc_descuento',
              // 'iva',
              // 'total_a_pagar',
               //'estadoFactura',
               [
                 'attribute' => 'total_a_pagar',//'format' => ['decimal',2],
                 'value' => function ($model, $key, $index, $grid) {
                   if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
                     $servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
                     return number_format($model->total_a_pagar+$servicio,2);
                   } else {
                     return number_format($model->total_a_pagar,2);
                   }
                 },
                 'options'=>['style'=>'width:7%'],
               ],
               [
                'attribute' => 'estadoFactura',
                //'label' => 'Estado Factura / Hacienda dice',
                'value' => function ($model, $key, $index, $grid) {
                    return $model->estadoFactura;
                },
                'options'=>['style'=>'width:8%'],
               ],
              // 'tipoFacturacion',
              // 'codigoVendedor',
              // 'subtotal',

              ['class' => 'yii\grid\ActionColumn',
              'template' => '{view}  {notacredito}  {xml}  {xml_res}  {pdf}',
              'buttons' => [
                  'view' => function ($url, $model, $key) {

                          return Html::a('', ['#'], [
                                  'class'=>'fa fa-eye fa-2x',
                                  'id' => 'activity-index-link-factura',
                                  'data-toggle' => 'modal',
                                  'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
                                  'data-target' => '#modalFactura',
                                  'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $model->idCabeza_Factura]),
                                  'data-pjax' => '0',
                                  'title' => Yii::t('app', 'Procede a mostrar la factura')]);
                      },
                  'notacredito' => function ($url, $model, $key) {
                    if ($model->factun_id > -1) {
                          return '&nbsp;&nbsp;'.Html::a('', ['#'], [
                                  'class'=>'fa fa-file-text-o fa-2x',
                                  'id' => 'activity-nota_credito',
                                  'data-toggle' => 'modal',
                                  //'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
                                  'data-target' => '#modalNotacredito',
                                  'data-url' => Url::to(['facturas-dia/movimientos', 'id' => $model->idCabeza_Factura]),
                                  'data-pjax' => '0',
                                  'title' => Yii::t('app', 'Consulta y creacion de un movimientos electrónicos')]);
                                } else {
                                  if ($model->estadoFactura == 'Cancelada'){
                                    return '&nbsp;&nbsp;'.Html::a('', ['#'], [
                                            'class'=>'fa fa-file-text-o fa-2x',
                                            'id' => 'activity-nota_credito',
                                            'data-toggle' => 'modal',
                                            //'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
                                            'data-target' => '#modalNotacredito',
                                            'data-url' => Url::to(['facturas-dia/movimientos', 'id' => $model->idCabeza_Factura]),
                                            'data-pjax' => '0',
                                            'title' => Yii::t('app', 'Consulta y creacion de notas de credito (SOLO PARA REGIMEN SIMPLIFICADO - 25514-H)')]);
                                  }

                                }
                      },
                  'xml' => function ($url, $model, $key) {
                    if ($model->factun_id > -1) {
                          return  '&nbsp;&nbsp;'.Html::a('<span class=""></span>', ['facturas-dia/xml', 'clave_fe'=>$model->clave_fe, 'tipo'=>'doc', 'docum'=>'FA'], [
                          'class'=>'fa fa-file-code-o fa-2x',
                          'id' => 'xml',
                          'data-pjax' => '0',
                          'title' => Yii::t('app', 'Descargar xml'),
                          ]);
                          } else { return ''; }
                      },
                  'xml_res' => function ($url, $model, $key){
                      if ($model->factun_id > -1) {
                        return '&nbsp;&nbsp;'.$xml_resp = Html::a('<span class=""></span>', ['facturas-dia/xml', 'clave_fe'=>$model->clave_fe, 'tipo'=>'res', 'docum'=>'FA'], [
                                              'class'=>'fa fa-code fa-2x',
                                              'id' => 'xml_res',
                                              'data-pjax' => '0',
                                              'title' => Yii::t('app', 'Descargar xml respuesta de hacienda'),
                                            ]);
                      } else { return ''; }
                  },
                  'pdf' => function ($url, $model, $key) {
                          return  '&nbsp;&nbsp;'.Html::a('<span class=""></span>', ['facturas-dia/pdf', 'idFactura'=>$model->idCabeza_Factura, 'id_factun_mov'=>''], [
                          'class'=>'fa fa-file-pdf-o fa-2x',
                          'id' => 'pdf',
                          'target'=>'_blank',
                          'data-pjax' => '0',
                          'title' => Yii::t('app', 'Convertir a PDF'),
                          ]);
                      },
              ],
              'options'=>['style'=>'width:13%'],
            ],
          ],
      ]); ?>
      <?php Pjax::end(); ?>
    </div>
    </div>
    </div>

    <div class="tab-pane fade" id="todas"><br>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 style="text-align:center">Todas las facturas</h2>
            <p style="text-align:center">Busca y accede a todas Facturas que se han realizado.</p>


            <!--div class="input-group">
              <span class="input-group-addon">Buscar</span>
              <input id="filtrar" type="text" class="form-control" placeholder="Ingrese una busqueda, ejemplo: 12-08-2016">
            </div-->
            <?php
                echo '<div class="col-lg-3">
                      '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'N. FACTURA', 'onKeyUp' => 'javascript:buscar()']).'
                      </div>
                      <div class="col-lg-3">
                      '. yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName', 'options' => ['class'=>'form-control', 'placeholder'=>'FECHA', 'id'=>'busqueda_fec', 'onchange'=>'javascript:buscar()']]) .'
                      </div>
                      <div class="col-lg-3">

                      '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cli', 'class' => 'form-control', 'placeholder' =>'CLIENTE', 'onKeyUp' => 'javascript:buscar()']).'&nbsp;
                      </div>
                      <div class="col-lg-3">

                      '.
                        Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => ["nin" => "Ninguna", "Pendiente" => "Pendiente", "Caja" => "Caja", "Crédito Caja" => "Crédit.Caja", "Crédito" => "Crédito", "Cancelada" => "Cancelada", "Anulada" => "Anulada"],
                                'options' => [
                                    'placeholder' => 'ESTADO',
                                    'id'=>'busqueda_est',
                                    'onchange' => 'javascript:buscar()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                            ])
                      .'&nbsp;
                      </div> ';
            ?>
            <h4 style="text-align:center">Facturas de clientes registrados</h4>

        </div>
        <div id="conten_pro">
                <?php
                //http://www.programacion.com.py/tag/yii
                    //echo '<div class="grid-view">';

                    echo '<table class="items table table-striped" id="tabla_lista_facturas" width="100%">';
                    echo '<thead>';
                    echo '<tr>
                              <th width="38%">No. FAC</th>
                                <th width="10%">FECHA</th>
                                <th width="20%"><center>CLIENTES</center></th>
                                <th width="10%"><center>ESTADO FACTURA</center></th>
                                <th width="22%" class="actions button-column">&nbsp;</th>
                            </tr>';
                    echo '</thead>';
                    echo '</table>';
                    echo '<div style="height: 200px;width: 100%; overflow-y: auto; ">';
                    echo '<div id="resultadoBusqueda"></div>';

                echo '</div>';
                ?>
        </div>
        <div class="panel-heading">
            <h4 style="text-align:center">Facturas de clientes no registrados</h4>
        </div>
        <div id="conten_pro">
            <?php
            echo '<table class="items table table-striped" id="tabla_lista_facturas2" width="100%">';
                    echo '<thead>';
                    echo '<tr>
                              <th width="38%">No. FAC</th>
                                <th width="10%">FECHA</th>
                                <th width="20%">CLIENTES</th>
                                <th width="10%">ESTADO FACTURA</th>
                                <th width="22%" class="actions button-column">&nbsp;</th>
                            </tr>';
                    echo '</thead>';
                    echo '</table>';
                echo '<div style="height: 250px;width: 100%; overflow-y: auto; ">';
                echo '<div id="resultadoBusqueda2"></div>';
                echo '</div>';
            ?>
        </div>
    </div>
</div>
</div>

    <?php
         Modal::begin([//modal que me muestra la factura seleccionada
                'id' => 'modalFactura',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Factura</h4></center>',
                'footer' => '<div id="footer_modal"></div>',
            ]);
            echo "<div id='well-factura'></div>";

            Modal::end();
    ?>
    <?php
         Modal::begin([//modal que me muestra la factura seleccionada
                'id' => 'modalNotacredito',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Movimientos (ND: Notas de débito / NC: Nota de crédito)</h4></center>',
                'footer' => '<div id="notificacion_movimiento"></div><a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
            ]);
            echo "
            <div id='movimientos'></div>
            ";
            Modal::end();
    ?>
</div>
