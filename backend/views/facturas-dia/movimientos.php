<?php
use backend\models\MovimientoCobrar;
use backend\models\Clientes;
use yii\helpers\Html;
use backend\models\Factun;
 ?>
 <script type="text/javascript">


 </script>

<div style="height: 100%;width: 100%; overflow-y: auto; ">

  <!--div class="col-lg-3">
    <ul class="nav nav-tabs nav-justified">
      <li role="presentation" ><a href="#debito" data-toggle="tab">Not.Deb</a></li>
      <li class="active" role="presentation"><a href="#credito" data-toggle="tab">Not.Cre</a></li>
    </ul>
    <div class="table-responsive tab-content">
      <div class="tab-pane fade" id="debito">
        <h3>En Mantenimiento</h3>
      </div>
      <div class="tab-pane fade in active" id="credito">
        <?php //if ($model->factun_id > -1) { ?>
        <br><center><label>Revocar factura con Nota de Crédito</label></center>
        <?php //} else { ?>
        <br><center><label>Nota de Crédito</label></center>
        <?php  //} ?>
        <br><label>Concepto:</label>
        <textarea class="form-control" rows="4" id="concepto_nc" required></textarea>
        <br>

          <?php
      /*    $suma_monto_movimiento = MovimientoCobrar::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->sum('monto_movimiento');
          if ($suma_monto_movimiento > 0) {
            if($model->factun_id > -1){//factura electronica
              echo '<center>Esta Factura ya cuenta con al menos un movimiento.</center>';
            } else { //factura normal
              echo '<label>Monto: </label><br>'.
                Html::input('text', '', '',
                ['size' => '8', 'class' => 'form-control', 'id' => 'monto_nc',
                'placeholder' =>' Monto', 'readonly' => false, 'onchange' => '',
                "onkeypress"=>"return isNumberDe(event)"]);
              echo '<br><p style="text-align: right;">'.
              Html::a('<i class="glyphicon glyphicon-plus"></i> Crear nota de Crédito', '#', [
                  'class'=>'btn btn-success',
                  'id'=>'btn_nc',
                  'data-loading-text'=>' Espere...',
                  'onclick'=>'agrega_nc("nc",'.$model->idCabeza_Factura.',this)',
                  'title'=>'Agrega una nueva nota de crédito a la factura'
                  ]).'</p>';
            }
          } else {
            if($model->factun_id > -1){//Factura electronica
            echo '<p style="text-align: right;">'.
            Html::a('<i class="glyphicon glyphicon-plus"></i> Revocar factura', '#', [
                'class'=>'btn btn-warning',
                'id'=>'btn_nc',
                'data-loading-text'=>' Espere...',
                'onclick'=>'agrega_movimiento("nc",'.$model->idCabeza_Factura.',this)',
                'title'=>'Agrega una nueva nota de crédito a la factura'
                ]).'</p>';
            } else { //factura normal
              echo '<label>Monto: </label><br>'.
                Html::input('text', '', '',
                ['size' => '8', 'class' => 'form-control', 'id' => 'monto_nc',
                'placeholder' =>' Monto', 'readonly' => false, 'onchange' => '',
                "onkeypress"=>"return isNumberDe(event)"]);
              echo '<br><p style="text-align: right;">'.
              Html::a('<i class="glyphicon glyphicon-plus"></i> Crear nota de Crédito', '#', [
                  'class'=>'btn btn-success',
                  'id'=>'btn_nc',
                  'data-loading-text'=>' Espere...',
                  'onclick'=>'agrega_nc("nc",'.$model->idCabeza_Factura.',this)',
                  'title'=>'Agrega una nueva nota de crédito a la factura'
                  ]).'</p>';
            }
          }
*/
          ?>
      </div>
    </div>
  </div-->
  <div class="col-lg-12">
  <?php
  $model_m_cobrar = new MovimientoCobrar();
  $suma_monto_nc = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'NC'])->sum('monto_movimiento');
  $suma_monto_nd = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->andWhere("tipmov = :tipmov", [":tipmov"=>'ND'])->sum('monto_movimiento');
  $saldo_fac = $model->total_a_pagar - $suma_monto_nc + $suma_monto_nd;
  $cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
  $email_cliente = @$cliente ? $cliente->email : '';
  $cliente = @$cliente ? $cliente->nombreCompleto : $model->idCliente;
    echo '<font size=5 >ID FACTURA: '.$model->idCabeza_Factura.'</font>
    <font style="float:right" size=4 >Monto inicial: <strong>'.number_format($model->total_a_pagar,2).'</strong></font><br>
    <font style="float:right" size=5 color="#8B8FCF"><strong>Monto final: '.number_format($saldo_fac,2).'</strong></font><br><br>
    <font style="float:right" size=4 color="#28B463">'.$cliente.'</font>';
    echo '<div class="col-xs-1">'.Html::input('hidden', '', $email_cliente, ['size' => '30', 'id' => 'email_cliente_mov', 'class' => 'form-control input-sm', 'placeholder' =>' Dirección email']).'</div>';
      $movimientofactura = $model_m_cobrar->find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->orderBy(['idMov' => SORT_DESC])->all();
      echo '<table class="items table table-striped">';
      echo '<thead>';
      printf('<tr><th>%s</th>
              <th>%s</th>
              <th><center>%s</center></th>
              <th><center>%s</center></th>
              <th><center>%s</center></th>
              <th><center>%s</center></th>
              ',
                      'ID',
                      'Fecha/hora',
                      'Tipo',
                      'Concepto',
                      'Monto',
                      'Saldo.FA'
                      );
      echo '</thead>';
      if($movimientofactura) {
      echo '<tbody>';
        foreach($movimientofactura as $position => $movi) {
          $envi_correo = Html::a('', null, [
                                  'class'=>'fa fa-paper-plane fa-2x',
                                  'id' => 'activity-index-link-agregacredpen',
                                  'data-toggle'=>'tooltip',
              										'onclick' => 'javascript:enviar_factura_correo_mov('.$movi['idCabeza_Factura'].','.$movi['factun_id_mov'].',"'.$movi['tipmov'].'")',
                                  'title'=>'Enviar movimiento al correo']);
          $xml = Html::a('<span class=""></span>', ['facturas-dia/xml', 'factun_id'=>$movi['factun_id_mov'], 'tipo'=>'doc', 'docum'=>'NC'], [
            'class'=>'fa fa-file-code-o fa-2x',
            'id' => 'xml',
            'data-pjax' => '0',
            'title' => Yii::t('app', 'Descargar xml'),
          ]);
          $pdf = Html::a('<span class=""></span>', ['facturas-dia/pdf', 'idFactura'=>$movi['idCabeza_Factura'], 'id_factun_mov'=>$movi['factun_id_mov']], [
                              'class'=>'fa fa-file-pdf-o fa-2x',
                              'id' => 'pdf',
                              'target'=>'_blank',
                              'data-pjax' => '0',
                              'title' => Yii::t('app', 'Convertir a PDF'),
                              ]);
          $accion_mov = '';
          $estado_hacienda = $movi['tipmov']=='AB' ? '' : '<span class="label label-success" style="font-size:12pt;">Aceptado por Hacienda</span>';
          if($movi['factun_id_mov'] > -1 && $movi['estado_hacienda'] != '01'){
            $accion_mov = $envi_correo . '<br><br>' . $xml . '<br><br>' . $pdf;
            $consultar_factun = new Factun();
            $api_consultar = 'https://'.Yii::$app->params['url_api'].'/api/v2/Documento/Consultar/'.$movi['clave_mov'];
            $result_conslt = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_consultar, 'PUT'));
            $estado_hacienda = $estado_detalle = '';
            foreach ($result_conslt->data as $key => $value) {
                if ($key == 'estado_hacienda') { $estado_hacienda = $value; }//con esto obtenemos el numero de factura de hacienda para atribuircelo al archivo
              }
              foreach ($result_conslt->mensajes as $key => $value) {
                if ($key == 'detalle') { $estado_detalle = $value; }//con esto obtenemos el numero de factura de hacienda para atribuircelo al archivo
              }
            if ($estado_hacienda=='01') $estado_hacienda = $movi['tipmov']=='AB' ? '' : '<span class="label label-success" style="font-size:12pt;">Aceptado por Hacienda</span>';
            elseif ($estado_hacienda=='02') $estado_hacienda = '<span class="label label-primary" style="font-size:12pt;">Recibida por Hacienda</span>';
            elseif ($estado_hacienda=='03') $estado_hacienda = '<span class="label label-danger" style="font-size:12pt;">Rechazada por Hacienda</span>'.'<br><br>Nota: '.$estado_detalle;
            elseif ($estado_hacienda=='04') $estado_hacienda = '<span class="label label-info" style="font-size:12pt;">Enviado a Hacienda</span>';
            elseif ($estado_hacienda=='05') {
              $api_facturar = 'https://'.Yii::$app->params['url_api'].'/api/v1/Documento/Facturar/'.$movi['factun_id_mov'];
              $facturar = json_decode($consultar_factun->ConeccionFactun_sin_pan($api_facturar, 'PUT'));
              $estado_hacienda = $model->fe.' &nbsp;&nbsp;&nbsp;<span class="label label-warning" style="font-size:12pt;">EN ESPERA</span>';
            }
          }
          $color_letra = '#34495E';
          if ($movi['tipmov']=='AB') {
            $color_letra = '#95A5A6';
          }
          printf('<tr style="color:'.$color_letra.';">
                    <td><font face="arial" size=2>%s</font></td>
                    <td><font face="arial" size=2>%s</font></td>
                    <td><font face="arial" size=2>%s</font></td>
                    <td><font face="arial" size=2>%s</font></td>
                    <td align="right"><font face="arial" size=2><strong>%s</strong></font></td>
                    <td align="right"><font face="arial" size=2>%s</font></td>
                  </tr>',
              $movi['idMov'],
              $movi['fecmov'],
              $movi['tipmov'],
              $movi['concepto'],
              number_format($movi['monto_movimiento'],2),
              number_format($movi['saldo_pendiente'],2)//,
              //$movi['tipmov'] != 'AB' ? $accion_mov : ''
            );
            echo "<tr><td colspan='6'><center>".$estado_hacienda."</center></td></tr>";
        }

      echo '</tbody>';
    } else {
      echo '<tbody>';
      echo "<tr><td colspan='7'><center>Sin movimientos</center></td></tr>";
      echo '</tbody>';
    }
      echo '</table>';
  ?>
  </div>

</div>
