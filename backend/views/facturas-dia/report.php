<?php
$style_imprime_fact = \Yii::$app->request->BaseUrl.'/css/style_imprime_fact.css';
use backend\models\Empresa;
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
use backend\models\Clientes;
use backend\models\FormasPago;
use backend\models\Factun;

$empresaimagen = new Empresa();
$empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
$products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $model->idCabeza_Factura])->all();
$detalle_factura = '';
if($products) {
  foreach($products as $position => $product) {
    $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
    $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
    $detalle_factura .= '<tr>
      <td class="no">'.$product['cantidad'].'</td>
      <td class="cod" style="font-size: 0.8em;">'.$product['codProdServicio'].'</td>
      <td class="desc" style="font-size: 0.8em;">'.$descrip.'</td>
      <td class="unit" style="font-size: 0.8em;">'.number_format($product['precio_unitario'], 2).'</td>
      <td class="qty" style="font-size: 0.8em;">'.number_format($product['descuento_producto'],2).'</td>
      <td class="unit" style="font-size: 0.8em;">'.number_format($product['subtotal_iva'],2).'</td>
      <td class="total">'.number_format(($product['precio_unitario']*$product['cantidad']),2).'</td>
    </tr>';
  }
}

$info_cliente = $info_invoice = $tipo_documento = '';
if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
    $tipo_documento = 'FE';
    $info_cliente = '
    <h2 class="name">'.$cliente->nombreCompleto.'</h2>
    <div class="address">'.$cliente->direccion.'</div>
    <div class="email">'.$cliente->celular.' / '.$cliente->telefono.'<br>'.$cliente->email.'</div>';
} else{
  $tipo_documento = 'TE';
    $info_cliente = '<h2 class="name">'.$model->idCliente.'</h2>';
}
$tipo_pago = $feven = '';
if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
    $tipo_pago = $modelP->desc_forma;
    if ($model->estadoFactura!='Crédito') {
      $feven = $model->fecha_final;
    }
    else {
      $feven = $model->fecha_vencimiento;
    }
}
$id_factun = $model->factun_id;
if ($id_factun == -1) {
  $info_invoice = '
  <div class="date">Fecha de la factura: '.$model->fecha_inicio.'</div>
  <div class="date">Fecha de vencimiento: '.$feven.'</div>';
} else {
  $consultar_factun = new Factun();
  $clave = $model->clave_fe;
  $numero_factura = $model->fe;

  $info_invoice = '
  <h2 class="name" style="color: #57B223;">'.$tipo_documento.': '.$numero_factura.'</h2>
  <div class="date">Fecha de la factura: '.$model->fecha_inicio.'</div>
  <div class="date">Fecha de vencimiento: '.$feven.'</div>
  <div style="font-size: 0.8em;  color: #FF0000;">Clave: '.$clave.'</div>';
}

$footer_pdf = $id_factun == -1 ? 'Autorizado mediante resolución 11-97 del 05/09/1997 de la D.G.T.D' : 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';

$servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
  $servicio_restaurante = '<tr>
                   <td colspan="3"></td>
                   <td colspan="3">SERVICIO 10%: </td>
                   <td>'.number_format($servicio,2).'</td>
               </tr>';
  $subtotal = $model->subtotal+$servicio;
  $total_a_pagar = $model->total_a_pagar+$servicio;
} else {
  $servicio_restaurante = '';
  $subtotal = $model->subtotal;
  $total_a_pagar = $model->total_a_pagar;
}
 ?>
<link href="<?= $style_imprime_fact ?>" rel="stylesheet">
<header class="clearfix">
  <div id="logo">
    <img src="<?= $empresaimagen->getImageurl('html') ?>" height="15%">
  </div>
  <div id="company">
    <h2 class="name"><?= $empresa->nombre ?> - ID: <?= $empresa->ced_juridica ?></h2>
    <div><?= $empresa->localidad ?> - <?= $empresa->direccion ?></div>
    <div><?= $empresa->telefono ?> / <?= $empresa->fax ?></div>
    <div><?= $empresa->sitioWeb ?> | <?= $empresa->email ?></div>
  </div>
</header>
<main>
  <div id="details" class="clearfix">
    <div id="client">
      <div class="to">Factura a:</div>
      <?= $info_cliente ?>
    </div>
    <div id="invoice">
      <h1>Factura no. <?= $model->idCabeza_Factura ?></h1>
      <?= $info_invoice ?>
      <h2>Condición Venta: <?= $model->estadoFactura ?> | Tipo de Pago:	<?= $tipo_pago ?></h2>
    </div>

  </div>
  <table border="0" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th class="no">C</th>
        <th class="unit">COD</th>
        <th class="qty">DESCRIPCIÓN</th>
        <th class="unit">PREC UNIT</th>
        <th class="qty">DESCTO</th>
        <th class="unit">IV %</th>
        <th class="total">TOTAL</th>
      </tr>
    </thead>
    <tbody>
      <?= $detalle_factura ?>
    </tbody>
    <tfoot>
      <?= $servicio_restaurante ?>
      <tr>
        <td colspan="3"></td>
        <td colspan="3">SUBTOTAL</td>
        <td><?= number_format($subtotal,2) ?></td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td colspan="3">TOTAL DESCUENTO</td>
        <td><?= number_format($model->porc_descuento,2) ?></td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td colspan="3">TOTAL IV</td>
        <td><?= number_format($model->iva,2) ?></td>
      </tr>
      <tr>
         <td colspan="3"></td>
         <td colspan="3"><h2>TOTAL A PAGAR: </h2></td>
         <td><h2><?= number_format($total_a_pagar,2) ?></h2></td>
      </tr>
    </tfoot>
  </table>
  <div id="thanks">¡Gracias por su preferencia!</div>
  <div id="notices">
    <div>Observación:</div>
    <div class="notice"><?= $model->observacion ?></div>
    <br><?= $footer_pdf ?>
  </div>
</main>
