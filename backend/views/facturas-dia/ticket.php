<?php

use yii\helpers\Html;
use backend\models\Empresa;
use yii\helpers\Url;
use backend\models\MovimientoCobrar;
use backend\models\NumberToLetterConverter;
use backend\models\Clientes;//para obtener el cliente
use backend\models\ClientesApartados;
use backend\models\Funcionario;
use backend\models\FacturasDia;
use backend\models\FormasPago;
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
use backend\models\Compra_view;
use backend\models\AgentesComisiones;
use backend\models\DetalleOrdServicio;
use backend\models\Servicios;
use backend\models\Familia;
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
$empresa_funcion = new Empresa();//Se obtienen datos de la empresa
/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */

//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
    $urlParts = parse_url($_SERVER['REQUEST_URI']);

    if (isset($urlParts['query'])){
        //
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){

            $empresaimagen = new Empresa();//Se obtienen datos de la empresa
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//busco la empresa para el encabezado

            $nombre_impresora = 'FACTURASNPS';
            //Create ESC/POS commands for sample receipt
            $esc = '0x1B'; //ESC byte in hex notation
            $newLine = '0x0A'; //LF byte in hex notation
            //$derecha = $esc . '!' . '0x00';//Establece el margen izquierdo en la columna n (donde n está entre 0 y 255) en el tono de carácter actual.
            //$cmds = $esc ."0x700x0";//abrir cajon
            $cmds = $esc . "@"; //Initializes the printer (ESC @)
            //Encabezado empresa-----------------------------------------------------------------
            $cmds .= $newLine . $newLine;
            $cmds .= $esc . '!' . '0x00'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
            $cmds .= $empresa->nombre.' - Ced: '.$empresa->ced_juridica.$newLine; //Nombre de la empresa
            $cmds .= $empresa->localidad.' - '.$empresa->direccion.$newLine; //Nombre de la empresa
            $cmds .= $empresa->sitioWeb.' / '. $empresa->email.$newLine; //Nombre de la empresa
            $cmds .= $empresa->telefono.' / '. $empresa->fax; //Nombre de la empresa

            $cmds .= $newLine . $newLine.$newLine;

            //Encabezado factura-------------------------------------------------------------------
            $modelcaja = FacturasDia::find()->where(['idCabeza_Factura'=>$id])->one();//vuelvo a llamar al modelo actual pero esta ves con las modificaciones que se aplicaron
            $firma = $modelcaja->estadoFactura=='Crédito' ? 'Firma:______________. Ced:________________'. $newLine . $newLine : '';
            $AG = '--';
            if ($modelcaja->idAgenteComision > 0) {
              $agente = AgentesComisiones::find()->where(['idAgenteComision'=>$modelcaja->idAgenteComision])->one();
              $AG = $modelcaja->idAgenteComision;// . ' - ' . $agente->nombre;
            }
            $estado_de_fac = $modelcaja->estadoFactura=='Crédito' ? 'Credito' : $modelcaja->estadoFactura;
            $fecha_facturacion = $modelcaja->estadoFactura=='Crédito' ? 'Pendiente' : $modelcaja->fecha_final;
            if ($modelcaja->factun_id > -1) {
              $fe = 'FE/TE: ' . $modelcaja->fe.$newLine;
              $clav = 'CLAVE: ' . $modelcaja->clave_fe.$newLine;
              $aut = 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';
              $f_e = 'Fecha emision: '. date('d-m-Y', strtotime( $modelcaja->fecha_emision )).$newLine;
              $f_c = 'Fecha cancelacion: '. $fecha_facturacion .$newLine.$newLine;
            } else {
              if ($modelcaja->estadoFactura == 'Apartado') {
                $cliente_apartado = ClientesApartados::findOne(['idCliente'=>$modelcaja->idCliente]);
                $fe_ven = strtotime ( '+'.$cliente_apartado->dias_apartado.' day' , strtotime ( $modelcaja->fecha_inicio ) ) ;
                $fe = $esc . '!' . '0x31' . 'APARTADO'.$newLine.$newLine. $esc . '!' . '0x00';
                $clav = '';
                $aut = 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';//"Autorizado mediante resolucion 11-97 del 05/09/1997 de la D.G.T.D";
                $f_e = '';
                $f_c = 'Fecha apartado: '. date('d-m-Y', strtotime( $modelcaja->fecha_inicio )).' / Vence: ' .date('d-m-Y', $fe_ven ).$newLine.$newLine;
              } else {
                if ($empresa->factun_conect == 1) {
                  $fe = $esc . '!' . '0x31' . 'Documento de contingencia'.$newLine. $esc . '!' . '0x00';
                  $clav = '';
                  $aut = 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';//"Autorizado mediante resolucion 11-97 del 05/09/1997 de la D.G.T.D";
                  $f_e = '';
                  $f_c = 'Fecha cancelacion: '. date('d-m-Y') .$newLine.$newLine;
                } else {
                  //$fe = $esc . '!' . '0x31' . 'Documento de contingencia'.$newLine. $esc . '!' . '0x00';
                  $clav = '';
                  $aut = 'Autorizado mediante resolucion 11-97 del 05/09/1997 de la D.G.T.D';
                  $f_e = '';
                  $f_c = 'Fecha cancelacion: '. date('d-m-Y') .$newLine.$newLine;
                }
              }
            }
            $cmds .= $esc . '!' . '0x00'; //Fuente de caracteres A seleccionado (ESC! 0)
            $cmds .= 'No INTERNO: ' . $id.$newLine;
            $cmds .= $fe;
            $cmds .= $clav;
            $cmds .= $f_e;
            $cmds .= $f_c;
            $cmds .= "CLIENTE:----------------------------------".$newLine;
            if (@$cliente = Clientes::find()->where(['idCliente'=>$modelcaja->idCliente])->one()) {
                $cmds .= "CED: ".$cliente->identificacion . $newLine;
                $cmds .= "Nombre: ".$cliente->nombreCompleto . $newLine;
                $cmds .= "Telefono: ".$cliente->telefono . $newLine;
                $cmds .= "Direccion: ".$cliente->direccion . $newLine;
            } else {
                $cmds .= $modelcaja->idCliente . $newLine;
            }
            $cmds .= "------------------------------------------". $newLine . $newLine;
            if ($modelcaja->estadoFactura == 'Apartado') {
            } else {
              $cmds .= 'Estado de la factura: ' . $estado_de_fac . $newLine. $newLine;
            }


            $cmds .= 'AG: ' . $AG . $newLine . $newLine;

            if(@$modelP = FormasPago::find()->where(['id_forma'=>$modelcaja->tipoFacturacion])->one()){
            if ($modelcaja->estadoFactura!='Crédito') {
                $cmds .= 'Tipo de pago: ' . $modelP->desc_forma . $newLine;
            } else {
                    $cmds .= 'Fecha Vencimiento: ' . $modelcaja->fecha_vencimiento . $newLine;
                }
            }

            $cmds .= 'Observacion: ' . $modelcaja->observacion . $newLine . $newLine . $newLine;

            //Detalle de factura--------------------------------------------------------------
            $cmds .= 'CANT      DESCRIPCION                 PRECIO' . $newLine;
            $products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $modelcaja->idCabeza_Factura])->all();
            $detallefactura = '';
            foreach($products as $product) {
                $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
                $servicios_os = '';
                if ($product['codProdServicio'] == "0") {
                  $det_ord_ser = DetalleOrdServicio::find()->where(['idOrdenServicio'=>$modelcaja->idOrdenServicio])->all();
                  foreach($det_ord_ser as $row){
                    //Obtengo una instancia de Servicios donde el codigo de servicio es igual al servicio que recorre el bucle
                    $modelServicio = Servicios::find()->where(['codProdServicio'=>$row['producto']])->one();
                    //Obtenemos una instancia de familia donde el código de familia es igual al servicio obtenido de la instancia anterior
                    $fami = Familia::find()->where(['codFamilia'=>$row['familia']])->one();
                    $servicios_os .=  '<'.$fami->descripcion . ' - ' . $modelServicio->nombreProductoServicio.'( '.number_format($modelServicio->precioMinServicio,2).' )>'."\n";
                  }
                }
                $pro_ser = $product['codProdServicio'] == '0' ? $servicios_os : "     <".$product['codProdServicio'] . '/' . $modelProd->ubicacion.">\n";
                $detallefactura .= new item($product['cantidad']. ' ' . substr($descrip,0,22) ,
                '  '.number_format(($product['precio_unitario']*$product['cantidad']),2)."\n".$pro_ser);
            }

            $servicio = ($modelcaja->subtotal - $modelcaja->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
            //Cierre de impresión------------------------------------------------------------
            $cmds .= $detallefactura ."---------------------------------------------" . $newLine;
            if (Yii::$app->params['servicioRestaurante']==true && $modelcaja->ignorar_porc_ser != 'checked') {
              $cmds .= new item("                 SERVICIO 10%:", number_format($servicio,2));
              $cmds .= new item("                 SUBTOTAL:", number_format($modelcaja->subtotal + $servicio,2));
            } else {
              $cmds .= new item("                 SUBTOTAL:", number_format($modelcaja->subtotal,2));
            }
            $cmds .= new item("                 TOTAL DESCUENTO:", number_format($modelcaja->porc_descuento,2));
            $cmds .= new item("                 TOTAL IV:", number_format($modelcaja->iva,2));
            if (Yii::$app->params['servicioRestaurante']==true && $modelcaja->ignorar_porc_ser != 'checked') {
              $cmds .= new item("                 TOTAL A PAGAR:",number_format($modelcaja->total_a_pagar + $servicio,2));
      			} else {
      				$cmds .= new item("                 TOTAL A PAGAR:",number_format($modelcaja->total_a_pagar,2));
      			}
            $cmds .= $newLine . $newLine;

            $modelF = Funcionario::find()->where(['idFuncionario'=>$modelcaja->codigoVendedor])->one();
            if ($modelcaja->estadoFactura == 'Apartado') {
              $cmds .= "Efectuado por: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2;
            } else {
              $cmds .= "Facturado por: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2;
            }
            $cmds .= $newLine . $newLine . $newLine;
            $cmds .= $firma;
            $cmds .= $newLine . $newLine . $newLine;
            $cmds .= $aut;
            $cmds .= $newLine . $newLine . $newLine;
            $cmds .= "GRACIAS POR PREFERIRNOS, ES UN GUSTO ATENDERLE.";
            $cmds .= $newLine . $newLine . $newLine . $newLine;

            $cmds .= $esc . "0x69";//cortar papel

            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente
            $cpj->clientPrinter = new InstalledPrinter($nombre_impresora);


            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();
            echo $cpj->sendToClient();

            ob_end_flush();
            exit();
            Compra_view::setCabezaFactura(null);
        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de tiquets-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
        //ejecuta la llamada de impresión
        jsWebClientPrint.print('sid=<?= Yii::$app->user->identity->last_session_id ?>');
        setTimeout("self.close()", 900 );
    });
</script>
<?php //otrs script
//Especifique la URL ABSOLUTA al archivo php que creará el objeto ClientPrintJob
//En este caso, esta misma página
$webClientPrintControllerAbsoluteURL = Utils::getRoot().$empresa_funcion->getWCP();
$demoPrintCommandsProcessAbsoluteURL = Utils::getRoot().Url::home().'?r=facturas-dia/ticket&id='.$id;
echo WebClientPrint::createScript($webClientPrintControllerAbsoluteURL, $demoPrintCommandsProcessAbsoluteURL, Yii::$app->user->identity->last_session_id);
//esta clase ayuda a ajustar caracteres centrados y a la derecha
class item
{
    private $nombre;
    private $precio;
    private $signo;

    public function __construct($nombre = '', $precio = '', $signo = false)
    {
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> signo = $signo;
    }

    public function __toString()
    {
        $rightCols = 13;
        $leftCols = 35;
        if ($this -> signo) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> nombre, $leftCols) ;

        $sign = ($this -> signo ? '$ ' : '');
        $right = str_pad($sign . $this -> precio, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}
?>
