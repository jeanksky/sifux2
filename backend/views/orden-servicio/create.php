
<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use kartik\switchinput\SwitchInput;
//use backend\models\Servicios;
//use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenServicio */

$this->title = 'Crear nueva orden de Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Ordenes de servicios pendientes de atender', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

//obtener datos del vehiculo
    function run_vehiculo() {
      var v_placa = document.getElementById("ddl-placa").value; //obtengo valor del id de la placa seleccionada
      $.ajax({//utilizo ajax para enviar dato por post al controlador en me debuelva los valores de vehiculo en tempo real
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/detallevehiculo') ?>",
                    type: "post",
                    dataType: 'json',
                    data: { 'v_placa': v_placa },//Asigno el valor de la placa a la variable que espera en la accion del controlador al que estoy enviando
                    beforeSend: function () { //Muestro en espera
                        $("#vehiculo").val("Procesando, espere por favor...");
                     },
                    success: function(data){//obtengo lo que me debuelve la funcion que retorna la accion del controlador
                         //$('#srt').val(v_placa);
                         $( "#vehiculo" ).html( data.resultados );//Muestro el resultado html que me debuelve el controlador
                         //setTimeout($('#ddl-cliente').val(data.idCliente).change(),2000);
                         $("#ddl-cliente").val(data.idCliente);
                         run_propietario(data.idCliente);
                     },
                    error: function(msg, status,err){
                         $('#vehiculo').val('Dato no esta llegando');
                     }
                });
    }

    function cargarprimerpropietario(v_placa){
        $.ajax({
            url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/primererpropietario') ?>",
                    type: "post",
                    data: { 'v_placa': v_placa },
                    success: function(data){//obtengo lo que me debuelve la funcion que retorna la accion del controlador
                        $('#ddl-cliente').val(data);
                     }
         });
    }

//obtener datos del propietario
    function run_propietario(v_cliente) {
          //var v_cliente = document.getElementById("ddl-cliente").value;
          $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/detallepropietario') ?>",
                    type: "post",
                    data: { 'v_cliente': v_cliente },
                    beforeSend: function () {
                        $("#propietario").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                         $( "#propietario" ).html( data );
                     },
                    error: function(msg, status,err){
                         $('#propietario').val('Dato no esta llegando');
                     }
                });
    }
//Para mostrar los servicios por cada categoria (familia) asignada
    /*$(function(){
        $('#tipo :checkbox').change(function(){
            var valor = $(this).val();

            //var dir = "<?php echo \Yii::$app->getUrlManager()->createUrl('orden-servicio/create') ?>";
            //var form = $('<form action="' + dir + '" method="post">' +
            //    '<input type="hidden" name="idF" value="' + valor +'"></input>' + '</form>');
            if(!$('#tipo :checkbox').is ( ':checked' ))
            {//Cuando no hay ninguno seleccionado
                $.ajax({
                   // url: 'index.php?r=orden-servicio%2Fservicios',
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosc') ?>", //Archivo PHP al que le enviarás el texto en <p>
                    //url: '<?php echo Yii::$app->request->baseUrl. '/orden-servicio/create' ?>',
                    type: "post",
                    //dataType: 'json',
                    data: { 'idF': valor  },
                    beforeSend: function () {
                        $("#recargar").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                         $('#res').val(valor);
                        // $('#montoS').val("0");
                         $( "#recargar" ).html( 'esperando...' );
                     }
                });
            }
            else
            {//cuando al menos uno este seleccionado
                //document.location.href='index.php?r=orden-servicio%2Fcreate&idF='+valor;
                //$(form).submit();
                $.ajax({
                    //url: 'index.php?r=orden-servicio%2Fservicios',
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosc') ?>", //Archivo PHP al que le enviarás el texto en <p>
                    //url: '<?php echo Yii::$app->request->baseUrl. '/orden-servicio/create' ?>',
                    type: "post",
                    //dataType: 'json',
                    data: { 'idF': valor  },
                    beforeSend: function () {
                        $("#recargar").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                         //$('#montoS').val(valor);
                        //$( "#recargar" ).html( data );

                            document.getElementById("recargar").innerHTML += data;
                     },
                    error: function(msg, status,err){
                         $('#recargar').val('Dato no esta llegando');
                     }
                });

            }
        });
    }); */


        function mostrarSer(valor){
         // var valor = document.getElementById("id-famili").value;
            $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosc') ?>", //Archivo PHP al que le enviarás el texto en <p>
                    type: "post",
                    data: { 'idF': valor },
                    beforeSend: function () {
                        $("#recargar").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                          //document.getElementById("recargar").innerHTML += data;
                          $("#recargar").append(data);
                          //document.getElementById("recargar").innerHTML += data;

                     }
                });
        }


        function ocultarSer(valor){
          //alert("Ahy servicios seleccionados asegurece de desmarcar todos los servicios de esta categoria");
      /*  $('#'+valor+':checkbox').change(function(){
            if(!$('#'+valor+':checkbox').is( ':checked' )){
              jQuery('#'+valor).remove();
            }else{
              alert("Ahy servicios de esta categoría seleccionados, asegurece de desmarcar todos los servicios de esta categoria");
            }
          });  */

         /*
          var marcado = $(".checked"+valor+"_").prop("checked") ? true : false;
          if(marcado == true){alert("Hay datos marcados"); $("#o"+valor+"p").prop("checked", "checked");} else {jQuery('#'+valor).remove();}
         */
    //Paso 1: Recorremos todo los servicios marcados de dicha categoría
    //y comprovamos si hay alguno marcado, si es así hace lo siguiente....
            $('.checked'+valor+'_:checked').each( //con esto obtengo los valores marchados "checked" cuya clase es checked+valorqueasigno+_
              function() {
             // var marcado = $("#vaSer"+valor+"_").prop("checked") ? true : false; //PAra saber si el checkbox está marcado
              if ($(this).is(':checked'))//Este if me dice que si estan mascados me mande cuales estan marcados
                {
                  //Esta alerta me trae todos los marcados
                  alert("El servicio con valor " + $(this).val() + " Esta seleccionado");
                  //Esto me selecciona de regreso al id cuyo valor esta siendo deseleccionado.
                  $("#o"+valor+"p").prop("checked", "checked");
                }
            });

    //Paso 2: Si no existe servicios marcados entonces se disparará esta función
            $("#o"+valor+"p").on( 'change', function() {
                if( !$(this).is(':checked') ) {//si se logró desmarcar la categoría..
                    jQuery('#'+valor).remove();//...que me borre lo que está en el div cuya clase es: valorqueleasigno
                }
            });
        }

    var total_iva=0; //total inicial con iva
    var total=0; //total inicial sin iva
    //para mostrar la suma de lo que cuesta cada servicio.
        function sumarMontSer(v_serv) {//Obtengo el valor del checkbox seleccionado para la suma
            $.ajax({//Envio por ajax usando post a la accion que me debuelva el costo del servicio segun el id seleccionado
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosmontoc') ?>",
                    type: "post",
                    data: { 'idV': v_serv }, //asigno el valor correspondiente del id seleccionado a la variable que me espera en el controlador
                    success: function(data){ //Funcion que espera el valor debuelto de la funcion del controlador
                        total_iva+=parseFloat(data); //total ahora es el valor + el dato que me debuelve la funcion
                        total = total_iva / 1.13;
                         $('#montoS').val(total.toFixed(2)); //Asigno el valor al text imput cuyo id es montoS usando 2 decimales
                         document.getElementById("monto_iva").innerHTML = '<h4>₡'+total_iva.toLocaleString('hi-IN', { style: 'decimal', decimal: '2' })+'</h4>';
                     }
                });
        }
    //para mostrar la resta de lo que cuesta cada servicio.
        function restarMontSer(v_serv) {//Obtengo el valor del checkbox seleccionado para la resta
        $.ajax({//Envio por ajax usando post a la accion que me debuelva el costo del servicio segun el id que no es seleccionado
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosmontoc') ?>",
                    type: "post",
                    data: { 'idV': v_serv }, //asigno el valor correspondiente del id seleccionado a la variable que me espera en el controlador
                    success: function(data){//Funcion que espera el valor debuelto de la funcion del controlador
                        total_iva-=parseFloat(data);//total ahora es el valor - el dato que me debuelve la funcion
                        total = total_iva / 1.13;
                         $('#montoS').val(total.toFixed(2)); //Asigno el valor al text imput cuyo id es montoS usando 2 decimales
                         document.getElementById("monto_iva").innerHTML = '<h4>₡'+total_iva.toLocaleString('hi-IN', { style: 'decimal', decimal: '2' })+'</h4>';
                     }
                });
        }

</script>




<?php
?>
<div class="orden-servicio-create">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <!--?php if (isset($_POST['idF'])) { echo $_POST['idF']; } ?-->
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>

    </p>
</div>

    <?= $this->render('_form', [
        'model' => $model,
        'familias' => $familias,
        //'servicios' => $servicios,
    ]) ?>

</div>
