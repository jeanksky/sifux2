<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\Servicios;
use yii\widgets\ActiveForm;
//use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenServicio */



$this->title = 'Orden de Servicio: ' . ' ' . $model->idOrdenServicio;
$this->params['breadcrumbs'][] = ['label' => 'Orden de Servicio', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idOrdenServicio/*, 'url' => ['view', 'id' => $model->idOrdenServicio]*/];
$this->params['breadcrumbs'][] = 'Vista';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

    
</script>

<div class="orden-servicio-update">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>
    <!--?= //Html::encode($this->title) ?-->
    <!--?= //Html::a('Regresar a la factura', ['regresar-factura'], ['class' => 'btn btn-default']) ?-->
    <?= $this->render('_form_', [
        'model' => $model,
        //'familias' => $familias,
    ]) ?>

</div>
