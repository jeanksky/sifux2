<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\Servicios;
use yii\widgets\ActiveForm;
use backend\models\Clientes;
use backend\models\Vehiculos;
use backend\models\Mecanicos;
use backend\models\Familia;
use backend\models\Marcas;
use backend\models\Modelo;
//use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenServicio */



$this->title = 'Orden de Servicio: ' . ' ' . $model->idOrdenServicio;
$this->params['breadcrumbs'][] = ['label' => 'Orden de Servicio', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idOrdenServicio/*, 'url' => ['view', 'id' => $model->idOrdenServicio]*/];
$this->params['breadcrumbs'][] = 'Vista';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">


</script>

<div class="panel panel-default">
<?php $modelMecanico = Mecanicos::find()->where(['idMecanico'=>$model->idMecanico])->one(); ?>
<div class="col-lg-12"><center><h4>Datos de Orden de servicio</h4></center>
	<p style="text-align:right;"><strong>Mecánico: </strong><?= $modelMecanico->nombre ?></p>
	<table style="width:100%">
	  <tr>
	    <th><label><strong>Datos del vehículo</strong></label></th>
	    <th><label><strong>Datos del cliente</strong></label></th>
	  </tr>
	  <tr>
	    <td>
			<?php
      	$modelVehiculo = Vehiculos::find()->where(['placa'=>$model->placa])->one();
        $marca = Marcas::findOne($modelVehiculo->marca);
        $modelo = Modelo::findOne($modelVehiculo->modelo);
          echo '<font size=1><strong>Placa:</strong> '.$model->placa;
          echo '<br><strong>Marca:</strong> '.$marca->nombre_marca;
          echo '<br><strong>Modelo:</strong> '.$modelo->nombre_modelo;
          echo '<br><strong>Año:</strong> '.$modelVehiculo->anio.'</font>';
       ?>
	    </td>
	    <td>
	    	<?php
                $modelClientes = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
                echo '<font size=1><strong>Nombre:</strong> '.$modelClientes->nombreCompleto;
                echo '<br><strong>Teléfono:</strong> '.$modelClientes->telefono;
                echo '<br><strong>Email:</strong> '.$modelClientes->email.'</font>';
            ?>
	    </td>
      <td> ______________________________<br> 
        <strong>Firma del cliente</strong>
      </td>
	  </tr>
	  <!--tr>
	    <td>Eve</td>
	    <td>Jackson</td>
	  </tr-->
	</table>
</div>

                <div class="col-lg-12">
                    <div class="well col-lg-12">
                        <label><strong>Incluye</strong></label>
                        <table style="width:100%">
						  <tr>
						    <td>
						    	<font size=1><?= '<strong>Radio:</strong> '.$model->radio ?></font>
						    </td>
						    <td>
						    	<font size=1><?= '<strong>Alfombra:</strong> '.$model->alfombra ?></font>
						    </td>
						    <td>
						    	<font size=1><?= '<strong>Herramientas:</strong> '.$model->herramienta ?></font>
						    </td>
						  </tr>
						  <tr>
						    <td>
						    	<font size=1><?= '<strong>Repuesto:</strong> '.$model->repuesto ?></font>
						    </td>
						    <td>
						    	<font size=1><?= '<strong>Cant Odometro:</strong> '.$model->cantOdometro ?></font>
						    </td>
						    <td>
						    	<font size=1><?= '<strong>Cant Combustible:</strong> '.$model->cantCombustible ?></font>
						    </td>
						  </tr>
						  <tr>
						    <td colspan="3"><font size=1><?= '<strong>Observaciones de Servicio:</strong> '.$model->observacionesServicio ?></font></td>
						  </tr>
						</table>

                    </div></div>
                    <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos de los servicios que se requieren
                        </div>
                        <!-- /.panel-heading -->

                        <?php
                            $espacio = '&nbsp&nbsp&nbsp&nbsp';
                        ?>
                        <div class="panel-body">
                            <!--label>Categorías seleccionadas en la Orden de Servicio</label-->


                                    <div class="form-group">
                                        <?php
                                            //Me obtiene los datos del detalle del detalle de orden de servicio
                                            $query = new yii\db\Query();
                                            $data = $query->select(['familia','producto'])
                                                ->from('tbl_detalle_ord_servicio')
                                                ->where(['=','idOrdenServicio', $model->idOrdenServicio])
                                                ->distinct()
                                                ->all();
                                            $filtro_f = null;
                                            $filtro_s = null;
                                            $num = 1;
                                            $familiarepetida = '';
                                            //Recorre los productos y familias correspondientes a esa orden de servicio
                                            foreach($data as $row){
                                                //condicion opcional para comprovar si hay datos duplicados
                                                if (/*$row['familia']!=$filtro_f &&*/ $row['producto']!=$filtro_s) {
                                                     //$modelFamilia = Familia::find()->where(['codFamilia'=>$row['familia']])->one();
                                                     //echo 'Familia: '.$modelFamilia->descripcion.'<br>';

                                                     //Obtengo una instancia de Servicios donde el codigo de servicio es igual al servicio que recorre el bucle
                                                     $modelServicio = Servicios::find()->where(['codProdServicio'=>$row['producto']])->one();
                                                     //Obtenemos una instancia de familia donde el código de familia es igual al servicio obtenido de la instancia anterior
                                                     $fami = Familia::find()->where(['codFamilia'=>$modelServicio->codFamilia])->one();
                                                     //agregamos en la variable precioyiva el precio total de todos los servicios con el 13% de inpuesto de venta.
                                                     $precioyiva = $modelServicio->precioMinServicio + ($modelServicio->precioMinServicio*0.13);

                                                    //echo $num.'. ('.$fami->descripcion.') '.$modelServicio->nombreProductoServicio.' ₡'.$precioyiva.'<br>';
                                                     echo '<div class="col-lg-4">';
                                                     if ($fami->descripcion != $familiarepetida) {

                                                            echo '<br><label><strong>'.$fami->descripcion.': </strong></label><br>';

                                                     	$familiarepetida = $fami->descripcion;
                                                     }
                                                     echo '<font size=1>'.$modelServicio->nombreProductoServicio.'
                                                          </font></div>';

                                                     $num++;// Contador.
                                                     $filtro_f = $row['familia']; //Iguala familia al filtro para que no se vuelva a tomar en cuenta
                                                     $filtro_s = $row['producto'];//Iguala producto al filtro para que no se vuelva a tomar en cuenta
                                                }

                                            }
                                        ?>

                                    </div>


                            <?= '<strong>Observaciones:</strong> '.$model->observaciones ?><br>
                            <?= '<strong>Monto total + IV:</strong> '.number_format($model->montoServicio + ($model->montoServicio * 0.13),2) ?>
                        </div>



                    </div>
                </div>







</div>
