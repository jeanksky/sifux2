<?php
//esta es solo vista para las modales
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\checkbox\CheckboxX;
use backend\models\Clientes;
use backend\models\Vehiculos;
use backend\models\Mecanicos;
use backend\models\Familia;
use backend\models\Marcas;
use backend\models\Modelo;
use kartik\widgets\Select2;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use backend\models\Servicios;
use backend\models\DetalleOrdServicio;
use kartik\money\MaskMoney;

//use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenServicio */
/* @var $form yii\widgets\ActiveForm */
?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script-->
<style type="text/css">
input[type=checkbox] {
   /* visibility: hidden;*/
    zoom:1.8;
}

/*.col-lg-1 img{
    width: 200px;
    height: 200px;
    margin:0 auto;
    border-radius: 50%;
    -webkit-box-shadow: 0px 0px 10px rgba(74, 74, 74, 0.75);
    -moz-box-shadow:    0px 0px 10px rgba(74, 74, 74, 0.75);
    box-shadow:         0px 0px 10px rgba(74, 74, 74, 0.75);
    -webkit-transition: all 1s ease-in-out;
    -moz-transition: all 1s ease-in-out;
}
.col-lg-1 img:hover{
    -webkit-transform: rotate(355deg);
    -moz-transform: rotate(355deg);
}*/
#dicSer {
    width: 20%;
    height: auto;

    display: inline-block;
    vertical-align: top;
    text-align: center;
    margin-left: 2%;
    margin-top: 1%;
    color: #4a4a4a;
    /*background-color: #FFFFFF;*/
    font-weight: bolder;
   /* font-size: 1.2rem;*/
    /*padding: 1%; /*Este sirve para darle altura al div*/
    -webkit-box-shadow: 0px 0px 10px rgba(79, 0, 79, 0.65);
    -moz-box-shadow:    0px 0px 10px rgba(79, 0, 79, 0.75);
    box-shadow:         0px 0px 10px rgba(79, 0, 79, 0.75);
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
}
#dicSer:hover{
    -webkit-transform: scale(1.2);
    -moz-transform: scale(1.2);
}
/*
#dicSer i{
    background-color: #4a4a4a;
    padding: 3px;
    color: white;
    width: 50%;
    padding-left: 20px;
    padding-right: 20px;
    border-radius: 4px;
}*/
</style>
<script type="text/javascript">

    //Funcion para que me permita ingresar solo numeros
    function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58))
                return true;
            return false;
        }
</script>
<?php /*
$this->registerJs('
    // obtener la id del formulario y establecer el manejador de eventos
        $("form#solicitante-form").on("beforeSubmit", function(e) {
            var form = $(this);
            $.post(
                form.attr("action")+"&submit=true",
                form.serialize()
            )
            .done(function(result) {
                form.parent().html(result.message);
                $.pjax.reload({container:"#solicitante-grid"});
            });
            return false;
        }).on("submit", function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        });
    ');*/
    $proforma = $model->observacionesServicio=='Proforma0' ? 'proforma' : '';
?>
<br>

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Datos de Orden de servicio correspondiente a esta factura <?= $proforma ?>.
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

            <div class="orden-servicio-form">

                <!--?php //$form = ActiveForm::begin(/*['method' => 'post']*/); ?-->
                <?php $form = ActiveForm::begin([
                    'id' => 'solicitante-form',
                    'method' => 'post',
                    'enableAjaxValidation' => true,
                    'enableClientScript' => true,
                    'enableClientValidation' => true,
                ]); ?>

                <!--?= $form->field($model, 'fecha')->textInput() ?-->
                <?php $fecha = date('d-m-Y'); // En formato ISO porque es compatible con casi todos los motores y los tipos de fecha
                $modelVehiculo = Vehiculos::find()->where(['placa'=>$model->placa])->one();
                $marca = Marcas::findOne($modelVehiculo->marca);
                $modelo = Modelo::findOne($modelVehiculo->modelo);
                ?>
                <div class="col-lg-12">

                <div class="col-lg-4">
                <?= $form->field($model,'fecha')->textInput(['readonly'=>true]) ?>
                </div>
                <div class="col-lg-8">
                    <strong>Porcentaje de avance de reparación o mantenimiento</strong>
                    <div class="panel-body">
                        <div class="">
                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                              aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $modelVehiculo->periodo_en_manten.'%'; ?>">
                                <?php echo $modelVehiculo->periodo_en_manten.'%'; ?>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
                <div class="col-lg-12">

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos del vehículo
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <!--?= //$form->field($model,'placa')->textInput(['readonly'=>true, 'onkeyup'=>'javascript:run_vehiculo()']) ?-->
                            <?php
                                echo '<strong>Placa:</strong> '.$model->placa;
                                echo '<br><strong>Marca:</strong> '.$marca->nombre_marca;
                                echo '<br><strong>Modelo:</strong> '.$modelo->nombre_modelo;
                                echo '<br><strong>Año:</strong> '.$modelVehiculo->anio;
                             ?>
                            <!--?= /*$form->field($model, 'placa')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Vehiculos::find()->asArray()->all(), 'placa', 'placa'),
                                'options'=>['id'=>'ddl-placa', 'readonly'=>true, 'onchange'=>'javascript:run_vehiculo()', 'placeholder'=>'- Selecccione -'],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true],
                            ]);*/?-->
                            <!--input type="text" id="srt" placeholder="esperando..."-->

                            <!--?= $form->field($model, 'placa')->widget(DepDrop::classname(), [
                               // 'data' => ArrayHelper::map(Vehiculos::find()->asArray()->all(), 'placa', 'placa'),
                                'options'=>['id'=>'ddl-placa', 'onchange'=>'javascript:run_vehiculo()', 'placeholder'=>'- Selecccione -'],
                                //'options'=>['id'=>'ddl-placa', 'placeholder'=>'- Selecccione -'],
                                'type' => DepDrop::TYPE_SELECT2,
                                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                'pluginOptions'=>[
                                    'depends'=>['ddl-cliente'],
                                    'placeholder'=>'- Selecccione -',
                                    'initialize'=> true,
                                    'url' => Url::to(['orden-servicio/cliente']),
                                    'loadingText' => 'Cargando...',
                                ]
                            ]);?-->
                         <div  class="tab-content" id="vehiculo" > </div>
                        <!--div class="col-lg-12">
                       </div-->

                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                 <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos del cliente
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <!--?= $form->field($model, 'idCliente')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'nombreCompleto'),
                                'options'=>['id'=>'ddl-cliente', 'onchange'=>'javascript:run_propietario()', 'placeholder'=>'- Selecccione -'],
                                // 'options'=>['id'=>'ddl-cliente', 'placeholder'=>'- Selecccione -'],
                                'pluginOptions' => ['allowClear' => true],

                            ]);?-->
                        <!--?= //$form->field($model,'idCliente')->textInput(['readonly'=>true]) ?-->
                        <?php
                        if ($model->idCliente) {
                            $modelClientes = Clientes::find()->where(['idCliente'=>$model->idCliente])->one();
                            echo '<strong>Nombre:</strong> '.$modelClientes->nombreCompleto;
                            echo '<br><strong>Teléfono:</strong> '.$modelClientes->telefono;
                            echo '<br><strong>Email:</strong> '.$modelClientes->email;
                        } else { echo 'Cliente no asignado'; }

                        ?>
                        <!--?= /*$form->field($model, 'idCliente')->widget(DepDrop::classname(), [
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'nombreCompleto'),
                                // 'data' =>[],
                                'options'=>['id'=>'ddl-cliente', 'onchange'=>'javascript:run_propietario()', 'placeholder'=>'- Selecccione -'],
                                'type' => DepDrop::TYPE_SELECT2,
                                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                'pluginOptions'=>[
                                    'depends'=>['ddl-placa'],
                                    'placeholder'=>'- Selecccione -',
                                    'initialize'=> true,
                                    'url' => Url::to(['orden-servicio/placa']),
                                    'loadingText' => 'Cargando...',
                                ]
                            ]);*/?-->
                            <div  class="tab-content" id="propietario" ></div>

                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div>


                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos del mecánico
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!--?= //$form->field($model,'idMecanico')->textInput(['readonly'=>true]) ?-->
                            <?php
                            $modelMecanico = Mecanicos::find()->where(['idMecanico'=>$model->idMecanico])->one();
                            if ($modelMecanico) {
                                echo '<strong>Nombre:</strong> '.$modelMecanico->nombre;
                            } else echo 'No aplicado';

                            ?>
                            <!--?= /*$form->field($model, 'idMecanico')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Mecanicos::find()->all(),'idMecanico','nombre'),
                            'language'=>'es',
                            'options' => ['placeholder' => '- Selecccione -'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]);
                            */?-->

                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div> </div>
                <?php
                if($model->observacionesServicio!='Proforma0'){
                ?>
                <div class="col-lg-12">
                    <div class="well col-lg-12">
                        <label>Incluye</label><br>
                        <legend></legend>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'radio')->radioList(['Si' => 'Si', 'No' => 'No']); ?>
                        <?php
                        /*$items = ['Si' => 'Si', 'No' => 'No'];
                        echo Html::radioList($model->radio, $model->radio, $items, [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            $disabled = true; // replace with whatever check you use for each item
                            return Html::radio($name, $checked, [
                                'value' => $value,
                                'label' => Html::encode($label),
                                'disabled' => $disabled,
                            ]);*/
                        ?>
                            <?= $form->field($model, 'alfombra')->radioList(['Si' => 'Si', 'No' => 'No']); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'herramienta')->radioList(['Si' => 'Si', 'No' => 'No']); ?>

                            <?= $form->field($model, 'repuesto')->radioList(['Si' => 'Si', 'No' => 'No']); ?>

                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'cantOdometro')->textInput(['readonly'=>true, "onkeypress"=>"return isNumber(event)"])->label('Cantidad de Odometro'); ?>
                            <?= $form->field($model, 'cantCombustible')->radioList(['R'=>'R','1/4'=>'1/4','1/2'=>'1/2','1/3'=>'1/3','F'=>'F'])->label('Cantidad de Combustible');?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'observacionesServicio')->textarea(['readonly'=>true, 'maxlength' => true])  ?>
                        </div>

                    </div>
                </div>
                <?php } ?>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos de los servicios brindados correspondientes a la presente factura
                        </div>
                        <!-- /.panel-heading -->

                        <?php

                                $espacio = '&nbsp&nbsp&nbsp&nbsp';
                                //$T_fa = \yii\helpers\ArrayHelper::map($familias, 'codFamilia', 'descripcion');
                               //$T_se = \yii\helpers\ArrayHelper::map($servicios, 'codProdServicio', 'nombreProductoServicio');

                                //$tiposFamilia = $form->field($model, 'famili')->checkboxList($T_fa, ['unselect'=>NULL,  'separator' => $espacio  /*, 'style' => 'width: 1px;'*/])->label(false);

                                //$tiposServicio = $form->field($model, 'servi')->checkboxList($T_se, ['unselect'=>NULL, 'separator' => $espacio/*, 'style' => 'width: 1px;'*/])->label(false);
                        ?>
                        <div class="panel-body">
                            <!--label>Categorías seleccionadas en la Orden de Servicio</label-->
                                <div id="tipo" class="col-lg-12">

                                    <?php //-------------------------------------------------------------------------
                                     //Esta parte por ahora es opcional ya que es para traer todos los servicios
                                     //$ids = Yii::$app->request->isAjax ? Yii::$app->request->post('idF') : null;
                                     //$servicios = Servicios::findAll(['codFamilia' => $ids]);
                                     //$servicios = Servicios::findAll(['tipo' => 'Servicio']);
                                     //$servicios = Servicios::find()->All();
                                     //$T_se = \yii\helpers\ArrayHelper::map($servicios, 'codProdServicio', 'nombreProductoServicio');
                                     //------------------------------------------------------------------------------
                                     ?>

                                    <!--?= //$form->field($model, 'famili')->checkboxList($T_fa, [ 'unselect'=>NULL, 'separator' => $espacio])->label(false) ?-->

                                    <div class="form-group">
                                        <?php
                                            //Me obtiene los datos del detalle del detalle de orden de servicio
                                            $query = new yii\db\Query();
                                            $data = $query->select(['familia','producto'])
                                                ->from('tbl_detalle_ord_servicio')
                                                ->where(['=','idOrdenServicio', $model->idOrdenServicio])
                                                ->distinct()
                                                ->all();
                                            $filtro_f = null;
                                            $filtro_s = null;
                                            $num = 1;
                                            $familiarepetida = '';
                                            //Recorre los productos y familias correspondientes a esa orden de servicio
                                            foreach($data as $row){
                                                //condicion opcional para comprovar si hay datos duplicados
                                                if (/*$row['familia']!=$filtro_f &&*/ $row['producto']!=$filtro_s) {
                                                     //$modelFamilia = Familia::find()->where(['codFamilia'=>$row['familia']])->one();
                                                     //echo 'Familia: '.$modelFamilia->descripcion.'<br>';

                                                     //Obtengo una instancia de Servicios donde el codigo de servicio es igual al servicio que recorre el bucle
                                                     $modelServicio = Servicios::find()->where(['codProdServicio'=>$row['producto']])->one();
                                                     //Obtenemos una instancia de familia donde el código de familia es igual al servicio obtenido de la instancia anterior
                                                     $fami = Familia::find()->where(['codFamilia'=>$modelServicio->codFamilia])->one();
                                                     //agregamos en la variable precioyiva el precio total de todos los servicios con el 13% de inpuesto de venta.
                                                     $precioyiva = $modelServicio->precioMinServicio + ($modelServicio->precioMinServicio*0.13);

                                                    //echo $num.'. ('.$fami->descripcion.') '.$modelServicio->nombreProductoServicio.' ₡'.$precioyiva.'<br>';


                                                    if ($fami->descripcion != $familiarepetida) {
                                                    echo '<div class="col-lg-4">';
                                                    echo '<div class="well col-lg-12">';
                                                    echo '<strong>'.$fami->descripcion.'</strong>
                                                            <legend></legend>';
                                                            //$familiarepetida = $fami->descripcion;
                                                    echo $modelServicio->nombreProductoServicio.' ₡'.number_format($precioyiva,2);
                                                    echo '</div>
                                                          </div>';
                                                        }


                                                     $num++;// Contador.
                                                     $filtro_f = $row['familia']; //Iguala familia al filtro para que no se vuelva a tomar en cuenta
                                                     $filtro_s = $row['producto'];//Iguala producto al filtro para que no se vuelva a tomar en cuenta
                                                }

                                            }
                                        ?>
                                        <!--?= /*$form->field($model, 'famili')->checkboxList(
                                        \yii\helpers\ArrayHelper::Map($familias,
                                            'codFamilia',
                                            'descripcion'),
                                        [ 'unselect'=>NULL,
                                          'separator' => $espacio,
                                          'item' => function($index, $label, $name, $checked, $value) {
                                            $label='<br>'.$label;
                                            return  '<label class="modal-radio" id="dicSer"  >' . Html::checkbox($name, $checked, [ 'class'=>'checked','id'=>'o'.$value.'p','onClick'=>'if (this.checked) mostrarSer('.$value.'); else ocultarSer('.$value.')', 'label' => $label, 'value' => $value, 'data-bind' => 'checked:fieldName']) . '</label>';
                                            }
                                        ])->label(false);*/ ?-->


                                    </div>
                                </div>
                                <!--div class="col-lg-12"><br-->
                                    <!--label>Servicios seleccionados en la Orden de Servicio</label-->
                                    <!--div class="tab-pane fade" id="res"-->
                                    <!--?= /*$form->field($model, 'servi')
                                              ->checkboxList($T_se, [
                                                                     'unselect'=>NULL,
                                                                     'separator' => $espacio,
                                                                     'item' => function($index, $label, $name, $checked, $value) {
                                                                                    $label='<br>'.$label;
                                                                                    return  '<label class="cl-ser" style="display: none;" id="content" >' . Html::checkbox($name, $checked, ['id'=>'vaSer', 'onClick'=>'if (this.checked) sumarMontSer('.$value.'); else restarMontSer('.$value.')' , 'label' => $label, 'value' => $value]) . '</label>';
                                                                                 }
                                                                    ])->label(false)*/ ?-->
                                    <!--input type="text" id="org"-->
                                    <!--/div-->

                                    <!--Aquí en este div es donde se muestran los servicios-->
                                    <!--div id="recargar"></div-->
                                <!--/div-->

                        </div>
                        <div class="panel-body">

                            <?= $form->field($model, 'observaciones')->textarea(['readonly'=>true,'maxlength' => true])  ?>

                            <div class="col-lg-4"><div id="montoSL"></div>
                            <!--?= //$form->field($model,'montoServicio')->textInput() ?-->

                            <!-- envio los datos en un textimput oculto -->
                            <!--?= //$form->field($model, 'montoServicio')->textInput(['id' => 'montoS','readonly'=>true , 'type' => 'hidden']) ?-->

                            </div>

                        </div>



                    </div>
                </div>


                        <?php ActiveForm::end(); ?>

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
