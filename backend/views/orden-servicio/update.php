<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\Servicios;
use yii\widgets\ActiveForm;
//use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenServicio */



$this->title = 'Actualizar Orden de Servicio: ' . ' ' . $model->idOrdenServicio;
$this->params['breadcrumbs'][] = ['label' => 'Ordenes de servicios pendientes de atender', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idOrdenServicio/*, 'url' => ['view', 'id' => $model->idOrdenServicio]*/];
$this->params['breadcrumbs'][] = 'Modificar orden de servicio';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

function cambia(periodo) {
    var placa = "<?php echo $model->placa ?>";
    $.ajax({
        url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/cambiaperiodo') ?>",
        type: "post",
        data: { 'periodo_en_manten': periodo, 'placa' : placa },
        success: function(data){
        document.getElementById("slider").innerHTML=periodo+'%';
        }
    });
}

//obtener datos del vehiculo
    function run_vehiculo() {
     var v_placa = document.getElementById("ddl-placa").value; //obtengo valor del id de la placa seleccionada
     $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/detallevehiculo') ?>",
                    type: "post",
                    dataType: 'json',
                    data: { 'v_placa': v_placa },
                    beforeSend: function () {
                        $("#vehiculo").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                         //$('#srt').val(v_placa);
                         document.getElementById("vehiculo").innerHTML = data.resultados;
                         $("#ddl-cliente").val(data.idCliente);
                         run_propietario(data.idCliente);
                        // $( "#vehiculo" ).html( data );
                     },
                    error: function(msg, status,err){
                         $('#vehiculo').val('Dato no esta llegando');
                     }
                });
    } setInterval(run_vehiculo,500); //con esto me hace la funcion automatica para que me muestre los datos sin volver a seleccionar

function mostrar_servicio_IV() {
    var monto_=document.getElementById("montoS").value;
    var total_ = parseFloat(monto_);
    var total_mas_iv_ = total_ + (total_ * 0.13);
    //document.getElementById("monto_iva").innerHTML = '<h4>₡'+total_mas_iv_.toLocaleString('hi-IN', { style: 'decimal', decimal: '2' })+'</h4>';
} setTimeout(mostrar_servicio_IV,500);

//obtener datos del propietario
    function run_propietario(v_cliente) {
        var idClienteBD = '<?= $model->idCliente ?>';
        $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/detallepropietario') ?>",
                    type: "post",
                    data: { 'v_cliente': v_cliente },
                    beforeSend: function () {
                        $("#propietario").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                         $( "#propietario" ).html( data );
                         if (idClienteBD != v_cliente) {
                           document.getElementById("compruebe_cliente_actualizado").innerHTML = '<font size="4">Cliente no asignado a la<br>Orden de Servicio, o hubo<br> un cambio de cliente, solo<br>actualice la Orden de Servicio.</font>';
                         } else {
                           document.getElementById("compruebe_cliente_actualizado").innerHTML = '';
                         }
                     },
                    error: function(msg, status,err){
                         $('#propietario').val('Dato no esta llegando');
                     }
                });
    }
    //Funcion que me trae los servicios cuya familia (categoria) este previamente marcada.
    function checked_categoria(){
        $('.checked:checked').each( //con esto obtengo los valores marchados "checked" cuya clase es checked
            function() {
            var valor = $(this).val(); //obtengo el valor de cada checkbox marcado
            $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosu') ?>", //Archivo PHP al que le enviarás el texto en <p>
                    type: "post",
                    data: { 'idF': valor , 'id' : '<?php echo $model->idOrdenServicio ?>'  },
                    beforeSend: function () {
                        $("#recargar").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                          document.getElementById("recargar").innerHTML += data;
                     }
                });//fin ajax
         });
    } setTimeout("checked_categoria()", 500); //con esto hacemos que la funcion se ejecute automaticamente

        function mostrarSer(valor){

            //var valor = document.getElementById('o1p').value;
            $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosu') ?>", //Archivo PHP al que le enviarás el texto en <p>
                    type: "post",
                    data: { 'idF': valor , 'id' : '<?php echo $model->idOrdenServicio ?>'  },
                    beforeSend: function () {
                        $("#recargar").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                          //document.getElementById("recargar").innerHTML += data;
                          $("#recargar").append(data);
                          //document.getElementById("recargar").innerHTML += data;

                     }
                });//fin ajax

        }

        //Quito el servicio que tengo asignado.
        function ocultarSer(valor){
            //Paso 1: Recorremos todo los servicios marcados de dicha categoría
            //y comprovamos si hay alguno marcado, si es así hace lo siguiente....
            $('.checked'+valor+'_:checked').each( //con esto obtengo los valores marchados "checked" cuya clase es checked+valorqueasigno+_
              function() {
             // var marcado = $("#vaSer"+valor+"_").prop("checked") ? true : false; //PAra saber si el checkbox está marcado
              if ($(this).is(':checked'))//Este if me dice que si estan mascados me mande cuales estan marcados
                {
                  //Esta alerta me trae todos los marcados
                  alert("El checkbox con valor " + $(this).val() + " esta seleccionado");
                  //Esto me selecciona de regreso al id cuyo valor esta siendo deseleccionado.
                  $("#o"+valor+"p").prop("checked", "checked");
                }
            });

            //Paso 2: Si no existe servicios marcados entonces se disparará esta función
            $("#o"+valor+"p").on( 'change', function() {
                if( !$(this).is(':checked') ) {//si se logró desmarcar la categoría..
                    jQuery('#'+valor).remove();//...que me borre lo que está en el div cuya clase es: valorqueleasigno
                }
            });
        }

        //para mostrar la suma de lo que cuesta cada servicio.
        function sumarMontSer(v_serv) {
            var total=document.getElementById("montoS").value;//Obtengo el dato que ya existe
            var operacion = 's';
            $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosmontou') ?>",
                    type: "post",
                    dataType: 'json',
                    data: { 'idV': v_serv, 'total': total, 'operacion': operacion },
                    success: function(data){
                         $('#montoS').val(data.total);
                         document.getElementById("monto_iva").innerHTML = '<h4>₡'+data.total_mas_iv+'</h4>';
                     }
                });
        }
    //para mostrar la resta de lo que cuesta cada servicio.
        function restarMontSer(v_serv) {
            var total=document.getElementById("montoS").value;
            var operacion = 'r';
            $.ajax({
                    url: "<?php echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosmontou') ?>",
                    type: "post",
                    dataType: 'json',
                    data: { 'idV': v_serv, 'total': total, 'operacion': operacion },
                    success: function(data){
                      //$('#montoS').val(data.total);
                      document.getElementById("montoS").value = data.total;
                      document.getElementById("monto_iva").innerHTML = '<h4>₡'+data.total_mas_iv+'</h4>';
                     }
                });
        }

  /*$(function(){
        $('#tipo :checkbox').change(function(){
            var valor = $(this).val();
            if(!$('#tipo :checkbox').is ( ':checked' ))
            {
                $.ajax({
                    url: "<?php //echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosu') ?>",
                    type: "post",
                    data: { 'idF': valor, 'id' : '<?php //echo $model->idOrdenServicio ?>' },
                    beforeSend: function () {
                        $("#org").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                         var valor = $(this).val();
                         $( "#recargar" ).html( 'Esperando categoría...' );
                     }
                });
            }
            else
            {
                $.ajax({
                    url: "<?php //echo Yii::$app->getUrlManager()->createUrl('orden-servicio/serviciosu') ?>",
                    type: "post",
                    data: { 'idF': valor , 'id' : '<?php //echo $model->idOrdenServicio ?>'  },
                    beforeSend: function () {
                        $("#recargar").val("Procesando, espere por favor...");
                     },
                    success: function(data){
                    	 //$(document).ready(function(){ setInterval(loadClima,1000); });
                         //$.pjax.reload({container:"#refrescar"});
                         //$(document).pjax('#refrescar');
                         $('#org').val(valor);
                         $( "#recargar" ).html( data );

                     },
                    error: function(msg, status,err){
                         $('#recargar').val('Dato no esta llegando');
                     }
                });

            }
        });
    });*/

/*function loadClima(){
$("#refrescar").load("<?php //echo Yii::$app->getUrlManager()->createUrl('orden-servicio/servicios') ?>");
} */


</script>

<div class="orden-servicio-update">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'familias' => $familias,
    ]) ?>

</div>
