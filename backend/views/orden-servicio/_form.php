<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\checkbox\CheckboxX;
use backend\models\Clientes;
use backend\models\Vehiculos;
use backend\models\Mecanicos;
use backend\models\Familia;
use kartik\widgets\Select2;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use backend\models\Servicios;
use kartik\money\MaskMoney;
use kartik\slider\Slider;
use backend\models\F_Proforma;
use backend\models\Ot;
//use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenServicio */
/* @var $form yii\widgets\ActiveForm */
$proforma = F_Proforma::getOrdenServicio();

$estado = F_Proforma::GetOrdenServicio()=='Si' ? 0 : 1;

?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script-->
<style type="text/css">
input[type=checkbox] {
   /* visibility: hidden;*/
    zoom:1.8;
}

#dicCat {
    width: 300px;
    text-align: center;
    margin-left: 3%;
    margin-top: 1%;
    color: #4a4a4a;
    background-color: #00FFBF;
    font-weight: bolder;
    -webkit-box-shadow: 0px 0px 10px rgba(79, 0, 79, 0.65);
    -moz-box-shadow:    0px 0px 10px rgba(79, 0, 79, 0.75);
    box-shadow:         0px 0px 10px rgba(79, 0, 79, 0.75);
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
}

#dicSer {
    width: 300px;
    text-align: center;
    margin-left: 3%;
    margin-top: 1%;
    color: #4a4a4a;
    background-color: #00FFFF;
    font-weight: bolder;
    -webkit-box-shadow: 0px 0px 10px rgba(79, 0, 79, 0.65);
    -moz-box-shadow:    0px 0px 10px rgba(79, 0, 79, 0.75);
    box-shadow:         0px 0px 10px rgba(79, 0, 79, 0.75);
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
}
#dicCat:hover {
    -webkit-transform: scale(1.2);
    -moz-transform: scale(1.2);
}
#dicSer:hover {
    -webkit-transform: scale(1.2);
    -moz-transform: scale(1.2);
}

.estado { display:none; }
/*
#dicSer i{
    background-color: #4a4a4a;
    padding: 3px;
    color: white;
    width: 50%;
    padding-left: 20px;
    padding-right: 20px;
    border-radius: 4px;
}*/
</style>
<script type="text/javascript">

//http://www.jose-aguilar.com/blog/mostrar-y-ocultar-el-contenido-de-un-div-con-un-checkbox/
/*function showContent() {
        check = document.getElementById("check");
        element = document.getElementById("mostrar");


            if (check.checked) {
                //element.style.display='block';
                $('#mostrar').show("slow");
            }
            else {
                //element.style.display='none';
                $('#mostrar').hide("slow");
            }


    }*/
   /*$(function(){
        $('#tipo :checkbox').change(function(){
            var valor = $(this).val();
            var form = $('<form action="#" method="post">' +
                '<input type="hidden" name="idF" id="org"></input>' + '</form>');
            //var form = $("<?php ActiveForm::begin(['method' => 'post', 'action'=>'#' ]); ?>" + "<input type='hidden' name='idF' id='org'></input>" + "<?php ActiveForm::end(); ?>");
            if(!$('#tipo :checkbox').is ( ':checked' ))
            {
                $('#org').val('');
                $(form).submit();
            }
            else
            {
                $('#org').val(valor);
                $(form).submit();
            }
        });
    });*/
  /* $(function(){
        //$('#tipo :checkbox').change(function(){
        $('#tipo :checkbox').change(function(){
            var valor = $(this).val();
            //var parametros = { "valor" : valor };
           // var form = $('<form action="index.php?r=orden-servicio%2Fcreate" method="post">' +
            //    '<input type="hidden" name="fa" value="' + valor + '"></input>' + '</form>');
            if(!$('#tipo :checkbox').is ( ':checked' ))
            {

               // $("#refrescar").load('_form.php');
                document.getElementById("resultado").innerHTML='';
                //document.getElementById("resultado").innerHTML='';
            }
            else
            {

                //$(form).submit();
                //$("#refrescar").load('_form.php');
                document.getElementById("resultado").innerHTML=parseInt(valor);

                //document.getElementById("resultado").value=parseInt(valor);
            }
        });
    });*/
    //Funcion para que me permita ingresar solo numeros
    function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58))
                return true;
            return false;
        }
    $(document).ready(function () {
    $('#generarOrdenSer').on('click', function () {
            var $btn = $(this).button('loading')
            // simulating a timeout
            setTimeout(function () {
                $btn.button('reset');
            }, 2000);
          });
    });
</script>
<br>

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
             Ingrese los datos que se le solicitan en el siguiente formulario.

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

            <div class="orden-servicio-form">

                <?php $form = ActiveForm::begin(/*['method' => 'post']*/); ?>

                <!--?= $form->field($model, 'fecha')->textInput() ?-->
                <?php $fecha = date("d-m-Y"); // En formato ISO porque es compatible con casi todos los motores y los tipos de fecha
                ?>
                <div class="col-lg-12">

                <div class="col-lg-4">
                <?php
                 $fechaobtenida = $model->isNewRecord ? $form->field($model,'fecha')->textInput([ 'value' => $fecha,'readonly'=>true]) : $form->field($model,'fecha')->textInput(['readonly'=>true]);
                 echo $fechaobtenida;
                  ?>
                <!--?= //$form->field($model,'fecha')->textInput([ 'value' => $fecha,'readonly'=>true]) ?-->
                </div>
                <div class="col-lg-8">
                <?php
                $bandera = false;
                if($proforma == 'Si'|| $model->observacionesServicio=='Proforma0') { echo '<div class="alert alert-info alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>¡Atención!</strong> Esta Orden de Servicio está activa para Proforma
                            </div>';
                            $bandera = true;
                           } else { ?>
                <label>Porcentaje de avance de reparación o mantenimiento: </label><div id="slider"></div>
                  <?php
                  if ($model->isNewRecord) {
                      $nivel = Slider::widget([
                        'sliderColor'=>Slider::TYPE_PRIMARY,
                        'name'=>'rating_1',
                        'value'=>5,
                        'pluginOptions'=>[
                            'step'=>1,
                            'min'=>0,
                            'max'=>100
                        ],
                        'options'=>['disabled'=>true]
                    ]);
                      echo $nivel;
                  } else {
                      $vehic = Vehiculos::find()->where(['placa'=>$model->placa])->one();
                     // $nivel = '<input type=range min=0 max=100 id=periodo onchange="cambia(this.value)" value='.$vehic->periodo_en_manten.' step=1 />';
                      $nivel = Slider::widget([
                            'sliderColor'=>Slider::TYPE_PRIMARY,
                            'name'=>'rating_1',
                            'value'=>$vehic->periodo_en_manten,
                            'options'=>['onchange'=>'javascript:cambia(this.value)'],
                            'pluginOptions'=>[
                                'size'=>'lg',
                                'step'=>1,
                                'min'=>0,
                                'max'=>100
                            ]
                        ]);
                      echo $nivel;
                  }

                  ?>
                <?php } ?>
                </div>
                </div>
                <div class="col-lg-12">

                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos del vehículo
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">


                            <?= $form->field($model, 'placa')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Vehiculos::find()->asArray()->all(), 'placa', 'placa'),
                                'options'=>['id'=>'ddl-placa', 'onchange'=>'javascript:run_vehiculo()', 'placeholder'=>'- Selecccione -'],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true],
                            ]);?>
                            <!--input type="text" id="srt" placeholder="esperando..."-->

                            <!--?= $form->field($model, 'placa')->widget(DepDrop::classname(), [
                               // 'data' => ArrayHelper::map(Vehiculos::find()->asArray()->all(), 'placa', 'placa'),
                                'options'=>['id'=>'ddl-placa', 'onchange'=>'javascript:run_vehiculo()', 'placeholder'=>'- Selecccione -'],
                                //'options'=>['id'=>'ddl-placa', 'placeholder'=>'- Selecccione -'],
                                'type' => DepDrop::TYPE_SELECT2,
                                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                'pluginOptions'=>[
                                    'depends'=>['ddl-cliente'],
                                    'placeholder'=>'- Selecccione -',
                                    'initialize'=> true,
                                    'url' => Url::to(['orden-servicio/cliente']),
                                    'loadingText' => 'Cargando...',
                                ]
                            ]);?-->
                         <div  class="tab-content" id="vehiculo" > </div>
                        <!--div class="col-lg-12">
                       </div-->

                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                 <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos del cliente
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <!--?= $form->field($model, 'idCliente')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'nombreCompleto'),
                                'options'=>['id'=>'ddl-cliente', 'onchange'=>'javascript:run_propietario()', 'placeholder'=>'- Selecccione -'],
                                // 'options'=>['id'=>'ddl-cliente', 'placeholder'=>'- Selecccione -'],
                                'pluginOptions' => ['allowClear' => true],

                            ]);?-->
                        <?= $form->field($model, 'idCliente')->textInput(["type"=>"hidden", "id"=>'ddl-cliente'])?>
                        <?php
                      /*  echo $form->field($model, 'idCliente')->widget(DepDrop::classname(), [
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'nombreCompleto'),
                                // 'data' =>[],
                                'options'=>['id'=>'ddl-cliente' ,'onchange'=>'javascript:run_propietario(this.value)', 'placeholder'=>'- Seleccione -'],
                                'type' => DepDrop::TYPE_SELECT2,
                                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                'pluginOptions'=>[
                                    'depends'=>['ddl-placa'],
                                    'placeholder'=>'- Seleccione -',
                                    'initialize'=> true,
                                    'url' => Url::to(['orden-servicio/placa']),
                                    'loadingText' => 'Cargando...',
                                ]
                            ])*/ ?>
                            <span id="compruebe_cliente_actualizado" style="float:right" class="label label-warning"></span>
                            <div  class="tab-content" id="propietario" ></div>

                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div>


                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos del mecánico
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?= $form->field($model, 'idMecanico')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Mecanicos::find()->all(),'idMecanico','nombre'),
                            'language'=>'es',
                            'options' => ['placeholder' => '- Selecccione -'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]);
                            ?>

                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div> </div>
                <?php
                if($proforma == 'Si' || $model->observacionesServicio=='Proforma0'){
                    $model->radio = 'No'; $model->alfombra = 'No'; $model->herramienta = 'No'; $model->repuesto = 'No';
                    $model->cantOdometro = 0; $model->cantCombustible = 'R'; $model->observacionesServicio = 'Proforma0';
                    echo '<div style="display:none;">';
                    echo $form->field($model, 'radio')->radioList(['Si' => 'Si', 'No' => 'No']);
                    echo $form->field($model, 'alfombra')->radioList(['Si' => 'Si', 'No' => 'No']);
                    echo $form->field($model, 'herramienta')->radioList(['Si' => 'Si', 'No' => 'No']);
                    echo $form->field($model, 'repuesto')->radioList(['Si' => 'Si', 'No' => 'No']);
                    echo $form->field($model, 'cantOdometro')->textInput(["onkeypress"=>"return isNumber(event)"])->label('Cantidad de Odometro');
                    echo $form->field($model, 'cantCombustible')->radioList(['R'=>'R','1/4'=>'1/4','1/2'=>'1/2','1/3'=>'1/3','F'=>'F'])->label('Cantidad de Combustible');
                    echo $form->field($model, 'observacionesServicio')->textarea(['maxlength' => true]);
                    echo "</div>";
                } else {
                ?>
                <div class="col-lg-12">
                    <div class="well col-lg-12">
                        <legend>Incluye</legend>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'radio')->radioList(['Si' => 'Si', 'No' => 'No']); ?>

                            <?= $form->field($model, 'alfombra')->radioList(['Si' => 'Si', 'No' => 'No']); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'herramienta')->radioList(['Si' => 'Si', 'No' => 'No']); ?>

                            <?= $form->field($model, 'repuesto')->radioList(['Si' => 'Si', 'No' => 'No']); ?>

                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'cantOdometro')->textInput(["onkeypress"=>"return isNumber(event)"])->label('Cantidad de Odometro'); ?>
                            <?= $form->field($model, 'cantCombustible')->radioList(['R'=>'R','1/4'=>'1/4','1/2'=>'1/2','1/3'=>'1/3','F'=>'F'])->label('Cantidad de Combustible');?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'observacionesServicio')->textarea(['maxlength' => true])  ?>
                        </div>

                    </div>
                </div>
                <?php } ?>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Agregar servicios
                        </div>
                        <!-- /.panel-heading -->

                        <?php

                                $espacio = '&nbsp&nbsp&nbsp&nbsp';
                                $T_fa = \yii\helpers\ArrayHelper::map($familias, 'codFamilia', 'descripcion');
                               //$T_se = \yii\helpers\ArrayHelper::map($servicios, 'codProdServicio', 'nombreProductoServicio');

                                //$tiposFamilia = $form->field($model, 'famili')->checkboxList($T_fa, ['unselect'=>NULL,  'separator' => $espacio  /*, 'style' => 'width: 1px;'*/])->label(false);

                                //$tiposServicio = $form->field($model, 'servi')->checkboxList($T_se, ['unselect'=>NULL, 'separator' => $espacio/*, 'style' => 'width: 1px;'*/])->label(false);
                        ?>
                        <div class="panel-body">
                            <label>Primero - Seleccione las categorías que necesita</label>
                                <div id="tipo" class="col-lg-12">

                                    <?php //-------------------------------------------------------------------------
                                     //Esta parte por ahora es opcional ya que es para traer todos los servicios
                                     $ids = Yii::$app->request->isAjax ? Yii::$app->request->post('idF') : null;
                                     //$servicios = Servicios::findAll(['codFamilia' => $ids]);
                                     $servicios = Servicios::findAll(['tipo' => 'Servicio']);
                                     //$servicios = Servicios::find()->All();
                                     $T_se = \yii\helpers\ArrayHelper::map($servicios, 'codProdServicio', 'nombreProductoServicio');
                                     //------------------------------------------------------------------------------
                                     ?>

                                    <!--?= //$form->field($model, 'famili')->checkboxList($T_fa, [ 'unselect'=>NULL, 'separator' => $espacio])->label(false) ?-->



                                        <?= $form->field($model, 'famili')->checkboxList(
                                        \yii\helpers\ArrayHelper::Map($familias,
                                            'codFamilia',
                                            'descripcion'),
                                        [ 'unselect'=>NULL,
                                          'separator' => $espacio,
                                          'item' => function($index, $label, $name, $checked, $value) {
                                            $label='<br>'.$label;
                                            return  '<label class="modal-radio" id="dicCat"  >' .
                                            Html::checkbox($name, $checked, [ 'class'=>'checked','id'=>'o'.$value.'p',
                                            'onClick'=>'if (this.checked) mostrarSer('.$value.'); else ocultarSer('.$value.')',
                                            'label' => $label, 'value' => $value, 'data-bind' => 'checked:fieldName']) . '</label>';
                                            }
                                        ])->label(false); ?>



                                </div>
                                <div class="col-lg-12"><br>
                                    <label>Segundo - Seleccione los servicios que recibira el vehículo</label>
                                    <!--div class="tab-pane fade" id="res"-->
                                    <!--?= /*$form->field($model, 'servi')
                                              ->checkboxList($T_se, [
                                                                     'unselect'=>NULL,
                                                                     'separator' => $espacio,
                                                                     'item' => function($index, $label, $name, $checked, $value) {
                                                                                    $label='<br>'.$label;
                                                                                    return  '<label class="cl-ser" style="display: none;" id="content" >' . Html::checkbox($name, $checked, ['id'=>'vaSer', 'onClick'=>'if (this.checked) sumarMontSer('.$value.'); else restarMontSer('.$value.')' , 'label' => $label, 'value' => $value]) . '</label>';
                                                                                 }
                                                                    ])->label(false)*/ ?-->
                                    <!--input type="text" id="org"-->
                                    <!--/div-->

                                    <!--Aquí en este div es donde se muestran los servicios-->
                                    <div id="recargar"></div>
                                </div>

                        </div>
                        <div class="panel-body">

                            <?= $form->field($model, 'observaciones')->textarea(['maxlength' => true])  ?>

                            <div class="col-lg-4"><!--div id "montoSL"></div-->
                            <!--?= //$form->field($model,'montoServicio')->textInput() ?-->


                            <?= $form->field($model, 'montoServicio')->textInput(['id' => 'montoS','readonly'=>true /*, 'type' => 'hidden'*/]) ?>

                            <!-- envio el estado activo o sea 1 -->
                            <?php
                            $monto_iva = '';
                            if ($model->isNewRecord) {
                              ?>
                               <div class="estado"><?= $form->field($model, 'estado')->textInput(['value' => $estado, 'type' => 'hidden']) ?></div>
                            <?php } else { ?>
                               <div class="estado"><?= $form->field($model, 'estado')->textInput(['type' => 'hidden']) ?></div>
                            <?php
                            $total_mas_iv = $model->montoServicio + ($model->montoServicio*0.13);
                            $monto_iva = number_format($total_mas_iv,2);
                            } ?>

                            </div>
                            <div class="col-lg-4"><label>Monto + IV</label><br><div id="monto_iva"><?= '<h4>₡'.$monto_iva.'</h4>' ?></div></div>

                        </div>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <center>
                                <?php
                                if (Yii::$app->params['ot'] == true && $model->isNewRecord && $bandera == false) {
                                $responsables_ot_permitidos = [];
                                $clientes_ot_perm = Clientes::find()->where(['ot'=>'Si'])->all();
                                foreach ($clientes_ot_perm as $key => $clientes_ot) {
                                  $Ot = Ot::find()->where(['idCliente'=>$clientes_ot->idCliente])->one();
                                  if ( ($Ot->estado_ot == 'Abierta') || ( $Ot->estado_ot == 'Cerrada' && $Ot->autorizacion_ot == 'Si' )) {
                                    $responsables_ot_permitidos[] = ['idCliente'=>$clientes_ot->idCliente, 'nombreCompleto'=>$clientes_ot->nombreCompleto];
                                  }
                                }
                                echo '<div class="col-sm-12"><strong>Si esta orden de servicio cuenta para una O.T asigne un responsable, de este modo se creara la O.T en la prefactura.</strong></div>
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">'.Select2::widget([
                                //Esto lo vamos a usar sin modelo porque solo será visual
                                    'value' => '',
                                    'name' => 'cliente_ot',
                                    'data' => ArrayHelper::map($responsables_ot_permitidos, 'idCliente',
                                        function($element) {
                                        return $element['idCliente'].' - '.$element['nombreCompleto'];
                                    }),
                                    'options' => [
                                        'id'=>'ddl-cliente-ot',
                                        'placeholder' => 'Seleccione un responsable O.T si lo requite' //esto me ayuda a habilitar o desabolitar segun el caso
                                    ],
                                    'pluginOptions' => [
                                          'allowClear' => true
                                      ]
                                ]).'</div><div class="col-sm-4"></div>';
                              }
                              ?>
                                <div class="col-sm-12">
                                <p>Cuando termine de agregar o actualizar todos los servicios proceda a generar la orden</p>
                                    <?= Html::submitButton($model->isNewRecord ? 'Generar orden de servicio y pre-factura' : 'Actualizar orden de servicio', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary btn-lg', 'id' => 'generarOrdenSer', 'data-loading-text'=>'Espere...']) ?>
                                    <?php if(!$model->isNewRecord) : ?>
                                    <?= Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir Orden de Servicio', ['/orden-servicio/report', 'id'=>$model->idOrdenServicio], [
                                        'class'=>'btn btn-info btn-lg',
                                        'target'=>'_blank',
                                        'data-toggle'=>'tooltip',
                                        'title'=>'Genera un PDF de la Orden para imprimir'
                                    ]) ?>
                                    </div>
                                    <?php endif ?>
                            </center>
                        </div></div>

                        <?php ActiveForm::end(); ?>

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div><br><br><br>
        </div>
