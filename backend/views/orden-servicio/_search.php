<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\OrdenServicioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-servicio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idOrdenServicio') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'numeroOrden') ?>

    <?= $form->field($model, 'idCliente') ?>

    <?= $form->field($model, 'placa') ?>

    <?php // echo $form->field($model, 'idMecanico') ?>

    <?php // echo $form->field($model, 'radio') ?>

    <?php // echo $form->field($model, 'alfombra') ?>

    <?php // echo $form->field($model, 'herramienta') ?>

    <?php // echo $form->field($model, 'repuesto') ?>

    <?php // echo $form->field($model, 'cantOdometro') ?>

    <?php // echo $form->field($model, 'cantCombustible') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
