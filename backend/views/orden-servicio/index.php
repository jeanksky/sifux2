<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Clientes;//para obtener el cliente
use kartik\switchinput\SwitchInput;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrdenServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ordenes de servicios pendientes de atender';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
                $('#org').val(id);
            }

    // obtener la id del formulario y establecer el manejador de eventos
   $("form#delete-pass-form").on("beforeSubmit", function(e) {
       var form = $(this);
       $.post(
           form.attr("action")+"&submit=true",
           form.serialize()
       )
       .done(function(result) {
           form.parent().html(result.message);
           $.pjax.reload({container:"#solicitante-grid"});
       });
       return false;
   }).on("submit", function(e){
       e.preventDefault();
       e.stopImmediatePropagation();
       return false;
   });
</script>
<div class="orden-servicio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear nueva orden de servicio', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Crear orden de servicio para proforma ', ['create_proforma'], ['class' => 'btn btn-info']) ?>
        <?php /*echo SwitchInput::widget([
                'name' => 'status_1',
                'value'=>true
            ]);*/ ?>
    </p>
    <div class="table-responsive">
         <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <!--?php  $dataProvider->pagination->pageSize = 20; ?-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idOrdenServicio',
            'fecha',
            //'idCliente',
            [
                        'attribute' => 'tbl_clientes.nombreCompleto',
                        'label' => 'Propietario',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $cliente = Clientes::findOne($model->idCliente);
                            if($cliente == '')
                            return 'Cliente no asignado';
                            else
                            return Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$model->idCliente);
                        },
            ],
            'placa',
            [   //mónto debito
                'attribute' => 'montoServicio',
                'label' => 'Monto',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                    return number_format($model->montoServicio,2);
                },
            ],
            // 'idMecanico',
            // 'radio',
            // 'alfombra',
            // 'herramienta',
            // 'repuesto',
            // 'cantOdometro',
            // 'cantCombustible',
            // 'observaciones',
            // 'observacionesServicio',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
              /*  'update' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                        'id' => 'activity-index-link-update',
                       // 'title' => Yii::t('app', 'Update'),
                        'data-toggle' => 'modal',
                        'data-target' => '#modalUpdate',
                        'data-url' => Url::to(['update', 'id' => $model->idCliente]),
                        'data-pjax' => '0',
                        ]);
                },*/
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#'/*['delete', 'id' => $model->idCliente]*/, [
                        'id' => 'activity-index-link-delete',
                        'class' => 'activity-delete-link',
                        'title' => Yii::t('app', 'Eliminar Orden de servicio para '.$model->placa),
                        'data-toggle' => 'modal',
                        'data-target' => '#modalDelete',
                        'data-value' => 'Eliminar',
                        'onclick' => 'javascript:enviaResultado('.$model->idOrdenServicio.')',
                        'href' => Url::to('#'),
                        'data-pjax' => '0',
                        ]);
                },

            ]
          ],
        ],
    ]); ?>
</div>
    <?php //-------------------------------------------------------
    //-------------------------------Pantallas modales-------------
    //------Eliminar
            Modal::begin([
                'id' => 'modalDelete',
                'header' => '<h4 class="modal-title">Para eliminar una Orden de servicio se necesita una clave administrador</h4>',
                'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
            ]);

             $form = ActiveForm::begin([
                'method' => 'post',
                'id' => 'delete-pass-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                //'enableClientScript' => true,
            ]);
            ?>
                    <input type="hidden" id="org" name = "idOrdenServicio" />
                    <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                    <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                    <div class="form-group" style="text-align:center">
                        <?= Html::a('Eliminar Orden de servicio', ['delete'], [
                            'class' => 'btn btn-danger btn-block',
                            'name' => 'login-button',
                            'data' => [
                                'confirm' => '¿Está seguro de eliminar esta orden de servicio?',
                                'method' => 'post',
                            ],
                        ]) ?>
                         <!--?= //Html::submitButton('Ingresar', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?-->
                    </div>
            <?php  ActiveForm::end();  ?>
         <?php
            Modal::end();
            //-------------------web/index.php?r=clientes%2Fdelete&id=45
    ?>
</div>
