<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock;

//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProveedoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<div class="proveedores-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

     <p style="text-align:right">
        <?= Html::a('Crear proveedores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <div id="semitransparente">
         <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'codProveedores',
                'options'=>['style'=>'width:40px'],
            ],
            'nombreEmpresa',
            [
                'attribute' => 'telefono',
                'options'=>['style'=>'width:100px'],
            ],
            'nombreContacto',
            'observaciones',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                                'delete' => function ($url, $model, $key) {

                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                        'id' => 'activity-index-link-delete',
                                        'class' => 'activity-delete-link',
                                        'title' => Yii::t('app', 'Eliminar proveedor '.$model->nombreEmpresa),
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modalDelete',
                                        'onclick' => 'javascript:enviaResultado('.$model->codProveedores.')',
                                        'href' => Url::to('#'),
                                        'data-pjax' => '0',
                                        ]);
                                },
                             ]
            ],
        ],
    ]); ?>
</div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un proveedor necesita del permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "codProveedores" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Proveedor', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este proveedor?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
</div>
