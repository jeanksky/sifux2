<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use backend\models\ContactosPagos;

/* @var $this yii\web\View */
/* @var $model backend\models\Proveedores */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
    //envía contacto de pagos al controlador
    function agrega_contacto_pagos(){
        var codProveedores = '<?= $codProveedores ?>';
        var nombre = document.getElementById('_nombre').value;
        var telefono_ofic = document.getElementById('_telefono_ofic').value;
        var extension = document.getElementById('_extension').value;
        var celular = document.getElementById('_celular').value;
        var email = document.getElementById('_email').value;
        if (nombre =='' || telefono_ofic == '' || celular =='' || email =='') {
            alert('Por favor no deje campos vacios');
        } else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/agrega_contacto_pagos') ?>",
                type:"post",
                data: { 'codProveedores': codProveedores, 'nombre' : nombre, 'telefono_ofic': telefono_ofic, 'extension': extension, 'celular': celular, 'email': email },
                success: function(data){
                    $("#tabla_contacto_pagos").load(location.href+' #tabla_contacto_pagos','');
                    $('#_nombre').val('');
                    $('#_telefono_ofic').val('');
                    $('#_extension').val('');
                    $('#_celular').val('');
                    $('#_email').val('');
                    
                },
                error: function(msg, status,err){
                 alert('No pasa 40');
                }
            }); 
        }
    }

    //Carga en la modal actualiza_modal_pagos los datos a ser actualizados
    function actualiza_modal_pago(position){
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/actualiza_modal_contacto_pago') ?>",
            type:"post",
            data: { 'position': position },
            success: function(data){
                $( "#modal_update_pagos" ).html( data );
            },
            error: function(msg, status,err){
             alert('No pasa 56');
            }
        }); 
    }

    //funcion que me envia la orden al controlador de proseder a la actualización del contacto seleccionado
    function actualizar_contacto_pagos(){
        var codProveedores = '<?= $codProveedores ?>';
        var nombre = document.getElementById('_nombre_').value;
        var telefono_ofic = document.getElementById('_telefono_ofic_').value;
        var extension = document.getElementById('_extension_').value;
        var celular = document.getElementById('_celular_').value;
        var email = document.getElementById('_email_').value;
        var idContacto_pago = document.getElementById('_position_u').value;
        if (nombre =='' || telefono_ofic == '' || celular =='' || email =='') {
            alert('Por favor no deje campos vacios');
        } else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/actualiza_contacto_pagos') ?>",
                type:"post",
                data: { 'codProveedores': codProveedores, 'nombre' : nombre, 'telefono_ofic': telefono_ofic, 'extension': extension, 'celular': celular, 'email': email, 'idContacto_pago': idContacto_pago },
                success: function(data){
                    $("#tabla_contacto_pagos").load(location.href+' #tabla_contacto_pagos','');                    
                },
                error: function(msg, status,err){
                 alert('No pasa 86');
                }
            }); 
        }
    }

    //elimina contacto de pedidos para el controlador
    function eliminarcontactopagos(idContacto_pago){
        if(confirm("Está seguro de eliminar este contacto")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/elimina_contacto_pagos') ?>",
                type:"post",
                data: { 'idContacto_pago': idContacto_pago },
                success: function(data){
                    $("#tabla_contacto_pagos").load(location.href+' #tabla_contacto_pagos','');                    
                },
                error: function(msg, status,err){
                 alert('No pasa 101');
                }
            });
        } 
    }
</script>

<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ingrese los datos del todos los contactos de PAGOS para este proveedor.
            <?= '<div class="pull-right">' .Html::a('', '#', ['class' => 'fa fa-plus fa-2x', 'data-toggle' => 'modal', 'data-target' => '#modalnuevocontpago', 'data-placement' => 'left', 'title' => Yii::t('app', 'Crear nuevo contacto')]) . '</div>'; ?>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
            <div style="height: 280px;width: 100%; overflow-y: auto; ">
            <div id="tabla_contacto_pagos" class="pagos-form">
            <?php
                $contactos_de_pagos = ContactosPagos::find()->where(['codProveedores'=>$codProveedores])->orderBy(['idContacto_pago' => SORT_DESC])->all(); 
                echo '<table class="items table table-striped">';
                        if($contactos_de_pagos) {
                        echo '<tbody>';
                            foreach($contactos_de_pagos as $position => $contacto) { 
                                $eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-2x', 'onclick'=>'javascript:eliminarcontactopagos('.$contacto['idContacto_pago'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar contacto') ]);
                                $actualizar = Html::a('', ['', 'id' => $contacto['idContacto_pago']], [
                                                'class'=>'fa fa-pencil fa-2x',
                                                'id' => 'activity-index-link-agregacredpen',
                                                'data-toggle' => 'modal',
                                                'onclick'=>'javascript:actualiza_modal_pago("'.$contacto['idContacto_pago'].'")',
                                                'data-target' => '#modalActualizaconpago',
                                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                                'data-pjax' => '0',
                                                'title' => Yii::t('app', 'Actualizar contacto') ]);
                                $accion = $actualizar.' '.$eliminar;
                                printf('<tr><td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td class="actions button-column">%s</td></tr>',
                                        $contacto['nombre'],
                                        $contacto['telefono_ofic'],
                                        $contacto['extension'],
                                        $contacto['celular'],
                                        $contacto['email'],
                                        $accion
                                    );
                            }
                        echo '</tbody>';
                        }
                        echo '</table>';
            ?>
            </div>
            </div>
            <!-- /.panel-body -->
        </div>
		<!-- /.panel -->
	</div>
</div>
<?php
    //Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevocontpago',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Crear nuevo contacto</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '', [
                    'class'=>'btn btn-success', 
                    'onclick'=>'agrega_contacto_pagos()',
                    //'target'=>'_blank', 
                    //'data-toggle'=>'tooltip', 
                    'data-dismiss'=>'modal',
                    'title'=>'Agregar nuevo contacto'
                ]),
            ]);
            echo '<div class="well">';
            echo '<label>Nombre: </label>';
            echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => '_nombre', 'placeholder' =>' Nombre', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '<br><label>Teléfono de Oficina: </label>';
            echo \yii\widgets\MaskedInput::widget([
                'name' => '_telefono_ofic',
                'id' => '_telefono_ofic',
                'mask' => '9999-99-99',
            ]);
            echo '<br><label>Extensión: </label>';
            echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => '_extension', 'placeholder' =>' Extensión', 'name' => '_extension', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '<br><label>Celular: </label>';
            echo \yii\widgets\MaskedInput::widget([
                'name' => '_celular',
                'id' => '_celular',
                'mask' => '9999-99-99',
            ]);
            echo '<br><label>Email: </label>';
            echo \yii\widgets\MaskedInput::widget([
                'name' => '_email',
                'id' => '_email',
                'clientOptions' => ['alias' =>  'email'],
            ]);
            echo '</div>';

            Modal::end(); 
    ?>
    <?php
    //muestra modal para actualizar nota de credito pendiente
        Modal::begin([
                'id' => 'modalActualizaconpago',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Actualizar contacto</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Actualizar', '', [
                    'class'=>'btn btn-success', 
                    'onclick'=>'actualizar_contacto_pagos()',
                    //'target'=>'_blank', 
                    //'data-toggle'=>'tooltip',
                    'data-dismiss'=>'modal', 
                    'title'=>'Actualizar contacto'
                ]),
            ]);
        echo '<div class="well">';
        echo '<div id="modal_update_pagos"></div>';
        echo '</div>';

        Modal::end(); 
    ?>
