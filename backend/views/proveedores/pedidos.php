<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use backend\models\ContactosPedidos;

/* @var $this yii\web\View */
/* @var $model backend\models\Proveedores */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
    //envía contacto de pedidos al controlador
    function agrega_contacto_pedidos(){
        var codProveedores = '<?= $codProveedores ?>';
        var nombre = document.getElementById('nombre').value;
        var telefono_ofic = document.getElementById('telefono_ofic').value;
        var extension = document.getElementById('extension').value;
        var celular = document.getElementById('celular').value;
        var email = document.getElementById('email').value;
        if (nombre =='' || telefono_ofic == '' || celular =='' || email =='') {
            alert('Por favor no deje campos vacios');
        } else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/agrega_contacto_pedido') ?>",
                type:"post",
                data: { 'codProveedores': codProveedores, 'nombre' : nombre, 'telefono_ofic': telefono_ofic, 'extension': extension, 'celular': celular, 'email': email },
                success: function(data){
                    $("#tabla_contacto_pedidos").load(location.href+' #tabla_contacto_pedidos','');
                    $('#nombre').val('');
                    $('#telefono_ofic').val('');
                    $('#extension').val('');
                    $('#celular').val('');
                    $('#email').val('');
                    
                },
                error: function(msg, status,err){
                 alert('No pasa 40');
                }
            }); 
        }
    }
    //Carga en la modal actualiza_modal_pedido los datos a ser actualizados
    function actualiza_modal_pedido(position){

        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/actualiza_modal_contacto_pedido') ?>",
            type:"post",
            data: { 'position': position },
            success: function(data){
                $( "#modal_update_pedidos" ).html( data );
            },
            error: function(msg, status,err){
             alert('No pasa 56');
            }
        }); 
    }

    //funcion que me envia la orden al controlador de proseder a la actualización del contacto seleccionado
    function actualizar_contacto_pedidos(){
        var codProveedores = '<?= $codProveedores ?>';
        var nombre = document.getElementById('nombre_').value;
        var telefono_ofic = document.getElementById('telefono_ofic_').value;
        var extension = document.getElementById('extension_').value;
        var celular = document.getElementById('celular_').value;
        var email = document.getElementById('email_').value;
        var idContacto_pedido = document.getElementById('position_u').value;
        if (nombre =='' || telefono_ofic == '' || celular =='' || email =='') {
            alert('Por favor no deje campos vacios');
        } else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/actualiza_contacto_pedido') ?>",
                type:"post",
                data: { 'codProveedores': codProveedores, 'nombre' : nombre, 'telefono_ofic': telefono_ofic, 'extension': extension, 'celular': celular, 'email': email, 'idContacto_pedido': idContacto_pedido },
                success: function(data){
                    $("#tabla_contacto_pedidos").load(location.href+' #tabla_contacto_pedidos','');                    
                },
                error: function(msg, status,err){
                 alert('No pasa 86');
                }
            }); 
        }
    }
    //elimina contacto de pedidos para el controlador
    function eliminarcontactopedido(idContacto_pedido){
        if(confirm("Está seguro de eliminar este contacto")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/elimina_contacto_pedido') ?>",
                type:"post",
                data: { 'idContacto_pedido': idContacto_pedido },
                success: function(data){
                    $("#tabla_contacto_pedidos").load(location.href+' #tabla_contacto_pedidos','');                    
                },
                error: function(msg, status,err){
                 alert('No pasa 101');
                }
            });
        } 
    }
</script>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ingrese los datos del todos los contactos de PEDIDOS para este proveedor.
            <?= '<div class="pull-right">' .Html::a('', '#', ['class' => 'fa fa-plus fa-2x', 'data-toggle' => 'modal', 'data-target' => '#modalnuevocontpedido', 'data-placement' => 'left', 'title' => Yii::t('app', 'Crear nuevo contacto')]) . '</div>'; ?>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
            <div style="height: 280px;width: 100%; overflow-y: auto; ">
            <div id="tabla_contacto_pedidos" class="pedidos-form">
            <?php
                $contactos_de_pedidos = ContactosPedidos::find()->where(['codProveedores'=>$codProveedores])->orderBy(['idContacto_pedido' => SORT_DESC])->all(); 
                echo '<table class="items table table-striped">';
                        if($contactos_de_pedidos) {
                        echo '<tbody>';
                            foreach($contactos_de_pedidos as $position => $contacto) { 
                                $eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-2x', 'onclick'=>'javascript:eliminarcontactopedido('.$contacto['idContacto_pedido'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar contacto') ]);
                                $actualizar = Html::a('', ['', 'id' => $contacto['idContacto_pedido']], [
                                                'class'=>'fa fa-pencil fa-2x',
                                                'id' => 'activity-index-link-agregacredpen',
                                                'data-toggle' => 'modal',
                                                'onclick'=>'javascript:actualiza_modal_pedido("'.$contacto['idContacto_pedido'].'")',
                                                'data-target' => '#modalActualizaconpedido',
                                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                                'data-pjax' => '0',
                                                'title' => Yii::t('app', 'Actualizar contacto') ]);
                                $accion = $actualizar.' '.$eliminar;
                                printf('<tr><td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td class="actions button-column">%s</td></tr>',
                                        $contacto['nombre'],
                                        $contacto['telefono_ofic'],
                                        $contacto['extension'],
                                        $contacto['celular'],
                                        $contacto['email'],
                                        $accion
                                    );
                            }
                        echo '</tbody>';
                        }
                        echo '</table>';
            ?>
			</div>
            </div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
    <?php
    //Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevocontpedido',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Crear nuevo contacto</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '', [
                    'class'=>'btn btn-success', 
                    'onclick'=>'agrega_contacto_pedidos()',
                    //'target'=>'_blank', 
                    //'data-toggle'=>'tooltip', 
                    'data-dismiss'=>'modal',
                    'title'=>'Agregar nuevo contacto'
                ]),
            ]);
            echo '<div class="well">';
            echo '<label>Nombre: </label>';
            echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'nombre', 'placeholder' =>' Nombre', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '<br><label>Teléfono de Oficina: </label>';
            echo \yii\widgets\MaskedInput::widget([
                'name' => 'telefono_ofic',
                'id' => 'telefono_ofic',
                'mask' => '9999-99-99',
            ]);
            echo '<br><label>Extensión: </label>';
            echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'extension', 'placeholder' =>' Extensión', 'name' => 'extension', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '<br><label>Celular: </label>';
            echo \yii\widgets\MaskedInput::widget([
                'name' => 'celular',
                'id' => 'celular',
                'mask' => '9999-99-99',
            ]);
            echo '<br><label>Email: </label>';
            echo \yii\widgets\MaskedInput::widget([
                'name' => 'email',
                'id' => 'email',
                'clientOptions' => ['alias' =>  'email'],
            ]);
            echo '</div>';

            Modal::end(); 
    ?>
    <?php
    //muestra modal para actualizar nota de credito pendiente
        Modal::begin([
                'id' => 'modalActualizaconpedido',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Actualizar contacto</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Actualizar', '', [
                    'class'=>'btn btn-success', 
                    'onclick'=>'actualizar_contacto_pedidos()',
                    //'target'=>'_blank', 
                    //'data-toggle'=>'tooltip',
                    'data-dismiss'=>'modal', 
                    'title'=>'Actualizar contacto'
                ]),
            ]);
        echo '<div class="well">';
        echo '<div id="modal_update_pedidos"></div>';
        echo '</div>';

        Modal::end(); 
    ?>
