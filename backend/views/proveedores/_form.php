<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use backend\models\Moneda;

/* @var $this yii\web\View */
/* @var $model backend\models\Proveedores */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div id="semitransparente">
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ingrese los datos que se le solicitan en el siguiente formulario.
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
<div class="proveedores-form">
<?php $form = ActiveForm::begin(); ?>
<div class="col-lg-6">
    
    <div class="col-lg-8">
        <?= $form->field($model, 'nombreEmpresa')->textInput(['maxlength' => 40]) ?>
        </div>
        <div class="col-lg-4">
          <!--?=$form->field($model, 'tipoIdentidad')->radioList(['1' => 'Física', '2' => 'Jurídica'], ['item' => function($index, $label, $name, $checked, $value) {$checked = $checked == 1 ? 'checked=""' : 'disabled=""';echo "<label><input tabindex='{$index}' type='radio' {$checked}'name='{$name}'value='{$value}'> {$label}</label>";}]);?-->
          <div id="tipo">
              <?=
              $form->field($model, 'tipoIdentidad')->radioList([
                  'Física'=>'Física',
                  'Jurídica'=>'Jurídica'
                ])?>
          </div>
        </div>
    <div class="col-lg-4">
         <?= $form->field($model, 'cedula')->textInput(['maxlength' => 20]) ?>
     </div>

    <div class="col-lg-4">
        <?= $form->field($model, 'telefono')->
                        widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9999-99-99',
                    ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'telefono2')->
                        widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9999-99-99',
                    ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'telefono3')->
                        widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9999-99-99',
                    ]) ?>
    </div>
    <div class="col-lg-8">
        <?= $form->field($model, 'nombreContacto')->textInput(['maxlength' => 80]) ?> 
    </div>
    <div class="col-lg-4">
    <!--?= $form->field($model, 'idMoneda')->textInput(['maxlength' => true]) ?-->
     <?= $form->field($model, 'idMoneda')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Moneda::find()->all(), 'idTipo_moneda', function($element) {
                                return $element['simbolo'].' - '.$element['descripcion'];
                            }),

                    'language'=>'es',
                    'options' => ['id'=>'loo' , 'placeholder' => '- Seleccione -'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Moneda'); ?>
    </div>
</div>
<div class="col-lg-6">
    <div class="col-lg-6">
        <?= $form->field($model, 'notific_cntc_pagos')->radioList(array('Si'=>'SI','No'=>'No'))?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'notific_cntc_pedidos')->radioList(array('Si'=>'SI','No'=>'No'))?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'diasCredito')->textInput(['maxlength' => 2 , 'type'=>"number"]) ?>
    </div>
    <div class="col-lg-9">
        <?= $form->field($model, 'observaciones')->textarea(['maxlength' => 130]) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Ingresar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
</div>