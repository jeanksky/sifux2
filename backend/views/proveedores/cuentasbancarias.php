<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use backend\models\CuentasBancariasProveedor;

/* @var $this yii\web\View */
/* @var $model backend\models\Proveedores */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
    //envía contacto de pagos al controlador
    function asignar_cuenta_bancaria(){
        var codProveedores = '<?= $codProveedores ?>';
        var entidadbancaria = document.getElementById('entidadbancaria').value;
        var descripcion = document.getElementById('descripcion').value;
        var cuentab = document.getElementById('cuentab').value;
        if (entidadbancaria =='' || descripcion == '' || cuentab =='' ) {
            alert('Por favor no deje campos vacios');
        } else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/asignar_cuenta_bancaria') ?>",
                type:"post",
                data: { 'codProveedores': codProveedores, 'entidadbancaria' : entidadbancaria, 'descripcion': descripcion, 'cuentab': cuentab },
                success: function(data){
                    $("#tabla_cuentas_bancarias").load(location.href+' #tabla_cuentas_bancarias','');
                    $('#entidadbancaria').val('');
                    $('#descripcion').val('');
                    $('#cuentab').val('');

                },
                error: function(msg, status,err){
                 alert('No pasa 40');
                }
            });
        }
    }

    //Carga en la modal actualiza_modal_cuenta_bancaria los datos a ser actualizados
    function actualiza_modal_cuenta_bancaria(position){
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/actualiza_modal_cuenta_bancaria') ?>",
            type:"post",
            data: { 'position': position },
            success: function(data){
                $( "#modal_update_cuenta_bancaria" ).html( data );
            },
            error: function(msg, status,err){
             alert('No pasa 56');
            }
        });
    }

    //funcion que me envia la orden al controlador de proseder a la actualización del contacto seleccionado
    function actualizar_cuenta_bancaria(){
        var codProveedores = '<?= $codProveedores ?>';
        var entidadbancaria = document.getElementById('_entidadbancaria').value;
        var descripcion = document.getElementById('_descripcion').value;
        var cuentab = document.getElementById('_cuentab').value;
        var id = document.getElementById('_position_u_').value;
        if (entidadbancaria =='' || descripcion == '' || cuentab =='') {
            alert('Por favor no deje campos vacios');
        } else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/actualizar_cuenta_bancaria') ?>",
                type:"post",
                data: { 'codProveedores': codProveedores, 'entidadbancaria' : entidadbancaria, 'descripcion': descripcion, 'cuentab': cuentab, 'id': id },
                success: function(data){
                    $("#tabla_cuentas_bancarias").load(location.href+' #tabla_cuentas_bancarias','');
                },
                error: function(msg, status,err){
                 alert('No pasa 86');
                }
            });
        }
    }

    //elimina contacto de pedidos para el controlador
    function elimina_cuenta_bancaria(id){
        if(confirm("Está seguro de eliminar esta cuenta")){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proveedores/elimina_cuenta_bancaria') ?>",
                type:"post",
                data: { 'id': id },
                success: function(data){
                    $("#tabla_cuentas_bancarias").load(location.href+' #tabla_cuentas_bancarias','');
                },
                error: function(msg, status,err){
                 alert('No pasa 101');
                }
            });
        }
    }
</script>

<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ingrese los datos de las cuentas bancarias para este proveedor.
            <?= '<div class="pull-right">' .Html::a('', '#', ['class' => 'fa fa-plus fa-2x', 'data-toggle' => 'modal', 'data-target' => '#modalnuevacuenta', 'data-placement' => 'left', 'title' => Yii::t('app', 'Asignar nueva cuenta')]) . '</div>'; ?>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
            <div style="height: 280px;width: 100%; overflow-y: auto; ">
            <div id="tabla_cuentas_bancarias" class="pagos-form">
            <?php
                $cuentasbancarias = CuentasBancariasProveedor::find()->where(['codProveedores'=>$codProveedores])->orderBy(['id' => SORT_DESC])->all();
                echo '<table class="items table table-striped">';
                        if($cuentasbancarias) {
                        echo '<tbody>';
                            foreach($cuentasbancarias as $position => $cuenta) { 
                                $eliminar = Html::a('', '#', ['class'=>'fa fa-remove fa-2x', 'onclick'=>'javascript:elimina_cuenta_bancaria('.$cuenta['id'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Eliminar cuenta') ]);
                                $actualizar = Html::a('', ['', 'id' => $cuenta['id']], [
                                                'class'=>'fa fa-pencil fa-2x',
                                                'id' => 'activity-index-link-agregacredpen',
                                                'data-toggle' => 'modal',
                                                'onclick'=>'javascript:actualiza_modal_cuenta_bancaria("'.$cuenta['id'].'")',
                                                'data-target' => '#modalActualizacuentab',
                                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                                'data-pjax' => '0',
                                                'title' => Yii::t('app', 'Actualizar cuenta') ]);
                                $accion = $actualizar.' '.$eliminar;
                                printf('<tr><td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td><font face="arial" size=4>%s</font></td>
                                        <td class="actions button-column">%s</td></tr>',
                                        $cuenta['entidad_bancaria'],
                                        $cuenta['descripcion'],
                                        $cuenta['cuenta_bancaria'],
                                        $accion
                                    );
                            }
                        echo '</tbody>';
                        }
                        echo '</table>';
            ?>
            </div>
            </div>
            <!-- /.panel-body -->
        </div>
		<!-- /.panel -->
	</div>
</div>
<?php
    //Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevacuenta',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Asignar nueva cuenta bancaria</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '', [
                    'class'=>'btn btn-success',
                    'onclick'=>'asignar_cuenta_bancaria()',
                    //'target'=>'_blank',
                    //'data-toggle'=>'tooltip',
                    'data-dismiss'=>'modal',
                    'title'=>'Agregar nueva cuenta'
                ]),
            ]);
            echo '<div class="well">';
            echo '<label>Entidad bancaria: </label>';
            echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'entidadbancaria', 'placeholder' =>' Entidad bancaria', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            ;
            echo '<br><label>Descripción: </label>';
            echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'descripcion', 'placeholder' =>' Descripción', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '<br><label>Cuenta bancaria: </label>';
            echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'cuentab', 'placeholder' =>' Cuenta bancaria', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '</div>';

            Modal::end();
    ?>
    <?php
    //muestra modal para actualizar nota de credito pendiente
        Modal::begin([
                'id' => 'modalActualizacuentab',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Actualizar cuenta bancaria</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Actualizar', '', [
                    'class'=>'btn btn-success',
                    'onclick'=>'actualizar_cuenta_bancaria()',
                    //'target'=>'_blank',
                    //'data-toggle'=>'tooltip',
                    'data-dismiss'=>'modal',
                    'title'=>'Actualizar cuenta bancaria'
                ]),
            ]);
        echo '<div class="well">';
        echo '<div id="modal_update_cuenta_bancaria"></div>';
        echo '</div>';

        Modal::end();
    ?>
