<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\ContactosPedidos;
use backend\models\ContactosPagos;
/* @var $this yii\web\View */
/* @var $model backend\models\Proveedores */

$this->title = 'Actualizar Proveedor: ' . ' ' . $model->nombreEmpresa;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombreEmpresa, 'url' => ['view', 'id' => $model->codProveedores]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//Me pinta el titulo con negro transparente
    $(document).ready(function(){
        $('[data-toggle="modal"]').tooltip();
    }); 
</script>
<div class="proveedores-update">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>
<ul class="nav nav-tabs nav-justified">
                <li class="active" role="presentation" ><a href="#empresa" data-toggle="tab">Datos de la empresa</a></li>
                <li role="presentation"><a href="#pedidos" data-toggle="tab">Contactos de Pedidos</a></li>
                <li role="presentation"><a href="#pagos" data-toggle="tab">Contactos de Pagos</a></li>
                <li role="presentation"><a href="#cuentasbancarias" data-toggle="tab">Cuentas bancarias</a></li>
</ul>
<div class="table-responsive tab-content">
    <div class="tab-pane fade in active" id="empresa"><br>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
    <div class="tab-pane fade" id="pedidos"><br>
        <?php $codProveedores = $model->codProveedores; ?>
        <?= $this->render('pedidos', [
            'codProveedores' => $codProveedores,
        ]) ?>
    </div>
    <div class="tab-pane fade" id="pagos"><br>
        <?= $this->render('pagos', [
                'codProveedores' => $codProveedores,
        ]) ?>
    </div>
    <div class="tab-pane fade" id="cuentasbancarias"><br>
        <?= $this->render('cuentasbancarias', [
                'codProveedores' => $codProveedores,
        ]) ?>
    </div>
</div>

    

</div>
