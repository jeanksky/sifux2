<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\DetalleCompra;
use backend\models\ProductoServicios;
use backend\models\Proveedores;
use backend\models\Usuarios;

/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */

$this->title = $model->idCompra;
$this->params['breadcrumbs'][] = ['label' => 'Compras Inventarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->all();
$product = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->one();
$user = Usuarios::find()->where(["=",'username', $product->usuario])->one();
$provee = Proveedores::findOne($model->idProveedor);
?>
<div class="compras-inventario-view">

    <h2>Detalle de la compra</h2>

<strong>Proveedor: </strong><?= $provee->nombreEmpresa ?><br>
<strong>No Factura: </strong><?= $model->numFactura ?><br>
<strong>Usuario que registra compra: </strong><?= $product->usuario ?> - <?= $user->nombre ?><br><br>
<strong>Nombre de empleado que ubica: </strong>__________________________________

<div class="col-lg-12" id="conten_prom">
            <br>
            <?php
            //http://www.programacion.com.py/tag/yii
                //echo '<div class="grid-view">';
                echo '<table style="font-size:10px;" class="items table table-striped">';
                echo '<thead>';
                printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr>',
                        'CÓDIGO',
                        'COD.PROV',
                        'PRODUCTO',
                        'ANTES',
                        'ENTRADA',
                        'AHORA',
                        'UBICACIÓN'
                        );
                echo '</thead>';
            //if($products) {
            ?>

            <?php
            if($products) {
            echo '<tbody>';
                foreach($products as $position => $product) {
                $nueva_cantidad = $product['cantidadAnterior'] + $product['cantidadEntrada'];
                $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                            //$product['noEntrada'],
                            $product['codProdServicio'],
                            $product['codigo_proveedor'],
                            $modelpro->nombreProductoServicio,
                            $product['cantidadAnterior'],
                            $product['cantidadEntrada'],
                            $nueva_cantidad,
                            //number_format($product['precioCompra'],2),
                            //$product['descuento'],
                            //$product['porcentUtilidad'],
                            //number_format($product['preciosinImp'],2),
                            //number_format($product['precioconImp'],2),
                            $modelpro->ubicacion
                            );
                        //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/


                }
            echo '</tbody>';
        }
            ?>
            <?php
            //}
                echo '</table>';
            ?>
            </div>
</div>
