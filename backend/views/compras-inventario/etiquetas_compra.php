<?php
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
use backend\Zebra\src\Client;
use backend\Zebra\src\Zpl\Builder;
use backend\Zebra\src\Zpl\Image;
use backend\models\Empresa;
use backend\models\ComprasInventario;
use yii\helpers\Url;
use backend\models\DetalleCompra;
use backend\models\ProductoServicios;


$empresa_funcion = new Empresa();
$model = ComprasInventario::findOne($id);
//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
    $urlParts = parse_url($_SERVER['REQUEST_URI']);

    if (isset($urlParts['query'])){
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){
          ob_end_clean();
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//Se obtienen datos de la empresa
            $products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->all();//Se obtienen datos del detalle de compra
            if ($products) {
              $nombre_impresora = 'ETIQUETASNPS';
                $cmds =  "";//declaro variable que obtiene el contenido de todas las etiquetas a imprimir
                $image = new Image(file_get_contents('../web/' . $empresa->logo_etiqueta));//obtenemos la imagen de acuerdo a la direccion
                $zpl = new Builder(); //creamos una instancia de donde vamos a corregir a la imagen en codigo zpl
                $zpl->fo(30, 10)->gf($image)->fs(); //convertimos la imagen y de lamos una pocisión en el mapa de la etiqueta
                foreach($products as $position => $product) {//recorro todos los productos que serán pasados por etiquetas
                    $producto = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();//obtengo los datos de cada producto que necesito imprimir en la etiqueta
                    //for ($i=1 ; $i<=$producto->cantidadInventario  ;$i++) { //recorro la cantidad de ese producto que esta en inventario
                    $precio_venta = $producto->exlmpuesto == 'Si' ? $producto->precioVenta : $producto->precioVentaImpuesto;
                        //Estos serían los comandos Zebra ZPL para moldear la etiqueta
                        $cmds .= "^XA";//inicio de etiqueta
                        $cmds .= "^LH10,20";//con solo 10 punto de ancho con 20 de alto inicio de etiqueta
                        $cmds .= "^FO20,1^GB750,175,4^FS";//primer borde de primer fila
                        $cmds .= "^FO20,1^GB750,360,4^FS";//segundo borde de segunda fila
                        $cmds .= "^FO20,171^GB320,190,4^FS";//linea verticar de la segunda fila

                        $cmds .= substr(strval($zpl), 3, -3);

                        $cmds .= "^FO280,30^ADN,18,10^FD FECHA REGISTRO: ^FS";//f1_b
                        $cmds .= "^FO280,55^ADN,36,20^FD ".$model->fechaRegistro." ^FS";//f1_c

                        $cmds .= "^FO280,100^ADN,18,10^FD DESCRIPCION: ^FS";//f1_d
                        $cmds .= "^FO280,125^ADN,36,20^FD ".substr($producto->nombreProductoServicio, 0, 18)." ^FS";//f1_E

                        $cmds .= "^FO30,200^ADN,18,10^FD UBI: ^FS";//f2_c1_a
                        $cmds .= "^FO30,225^ADN,36,20^FD ".$producto->ubicacion." ^FS";//f2_c1_b

                        $cmds .= "^FO30,280^ADN,18,10^FD PRECIO COLONES: ^FS";//f2_c1_c
                        $cmds .= "^FO30,305^ADN,36,20^FD ".number_format($precio_venta,2)." ^FS";//f2_c1_d

                        $cmds .= "^FO340,200^ADN,18,10^FD CODIGO:  (COD.PROV: ".$product['codigo_proveedor'].") ^FS";//f2_c2_a
                        $cmds .= "^FO340,225^ADN,36,20^FD ".$product['codProdServicio']." ^FS";//f2_c2_b

                        $cmds .= "^FO400,260^BY1^B3N,,80^FD".$product['codProdServicio']."^FS";//f2_c2_c

                        //$cmds .= "^PQ1,0,1,Y"; //mantiene un control de constancia por la impreción de varias etiquetas.
                        $cmds .= "^PQ".$product['cantidadEntrada'];//imprime cantidad de la compra ** $producto->cantidadInventario;//imprime la cantidad de esta etiqueta en inventario
                        $cmds .= "^XZ";//cierre de la etiqueta
                    //}//fin for
                }//fin foreach
            }//fin if $products
            else {
                $cmds .= "^XA";//inicio de etiqueta
                $cmds .= "^FO30,200^ADN,18,10^FD NO SE ENCUENTRAN DATOS DE etiquetas_print ".$products." ^FS";//f2_c1_a
                $cmds .= "^XZ";//cierre de la etiqueta
            }
            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente
            $cpj->clientPrinter = new InstalledPrinter($nombre_impresora);


            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();

            echo $cpj->sendToClient();

            ob_end_flush();
            exit();
        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de etiqueta-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//Especifique la URL ABSOLUTA al archivo php que creará el objeto ClientPrintJob
//En este caso, esta misma página
$webClientPrintControllerAbsoluteURL = Utils::getRoot().$empresa_funcion->getWCP();
$demoPrintCommandsProcessAbsoluteURL = Utils::getRoot().Url::home().'?r=compras-inventario/etiquetas_compra&id='.$id;
echo WebClientPrint::createScript($webClientPrintControllerAbsoluteURL, $demoPrintCommandsProcessAbsoluteURL, Yii::$app->user->identity->last_session_id);

 ?>
