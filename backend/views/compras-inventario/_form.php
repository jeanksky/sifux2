<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\TipoCambio;
use backend\models\Proveedores;
use backend\models\OrdenCompraProv;
use backend\models\ComprasInventario;
use backend\models\CompraInventarioSession;
use backend\models\ProductoServicios;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\money\MaskMoney;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use backend\models\DetalleCompra;
use yii\widgets\MaskedInput;
use backend\models\Moneda;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $products = CompraInventarioSession::getContenidoProducto();
    $moned_lo = Moneda::find()->where(['monedalocal'=>'x'])->one();
    $moneda_local = $moned_lo->simbolo;
    $moneda_prov = '°';
    if ($proveedor = Proveedores::find()->where(['=', 'codProveedores', CompraInventarioSession::getProveedor()])->one()) {
      $moneda = Moneda::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->one();

      if (@$moneda) {
       $moneda_prov = $moneda->simbolo;
      }
    }


 ?>
 <?php
 $subTotal_productos = 0;
 $subtotalmenosdescuento = 0;
 $impuesto_productos = 0;
 $total_productos = 0;
 $impuesto = Yii::$app->params['porciento_impuesto'];
 if($products) {
   foreach($products as $position => $product) {
     $subTotal_productos_ = $products[$position]['precioCompra'] * $products[$position]['cantidadEntrada'];
     $subtotalmenosdescuento_ = ($products[$position]['precioCompra'] * $products[$position]['descuentu'] / 100) * $products[$position]['cantidadEntrada'];
     $subTotal_productos += $products[$position]['precioCompra'] * $products[$position]['cantidadEntrada'];
     $subtotalmenosdescuento += ($products[$position]['precioCompra'] * $products[$position]['descuentu'] / 100) * $products[$position]['cantidadEntrada'];
     $impuesto_productos += $products[$position]['exImpuesto'] == 'Si' ? 0 : ($subTotal_productos_ - $subtotalmenosdescuento_) * $impuesto / 100;
   }

   //$subtotalmenosdescuento = $subTotal_productos - $descuento_productos;
   //$impuesto_productos = $subtotalmenosdescuento * $impuesto / 100;
   $total_productos = $subTotal_productos - $subtotalmenosdescuento + $impuesto_productos;
   /*CompraInventarioSession::setSubtotal($subTotal_productos);
   CompraInventarioSession::setDescuento($descuento_productos);
   CompraInventarioSession::setImpuesto($impuesto_productos);
   CompraInventarioSession::setTotal($total_productos);*/
 }
 ?>
<?php $form = ActiveForm::begin([
        //'enableAjaxValidation'=>true
    ]); ?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->
<script type="text/javascript">
    $(document).ready(function () {
      $('#btn_aplicar_porc_masivo').addClass('disabled');
    });
    $(document).ready(function () {
      //me checa todas las facturas pendientes
      div = document.getElementById('porcentaje_masivo');
      $('#checkAll').on('change', function() {
          if (this.checked == true) {
              div.style.display = '';
              $('#tabla_producto_compra').find('input[name="checkboxRow[]"]').prop('checked', true);
              $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
              $("#descuento_masivo").prop('disabled', true);//input desabilitados
              document.getElementById('descuento_masivo').value = '';//input vacidos
              $("#utilidad_masiva").prop('disabled', true);//input desabilitados
              document.getElementById('utilidad_masiva').value = '';//input vacidos
              $('input[name=descuento_masivo_check]').attr('checked', false);//check desmarcado
              $('input[name=utilidad_masiva_check]').attr('checked', false);//check desmarcado
            }
          else {
              div.style.display = 'none';
              $('#tabla_producto_compra').find('input[name="checkboxRow[]"]').prop('checked', false);
            }
      });
      //accion para abilitar o desabilitar el input de descuento masivo
      $('#descuento_masivo_check').on('click', function() {
        var utilidad_masiva = document.getElementById('utilidad_masiva').value;
        if (this.checked == true){
          $("#descuento_masivo").prop('disabled', false);
        } else {
          $("#descuento_masivo").prop('disabled', true);
          document.getElementById('descuento_masivo').value = '';
          if (utilidad_masiva=="") {//compruebo que el campo de utilidad masiva esté vacio para decir que si puedo desabilitar el boton
            $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
          }
        }
      });
      //accion para abilitar o desabilitar el input de utilidad masivo
      $('#utilidad_masiva_check').on('click', function() {
        var descuento_masivo = document.getElementById('descuento_masivo').value;
        if (this.checked == true){
          $("#utilidad_masiva").prop('disabled', false);
        } else {
          $("#utilidad_masiva").prop('disabled', true);
          document.getElementById('utilidad_masiva').value = '';
          if (descuento_masivo=="") {//compruebo que el campo de descuento masivo esté vacio para decir que si puedo desabilitar el boton
            $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
          }
        }
      });
    });

    //me permite ocultar el area div_costo_fletes
    function div_costo_fletes() {
        div_fletes = document.getElementById('div_costo_fletes');
        if (div_fletes.style.display != 'none') { div_fletes.style.display = 'none'; } else { div_fletes.style.display = ''; }
    }

    function obtenermontoflete() {
      var fletes = document.getElementById('fletes').value;
      var impuesto_productos = '<?= $impuesto_productos ?>';
      var total_productos = '<?= $total_productos ?>';
      var checkboxValues = "";
      $('input[name="check_iv_fletes"]:checked').each(function() {//
          checkboxValues += $(this).val() + ",";//
      });//
      if (checkboxValues=='') {
        impuesto_flete = '';
      } else {
        impuesto_flete = 'si';
      }
      $.ajax({
         url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenermontoflete') ?>",
         type:"post",
         dataType: 'json',
         data: { 'fletes': fletes, 'impuesto_flete': impuesto_flete,
         'impuesto_productos' : impuesto_productos, 'total_productos' : total_productos },
         success: function(data){
           /*$('.monto_impuesto_productos').val(data.impuestos);
           $('.monto_total_productos').val(data.total);*/
           $( "#div_impuesto_productos" ).html( data.impuestos );
           $( "#div_total_productos" ).html( data.total );
         }
      });
    }

    function guardarcompra() {
    fletes = document.getElementById('fletes').value;
      var checkboxValues = "";
      $('input[name="check_iv_fletes"]:checked').each(function() {//
          checkboxValues += $(this).val() + ",";//
      });//
      if (checkboxValues=='') {
        impuesto_flete = '';
      } else {
        impuesto_flete = 'si';
      }
      $.ajax({
         url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/guardarcompra') ?>",
         type:"post",
         data: { 'fletes': fletes, 'impuesto_flete': impuesto_flete },
         success: function(idCompra){
           window.location.href = '<?= Yii::$app->getUrlManager()->createUrl('compras-inventario/update') ?>'+'&id='+idCompra+'&carga=0';
         }
      });
    }

    //comprueba si todos los check estan marcados o desmarcados
    function comprobar_check_marcado() {
      var checkboxValues = "";
      $('input[name="checkboxRow[]"]:checked').each(function() {//
          checkboxValues += $(this).val() + ",";//
      });//
      if (checkboxValues=='') {
        div.style.display = 'none';
        $('input[name=checkAll]').attr('checked', false);
        $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
        $("#descuento_masivo").prop('disabled', true);//input desabilitados
        document.getElementById('descuento_masivo').value = '';//input vacidos
        $("#utilidad_masiva").prop('disabled', true);//input desabilitados
        document.getElementById('utilidad_masiva').value = '';//input vacidos
        $('input[name=descuento_masivo_check]').attr('checked', false);//check desmarcado
        $('input[name=utilidad_masiva_check]').attr('checked', false);//check desmarcado
      } else {
        div.style.display = '';
      }
    }
        //espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
      var nav4 = window.Event ? true : false;
      var key = nav4 ? evt.which : evt.keyCode;
      return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

    //desde los input de los poprcentajes abilito el boton de aplicar % mesivo cuando almenos uno esté con un valor
    function habilitarbtn_aplica_masivo() {
      var descuento_masivo = document.getElementById('descuento_masivo').value;
      var utilidad_masiva = document.getElementById('utilidad_masiva').value;
      if (descuento_masivo == '' && utilidad_masiva == '') {
        $('#btn_aplicar_porc_masivo').addClass('disabled');
      } else {
        $('#btn_aplicar_porc_masivo').removeClass('disabled');
      }
    }

    //obtengo los productos marcados para actualizar los porcentajes
    function obtener_productos_marcados(btn_prod_marc){
        var checkboxValues = "";
        $('input[name="checkboxRow[]"]:checked').each(function() {//
            checkboxValues += $(this).val() + ",";//
        });//
        //eliminamos la última coma.
        checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//
        var descuento_masivo = document.getElementById('descuento_masivo').value;
        var utilidad_masiva = document.getElementById('utilidad_masiva').value;
        $.ajax({
                url:"<?= Yii::$app->getUrlManager()->createUrl('compras-inventario/obtener_productos_marcados_create') ?>",
                type:"post",
                data: { 'checkboxValues' : checkboxValues, 'descuento_masivo' : descuento_masivo,
                        'utilidad_masiva' : utilidad_masiva },
                beforeSend: function() { //mientras se ejecuran los cambios pongo el boton en espera
                  var $btn = $(btn_prod_marc).button('loading');
                        // simulating a timeout
                  setTimeout(function () { $btn.button('reset'); }, 60000);
                },
                success: function(notif){
                },
                error: function(msg, status,err){
                 //alert('No pasa 36');
                }
            });
    }
</script>
<div class="compras-inventario-form">
    <div class="well col-lg-12">
    <div id="conten_pro" class="">
        <div class="col-lg-12">
            <div class="col-lg-3">
                <?php if (@$idProveedor = CompraInventarioSession::getProveedor()) {
                    $model->idProveedor = $idProveedor;
                    echo $form->field($model, 'idProveedor')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Proveedores::find()->all(),'codProveedores',
                        function($element) {
                            return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                        }),
                    'language'=>'es',
                    'disabled' => true,
                    'options' => ['id'=>'idProveedor', 'placeholder' => '- Seleccione -'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Proveedor');
                } else {
                    echo $form->field($model, 'idProveedor')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Proveedores::find()->all(),'codProveedores',
                        function($element) {
                            return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                        }),
                    'language'=>'es',
                    'options' => ['id'=>'idProveedor' ,'placeholder' => '- Seleccione -'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Proveedor');
                    } ?>
            </div>
            <div class="col-lg-3">
                <?php if (@$numFactura = CompraInventarioSession::getNofactura()) {
                        $model->numFactura = $numFactura;
                        echo $form->field($model, 'numFactura')->textInput(['onkeypress'=>''])->textInput(['readonly' => true]);
                } else {
                        echo $form->field($model, 'numFactura')->textInput(['onkeypress'=>'']);
                    } ?>
                <!--?php
                    echo '<strong>Número de Factura</strong>';
                    echo Html::input('text', 'new-numFactura', '', ['id' => '', 'class' => 'form-control', 'onkeypress'=>'return isNumber(event)']);
                ?-->
            </div>
            <div class="col-lg-3">
              <?php if (@$n_interno_prov = CompraInventarioSession::getInterno_prov()) {
                      $model->n_interno_prov = $n_interno_prov;
                      echo $form->field($model, 'n_interno_prov')->textInput(['onkeypress'=>''])->textInput(['readonly' => true]);
              } else {
                      echo $form->field($model, 'n_interno_prov')->textInput(['onkeypress'=>'']);
                  } ?>
            </div>
            <div class="col-lg-3">
              <?php if (@$products) {
                $style = 'display: none;';

                echo $form->field($model, 'fletes', [
                   'inputTemplate' => '<div class="form-group input-group" id="div_costo_fletes" style="'.$style.'">
                   <span class="input-group-addon">
                   <i class="fa fa-plus-circle"></i> <span class="fa fa-truck"></span>
                   </span>
                    {input}
                   <span class="input-group-addon" title="El check marcado significa que se aplica impuesto al flete">
                     <input type="checkbox" name="check_iv_fletes" onclick="obtenermontoflete()" >
                   </span></div>',
                   ])->widget(\yii\widgets\MaskedInput::className(), [
                       //'name' => 'input-33',
                       'options' => ['id'=>'fletes','onkeyup'=>'javascript:obtenermontoflete()',
                       'class'=>'form-control', "onkeypress"=>"return isNumberDe(event)",'placeholder' =>'Monto por fletes',
                        'style'=>"font-family: Fantasy; font-size: 16pt; text-align: center;"],
                       'clientOptions' => [
                           'alias' =>  'decimal',
                           'groupSeparator' => ',',
                           'autoGroup' => true
                       ],
                   ])->label('<a href="javascript:div_costo_fletes();">Incluir costos por fletes</a> (incluir al final antes de aplicar compra)');
              } ?>
            </div>

        </div>
        <div class="col-lg-12">
            <div class="col-lg-3">
                <div id="pago">
                    <?php if (@$formaPago = CompraInventarioSession::getFormapago()) {
                        $model->formaPago = $formaPago;
                        echo $form->field($model, 'formaPago')->radioList(['Contado' => 'Contado', 'Crédito' => 'Crédito'])->textInput(['readonly' => true]);
                    }
                    else {
                        //$model->formaPago = 'Crédito';
                        $model->formaPago = $model->isNewRecord ? 'Crédito' : $model->formaPago;
                        echo $form->field($model, 'formaPago')->radioList(['Contado' => 'Contado', 'Crédito' => 'Crédito']);
                        } ?>
                </div>
            </div>
            <div class="col-lg-3">
            <?php if (@$fechaDocumento = CompraInventarioSession::getFechadocumento() ) {
                $model->fechaDocumento = $fechaDocumento;
                echo $form->field($model,'fechaDocumento')-> widget(DatePicker::className(),[
                                                           'name' => 'check_issue_date',
                                                            'disabled' => true,
                                                            'options' => ['id' => 'fecDoc', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                            'pluginOptions' => [
                                                                'format' => 'dd-m-yyyy',
                                                                'todayHighlight' => true,
                                                                'autoclose' => true,
                                                            ]
                                                        ]);
            } else {
                echo $form->field($model,'fechaDocumento')-> widget(DatePicker::className(),[
                                                           'name' => 'check_issue_date',
                                                            'disabled' => false,
                                                            'options' => ['id' => 'fecDoc', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                            'pluginOptions' => [
                                                                'format' => 'dd-m-yyyy',
                                                                'todayHighlight' => true,
                                                                'autoclose' => true,
                                                            ]
                                                        ]);
                } ?>
            </div>
            <div class="col-lg-3">
            <?php if (@$fechaRegistro = CompraInventarioSession::getFecharegistro() ) {
                $model->fechaRegistro = $fechaRegistro;
                echo $form->field($model,'fechaRegistro')-> widget(DatePicker::className(),[
                                                           'name' => 'check_issue_date',
                                                            'disabled' => true,
                                                            //'options' => ['id' => 'fecRe', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                            'pluginOptions' => [
                                                                'format' => 'dd-m-yyyy',
                                                                'todayHighlight' => true,
                                                                'autoclose' => true,
                                                            ]
                                                        ]);
            } else {
                $model->fechaRegistro = $model->isNewRecord ? date('d-m-Y') : $model->fechaRegistro;
                echo $form->field($model,'fechaRegistro')-> widget(DatePicker::className(),[
                                                           'name' => 'check_issue_date',
                                                            'disabled' => false,
                                                            //'options' => ['id' => 'fecRe', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                            'pluginOptions' => [
                                                                'format' => 'dd-m-yyyy',
                                                                'todayHighlight' => true,
                                                                'autoclose' => true,
                                                            ]
                                                        ]);
                } ?>
            </div>
            <div class="col-lg-3">
              <?php if (CompraInventarioSession::getFormapago() ) {
                      if(@$fechaVencimiento = CompraInventarioSession::getFechavencimiento())
                      {
                          $model->fechaVencimiento = $fechaVencimiento;
                          echo $form->field($model,'fechaVencimiento')-> widget(DatePicker::className(),[
                                                                 'name' => 'check_issue_date',
                                                                  'disabled' => true,
                                                                  'options' => ['id' => 'fecVe'],
                                                                  'pluginOptions' => [
                                                                      'format' => 'dd-m-yyyy',
                                                                      'todayHighlight' => true,
                                                                      'autoclose' => true,
                                                                  ]
                                                              ])->label('Fecha Vencimiento (Crédito)');
                              } else { echo ""; }

              } else{
                  echo '<div id="contenido_a_mostrar">';
                  $model->fechaVencimiento = date('d-m-Y');
                  echo $form->field($model,'fechaVencimiento')->widget(yii\jui\DatePicker::className(),[
                                                             'name' => 'check_issue_date',
                                                              //'disabled' => false,
                                                              'options' => ['class'=>'form-control', 'id' => 'fecVe'],
                                                              /*'pluginOptions' => [
                                                                  'format' => 'dd-m-yyyy',
                                                                  'todayHighlight' => true,
                                                                  'autoclose' => true,
                                                              ]*/
                                                          ])->label('Fecha Vencimiento (Crédito)');
                  echo '</div>';
                  } ?>
            </div>
        </div>

        <div class="col-lg-12"><br>
            <div class="col-lg-3">
                <?php
                $tipo_cambio = TipoCambio::find()->where(['usar'=>'x'])->one();
                if (@$subTotal = CompraInventarioSession::getSubtotal()) {
                    $model->subTotal = $subTotal;
                    echo $form->field($model, 'subTotal')->widget(MaskMoney::classname(), [
                                                        'options' => ['style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                        'disabled' => true,
                                                        'pluginOptions' => [
                                                            'prefix' => $moneda_prov,
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ]
                                                    ]);
                    $precio_dol_subtot = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($subTotal_productos / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                    echo '<div style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($subTotal_productos,2).' '.$precio_dol_subtot.'</div>';
                } else {
                  echo $form->field($model, 'subTotal')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['id'=>'subTotal','class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]);
                    } ?>
            </div>
            <div class="col-lg-3">
                <?php if (@$descuento = CompraInventarioSession::getDescuento()) {
                    $model->descuento = $descuento;
                    echo $form->field($model, 'descuento')->widget(MaskMoney::classname(), [
                                                        'options' => ['style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                        'disabled' => true,
                                                        'pluginOptions' => [
                                                            'prefix' => $moneda_prov,
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ]
                                                    ]);
                    $precio_dol_desc = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($subtotalmenosdescuento / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                    echo '<div style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($subtotalmenosdescuento,2).' '.$precio_dol_desc.'</div>';
                } else {
                    echo $form->field($model, 'descuento')->widget(\yii\widgets\MaskedInput::className(), [
                        //'name' => 'input-33',
                        'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                        'clientOptions' => [
                            'alias' =>  'decimal',
                            'groupSeparator' => ',',
                           // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                            'autoGroup' => true
                        ],
                    ]);
                  } ?>

            </div>
            <div class="col-lg-3">
                <?php if (@$impuesto = CompraInventarioSession::getImpuesto()) {
                    $model->impuesto = $impuesto;
                    echo $form->field($model, 'impuesto')->widget(MaskMoney::classname(), [
                                                        'options' => ['class'=>'monto_impuesto_productos','style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                        'disabled' => true,
                                                        'pluginOptions' => [
                                                            'prefix' => $moneda_prov,
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ]
                                                    ]);
                    $precio_dol_impu = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($impuesto_productos / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                    echo '<div id="div_impuesto_productos" style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($impuesto_productos,2).' '.$precio_dol_impu.'</div>';
                } else {
                  echo $form->field($model, 'impuesto')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['id'=>'impuesto','class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]);
                    } ?>

            </div>
            <div class="col-lg-3">
                <?php if (@$total = CompraInventarioSession::getTotal()) {
                    $model->total = $total;
                    echo $form->field($model, 'total')->widget(MaskMoney::classname(), [
                                                        'options' => ['class'=>'monto_total_productos','style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                        'disabled' => true,
                                                        'pluginOptions' => [
                                                            'prefix' => $moneda_prov,
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ]
                                                    ]);
                    $precio_dol_total = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($total_productos / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                    echo '<div id="div_total_productos" style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($total_productos,2).' '.$precio_dol_total.'</div>';
                } else {
                  echo $form->field($model, 'total')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['id'=>'total','class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]);
                    } ?>

            </div>
            <div class="col-lg-12">
            <br>
            <div class="form-group">
                <?php

                    if (CompraInventarioSession::getFormapago()) {

                        //echo '<button type="button" onclick="javascript:desactivarsession()" class="btn btn-warning">Advertencia</button>';
                        echo Html::a('Borrar y empezar de nuevo', ['desactivarsession'], [
                            'class' => 'btn btn-warning',
                            //'onclick'=>'javascript:desactivarsession()',
                            'data' => [
                                'confirm' => '¿Seguro que quieres borrar y empezar de nuevo?',
                                'method' => 'post',
                            ],
                        ]).' '.Html::a('<span class="fa fa-arrow-left"></span> Regresar sin borrar datos', ['index'], ['class' => 'btn btn-default']);
                        //echo Html::submitButton($model->isNewRecord ? 'Agregar Productos' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                     } else {
                        echo '<p>Asegurese de llenar bien los datos. Cuando termine el formulario presione "Agregar Productos" para agregar productos a la compra</p>';
                        echo Html::submitButton($model->isNewRecord ? '<span class="fa fa-cart-plus"></span> Agregar Productos' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']).' '.Html::a('<span class="fa fa-arrow-left"></span> Regresar', ['index'], ['class' => 'btn btn-default']);
                     }
                ?>
            </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <?php if(CompraInventarioSession::getFormapago()) :
        if($_GET['carga'] == true){ ?>

            <script type="text/javascript">
            var aircod = '<?php echo CompraInventarioSession::getProductoAir() ?>';
            var airprecio = '<?php echo CompraInventarioSession::getPrecioAir() ?>';
            var airutil = '<?php echo CompraInventarioSession::getUtilidadAir() ?>';
            var airV = '<?php echo CompraInventarioSession::getVentadAir() ?>';
            var airVI = '<?php echo CompraInventarioSession::getVentaImdAir() ?>';
            $(window).load(function(){
                run_producto_air(aircod, airprecio, airutil, airV, airVI);
                /*$('#codProdServicio').val('listo');
                $('#modalAgregaprod').modal('show');*/
            });
        </script>
        <?php } ?>
        <div  id="formid" class="col-lg-12">
            <br>
            <legend>Proceda a agregar los productos de esta compra</legend>
            <p align="right"><font color="red">IMPORTANTE!</font> No agregue producto a la compra si existe una orden de compra pendiente que desea agregar, después de agregar la orden ya puede agregar cualquier otro producto.</p>
                <div class="col-lg-2">
                <?= Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'id' => 'medio', 'class' => 'form-control', 'placeholder' =>'Ingrese código de producto aqui', 'onkeyup'=>'javascript:this.value=Mayuculas(this.value)', 'onchange' => 'javascript:run_producto_input(this.value)'])/*Select2::widget([
                            'name' => 'codProdServicio',
                            'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',
                                function($element) {
                                    return $element['codProdServicio'].' - '.$element['nombreProductoServicio'];
                                }),
                            'language'=>'es',
                            'options' => ['onchange'=>'javascript:run_producto(this.value)','placeholder' => '- Seleccione el producto a agregar -'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]);*/ ?>
                </div>
                <div class="col-lg-2">
                <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar producto a compra', ['#'], [
                                'class' => 'btn btn-success',
                                'id' => 'activity-index-link-agregaprod',
                                'data-toggle' => 'modal',
                                //'onclick'=>'javascript:agrega_modalprod()',
                                'data-target' => '#modalAgregaprod',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Agrega un nuevo producto a la compra')]) ?>

                </div>
                <div class="col-lg-4">
                <?php $model->idOrdenCompra = CompraInventarioSession::getOrdenCompra(); ?>
                <?= $form->field($model, 'idOrdenCompra')->widget(Select2::classname(), [
                      'data' => ArrayHelper::map(OrdenCompraProv::find()
                                    ->where(['idProveedor'=>$model->idProveedor])
                                    ->andWhere('estado = :estado OR idOrdenCompra = :idOrdenCompra', [':estado'=>'Generada', ':idOrdenCompra'=>$model->idOrdenCompra])
                                    ->all(),'idOrdenCompra',
                          function($element) {
                            $compra = ComprasInventario::find()->where(['idOrdenCompra'=>$element['idOrdenCompra']])->one();
                            if (@$compra->idOrdenCompra != $element['idOrdenCompra']){
                              return 'Orden de compra No '.$element['idOrdenCompra'].' - Registrado el '.date('d-m-Y', strtotime($element['fecha_registro']));
                            } else {
                              return 'Orden de compra No '.$element['idOrdenCompra'].' (Apartada para la compra No '.$compra->idCompra.')';
                            }
                          }),
                      'language'=>'es',
                      'disabled' => $model->estado == 'Aplicado' ? true : false,
                      'options' => ['onchange'=>'cargar_compra()',
                                    'id'=>'idOrdenCompra',
                                    'placeholder' => '- Asigne la respectiva orden de compra -',
                                    'options' => ArrayHelper::map(OrdenCompraProv::find()
                                                  ->where(['idProveedor'=>$model->idProveedor])
                                                  ->andWhere('estado = :estado OR idOrdenCompra = :idOrdenCompra', [':estado'=>'Generada', ':idOrdenCompra'=>$model->idOrdenCompra])
                                                  ->all(),'idOrdenCompra',
                                        function($element) {
                                          $compra = ComprasInventario::find()->where(['idOrdenCompra'=>$element['idOrdenCompra']])->one();
                                          if (@$compra->idOrdenCompra != $element['idOrdenCompra']){
                                            return ['disabled' => false];
                                          } else {
                                            return ['disabled' => true];
                                          }
                                        })
                                   ],
                      'pluginOptions' => [
                          'allowClear' => true
                      ],
                  ])->label('Orden de compra') ?>

                </div>
                <div class="col-lg-4"><br>
                  <?php if($products) : ?>
                  <?= Html::a('<i class="fa fa-cart-arrow-down"></i> Guardar compra', '#', [
                              'onclick' => 'guardarcompra()',
                              'class' => 'btn btn-primary',
                          ]) ?>
                  <?php endif ?>
                <span style="float:right">
                  <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar nuevo producto a inventario', ['#'], [
                                'class' => 'btn btn-info',
                                'id' => 'activity-index-link-agregaprod-inv',
                                'data-toggle' => 'modal',
                                'data-target' => '#modalAgregaprodinv',
                                'onclick' => '$.fn.modal.Constructor.prototype.enforceFocus = function() {};',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Agrega un nuevo producto a inventario')]) ?>
                </span>
                </div>
        </div>
        <div id="porcentaje_masivo" class="col-lg-12 panel panel-warning" style="display: none;">
          <div class="panel-body">
            <div class="col-lg-6">
              Seleccione el check correspondiente al que desea aplicar el porcentaje masivo y luego seleccione <strong>"Aplicar % masivo"</strong>, podrá hacer esto las veces que sean necesarias de los productos marcados.
            </div>
            <div class="col-lg-2" title="% Descuento masivo">
              <div class="input-group">
                <span class="input-group-addon">
                  <input type="checkbox" id="descuento_masivo_check" name="descuento_masivo_check">
                </span>
                <input type="text" id="descuento_masivo" onkeyup='habilitarbtn_aplica_masivo()' onkeypress="return isNumberDe(event)" class="form-control" placeholder="% Descuento masivo" disabled>
              </div><!-- /input-group -->
            </div>
            <div class="col-lg-2" title="% Utilidad masiva">
              <div class="input-group">
                <span class="input-group-addon">
                  <input type="checkbox" id="utilidad_masiva_check" name="utilidad_masiva_check">
                </span>
                <input type="text" id="utilidad_masiva" onkeyup='habilitarbtn_aplica_masivo()' onkeypress="return isNumberDe(event)" class="form-control" placeholder="% Utilidad masiva" disabled>
              </div><!-- /input-group -->
            </div>
            <div class="col-lg-2">
              <a class="btn btn-warning btn-block" id="btn_aplicar_porc_masivo" onclick="obtener_productos_marcados(this)" data-loading-text="Espere, cambios en proceso..." ><span class="fa fa-cubes" aria-hidden="true"></span> Aplicar % masivo</a>
            </div>
          </div>
        </div>
        <div class="col-lg-12" id="conten_prom">
            <br>
            <?php
            //http://www.programacion.com.py/tag/yii
                //echo '<div class="grid-view">';
                echo '<table class="items table table-striped" id="tabla_producto_compra">';
                echo '<thead>';
                printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th style="text-align:right;">%s</th><th style="text-align:right;">%s</th>
                <th style="text-align:right;">%s</th><th style="text-align:center;">%s</th><th>%s</th><th class="actions button-column">&nbsp;</th>
                <th class="actions button-column" style="text-align:right">
                <input type="checkbox" id="checkAll" name="checkAll" title="Marca todos los productos para editarle sus porcentajes de descuento y/o porcentaje de utilidad" />
                </th>
                </tr>', '#',
                        'CÓDIGO',
                        'COD.PROV',
                        'PRODUCTO',
                        'CANT',
                        'PREC. COMPRA',
                        'PREC. VENTA',
                        'PREC. VENT + IMP',
                        'EXENT.IV',
                        'APLICADO'
                        );
                echo '</thead>';
            //if($products) {
            ?>
            <?php
            if($products) {
            echo '<tbody>';
                foreach($products as $position => $product) {
                $border = $product['modificado'] == 'Si' ? 'blue 10px solid;' : 'white 10px solid;';
                $eliminar = Html::a('', ['/compras-inventario/deleteitem', 'id' => $position], ['class'=>'glyphicon glyphicon-remove', 'title' => Yii::t('app', 'Quitar este producto'), 'data' => [
                                    'confirm' => '¿Está seguro de quitar este producto de la orden?'] ]);
                $actualizar = Html::a('', ['', 'id' => $position], [
                                'class'=>'glyphicon glyphicon-pencil',
                                'id' => 'activity-index-link-actualizaprod',
                                'data-toggle' => 'modal',
                                'onclick'=>'javascript:actualiza_modalprod("'.$position.'")',
                                'data-target' => '#modalActualizaprod',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Actualizar producto') ]);
                $accion = $actualizar.' '.$eliminar;
                $checkbox = '<input id="striped" type="checkbox" onChange="comprobar_check_marcado()" name="checkboxRow[]" value="'.$position.'">';
                $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                printf('<tr style = "border-left:  '.$border.' ">
                <td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td align="right">%s</td><td align="right">%s</td><td align="right">%s</td>
                <td align="center">%s</td><td>%s</td><td class="actions button-column" align="right">%s</td>
                <td class="actions button-column" align="right" title="Seleccione los productos que desea cambiar su porcentaje de descuento o utilidad">%s</td></tr>',
                            //$product['noEntrada'],
                            $position+1,
                            $product['codProdServicio'],
                            $product['codigo_proveedor'],
                            $modelpro->nombreProductoServicio,
                            $product['cantidadEntrada'],
                            number_format($product['precioCompra'],2),
                            //$product['descuento'],
                            //$product['porcentUtilidad'],
                            number_format($product['preciosinImp'],2),
                            number_format($product['precioconImp'],2),
                            $product['exImpuesto'],
                            //$product['usuario'],
                            $product['estado'],
                            $accion, $checkbox
                            );
                        //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/


                }
            echo '</tbody>';
        }
            ?>
            <?php
            //}
                echo '</table>';
            ?>
            </div>
        <?php endif ?>
    </div>
    </div>
</div>

<?php //--------------------------------------AGREGA PRODUCTO DE UNA COMPRA
$form = ActiveForm::begin([ 'options' => ['name'=>'caja'], 'action'=>['/compras-inventario/agrega_modalprod'] ]);
         Modal::begin([
                'id' => 'modalAgregaprod',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Detalle de Entrada</h4></center>',
                'footer' => Html::a('<i class="glyphicon glyphicon-search"></i> Buscar producto', ['#'], [
                                'class' => 'btn btn-info',
                                'id' => 'activity-index-link-inventario',
                                'data-toggle' => 'modal',
                                //'onclick'=>'javascript:agrega_modalprod()',
                                'data-target' => '#modalinventario',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Busqueda en Inventario [F2]')]).
                                '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>
                                <input type="submit" value="Agregar" class="btn btn-success" id="agregar_pro_comp">
                    ',
                ]);
            //echo "<div id='modal_agrega'></div>";

        echo '<div class="panel panel-default">';
        echo '<div class="panel-body">';
        echo '<div class="well col-lg-12">';
        echo '<div id="datos_de_inventario"></div>';
        echo '</div>';
        $modeldetallecompra = new DetalleCompra();

        //echo $form->field($modeldetallecompra, 'noEntrada')->textInput();
        //echo $form->field($modeldetallecompra, 'codProdServicio')->textInput().'<br>';
        echo '<div class="col-lg-4">';
        /*echo $form->field($modeldetallecompra, 'codProdServicio')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',
                                function($element) {
                                    return $element['codProdServicio'].' - '.$element['nombreProductoServicio'];
                                }),
                            'language'=>'es',
                            'options' => ['onchange'=>'javascript:run_producto(this.value)','placeholder' => '- Seleccione -'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]);*/
        echo $form->field($modeldetallecompra, 'codProdServicio')->textInput(['class'=>'codProdServicio form-control', 'readonly'=>true]);
        if ($moneda_local != $moneda_prov) {
          echo '<div class="col-xs-5">
                  <label>Calculadora</label>
                  <input class="form-control" id="precio_dolar" placeholder="Dolares" type="text" onkeyup="javascript:convertir_dolar()">
                </div>
                <div class="col-xs-7">'.$form->field($modeldetallecompra, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                            //'name' => 'input-33',
                            'options' => ['id'=>'precioCompra', 'class'=>'precioc form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                            'clientOptions' => [
                                'alias' =>  'decimal',
                                'groupSeparator' => ',',
                               // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                                'autoGroup' => true
                            ],
                        ]).'</div>';
        } else {
          echo $form->field($modeldetallecompra, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                            //'name' => 'input-33',
                            'options' => ['id'=>'precioCompra', 'class'=>'precioc form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                            'clientOptions' => [
                                'alias' =>  'decimal',
                                'groupSeparator' => ',',
                               // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                                'autoGroup' => true
                            ],
                        ]);
        }

        echo '<div id="busca_focus">'.$form->field($modeldetallecompra, 'cantidadEntrada')->textInput(['class'=>'cant_entr form-control', "onkeypress"=>"return isNumberDe(event)"]).'</div>';
        echo $form->field($modeldetallecompra, 'descuento')->widget(\yii\widgets\MaskedInput::className(), [
                          //'name' => 'input-33',
                          'options' => ['class'=>'descuento form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                          'clientOptions' => [
                              'alias' =>  'decimal',
                              'groupSeparator' => ',',
                              'autoGroup' => true
                          ],
                      ]);
        echo '</div>';
        echo '<div class="col-lg-4">';
        $modeldetallecompra->exImpuesto = 'No';
        echo $form->field($modeldetallecompra, 'exImpuesto')->radioList(['Si'=>'Si','No'=>'No']);
        echo $form->field($modeldetallecompra, 'porcentUtilidad')->widget(\yii\widgets\MaskedInput::className(), [
                          //'name' => 'input-33',
                          'options' => ['class'=>'porcentu form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                          'clientOptions' => [
                              'alias' =>  'decimal',
                              'groupSeparator' => ',',
                              'autoGroup' => true
                          ],
                      ]);
        echo $form->field($modeldetallecompra, 'preciosinImp')->widget(\yii\widgets\MaskedInput::className(), [
                          //'name' => 'input-33',
                          'options' => ['class'=>'preciov form-control', 'onkeyup'=>'javascript:obtenerUtilidadImpu()', "onkeypress"=>"return isNumberDe(event)"],
                          'clientOptions' => [
                              'alias' =>  'decimal',
                              'groupSeparator' => ',',
                              'autoGroup' => true
                          ],
                      ]);
        echo $form->field($modeldetallecompra, 'precioconImp')->widget(\yii\widgets\MaskedInput::className(), [
                          //'name' => 'input-33',
                          'options' => ['class'=>'precioventaimpu form-control', 'onkeyup'=>'javascript:obtenerVentaUti()', "onkeypress"=>"return isNumberDe(event)"],
                          'clientOptions' => [
                              'alias' =>  'decimal',
                              'groupSeparator' => ',',
                              'autoGroup' => true
                          ],
                      ]);
        echo '</div>';
        echo '<div class="col-lg-4">';
        echo $form->field($modeldetallecompra, 'codigo_proveedor')->textInput(['class'=>'codigo_proveedor form-control']);
        echo $form->field($modeldetallecompra, 'usuario')->textInput(['value'=>Yii::$app->user->identity->username, 'readonly'=>true]);
        echo $form->field($modeldetallecompra, 'estado')->textInput(['value'=>'No Aplicado', 'readonly'=>true]);

        echo '';

        //echo Html::a('<i class="glyphicon glyphicon-shopping-cart"></i> Guardar compra', '', ['class' => 'btn btn-primary']);
        echo '</div>';
        echo '</div>';
        echo '</div>';
        Modal::end();
        ActiveForm::end();
    ?>

    <?php //----------------------------------ACTUALIZA UN PRODUCTO EN LA COMPRA
        Modal::begin([
                'id' => 'modalActualizaprod',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Detalle de Entrada</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '', [
                    'class'=>'btn btn-info',
                    //'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Agregar este producto a la orden de compra'
                ])*/,
            ]);
        echo '<div id="modal_update_product"></div>';

        Modal::end();
    ?>

    <?php //------------------AGREGA PRODUCTO A INVENTARIO
        Modal::begin([
                'id' => 'modalAgregaprodinv',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Agregar nuevo producto a inventario</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
            ]);
        echo '<div class="panel panel-default">';
        echo '<div class="panel-body">';
        $modelproducto = new ProductoServicios();
        $form = ActiveForm::begin([ 'options' => ['name'=>'cajapro'], 'action'=>['/compras-inventario/agrega_prod_inv'] ]);


    echo '<div class="producto-servicios-form">
            <div class="col-lg-4">';
                echo $form->field($modelproducto, 'tipo')->textInput(['readonly' => true, 'value' => 'Producto']);
                echo $form->field($modelproducto, 'codProdServicio')->textInput(['onkeyup'=>'javascript:this.value=Mayuculas(this.value)']);
                echo $form->field($modelproducto, 'nombreProductoServicio')->textarea(['maxlength' => 500]);
                echo $form->field($modelproducto, 'idUnidadMedida')->widget(Select2::classname(), [
                                   'data' => ArrayHelper::map($modelproducto->getUnidadMedida(),'idUnidadMedida', function($element){
                                           //return $element->numero_cuenta;
                                       return $element['descripcionUnidadMedida'].' / '.$element['simbolo'];
                                       }),
                                   'language'=>'es',
                                   'options' => ['placeholder' => '- Seleccione -'],
                                   'pluginOptions' => [
                                       'allowClear' => true,
                                   ],
                               ]);
      echo '</div>
            <div class="col-lg-4">
                <div class="col-lg-6">';
                    echo $form->field($modelproducto, 'cantidadMinima')->textInput();
          echo '</div>
                <div class="col-lg-6">';
                    echo $form->field($modelproducto, 'ubicacion')->textInput();
          echo '</div>';
          echo '<div class="col-lg-12">';
            echo $form->field($modelproducto, 'codFamilia')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($modelproducto->getNombreFamilia(),'codFamilia',function($element){
                                //return $element->numero_cuenta;
                            return $element['codFamilia'].' - '.$element['descripcion'];
                            }),
                        'language'=>'es',
                        'options' => ['placeholder' => '- Seleccione -'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Categoría');

          echo '</div>
                <div class="col-lg-12">';
                    echo $form->field($modelproducto, 'precioCompra')->textInput(['id'=>'preciocIn', 'onkeyup'=>'javascript:obtenerPrecioVentaIn()']);
         echo ' </div>
                <div class="col-lg-12">';
                    echo $form->field($modelproducto, 'porcentUnidad')->textInput(['id'=>'porcentuIn', 'onkeyup'=>'javascript:obtenerPrecioVentauIn()']);
          echo '</div>
            </div>
            <div class="col-lg-4">';
            $modelproducto->exlmpuesto = 'No';
            $modelproducto->anularDescuento = 'No';
                echo $form->field($modelproducto, 'precioVenta')->textInput(['id'=>'preciovIn', 'onkeyup'=>'obtenerUtilidadImpuIn()']);
                echo $form->field($modelproducto, 'exlmpuesto')->radioList(['Si'=>'Si','No'=>'No']);
                echo $form->field($modelproducto, 'precioVentaImpuesto')->textInput(['id'=>'precioventaimpuIn', 'onkeyup'=>'obtenerVentaUtiIn()']);
                echo $form->field($modelproducto, 'anularDescuento')->radioList(['Si'=>'Si','No'=>'No']);
                echo '<div class="form-group" style="text-align:right">';
                echo '<input type="submit" value="Crear" class="btn btn-primary">';
      echo '</div>';
    echo '</div>
        </div>';

        ActiveForm::end();
        echo '</div>';
        echo '</div>';
        Modal::end();
    ?>
