<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\Proveedores;
use backend\models\TipoCambio;
use backend\models\CompraInventarioSession;
use backend\models\ComprasInventario;
use backend\models\ProductoServicios;
use backend\models\Notificar_compra;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;
use kartik\widgets\DatePicker;
use kartik\money\MaskMoney;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use backend\models\DetalleCompra;
use backend\models\Moneda;
use backend\models\OrdenCompraProv;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/*use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;*/
/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
  #modal_resolucion_xml .modal-dialog{
  width: 18%!important;
  /*margin: 0 auto;*/
  }

  .popover{
  max-width:700px;
}
</style>
<?php //$products = CompraInventarioSession::getContenidoProducto();
$moned_lo = Moneda::find()->where(['monedalocal'=>'x'])->one();
$moneda_local = $moned_lo->simbolo;
    $products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->orderBy(['idDetalleCompra' => SORT_ASC])->all();
    $proveedor = Proveedores::find()->where(['=', 'codProveedores', $model->idProveedor])->one();
    $moneda = Moneda::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->one();
    $moneda_prov = '°';
    if (@$moneda) {
     $moneda_prov = $moneda->simbolo;
    }
    $impuesto_productos = 0;
    $impuesto = Yii::$app->params['porciento_impuesto'];
    $porc_descuento = $precioporcantidad = 0;
    $monto_impuesto_fletes = $model->iv_fletes=='si' ? $model->fletes * $impuesto / 100 : 0;
    foreach ($products as $position => $product)
    { //al recoger los valores sumo el ciclo para obtener el subtotal y descuento
      $precioporcantidad_ = $product['precioCompra'] * $product['cantidadEntrada'];
      $porc_descuento_ = ($product['precioCompra'] * $product['descuento'] / 100) * $product['cantidadEntrada'];
      $precioporcantidad += $product['precioCompra'] * $product['cantidadEntrada'];
      $porc_descuento += ($product['precioCompra'] * $product['descuento'] / 100) * $product['cantidadEntrada'];
      $impuesto_productos += $product['exImpuesto'] == 'Si' ? 0 : (($precioporcantidad_ - $porc_descuento_) * $impuesto / 100 ) + $monto_impuesto_fletes;
    }


    //$impuesto_productos = (($precioporcantidad - $porc_descuento) * $impuesto / 100 ) + $monto_impuesto_fletes;
    $total_productos = $precioporcantidad - $porc_descuento + $impuesto_productos + $model->fletes;
    /*CompraInventarioSession::setSubtotalup($subTotal_productos);
    CompraInventarioSession::setDescuentoup($descuento_productos);
    CompraInventarioSession::setImpuestoup($impuesto_productos);
    CompraInventarioSession::setTotalup($total_productos);*/
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
      $('[data-toggle="popover"]').popover();
  });

function enviar_xml_not() {
var resolucion = document.getElementById('resolucion').value;
var detalle = document.getElementById('detalle').value;
var xml = document.getElementById('xml').value;
$.ajax({
   url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/enviar_xml_not') ?>",
   type:"post",
   //dataType: 'json',
   data: { 'resolucion': resolucion, 'detalle': detalle, 'xml': xml },
   success: function(data){
     /*$('.monto_impuesto_productos').val(data.impuestos);
     $('.monto_total_productos').val(data.total);*/
     $( "#notificacion_movimiento" ).html( data );
   }
});
}

function imprimir() {
  if (window.print) {
      window.print();
  } else {
      alert("La función de impresion no esta soportada por su navegador.");
  }
}

$(document).ready(function () {
$('#btn_aplicar_porc_masivo').addClass('disabled');
});

$(document).ready(function () {
//me checa todas las facturas pendientes
div = document.getElementById('porcentaje_masivo');
$('#checkAll').on('change', function() {
    if (this.checked == true) {
        div.style.display = '';
        $('#tabla_producto_compra').find('input[name="checkboxRow[]"]').prop('checked', true);
        $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
        $("#descuento_masivo").prop('disabled', true);//input desabilitados
        document.getElementById('descuento_masivo').value = '';//input vacidos
        $("#utilidad_masiva").prop('disabled', true);//input desabilitados
        document.getElementById('utilidad_masiva').value = '';//input vacidos
        $('input[name=descuento_masivo_check]').attr('checked', false);//check desmarcado
        $('input[name=utilidad_masiva_check]').attr('checked', false);//check desmarcado
      }
    else {
        div.style.display = 'none';
        $('#tabla_producto_compra').find('input[name="checkboxRow[]"]').prop('checked', false);
      }
});
//accion para abilitar o desabilitar el input de descuento masivo
$('#descuento_masivo_check').on('click', function() {
  var utilidad_masiva = document.getElementById('utilidad_masiva').value;
  if (this.checked == true){
    $("#descuento_masivo").prop('disabled', false);
  } else {
    $("#descuento_masivo").prop('disabled', true);
    document.getElementById('descuento_masivo').value = '';
    if (utilidad_masiva=="") {//compruebo que el campo de utilidad masiva esté vacio para decir que si puedo desabilitar el boton
      $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
    }
  }
});
//accion para abilitar o desabilitar el input de utilidad masivo
$('#utilidad_masiva_check').on('click', function() {
  var descuento_masivo = document.getElementById('descuento_masivo').value;
  if (this.checked == true){
    $("#utilidad_masiva").prop('disabled', false);
  } else {
    $("#utilidad_masiva").prop('disabled', true);
    document.getElementById('utilidad_masiva').value = '';
    if (descuento_masivo=="") {//compruebo que el campo de descuento masivo esté vacio para decir que si puedo desabilitar el boton
      $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
    }
  }
});
});

//me permite ocultar el area div_costo_fletes
function div_costo_fletes() {
  div_fletes = document.getElementById('div_costo_fletes');
  if (div_fletes.style.display != 'none') { div_fletes.style.display = 'none'; } else { div_fletes.style.display = ''; }
}

function obtenermontoflete() {
var fletes = document.getElementById('fletes').value;
var subTotal = '<?= $precioporcantidad ?>';
var descuento_productos = '<?= $porc_descuento ?>';
var checkboxValues = "";
var idCompra = '<?= $model->idCompra ?>';
$('input[name="check_iv_fletes"]:checked').each(function() {//
    checkboxValues += $(this).val() + ",";//
});//
if (checkboxValues=='') {
  impuesto_flete = '';
} else {
  impuesto_flete = 'si';
}
$.ajax({
   url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenermontoflete_u') ?>",
   type:"post",
   dataType: 'json',
   data: { 'fletes': fletes, 'impuesto_flete': impuesto_flete, 'idCompra': idCompra,
   'subTotal' : subTotal, 'descuento_productos' : descuento_productos },
   success: function(data){
     /*$('.monto_impuesto_productos').val(data.impuestos);
     $('.monto_total_productos').val(data.total);*/
     $( "#div_impuesto_productos" ).html( data.impuestos );
     $( "#div_total_productos" ).html( data.total );
   }
});
}

//comprueba si todos los check estan marcados o desmarcados
function comprobar_check_marcado() {
var checkboxValues = "";
$('input[name="checkboxRow[]"]:checked').each(function() {//
    checkboxValues += $(this).val() + ",";//
});//
if (checkboxValues=='') {
  div.style.display = 'none';
  $('input[name=checkAll]').attr('checked', false);
  $('#btn_aplicar_porc_masivo').addClass('disabled');//desabilito el boton de aplicar % masivo
  $("#descuento_masivo").prop('disabled', true);//input desabilitados
  document.getElementById('descuento_masivo').value = '';//input vacidos
  $("#utilidad_masiva").prop('disabled', true);//input desabilitados
  document.getElementById('utilidad_masiva').value = '';//input vacidos
  $('input[name=descuento_masivo_check]').attr('checked', false);//check desmarcado
  $('input[name=utilidad_masiva_check]').attr('checked', false);//check desmarcado
} else {
  div.style.display = '';
}
}
//desde los input de los poprcentajes abilito el boton de aplicar % mesivo cuando almenos uno esté con un valor
function habilitarbtn_aplica_masivo() {
var descuento_masivo = document.getElementById('descuento_masivo').value;
var utilidad_masiva = document.getElementById('utilidad_masiva').value;
if (descuento_masivo == '' && utilidad_masiva == '') {
  $('#btn_aplicar_porc_masivo').addClass('disabled');
} else {
  $('#btn_aplicar_porc_masivo').removeClass('disabled');
}
}
//obtengo los productos marcados para actualizar los porcentajes
function obtener_productos_marcados(btn_prod_marc){
  var checkboxValues = "";
  $('input[name="checkboxRow[]"]:checked').each(function() {//
      checkboxValues += $(this).val() + ",";//
  });//
  //eliminamos la última coma.
  checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//
  var descuento_masivo = document.getElementById('descuento_masivo').value;
  var utilidad_masiva = document.getElementById('utilidad_masiva').value;
  idCompra = '<?= $model->idCompra ?>';
  $.ajax({
          url:"<?= Yii::$app->getUrlManager()->createUrl('compras-inventario/obtener_productos_marcados_update') ?>",
          type:"post",
          data: { 'checkboxValues' : checkboxValues, 'descuento_masivo' : descuento_masivo,
                  'utilidad_masiva' : utilidad_masiva, 'idCompra': idCompra },
          beforeSend: function() { //mientras se ejecuran los cambios pongo el boton en espera
            var $btn = $(btn_prod_marc).button('loading');
                  // simulating a timeout
            setTimeout(function () { $btn.button('reset'); }, 60000);
          },
          success: function(notif){
          },
          error: function(msg, status,err){
           //alert('No pasa 36');
          }
      });
}
//muestra el boton enviar en modo espera mientras se envia los datos a hacienda
function esperar_confirmacion(this_) {
tipo_documento = $('.tipo_documento_').val();
resolucion = $('.resolucion_').val();
detalle = $('.detalle_').val();
detalle = $('.detalle_').val();
archivo = $('.archivo_').val();
if (tipo_documento!='' && resolucion!='' && detalle!='' && archivo!='') {

  $(this_).button('loading');
}

}
</script>
<?php
  //echo Yii::$app->getUrlManager()->createUrl('compras-inventario/update');
  //echo '<br>'.Url::to();
  //echo '<br>'.Utils::getRoot().'/ProyectosNelux/aplicaciones_web/Lutux/backend/view/compras-inventario/_form_u.php';
  //Especifique la URL ABSOLUTA al archivo php que creará el objeto ClientPrintJob
  //En este caso, esta misma página

?>
<?php $form = ActiveForm::begin([]); ?>
<div class="compras-inventario-form">
  <div class="well col-lg-12">
  <div id="conten_pro">
      <div class="col-lg-12">
          <div class="col-lg-3">
              <?php if (@$idProveedor = CompraInventarioSession::getProveedorup()) {
                  $model->idProveedor = $idProveedor;
                  echo $form->field($model, 'idProveedor')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(Proveedores::find()->all(),'codProveedores',
                      function($element) {
                          return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                      }),
                  'language'=>'es',
                  'disabled' => true,
                  'options' => ['id'=>'idProveedor', 'placeholder' => '- Seleccione -'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ])->label('Proveedor');
              } else {
                  echo $form->field($model, 'idProveedor')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(Proveedores::find()->all(),'codProveedores',
                      function($element) {
                          return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                      }),
                  'language'=>'es',
                  'options' => ['id'=>'idProveedor' ,'placeholder' => '- Seleccione -'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ])->label('Proveedor');
                  } ?>
          </div>
          <div class="col-lg-3">
              <?php if (@$numFactura = CompraInventarioSession::getNofacturaup()) {
                      $model->numFactura = $numFactura;
                      echo $form->field($model, 'numFactura')->textInput(['onkeypress'=>''])->textInput(['readonly' => true]);
              } else {
                      echo $form->field($model, 'numFactura')->textInput(['onkeypress'=>'']);
                  } ?>
              <!--?php
                  echo '<strong>Número de Factura</strong>';
                  echo Html::input('text', 'new-numFactura', '', ['id' => '', 'class' => 'form-control', 'onkeypress'=>'return isNumber(event)']);
              ?-->
          </div>
          <div class="col-lg-3">
            <?php if (@$n_interno_prov = CompraInventarioSession::getInterno_provup()) {
                    $model->n_interno_prov = $n_interno_prov;
                    echo $form->field($model, 'n_interno_prov')->textInput(['onkeypress'=>''])->textInput(['readonly' => true]);
            } else {
                    echo $form->field($model, 'n_interno_prov')->textInput(['onkeypress'=>'']);
                } ?>
          </div>
          <div class="col-lg-3">
            <?php if (@$products) {
              $is_checked = '';
              $style = 'display: none;';
              if ($model->fletes!=null) { $style = ''; }
              if ($model->iv_fletes=='si') { $is_checked = 'checked'; }
              $is_disabled = false; $is_disabled_check = '';
              if ($model->estado == 'Aplicado') { $is_disabled = true; $is_disabled_check = 'disabled';}
              echo $form->field($model, 'fletes', [
                 'inputTemplate' => '<div class="form-group input-group" id="div_costo_fletes" style="'.$style.'">
                 <span class="input-group-addon">
                 <i class="fa fa-plus-circle"></i> <span class="fa fa-truck"></span>
                 </span>
                  {input}
                 <span class="input-group-addon" title="El check marcado significa que se aplica impuesto al flete">
                   <input type="checkbox" name="check_iv_fletes" onclick="obtenermontoflete()" '.$is_checked.' '.$is_disabled_check.'>
                 </span></div>',
                 ])->widget(\yii\widgets\MaskedInput::className(), [
                     //'name' => 'input-33',
                     'options' => ['id'=>'fletes','onkeyup'=>'javascript:obtenermontoflete()',
                     'disabled' => $is_disabled,
                     'class'=>'form-control', "onkeypress"=>"return isNumberDe(event)",'placeholder' =>'Monto por fletes',
                      'style'=>"font-family: Fantasy; font-size: 16pt; text-align: center;"],
                     'clientOptions' => [
                         'alias' =>  'decimal',
                         'groupSeparator' => ',',
                         'autoGroup' => true
                     ],
                 ])->label('<a href="javascript:div_costo_fletes();">Incluir costos por fletes</a> (incluir al final antes de aplicar compra)');
            } ?>
          </div>
      </div>
      <div class="col-lg-12">
          <div class="col-lg-3">
              <div id="pago">
                  <?php if (@$formaPago = CompraInventarioSession::getFormapagoup()) {
                      $model->formaPago = $formaPago;
                      echo $form->field($model, 'formaPago')->radioList(['Contado' => 'Contado', 'Crédito' => 'Crédito'])->textInput(['readonly' => true]);
                  }
                  else {
                      //$model->formaPago = 'Crédito';
                      $model->formaPago = $model->isNewRecord ? 'Crédito' : $model->formaPago;
                      echo $form->field($model, 'formaPago')->radioList(['Contado' => 'Contado', 'Crédito' => 'Crédito']);
                      } ?>
              </div>
          </div>
          <div class="col-lg-3">
          <?php if (@$fechaDocumento = CompraInventarioSession::getFechadocumentoup() ) {
              $model->fechaDocumento = $fechaDocumento;
              echo $form->field($model,'fechaDocumento')-> widget(DatePicker::className(),[
                                                         'name' => 'check_issue_date',
                                                          'disabled' => true,
                                                          'options' => ['id' => 'fecDoc', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                          'pluginOptions' => [
                                                              'format' => 'dd-m-yyyy',
                                                              'todayHighlight' => true,
                                                              'autoclose' => true,
                                                          ]
                                                      ]);
          } else {
              echo $form->field($model,'fechaDocumento')-> widget(DatePicker::className(),[
                                                         'name' => 'check_issue_date',
                                                          'disabled' => false,
                                                          'options' => ['id' => 'fecDoc', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                          'pluginOptions' => [
                                                              'format' => 'dd-m-yyyy',
                                                              'todayHighlight' => true,
                                                              'autoclose' => true,
                                                          ]
                                                      ]);
              } ?>
          </div>
          <div class="col-lg-3">
          <?php if (@$fechaRegistro = CompraInventarioSession::getFecharegistroup() ) {
              $model->fechaRegistro = $fechaRegistro;
              echo $form->field($model,'fechaRegistro')-> widget(DatePicker::className(),[
                                                         'name' => 'check_issue_date',
                                                          'disabled' => true,
                                                          //'options' => ['id' => 'fecRe', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                          'pluginOptions' => [
                                                              'format' => 'dd-m-yyyy',
                                                              'todayHighlight' => true,
                                                              'autoclose' => true,
                                                          ]
                                                      ]);
          } else {
              $model->fechaRegistro = $model->isNewRecord ? date('d-m-Y ') : $model->fechaRegistro;
              echo $form->field($model,'fechaRegistro')-> widget(DatePicker::className(),[
                                                         'name' => 'check_issue_date',
                                                          'disabled' => false,
                                                          //'options' => ['id' => 'fecRe', 'onchange'=>'javascript:agrega_fecha(this.value)'],
                                                          'pluginOptions' => [
                                                              'format' => 'dd-m-yyyy',
                                                              'todayHighlight' => true,
                                                              'autoclose' => true,
                                                          ]
                                                      ]);
              } ?>
          </div>
          <div class="col-lg-3">
          <?php if (CompraInventarioSession::getFormapagoup() ) {
                  if(@$fechaVencimiento = CompraInventarioSession::getFechavencimientoup())
                  {
                      $model->fechaVencimiento = $fechaVencimiento;
                      echo $form->field($model,'fechaVencimiento')-> widget(DatePicker::className(),[
                                                             'name' => 'check_issue_date',
                                                              'disabled' => true,
                                                              'options' => ['id' => 'fecVe'],
                                                              'pluginOptions' => [
                                                                  'format' => 'dd-m-yyyy',
                                                                  'todayHighlight' => true,
                                                                  'autoclose' => true,
                                                              ]
                                                          ])->label('Fecha Vencimiento (Crédito)');
                          } else { echo ""; }

          } else{
              echo '<div id="contenido_a_mostrar">';
              //$model->fechaVencimiento = '00-00-0000';
              echo $form->field($model,'fechaVencimiento')-> widget(DatePicker::className(),[
                                                         'name' => 'check_issue_date',
                                                          'disabled' => false,
                                                          'options' => ['id' => 'fecVe'],
                                                          'pluginOptions' => [
                                                              'format' => 'dd-m-yyyy',
                                                              'todayHighlight' => true,
                                                              'autoclose' => true,
                                                          ]
                                                      ])->label('Fecha Vencimiento (Crédito)');
              echo '</div>';
              } ?>
          </div>
      </div>

      <div class="col-lg-12"><br>
          <div class="col-lg-3">
              <?php
              $tipo_cambio = TipoCambio::find()->where(['usar'=>'x'])->one();
              if (@$numFactura = CompraInventarioSession::getNofacturaup()) {

                  echo $form->field($model, 'subTotal')->widget(MaskMoney::classname(), [
                    'options' => ['style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                      'disabled' => true,
                                                      'pluginOptions' => [
                                                          'prefix' => $moneda_prov,
                                                          'thousandSeparator' => ',',
                                                          'decimalSeparator' => '.',
                                                           'precision' => 2,
                                                           'allowZero' => false,
                                                          'allowNegative' => false,
                                                      ]
                                                  ]);
                  $precio_dol_subtot = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($precioporcantidad / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                  echo '<div style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($precioporcantidad,2).' '.$precio_dol_subtot.'</div>';
              } else {
                  echo $form->field($model, 'subTotal')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]);
                  } ?>
          </div>
          <div class="col-lg-3">
              <?php if (@$numFactura = CompraInventarioSession::getNofacturaup()) {
                  echo $form->field($model, 'descuento')->widget(MaskMoney::classname(), [
                    'options' => ['class'=>'descuento_compra', 'style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                      'disabled' => true,
                                                      'pluginOptions' => [
                                                          'prefix' => $moneda_prov,
                                                          'thousandSeparator' => ',',
                                                          'decimalSeparator' => '.',
                                                           'precision' => 2,
                                                           'allowZero' => false,
                                                          'allowNegative' => false,
                                                      ]
                                                  ]);
                  $precio_dol_desc = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($porc_descuento / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                  echo '<div style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($porc_descuento,2).' '.$precio_dol_desc.'</div>';
              } else {
                  echo $form->field($model, 'descuento')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]);
                  } ?>

          </div>
          <div class="col-lg-3">
              <?php if (@$numFactura = CompraInventarioSession::getNofacturaup()) {
                  echo $form->field($model, 'impuesto')->widget(MaskMoney::classname(), [
                    'options' => ['class'=>'monto_impuesto_productos','style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                      'disabled' => true,
                                                      'pluginOptions' => [
                                                          'prefix' => $moneda_prov,
                                                          'thousandSeparator' => ',',
                                                          'decimalSeparator' => '.',
                                                           'precision' => 2,
                                                           'allowZero' => false,
                                                          'allowNegative' => false,
                                                      ]
                                                  ]);
                  $precio_dol_impu = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($impuesto_productos / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                  echo '<div id="div_impuesto_productos" style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($impuesto_productos,2).' '.$precio_dol_impu.'</div>';
              } else {
                  echo $form->field($model, 'impuesto')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]);
                  } ?>

          </div>
          <div class="col-lg-3">
              <?php if (@$numFactura = CompraInventarioSession::getNofacturaup()) {
                  echo $form->field($model, 'total')->widget(MaskMoney::classname(), [
                    'options' => ['class'=>'monto_total_productos','style'=>"font-family: Fantasy; font-size: 20pt; text-align: center;"],
                                                      'disabled' => true,
                                                      'pluginOptions' => [
                                                          'prefix' => $moneda_prov,
                                                          'thousandSeparator' => ',',
                                                          'decimalSeparator' => '.',
                                                           'precision' => 2,
                                                           'allowZero' => false,
                                                          'allowNegative' => false,
                                                      ]
                                                  ]);
                  $precio_dol_total = $moneda_prov == $moneda_local ? '' : '<strong>('.$moneda_prov . number_format($total_productos / $tipo_cambio->valor_tipo_cambio,2).')</strong>';
                  echo '<div id="div_total_productos" style="background-color:#FFC; padding:12px; font-size:20px; text-align: center;">'.$moneda_local.number_format($total_productos,2).' '.$precio_dol_total.'</div>';
              } else {
                  echo $form->field($model, 'total')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]);
                  } ?>

          </div>
          <div class="col-lg-12">
          <br>
          <div class="form-group">
              <?php

                  if (CompraInventarioSession::getFormapagoup()) {

                      //echo '<button type="button" onclick="javascript:desactivarsession()" class="btn btn-warning">Advertencia</button>';
                      echo Html::a('<span class="glyphicon glyphicon-refresh"></span> Actualizar encabezado de Compra', ['desactivarsessionu', 'id' => $model->idCompra ], [
                          'class' => 'btn btn-warning',
                          //'onclick'=>'javascript:desactivarsession()',
                         /* 'data' => [
                              'confirm' => '¿Seguro que quieres borrar y empezar de nuevo?',
                              'method' => 'post',
                          ],*/
                      ]).' '.Html::a('<span class="fa fa-arrow-left"></span> Regresar', ['index'], ['class' => 'btn btn-default']);
                      //echo Html::submitButton($model->isNewRecord ? 'Agregar Productos' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                      if ($model->estado == 'Aplicado')
                      { echo ' '.Html::a('<span class="glyphicon glyphicon-print"></span> Imprimir hoja de ingreso', ['report', 'id'=>$model->idCompra], ['class' => 'btn btn-info','target'=>'_blank']);
                      echo ' '.Html::a('<span class="glyphicon glyphicon-tags"></span> Imprimir etiquetas', ['compras-inventario/update', 'id'=>$model->idCompra, 'carga'=>0]/*['etiquetas', 'id'=>$model->idCompra]*/, ['class' => 'btn btn-info', 'id' => 'imprime_e_Button', 'data-loading-text'=>'Espere, imprimiendo...', 'onclick'=>'jsWebClientPrint.print()'/*,'target'=>'_blank'*/]);
                        echo '<button class="close tip" data-toggle="tooltip" title="Launch QZ" id="launch" href="#" onclick="launchQZ();" style="display: none;">
                                                <i class="fa fa-external-link"></i>
                              </button>';
                        echo '<font style="float:right">'.Html::a(' Notificar resolución al Ministerio de Hacienda', ['#'], [
                                'class'=>'fa fa-file-code-o fa-2x',
                                'id' => 'activity-resolucion',
                                'data-toggle' => 'modal',
                                //'onclick'=>'javascript:agrega_footer('.$model->idCabeza_Factura.')',//asigna id de factura para imprimir
                                'data-target' => '#modal_resolucion_xml',
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Notifique esta compra, enviando el xml y su resolución en menos de 8 días despues de la fecha de la factura de compra.')]).'</font>';
                        $sql = "SELECT * FROM recepcion_hacienda WHERE idCompra = :idCompra";
                        $command = \Yii::$app->db->createCommand($sql);
                        $command->bindValue(":idCompra", $model->idCompra);
                        $consulta = $command->queryAll();
                        $detalle_notificacion = '';

                        foreach ($consulta as $key => $value) {
                          $clave_recepcion = Html::a($value['id_factun'], null, [
                                        //'class'=>'fa-2x',
                                        'style'=>'cursor:pointer;',
                                        'data-content' => $value['clave_recepcion'],
                                        'data-toggle' => 'popover',
                                        'title' => Yii::t('app', 'Clave recepción'),
                                      ]);
                          $consulta = Html::a('<span class=""></span>', ['compras-inventario/pdf_resolucion', 'id'=>$value['id']], [
                                        'class'=>'fa fa-file-pdf-o fa-2x',
                                        'id' => 'pdf',
                                        'target'=>'_blank',
                                        'data-pjax' => '0',
                                        'title' => Yii::t('app', 'Mostrar Resolución en PDF'),
                                      ]);
                          if ( $value['respuesta_hacienda'] == '01' ) {
                            $respuesta_hacienda = '<font color="#26DD54">ACEPTADA POR HACIENDA</font>';
                          } elseif ( $value['respuesta_hacienda'] == '03' ) {
                            $respuesta_hacienda = '<font color="red">RECHAZADA POR HACIENDA</font>';
                          } else {
                            $respuesta_hacienda = 'ESPERANDO RESPUESTA';
                          }
                          $detalle_notificacion .= '<tr>

                                                      <td>'.date('d-m-Y h:i:s A', strtotime( $value['fecha'] )).'</td>
                                                      <td>'.$clave_recepcion.'</td>
                                                      <td>'.$value['tipo_documento'].'</td>
                                                      <td>'.$value['resolucion'].'</td>
                                                      <td>'.$value['detalle'].'</td>
                                                      <td>'.$respuesta_hacienda.'</td>
                                                      <td>'.$consulta.'</td>
                                                    </tr>';
                        }
                        echo '<div class="col-lg-3"></div><div class="col-lg-9"><br>
                        <table class="items table table-striped" >
                          <thead>
                            <tr>
                              <th>Fecha</th>
                              <th>ID Factun</th>
                              <th>Tipo documento</th>
                              <th>Resolución</th>
                              <th>Detalle</th>
                              <th>Respuesta de Hacienda</th>
                              <th></th>
                            <tr>
                          </thead>
                          <tbody>
                          '.$detalle_notificacion.'
                          </tbody>
                        </table>
                        </div>';
                       }
                   } else {
                      echo '<p>Asegurese de llenar bien los datos. Cuando termine de actualizar presione "Actualizar" para agregar productos a la compra</p>';
                      echo Html::submitButton($model->isNewRecord ? '<span class="fa fa-cart-plus"></span> Agregar Productos' : '<span class="fa fa-floppy-o"></span> Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']).'
                         '.Html::a('<span class="fa fa-ban"></span> Cancelar', ['cancelaru', 'id'=>$model->idCompra], ['class' => 'btn btn-default']).'
                         '.Html::a('<span class="fa fa-arrow-left"></span> Regresar', ['index'], ['class' => 'btn btn-default']);
                   }
              ?>
          </div>
          </div>

      </div>
      <?php ActiveForm::end(); ?>
      <?php if(CompraInventarioSession::getFormapagoup()) :
      if($_GET['carga'] == true){ ?>

          <script type="text/javascript">
          var aircod = '<?php echo CompraInventarioSession::getProductoAir() ?>';
          var airprecio = '<?php echo CompraInventarioSession::getPrecioAir() ?>';
          var airutil = '<?php echo CompraInventarioSession::getUtilidadAir() ?>';
          var airV = '<?php echo CompraInventarioSession::getVentadAir() ?>';
          var airVI = '<?php echo CompraInventarioSession::getVentaImdAir() ?>';
          $(window).load(function(){
              run_producto_air(aircod, airprecio, airutil, airV, airVI);
              /*$('#codProdServicio').val('listo');
              $('#modalAgregaprod').modal('show');*/
          });
      </script>
      <?php } ?>
      <?php if($model->estado != 'Aplicado') : ?>
      <div  id="formid" class="col-lg-12">
          <br>
          <legend>Proceda a agregar los productos de esta compra</legend>
          <p align="right"><font color="red">IMPORTANTE!</font> No agregue producto a la compra si existe una orden de compra pendiente que desea agregar, después de agregar la orden ya puede agregar cualquier otro producto.</p>
          <div class="">
              <div class="col-lg-2">
              <?= Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'id' => 'medio', 'class' => 'form-control', 'placeholder' =>'Ingrese código de producto aqui', 'onkeyup'=>'javascript:this.value=Mayuculas(this.value)', 'onchange' => 'javascript:run_producto_input(this.value)'])/*Select2::widget([
                          'name' => 'codProdServicio',
                          'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',
                              function($element) {
                                  return $element['codProdServicio'].' - '.$element['nombreProductoServicio'];
                              }),
                          'language'=>'es',
                          'options' => ['onchange'=>'javascript:run_producto(this.value)','placeholder' => '- Seleccione el producto a agregar -'],
                          'pluginOptions' => [
                              'allowClear' => true,
                          ],
                      ]);*/ ?>
              </div>
              <div class="col-lg-2">
              <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar producto a compra', ['#'], [
                              'class' => 'btn btn-success',
                              'id' => 'activity-index-link-agregaprod',
                              'data-toggle' => 'modal',
                              'onclick'=>'javascript:agrega_modalprod()',
                              'data-target' => '#modalAgregaprod',
                              //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                              'data-pjax' => '0',
                              'title' => Yii::t('app', 'Agrega un nuevo producto a la compra')]) ?>
              </div>
              <div class="col-lg-4">
              <?= $form->field($model, 'idOrdenCompra')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(OrdenCompraProv::find()
                                  ->where(['idProveedor'=>$model->idProveedor])
                                  ->andWhere('estado = :estado OR idOrdenCompra = :idOrdenCompra', [':estado'=>'Generada', ':idOrdenCompra'=>$model->idOrdenCompra])
                                  ->all(),'idOrdenCompra',
                        function($element) {
                          $compra = ComprasInventario::find()->where(['idOrdenCompra'=>$element['idOrdenCompra']])->one();
                          if (@$compra->idOrdenCompra != $element['idOrdenCompra']){
                            return 'Orden de compra No '.$element['idOrdenCompra'].' - Registrado el '.date('d-m-Y', strtotime($element['fecha_registro']));
                          } else {
                            return 'Orden de compra No '.$element['idOrdenCompra'].' (Apartada para la compra No '.$compra->idCompra.')';
                          }
                        }),
                    'language'=>'es',
                    'disabled' => $model->estado == 'Aplicado' ? true : false,
                    'options' => ['onchange'=>'cargar_compra()',
                                  'id'=>'idOrdenCompra',
                                  'placeholder' => '- Asigne la respectiva orden de compra -',
                                  'options' => ArrayHelper::map(OrdenCompraProv::find()
                                                ->where(['idProveedor'=>$model->idProveedor])
                                                ->andWhere('estado = :estado OR idOrdenCompra = :idOrdenCompra', [':estado'=>'Generada', ':idOrdenCompra'=>$model->idOrdenCompra])
                                                ->all(),'idOrdenCompra',
                                      function($element) {
                                        $compra = ComprasInventario::find()->where(['idOrdenCompra'=>$element['idOrdenCompra']])->one();
                                        if (@$compra->idOrdenCompra != $element['idOrdenCompra']){
                                          return ['disabled' => false];
                                        } else {
                                          return ['disabled' => true];
                                        }
                                      })
                                 ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Orden de compra') ?>
              </div>
              <div class="col-lg-4"><br>
              <?php if($products) : ?>


              <?= Html::a('<i class="fa fa-credit-card"></i> Aplicar Compra', ['aplicarcompra', 'id'=>$model->idCompra], [
                          'class' => 'btn btn-primary',
                          'id'=>'aplicar_compra',
                          //'onclick'=>'javascript:desactivarsession()',
                          'data' => [
                              'confirm' => '¿Seguro de aplicar esta Compra? si lo hace no podrá editar el detalle.',
                              'method' => 'post',
                          ],
                      ]) ?>

              <?php endif ?>
              <span style="float:right">
              <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar nuevo producto a inventario', ['#'], [
                              'class' => 'btn btn-success',
                              'id' => 'activity-index-link-agregaprod-inv',
                              'data-toggle' => 'modal',
                              'data-target' => '#modalAgregaprodinv',
                              'onclick' => '$.fn.modal.Constructor.prototype.enforceFocus = function() {};',
                              //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                              'data-pjax' => '0',
                              'title' => Yii::t('app', 'Agrega un nuevo producto a inventario')]) ?>
              </span>
              </div>
          </div>
      </div>
      <?php endif ?>
      <div id="porcentaje_masivo" class="col-lg-12 panel panel-warning" style="display: none;">
        <div class="panel-body">
          <div class="col-lg-6">
            Seleccione el check correspondiente al que desea aplicar el porcentaje masivo y luego seleccione <strong>"Aplicar % masivo"</strong>, podrá hacer esto las veces que sean necesarias de los productos marcados.
          </div>
          <div class="col-lg-2" title="% Descuento masivo">
            <div class="input-group">
              <span class="input-group-addon">
                <input type="checkbox" id="descuento_masivo_check" name="descuento_masivo_check">
              </span>
              <input type="text" id="descuento_masivo" onkeyup='habilitarbtn_aplica_masivo()' onkeypress="return isNumberDe(event)" class="form-control" placeholder="% Descuento masivo" disabled>
            </div><!-- /input-group -->
          </div>
          <div class="col-lg-2" title="% Utilidad masiva">
            <div class="input-group">
              <span class="input-group-addon">
                <input type="checkbox" id="utilidad_masiva_check" name="utilidad_masiva_check">
              </span>
              <input type="text" id="utilidad_masiva" onkeyup='habilitarbtn_aplica_masivo()' onkeypress="return isNumberDe(event)" class="form-control" placeholder="% Utilidad masiva" disabled>
            </div><!-- /input-group -->
          </div>
          <div class="col-lg-2">
            <a class="btn btn-warning btn-block" id="btn_aplicar_porc_masivo" onclick="obtener_productos_marcados(this)" data-loading-text="Espere, cambios en proceso..." ><span class="fa fa-cubes" aria-hidden="true"></span> Aplicar % masivo</a>
          </div>
        </div>
      </div>
      <div class="col-lg-12" id="conten_prom">
          <br>
          <?php
          //http://www.programacion.com.py/tag/yii
              //echo '<div class="grid-view">';
              $checkAll = '';
              if ($model->estado != 'Aplicado') {
                $checkAll = '<input type="checkbox" id="checkAll" name="checkAll" />';
              }
              echo '<table class="items table table-striped" id="tabla_producto_compra">';
              echo '<thead>';
              printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th>
              <th style="text-align:right;">%s</th><th style="text-align:right;">%s</th><th style="text-align:right;">%s</th><th style="text-align:center;">%s</th><th>%s</th><th class="actions button-column" ></th>
              <th class="actions button-column" style="text-align:right" title="Marca todos los productos para editarle sus porcentajes de descuento y/o porcentaje de utilidad">
              '.$checkAll.'</tr>',
                      '#',
                      'CÓDIGO',
                      'COD.PROV',
                      'PRODUCTO',
                      'CANT',
                      'PREC. COMPRA',
                      'PREC. VENTA',
                      'PREC. VENT + IMP',
                      'EXENT.IV',
                      'APLICADO'
                      );
              echo '</thead>';
          //if($products) {
          ?>
          <?php
          if($products) {
          echo '<tbody>';
              foreach($products as $position => $product) {
              $border = $product->modificado == 'Si' ? 'blue 10px solid;' : 'white 10px solid;';
              $eliminar = Html::a('', ['/compras-inventario/deleteitem_update', 'id' => $model->idCompra, 'cod' => $product['codProdServicio'] ], ['class'=>'glyphicon glyphicon-remove','data' => [
                                  'confirm' => '¿Está seguro de quitar este producto de la orden?'] ]);
              $actualizar = Html::a('', ['', 'id' => $position], [
                              'class'=>'glyphicon glyphicon-pencil',
                              'id' => 'activity-index-link-agregaprod',
                              'value' => $product['idDetalleCompra'],
                              'data-toggle' => 'modal',
                              'onclick'=>'javascript:actualiza_modalprod('.$product['idDetalleCompra'].')',
                              'data-target' => '#modalActualizaprod',
                              //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                              'data-pjax' => '0',
                              'title' => Yii::t('app', 'Actualizar producto') ]);
              $accion = $checkbox = '';
              if ($model->estado != 'Aplicado') {
                  $accion = $actualizar.' '.$eliminar;
                  $checkbox = '<input id="striped" type="checkbox" onChange="comprobar_check_marcado()" name="checkboxRow[]" value="'.$product['idDetalleCompra'].'">';
              } else {$accion = $product['usuario'];}

              $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
              printf('<tr style = "border-left:  '.$border.' ">
              <td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td align="right">%s</td><td align="right">%s</td>
              <td align="right">%s</td><td align="center">%s</td><td>%s</td><td>%s</td>
              <td class="actions button-column" align="right" title="Seleccione los productos que desea cambiar su porcentaje de descuento o utilidad">%s</td></tr>',
                          //$product['noEntrada'],
                          $position+1,
                          $product['codProdServicio'],
                          $product['codigo_proveedor'],
                          $modelpro->nombreProductoServicio,
                          $product['cantidadEntrada'],
                          number_format($product['precioCompra'],2),
                          //$product['descuento'],
                          //$product['porcentUtilidad'],
                          number_format($product['preciosinImp'],2),
                          number_format($product['precioconImp'],2),
                          $product['exImpuesto'],
                          //$product['usuario'],
                          $product['estado'],
                          $accion, $checkbox
                          );
                      //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/


              }
          echo '</tbody>';
      }
          ?>
          <?php
          //}
              echo '</table>';
          ?>
          </div>
      <?php endif ?>
  </div>
  </div>
</div>
<script type="text/javascript">

  </script>
<?php    //Esta modal es el formulario para agregar productos a la compra
      $form = ActiveForm::begin([ 'options' => ['name'=>'caja'], 'action'=>['/compras-inventario/agrega_modalprod_update', 'id'=>$model->idCompra] ]);
       Modal::begin([
              'id' => 'modalAgregaprod',
              'size'=>'modal-lg',
              'header' => '<center><h4 class="modal-title">Detalle de Entrada</h4></center>',
              'footer' => Html::a('<i class="glyphicon glyphicon-search"></i> Buscar producto', ['#'], [
                              'class' => 'btn btn-info',
                              'id' => 'activity-index-link-inventario',
                              'data-toggle' => 'modal',
                              //'onclick'=>'javascript:agrega_modalprod()',
                              'data-target' => '#modalinventario',
                              //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                              'data-pjax' => '0',
                              'title' => Yii::t('app', 'Busqueda en Inventario [F2]')]).
                              '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>
                              <input type="submit" value="Agregar" id="agregar_pro_comp" class="btn btn-success">',
          ]);
          //echo "<div id='modal_agrega'></div>";

      echo '<div class="panel panel-default">';
      echo '<div class="panel-body">';
      echo '<div class="well col-lg-12">';
      echo '<div id="datos_de_inventario"></div>';
      echo '</div>';
      $modeldetallecompra = new DetalleCompra();

      //echo $form->field($modeldetallecompra, 'noEntrada')->textInput();
      //echo $form->field($modeldetallecompra, 'codProdServicio')->textInput().'<br>';
      echo '<div class="col-lg-4">';
      /*echo $form->field($modeldetallecompra, 'codProdServicio')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',
                              function($element) {
                                  return $element['codProdServicio'].' - '.$element['nombreProductoServicio'];
                              }),
                          'language'=>'es',
                          'options' => ['onchange'=>'javascript:run_producto(this.value)','placeholder' => '- Seleccione -'],
                          'pluginOptions' => [
                              'allowClear' => true,
                          ],
                      ]);*/
      echo $form->field($modeldetallecompra, 'codProdServicio')->textInput(['id'=>'codProdServicio', 'readonly'=>true]);
      //echo $form->field($modeldetallecompra, 'precioCompra')->textInput(['id'=>'precioc', 'onkeyup'=>'javascript:obtenerPrecioVenta()']);
      if ($moneda_local != $moneda_prov) {
        echo '<div class="col-xs-5">
                <label>Calculadora</label>
                <input class="form-control" id="precio_dolar" type="text" placeholder="Dolares" onkeyup="javascript:convertir_dolar()">
              </div>
              <div class="col-xs-7">'.$form->field($modeldetallecompra, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                          //'name' => 'input-33',
                          'options' => ['id'=>'precioCompra', 'class'=>'precioc form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                          'clientOptions' => [
                              'alias' =>  'decimal',
                              'groupSeparator' => ',',
                             // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                              'autoGroup' => true
                          ],
                      ]).'</div>';
       }
      else {
        echo $form->field($modeldetallecompra, 'precioCompra')->widget(\yii\widgets\MaskedInput::className(), [
                          //'name' => 'input-33',
                          'options' => ['id'=>'precioCompra', 'class'=>'precioc form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                          'clientOptions' => [
                              'alias' =>  'decimal',
                              'groupSeparator' => ',',
                             // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                              'autoGroup' => true
                          ],
                      ]);
      }

      echo $form->field($modeldetallecompra, 'cantidadEntrada')->textInput();
      //echo $form->field($modeldetallecompra, 'descuento')->textInput();
      echo $form->field($modeldetallecompra, 'descuento')->widget(\yii\widgets\MaskedInput::className(), [
                        //'name' => 'input-33',
                        'options' => ['class'=>'descuento form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                        'clientOptions' => [
                            'alias' =>  'decimal',
                            'groupSeparator' => ',',
                            'autoGroup' => true
                        ],
                    ]);
      echo '</div>';
      echo '<div class="col-lg-4">';
      $modeldetallecompra->exImpuesto = 'No';
      echo $form->field($modeldetallecompra, 'exImpuesto')->radioList(['Si'=>'Si','No'=>'No']);
      //echo $form->field($modeldetallecompra, 'porcentUtilidad')->textInput(['id'=>'porcentu', 'onkeyup'=>'javascript:obtenerPrecioVentau()']);
      echo $form->field($modeldetallecompra, 'porcentUtilidad')->widget(\yii\widgets\MaskedInput::className(), [
                        //'name' => 'input-33',
                        'options' => ['id'=>'porcentUtilidad', 'class'=>'porcentu form-control', 'onkeyup'=>'javascript:obtenerPrecioVenta()', "onkeypress"=>"return isNumberDe(event)"],
                        'clientOptions' => [
                            'alias' =>  'decimal',
                            'groupSeparator' => ',',
                            'autoGroup' => true
                        ],
                    ]);
      //echo $form->field($modeldetallecompra, 'preciosinImp')->textInput(['id'=>'preciov', 'onkeyup'=>'obtenerUtilidadImpu()']);
      echo $form->field($modeldetallecompra, 'preciosinImp')->widget(\yii\widgets\MaskedInput::className(), [
                        //'name' => 'input-33',
                        'options' => ['id'=>'preciosinImp', 'class'=>'preciov form-control', 'onkeyup'=>'javascript:obtenerUtilidadImpu()', "onkeypress"=>"return isNumberDe(event)"],
                        'clientOptions' => [
                            'alias' =>  'decimal',
                            'groupSeparator' => ',',
                            'autoGroup' => true
                        ],
                    ]);
      //echo $form->field($modeldetallecompra, 'precioconImp')->textInput(['id'=>'precioventaimpu', 'onkeyup'=>'obtenerVentaUti()']);
      echo $form->field($modeldetallecompra, 'precioconImp')->widget(\yii\widgets\MaskedInput::className(), [
                        //'name' => 'input-33',
                        'options' => ['id'=>'precioconImp', 'class'=>'precioventaimpu form-control', 'onkeyup'=>'javascript:obtenerVentaUti()', "onkeypress"=>"return isNumberDe(event)"],
                        'clientOptions' => [
                            'alias' =>  'decimal',
                            'groupSeparator' => ',',
                            'autoGroup' => true
                        ],
                    ]);
      echo '</div>';
      echo '<div class="col-lg-4">';
      echo $form->field($modeldetallecompra, 'codigo_proveedor')->textInput(['readonly'=>false, 'class'=>'codigo_proveedor form-control']);
      echo $form->field($modeldetallecompra, 'usuario')->textInput(['value'=>Yii::$app->user->identity->username, 'readonly'=>true]);
      echo $form->field($modeldetallecompra, 'estado')->textInput(['value'=>'No Aplicado', 'readonly'=>true]);

      echo $form->field($modeldetallecompra, 'idCompra')->textInput(['value'=>$model->idCompra, 'type'=>'hidden']);

      echo '';

      //echo Html::a('<i class="glyphicon glyphicon-shopping-cart"></i> Guardar compra', '', ['class' => 'btn btn-primary']);
      echo '</div>';
      echo '</div>';
      echo '</div>';
      Modal::end();
      ActiveForm::end();
  ?>

  <?php
      Modal::begin([
              'id' => 'modalActualizaprod',
              'size'=>'modal-lg',
              'header' => '<center><h4 class="modal-title">Detalle de Entrada</h4></center>',
              'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '', [
                  'class'=>'btn btn-info',
                  //'target'=>'_blank',
                  'data-toggle'=>'tooltip',
                  'title'=>'Agregar este producto a la orden de compra'
              ])*/,
          ]);
      echo '<div id="modal_update_product"></div>';

      Modal::end();
  ?>

  <?php
      Modal::begin([//esta modal es para ingresar productos nuevos a inventario
              'id' => 'modalAgregaprodinv',
              'size'=>'modal-lg',
              'header' => '<center><h4 class="modal-title">Agregar nuevo producto a inventario</h4></center>',
              'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
          ]);
      echo '<div class="panel panel-default">';
      echo '<div class="panel-body">';
      $modelproducto = new ProductoServicios();
      $form = ActiveForm::begin([ 'options' => ['name'=>'cajapro'], 'action'=>['/compras-inventario/agrega_prod_inv_update', 'id' => $model->idCompra] ]);


  echo '<div class="producto-servicios-form">
          <div class="col-lg-4">';
              echo $form->field($modelproducto, 'tipo')->textInput(['readonly' => true, 'value' => 'Producto']);
              echo $form->field($modelproducto, 'codProdServicio')->textInput(['onkeyup'=>'javascript:this.value=Mayuculas(this.value)']);
              echo $form->field($modelproducto, 'nombreProductoServicio')->textarea(['maxlength' => 500]);
              echo $form->field($modelproducto, 'idUnidadMedida')->widget(Select2::classname(), [
                                 'data' => ArrayHelper::map($modelproducto->getUnidadMedida(),'idUnidadMedida', function($element){
                                         //return $element->numero_cuenta;
                                     return $element['descripcionUnidadMedida'].' / '.$element['simbolo'];
                                     }),
                                 'language'=>'es',
                                 'options' => ['placeholder' => '- Seleccione -'],
                                 'pluginOptions' => [
                                     'allowClear' => true,
                                 ],
                             ]);
    echo '</div>
          <div class="col-lg-4">
              <div class="col-lg-6">';
                  echo $form->field($modelproducto, 'cantidadMinima')->textInput();
        echo '</div>
              <div class="col-lg-6">';
                  echo $form->field($modelproducto, 'ubicacion')->textInput();
        echo '</div>';
        echo '<div class="col-lg-12">';
          echo $form->field($modelproducto, 'codFamilia')->widget(Select2::classname(), [
                      'data' => ArrayHelper::map($modelproducto->getNombreFamilia(),'codFamilia', function($element) {
                      return $element['codFamilia'].' - '.$element['descripcion'];
                  }),
                      'language'=>'es',
                      'options' => ['placeholder' => '- Seleccione -'],
                      'pluginOptions' => [
                          'allowClear' => true
                      ],
                  ])->label('Categoría');

        echo '</div>
              <div class="col-lg-12">';
                  echo $form->field($modelproducto, 'precioCompra')->textInput(['id'=>'preciocIn', 'onkeyup'=>'javascript:obtenerPrecioVentaIn()']);
       echo ' </div>
              <div class="col-lg-12">';
                  echo $form->field($modelproducto, 'porcentUnidad')->textInput(['id'=>'porcentuIn', 'onkeyup'=>'javascript:obtenerPrecioVentauIn()']);
                  $modelproducto->exlmpuesto = 'No';
                  $modelproducto->anularDescuento = 'No';
        echo '</div>
          </div>
          <div class="col-lg-4">';
              echo $form->field($modelproducto, 'precioVenta')->textInput(['id'=>'preciovIn', 'onkeyup'=>'obtenerUtilidadImpuIn()']);
              echo $form->field($modelproducto, 'exlmpuesto')->radioList(['Si'=>'Si','No'=>'No']);
              echo $form->field($modelproducto, 'precioVentaImpuesto')->textInput(['id'=>'precioventaimpuIn', 'onkeyup'=>'obtenerVentaUtiIn()']);
              echo $form->field($modelproducto, 'anularDescuento')->radioList(['Si'=>'Si','No'=>'No']);
              echo '<div class="form-group" style="text-align:right">';
              echo '<input type="submit" value="Crear" class="btn btn-primary">';
    echo '</div>';
  echo '</div>
      </div>';

      ActiveForm::end();
      echo '</div>';
      echo '</div>';
      Modal::end();
  ?>
  <?php
      $form = ActiveForm::begin([
          'action'=>['/compras-inventario/enviar_xml_not', 'id'=>$model->idCompra],
          'options'=>['enctype'=>'multipart/form-data']]); // important
           ?>

  <?php
       Modal::begin([//modal que me muestra la factura seleccionada
              'id' => 'modal_resolucion_xml',
              //'size'=>'modal-sm',
              'header' => '<center><h4 class="modal-title">Confirmación de resolución</h4></center>',
              'footer' => '<input type="submit" value="Enviar" class="btn btn-success" data-loading-text="Enviando espere..." onClick="esperar_confirmacion(this)"> <a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
          ]);
          $model_compras = new Notificar_compra();
          echo $form->field($model_compras, 'tipo_documento')->widget(Select2::classname(), [
          'data' => [
            "Factura Electrónica" => "Factura Electrónica",
            "Nota de débito electronica" => "Nota de débito electronica",
            "Nota de crédito electronica" => "Nota de crédito electronica"
          ],
          'language'=>'es',
          'options' => ['class'=>'tipo_documento_' ,'placeholder' => '- Seleccione -'],
          'pluginOptions' => [
              'initialize'=> true,'allowClear' => true
          ],
          ])->label('Tipo de documento:');
          echo $form->field($model_compras, 'resolucion')->widget(Select2::classname(), [
          'data' => [1 => "Aceptado", 2 => "Parcialmente aceptado", 3 => "Rechazado"],
          'language'=>'es',
          'options' => ['class'=>'resolucion_' ,'placeholder' => '- Seleccione -'],
          'pluginOptions' => [
              'initialize'=> true,'allowClear' => true
          ],
          ])->label('Resolución:');

          echo $form->field($model_compras, 'detalle')->textInput(['class'=>'detalle_ form-control','maxlength' => true])->label('Detalle:');
          echo $form->field($model_compras, 'archivo')->widget(FileInput::classname(), [
              'options' => [  'class'=>'archivo_' ],
              'pluginOptions' => [
              'allowedFileExtensions'=>['xml'],
              'showPreview' => true,
              'showCaption' => true,
              'showRemove' => false,
              'showUpload' => false
          ]])->label('Archivo XML que recibio por la compra:');
          Modal::end();
  ?>
    <?php ActiveForm::end(); ?>
