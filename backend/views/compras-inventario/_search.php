<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ComprasInventarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compras-inventario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idCompra') ?>

    <?= $form->field($model, 'numFactura') ?>

    <?= $form->field($model, 'fechaRegistro') ?>

    <?= $form->field($model, 'fechaVencimiento') ?>

    <?= $form->field($model, 'idProveedor') ?>

    <?php // echo $form->field($model, 'formaPago') ?>

    <?php // echo $form->field($model, 'subTotal') ?>

    <?php // echo $form->field($model, 'descuento') ?>

    <?php // echo $form->field($model, 'impuesto') ?>

    <?php // echo $form->field($model, 'total') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
