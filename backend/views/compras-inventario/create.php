<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\CompraInventarioSession;
use yii\bootstrap\Modal;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';

/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */

$this->title = 'Nueva Compra';
$this->params['breadcrumbs'][] = ['label' => 'Compras Inventarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//FUNCION PARA ABRIR LA MODAL DE BUSQUEDA DE MODAL
window.onkeydown = tecla;

    function tecla(event) {
        //event.preventDefault();
        num = event.keyCode;

        if(num==113)
            $('#modalinventario').modal('show');//muestro la modal

    }
  //Funcion obtener datos de radio y darle opciones
	$(function(){
		$('#pago :radio').change(function(){
            var fpago = $(this).val();
            if (fpago == 'Contado')
                  {
                    $('#contenido_a_mostrar').hide("slow");
                    //$('#fecVe').prop('readonly', true);
                    $('#fecVe').val('');
                  }
            else if (fpago == 'Crédito')
            {
                $('#contenido_a_mostrar').show("slow");
                //$('#fecVe').prop('readonly', true);
                $('#fecVe').val('00-00-0000');
            }
        });
	});
    //Funcion para que me permita ingresar solo numeros
    function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58))
                return true;
            return false;
        }
    //Funcion para obtener la suma de 30 dias a la fecha de documento
    function agrega_fecha(fecha){
    	var fVe = $("#fecVe").val();

    	var idProveedor = document.getElementById("idProveedor").value;
    	$.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/sumafecha') ?>",
        type:"post",
        data: { 'fechaR': fecha, 'idProveedor': idProveedor },
        success: function(data){
        //$("#conten_pro").load(location.href+' #conten_pro','');
        //alert(fpago);
        if (fVe!='') {
            //$('#fecVe').val(data);
            document.getElementById("fecVe").value = data;
            //document.getElementById('fecVe').disabled = true;
        } else { $('#fecVe').val(''); }
        	//$('#fecVe').val(data);
        },
        error: function(msg, status,err){
            $('#fecVe').val('No llegó');
        }
    });
    }
    function desactivarsession(){
        var formaPago = '<?php echo CompraInventarioSession::getFormapago() ?>';
        /*var fechaRegi =
        var fechaVenc =
        var proveedor =
        var noFactura =
        var subtotal =
        var descuento =
        var impuesto =
        var total =*/
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/desactivarsession') ?>",
            type:"post",
            data: { 'formaPago': formaPago },
            success: function(data){
                //location.reload();
                //$("#conten_pro").load(location.href+' #conten_pro','');
                //$('#fecVe').val('bien');
            },
            error: function(msg, status,err){
             alert('No pasa 83');
            }
        });
    }

    //funsion para actualizar un producto en session
    function actualiza_modalprod(position){
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/actualiza_modalprod') ?>",
            type:"post",
            data: { 'position': position },
            success: function(data){
                $( "#modal_update_product" ).html( data );
            },
            error: function(msg, status,err){
             alert('No pasa 96');
            }
        });
    }
    //carga producto desde input
    function run_producto_input(idproducto) {
      var idProveedor = document.getElementById("idProveedor").value;
      $.ajax({
       url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/run_producto_input') ?>",
       type:"post",
       data: { 'idProveedor': idProveedor, 'idproducto': idproducto },
       success: function(data){
         carga_producto_compra(idproducto, data);
       },
       error: function(msg, status,err){
        alert('No pasa 114');
       }
   });

    }
    //carga los datos del producto que seleccione a la modal para agregar el producto a la compra
    /*function run_producto(idproducto){
       $.ajax({
        url:"<?php //echo Yii::$app->getUrlManager()->createUrl('compras-inventario/run_producto') ?>",
        type:"post",
        data: { 'idproducto': idproducto },
        success: function(data){
            $( "#datos_de_inventario" ).html( data );
            $('.codProdServicio').val(idproducto);
						//$('input:text[name=codProdServicio]').val(idproducto);
            $('#modalAgregaprod').modal('show');
        },
        error: function(msg, status,err){
         //alert('No pasa 114');
        }
    });
  }*/

    //esta funcion me permite mostrar el formulario para agregar producto a la compra
    function mostrardetallentrada() {
      $('#modalAgregaprod').modal('show');
    }

    //esta funcion me permite cargar los productos que recientemente se ingresaron a inventario a la nueva compra
    //abriendo una nueva modal para la compra
    function run_producto_air(aircod, airprecio, airutil, airV, airVI){
       //var v_placa = document.getElementById("ddl-placa").value;
       $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/run_producto_air') ?>",
        type:"post",
        dataType: 'json',
        data: { 'aircod': aircod },
        success: function(data){
            $( "#datos_de_inventario" ).html( data.encab );
            /*document.getElementById("precioCompra").value = data.precioCompra;
            document.getElementById("porcentUtilidad").value = data.porcentUtilidad;
            document.getElementById("preciosinImp").value = data.preciosinImp;
            document.getElementById("precioconImp").value = data.precioconImp;*/
            $('.codProdServicio').val(aircod);
            $('.descuento').val(data.descuento);
						//$('input:text[name=codProdServicio]').val(aircod);
						$('.precioc').val(data.precioCompra); //$('input:text[name=precioc]').val(airprecio);//
            $('.porcentu').val(data.porcentUtilidad); //$('input:text[name=porcentu]').val(airutil);//
            $('.preciov').val(data.preciosinImp);//$('input:text[name=preciov]').val(airV);//
            $('.precioventaimpu').val(data.precioconImp);//$('input:text[name=precioventaimpu]').val(airVI);//
            $(".cant_entr").focus();
            $('#modalAgregaprod').modal('show');
        },
        error: function(msg, status,err){
         alert('No pasa 137');
        }
    });
    }
    //----------------------------------------------------------------------------------------------------
    //Estas funciones son para agregar productos a la compra
		function isNumberDe(evt) {//funcion que me permite ingresar solo numeros con decimales
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

    //Transformar a mayúsculas todos los caracteres
    function Mayuculas(tx){
      //Retornar valor convertido a mayusculas
      return tx.toUpperCase();
    }

		//esta funcion obtiene el precio de compra (considera descuento y porcentage utilidad) y genera precio sin impuesto y precio con impuesto
		//PARA AGREGAR PRODUCTO
    function obtenerPrecioVenta() {//Obtengo los valores de compra en el orden que sea
            //caja=document.forms["caja"].elements;
            precioc = $(".precioc").val();//Number(caja["precioc"].value);
            //var precioc = document.getElementById("precioc").value;
						porcentu = $(".porcentu").val();
						descuento = $(".descuento").val();
						$.ajax({
										url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenerprecioventa') ?>",
										type:"post",
										dataType: 'json',
										data: { 'precioc' : precioc, 'porcentu' : porcentu, 'descuento' : descuento},
										success: function(respuesta){
											$(".preciov").val(respuesta.preciov);//caja["preciov"].value=total.toFixed(2);
					            $(".precioventaimpu").val(respuesta.precioventaimpu);//caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
                      if (porcentu < 0) {
                        $("#agregar_pro_comp").prop('disabled', true);
                      } else {
                        $("#agregar_pro_comp").prop('disabled', false);
                      }
                    },
										error: function(msg, status,err){
										 alert('Error linea 148');
										}
									});

        }
				//PARA ACTUALIZAR PRODUCTO
				function obtenerPrecioVenta_() {//Obtengo el valor del checkbox seleccionado para la suma
				  precioc = $(".precioc_").val();//Number(caja["precioc"].value);
				  //var precioc = document.getElementById("precioc").value;
				  porcentu = $(".porcentu_").val();
				  descuento = $(".descuento_").val();
				  $.ajax({
				          url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenerprecioventa') ?>",
				          type:"post",
				          dataType: 'json',
				          data: { 'precioc' : precioc, 'porcentu' : porcentu, 'descuento' : descuento },
				          success: function(respuesta){
				            $(".preciov_").val(respuesta.preciov);//caja["preciov"].value=total.toFixed(2);
				            $(".precioventaimpu_").val(respuesta.precioventaimpu);//caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
				          },
				          error: function(msg, status,err){
				           alert('Error linea 148');
				          }
				       });
				}

		//Esta funcion me obtiene el precio sin impuesto (considera el precio de compra y el descuento) me genera la utilidad y el precio mas impuesto
		//PARA AGREGAR PRODUCTO
    function obtenerUtilidadImpu() {
            //caja=document.forms["caja"].elements;
            precioc = $(".precioc").val(); //precioc=Number(caja["precioc"].value);
            preciov = $(".preciov").val(); //preciov=Number(caja["preciov"].value);
						descuento = $(".descuento").val();
						$.ajax({
										url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenerutilidadimpu') ?>",
										type:"post",
										dataType: 'json',
										data: { 'precioc' : precioc, 'preciov' : preciov, 'descuento' : descuento },
										success: function(respuesta){
											$(".porcentu").val(respuesta.porcentu);//caja["preciov"].value=total.toFixed(2);
					            $(".precioventaimpu").val(respuesta.precioventaimpu);//caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
                      if (respuesta.porcentu < 0) {
                        $("#agregar_pro_comp").prop('disabled', true);
                      } else {
                        $("#agregar_pro_comp").prop('disabled', false);
                      }
                    },
										error: function(msg, status,err){
										 alert('Error linea 148');
										}
									});
        }
		//PARA ACTUALIZAR PRODUCTO
		function obtenerUtilidadImpu_(){
			precioc = $(".precioc_").val(); //precioc=Number(caja["precioc"].value);
			preciov = $(".preciov_").val(); //preciov=Number(caja["preciov"].value);
			descuento = $(".descuento_").val();
			$.ajax({
							url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenerutilidadimpu') ?>",
							type:"post",
							dataType: 'json',
							data: { 'precioc' : precioc, 'preciov' : preciov, 'descuento' : descuento },
							success: function(respuesta){
								$(".porcentu_").val(respuesta.porcentu);//caja["preciov"].value=total.toFixed(2);
								$(".precioventaimpu_").val(respuesta.precioventaimpu);//caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
                if (respuesta.porcentu < 0) {
                  $("#agregar_pro_comp_u").prop('disabled', true);
                } else {
                  $("#agregar_pro_comp_u").prop('disabled', false);
                }
              },
							error: function(msg, status,err){
							 alert('Error linea 148');
							}
						});
		}

		//Esta funcion me obtiene el precio con impuesto (considera el precio de compra y el descuento) me genera la utilidad y el precio sin impuesto
		//PARA AGRAGAR PRODUCTO
    function obtenerVentaUti() {//Obtengo el valor del checkbox seleccionado para la suma
            //caja=document.forms["caja"].elements;
            precioc = $(".precioc").val();//Number(caja["precioc"].value);
            precioventaimpu = $(".precioventaimpu").val();//Number(caja["precioventaimpu"].value);
						descuento = $(".descuento").val();
						$.ajax({
										url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenerventauti') ?>",
										type:"post",
										dataType: 'json',
										data: { 'precioc' : precioc, 'precioventaimpu' : precioventaimpu, 'descuento' : descuento },
										success: function(respuesta){
											$(".porcentu").val(respuesta.porcentu);//caja["preciov"].value=total.toFixed(2);
					            $(".preciov").val(respuesta.preciov);//caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
                      if (respuesta.porcentu < 0) {
                        $("#agregar_pro_comp").prop('disabled', true);
                      } else {
                        $("#agregar_pro_comp").prop('disabled', false);
                      }
                    },
										error: function(msg, status,err){
										 alert('Error linea 148');
										}
									});
            /*preciov = precioventaimpu/1.13;
            //el % del margen sale del el precio de ganacia dividido del precio de compra
            utilidad = preciov/precioc;

            //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
            subtotalporc=utilidad*100;
            //para luego restarle 100 dejandonos el margen de utilidad
            totalporc=subtotalporc-100;
            caja["porcentu"].value=totalporc.toFixed(2);
            caja["preciov"].value=preciov.toFixed(2);*/
        }
		//PARA ACTUALIZAR PRODUCTO
		function obtenerVentaUti_() {//Obtengo el valor del checkbox seleccionado para la suma
			precioc = $(".precioc_").val();//Number(caja["precioc"].value);
			precioventaimpu = $(".precioventaimpu_").val();//Number(caja["precioventaimpu"].value);
			descuento = $(".descuento_").val();
			$.ajax({
							url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtenerventauti') ?>",
							type:"post",
							dataType: 'json',
							data: { 'precioc' : precioc, 'precioventaimpu' : precioventaimpu, 'descuento' : descuento },
							success: function(respuesta){
								$(".porcentu_").val(respuesta.porcentu);//caja["preciov"].value=total.toFixed(2);
								$(".preciov_").val(respuesta.preciov);//caja["precioventaimpu"].value=(total+(total * iva)).toFixed(2);
							},
							error: function(msg, status,err){
							 alert('Error linea 148');
							}
						});
        }
    //----------------------------------------------------------------------------------------------------
    //Estas funciones son para agregar productos a inventario
    function obtenerPrecioVentaIn() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["cajapro"].elements;
            precioc=Number(caja["preciocIn"].value);
            //var precioc = document.getElementById("precioc").value;
            iva=13/100;
            //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
            utilidad=Number(caja["porcentuIn"].value)/100;
            //var utilidad = $("#porcentu").val();

            //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
            subtotal=precioc*utilidad;
            //para luego sumarlo al precio de compra
            total=precioc+subtotal;
            caja["preciovIn"].value=total.toFixed(2);
            caja["precioventaimpuIn"].value=(total+(total * iva)).toFixed(2);
        }
    function obtenerPrecioVentauIn() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["cajapro"].elements;
            precioc=Number(caja["preciocIn"].value);
            iva=13/100;

            //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
            utilidad=Number(caja["porcentuIn"].value)/100;

            //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
            subtotal=precioc*utilidad;
            //para luego sumarlo al precio de compra
            total=precioc+subtotal;
            caja["preciovIn"].value=total.toFixed(2);
            caja["precioventaimpuIn"].value=(total+(total * iva)).toFixed(2);
        }
    function obtenerUtilidadImpuIn() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["cajapro"].elements;
            precioc=Number(caja["preciocIn"].value);
            preciov=Number(caja["preciovIn"].value);
            iva=13/100;
            //el % del margen sale del el precio de ganacia dividido del precio de compra
            utilidad = preciov/precioc;

            //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
            subtotal=utilidad*100;
            //para luego restarle 100 dejandonos el margen de utilidad
            total=subtotal-100;
            caja["porcentuIn"].value=total.toFixed(2);
            caja["precioventaimpuIn"].value=(preciov+(preciov * iva)).toFixed(2);
        }
    function obtenerVentaUtiIn() {//Obtengo el valor del checkbox seleccionado para la suma
            caja=document.forms["cajapro"].elements;
            precioc=Number(caja["preciocIn"].value);
            precioventaimpu=Number(caja["precioventaimpuIn"].value);

            preciov = precioventaimpu/1.13;
            //el % del margen sale del el precio de ganacia dividido del precio de compra
            utilidad = preciov/precioc;

            //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
            subtotalporc=utilidad*100;
            //para luego restarle 100 dejandonos el margen de utilidad
            totalporc=subtotalporc-100;
            caja["porcentuIn"].value=totalporc.toFixed(2);
            caja["preciovIn"].value=preciov.toFixed(2);
        }
    //activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
    });

    //me permite buscar por like los productos a BD
    function buscar() {
        var idProveedor = document.getElementById("idProveedor").value;
        var textoBusqueda_cod = $("input#busqueda_cod").val();
        var textoBusqueda_des = $("input#busqueda_des").val();
        var textoBusqueda_fam = $("input#busqueda_fam").val();
        var textoBusqueda_cat = $("input#busqueda_cat").val();
        var textoBusqueda_cpr = $("input#busqueda_cpr").val();
        var textoBusqueda_ubi = $("input#busqueda_ubi").val();
        if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
            $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/busqueda_producto') ?>",
                type:"post",
                data: {
                    'idProveedor' : idProveedor,
                    'valorBusqueda_cod' : textoBusqueda_cod,
                    'valorBusqueda_des' : textoBusqueda_des,
                    'valorBusqueda_fam' : textoBusqueda_fam,
                    'valorBusqueda_cat' : textoBusqueda_cat,
                    'valorBusqueda_cpr' : textoBusqueda_cpr,
                    'valorBusqueda_ubi' : textoBusqueda_ubi },
                beforeSend : function() {
                  $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 286');
                }
            });
        }
    }

    //Me agrega el producto que seleccioné luego de la busqueda para cargarlo a la modal que agrega productos a la compra
    function carga_producto_compra(codPro, codigo_proveedor){

        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/run_producto') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idproducto': codPro },
                success: function(data){
                    $('#modalinventario').modal('hide');
                    $("#busca_focus").slideDown(function(){
                    //$(".cant_entr").focus();
                    });
                    $( "#datos_de_inventario" ).html( data.encab );
                    /*document.getElementById("precioCompra").value = data.precioCompra;
                    document.getElementById("porcentUtilidad").value = data.porcentUtilidad;
                    document.getElementById("preciosinImp").value = data.preciosinImp;
                    document.getElementById("precioconImp").value = data.precioconImp;*/
                    $('.precioc').val(data.precioCompra); //$('input:text[name=precioc]').val(airprecio);//
                    $('.porcentu').val(data.porcentUtilidad); //$('input:text[name=porcentu]').val(airutil);//
                    $('.preciov').val(data.preciosinImp);//$('input:text[name=preciov]').val(airV);//
                    $('.precioventaimpu').val(data.precioconImp);//$('input:text[name=precioventaimpu]').val(airVI);//

                    $('.codProdServicio').val(codPro);
                    $('.descuento').val(data.descuento);
                    $('.codigo_proveedor').val(codigo_proveedor);

                    $('#modalAgregaprod').modal('show');//muestro la modal
										//$('input:text[name=codProdServicio]').val(codPro);

                },
                error: function(msg, status,err){
                 alert('Producto no encontrado');
                }
            });
    }

    //carga los productos desde una compra
    function cargar_compra() {
      idOrdenCompra = document.getElementById("idOrdenCompra").value;
      $.ajax({
       url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/cargar_compra') ?>",
       type:"post",
       data: { 'idOrdenCompra': idOrdenCompra },
       success: function(data){
       },
       error: function(msg, status,err){
       }
    });
    }
</script>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="compras-inventario-create">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>


    <center><h1><?= Html::encode($this->title) ?><small><br>Si necesita incluir costos por fletes primero guarde la compra.</small></h1></center>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php
//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a> '.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar nuevo producto a inventario', ['#'], [
                                'class' => 'btn btn-success',
                                'data-dismiss'=>'modal', //me quita la modal activa asi puedo agregar la otra
                                'id' => 'activity-index-link-agregaprod-inv',
                                'data-toggle' => 'modal',
                                'data-target' => '#modalAgregaprodinv',
                                'onclick' => '$.fn.modal.Constructor.prototype.enforceFocus = function() {};',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Agrega un nuevo producto a inventario')]),
        ]);
        echo '<div class="well">';
        //echo '<input type="text" name="busqueda" id="busqueda" value="" placeholder="" maxlength="30" autocomplete="off" onKeyUp="buscar();" />';
        echo '  <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
                                </div>';
        echo '
                <br>
                    <table class="table table-hover" id="tabla_lista_productos">
                        <thead>
                            <tr>
                              <th width="150">CÓDIGO LOCA</th>
                              <th width="380">DESCRIPCIÓN</th>
                              <th width="100">CANT.INV</th>
                              <th width="130">COD.ALTER</th>
                              <th width="120">CÓD.PROV</th>
                              <th width="50">UBICACIÓN</th>
                              <th width="140">PRECIO + IV</th>
                              <th class="actions button-column" width="60">&nbsp;</th>
                            </tr>
                        </thead>



                    </table><div id="resultadoBusqueda" style="height: 400px;width: 100%; overflow-y: auto; ">
                </div>
             </div>';
        echo '';


        Modal::end();
