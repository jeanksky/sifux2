<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
//use backend\models\DetalleCompra;
use backend\models\ProductoServicios;
use backend\models\CompraInventarioSession;
use backend\models\Proveedores;
use backend\models\Usuarios;
use kartik\widgets\AlertBlock;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use backend\models\Empresa;
use yii\helpers\Url;
use yii\helpers\Json;
use backend\models\DetalleCompra;
use backend\models\Etiquetas;
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
use backend\Zebra\src\Client;
use backend\Zebra\src\Zpl\Builder;
use backend\Zebra\src\Zpl\Image;
/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */
$empresa_funcion = new Empresa();
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
//https://github.com/robgridley/zebra
//https://packagist.org/packages/alex-bond/yii2-thumbler

//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
echo $this->render('etiquetas_marcadas', [//aqui llama etiquetas_marcadas que imprime la factura correspondiente a este id

  ]);
/*    $urlParts = parse_url($_SERVER['REQUEST_URI']);

    if (isset($urlParts['query'])){
    	//
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){

            $empresaimagen = new Empresa();//Se obtienen datos de la empresa
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//Se obtienen datos de la empresa
            //$products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->all();//Se obtienen datos del detalle de compra

            $etiquetas_print = Etiquetas::find()->where(["=",'estado', 'act'])->all();//Se obtienen datos del detalle de compra
            $nombre_impresora = 'ETIQUETASNPS';
            //$printerName = urldecode($qs['printerName']);//Obtiene el nombre de la impresoradesde el scrip
            $cmds =  "";//declaro variable que obtiene el contenido de todas las etiquetas a imprimir

            if ($etiquetas_print) {

            $image = new Image(file_get_contents('../web/' . $empresa->logo_etiqueta));//obtenemos la imagen de acuerdo a la direccion
            $zpl = new Builder(); //creamos una instancia de donde vamos a corregir a la imagen en codigo zpl
            $zpl->fo(30, 10)->gf($image)->fs(); //convertimos la imagen y de lamos una pocisión en el mapa de la etiqueta

            foreach($etiquetas_print as $position => $etiqueta) {//recorro todos los productos que serán pasados por etiquetas
                //$producto = ProductoServicios::find()->where(['codProdServicio'=>$etiqueta['codProdServicio']])->one();//obtengo los datos de cada producto que necesito imprimir en la etiqueta
                if ($etiqueta['estado']=='act') {
	                $cantidad_et = intval($etiqueta['cantidad']);
	                //for ($i=1 ; $i<=$cantidad_et ;$i++) { //recorro la cantidad de ese producto que esta en inventario

	                    //Estos serían los comandos Zebra ZPL para moldear la etiqueta
	                    $cmds .= "^XA";//inicio de etiqueta
	                    $cmds .= "^LH10,20";//con solo 10 punto de ancho con 20 de alto inicio de etiqueta
	                    $cmds .= "^FO20,1^GB750,175,4^FS";//primer borde de primer fila
	                    $cmds .= "^FO20,1^GB750,360,4^FS";//segundo borde de segunda fila
	                    $cmds .= "^FO20,171^GB320,190,4^FS";//linea verticar de la segunda fila

                        //para la hora de imprimir la imagen debemos de quitarle el inicio y corte de imprecion que el código genera por eso
                        $cmds .= substr(strval($zpl), 3, -3); //substraemos los primeros y utimos 3 caracteres que me abren y cierran la impreción
	                   // $cmds .= "^FO50,20^GFA,2375,2375,25,,::::hJ02,hJ07,hJ078,hJ0F8,hI01FC,:hI03FE,hI03FF,hI07FF,hI07FF8,hI0IF8,hI0IFC,hH01IFE,hH03IFE,hH03JF,hH07JF,hH07JF8,hH0FFDFF8,hH0FFCFFC,gH01EX0FF07FC,001E00F01F1FFC1FFE0F00FFCIF3F01EIFE7FF01F803FE,001E00F01F1IF1IF0F03FFCIF3F81EIFE7FF81F0033E,001E00F01F1IF1IF8F07FF8IF3F81EIFE7FFC3E0071E,001E00F01F1E0F9F0F8F0FE08F803FC1E7FFE787E3C04E0F,001E00F01F1E0F9F07CF0F800F003FC1E07C0783E381DE0F,001E00F01F1E0F9F07CF1FI0F003FE1E07C0783E783FF07,001E00F01F1E0F1F07CF1FI0F003DE1E07C0783E707FF878,001E00F01F1F7F1F0F8F1EI0F823CF1E07C0783C70IF838,001E00F01F1FFC1IF0F3EI0FFE3CF1E07C078FC70IFC38,001E00F01F1FFE1FFE0F3EI0FFE3CF9E07C07FF8F0IFC38,001E00F01F1IF1FFE0F3EI0FFE3C79E07C07FF0F0IFC38,001E00F01F1E0F9IF0F3EI0F003C3DE07C07FF8F0IFC38,001E00F01F1E079F1F0F1EI0F003C3FE07C078F870IF838,001E00F81F1E079F0F8F1FI0F003C1FE07C0787C707FF878,001E00F81E1E079F0F8F1F800F003C1FE07C0787C783FF078,001E007C3E1E0F9F078F0FC00F003C0FE07C0783C781FE0F,001FFE7FFE1IF9F07CF0IF8IF3C0FE07C0783E3C1C80F,001FFE3FFC1IF1F07CF07FF8IF3C07E07C0783E3E3801E,001FFE1FF81FFC1F07CF01FFCIF3C07E07C0781E1F7003E,M07C00FE00A03C5003F0IF1C03C0380781E0FF007C,hH07F83F8,hH03FDFF,hH01IFC,hI07FF,hJ078,007F8003FCI0FFCI07FL01IFI03FF8I0FF8003F8,007F8003FC001FFEI0FFL0FF3FC00IFEI0FFC003FC,004180030C001806I0C1K03EI0C03C007800C060030C,006180030C001802I0C1K07J0C0EI01C00C060030C,004180030C003003I0C1K0EJ081CJ0600C030030C,004180030C0031C3I0C1J01807FF8381FE0300C030030C,004180030C0031C3I0C1J0301F0F8303878180C018030C,004180030C006141800C1J0607J060601C0C0C008030C,004180030C006361800C1J0C0EJ0C0C0040C0810C030C,004180030C006321800C1J0C1CJ0C180060608386030C,004180030C004320800C1I01818I0181I03060C386030C,004180030C00C230C00C1I0183J01830043020C2C3030C,004180030400C230C00C1I0182J018218C1830C2C3030C,004180030400C610400C1I0306J03060CC1830C261830C,0040IFE04018618600C1I0306J03061CE1810C260830C,00401FFC04018418600C1I0306J03061FE0810C230C30C,004L04018C18200C1I0304J03061FE0810C230430C,004L04030C08300C1I0304J03061FF0810C218630C,004L04030C0C300C1I0304J03040FE08108218230C,00407FFC0403080C100C1I0304J03060FE0810820C330C,0041IFE04061804180C1I0306J030607F8810820C130C,004180030C060FFC180C1I0306J030623FC81082061B0C,004180030C060180180C1I0306J03063FE18308206090C,004180030C04K080C1I0302J0302076183082030D0C,004180030C0CK0C0C1I0103J0103007902082010F0C,004180030C0C1FFC0C0C1I0181J01830023020C201870C,0041800304083FFE040C1I01818I01818003060C200C30C,0041800304183003060C1J0C0CJ0C18006040C200C00C,0041800304182003060C1J0C07J0C0C00C0C0C200600C,0041800304186001020C1IFE603C038607018180C200600C,0041800304306001830C0IFE300IF8303FF0380C200300C,004180030C304001830CJ061800F98380FC0700C200300C,004180030C30CI0810CJ060EI0181CJ0E00C200180C,004180030460CI0C18CJ06078I0C07I03800C2I080C,007F8003FC7FCI0FF8KFE01F007C03C01FI0FEI0FFC,007F8003FC7F8I0FF8KFE007IF800IFCI0FEI07FC,gN03EK0FC,,:::::::^FS"; //f1_a imagen de la empresa

	                    $cmds .= "^FO280,30^ADN,18,10^FD FECHA REGISTRO: ^FS";//f1_b
	                    $cmds .= "^FO280,55^ADN,36,20^FD ".$etiqueta['fecha_reg']." ^FS";//f1_c

	                    $cmds .= "^FO280,100^ADN,18,10^FD DESCRIPCION: ^FS";//f1_d
	                    $cmds .= "^FO280,125^ADN,36,20^FD ".substr($etiqueta['nombre'], 0, 18)." ^FS";//f1_E

	                    $cmds .= "^FO30,200^ADN,18,10^FD UBI: ^FS";//f2_c1_a
	                    $cmds .= "^FO30,225^ADN,36,20^FD ".$etiqueta['ubi']." ^FS";//f2_c1_b

	                    $cmds .= "^FO30,280^ADN,18,10^FD PRECIO COLONES: ^FS";//f2_c1_c
	                    $cmds .= "^FO30,305^ADN,36,20^FD ".number_format($etiqueta['precio'],2)." ^FS";//f2_c1_d

	                    $cmds .= "^FO340,200^ADN,18,10^FD CODIGO: ^FS";//f2_c2_a
	                    $cmds .= "^FO340,225^ADN,36,20^FD ".$etiqueta['codigo']." ^FS";//f2_c2_b

	                    $cmds .= "^FO400,260^BY1^B3N,,80^FD".$etiqueta['codigo']."^FS";//f2_c2_c

	                    //$cmds .= "^PQ1,0,1,Y"; //mantiene un control de constancia por la impreción de varias etiquetas.
                      $cmds .= "^PQ".$cantidad_et;
                      $cmds .= "^XZ";//cierre de la etiqueta
	                //}//fin for
            	}//fin if
            }//fin foreach
            }//fin if $etiquetas_print
            else {
            	$cmds .= "^XA";//inicio de etiqueta
            	$cmds .= "^FO30,200^ADN,18,10^FD NO SE ENCUENTRAN DATOS DE etiquetas_print ".$etiquetas_print." ^FS";//f2_c1_a
            	$cmds .= "^XZ";//cierre de la etiqueta
            }
            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente

            $cpj->clientPrinter = new InstalledPrinter($nombre_impresora);


            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();
            echo $cpj->sendToClient();

            ob_end_flush();
            exit();
        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de etiqueta-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
*/


$this->title = 'Etiquetas';
$this->params['breadcrumbs'][] = ['label' => 'Compras Inventarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//me permite buscar por like los productos a BD
    function buscar() {
      var textoBusqueda_cod = $("input#busqueda_cod").val();
      var textoBusqueda_des = $("input#busqueda_des").val();
      var textoBusqueda_fam = $("input#busqueda_fam").val();
      var textoBusqueda_cat = $("input#busqueda_cat").val();
      var textoBusqueda_cpr = $("input#busqueda_cpr").val();
      var textoBusqueda_ubi = $("input#busqueda_ubi").val();
        if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
            $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');

        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/busqueda_producto') ?>",
                type:"post",
                data: {
                  'valorBusqueda_cod' : textoBusqueda_cod,
                  'valorBusqueda_des' : textoBusqueda_des,
                  'valorBusqueda_fam' : textoBusqueda_fam,
                  'valorBusqueda_cat' : textoBusqueda_cat,
                  'valorBusqueda_cpr' : textoBusqueda_cpr,
                  'valorBusqueda_ubi' : textoBusqueda_ubi
                 },
                 beforeSend : function() {
                   $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                 },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 286');
                }
            });
        }
    }
    //Me agrega el producto que seleccioné luego de la busqueda para cargarlo a la modal que agrega productos a la compra
    function carga_producto_compra(codPro){
        //alert(codPro);
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/run_producto_etiqueta') ?>",
                type:"post",
                data: { 'idproducto': codPro },
                success: function(data){
                    $( "#datos_de_inventario" ).html( data );
                    $('#codProdServicio').val(codPro);
                    $('#modalinventario').modal('hide');
                },
                error: function(msg, status,err){
                 alert('Producto no encontrado');
                }
            });
    }

    //funcion que agrega etiquetas con su respectiva cantidad
    function run_agrega_etiquetas_cantidad(cantidad, producto){
    	var producto = document.getElementById("producto_code").value;
    	var cantidad = document.getElementById("cant_etiq").value;
    	//alert('cantidad:'+cantidad+' Producto:'+producto);
    	$.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/run_agrega_etiqueta') ?>",
                type:"post",
                data: { 'producto': producto, 'cantidad' : cantidad },
                success: function(data){

                },
                error: function(msg, status,err){
                 //alert('No pasa 70');
                }
            });
    }

    //funcion para agregar una compra a etiquetas
    function run_agrega_fac_etiq(){
        var compra = document.getElementById("fac_etiq").value;
        var prov = document.getElementById("prov").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/agrega_compra_etiqueta') ?>",
            type:"post",
            data: { 'compra': compra, 'prov': prov },
            success: function(data){
            },
            error: function(msg, status,err){
             //alert('No pasa 194');
            }
        });
    }

    //funcion para editar la cantidad de la etiqueta
    function actualiza_modaletiq(position){
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/actualiza_modaletiq') ?>",
            type:"post",
            data: { 'position': position },
            success: function(data){
                $( "#modal_update_etiq" ).html( data );
            },
            error: function(msg, status,err){
             alert('No pasa 96');
            }
        });
    }

    //ejecuto la accion de imprimir las etiquetas seleccionadas
    function obtener_etiquetas_marcadas(){
    	var checkboxValues = "";//
		$('input[name="checkboxRow[]"]:checked').each(function() {//
					checkboxValues += $(this).val() + ",";//
					//checkboxValues.push($(this).val());//
				});//
		//eliminamos la última coma.
		checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//
		//alert(checkboxValues);
		if (checkboxValues) {
			var $btn = $("#imprime_e_Button").button('loading');//obtengo el valor del voton para ponerlo a esperar
            // simulating a timeout
            setTimeout(function () {//reseteo el boton poniendolo a esperar 20 segundos, promedio mientras se imprime
                $btn.button('reset');
            }, 20000);
			$.ajax({
	                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/obtener_etiquetas_marcadas') ?>",
	                type:"post",
	                data: { 'checkboxValues' : checkboxValues },
	                success: function(data){
	                    /*$("#not_cr_pen").load(location.href+' #not_cr_pen','');
	                    $('#concepto').val('');
	                    $('#monto_nc').val('');*/
	                    //alert(data);
	                    jsWebClientPrint.print();
                        $( "#etiquetas_marcadas" ).html( data );
	                },
	                error: function(msg, status,err){
	                 alert('No pasa 226');
	                }
	            });
		} else alert('No hay etiquetas marcadas');
    }

    //ejecuto la accion de imprimir las etiquetas seleccionadas
    function eliminar_etiquetas_marcadas(){
        confirmar=confirm("¿Seguro que deseas eliminar las etiquetas marcadas?");
        if (confirmar){
        var checkboxValues = "";//
        $('input[name="checkboxRow[]"]:checked').each(function() {//
                    checkboxValues += $(this).val() + ",";//
                    //checkboxValues.push($(this).val());//
                });//
        //eliminamos la última coma.
        checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//
        //alert(checkboxValues);
        if (checkboxValues) {
            var $btn = $("#elimina_e_Button").button('loading');//obtengo el valor del voton para ponerlo a esperar
            // simulating a timeout
            setTimeout(function () {//reseteo el boton poniendolo a esperar 20 segundos, promedio mientras se imprime
                $btn.button('reset');
            }, 2000);
            $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/eliminar_etiquetas_marcadas') ?>",
                    type:"post",
                    data: { 'checkboxValues' : checkboxValues },
                    success: function(data){
                        $("#lista_etiquetas").load(location.href+' #lista_etiquetas','');
                    }
                });
        } else alert('No hay etiquetas marcadas');
    } else {}
    }

    $(document).on('click', '#activity-index-link-agrega_compra_etiqueta', (function() {
      $.fn.modal.Constructor.prototype.enforceFocus = function() {};
      $('#modal_agrega_compra_etiqueta').modal('show');//muestro la modal
      /*  $.get(
            $(this).data('url'),
            function (data) {
                $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                $('#well-create-movimiento').html(data);//muestro en el div los datos del create
                $('#crearmovimientomodal').modal('show');//muestro la modal

            }
        );*/
    }));

    //selecciono todas las etiquetas a imprimir checandolas
    $(document).ready(function () {
    	//me checa todas las facturas
	    $('#checkAll').on('click', function() {
	        if (this.checked == true)
	            $('#tabla_etiquetas').find('input[name="checkboxRow[]"]').prop('checked', true);
	        else
	            $('#tabla_etiquetas').find('input[name="checkboxRow[]"]').prop('checked', false);
	    });
    });
</script>
<?php //otrs script
?>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="compras-inventario-etiquetas">
    <?= AlertBlock::widget([
	    'type' => AlertBlock::TYPE_GROWL,
	    'useSessionFlash' => true
    ]);?>

    <h1><?= Html::encode($this->title) ?></h1>
    <p style="text-align:right">
        <?= Html::a('<i class="fa fa-arrow-left"></i> Regresar a compras', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Ingresar nueva Etiqueta', ['#'], [
                                'class' => 'btn btn-success',
                                'id' => 'activity-index-link-agregaprod',
                                'data-toggle' => 'modal',
                                //'onclick'=>'javascript:agrega_modalprod()',
                                'data-target' => '#modal_agrega_etiqueta',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Agrega una nueva etiqueta a la orden')]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-shopping-cart"></i> Ingresar etiquetas de una compra', '#', [
                                'class' => 'btn btn-warning',
                                'id' => 'activity-index-link-agrega_compra_etiqueta',
                                'data-toggle' => 'modal',
                                //'onclick'=>'javascript:agrega_modalprod()',
                                //'data-target' => '#modal_agrega_compra_etiqueta',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Agrega las etiquetas de una compra')]) ?>
        <?php  if ($etiquetas) : ?>

        <?= Html::a('<span class="glyphicon glyphicon-tags"></span> Imprimir etiquetas marcadas', '#'/*['etiquetas_marcadas']*/, ['class' => 'btn btn-info', 'id' => 'imprime_e_Button', 'data-loading-text'=>'Espere, imprimiendo...', 'onclick'=>'obtener_etiquetas_marcadas()'/*,'target'=>'_blank'*/]) ?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> Desechar etiquetas marcadas', '#'/*['etiquetas_marcadas']*/, ['class' => 'btn btn-danger', 'id' => 'elimina_e_Button', 'data-loading-text'=>'Espere, eliminando...', 'onclick'=>'eliminar_etiquetas_marcadas()'/*,'target'=>'_blank'*/]) ?>
        <?php endif;  ?>
    </p>
    <div  id="formid" class="col-lg-12">
        <legend>Proceda a agregar las etiquetas a imprimir</legend>
    </div>
    <div  id="lista_etiquetas">
    <?php
        echo '<table class="items table table-striped" id="tabla_etiquetas">';
        echo '<thead>';
        printf('<tr><th class="actions button-column"><input type="checkbox" id="checkAll" />&nbsp;</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th class="actions button-column">&nbsp;</th></tr>',
                'CÓDIGO',
                'PRODUCTO',
                'FECHA',
                'CANTIDAD',
                'PREC. VENTA',
                'UBICACIÓN'
                );
        echo '</thead>';
        if($etiquetas) {
        	echo '<tbody>';
        	foreach($etiquetas as $position => $etiqueta) {
        		$codigo = $etiqueta['codigo'];
        		$checkbox = '<input id="striped" type="checkbox" name="checkboxRow[]" value="'.$codigo.'">';
        		$eliminar = Html::a('', ['/compras-inventario/deleteetiqueta', 'id' => $etiqueta['codigo']], ['class'=>'glyphicon glyphicon-remove', 'title' => Yii::t('app', 'Quitar esta etiqueta'), 'data' => [
                                    'confirm' => '¿Está seguro de quitar esta etiqueta de la orden?'] ]);
                $actualizar = Html::a('', ['', 'id' => $etiqueta['codigo']], [
                                'class'=>'glyphicon glyphicon-pencil',
                                'id' => 'activity-index-link-agregaprod',
                                'data-toggle' => 'modal',
                                'onclick'=>'javascript:actualiza_modaletiq("'.$etiqueta['codigo'].'")',
                                'data-target' => '#modalActualizaetiq',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Actualizar etiqueta') ]);
                $accion = $actualizar.' '.$eliminar;
        		printf('<tr><td class="actions button-column">%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td class="actions button-column">%s</td></tr>',
                            $checkbox,
                            $etiqueta['codigo'],
                            $etiqueta['nombre'],
                            $etiqueta['fecha_reg'],
                            $etiqueta['cantidad'],
                            number_format($etiqueta['precio'],2),
                            $etiqueta['ubi'].' '.$etiqueta['estado'],
                            $accion
                            );
        	}
        	echo '</tbody>';
        }
        echo '</table>';

    ?>
    </div>
</div>



<?php //-----------------------------------modal para agregar etiqueta
         Modal::begin([
                'id' => 'modal_agrega_etiqueta',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Agrega nueva Etiqueta</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-search"></i> Buscar producto', ['#'], [
                                'class' => 'btn btn-success',
                                'id' => 'activity-index-link-inventario',
                                'data-toggle' => 'modal',
                                //'onclick'=>'javascript:agrega_modalprod()',
                                'data-target' => '#modalinventario',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Inventario')]),
            ]);
         echo '<div class="well">
         		<div id="datos_de_inventario"><center>BUSQUE UN PRODUCTO Y AGREGUELO COMO ETIQUETA</center></div><br><br>
         	  </div>';
         Modal::end();
?>
<?php //----------------------------------------------modal para agregar compra a etiquetas
         Modal::begin([
                'id' => 'modal_agrega_compra_etiqueta',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Agrega etiquetas de una compra</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" onclick="run_agrega_fac_etiq()">Agregar</a> <a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
            ]);
        echo '<div class="well">
                <div class="col-lg-6">
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon3">Proveedor</span>
                    '.Select2::widget([
                                'name' => 'prov',
                                'id' => 'prov',
                                'data' => ArrayHelper::map(Proveedores::find()->all(), 'codProveedores',
                                    function($element) {
                                        return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                                    }),
                                'language'=>'es',
                                'disabled' => false,
                                'options' => ['placeholder' => '- Seleccione -'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]).'
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon3">No. Factura</span>
                    <input type="text" class="form-control" id="fac_etiq" value="" placeholder="Ingrese el No de la factura">
                  </div>
                </div><br><br>
              </div>';
         Modal::end();
?>
<?php //------------------------------modal para actualizar etiquetas
        Modal::begin([
                'id' => 'modalActualizaetiq',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Actualizar cantidad de etiqueta</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', '', [
                    'class'=>'btn btn-info',
                    //'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Agregar este producto a la orden de compra'
                ])*/,
            ]);
        echo '<div class="well"><div id="modal_update_etiq"></div><br><br></div>';

        Modal::end();
    ?>
<?php
//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a> '
        ]);
        echo '<div class="well">';
        //echo '<input type="text" name="busqueda" id="busqueda" value="" placeholder="" maxlength="30" autocomplete="off" onKeyUp="buscar();" />';
        echo '  <div class="col-lg-2">
                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
                </div>
                <div class="col-lg-2">
                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
                </div>
                <div class="col-lg-2">
                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
                </div>
                <div class="col-lg-2">
                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
                </div>
                <div class="col-lg-2">
                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
                </div>
                <div class="col-lg-2">
                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
                </div>';
        echo '
                <br>
                    <table class="table table-hover" id="tabla_lista_productos">
                        <thead>
                            <tr>
                              <th width="150">CÓDIGO LOCA</th>
                              <th width="380">DESCRIPCIÓN</th>
                              <th width="100">CANT.INV</th>
                              <th width="130">COD.ALTER</th>
                              <th width="120">CÓD.PROV</th>
                              <th width="50">UBICACIÓN</th>
                              <th width="140">PRECIO + IV</th>
                              <th class="actions button-column" width="60">&nbsp;</th>
                            </tr>
                        </thead>



                    </table><div id="resultadoBusqueda" style="height: 400px;width: 100%; overflow-y: auto; ">
                </div>
             </div>';
        echo '';


        Modal::end();
