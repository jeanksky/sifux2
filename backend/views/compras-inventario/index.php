<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Proveedores;//para obtener proveedores
use backend\models\RecepcionHacienda;//traer los estado hacienda

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ComprasInventarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Compras Inventarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compras-inventario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <input type="hidden" id="estado_h" value="NULLO">
    <p style="text-align:right">
        <?= Html::a('Módulo de etiqueta', ['etiquetas'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Ingresar nueva compra', ['create', 'carga'=>false], ['class' => 'btn btn-success']) ?>
    </p>
    <p align="center"><strong>Nota:</strong> Asegúrese que antes de hacer una compra previamente se halla ingresado el proveedor.</p>
     <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <!--?php  $dataProvider->pagination->pageSize = 15; ?-->
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idCompra',
            [
              'attribute' => 'n_interno_prov',
              'label' => 'N° Interno',
            ],
            [
              'attribute' => 'numFactura',
              'label' => 'N° Factura',
            ],
            'fechaRegistro',
            'fechaVencimiento',
            //'idProveedor',
            [
                        'attribute' => 'tbl_proveedores.nombreEmpresa',
                        'label' => 'Proveedores',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            if($proveedor = Proveedores::findOne($model->idProveedor))
                            {
                                return Html::a($proveedor->nombreEmpresa, '../web/index.php?r=proveedores%2Fview&id='.$model->idProveedor);
                            }else{
                                if ($model->idProveedor=="")
                                return 'Proveedor no asignado';
                                else
                                return $model->idProveedor;
                            }
                        },
            ],
            'formaPago',
                 [
                'attribute' => 'total',//'format' => ['decimal',2],
                'value' => function ($model, $key, $index, $grid) {return number_format($model->total,2);},
                'options'=>['style'=>'width:15%'],
             ],
            //'estado',
              'estado_pago',
               [
                        'attribute' => 'estado',
                        'label' => 'Compra - Usuario & (Hacienda)',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            if($estadoHacienda = RecepcionHacienda::find()->where(['idCompra'=>$model->idCompra])->andWhere('tipo_documento = "Factura Electrónica"',[])->one())
                            {
                              if ($estadoHacienda->respuesta_hacienda == '01') {
                                $respuesta_hacienda = 'Aceptada por Hacienda';
                              } else if ($estadoHacienda->respuesta_hacienda == '03'){
                                $respuesta_hacienda = 'Rechazada por Hacienda';
                              } else {
                                $respuesta_hacienda = 'Esperando respuesta';
                              }
                                if ($estadoHacienda->respuesta_hacienda == '01') {
                                    return $model->estado.' - '.$estadoHacienda->resolucion.' & (<strong><font color="#26DD54">'.$respuesta_hacienda.'</font></strong>)';
                                }
                                 else if ($estadoHacienda->respuesta_hacienda == '03') {
                                    return $model->estado.' - '.$estadoHacienda->resolucion.' & (<strong><font color="red">'.$respuesta_hacienda.'</font></strong>)';
                                }
                                 else {
                                    return $model->estado.' - '.$estadoHacienda->resolucion.' & (<strong><font color="#FFBF00">'.$respuesta_hacienda.'</font></strong>)';
                                }
                            }//fin del if
                            else{
                                return $model->estado.' & ()';
                            }
                        },
            ],
             //'subTotal',
             //'descuento',
             //'impuesto',
             //'total',
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'buttons' => [
                    'update' => function ($url, $model, $key) {
                            return Html::a('<span class=""> </span>', ['/compras-inventario/update', 'id' => $model->idCompra, 'carga'=>false], [
                                'class'=>'fa fa-eye',
                                //'id' => 'activity-index-link-update',
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Procede a ver la Compra'),
                                ]);
                        },
                ],
            'options'=>['style'=>'width:30px'],],
        ],
    ]); ?>

</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //re_enviar_xml_not();
  //  verificarEstadoHAciendoGometa();
  //  setInterval('verificarEstadoHAciendoGometa()',60000); /*1 min*/
  //  setTimeout("re_enviar_xml_not()", 8000 );
  //  setInterval('re_enviar_xml_not()',120000); /*120 segundos*/
  });

  /*vehiel 05-12-2018
  metodo que verificacion del estado del tarro de haciendo con el api de gometa*/
  function verificarEstadoHAciendoGometa(){
    gometa = 'http://apis.gometa.org/status/status.json';
    proxy = 'https://cors-anywhere.herokuapp.com/';
    url = proxy+gometa;
    $.ajax({
      'url': url,
      'dataType':'json',
      'crossDomain':false,
      'type':'get',
      beforeSend: function(xhr){
        xhr.setRequestHeader("Access-Control-Allow-Origin","*");
      },
      success: function(data){
        document.getElementById("estado_h").value = data['api-prod']['status'];
      },
      error:function(){
      }
    });
  }

  //confirma el estado de resolucion en hacienda en segundo plano, o reenvia la resolucion si está estancada
  function re_enviar_xml_not() {
    var estado_hacienda =  document.getElementById("estado_h").value;
    if (estado_hacienda == 'OK') {
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('recepcion-hacienda-general/renviar_xml_not') ?>",
          type:"post",
          success: function(data){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/renviar_xml_not') ?>",
                type:"post",
                success: function(data){
                  $.ajax({
                      url:"<?php echo Yii::$app->getUrlManager()->createUrl('gastos/renviar_xml_not') ?>",
                      type:"post",
                      success: function(data){ },
                  });//fin ajax gastos
                },
            });//fin ajax compras-inventario
          },
      });//fin ajax recepcion-hacienda-general
    }
  }
</script>
