<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TipoMovimientos */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-lg-6">
<div class="panel panel-default">
        <div class="panel-heading">
            Ingrese los datos que se le solicitan en el siguiente formulario.
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
<div class="tipo-movimientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'codigoTipo')->textInput(['maxlength' => true]) ?-->

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'acciones')->dropDownList(['S '=>'S ', 'R'=>'R'])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
</div></div>
