<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Marcas;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Modelo */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="col-lg-6">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ingrese los datos que se le solicitan en el siguiente formulario.
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">

			<div class="modelo-form">

				<?php $form = ActiveForm::begin(); ?>


				<?= $form->field($model, 'codMarca')->widget(Select2::classname(), [
				    'data' => ArrayHelper::map(Marcas::find()->all(),'codMarca', function($element){
							return $element->nombre_marca;
						}),
				    'language'=>'es',
				    'options' => ['placeholder' => ''],
				    'pluginOptions' => [
				        'allowClear' => true,
				    ],
				]);
				    ?>

				<?= $form->field($model, 'nombre_modelo')->textInput(['maxlength' => 20]) ?>

				<?= $form->field($model, 'slug')->textInput(['maxlength' => 20]) ?>

				<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>

				<?php ActiveForm::end(); ?>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
