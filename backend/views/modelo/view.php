<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\models\Marcas;

/* @var $this yii\web\View */
/* @var $model backend\models\Modelo */
$marca = Marcas::findOne($model->codMarca);
$this->title = $marca->nombre_marca . ' - ' . $model->nombre_modelo;
$this->params['breadcrumbs'][] = ['label' => 'Modelos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-lg-6">
<div class="modelo-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codModelo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codModelo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres borrar este modelo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div id="semitransparente">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            'nombre_modelo',
            'slug'
        ],
    ]) ?>
</div>
</div>
</div>
