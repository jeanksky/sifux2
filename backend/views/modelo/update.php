<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
/* @var $this yii\web\View */
/* @var $model backend\models\Modelo */

$this->title = 'Actualizar Modelo: ' . ' ' . $model->nombre_modelo;
$this->params['breadcrumbs'][] = ['label' => 'Modelos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre_modelo, 'url' => ['view', 'id' => $model->codModelo]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="modelo-update">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
