<div class="col-md-6">
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Marcas;
use kartik\widgets\AlertBlock; //para mostrar la alerta

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ModeloSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modelos';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<div class="modelo-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear modelo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 <div id="semitransparente">
 <?php  $dataProvider->pagination->pageSize = 10; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre_modelo',
            //'slug',
            [
                        'attribute' => 'tbl_marcas.nombre_marca',
                        'label' => 'Marca',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $marca = Marcas::findOne($model->codMarca);
                            if (@$marca)
                              return $marca->nombre_marca;
                            else {
                              return 'Sin marca';
                            }


                        },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div></div>
