<?php
use backend\models\DetalleOrdenCompraTemporal;
use backend\models\ProductoProveedores;
use backend\models\ProductoServicios;
use yii\helpers\Html;
  $detalle_orden_compr_tem = DetalleOrdenCompraTemporal::find()->where(['=','idProveedores', $idProveedor])->orderBy(['idDetaOrdComTempo' => SORT_DESC])->all();

?>
<style media="screen">
  input[type=checkbox] {
     /* visibility: hidden;*/
      zoom:1.8;
  }
  .tbody_ {
  display: block; /*obligado*/
  height: 300px; /*la que necesites*/
  width : 100%;
  overflow: auto; /*obligado*/
  }
  .thead_ {
    display: block; /*obligado*/
    width : 100%;
  }

  #tam_descr { width : 50%; }
  #tam_cantp { width : 10%; }
</style>
<table class="items table table-striped" id="tabla_productos_pedidos"  >
  <thead class="thead_">
    <tr>
      <th><font face="arial" size=2>#</font></th>
      <th><font face="arial" size=2>Cód.Interno</font></th>
      <th><font face="arial" size=2>Cód.Proveedor</font></th>
      <th id="tam_descr"><font face="arial" size=2>Descripción</font></th>
      <th><font face="arial" size=4 id="tam_cantp"><span class="label label-default">Cant.Existente</span></font></th>
      <th style="text-align:right" id="tam_cantp"><font face="arial" size=4><span class="label label-warning">Cant.Mínima</span></font></th>
      <th style="text-align:right" id="tam_cantp"><font face="arial" size=4><span class="label label-info">Cant.Sugerida</span></font></th>
      <th style="text-align:right" id="tam_cantp"><font face="arial" size=4><span class="label label-success">Cantidad a pedir</span></font></th>
      <th class="actions button-column"></th>
    </tr>
  </thead>
  <tbody class="tbody_">
    <?php foreach($detalle_orden_compr_tem as $position => $product) {
      $border = '';
      $prod_prov = ProductoProveedores::find()->where(['=','codProveedores', $product['idProveedores']])->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$product['codProdServicio']])->one();
      $producto = ProductoServicios::find()->where(['=','codProdServicio', $prod_prov->codProdServicio])->one();
      $cantidad_sugerida = $producto->cantidadInventario;
       $checkbox = '<input id="striped" type="checkbox" name="checkboxRowinv[]" value="'.$producto->codProdServicio.'">';
       //muestra en colores los productos por la diferencia de la cantidad en einventario con el minimo
       if($producto->cantidadInventario < $producto->cantidadMinima){
           $border = 'red 10px solid;';
           }
       if($producto->cantidadInventario == $producto->cantidadMinima){
           $border = 'orange 10px solid;';
       }
       $cantidad_cercana = $producto->cantidadMinima + 3;
       if($producto->cantidadInventario > $producto->cantidadMinima && $producto->cantidadInventario <= $cantidad_cercana){
           $border = 'blue 10px solid;';
       }
    ?>
    <tr style = "border-left: <?= $border ?> border-right: ; cursor:pointer " onclick="marcarproductopedido(this, '<?= $product["codProdServicio"] ?>', <?= $idProveedor ?>)">
      <td align="center"><font face="arial" size=2><?= $position+1 ?></font></td>
      <td align="center" style="background: ;"><font face="arial" size=2><?= $producto->codProdServicio ?></font></td>
      <td align="center"><font face="arial" size=2><?= $prod_prov->codigo_proveedor ?></font></td>
      <td align="left" id="tam_descr"><font face="arial" size=2><?= $producto->nombreProductoServicio ?></font></td>
      <td align="center"><font face="arial" size=5><span class="label label-default"><?= $producto->cantidadInventario ?></span></font></td>
      <td align="center" id="tam_cantp"><font face="arial" size=5><span class="label label-warning"><?= $producto->cantidadMinima ?></span></font></td>
      <td align="center" id="tam_cantp"><font face="arial" size=5><span class="label label-info"><?= $product['cantidad_sugerida'] ?></span></font></td>
      <td align="center" id="tam_cantp" id="tam_cantp"><font face="arial" size=5><span class="label label-success"><?= $product['cantidad_pedir'] ?></span></font></td>
      <td class="actions button-column"><?= $checkbox ?></td>
    </tr>
  <?php } ?>
  </tbody>
</table>
