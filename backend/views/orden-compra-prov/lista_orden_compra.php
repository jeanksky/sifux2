<?php
use backend\models\OrdenCompraProv;
use backend\models\Proveedores;
use backend\models\ContactosPedidos;
use backend\models\Transporte;
$ordencompraprov = OrdenCompraProv::find()->where([ 'idProveedor'=>$idProveedor ])->orderBy(['idOrdenCompra' => SORT_DESC])->limit(20)->all();
?>
<br>
<style media="screen">
#th_ {
 width: 10%;
 text-align: center;
  font-style:italic;
  font-weight:bold;
    font-size:14pt;
  color:#3A3535;
  font-family:'Helvetica','Verdana','Monaco',sans-serif;
}
#td_ {
  width: 10%;
  font-weight:bold;
  color: #0C0482;
  font-size:12pt;
  font-family:'Helvetica','Verdana','Monaco',sans-serif;
}
/* clase para ocultar el div al inicio */
.oculto {
  display:none;
  margin-left:1em;
  margin-right:1em;
  border-left: thick solid #044e7d;/*linea izquierda en el div*/
  border-right: thick solid #044e7d;/*linea derecha en el div*/
  height: 400px; /*altura del div*/
}


</style>
<script type="text/javascript">
/*****************************Ocultar div**************************/
var visto = null;
function ver(linea,num) {
  var elementos = document.getElementById('tabla_lista_compra').
  getElementsByTagName('tbody')[0].getElementsByTagName('tr');
  // Por cada TR empezando por el segundo, ponemos fondo blanco.
  for (var i = 0; i < elementos.length; i++) {
      elementos[i].style.background='white';
  }
  // Al elemento clickeado le ponemos fondo amarillo.
  if (linea.style.background=="yellow") {
    linea.style.background="white";
  } else {
    linea.style.background="yellow";
  }

  //-----------------------------------------------------------
  obtenerordencompra(num);
  obj = document.getElementById(num);
  obj.style.display = (obj==visto) ? 'none' : 'block';
  if (visto != null)
  visto.style.display = 'none';
  visto = (obj==visto) ? null : obj;
}
//agrega en el div las facturas del tramite correspondiente
function obtenerordencompra(idOrdenCompra) {
    document.getElementById(idOrdenCompra).innerHTML="";//tengo que destruir el contenido del div de
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/obtenerordencompra') ?>" , { 'idOrdenCompra' : idOrdenCompra } ,
          function( data ) {
            $('#'+idOrdenCompra).html(data);
          });
  }

function imprimir_orden_compra(idOrdenCompra) {
  $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/imprimir_orden_compra') ?>" ,
      { 'idOrdenCompra' : idOrdenCompra } ,
      function( data ) {
        var ventimp=window.open(' ',"MsgWindow", "width=1200,height=600");//'MsgWindow'
          ventimp.document.write('<title>Orden de compra No.'+idOrdenCompra+'</title>'+data);
          ventimp.document.close();
          ventimp.print();
          ventimp.close();
      });
}
function enviar_orden_compra(idOrdenCompra) {
  var confirm_envio = confirm("¿Está seguro de re-enviar esta orden de compra?");
  if (confirm_envio == true) {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('orden-compra-prov/enviar_orden_compra') ?>",
        type:"post",
        data: { 'idOrdenCompra' : idOrdenCompra },
        beforeSend: function() {
          $('#notificaciones_envio_correo').html();
          document.getElementById("notificaciones_envio_correo").innerHTML = '<center><span class="label label-info" style="font-size:15pt;">Espere re-envi@ndo...</span></center>';
        },
        success: function(notif){
          document.getElementById("notificaciones_envio_correo").innerHTML = notif;
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#notificaciones_envio_correo").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 4 segundos
              $("#notificaciones_envio_correo").fadeOut();
          },5000);
        },
        error: function(msg, status,err){
          document.getElementById("notificaciones_envio_correo").innerHTML = '<center><span class="label label-danger" style="font-size:15pt;">Error de comunicación.</span></center>';
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#notificaciones_envio_correo").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 4 segundos
              $("#notificaciones_envio_correo").fadeOut();
          },5000);
        }
    });
  }
}
</script>
<table class = "table table-bordered" id="tabla_lista_compra">
  <thead class="thead_lista_compra">
    <tr class="success">
      <th id="th_">id</th>
      <th id="th_">N° Compra</th>
      <th id="th_">Registro</th>
      <th id="th_">Ingreso mercadería</th>
      <th id="th_">Prioridad</th>
      <th id="th_">contacto</th>
      <th id="th_">Transporte</th>
      <th id="th_">Estado</th>
    </tr>
  </thead>
  <tbody class="tbody_lista_compra">
    <?php
      if ($idCompra!=null) {
        $ordencompraprov = OrdenCompraProv::find()->where([ 'idProveedor'=>$idProveedor ])
        ->andWhere("idCompra = :idCompra", [":idCompra"=>$idCompra ])
        ->orderBy(['idOrdenCompra' => SORT_ASC])->limit(20)->all();
      } else {
        if ($busqueda_prio!='' && $busqueda_est!='') {
          $ordencompraprov = OrdenCompraProv::find()->where(['idProveedor'=> $idProveedor])
          ->andWhere("prioridad = :prioridad and estado = :estado", [":prioridad"=>$busqueda_prio,":estado"=>$busqueda_est])
          ->orderBy(['idOrdenCompra' => SORT_ASC])->limit(20)->all();
        } else if ($busqueda_prio!='') {
          $ordencompraprov = OrdenCompraProv::find()->where(['idProveedor'=> $idProveedor])
          ->andWhere("prioridad = :prioridad", [":prioridad"=>$busqueda_prio])
          ->orderBy(['idOrdenCompra' => SORT_ASC])->limit(20)->all();
        } else if ($busqueda_est!='') {
          $ordencompraprov = OrdenCompraProv::find()->where(['idProveedor'=> $idProveedor])
          ->andWhere("estado = :estado", [":estado"=>$busqueda_est])
          ->orderBy(['idOrdenCompra' => SORT_ASC])->limit(20)->all();
        } else if($fecha_reg_compra!=''){
          $ordencompraprov = OrdenCompraProv::find()->where(['idProveedor'=> $idProveedor])
          ->andWhere("fecha_registro = :fecha_registro", [":fecha_registro"=>$fecha_reg_compra])
          ->orderBy(['idOrdenCompra' => SORT_ASC])->limit(20)->all();
        }
      }
      foreach($ordencompraprov as $position => $compra) {
        $proveedor = Proveedores::find()->where(['=','codProveedores', $compra['idProveedor']])->one();
        $contacto_pedido = ContactosPedidos::findOne($compra['idContacto_pedido']);
        $Transporte = Transporte::find()->where(['=','id_transporte', $compra['id_transporte']])->one();
        $compra_id = $compra['idCompra'] == '' ? '(Sin asignar)': $compra['idCompra'];
        $fecha_ingreso_mercaderia = $compra['fecha_ingreso_mercaderia'] == '' ? '(Esperando)': date('d-m-Y', strtotime( $compra['fecha_ingreso_mercaderia'] ));
        $nombre_contacto_pedido = @$contacto_pedido ? $contacto_pedido->nombre : 'El contacto fue eliminado';
        $nombre_transporte = @$Transporte ? $Transporte->nombre_transporte : 'El nombre de transporte fue eliminado';
        echo '<tr style="cursor:pointer" onclick="ver(this,'.$compra['idOrdenCompra'].')" >
                <td align="center" id="td_"> '.$compra['idOrdenCompra'].' </td>
                <td align="center" id="td_"> '.$compra_id.' </td>
                <td align="center" id="td_"> '.date('d-m-Y', strtotime( $compra['fecha_registro'] )).' </td>
                <td align="center" id="td_"> '.$fecha_ingreso_mercaderia.' </td>
                <td align="center" id="td_"> '.$compra['prioridad'].' </td>
                <td align="center" id="td_"> '.$nombre_contacto_pedido.' </td>
                <td align="center" id="td_"> '.$nombre_transporte.' </td>
                <td align="center" id="td_"> '.$compra['estado'].' </td>
              </tr>
              <tr  >
    						<td colspan="10" >
                <div id="'.$compra['idOrdenCompra'].'" class="oculto"></div>
                </td>
    					</tr>';
      }
    ?>
  </tbody>
</table>
