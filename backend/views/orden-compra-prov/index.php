<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\OrdenCompraSession;
use kartik\widgets\AlertBlock;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\Proveedores;
use backend\models\Moneda;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrdenCompraProvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
$this->title = 'Orden de compras (Proveeduría)';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
window.onkeydown = tecla;//con este metodo al precionr f2 me hablre la ventana ára seleccionar un proveedor
	function tecla(event) {
		//event.preventDefault();
		num = event.keyCode;
		if(num==113)
			$('#modalproveedor').modal('show');//muestro la modal
	}

$(document).ready(function(){
        $('[data-toggle="popover"]').popover();
        $('#realizar_orden_compra').addClass('disabled');
    });
    //Carga un proveedor en session (plataforma)
    function agrega_proveedor(idProveedor){
        $('#panel').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('orden-compra-prov/addproveedor') ?>",
            type:"post",
            data: { 'codProveedor': idProveedor },
            success: function(data){
            },
            error: function(msg, status,err){
            }
        });

    }
    //me permite buscar por like los proveedores a BD
    function buscar() {
        var textoBusqueda = $("input#busqueda").val();
        if (textoBusqueda != "") {

            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('orden-compra-prov/busqueda_proveedor') ?>",
                type:"post",
                data: { 'valorBusqueda' : textoBusqueda },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 alert('No pasa 47');
                }
            });
        } else {
            ("#resultadoBusqueda").html('');
        }
    }

    function buscar_ordenes_compra() {
      busqueda_prio = document.getElementById('busqueda_prio').value;
      busqueda_est = document.getElementById('busqueda_est').value;
      idProveedor = document.getElementById('id_proveedor').value;
      fecha_reg_compra = document.getElementById('fecha_reg_compra').value;
      compra = document.getElementById('compra').value;
      document.getElementById("tabla_orden_compra").innerHTML="";//tengo que destruir el contenido del div
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/lista_orden_compra') ?>" ,
          { 'idProveedor' : idProveedor, 'busqueda_prio' : busqueda_prio, 'busqueda_est' : busqueda_est,
          'fecha_reg_compra' : fecha_reg_compra, 'compra' : compra } ,
          function( data ) {
            $('#tabla_orden_compra').html(data);
          });
    }
</script>

<div class="orden-compra-prov-index">

    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="col-lg-12">
        <div class="panel panel-warning">
            <div class="panel-heading">
                Seleccione el proveedor
                <?php
                 if (@$idproveedor = OrdenCompraSession::getIDproveedor()) {
                       echo '<div class="pull-right">' .Html::a('Actualizar datos de este Proveedor', ['proveedores/update', 'id'=>$idproveedor], ['class' => 'btn btn-primary btn-xs', 'target'=>'_blank']) .
                       ' ' . Html::a('Limpiar para agregar otro proveedor', ['limpiar'], ['class' => 'btn btn-warning btn-xs']) . '</div>';
                 } ?>
            </div>
            <div class="panel-body" id="datos_proveedor">
                <div class="col-lg-2">
                <label>ID Proveedor</label>
                <?php
                    if (@$idproveedor = OrdenCompraSession::getIDproveedor()) {
                        echo '<h4>'.$idproveedor.'</h4><input type="hidden" id="id_proveedor" value="'.$idproveedor.'">';

                    } else
                         echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'class' => 'form-control', 'placeholder' =>'ID Proveedor', 'onchange' => 'javascript:agrega_proveedor(this.value)']);
                ?>
                </div>
                <div class="col-lg-4">
                <label>Proveedor</label>
                <div class="input-group">
                <span class="input-group-btn">
                    <button class="btn btn-default" data-toggle="modal" data-placement="top" title="Buscar proveedor para pago de facturas" type="button" data-target="#modalproveedor"><span class="glyphicon glyphicon-search"></span></button>
                </span>
                <?php
                if (@$proveedor = OrdenCompraSession::getProveedor()) {
                    echo Html::input('text', '', $proveedor, ['class' => 'form-control', 'placeholder' => $proveedor, 'readonly' => true, 'onchange' => '']);
                } else
                    echo Html::input('text', '', '', ['class' => 'form-control', 'placeholder' =>'Nombre del proveedor', 'readonly' => true, 'onchange' => '']);
                ?>
                </div>
                </div>
                <div class="col-lg-6">
                <?php
                if (@$idproveedor = OrdenCompraSession::getIDproveedor()) {//Muestro el saldo que se debe al proveedor
                  $proveedor_ = Proveedores::find()->where(['=','codProveedores', $idproveedor])->one();
                  $mes_actual = "'".date("m-Y")."'";
                  $mo = Moneda::findOne($proveedor_->idMoneda);
                  $total_compras_proveedor = Yii::$app->db->createCommand("SELECT SUM(total) FROM tbl_compras WHERE idProveedor = ".$idproveedor." AND DATE_FORMAT(fechaRegistro, '%m-%Y') = " . $mes_actual);
                  $total_compras_global = Yii::$app->db->createCommand("SELECT SUM(total) FROM tbl_compras WHERE DATE_FORMAT(fechaRegistro, '%m-%Y') = " . $mes_actual);
                    echo '<span style="float:left">
                            <label>Total compras del proveedor en el mes:</label>
                            <div style="background-color:#FFC; padding:12px">';
                                echo '<div id="saldo_proveedor">'.$mo->simbolo." ".number_format(floatval($total_compras_proveedor->queryScalar()),2).'</div>';
                    echo   '</div>
                          </span>';
                    echo '<span style="float:right">
                            <label>Total compras global del mes:</label>
                            <div style="background-color:#FFC; padding:12px">';
                                echo '<div id="saldo_proveedor">'.$mo->simbolo." ".number_format(floatval($total_compras_global->queryScalar()),2).'</div>';
                    echo   '</div>
                          </span>';
                      }
                ?>
                </div>
            </div>
        </div>
        <?= AlertBlock::widget([
                'type' => AlertBlock::TYPE_ALERT,
                'useSessionFlash' => true,
                'delay' => 20000
            ]);?>
    </div>
    <ul class="nav nav-tabs nav-justified">
        <li class="active" role="presentation" ><a href="#ordencompra" data-toggle="tab" id="ordencompra_tab">Orden de compra</a></li>
        <li role="presentation"><a href="#listaordencompra" data-toggle="tab" id="listaordencompra_tab">Lista de Ordenes de compras</a></li>
    </ul>
    <?php  //llamo la vista con las facturas pendientes de cancelar del proveedor seleccionado
        if (@$idproveedor = OrdenCompraSession::getIDproveedor()) {
    ?>
      <div class="table-responsive tab-content">
        <div class="tab-pane fade in active" id="ordencompra">
          <center><h2>Generar Orden de Compra / Estadisticas del producto</h2></center>
          <?= $this->render('orden_compra', [ 'idProveedor' => $idproveedor ]); ?>
        </div>
        <div class="tab-pane fade" id="listaordencompra">
          <center><h2>Lista de las Ordenes de Compras</h2></center>
          <div class="col-lg-12 alert alert-info" role="alert">
                  <div class="col-lg-3">
                    <?= Html::input('text', '', '', ['size' => '30', 'id' => 'compra',
                    'class' => 'form-control', 'placeholder' =>'Ingrese No Compra y ENTER',
                    'onchange' => 'javascript:buscar_ordenes_compra()']) ?>
                  </div>
                  <div class="col-lg-3">
                    <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                    'options' => ['class'=>'form-control', 'placeholder'=>'FECHA REGISTRO dd-mm-yyyy', 'onchange'=>'javascript:buscar_ordenes_compra()',
                    'id'=>'fecha_reg_compra']]) ?>
                  </div>
                  <div class="col-lg-3">
                    <?= Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                    'name' => '',
                    'data' => ["Baja" => "Baja", "Media" => "Media", "Alta" => "Alta"],
                    'options' => [
                        'placeholder' => 'PRIORIDAD',
                        'id'=>'busqueda_prio',
                        'onchange' => 'javascript:buscar_ordenes_compra()',
                        'multiple' => false //esto me ayuda a que solo se obtenga uno
                    ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
                    ]) ?>
                  </div>

                  <div class="col-lg-3">
                   <?= Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                        'name' => '',
                        'data' => ["Generada" => "Generada", "Completa" => "Completa"],
                        'options' => [
                            'placeholder' => 'ESTADO',
                            'id'=>'busqueda_est',
                            'onchange' => 'javascript:buscar_ordenes_compra()',
                            'multiple' => false //esto me ayuda a que solo se obtenga uno
                        ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
                    ]) ?>
                  </div>
              </div>
          <div id="tabla_orden_compra">
            <?= $this->render('lista_orden_compra', [ 'idProveedor' => $idproveedor,
            'busqueda_prio' => '',
            'busqueda_est' => '',
            'fecha_reg_compra' => '',
            'idCompra' => null ]) ?>
          </div>
        </div>
      </div>
    <?php
        } else {
    ?>
    <div class="table-responsive tab-content">
      <div class="tab-pane fade in active">
      <br>
        <div class="col-lg-12">
          <div class="panel panel-info" id="panel">
            <center><h3>Esperando un proveedor<h3></center>
          </div>
        </div>
      </div>
    </div>
    <?php
            }
    ?>



    <!-- Modal para proveedores -->
    <div class="modal fade" id="modalproveedor" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Lista de Proveedores</h4>
        </div>
        <div class="modal-body">
            <?php
                echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '80', 'id' => 'busqueda', 'class' => 'form-control', 'placeholder' =>'Busque el ID o nombre del proveedor que desea agregar', 'onKeyUp' => 'javascript:buscar()']);
            ?>
            <br>
            <div id="resultadoBusqueda" style="height: 200px;width: 100%; overflow-y: auto; "></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>
      </div>
    </div>
    </div>
</div>
