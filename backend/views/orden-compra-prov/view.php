<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenCompraProv */

$this->title = $model->idOrdenCompra;
$this->params['breadcrumbs'][] = ['label' => 'Orden Compra Provs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-compra-prov-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idOrdenCompra], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idOrdenCompra], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idOrdenCompra',
            'idCompra',
            'idProveedor',
            'fecha_registro',
            'fecha_ingreso_mercaderia',
            'prioridad',
            'idContacto_pedido',
            'id_transporte',
            'observaciones',
            'estado',
        ],
    ]) ?>

</div>
