<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\OrdenCompraProvSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-compra-prov-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idOrdenCompra') ?>

    <?= $form->field($model, 'idCompra') ?>

    <?= $form->field($model, 'idProveedor') ?>

    <?= $form->field($model, 'fecha_registro') ?>

    <?= $form->field($model, 'fecha_ingreso_mercaderia') ?>

    <?php // echo $form->field($model, 'prioridad') ?>

    <?php // echo $form->field($model, 'idContacto_pedido') ?>

    <?php // echo $form->field($model, 'id_transporte') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
