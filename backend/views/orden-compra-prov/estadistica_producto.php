<?php
use backend\models\ProductoProveedores;
use backend\models\ProductoServicios;
use backend\models\ComprasInventario;
use backend\models\DetalleCompra;
use backend\models\Proveedores;
use backend\models\ProductoPedido;
use backend\models\OrdenCompraProv;
use backend\models\DetalleOrdenCompra;
use backend\models\OrdenCompraSession;
use yii\helpers\Html;
$prod_prov = ProductoProveedores::find()->where(['=','codProveedores', $idProveedor])->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$idProducto])->one();
$producto = ProductoServicios::find()->where(['=','codProdServicio', $prod_prov->codProdServicio])->one();
 ?>
<script type="text/javascript">
$(document).ready(function () {
        //me permite usar el rengo de fechas para obtener la cantidad sujerida
        $('#dias_rendimiento_rango_fechas').on('click', function() {
          SetScrollValue();
          var idProveedor = '<?= $idProveedor ?>';
          var categoria_excluir = $("#categoria").val();
          var lista_categoria = '0';
          if (categoria_excluir==null) {
            lista_categoria = '0';
          } else {
            lista_categoria = categoria_excluir.toString();
          }
          var filtro_minimo = document.getElementById('filtro_minim').value;
          var prov_fech_desde = document.getElementById('prov_fech_desde').value;
          var prov_fech_hasta = document.getElementById('prov_fech_hasta').value;
          check = '';
            if (this.checked == true){//Compruebo check---------------------------------------------------
              check = 'si';
              $("#dias_rendimiento").prop('disabled', true);//desabilito el campo de dias rendimiento
              document.getElementById('dias_rendimiento').value = '';//y le pongo valor nullo
              $("#categoria").prop('disabled', true);
                $('#btn_cm').addClass('disabled');
                $('#btn_im').addClass('disabled');
                $('#btn_mm').addClass('disabled');
                $('#btn_t').addClass('disabled');
            } else {//---------------------------------------------------
              check = 'no';
              $("#dias_rendimiento").prop('disabled', false);//habilito el campo de dias rendimiento
              $("#categoria").prop('disabled', false);//habilito el campo de EXCLUSION DE CATEGORIAS
              $('#btn_cm').removeClass('disabled');
              $('#btn_im').removeClass('disabled');
              $('#btn_mm').removeClass('disabled');
              $('#btn_t').removeClass('disabled');
            }//---------------------------------------------------
            $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_disponibles_rango_fechas') ?>" ,
            { 'idProveedor' : idProveedor, 'filtro_minimo' : filtro_minimo,
            'filtro_excluir' : lista_categoria, 'prov_fech_desde' : prov_fech_desde,
            'prov_fech_hasta' : prov_fech_hasta, 'check' : check } ,
            function( data ) {
              $('#orden_compra_productos_disponibles').html(data);
              GetScrollBarValue();
            });
        });
});
function refrescarDIV_estadistica_producto(){
    //$("#comprados_vendidos").load(location.href+' #comprados_vendidos','');
    var codProdServicio = '<?= $prod_prov->codProdServicio ?>';
    buscar_estadistica_producto(codProdServicio);

}
setInterval('refrescarDIV_estadistica_producto()',2000);
</script>
<div style="height: 100%;width: 100%; overflow-y: auto; ">
  <center><h3><a href="javascript:opcion_acordeon();">Estadisticas del producto <?= $idProducto ?></a><br><small>(<?= $producto->nombreProductoServicio ?>)</small></h3></center>
  <div id="opcion_acordeon_producto">
   <div class="panel panel-info">
     <?php
        $codProdServicio = $prod_prov->codProdServicio;
        $sql_comprados = "SELECT SUM(cd.`cantidadEntrada`) AS cantidad_compra FROM tbl_compras c INNER JOIN tbl_detalle_compra cd
        ON cd.`idCompra` = c.`idCompra`
        WHERE cd.`codProdServicio` = :codProdServicio
        AND cd.`estado` = 'Aplicado'
        AND c.`fechaRegistro` BETWEEN :fecha_desde AND :fecha_hasta";
            $command = \Yii::$app->db->createCommand($sql_comprados);
            $command->bindParam(":codProdServicio", $codProdServicio);
            $command->bindParam(":fecha_desde", $fedes);
            $command->bindParam(":fecha_hasta", $fehas);
            $result_comprados = $command->queryOne();
        $sql_vendidos = "SELECT SUM(vd.`cantidad`) AS cantidad_venta FROM tbl_encabezado_factura v INNER JOIN tbl_detalle_facturas vd
        ON vd.`idCabeza_factura` = v.`idCabeza_Factura`
        WHERE vd.`codProdServicio`= :codProdServicio
        AND v.`estadoFactura` IN ('Cancelada','Crédito')
        AND v.`fecha_inicio` BETWEEN :fecha_desde AND :fecha_hasta";
            $command2 = \Yii::$app->db->createCommand($sql_vendidos);
            $command2->bindParam(":codProdServicio", $codProdServicio);
            $command2->bindParam(":fecha_desde", $fedes);
            $command2->bindParam(":fecha_hasta", $fehas);
            $result_vendidos = $command2->queryOne();

        //$facturasCanceladas = MovimientoCredito::find()->where(['=','idCliente', $idCliente])->andWhere("estadoFactura = :estadoFactura", [':estadoFactura'=>'Cancelada'])->andWhere("fecha_inicio BETWEEN :fecha_uno AND :fecha_dos",[':fecha_uno'=>$fedes, ':fecha_dos'=>$feast])->orderBy(['idCabeza_Factura' => SORT_DESC])->all();
        $comprados = $result_comprados['cantidad_compra'] == null ? 0 : $result_comprados['cantidad_compra'];
        $vendidos = $result_vendidos['cantidad_venta'] == null ? 0 : $result_vendidos['cantidad_venta'];
        $checked = OrdenCompraSession::getFechasrendimiento() == 'si' ? 'checked' : '';

        $agotamiento = 0;
        $abastecimiento = 0;
        $rendimiento = 0;
        if ($comprados!=0) {
          $agotamiento = number_format(($vendidos*100)/$comprados,1);
          $abastecimiento = number_format(100 - $agotamiento,1);
          $rendimiento = number_format($vendidos/$comprados*100,1);
        }
      ?>
     <div class="panel-heading" id="movimiento_producto_head">
       Movimientos desde
       <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
       'options' => [ 'placeholder'=>'Fecha desde', 'onchange'=>'javascript:buscar_estadistica_producto("'.$codProdServicio.'")', 'size' => '10',
       'id'=>'prov_fech_desde', 'style'=>"font-family: Fantasy; font-size: 13pt; text-align: center;"]]) ?> hasta
       <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
       'options' => [ 'placeholder'=>'Fecha hasta', 'onchange'=>'javascript:buscar_estadistica_producto("'.$codProdServicio.'")', 'size' => '10',
       'id'=>'prov_fech_hasta', 'style'=>"font-family: Fantasy; font-size: 13pt; text-align: center;"]]) ?>.
       <span style="float:right">
         <input id="dias_rendimiento_rango_fechas" type="checkbox" name="dias_rendimiento_fecha" value="" title="Al seleccionar esta opción la cantidad sugerida a comprar de los productos dependerán solo del rango de rendimiento entre las fechas establecidas aquí. Si desea excluir categorias y filtrar la cantidad del producto hazlo antes de seleccionar la casilla" <?= $checked ?> >
       </span>
     </div>
     <div class="panel-body">
       <div class="">

       </div>
        <div class="col-lg-3">
          <center>
            <label>Comprados:</label>
            <div style="background-color:#FFC; padding:12px">
              <div id="comprados" style="font-family: Fantasy; font-size: 18pt;"><?= $comprados ?></div>
            </div>
          </center>
        </div>
        <div class="col-lg-3">
          <center>
            <label>Vendidos:</label>
            <div style="background-color:#FFC; padding:12px">
              <div id="vendidos" style="font-family: Fantasy; font-size: 18pt;"><?= $vendidos ?></div>
            </div>
          </center>
        </div>
        <!--div class="col-lg-6">
          <label>Consumo y Rendimiento</label>
          <div id="rendimiento_div">
            <div class="progress progress-striped active" title="Verde: Abastecimiento (<?= $abastecimiento ?>%) / Rojo: Agotamiento (<?= $agotamiento ?>%)">
              <div class="progress-bar progress-bar-success" style="width: <?= $abastecimiento ?>%"><font size=3><?= $abastecimiento ?>%</font></div>
              <div class="progress-bar progress-bar-danger" style="width: <?= $agotamiento ?>%"><font size=3><?= $agotamiento ?>%</font></div>
            </div>
            <div class="progress progress-striped active" title="El porcentaje de rendimiento en consumo es de <?= $rendimiento ?>%">
              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= $rendimiento ?>%" >
                <font size=4><?= $rendimiento ?>%</font>
              </div>
            </div>
          </div>
        </div-->

     </div>
   </div>
   <div class="panel panel-info">
     <div class="panel-heading">
       Proveedores a los que se le han comprado en este rango de fechas.
     </div>
     <div class="panel-body" style="height: 250px;width: 100%; overflow-y: auto; ">
       <div id="tabla_proveedores_que_se_le_compra">
         <?php
         //la fecha desde y hasta ya vienen asignadas a 90 dias si no se ha digitado de lo contrario el rango que el usuario ya halla establecido
         $sql_prov_com = "SELECT MAX(cd.`idDetalleCompra`) AS detalle_max, MAX(c.`fechaRegistro`) AS fecha_max, c.`idProveedor` FROM tbl_compras c
                          INNER JOIN tbl_detalle_compra cd
                          ON c.`idCompra` = cd.`idCompra`
                          WHERE cd.`codProdServicio` = :codProdServicio
                          AND c.`fechaRegistro` BETWEEN :fecha_desde AND :fecha_hasta
                          GROUP BY c.`idProveedor` ORDER BY detalle_max DESC";
             $command_prov_com = \Yii::$app->db->createCommand($sql_prov_com);
             $command_prov_com->bindParam(":codProdServicio", $codProdServicio);
             $command_prov_com->bindParam(":fecha_desde", $fedes);
             $command_prov_com->bindParam(":fecha_hasta", $fehas);
             $result_prov_com = $command_prov_com->queryAll();
          ?>
         <table class="items table table-striped">
           <thead>
             <tr>
               <th><font face="arial" size=2>Id.Prov</font></th>
               <th><font face="arial" size=2>Nombre proveedor</font></th>
               <th><font face="arial" size=2>Fech.Compra</font></th>
               <th style="text-align:right"><font face="arial" size=2>Precio</font></th>
             </tr>
           </thead>
           <tbody>
             <?php
                foreach($result_prov_com as $resultados){
                  $detallecompras = DetalleCompra::find()->where(['=','idDetalleCompra', $resultados['detalle_max']])->one();
                  $proveedores = Proveedores::find()->where(['=','codProveedores', $resultados['idProveedor']])->one();
                  echo '<tr>
                          <td>'.$resultados['idProveedor'].'</td>
                          <td>'.$proveedores->nombreEmpresa.'</td>
                          <td>'.date('d-m-Y', strtotime( $resultados['fecha_max'] )).'</td>
                          <td align="right">'.number_format(@$detallecompras->precioCompra,2).'</td>
                        </tr>';
                  $detallecompras = '';
                }
             ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="panel panel-info">
     <div class="panel-heading">
       Proveedores que ya cuentan con un pedido de este producto.
     </div>
     <div class="panel-body" style="height: 150px;width: 100%; overflow-y: auto; ">
       <div id="tabla_proveedores_con_este_pedido">
         <table class="items table table-striped">
           <thead>
             <tr>
               <th><font face="arial" size=2>Nombre proveedor</font></th>
               <th><font face="arial" size=2>Fecha pedido</font></th>
               <th><font face="arial" size=2>Cantidad pedido</font></th>
             </tr>
           </thead>
           <tbody>
             <?php
             $producto_pedido = ProductoPedido::find()->where(['<>','idProveedor', $idProveedor])
             ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$idProducto])->all();
                foreach($producto_pedido as $producto){
                  $orden_compra_prov = OrdenCompraProv::find()->where(['=','idOrdenCompra', $producto['idOrdenCompra']])
                  ->andWhere("estado = :estado", [":estado"=>'Generada'])->one();
                  if (@$orden_compra_prov) {
                    $detalle_orden_compr = DetalleOrdenCompra::find()->where(['=','idOrdenCompra', $producto['idOrdenCompra']])
                    ->andWhere("codProdServicio = :codProdServicio", [":codProdServicio"=>$idProducto])->one();
                    $proveedores = Proveedores::find()->where(['=','codProveedores', $producto['idProveedor']])->one();
                    echo '<tr class="danger parpadea text2">
                            <td>' .$proveedores->nombreEmpresa.'</td>
                            <td style="text-align:center">'.date('d-m-Y', strtotime( $orden_compra_prov->fecha_registro )).'</td>
                            <td style="text-align:center">'.$detalle_orden_compr->cantidad_pedir.'</td>
                          </tr>';
                  }
                }
             ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
  </div>
</div>
