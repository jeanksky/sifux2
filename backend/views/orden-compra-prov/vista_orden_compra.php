<?php
use backend\models\OrdenCompraProv;
use backend\models\DetalleOrdenCompra;
use backend\models\ContactosPedidos;
use backend\models\Transporte;
use backend\models\ProductoServicios;
use backend\models\ProductoProveedores;
use backend\models\Empresa;

$ordencompraprov = OrdenCompraProv::find()->where([ 'idOrdenCompra'=>$idOrdenCompra ])->one();
$detalle_orden_compra = DetalleOrdenCompra::find()->where(['idOrdenCompra'=>$ordencompraprov->idOrdenCompra])->orderBy(['idDetaOrdCom' => SORT_ASC])->all();
$contacto_pedidos = ContactosPedidos::find()->where([ 'idContacto_pedido'=>$ordencompraprov->idContacto_pedido ])->one();
$Transporte = Transporte::find()->where(['=','id_transporte', $ordencompraprov->id_transporte])->one();
$empresaimagen = new Empresa();
$empresa = Empresa::find()->where(['idEmpresa'=>1])->one();

$cant_filas = 1;
foreach ($detalle_orden_compra as $key) {
  $cant_filas += 1;
}
?>
<div class="panel panel-default">
    <div class="panel-body">
        <table>
            <tbody>
            <tr>
            <td>
            <img src="<?= $empresaimagen->getImageurl('html') ?>" style="height: 120px;">
            </td>
            <td>
            <strong><?= $empresa->nombre ?></strong><br>
            <strong>Cédula jurídica:</strong> <?= $empresa->ced_juridica ?><br>
            <strong>Teléfono(s):</strong> <?= $empresa->telefono .' / '. $empresa->fax ?><br>
            <strong>Dirección:</strong> <?= $empresa->localidad .' - '. $empresa->direccion ?><br>
            <strong>Dirección:</strong> <?= $empresa->sitioWeb ?><br>
            <strong>Email:</strong> <?= $empresa->email_proveeduria ?>
            </td>
            <tr>
            </tbody>
        </table>
    </div>
</div>
<h4 style="text-align:right; color: #891313; font-family: Century Gothic, sans-serif;">Pedido de compra N°: <?= $idOrdenCompra ?><br>Fecha: <?= date('d-m-Y', strtotime( $ordencompraprov->fecha_registro )) ?></h4>
<table  border="1" bordercolor="#B29E9E">
  <tr>
      <th style="width: 400px; font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;">
        Prioridad: <font size=5><?= $ordencompraprov->prioridad ?></font><hr>
        Contacto pedido: <br><font size=4><?= @$contacto_pedidos ? $contacto_pedidos->nombre : 'El contacto fue eliminado' ?></font>

      </th>
      <th style="width: 600px; font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;">

        <font style="float:left;"><strong>Transporte:</strong><br><font size=4><?= @$Transporte ? $Transporte->nombre_transporte : 'Transporte elimado' ?></font></font>
        <font style="float:right;">Teléfono: <br><font size=4><?= @$Transporte ? $Transporte->telefono : '' ?></font></font><br><br>
        <font><strong>Dirección:</strong> <br><?= @$Transporte ? $Transporte->direccion : '' ?> </font>
      </th>
  </tr>
</table>
<p>
  <font size=4>Observacion: <?= $ordencompraprov->observaciones ?></font>
</p>
<p>
  <font size=3>IMPORTANTE: Entiéndase que <strong>**Código producto</strong> es el código
    proveniente de su base de datos según nuestros registros de compras.</font>
</p>
<table border=1 cellspacing=0 cellpadding=2 bordercolor="#4169e1" >
  <tr>
    <th style="font-size:18px;background-color:#4169e1;color:#ffffff; width: 200px; ">Código producto</th>
    <th style="font-size:18px;background-color:#4169e1;color:#ffffff; width: 570px;">Descripción</th>
    <th style="font-size:18px;background-color:#4169e1;color:#ffffff; width: 230px; ">Cantidad de pedido</th>
  </tr>
  <?php
  foreach ($detalle_orden_compra as $key => $producto) {
    $productoser = ProductoServicios::find()->where(['codProdServicio'=>$producto['codProdServicio']])->one();
    $prod_prov = ProductoProveedores::find()->where(['=','idProd_prov', $producto['idProd_prov']])->one();
    echo '<tr>
            <td align="center">'.$prod_prov->codigo_proveedor.'</td>
            <td>'.$productoser->nombreProductoServicio.'</td>
            <td align="center">'.$producto['cantidad_pedir'].'</td>
          </tr>';
  }
   ?>
</table>
