<?php
  use yii\helpers\Html;
  use backend\models\ProductoServicios;
  use backend\models\ProductoProveedores;
  use backend\models\Proveedores;
  use yii\bootstrap\Modal;
  use yii\helpers\ArrayHelper;
  use kartik\widgets\Select2;
  use yii\widgets\ActiveForm;
//------------------ACTUALIZA PRODUCTO A INVENTARIO
$form = ActiveForm::begin(['options' => ['name'=>'cajapro', 'target'=>"_blank", 'id'=>'target']/*, 'action'=>['actualiza_prod_inv', 'id'=>$codProdServicio]*/ ]);
    Modal::begin([
            'id' => 'modalActualiprodinv',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Modificar este producto en inventario</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" id="salir_mod_inven" data-dismiss="modal">Salir</a> <input value="Actualizar" onclick="actualiza_refresca()" class="btn btn-success">',
        ]);

    echo '<div class="panel panel-default">';
      echo '<div class="panel-body">';
        echo '<ul class="nav nav-tabs nav-justified">
                  <li class="active" role="presentation" ><a href="#productos" data-toggle="tab">Datos del producto</a></li>
                  <li role="presentation"><a href="#proveedores" id="proveedores_tab" data-toggle="tab">Proveedores asociados</a></li>
              </ul>';
    //$modelproducto = ProductoServicios::find()->where(['=','codProdServicio', $codProdServicio])->one();
    echo '<div class="table-responsive tab-content">';
        echo '<div class="tab-pane fade in active" id="productos"><br>';
          echo '<div class="producto-servicios-form">
                  <div class="col-lg-4">';
                  echo $form->field($modelproducto, 'tipo')->textInput(['readonly' => true, 'value' => 'Producto']);
                  echo $form->field($modelproducto, 'codProdServicio')->textInput();
                  echo $form->field($modelproducto, 'nombreProductoServicio')->textarea(['maxlength' => 500]);
                  echo $form->field($modelproducto, 'cantidad_dias_produccion')->textInput(["onkeypress"=>"return isNumberDe(event)"]);

            echo '</div>
                  <div class="col-lg-4">
                    <div class="col-lg-6">';
                    echo $form->field($modelproducto, 'cantidadMinima')->textInput();
              echo '</div>
                    <div class="col-lg-6">';
                    echo $form->field($modelproducto, 'ubicacion')->textInput();
              echo '</div>';
              echo '<div class="col-lg-12">';
        echo $form->field($modelproducto, 'codFamilia')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($modelproducto->getNombreFamilia(),'codFamilia',function($element) {
                                return $element['codFamilia'].' - '.$element['descripcion'];
                            }),
                    'language'=>'es',
                    'options' => ['placeholder' => '- Seleccione -'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Categoría');

              echo '</div>
                    <div class="col-lg-12">';
                      echo $form->field($modelproducto, 'precioCompra')->textInput(['id'=>'preciocIn', 'onkeyup'=>'javascript:obtenerPrecioVentaIn()']);
             echo ' </div>
                    <div class="col-lg-12">';
                        echo $form->field($modelproducto, 'porcentUnidad')->textInput(['id'=>'porcentuIn', 'onkeyup'=>'javascript:obtenerPrecioVentauIn()']);
              echo '</div>
                  </div>
                  <div class="col-lg-4">';
                    echo $form->field($modelproducto, 'precioVenta')->textInput(['id'=>'preciovIn', 'onkeyup'=>'obtenerUtilidadImpuIn()']);
                    echo $form->field($modelproducto, 'exlmpuesto')->radioList(['Si'=>'Si','No'=>'No']);
                    echo $form->field($modelproducto, 'precioVentaImpuesto')->textInput(['id'=>'precioventaimpuIn', 'onkeyup'=>'obtenerVentaUtiIn()']);
                    echo $form->field($modelproducto, 'anularDescuento')->radioList(['Si'=>'Si','No'=>'No']);
                    echo '<div class="form-group" style="text-align:right">';
                    echo '</div>';
            echo '</div>
                </div>';
        echo '</div>';
  ?>
              <div class="tab-pane fade" id="proveedores">
                <div class="col-lg-12"><br>
                	Agregue los proveedores que ofrecen este producto.
                  <div class="pull-right">
                    <?= Html::a('', '#', ['class' => 'fa fa-plus fa-2x', 'data-toggle' => 'modal', 'onclick' => 'javascript: $.fn.modal.Constructor.prototype.enforceFocus = function() {};', 'data-target' => '#modalnuevoproveedor', 'data-placement' => 'left', 'title' => Yii::t('app', 'Agregar nuevo proveedor')])  ?>
                  </div>
                  <div class="scroll">
                    <div id="tabla_proveedores" class="pagos-form">
                      <?php
              					$proveedores_p = ProductoProveedores::find()->where([ 'codProdServicio'=>$codProdServicio ])->orderBy(['idProd_prov' => SORT_DESC])->all();
              					echo '<table class="items table table-striped">';
              					if($proveedores_p) {
              					echo '<tbody>';
              						foreach($proveedores_p as $position => $proveedor_p) {
              							$proveedor = Proveedores::find()->where(['codProveedores'=>$proveedor_p['codProveedores']])->one();
              							$nombreEmpresa = @$proveedor ? $proveedor->nombreEmpresa : $proveedor_p['codProveedores'].' **Este proveedor fue eliminado**';
              							$eliminar = Html::a('', null, ['class'=>'fa fa-remove fa-2x', 'style'=>'cursor:pointer;', 'onclick'=>'javascript:elimina_proveedor('.$proveedor_p['idProd_prov'].')' ,'data-toggle' => 'titulo', 'title' => Yii::t('app', 'Descartar este proveedor') ]);
              							$actualizar = Html::a('', ['', 'id' => $proveedor_p['idProd_prov']], [
              															'class'=>'fa fa-pencil fa-2x',
              															'id' => 'activity-index-link-agregacredpen',
              															'data-toggle' => 'modal',
              															'onclick'=>'javascript:actualiza_modal_proveedor('.$proveedor_p['idProd_prov'].')',
              															'data-target' => '#modalActualizaproveedor',
              															//'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
              															'data-pjax' => '0',
              															'title' => Yii::t('app', 'Actualizar datos') ]);
              							$accion = $actualizar.' '.$eliminar;
              							printf('<tr><td><font face="arial" size=4>%s</font></td>
              											<td><font face="arial" size=4>%s</font></td>
              											<td class="actions button-column">%s</td></tr>',
              											'<strong>PROVEEDOR:</strong> '.$nombreEmpresa,
              											'<strong>CÓDIGO:</strong> '.$proveedor_p['codigo_proveedor'],
              											$accion
              									);
              						}
              					echo '</tbody>';
              					}
              					echo '</table>';
              				?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
<?php
      echo '</div>';
    echo '</div>';
    Modal::end();
    ActiveForm::end();
?>
<?php
    //Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevoproveedor',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Asignar nuevo proveedor</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', null, [
                    'class'=>'btn btn-success',
                    'style'=>'cursor:pointer;',
                    'onclick'=>'asignar_proveedor()',
                    'title'=>'Agregar nuevo proveedor'
                ]),
            ]);
            echo '<div class="well">';
            echo '<label>Proveedor: </label>';
						echo Select2::widget([
														'name' => '',
														'data' => ArrayHelper::map(Proveedores::find()->all(), 'codProveedores',
																function($element) {
																 return $element['codProveedores'].' - '.$element['nombreEmpresa'];
														}),
														'options' => [
																'id'=>'ddl-codProveedores',
																'placeholder'=>'- Seleccione -',
																//'onchange'=>'javascript:',
																//'multiple' => false //esto me ayuda a que solo se obtenga uno
														],
														'pluginOptions' => ['initialize'=> true,'allowClear' => true]
												]);
            echo '<br><label>Código del producto (PROVEEDOR): </label>';
            echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'codigo_proveedor', 'placeholder' =>' Código', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
            echo '</div>';

            Modal::end();
    ?>
<?php
//Muestra una modal para actualizar los datos seleccionados de la relacion con el proveedor
     Modal::begin([
            'id' => 'modalActualizaproveedor',
            'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Actualizar proveedor</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-refresh"></i> Actualizar', null, [
                'class'=>'btn btn-success',
                'style'=>'cursor:pointer;',
                'onclick'=>'actualiza_proveedor()',
                'title'=>'Agregar nuevo proveedor'
            ]),
        ]);
        echo '<div class="well">';
        echo' <input type="hidden" name="idProd_prov" id="idProd_prov">';
        echo '<label>Proveedor: </label>';
        echo Select2::widget([
                        'name' => '',
                        'data' => ArrayHelper::map(Proveedores::find()->all(), 'codProveedores',
                            function($element) {
                             return $element['codProveedores'].' - '.$element['nombreEmpresa'];
                        }),
                        'options' => [
                            'id'=>'ddl-codProveedores_',
                            'placeholder'=>'- Seleccione -',
                            //'onchange'=>'javascript:',
                            //'multiple' => false //esto me ayuda a que solo se obtenga uno
                        ],
                        'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                    ]);
        echo '<br><label>Código del producto (PROVEEDOR): </label>';
        echo Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'codigo_proveedor_', 'placeholder' =>' Código', 'name' => 'descripcion', 'readonly' => false, 'onchange' => '', "onkeypress"=>""]);
        echo '</div>';

        Modal::end();
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//Funcion para que me permita ingresar solo numeros
  function isNumber(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode;
      if ( (charCode > 47 && charCode < 58))
          return true;
      return false;
  }
  //----------------------------------------------------------------------------------------------------
  //Estas funciones son para agregar productos a inventario
  function obtenerPrecioVentaIn() {//Obtengo el valor del checkbox seleccionado para la suma
          caja=document.forms["cajapro"].elements;
          precioc=Number(caja["preciocIn"].value);
          //var precioc = document.getElementById("precioc").value;
          iva=13/100;
          //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
          utilidad=Number(caja["porcentuIn"].value)/100;
          //var utilidad = $("#porcentu").val();

          //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
          subtotal=precioc*utilidad;
          //para luego sumarlo al precio de compra
          total=precioc+subtotal;
          caja["preciovIn"].value=total.toFixed(2);
          caja["precioventaimpuIn"].value=(total+(total * iva)).toFixed(2);
      }
  function obtenerPrecioVentauIn() {//Obtengo el valor del checkbox seleccionado para la suma
          caja=document.forms["cajapro"].elements;
          precioc=Number(caja["preciocIn"].value);
          iva=13/100;

          //Aqui el detalle, el numero ingresado en 'utilidad', se debe dividir entre 100 para convertirlo a un valor porcentual.
          utilidad=Number(caja["porcentuIn"].value)/100;

          //Despues simplemente multiplicamos el utlilidad por el precio de compra y lo asignas al sutotal
          subtotal=precioc*utilidad;
          //para luego sumarlo al precio de compra
          total=precioc+subtotal;
          caja["preciovIn"].value=total.toFixed(2);
          caja["precioventaimpuIn"].value=(total+(total * iva)).toFixed(2);
      }
  function obtenerUtilidadImpuIn() {//Obtengo el valor del checkbox seleccionado para la suma
          caja=document.forms["cajapro"].elements;
          precioc=Number(caja["preciocIn"].value);
          preciov=Number(caja["preciovIn"].value);
          iva=13/100;
          //el % del margen sale del el precio de ganacia dividido del precio de compra
          utilidad = preciov/precioc;

          //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
          subtotal=utilidad*100;
          //para luego restarle 100 dejandonos el margen de utilidad
          total=subtotal-100;
          caja["porcentuIn"].value=total.toFixed(2);
          caja["precioventaimpuIn"].value=(preciov+(preciov * iva)).toFixed(2);
      }
  function obtenerVentaUtiIn() {//Obtengo el valor del checkbox seleccionado para la suma
          caja=document.forms["cajapro"].elements;
          precioc=Number(caja["preciocIn"].value);
          precioventaimpu=Number(caja["precioventaimpuIn"].value);

          preciov = precioventaimpu/1.13;
          //el % del margen sale del el precio de ganacia dividido del precio de compra
          utilidad = preciov/precioc;

          //Despues simplemente multiplicamos la utlilidad por 100 para que deje de ser decimal
          subtotalporc=utilidad*100;
          //para luego restarle 100 dejandonos el margen de utilidad
          totalporc=subtotalporc-100;
          caja["porcentuIn"].value=totalporc.toFixed(2);
          caja["preciovIn"].value=preciov.toFixed(2);
      }
      //muestra la modal con los datos seleccionados del proveedor relacionado
      function actualiza_modal_proveedor(idProd_prov) {
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/actualiza_modal_proveedor') ?>",
            type:"post",
            dataType: 'json',
            data: { 'idProd_prov': idProd_prov },
            success: function(data){
                $('#idProd_prov').val(data.idProd_prov);
                $('#ddl-codProveedores_').val(data.codProveedores).change();
                $('#codigo_proveedor_').val(data.codigo_proveedor);
                //$("#update-monedas").load(location.href+' #update-monedas','');
            },
            error: function(msg, status,err){
             alert('Error, consulte linea 166 (informar a Nelux)');
            }
        });
      }
      //esta funcion me permite asignar el proveedor seleccionado al producto
      function asignar_proveedor(){
          codProdServicio = '<?= $codProdServicio ?>';
          var codProveedores = document.getElementById('ddl-codProveedores');
          var codigo_proveedor = document.getElementById('codigo_proveedor').value;
          if (codigo_proveedor == '' || codProveedores.value == '') {
              alert('Por favor no deje campos vacios');
          } else {
              $.ajax({
                  url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/asignar_proveedor') ?>",
                  type:"post",
                  data: {	'codProdServicio': codProdServicio, 'codProveedores': codProveedores.value, 'codigo_proveedor' : codigo_proveedor },
                  beforeSend: function() {
                    //$('#modalnuevoproveedor').hide("slow");//quito la modal modalnuevoproveedor
                    $('#modalnuevoproveedor').modal('toggle');//elimino el fondo nego de la modal modalnuevoproveedor
                    //$('#modalActualiprodinv').hide("slow");//quito la modal modalActualiprodinv
                    $('#modalActualiprodinv').modal('toggle');//elimino el fondo nego de la modal modalActualiprodinv
                  },
                  success: function(data){
                    /*$('#ddl-codProveedores').val('').change();
                    $('#codigo_proveedor').val('');*/

                    $.get( "<?= Yii::$app->getUrlManager()->createUrl(['orden-compra-prov/actualizar_producto_modal', 'id'=>$codProdServicio]) ?>", function( data ) {

                      $('#actualizar_producto').html(data);
                      $('#modalActualiprodinv').modal('show');//muestro la modal
                      $("#proveedores").addClass("active"); //activo la pestaña tav
                      $("#proveedores_tab").tab("show"); //muestro lo que contiene la pestaña
                    });


                  },
                  error: function(msg, status,err){
                   alert('Error, consulte linea 36 (informar a Nelux)');
                  }
              });
          }
      }
      //funcion que me actualiza los datos mostrados en la modal luego de la modificacion
      function actualiza_proveedor() {
        var idProd_prov = document.getElementById('idProd_prov').value;
        var codProveedores = document.getElementById('ddl-codProveedores_');
        var codigo_proveedor = document.getElementById('codigo_proveedor_').value;
        if (codigo_proveedor == '' || codProveedores.value == '') {
            alert('Por favor no deje campos vacios');
        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/actualiza_proveedor') ?>",
                type:"post",
                data: {	'idProd_prov': idProd_prov, 'codProveedores': codProveedores.value, 'codigo_proveedor': codigo_proveedor },
                beforeSend: function() {
                  //$('#modalActualizaproveedor').hide("slow");//quito la modal modalnuevoproveedor
                  $('#modalActualizaproveedor').modal('toggle');//elimino el fondo nego de la modal modalnuevoproveedor
                  //$('#modalActualiprodinv').hide("slow");//quito la modal modalnuevoproveedor
                  $('#modalActualiprodinv').modal('toggle');//elimino el fondo nego de la modal modalActualiprodinv
                },
                success: function(data){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl(['orden-compra-prov/actualizar_producto_modal', 'id'=>$codProdServicio]) ?>", function( data ) {

                      $('#actualizar_producto').html(data);
                      $('#modalActualiprodinv').modal('show');//muestro la modal
                      $("#proveedores").addClass("active"); //activo la pestaña tav
                      $("#proveedores_tab").tab("show"); //muestro lo que contiene la pestaña
                    });
                    /*$('#ddl-codProveedores_').val('').change();
                    $('#codigo_proveedor_').val('');*/
                },
                error: function(msg, status,err){
                 alert('Error, consulte linea 36 (informar a Nelux)');
                }
            });
        }
      }
      //elimina proveedor del producto
      function elimina_proveedor(id){
          if(confirm("Está seguro de quitar este proveedor")){
              $.ajax({
                  url:"<?php echo Yii::$app->getUrlManager()->createUrl('producto-servicios/elimina_proveedor') ?>",
                  type:"post",
                  data: { 'id': id },
                  beforeSend: function() {
                    //$('#modalActualiprodinv').hide("slow");//quito la modal modalnuevoproveedor
                    $('#modalActualiprodinv').modal('toggle');//elimino el fondo nego de la modal modalActualiprodinv
                    //$('#modalActualizaproveedor').hide("slow");//quito la modal modalActualizaproveedor
                    //$('#modalActualizaproveedor').modal('toggle');//elimino el fondo nego de la modal modalActualizaproveedor
                  },
                  success: function(data){
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl(['orden-compra-prov/actualizar_producto_modal', 'id'=>$codProdServicio]) ?>", function( data ) {
                      $('#actualizar_producto').html(data);//CARGO LOS NUEVOS DATOS DE LA MODAL
                      $('#modalActualiprodinv').modal('show');//muestro la modal
                      $("#proveedores").addClass("active"); //activo la pestaña tav
                      $("#proveedores_tab").tab("show"); //muestro lo que contiene la pestaña
                    });
                  },
                  error: function(msg, status,err){
                   alert('No pasa 101');
                  }
              });
          }
      }

</script>
