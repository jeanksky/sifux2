<?php
use yii\helpers\Html;
use backend\models\Familia;
use backend\models\ContactosPedidos;
use backend\models\OrdenCompraSession;
use backend\models\Transporte;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use backend\models\ProductoServicios;
$load_buton_ = \Yii::$app->request->BaseUrl.'/img/load.gif';
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
$loading3 = \Yii::$app->request->BaseUrl.'/img/loading3.gif';
//$jscroll = \Yii::$app->request->BaseUrl.'/js/jquery.jscroll.js';
$nombre_categoria = Familia::find()
->where(['=','estado','1'])
->andwhere(['=','tipo','Producto'])
->orderBy('codFamilia ASC')
->all();
$limit = 10;
//con lo siguiente pondo las fechas de inicio de busqueda desde hace dos mesces hasta la fecha actual
$diasdesde = 90; //tres meses (90 dias)
$fecha_hasta = date('d-m-Y');
$fecharestada = strtotime ( '-'.$diasdesde.' day' , strtotime ( $fecha_hasta ) ) ;
$fecha_desde = date ( 'j-m-Y' , $fecharestada );

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
/*$(document).ready(function(){
  var scroll = $('#orden_compra_productos_disponibles');
  var div_pd = $('#div_productos_disponibles');
  scroll.scroll(function(){
      var posicion_scroll = scroll.scrollTop(); // VALOR QUE SE HA MOVIDO DEL SCROLL
      var limit = document.getElementById('cantidad_a_mostrar').value;
      if (posicion_scroll >= $(div_pd).height() - $(scroll).height()) { // SI EL SCROLL HA SUPERADO EL ALTO DE TU DIV
        //alert($(div_pd).height()+' '+$(scroll).height())
         mostrar_mas_productos(limit);
      } else {
      }
  });
});*/
//Funcion para que me permita ingresar solo numeros
  function isNumber(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if ( (charCode > 47 && charCode < 58))
              return true;
          return false;
      }
      //Función que busca el DIV, coge su valor y lo guarda en el HiddenField
      function SetScrollValue() {
          var divObj = document.getElementById("orden_compra_productos_disponibles");
          var obj = document.getElementById("HiddenScrollbarPosition");
          if (obj != null) {
              obj.value = divObj.scrollTop;
              return obj;
          }
      }
      //Función que busca el HiddenField, coge el valor previamente guardado
      // y lo setea de nuevo en la propiedad scrollTop de nuestro DIV
      function GetScrollBarValue() {
          var divObj = document.getElementById("orden_compra_productos_disponibles");
          var obj = document.getElementById("HiddenScrollbarPosition");
          if (divObj != null) {
              divObj.scrollTop = obj.value;
              return obj;
          }
      }

  function mostrar_mas_productos(limit) {
    var idProveedor = '<?= $idProveedor ?>';
    var categoria_excluir = $("#categoria").val();
    var lista_categoria = '0';
    var posi_ini = SetScrollValue();
    if (categoria_excluir==null) {
      lista_categoria = '0';
    } else {
      lista_categoria = categoria_excluir.toString();
    }
    var prov_fech_desde = '';
    var prov_fech_hasta = '';
    filtro = document.getElementById('filtro_minim').value;
    var dias_rendimiento = document.getElementById('dias_rendimiento').value;
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_disponibles') ?>" ,
        {
          'limit' : limit,
          'idProveedor' : idProveedor,
          'filtro_minimo' : filtro,
          'filtro_excluir' : lista_categoria,
          'dias_rendimiento' : dias_rendimiento,
          'prov_fech_desde' : prov_fech_desde,
          'prov_fech_hasta' : prov_fech_hasta
        } ,
          function( data ) {
        //  $('#orden_compra_productos_disponibles').html(data);
          $("#orden_compra_productos_disponibles").html(data);
          document.getElementById('cantidad_a_mostrar').value = limit;
          var posi_fin = GetScrollBarValue();
        });
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_pedido') ?>" ,
        { 'idProveedor' : idProveedor } , function( data ) {
          $('#orden_compra_productos_pedido').html(data);
        });
  }

  //funcion que me filtra por cerca del minimo, igual al minimo, menor al minimo o todos
  function accion_filtro_minimo(filtro) {
    var idProveedor = '<?= $idProveedor ?>';
    var categoria_excluir = $("#categoria").val();
    var lista_categoria = '0';
    SetScrollValue();
    if (categoria_excluir==null) {
      lista_categoria = '0';
    } else {
      lista_categoria = categoria_excluir.toString();
    }
    var prov_fech_desde = '';
    var prov_fech_hasta = '';
    document.getElementById('filtro_minim').value = filtro;
    var limit = document.getElementById('cantidad_a_mostrar').value;
    var dias_rendimiento = document.getElementById('dias_rendimiento').value;
    $('#orden_compra_productos_disponibles').html('<center><img src="<?= $load_buton_ ?>" style="height: 300px;"></center>');
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_disponibles') ?>" ,
        {
          'limit' : limit,
          'idProveedor' : idProveedor,
          'filtro_minimo' : filtro,
          'filtro_excluir' : lista_categoria,
          'dias_rendimiento' : dias_rendimiento,
          'prov_fech_desde' : prov_fech_desde,
          'prov_fech_hasta' : prov_fech_hasta
        } ,
          function( data ) {
          $('#orden_compra_productos_disponibles').html(data);
          //document.getElementById("orden_compra_productos_disponibles").innerHTML = data;
          GetScrollBarValue();
        });
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_pedido') ?>" ,
        { 'idProveedor' : idProveedor } , function( data ) {
          $('#orden_compra_productos_pedido').html(data);
        });
  }

  //asigna a cada producto antes del pediso su respectiva cantidad sujerida
  function asignarcantidadsujerida(cantidad_sugerida,codProdServicio) {
    //$('#'+codProdServicio).val(cantidad_sugerida).change();
    if (cantidad_sugerida!=0) {
      document.getElementById(codProdServicio).select();
      document.getElementById(codProdServicio).value = cantidad_sugerida;
    }
    //$(codProdServicio).change();
  }

  //agrega el producto y refresca las tablas
  function agrega_producto_pedido(key, idinput, cantidad_sugerida, idProd_prov, limit) {
      if(key == 13) {
        SetScrollValue();
        var cantidad_pedir = document.getElementById(idinput).value;
        var idProveedor = '<?= $idProveedor ?>';
        $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('orden-compra-prov/agrega_producto_pedido') ?>",
          type:"post",
          data: { 'codigo_producto' : idinput, 'cantidad_sugerida' : cantidad_sugerida, 'cantidad_pedir' : cantidad_pedir, 'idProveedor' : idProveedor, 'idProd_prov' : idProd_prov },
          beforeSend: function() {
              $('#notificacion_orden_pedido').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
          },
          success: function(notif){
            filtro_minimo = document.getElementById('filtro_minim').value;
            var categoria_excluir = $("#categoria").val();
            var lista_categoria = '0';
            var prov_fech_desde = document.getElementById('prov_fech_desde').value;
            var prov_fech_hasta = document.getElementById('prov_fech_hasta').value;
            div = document.getElementById('opcion_acordeon_producto');
            if (categoria_excluir==null) {
              lista_categoria = '0';
            } else {
              lista_categoria = categoria_excluir.toString();
            }
            var dias_rendimiento = document.getElementById('dias_rendimiento').value;
            //me refresca la tabla de productos disponibles
            $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_disponibles') ?>" ,
            { 'idProveedor' : idProveedor, 'filtro_minimo' : filtro_minimo, 'limit' : limit,
            'filtro_excluir' : lista_categoria, 'dias_rendimiento': dias_rendimiento,
            'prov_fech_desde' : prov_fech_desde, 'prov_fech_hasta' : prov_fech_hasta } ,
            function( data ) {
              $('#orden_compra_productos_disponibles').html(data);
            });
            //me refresca la tabla de productos para nuevo pedido
            $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_pedido') ?>" ,
            { 'idProveedor' : idProveedor } , function( data ) {
              $('#orden_compra_productos_pedido').html(data);
            });
            document.getElementById("notificacion_orden_pedido").innerHTML = notif;
            setTimeout(function() {//aparece la alerta en un milisegundo
                $("#notificacion_orden_pedido").fadeIn();
            });
            setTimeout(function() {//se oculta la alerta luego de 4 segundos
                $("#notificacion_orden_pedido").fadeOut();
            },15000);
            div.style.display = 'none';
            document.getElementById("media").checked = true;
            GetScrollBarValue();
          }
      });
      }

  }

  //devuelve los productos en el pedido para la tabla de productos disponibles
  function obtener_productos_pedidos_regreso() {
    var checkboxValues = "";//declaramos la variable conjunta de checbox vacia
    var idProveedor = '<?= $idProveedor ?>';
    div = document.getElementById('opcion_acordeon_producto');
    SetScrollValue();
    $('input[name="checkboxRowinv[]"]:checked').each(function() {//
        checkboxValues += $(this).val() + ",";
    });//
    //eliminamos la última coma.
    checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);
    $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('orden-compra-prov/obtener_productos_pedidos_regreso') ?>",
            type:"post",
            data: { 'checkboxValues' : checkboxValues, 'idProveedor' : idProveedor },
            beforeSend: function() {
                //$('#load_buton').html('<center><img src="<?php //echo $load_buton; ?>" style="height: 32px;"></center>');
            },
            success: function(notif){
              filtro_minimo = document.getElementById('filtro_minim').value;
              var categoria_excluir = $("#categoria").val();
              var lista_categoria = '0';
              if (categoria_excluir==null) {
                lista_categoria = '0';
              } else {
                lista_categoria = categoria_excluir.toString();
              }
              var prov_fech_desde = document.getElementById('prov_fech_desde').value;
              var prov_fech_hasta = document.getElementById('prov_fech_hasta').value;
              var dias_rendimiento = document.getElementById('dias_rendimiento').value;
              var limit = document.getElementById('cantidad_a_mostrar').value;
              $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_disponibles') ?>" ,
              { 'idProveedor' : idProveedor,
                'limit' : limit,
                'filtro_minimo' : filtro_minimo,
                'filtro_excluir' : lista_categoria,
                'dias_rendimiento': dias_rendimiento,
                'prov_fech_desde' : prov_fech_desde,
                'prov_fech_hasta' : prov_fech_hasta
              } ,
                function( data ) {
                $('#orden_compra_productos_disponibles').html(data);
              });
              $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_pedido') ?>" , { 'idProveedor' : idProveedor } , function( data ) {
                $('#orden_compra_productos_pedido').html(data);
              });
              document.getElementById("notificacion_orden_pedido").innerHTML = notif;
              setTimeout(function() {//aparece la alerta en un milisegundo
                  $("#notificacion_orden_pedido").fadeIn();
              });
              setTimeout(function() {//se oculta la alerta luego de 4 segundos
                  $("#notificacion_orden_pedido").fadeOut();
              },15000);
              div.style.display = 'none';
              GetScrollBarValue();
            },
            error: function(msg, status,err){
             //alert('No pasa 56');
            }
        });
  }

  //con esta funcion filtro las categorias que quiero excluir
  function excluir_categoria() {
    SetScrollValue();
    var idProveedor = '<?= $idProveedor ?>';
    var categoria_excluir = $("#categoria").val();
    var limit = document.getElementById('cantidad_a_mostrar').value;
    var lista_categoria = '0';
    if (categoria_excluir==null) {
      lista_categoria = '0';
    } else {
      lista_categoria = categoria_excluir.toString();
    }
    var filtro_minimo = document.getElementById('filtro_minim').value;
    var prov_fech_desde = '';
    var prov_fech_hasta = '';
    var dias_rendimiento = document.getElementById('dias_rendimiento').value;
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_disponibles') ?>" ,
    { 'idProveedor' : idProveedor, 'limit' : limit, 'filtro_minimo' : filtro_minimo,
    'filtro_excluir' : lista_categoria, 'dias_rendimiento' : dias_rendimiento,
    'prov_fech_desde' : prov_fech_desde, 'prov_fech_hasta' : prov_fech_hasta } ,
    function( data ) {
          $('#orden_compra_productos_disponibles').html(data);
        });
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_pedido') ?>" ,
        { 'idProveedor' : idProveedor } , function( data ) {
          $('#orden_compra_productos_pedido').html(data);
        });
        GetScrollBarValue();
  }


  //obtengo el valor del producto seleccionado y la linea
  function marcarproducto(linea, idProducto, idProveedor) {
    //http://foro.elhacker.net/desarrollo_web/evento_onfocus_en_fila_de_una_tabla-t381665.0.html
    // Obtenemos todos los TR de la tabla con id "tabla_facturas_pendientes"
    // despues del tbody.
    var elementos = document.getElementById('tabla_productos_disponibles').
    getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    // Por cada TR empezando por el segundo, ponemos fondo blanco.
    for (var i = 0; i < elementos.length; i++) {
        elementos[i].style.background='white';
    }
    // Al elemento clickeado le ponemos fondo amarillo.
    linea.style.background="yellow";
    inyectarproducto_estadistica(idProducto, idProveedor);//llamo la funcion que me llenará el modulo de estadistica inyectando el id del producto
  }
  //lo mismo que arriba pero con la tabla tabla_productos_pedidos
  function marcarproductopedido(linea, idProducto, idProveedor) {

    var elementos = document.getElementById('tabla_productos_pedidos').
    getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (var i = 0; i < elementos.length; i++) {
        elementos[i].style.background='white';
      }
    linea.style.background="yellow";
    inyectarproducto_estadistica(idProducto, idProveedor);//llamo la funcion que me llenará el modulo de estadistica inyectando el id del producto
    }


  //inyecto los datos del cliente que necesito para manipular un movimiento de credito
  function inyectarproducto_estadistica(idProducto, idProveedor){
    var desde = document.getElementById("fech_desde_prov").value;
    var hasta = document.getElementById("fech_hasta_prov").value;
    document.getElementById("estadistica_producto").innerHTML = '<center><img src="<?= $load_buton_ ?>" style="height: 300px;"></center>';
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/estadistica_producto') ?>" , { 'idProducto' : idProducto, 'idProveedor' : idProveedor, 'desde' : desde, 'hasta' : hasta } ,
          function( data ) {
            if ($('#estadistica_producto').html(data).fadeIn('fast')) {
              $('#estadistica_producto').clearQueue();
              document.getElementById("prov_fech_desde").value = document.getElementById("fech_desde_prov").value;
              document.getElementById("prov_fech_hasta").value = document.getElementById("fech_hasta_prov").value;
            }
            //$("#estadistica_producto").hide().html(data).fadeIn('fast');
          });
  }

  //me permite ocultar el area que no voy a usar y mostrarlo si lo necesito
  function opcion_acordeon() {
      div = document.getElementById('opcion_acordeon_producto');
      if (div.style.display != 'none') { div.style.display = 'none'; } else { div.style.display = ''; }
  }

  //Funcion para obtener la suma de 30 dias a la fecha de documento
  function buscar_estadistica_producto(codProdServicio){
  	//var feast = $("#feast").val();

  	var prov_fech_desde = document.getElementById("prov_fech_desde").value;
  	var prov_fech_hasta = document.getElementById("prov_fech_hasta").value;
    idProveedor = '<?= $idProveedor ?>';
  	$.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('orden-compra-prov/buscar_estadistica_producto') ?>",
        type:"post",
        dataType: 'json',
        data: { 'prov_fech_desde': prov_fech_desde, 'prov_fech_hasta': prov_fech_hasta, 'codProdServicio' : codProdServicio, 'idProveedor': idProveedor },
        beforeSend: function() {
        	//$('#tabla_proveedores_que_se_le_compra').html('<center><img src="<?= $loading3; ?>" style="height: 200px;"></center>');
          //$('#tabla_proveedores_con_este_pedido').html('<center><img src="<?= $loading3; ?>" style="height: 100px;"></center>');
        },
        success: function(data){
          $('#comprados').html(data.comprados);
          $('#vendidos').html(data.vendidos);
          $('#rendimiento_div').html(data.rendimiento_div);
          $('#tabla_proveedores_que_se_le_compra').html(data.html_p_c);
          $('#tabla_proveedores_con_este_pedido').html(data.html_p_p);
          document.getElementById("fech_desde_prov").value = prov_fech_desde;
          document.getElementById("fech_hasta_prov").value = prov_fech_hasta;
        	//$("#tabla_proveedores_que_se_le_compra").load(location.href+' #tabla_proveedores_que_se_le_compra','');
        },
        error: function(msg, status,err){
            alert('error 32');
        }
    });
    //solo si el checkbox esta marcado procedo con la ejecucion decantidad sujerida para los productos
    if ($('#dias_rendimiento_rango_fechas').prop('checked')) {
        //aqui me modificará la cantidad sujerida de los productos deacuerdo este rango de fecha.
        SetScrollValue();
        var idProveedor = '<?= $idProveedor ?>';
        var categoria_excluir = $("#categoria").val();
        var lista_categoria = '0';
        if (categoria_excluir==null) {
          lista_categoria = '0';
        } else {
          lista_categoria = categoria_excluir.toString();
        }
        var filtro_minimo = document.getElementById('filtro_minim').value;
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_disponibles_rango_fechas') ?>" ,
        { 'idProveedor' : idProveedor, 'limit' : '<?= $limit ?>', 'filtro_minimo' : filtro_minimo,
        'filtro_excluir' : lista_categoria, 'prov_fech_desde' : prov_fech_desde,
        'prov_fech_hasta' : prov_fech_hasta, 'check' : 'si' } ,
        function( data ) {
          $('#orden_compra_productos_disponibles').html(data);
          GetScrollBarValue();
        });
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('orden-compra-prov/orden_compra_productos_pedido') ?>" ,
        { 'idProveedor' : idProveedor } , function( data ) {
          $('#orden_compra_productos_pedido').html(data);
        });
    }
  }
  //para abilitar el boton cuando sean las condiciones dadas respectivas al orden con el que se tienen que llenar el formulario
  function habilitarbtn() {
    idContacto_pedido = document.getElementById('idContacto_pedido').value;
    idTransporte = document.getElementById('idTransporte').value;
    observacion_pedido = document.getElementById('observacion_pedido').value;
    if (idContacto_pedido == '' || idTransporte == '' || observacion_pedido == '') {
      $('#realizar_orden_compra').addClass('disabled');
    } else {
      $('#realizar_orden_compra').removeClass('disabled');
    }
  }
  //funcion para completar la compras
  function completar_compra(this_boton) {
    idProveedor = '<?= $idProveedor ?>';
    var confirm_compra = confirm("¿Está seguro de generar esta compra?");
    if (confirm_compra == true) {
      var $btn = $(this_boton).button('loading');
      // simulating a timeout
      setTimeout(function () {
          $btn.button('reset');
      }, 60000);
      idContacto_pedido = document.getElementById('idContacto_pedido').value;
      idTransporte = document.getElementById('idTransporte').value;
      observacion_pedido = document.getElementById('observacion_pedido').value;
      prioridad = $('input:radio[name=prioridad]:checked').val();
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('orden-compra-prov/completar_compra') ?>",
      type:"post",
      dataType: 'json',
      data: { 'idProveedor' : idProveedor, 'idContacto_pedido' : idContacto_pedido, 'idTransporte' : idTransporte, 'observacion_pedido' : observacion_pedido, 'prioridad' : prioridad },
      beforeSend: function() {
      },
      success: function(data){
      },
      error: function(msg, status,err){
          //alert('error 32');
      }
  });
    } else { }
  }

  //esto me muestra la modal que tengo para modificar los productos
  $(document).on('click', '#activity-index-link-actualizaprod-inv', (function() {
              $.get(
                  $(this).data('url'),
                  function (data) {
                      document.getElementById("actualizar_producto").innerHTML="";//tengo que destruir el contenido del div para evitar conflictos
                      $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                      $('#actualizar_producto').html(data);//muestro en el div los datos de update
                      $('#modalActualiprodinv').modal('show');//muestro la modal

                  }
              );
          }));

  $(document).on('click', '#salir_mod_inven', (function() {
      obtener_productos_pedidos_regreso();
    }));

  function actualiza_refresca() {
    if ($( "#target" ).submit()) {
      $('#modalActualiprodinv').modal('hide');
      setTimeout("obtener_productos_pedidos_regreso();", 2000 );
      $('#notificacion_actualiz_pro').html('<br><div class="alert alert-success" style="font-size:15pt;">Producto actualizado</div>');
      setTimeout(function() {//aparece la alerta en un milisegundo
                  $("#notificacion_actualiz_pro").fadeIn();
              });
              setTimeout(function() {//se oculta la alerta luego de 4 segundos
                  $("#notificacion_actualiz_pro").fadeOut();
              },15000);
    }

  }
</script>
<style media="screen">
/*#categoria {
  width: 100%;
  margin: 0 auto;
}
#categoria, .select2-container .select2-selection {
  height: 120px;
  overflow: auto; display: block;
}*/
/*.select2-container--default .select2-selection--multiple {
  border-radius: 0;
  line-height: 30px;
  font-size: 14p;
  border: 1px solid #fafafa;
}*/
</style>
<!--area izquierda-->
<div class="col-lg-8 panel panel-primary">
  <p><input type="hidden" id="fech_desde_prov" value="<?= $fecha_desde ?>"></input><input type="hidden" id="fech_hasta_prov" value="<?= $fecha_hasta ?>"></input>
  <div class="col-lg-6">
    <label>Excluir categorías: </label>
    <?=
    '<div style="height: 130px; overflow: auto; display: block;">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
        'name' => '',
        'data' => ArrayHelper::map($nombre_categoria,'codFamilia',function($element) {
                    return $element['codFamilia'].' - '.$element['descripcion'];
                }),
        'options' => [
            'placeholder' => '- Seleccione las categorías a excluir -',
            'id'=>'categoria',
            'onchange'=>'javascript:excluir_categoria()',
            'multiple' => true //esto me ayuda a que la busqueda sea multiple
        ],
        'pluginOptions' => ['initialize'=> true,'allowClear' => true]
    ]).'</div>'
    ?>
  </div>

  <div class="col-lg-6">
    <p style="text-align:right">
        <label>Filtrando cantidad del producto:</label>&nbsp;&nbsp;<input type="text" id="filtro_minim" style="text-align: center;" value='Todos' disabled><br><br>
        <a class="btn btn-primary btn-sm" id="btn_cm" onclick="accion_filtro_minimo('Cerca del mínimo')" >Cerca del mínimo</a>
        <a class="btn btn-warning btn-sm" id="btn_im" onclick="accion_filtro_minimo('Igual al mínimo')" >Igual al mínimo</a>
        <a class="btn btn-danger btn-sm" id="btn_mm" onclick="accion_filtro_minimo('Menor al mínimo')" >Menor al mínimo</a>
        <a class="btn btn-default btn-sm" id="btn_t" onclick="accion_filtro_minimo('Todos')" >Todos</a>
    </p>
    <div class="col-lg-6">
      <label for="dias_rendimiento">Días rendimiento</label>
      <?= Html::input('text', '', OrdenCompraSession::getDiasrendimiento(), ['class' => 'form-control input-lg',
      'id' => 'dias_rendimiento', 'style'=>'font-family: Fantasy; font-size: 30pt; text-align: center;',
      'onkeyup' => 'javascript:excluir_categoria()', 'onkeypress'=>'return isNumber(event)',
      'title'=>"La cantidad de días que aquí se agregue (de forma opcional) deciderá la cantidad sugerida a comprar de los productos de este proveedor." ]) ?>
    </div>
  </div>
  </p>
  <div class="col-lg-12">
    <h3>Productos disponibles</h3>
    <p>Estos son los productos asignados a este proveedor, seleccione cada uno para ver su información de pedido,
      y si desea agregarlo a un nuevo pedido agregue una cantidad a pedir y precione ENTER. ***SI LA DESCRIPCIÓN DEL PRODUCTO PARPADEA ES PORQUE ESTÁ EN SU PUNTO DE PEDIDO.</p>
    <input type="hidden" id="cantidad_a_mostrar" value="<?= $limit ?>" />
    <input type="hidden" id="HiddenScrollbarPosition" runat="server" />
    <table class="table table-inverse" width="100%">
      <thead class="">
        <tr class="table-warning">
          <th width="20%"><font face="arial" size=2>Cód.Interno (Categ)</font></th>
          <th width="10%"><font face="arial" size=2>Cód.Proveedor</font></th>
          <th width="35%"><font face="arial" size=2>Descripción</font></th>
          <th width="5%" style="text-align:center"><font face="arial" size=4><span class="label label-default">Cant.Existente</span></font></th>
          <th width="5%" style="text-align:center"><font face="arial" size=4><span class="label label-warning">Cant.Mínima</span></font></th>
          <th width="5%" style="text-align:center"><font face="arial" size=4><span class="label label-info">Prom.Sugerida</span></font></th>
          <th width="15%" style="text-align:right" id="tam_cantp"><font face="arial" size=4><span class="label label-success">Cantidad a pedir</span></font></th>
          <th width="5%" class="actions button-column"></th>
        </tr>
      </thead>
    </table>
    <div class="table-responsive tab-content scroll" id="orden_compra_productos_disponibles" style="border: 2px solid #333; margin-top:10px; height: 400px; overflow: auto; display: block;">
      <?= $this->render('orden_compra_productos_disponibles', [
        'idProveedor' => $idProveedor,
        'limit' => $limit,
        'filtro_minimo' => 'Todos',
        'filtro_excluir' => '0',
        'prov_fech_desde' => '',
        'prov_fech_hasta' => '' ]); ?>
    </div>

  </div>
  <div class="col-lg-12" id="notificacion_actualiz_pro"></div>
  <div class="col-lg-12" id="notificacion_orden_pedido"></div>
  <div class="col-lg-12">
    <h3>Productos para nuevo pedido</h3>
    <p>Estos son los productos para un nuevo pedido, si desea devolver alguno marquelo y presione <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>.
      <span style="float:right">
          <a data-placement="top" style="cursor:pointer">
              <span onclick="obtener_productos_pedidos_regreso()" title="Devolver productos seleccionados" data-toggle="titulo" class="fa fa-arrow-circle-o-up fa-3x" aria-hidden="true"></span>
          </a>
      </span>
    </p>
    <div class="table-responsive tab-content" id="orden_compra_productos_pedido" style="width: 100%; overflow-x: auto; border: 2px solid #333;">
      <?= $this->render('orden_compra_productos_pedido', [ 'idProveedor' => $idProveedor ]); ?>
    </div><br>
  </div>
</div>

<!--area derecha-->
<div class="col-lg-4" id="estadistica_producto">
  <div class="panel panel-success">
    <center><h3>Esperando un producto<br><small>(Seleccione uno de los productos disponibles<br>al que desea consultar su estadística)</small><h3></center>
  </div>
</div>
<div class="col-lg-4"><hr style="background-color: red; height: 1px; border: 0;">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <center><strong>Generar Orden de Compra</strong></center>
    </div>
    <div class="panel-body">
      <div class="col-lg-12">
        <label>Prioridad del pedido:</label>
        <span style="float:right">
          <label class="radio-inline"><input type="radio" name="prioridad" id="baja" value="Baja">Baja</label>
          <label class="radio-inline"><input type="radio" name="prioridad" id="media" value="Media" checked>Media</label>
          <label class="radio-inline"><input type="radio" name="prioridad" id="alta" value="Alta">Alta</label>
        </span><br><br>
        <label>Contacto de pedido:</label>
        <?= Select2::widget([
                    'name' => '',
                    //'value' => '',
                    'data' => ArrayHelper::map(ContactosPedidos::find()->where(['=','codProveedores', $idProveedor])->all(), 'idContacto_pedido',
                        function($element) {
                        return $element['nombre'].' - '.$element['email'];
                    }),
                    'options' => [
                        'id'=>'idContacto_pedido',
                        'placeholder'=>'- Seleccione nombre del contacto -',
                        'onchange'=>'javascript:habilitarbtn()',
                        'multiple' => false //esto me ayuda a que solo se obtenga uno
                    ],
                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                ]) ?>
        <br>
        <label>Transporte:</label>
        <?= Select2::widget([
                    'name' => '',
                    //'value' => '',
                    'data' => ArrayHelper::map(Transporte::find()->where(['=','estado', 'activo'])->all(), 'id_transporte',
                        function($element) {
                        return $element['nombre_transporte'];
                    }),
                    'options' => [
                        'id'=>'idTransporte',
                        'placeholder'=>'- Seleccione el transporte a utilizar -',
                        'onchange'=>'javascript:habilitarbtn()',
                        'multiple' => false //esto me ayuda a que solo se obtenga uno
                    ],
                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                ]) ?>
        <br>
        <label>Observaciones del pedido:</label>
        <textarea id="observacion_pedido" rows="2" class="form-control" onkeyup="habilitarbtn()" onkeypress="if(this.value.length == 120){return false;}else{return toUpper(event,this);}"></textarea>
        <br>
        <a class="btn btn-success btn-lg btn-block" id="realizar_orden_compra" onclick="completar_compra(this)" data-loading-text="Espere, trámite en proceso..."><span class="fa fa-cart-plus" aria-hidden="true"></span> Realizar Orden de compra</a>
      </div>
    </div>
  </div>
</div>
