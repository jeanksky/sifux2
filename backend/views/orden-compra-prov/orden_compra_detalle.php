<?php
use backend\models\ProductoServicios;
use backend\models\ProductoProveedores;
?>
<div style="height: 400px; width: 100%; overflow-y: auto; border-left: thick solid #f2dede;">
  <table class="items table table-sm" id=""  >
          <thead >
            <tr class="danger">
              <th style="text-align:center">Código producto (Local)</th>
              <th style="text-align:center">Código producto (Proveedor)</th>
              <th style="text-align:center">Descripción</th>
              <th style="text-align:center">Cantidad sujerida</th>
              <th style="text-align:center">Cantidad de pedido</th>
            </tr>
          </thead>

  <?php
  foreach ($detalle_orden_compra as $productoser => $producto) {
    $productoser = ProductoServicios::find()->where(['codProdServicio'=>$producto['codProdServicio']])->one();
    $prod_prov = ProductoProveedores::find()->where(['=','idProd_prov', $producto['idProd_prov']])->one();
    echo '<tbody ><tr>
              <td align="center" >'.$producto['codProdServicio'].'</td>
              <td align="center" >'.$prod_prov->codigo_proveedor.'</td>
              <td align="center" >'.$productoser->nombreProductoServicio.'</td>
              <td align="center" >'.$producto['cantidad_sugerida'].'</td>
              <td align="center" >'.$producto['cantidad_pedir'].'</td>
            </tr></tbody>';
  }
   ?>
  </table>
</div>
