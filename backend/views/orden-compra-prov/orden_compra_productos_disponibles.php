<?php
use backend\models\ProductoProveedores;
use backend\models\ProductoServicios;
use backend\models\OrdenCompraSession;
use yii\helpers\Html;
use yii\helpers\Url;
$load_buton_ = \Yii::$app->request->BaseUrl.'/img/load.gif';
  $load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
  $cantidad_sugerida = 1;
  $promedio_sugerido = 0;
?>
<style media="screen">
  .tbody_ {
  display: block; /*obligado*/
  height: 300px; /*la que necesites*/
  width : 100%;
  overflow: auto; /*obligado*/
  }
  .thead_ {
    display: block; /*obligado*/
    /*width : 100%;*/
  }
  /*#tam_descr { width : 30%; }
  #tam_cantp { width : 20%; }*/
  .text {
  font-size:15px;
  font-family:helvetica;
  font-weight:bold;
  color:#E87B07;
  text-transform:uppercase;
}
.text2 {
font-size:13px;
font-family:helvetica;
font-weight:bold;
color:#FE2E64;
text-transform:uppercase;
}
.parpadea {

  animation-name: parpadeo;
  animation-duration: 0.5s;
  animation-timing-function: linear;
  animation-iteration-count: infinite;

  -webkit-animation-name:parpadeo;
  -webkit-animation-duration: 0.5s;
  -webkit-animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
}

@-moz-keyframes parpadeo{
  15% { opacity: 1.0; }
  20% { opacity: 0.0; }
  25% { opacity: 1.0; }
}

@-webkit-keyframes parpadeo {
  15% { opacity: 1.0; }
  20% { opacity: 0.0; }
   25% { opacity: 1.0; }
}

@keyframes parpadeo {
  15% { opacity: 1.0; }
   20% { opacity: 0.0; }
  25% { opacity: 1.0; }
}
</style>

<div id="div_productos_disponibles">
<table class="items table table-sm table-inverse" id="tabla_productos_disponibles" width="100%">
  <tbody>
    <!--tbody class="tbody_" id="tbody_pd"-->
    <?php
    if ($filtro_minimo == 'Todos') {
      $prod_proveedor = ProductoProveedores::find()->innerJoin('tbl_producto_servicios', 'tbl_producto_proveedores.codProdServicio = tbl_producto_servicios.codProdServicio')
      ->where(['=','tbl_producto_proveedores.codProveedores', $idProveedor])
      ->andWhere("tbl_producto_proveedores.estado = 'Disponible' AND tbl_producto_servicios.codFamilia NOT IN (".$filtro_excluir.")",[])
      ->orderBy(['tbl_producto_servicios.codFamilia' => SORT_ASC])
      ->orderBy(['tbl_producto_servicios.codProdServicio' => SORT_ASC]);
    } else if ($filtro_minimo == 'Cerca del mínimo') {
      $prod_proveedor = ProductoProveedores::find()->innerJoin('tbl_producto_servicios', 'tbl_producto_proveedores.codProdServicio = tbl_producto_servicios.codProdServicio')
      ->where(['=','tbl_producto_proveedores.codProveedores', $idProveedor])
      ->andWhere("tbl_producto_proveedores.estado = 'Disponible' AND tbl_producto_servicios.codFamilia NOT IN (".$filtro_excluir.") AND cantidadInventario <= (cantidadMinima + 3) AND cantidadInventario > cantidadMinima",[])
      ->orderBy(['tbl_producto_servicios.codFamilia' => SORT_ASC])
      ->orderBy(['tbl_producto_servicios.codProdServicio' => SORT_ASC]);
    } else if ($filtro_minimo == 'Igual al mínimo') {
      $prod_proveedor = ProductoProveedores::find()->innerJoin('tbl_producto_servicios', 'tbl_producto_proveedores.codProdServicio = tbl_producto_servicios.codProdServicio')
      ->where(['=','tbl_producto_proveedores.codProveedores', $idProveedor])
      ->andWhere("tbl_producto_proveedores.estado = 'Disponible' AND tbl_producto_servicios.codFamilia NOT IN (".$filtro_excluir.") AND cantidadInventario = cantidadMinima",[])
      ->orderBy(['tbl_producto_servicios.codFamilia' => SORT_ASC])
      ->orderBy(['tbl_producto_servicios.codProdServicio' => SORT_ASC]);
      } else if ($filtro_minimo == 'Menor al mínimo') {
        $prod_proveedor = ProductoProveedores::find()->innerJoin('tbl_producto_servicios', 'tbl_producto_proveedores.codProdServicio = tbl_producto_servicios.codProdServicio')
        ->where(['=','tbl_producto_proveedores.codProveedores', $idProveedor])
        ->andWhere("tbl_producto_proveedores.estado = 'Disponible' AND tbl_producto_servicios.codFamilia NOT IN (".$filtro_excluir.") AND cantidadInventario < cantidadMinima",[])
        ->orderBy(['tbl_producto_servicios.codFamilia' => SORT_ASC])
        ->orderBy(['tbl_producto_servicios.codProdServicio' => SORT_ASC]);
    }
    $cant_prod_suma = $limit + 10; $cant_prod = 0;
    $cant_prod_resta = $limit - 10;
    foreach($prod_proveedor->limit($limit)->all() as $position => $product) {
      $cant_prod += 1;
      $border = '';
      $codProdServicio = "'".$product['codProdServicio']."'"; $idinput = $product['codProdServicio'];
      //-------------------------------------------------------------------------------------------------
      //Obtemos la cantidad sujerida de este producto y la pronta sugerencia de pedido
      $inventario = ProductoServicios::findOne($product['codProdServicio']);
      $pronto_pedido = false;
      $dias = OrdenCompraSession::getDiasrendimiento();
      if ($dias==null) {
        $dias = $inventario->cantidad_dias_produccion == null ? 1 : $inventario->cantidad_dias_produccion;
      }
      $fehas = date('Y-m-d');
      $fecharestada = strtotime ( '-'.$dias.' day' , strtotime ( $fehas ) ) ;
      $fedes = date ( 'Y-m-j' , $fecharestada );
      //si exite marcado el check para usar las fechas de movimiento se usan
      //estas fechas para otorgar el rango de los dias en rendimiento
      if (OrdenCompraSession::getFechasrendimiento()=='si') {
        $fedes = date('Y-m-j', strtotime( $prov_fech_desde ));
        $fehas = date('Y-m-j', strtotime( $prov_fech_hasta ));
        $fedes_d = date('j', strtotime( $prov_fech_desde ));
        $fedes_m = date('m', strtotime( $prov_fech_desde ));
        $fedes_a = date('Y', strtotime( $prov_fech_desde ));
        $fehas_d = date('j', strtotime( $prov_fech_hasta ));
        $fehas_m = date('m', strtotime( $prov_fech_hasta ));
        $fehas_a = date('Y', strtotime( $prov_fech_hasta ));
        //calculo timestam de las dos fechas
        $timestamp1 = mktime(0,0,0,$fedes_m,$fedes_d,$fedes_a);
        $timestamp2 = mktime(0,0,0,$fehas_m,$fehas_d,$fehas_a);
        //resto a una fecha la otra
        $segundos_diferencia = $timestamp1 - $timestamp2; //echo $segundos_diferencia;
        //convierto segundos en días
        $dias = $segundos_diferencia / (60 * 60 * 24);
        //obtengo el valor absoulto de los días (quito el posible signo negativo)
        $dias = abs($dias);
        //quito los decimales a los días de diferencia
        $dias = floor($dias);
      }
      $dias = $dias / 100; //si el rendimiento no es por día sino mensual se incluye esta formula. (si fuera super mercado se podría quitar)
      //obtenemos la suma de la cantidad del producto vendido en el rango de fecha
      /*$sql_vendidos = "SELECT SUM(tbl_detalle_facturas.`cantidad`) AS cantidad_venta
      FROM tbl_encabezado_factura INNER JOIN tbl_detalle_facturas
      ON tbl_detalle_facturas.`idCabeza_factura` = tbl_encabezado_factura.`idCabeza_Factura`
      WHERE tbl_detalle_facturas.`codProdServicio`= :codProdServicio
      AND tbl_encabezado_factura.`estadoFactura` IN ('Cancelada','Crédito')
      AND tbl_encabezado_factura.`fecha_inicio` BETWEEN :fecha_desde AND :fecha_hasta";
          $command_v = \Yii::$app->db->createCommand($sql_vendidos);
          $command_v->bindParam(":codProdServicio", $idinput);
          $command_v->bindParam(":fecha_desde", $fedes);
          $command_v->bindParam(":fecha_hasta", $fehas);
          $result_vendidos = $command_v->queryOne();*/
      //obtenemos la suma mayor y menor de la cantidad del producto en el rango de fecha
    /*  $sql_minmax_vendidos = "SELECT MAX(v.`cantidad_venta`) AS venta_max, MIN(v.`cantidad_venta`) AS venta_min
      FROM (SELECT tbl_encabezado_factura.`fecha_inicio`, SUM(tbl_detalle_facturas.`cantidad`)
      AS cantidad_venta FROM tbl_encabezado_factura
      INNER JOIN tbl_detalle_facturas
      ON tbl_detalle_facturas.`idCabeza_factura` = tbl_encabezado_factura.`idCabeza_Factura`
      WHERE tbl_detalle_facturas.`codProdServicio`= :codProdServicio
      AND tbl_encabezado_factura.`estadoFactura` IN ('Cancelada','Crédito')
      AND tbl_encabezado_factura.`fecha_inicio` BETWEEN :fecha_desde AND :fecha_hasta
      GROUP BY tbl_encabezado_factura.`fecha_inicio`) v";
      $command_min_man_v = \Yii::$app->db->createCommand($sql_minmax_vendidos);
      $command_min_man_v->bindParam(":codProdServicio", $idinput);
      $command_min_man_v->bindParam(":fecha_desde", $fedes);
      $command_min_man_v->bindParam(":fecha_hasta", $fehas);
      $consulta_vendidos = $command_min_man_v->queryOne();*/
      /*$cmx = $consulta_vendidos['venta_max']; //Cantidad del día que más se consumio de este producto
      $cmn = $consulta_vendidos['venta_min']; //Cantidad del día que menos se consumio de este producto
      //recorremos el ciclo para comprovar la cantidad de venta mas alta y mas vaja

      $cp = $result_vendidos['cantidad_venta'] / $dias; //consumo promedio por día en el rango de días
      $e = $producto->cantidadInventario; //Existencia actual
      $emn = $cmn * $dias; //Existencia mínima (Inventario de seguridad)
      $emx = ( $cmx * $dias ) + $emn; //Existencia máxima
      $pp = ( $cp * $dias ) + $emn; //punto de pedido (cuando se llegue a esta cantidad se sujiere que se pida)
      $cantidad_sugerida = $emx - $e; //Cantidad sujerida de pedido
      if ($e <= $pp ) {//si la existencia actual es menor al punto de pedido
        $pronto_pedido = true;
      }*/
      $descripcion_producto = $inventario->nombreProductoServicio;
      /*if ($pronto_pedido == true) { //el producto parpada cuando esta en pedido
        $descripcion_producto = '<span class="parpadea text"><strong>'.$producto->nombreProductoServicio.'</strong></span>';
      }*/
    /*  $promedio_sugerido = $cantidad_sugerida <= 0 ? 0 : $cantidad_sugerida;//el promedio solo me muestra el aproximado
      //luego compruebo que si es mayor o igual me redondé el valor a pedir hacia arriba de lo contrario hacia abajo
      $cantidad_comprovada = $cantidad_sugerida >= (intval($cantidad_sugerida) + 0.5) ? ceil($promedio_sugerido) : floor($promedio_sugerido);
      $cantidad_sugerida = $cantidad_sugerida <= 0 ? 0 : $cantidad_comprovada;//y con esto obtengo la cantidad sujerida
      */
      //-------------------------------------------------------------------------------------------------
      //muestra en colores los productos por la diferencia de la cantidad en einventario con el minimo
      if($inventario->cantidadInventario < $inventario->cantidadMinima){
          $border = 'red 10px solid;';
          }
      if($inventario->cantidadInventario == $inventario->cantidadMinima){
          $border = 'orange 10px solid;';
      }
      $cantidad_cercana = $inventario->cantidadMinima + 3;
      if($inventario->cantidadInventario > $inventario->cantidadMinima && $inventario->cantidadInventario <= $cantidad_cercana){
          $border = 'blue 10px solid;';
      }
      $cantidad_pedir = '<div class="input-group col-xs-12">
                          <span class="input-group-btn">
                              <button class="btn btn-success btn-sm" onclick="asignarcantidadsujerida('.$cantidad_sugerida.', '.$codProdServicio.')" data-placement="top" title="Agrega cantidad sujerida a cantidad a pedir" type="button" ><span class="fa fa-arrow-right"></span></button>
                          </span>
                        '.
                          Html::input('text', '', '', ['class' => 'form-control input-sm', 'id' => $idinput, 'style'=>'font-family: Fantasy; font-size: 18pt; text-align: center;', 'onkeypress'=>'return isNumber(event)', 'onkeyup'=>'javascript:agrega_producto_pedido(event.keyCode,"'.$idinput.'",'.$cantidad_sugerida.','.$product['idProd_prov'].', '.$limit.');'])
                       .'</div>';
      $actualizar_producto = Html::a('', '#', [
                     'class'=>'fa fa-pencil fa-2x',
                     'id' => 'activity-index-link-actualizaprod-inv',
                     'data-toggle' => 'modal',
                     'data-target' => '#modalActualiprodinv',
                     'data-url' => Url::to(['actualizar_producto_modal', 'id' => $product['codProdServicio']]),
                     'data-pjax' => '0',
                     'title' => Yii::t('app', 'Procede a modificar el producto')]);
     $codigo_proveedor = Html::a('Cod.Prov', null, [
                   //'class'=>'fa-2x',
                   'style'=>'cursor:pointer;',
                   'data-content' => $product['codigo_proveedor'],
                   'data-toggle' => 'popover',
                   'title' => Yii::t('app', 'Código de proveedor'),
                 ]);
    ?>
    <tr style = "border-left: <?= $border ?>; border-right: ; cursor:pointer; " title="<?= $descripcion_producto ?>" onclick="marcarproducto(this, '<?= $product["codProdServicio"] ?>', <?= $idProveedor ?>)">
      <td align="left" width="20%"><font face="arial" size=2><?= $product['codProdServicio'].' ('.$inventario->codFamilia.')'/*.'<br>'.$fedes.' '.$fehas.'<br>'.$dias*/ ?></font></td>
      <td align="left" width="10%"><font face="arial" size=2><?=  $codigo_proveedor/*'max:'.$cmx . ' Min:'.$cmn */?></font></td>
      <td align="left" width="35%"><font face="arial" size=2><?= substr($descripcion_producto,0,30) ?></font></td>
      <td align="center" width="5%"><font face="arial" size=5><span class="label label-default"><?= $inventario->cantidadInventario ?></span></font></td>
      <td align="center" width="5%"><font face="arial" size=5><span class="label label-warning"><?= $inventario->cantidadMinima ?></span></font></td>
      <td align="center" width="5%"><font face="arial" size=5><span class="label label-info"><?= $promedio_sugerido ?></span></font></td>
      <td align="right" width="15%"><?= $cantidad_pedir ?></td>
      <td align="right" width="5%" class="actions button-column"><?= $actualizar_producto ?></td>
    </tr>
    <?php
    }//fin foreach
      if ($cant_prod == $prod_proveedor->count()) {
        $titulo_btn_mas_prod = 'Todos los productos mostrados ('.$cant_prod .' de '.$prod_proveedor->count().')';
      } else {
        $titulo_btn_mas_prod = 'Mostrar más productos ('.$cant_prod .' de '.$prod_proveedor->count().')';
      }
    ?>
  <tr>
    <td colspan="8" style="text-align:center">
      <a class="btn btn-default btn-lg" onclick="mostrar_mas_productos(<?= $cant_prod_suma ?>)" ><?= $titulo_btn_mas_prod ?></a>
    </td>
  </tr>
  </tbody>
</table>
</div>
<div id="actualizar_producto"></div>
<!--div id="final">Activo</div>
<div class="">
  <center><img src="<?= $load_buton_ ?>" style="height: 300px;"></center>
</div-->
<script type="text/javascript">
const MARGEN = .1;

$(document).ready(function(){
        $('[data-toggle="popover"]').popover();

// EVENTO CUANDO SE MUEVE EL SCROLL, EL MISMO APLICA TAMBIEN CUANDO SE RESIZA
var scroll = $('#orden_compra_productos_disponibles');
var div_pd = $('#div_productos_disponibles');

scroll.scroll(function(){
    var posicion_scroll = scroll.scrollTop(); // VALOR QUE SE HA MOVIDO DEL SCROLL
    var contenido2 = document.getElementById("final");
    if (MARGEN == $(div_pd).height() - posicion_scroll - $(scroll).height() ) { // SI EL SCROLL HA SUPERADO EL ALTO DE TU DIV
      alert( $(div_pd).height() +' '+ posicion_scroll + ' '+ $(scroll).height() + ' ' + MARGEN/*($(div_pd).height() - $(scroll).height())*/)
      //console.log($(div_pd).height() +' '+ posicion_scroll + ' '+ $(scroll).height() + ' ' + MARGEN);
      //mostrar_mas_productos('<?= $cant_prod_suma ?>');
    } else {
       //mostrar_mas_productos('<?= $cant_prod_resta ?>');
    }
});
    });
</script>
