<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenCompraProv */

$this->title = 'Update Orden Compra Prov: ' . ' ' . $model->idOrdenCompra;
$this->params['breadcrumbs'][] = ['label' => 'Orden Compra Provs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idOrdenCompra, 'url' => ['view', 'id' => $model->idOrdenCompra]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="orden-compra-prov-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
