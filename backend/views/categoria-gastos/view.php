<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\AlertBlock; //para mostrar la alerta
/* @var $this yii\web\View */
/* @var $model backend\models\CategoriaGastos */

$this->title = $model->descripcionCategoriaGastos;
$this->params['breadcrumbs'][] = ['label' => 'Categoria Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoria-gastos-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idCategoriaGastos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idCategoriaGastos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar este artículo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCategoriaGastos',
            'descripcionCategoriaGastos',
            'estadoCategoriaGastos',
        ],
    ]) ?>

</div>
