<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock; //para mostrar la alerta

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CategoriaGastosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categoria Gastos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoria-gastos-index">

<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear Categoria Gastos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idCategoriaGastos',
            'descripcionCategoriaGastos',
            'estadoCategoriaGastos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>
