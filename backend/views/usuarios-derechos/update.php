<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('body').on('click', '.refin', function(e) {
        $("#refrescar").load(location.href+' #refrescar','');
    });
    $('body').on('click', '.reffa', function(e) {
        $("#refrescar").load(location.href+' #refrescar','');
    });
});
</script>
<div class="col-md-12">
<?php

use yii\helpers\Html;
use kartik\icons\Icon;
//Icon::map($this);

/* @var $this yii\web\View */
/* @var $model backend\models\UsuariosDerechos */

// $this->title = 'Actualizar derechos de usuario para: ' . ' ' . $model->nombre;
$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';

?>
<div class="usuarios-derechos-update">

   <div class="col-md-12"> <h3><?= Html::encode($this->title) ?></h3></div>

    <?= $this->render('_form', [
        'model' => $model,
      //  'tipoAsercaDe' => $tipoAsercaDe,
        'tipoClitI' => $tipoClitI, 'tipoClitC' => $tipoClitC, 'tipoClitU' => $tipoClitU,
        'tipoClitD' => $tipoClitD, 'tipoClitV' => $tipoClitV,
        'tipoFunI' => $tipoFunI, 'tipoFunC' => $tipoFunC, 'tipoFunU' => $tipoFunU,
        'tipoFunD' => $tipoFunD, 'tipoFunV' => $tipoFunV,
        'tipoMecI' => $tipoMecI, 'tipoMecC' => $tipoMecC, 'tipoMecU' => $tipoMecU,
        'tipoMecD' => $tipoMecD, 'tipoMecV' => $tipoMecV,
        'tipoMarI' => $tipoMarI, 'tipoMarC' => $tipoMarC, 'tipoMarU' => $tipoMarU,
        'tipoMarD' => $tipoMarD, 'tipoMarV' => $tipoMarV,
        'tipoModI' => $tipoModI, 'tipoModC' => $tipoModC, 'tipoModU' => $tipoModU,
        'tipoModD' => $tipoModD, 'tipoModV' => $tipoModV,
        'tipoVeI' => $tipoVeI, 'tipoVeC' => $tipoVeC, 'tipoVeU' => $tipoVeU,
        'tipoVeD' => $tipoVeD, 'tipoVeV' => $tipoVeV,
        'tipoFaI' => $tipoFaI, 'tipoFaC' => $tipoFaC, 'tipoFaU' => $tipoFaU,
        'tipoFaD' => $tipoFaD, 'tipoFaV' => $tipoFaV,
        'tipoSeI' => $tipoSeI, 'tipoSeC' => $tipoSeC, 'tipoSeU' => $tipoSeU,
        'tipoSeD' => $tipoSeD, 'tipoSeV' => $tipoSeV,
        'tipoPrI' => $tipoPrI, 'tipoPrC' => $tipoPrC, 'tipoPrU' => $tipoPrU,
        'tipoPrD' => $tipoPrD, 'tipoPrV' => $tipoPrV,
        'tipoPI' => $tipoPI, 'tipoPC' => $tipoPC, 'tipoPU' => $tipoPU,
        'tipoPD' => $tipoPD, 'tipoPV' => $tipoPV,
        'tipoOrSeI' => $tipoOrSeI, 'tipoOrSeC' => $tipoOrSeC, 'tipoOrSeU' => $tipoOrSeU,
        'tipoOrSeD' => $tipoOrSeD, 'tipoOrSeV' => $tipoOrSeV,
        'tipoVerI' => $tipoVerI, 'tipoVerC' => $tipoVerC, 'tipoVerU' => $tipoVerU,
        'tipoVerD' => $tipoVerD, 'tipoVerV' => $tipoVerV,
        'tipoComI' => $tipoComI, 'tipoComC' => $tipoComC, 'tipoComU' => $tipoComU,
        'tipoComD' => $tipoComD, 'tipoComV' => $tipoComV,
        'tipoCueB'=>$tipoCueB,
        'tipoTPI'=>$tipoTPI, 'tipoTPMo'=> $tipoTPMo ,'tipoTPCP'=>$tipoTPCP,
        'tipoTipmI' => $tipoTipmI, 'tipoTipmC' => $tipoTipmC, 'tipoTipmU' => $tipoTipmU,
        'tipoTipmD' => $tipoTipmD, 'tipoTipmV' => $tipoTipmV,
        'tipoMovI' => $tipoMovI, 'tipoMovC' => $tipoMovC, 'tipoMovU' => $tipoMovU,
        'tipoMovD' => $tipoMovD, 'tipoMovA' => $tipoMovA,
        'tipoFacD' => $tipoFacD,
        'tipoDevPro' => $tipoDevPro,
        'tipoMovCreI'=>$tipoMovCreI, 'tipoMovCreA'=>$tipoMovCreA,
        'tipoMovCreNC' => $tipoMovCreNC, 'tipoMovCreCF' => $tipoMovCreCF,
        'tipoRes_doXML' => $tipoRes_doXML,
        'tipoOrComI' => $tipoOrComI, 'tipoOrComC' => $tipoOrComC,
        'tipoTranspI' => $tipoTranspI, 'tipoTranspC' => $tipoTranspC,
        'tipoTranspU' => $tipoTranspU, 'tipoTranspD' => $tipoTranspD,
        'tipoLibB' => $tipoLibB,
        'tipoCaPa' => $tipoCaPa, 'tipoCoPa' => $tipoCoPa,
        'tipoProf_vt' => $tipoProf_vt, 'tipoProf_c' => $tipoProf_c,
        'tipoProf_u' => $tipoProf_u,
        'tipoprefa_vt' => $tipoprefa_vt, 'tipoprefa_c' => $tipoprefa_c,
        'tipoprefa_u' => $tipoprefa_u, 'tipoprefa_d' => $tipoprefa_d,
        'tipocaja_vt' => $tipocaja_vt, 'tipocaja_dev' => $tipocaja_dev,
        'tipocaja_can' => $tipocaja_can, 'tipocaja_rev' => $tipocaja_rev,
        'tipocategas_vt' => $tipocategas_vt, 'tipocategas_c' => $tipocategas_c,
        'tipocategas_u' => $tipocategas_u, 'tipocategas_d' => $tipocategas_d,
        'tipotipgas_vt' => $tipotipgas_vt, 'tipotipgas_c' => $tipotipgas_c,
        'tipotipgas_u' => $tipotipgas_u, 'tipotipgas_d' => $tipotipgas_d,
        'tipoprovgas_vt' => $tipoprovgas_vt, 'tipoprovgas_c' => $tipoprovgas_c,
        'tipoprovgas_u' => $tipoprovgas_u, 'tipoprovgas_d' => $tipoprovgas_d,
        'tiporegistgas_d' => $tiporegistgas_d,
        'tiporbancos_m' => $tiporbancos_m, 'tiporcaja_m' => $tiporcaja_m,
        'tiporcompras_m' => $tiporcompras_m, 'tiporinventario_m' => $tiporinventario_m,
        'tipoorden_servicio_m' => $tipoorden_servicio_m, 'tipoproveedores_m' => $tipoproveedores_m,
        'tipousuarios_m' => $tipousuarios_m,
        'tipomarpro' => $tipomarpro
    ]) ?>

    <!--?= $this->render('_formDerechos', [
        'model' => $model,
    ]) ?-->

</div></div>
