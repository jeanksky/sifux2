<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\UsuariosDerechos */

$this->title = 'Create Usuarios Derechos';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Derechos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarios-derechos-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'tipoOperaciones' => $tipoOperaciones
    ]) ?>

</div>
