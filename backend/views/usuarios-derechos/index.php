<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\UsuariosDerechos;//Para el filtro
use backend\models\Funcionario;

//use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;

use kartik\widgets\Growl;
//use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsuariosDerechosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//https://www.youtube.com/watch?v=7u0p8fKriy0
//http://demos.krajee.com/grid#editable-column
//http://demos.krajee.com/grid-demo#comments

$this->title = 'Usuarios "Asignación de derechos"';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarios-derechos-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <p align="center">Para ver o para editar los derechos de cada usuario seleccione la
    opción que está a la derecha de cada usuario listado en la tabla</p></br>
    <?php //echo UsuariosDerechosSearch::getTipo(); // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <!--?= // Html::dropDownList('price_type_id', null, UsuariosDerechos::getTipo()) ?-->
    <!--p-->
        <!--?= Html::a('Create Usuarios Derechos', ['create'], ['class' => 'btn btn-success']) ?-->
    <!--/p-->
    <div class="table-responsive">
    <div id="semitransparente">
              <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
    <?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <div id="semitransparente">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,'filterSelector' => 'select[name="per-page"]',    
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            'nombre',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
             'email:email',
            //'status',
            //'created_at',
            //'updated_at',
            //'nombre',
            // 'codigo',
            // 'tipo_usuario',
            //['attribute' => 'tipo_usuario', 'filter'=>Html::dropDownList(UsuariosDerechos::getTipoEstado(),['prompt' => ''])],
            // ['attribute' => 'tipo_usuario'],
             //https://www.youtube.com/watch?v=OLfz7Iy_y84
             [
              'attribute' => 'tipo_usuario',
              'value' => 'tipoF.tipo_usuario',
              'filter'=>Html::activeDropDownList($searchModel, 'tipo_usuario',
                ArrayHelper::map(Funcionario::find()
                ->asArray()
                ->all(),'tipo_usuario','tipo_usuario'),['class' => 'form-control','prompt' => 'Todos'])
            ],
//             SELECT * FROM USER WHERE tipo_usuario NOT IN ( SELECT tipo_usuario FROM USER
// WHERE tipo_usuario = 'Cliente' )

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}',
                'buttons'=>[
                              'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                        'title' => Yii::t('yii', 'Agregar o quitar permisos'),
                                ]);

                              }
                          ]
            ],
        ],
    ]); ?>

<?php
  /*  echo GridView::widget([
    'id' => 'kv-grid-demo',
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'columns'=>[
        ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            'nombre'
    ],
    'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar'=> [
        ['content'=>
            Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>'', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>''])
        ],
        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export'=>[
        'fontAwesome'=>true
    ],
    // parameters from the demo form
    'bordered'=>true,
    'striped'=>true,
    'condensed'=>true,
    'responsive'=>true,
    'hover'=>true,
    //'showPageSummary'=>$pageSummary,
    'panel'=>[
        //'type'=>GridView::TYPE_PRIMARY,
        //'heading'=>$heading,
    ],
    'persistResize'=>false,
    //'exportConfig'=>$exportConfig,
]);*/
?>
    </div>

</div>
</div>
</div>
