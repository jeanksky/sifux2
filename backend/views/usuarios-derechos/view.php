
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\UsuariosDerechos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios "Asignación de derechos"', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="col-md-12">
<div class="usuarios-derechos-view">
<div class="col-md-12">
<h3><strong>Gestión de Usuarios</strong></h3>
<h3><?= Html::encode($this->title) ?></h3> </div>

        <br>
    <div class="col-md-3">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
            ],
        ]) ?>
    </div>
    <div class="col-md-3">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'tipo_usuario',
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
    <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar Permisos', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p></div>
        <!-- <p>
            ?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?
        </p>   -->
 <!--  <h3>Índice de permisos permitidos</h3> -->
        <div class="col-md-12">
        <?php
       //  $Ade = null;
         $C_vt = null; $C_c = null; $C_u = null; $C_e = null; $C_v = null; //Cliente
         $F_vt = null; $F_c = null; $F_u = null; $F_e = null; $F_v = null; //Funcionario
         $M_vt = null; $M_c = null; $M_u = null; $M_e = null; $M_v = null; //Mecanico
         $Ma_vt = null; $Ma_c = null; $Ma_u = null; $Ma_e = null; $Ma_v = null; //Marca
         $Mo_vt = null; $Mo_c = null; $Mo_u = null; $Mo_e = null; $Mo_v = null; //Modelo
         $Ver_vt = null; $Ver_c = null; $Ver_u = null; $Ver_e = null; $Ver_v = null; //Versión
         $Ve_vt = null; $Ve_c = null; $Ve_u = null; $Ve_e = null; $Ve_v = null; //Vehiculo
         $Comp_vt = null; $Comp_c = null; $Comp_u = null;//Cómpras
         $Fa_vt = null; $Fa_c = null; $Fa_u = null; $Fa_e = null; $Fa_v = null; //Familia
         $Se_vt = null; $Se_c = null; $Se_u = null; $Se_e = null; $Se_v = null; //Servicios
         $Pr_vt = null; $Pr_c = null; $Pr_u = null; $Pr_e = null; $Pr_v = null; //Proveedores
         $P_vt = null; $P_c = null; $P_u = null; $P_e = null; $P_v = null; //Productos
         $MarPro = null;
         $OrSe_vt = null; $OrSe_c = null; $OrSe_u = null; $OrSe_e = null; $OrSe_v = null; //Orden de Productos
         $DevPro = null;
         $Caj_vt = $Caj_dev = $Caj_can = $Caj_rev = null; /*caja*/
         $FacDia_vt = null;/*facturas dia*/
         $TraPag_con_mod = $TraPag_crear_mov = $TraPag_pago_pro = null; /*tramite pago a proveedores*/

        foreach ($model->operacionesPermitidasList as $operacionPermitida) {
            //echo $operacionPermitida['nombre'] . " | ";
         //   if ($operacionPermitida['nombre'] == 'Acerca de') {$Ade = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'clientes-index') {$C_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'clientes-create') {$C_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'clientes-update') {$C_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'clientes-delete') {$C_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'clientes-view') {$C_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'funcionario-index') {$F_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'funcionario-create') {$F_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'funcionario-update') {$F_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'funcionario-delete') {$F_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'funcionario-view') {$F_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'mecanicos-index') {$M_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'mecanicos-create') {$M_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'mecanicos-update') {$M_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'mecanicos-delete') {$M_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'mecanicos-view') {$M_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'marcas-index') {$Ma_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'marcas-create') {$Ma_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'marcas-update') {$Ma_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'marcas-delete') {$Ma_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'marcas-view') {$Ma_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'modelo-index') {$Mo_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'modelo-create') {$Mo_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'modelo-update') {$Mo_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'modelo-delete') {$Mo_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'modelo-view') {$Mo_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'version-index') {$Ver_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'version-create') {$Ver_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'version-update') {$Ver_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'version-delete') {$Ver_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'version-view') {$Ver_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'compras-inventario-index') { $Comp_vt =  $operacionPermitida['nombre']; }
            if ($operacionPermitida['nombre'] == 'compras-inventario-create') { $Comp_c =  $operacionPermitida['nombre']; }
            if ($operacionPermitida['nombre'] == 'compras-inventario-update') { $Comp_u =  $operacionPermitida['nombre']; }

            if ($operacionPermitida['nombre'] == 'vehiculos-index') {$Ve_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'vehiculos-create') {$Ve_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'vehiculos-update') {$Ve_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'vehiculos-delete') {$Ve_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'vehiculos-view') {$Ve_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'familia-index') {$Fa_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'familia-create') {$Fa_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'familia-update') {$Fa_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'familia-delete') {$Fa_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'familia-view') {$Fa_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'servicios-index') {$Se_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'servicios-create') {$Se_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'servicios-update') {$Se_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'servicios-delete') {$Se_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'servicios-view') {$Se_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'proveedores-index') {$Pr_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'proveedores-create') {$Pr_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'proveedores-update') {$Pr_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'proveedores-delete') {$Pr_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'proveedores-view') {$Pr_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'producto-servicios-index') {$P_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'producto-servicios-create') {$P_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'producto-servicios-update') {$P_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'proveedores-delete') {$P_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'proveedores-view') {$P_v = $operacionPermitida['nombre'];}

            if ($operacionPermitida['nombre'] == 'orden-servicio-index') {$OrSe_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'orden-servicio-create') {$OrSe_c = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'orden-servicio-update') {$OrSe_u = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'orden-servicio-delete') {$OrSe_e = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'orden-servicio-view') {$OrSe_v = $operacionPermitida['nombre'];}
            /*devolución de porductos*/
            if ($operacionPermitida['nombre'] == 'devoluciones-producto-index') {$DevPro = $operacionPermitida['nombre'];}
            /*marcas producto*/
            if ($operacionPermitida['nombre'] == 'marcas-producto-index') {$MarPro = $operacionPermitida['nombre'];}
            /*facturas dia*/
            if ($operacionPermitida['nombre'] == 'facturas-dia-index') {$FacDia_vt = $operacionPermitida['nombre'];}
            /*caja*/
            if ($operacionPermitida['nombre'] == 'cabeza-caja-index') {$Caj_vt = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'cabeza-prefactura-devolverfactura') {$Caj_dev = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'cabeza-caja-cancelar-factura') {$Caj_can = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'cabeza-caja') {$Caj_rev = $operacionPermitida['nombre'];}
            /*tramite pagos proveedor*/
            if ($operacionPermitida['nombre'] == 'tramite-pago-index') {$TraPag_con_mod = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'tramite-pago-movimientos') {$TraPag_crear_mov = $operacionPermitida['nombre'];}
            if ($operacionPermitida['nombre'] == 'tramite-pago-completar_pago') {$TraPag_pago_pro = $operacionPermitida['nombre'];}
        }
        ?>
    <div class="col-lg-6">
      <table class = "table table-bordered">

        <!--tr>
          <td style="text-align:center"><strong>Página</strong></td>
          <td style="text-align:center"><?php //if ($Ade != null) echo $Ade; else echo ''; ?></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr-->

<!--         <tr>
          <td style="text-align:center"><strong>Gestion de Usuarios</strong></td>
          <td colspan="5"><center>Solo Admnistrador</center></td>
        </tr>
          -->
        <tr>
          <td class="success"><strong>Módulo de Clientes</strong></td>
          <td style="text-align:center"><?php if ($C_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($C_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($C_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($C_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($C_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Funcionarios</strong></td>
          <td style="text-align:center"><?php if ($F_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($F_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($F_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($F_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($F_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Mecánico</strong></td>
          <td style="text-align:center"><?php if ($M_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($M_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Módulo de Marca</strong></td>
          <td style="text-align:center"><?php if ($Ma_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ma_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ma_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ma_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ma_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Modelo</strong></td>
          <td style="text-align:center"><?php if ($Mo_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Mo_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Mo_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Mo_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Mo_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Versión vehicular</strong></td>
          <td style="text-align:center"><?php if ($Ver_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ver_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ver_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ver_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ver_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Vehículos</strong></td>
          <td style="text-align:center"><?php if ($Ve_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ve_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ve_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ve_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Ve_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Cómpras</strong></td>
          <td style="text-align:center"><?php if ($Comp_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Comp_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Comp_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Trámite de pagos a proveedores</strong></td>
          <td style="text-align:center"><?php if ($TraPag_con_mod != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($TraPag_crear_mov != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($TraPag_pago_pro != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
        </tr>

        <tr>
          <td class="success"><strong>Bancos</strong></td>
          <td style="text-align:center"></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Módulo de Categoría de producto/Servicio</strong></td>
          <td style="text-align:center"><?php if ($Fa_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Fa_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Fa_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Fa_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Fa_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Servicio</strong></td>
          <td style="text-align:center"><?php if ($Se_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Se_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Se_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Se_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Se_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Proveedores</strong></td>
          <td style="text-align:center"><?php if ($Pr_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Pr_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Pr_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Pr_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Pr_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Módulo de Productos</strong></td>
          <td style="text-align:center"><?php if ($P_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($P_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($P_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($P_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($P_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>
        <tr>
          <td class="success"><strong>Módulo de Marcas de Producto</strong></td>
          <td style="text-align:center"><?php if ($MarPro != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Tipo movimientos</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>

        <tr>
          <td class="success"><strong>Movimiento a inventario</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Módulo de Orden de Servicio</strong></td>
          <td style="text-align:center"><?php if ($OrSe_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($OrSe_c != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($OrSe_u != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($OrSe_e != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($OrSe_v != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo '';?></td>
        </tr>

        <tr>
          <td class="success"><strong>Facturas proforma</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>

        <tr>
          <td class="success"><strong>Pre-factura</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Caja</strong></td>
          <td style="text-align:center"><?php if ($Caj_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Caj_dev != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Caj_can != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
          <td style="text-align:center"><?php if ($Caj_rev != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
        </tr>

        <tr>
          <td class="success"><strong>Facturas diarias</strong></td>
          <td style="text-align:center"><?php if ($FacDia_vt != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
        </tr>
        <tr>
          <td class="success"><strong>Devolución Productos</strong></td>
          <td style="text-align:center"><?php if ($DevPro != null) echo '<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i>'; else echo ''; ?></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Cuenta por cobrar</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Categoría de gastos</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>
        <tr>
          <td class="success"><strong>Tipo Gastos</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>
        <tr>
          <td class="success"><strong>Proveedores gastos</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>
        <tr>
          <td class="success"><strong>Registro Gastos</strong></td>
          <td style="text-align:center"></td>
        </tr>
      </table>
    </div>
    <div class="col-lg-6">
      <table class = "table table-bordered">
        <tr>
          <td class="success"><strong>Reporte</strong></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"></td>
        </tr>
      </table>
    </div>
  </div>

</div>
</div>
