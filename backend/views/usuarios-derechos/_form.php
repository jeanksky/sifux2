<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
//use kartik\widgets\ActiveForm;
//$form = ActiveForm::begin();
/* @var $this yii\web\View */
/* @var $model backend\models\UsuariosDerechos */
/* @var $form yii\widgets\ActiveForm */
$checkbox_permisos = \Yii::$app->request->BaseUrl.'/css/checkbox_permisos.css';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//$(':checkbox').checkboxpicker();
function ma_clientes() { $('#fila_clientes').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_funcionario() { $('#fila_funcionarios').find('input[type="checkbox"]').prop('checked', true); }
function ma_mecanico() { $('#fila_mecanicos').find('input[type="checkbox"]').prop('checked', true); }
//----------------------------------------------------
function ma_marcas() { $('#fila_marcas').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_modelos() { $('#fila_modelos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_versionvehicular() { $('#fila_versionvehicular').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_vehiculos() { $('#fila_vehiculos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
//----------------------------------------------------
function ma_transportes() { $('#fila_transportes').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_or_compras() { $('#fila_ord_compras').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_compras() { $('#fila_compras').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_tramitepagos() { $('#fila_tramitepagos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_canceltramitepagos() { $('#fila_canceltramitepagos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_aplictramitepagos() { $('#fila_aplictramitepagos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_bancos() { $('#fila_bancos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_libros() { $('#fila_libros').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
//----------------------------------------------------
function ma_categorias() { $('#fila_categorias').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_servicios() { $('#fila_servicios').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_proveedores() { $('#fila_proveedores').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_productos() { $('#fila_productos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_marcaproducto() { $('#fila_marcaproducto').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
//----------------------------------------------------
function ma_tipomovimiento() { $('#fila_tipomovimiento').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_moviminventario() { $('#fila_moviminventario').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
//----------------------------------------------------
function ma_ordenservicio() { $('#fila_ordenservicio').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_facturasdiarias() { $('#fila_facturasdiarias').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_devolucionproducto() { $('#fila_devolucionproducto').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_cuentascobrar() { $('#fila_cuentascobrar').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_recepcion() { $('#fila_recepcion').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
//----------------------------------------------------
function ma_categoriagastos() { $('#fila_categoriagastos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_tipogastos() { $('#fila_tipogastos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_proveegastos() { $('#fila_proveegastos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_regisgastos() { $('#fila_regisgastos').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
//----------------------------------------------------
function ma_proforma() { $('#fila_proforma').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_prefactura() { $('#fila_prefactura').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
function ma_facturacion() { $('#fila_facturacion').find( 'input[type="checkbox"]' ).prop( "checked", true ); }
//----------------------------------------------------
function ma_reporte() { $('#fila_reporte').find( 'input[type="checkbox"]' ).prop( "checked", true ); }


</script>
<link href="<?= $checkbox_permisos ?>" rel="stylesheet">
<style>
/*
h1 {
    color: #eee;
    font: 30px Arial, sans-serif;
    -webkit-font-smoothing: antialiased;
    text-shadow: 0px 1px black;
    text-align: center;
    margin-bottom: 50px;
}*/

input[type=checkbox] {
   /* visibility: hidden;*/
    zoom:1.8;
}


</style>

<div class="usuarios-derechos-form">

        <?php $form = ActiveForm::begin(); $this->title = $model->nombre;   ?>

            <div class="col-md-2">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'readonly'=>"readonly"]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'tipo_usuario')->textInput(['maxlength' => 20, 'readonly'=>"readonly"]) ?>
            </div>
            <!--?= $form->field($model, 'auth_key')->textInput(['maxlength' => 32]) ?-->

            <!--?= $form->field($model, 'password_hash')->textInput(['maxlength' => 255]) ?-->

            <!--?= $form->field($model, 'password_reset_token')->textInput(['maxlength' => 255]) ?-->

            <!--?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?-->

            <!--?= $form->field($model, 'status')->textInput() ?-->

            <!--?= $form->field($model, 'created_at')->textInput() ?-->

            <!--?= $form->field($model, 'updated_at')->textInput() ?-->

            <!--?= $form->field($model, 'nombre')->textInput(['maxlength' => 50]) ?-->

            <!--?= $form->field($model, 'codigo')->textInput(['maxlength' => 20]) ?-->


    <div class="col-md-12">
      <center>
        <h3>Area de asignación de permisos</h3>
        <p>
          En las siguientes pestañas se encuentran los modulos correspondientes para asignarle a <?= $model->nombre ?> sus respectivos permisos.<br>
          Existen otros módulos que NO se otorgan permisos desde aqui pero son asignados en el momento que este usuario es tipo administrador.
        </p>
      </center>
        <?php //como obtener uno de ArrayHelper http://www.yiiframework.com/doc-2.0/guide-helper-array.html
        //$opciones = \yii\helpers\ArrayHelper::map($tipoOperaciones, 'id', 'traduccion');
       // echo $form->field($model, 'operaciones')->checkboxList($opciones, ['separator' => '<p>']);


      //  $Ade = \yii\helpers\ArrayHelper::map($tipoAsercaDe, 'id', 'traduccion');
        $C_vt = \yii\helpers\ArrayHelper::map($tipoClitI, 'id', 'traduccion');//------Cliente------------------------
        $C_c = \yii\helpers\ArrayHelper::map($tipoClitC, 'id', 'traduccion');
        $C_u = \yii\helpers\ArrayHelper::map($tipoClitU, 'id', 'traduccion');
        $C_d = \yii\helpers\ArrayHelper::map($tipoClitD, 'id', 'traduccion');
        $C_v = \yii\helpers\ArrayHelper::map($tipoClitV, 'id', 'traduccion');
        $F_vt = \yii\helpers\ArrayHelper::map($tipoFunI, 'id', 'traduccion');//------Funcionarios------------------------
        $F_c = \yii\helpers\ArrayHelper::map($tipoFunC, 'id', 'traduccion');
        $F_u = \yii\helpers\ArrayHelper::map($tipoFunU, 'id', 'traduccion');
        $F_d = \yii\helpers\ArrayHelper::map($tipoFunD, 'id', 'traduccion');
        $F_v = \yii\helpers\ArrayHelper::map($tipoFunV, 'id', 'traduccion');
        $M_vt = \yii\helpers\ArrayHelper::map($tipoMecI, 'id', 'traduccion');//------Mecánicos------------------------
        $M_c = \yii\helpers\ArrayHelper::map($tipoMecC, 'id', 'traduccion');
        $M_u = \yii\helpers\ArrayHelper::map($tipoMecU, 'id', 'traduccion');
        $M_d = \yii\helpers\ArrayHelper::map($tipoMecD, 'id', 'traduccion');
        $M_v = \yii\helpers\ArrayHelper::map($tipoMecV, 'id', 'traduccion');
        $Ma_vt = \yii\helpers\ArrayHelper::map($tipoMarI, 'id', 'traduccion');//------Marcas------------------------
        $Ma_c = \yii\helpers\ArrayHelper::map($tipoMarC, 'id', 'traduccion');
        $Ma_u = \yii\helpers\ArrayHelper::map($tipoMarU, 'id', 'traduccion');
        $Ma_d = \yii\helpers\ArrayHelper::map($tipoMarD, 'id', 'traduccion');
        $Ma_v = \yii\helpers\ArrayHelper::map($tipoMarV, 'id', 'traduccion');
        $Mo_vt = \yii\helpers\ArrayHelper::map($tipoModI, 'id', 'traduccion');//------Modelo------------------------
        $Mo_c = \yii\helpers\ArrayHelper::map($tipoModC, 'id', 'traduccion');
        $Mo_u = \yii\helpers\ArrayHelper::map($tipoModU, 'id', 'traduccion');
        $Mo_d = \yii\helpers\ArrayHelper::map($tipoModD, 'id', 'traduccion');
        $Mo_v = \yii\helpers\ArrayHelper::map($tipoModV, 'id', 'traduccion');
        $Ver_vt = \yii\helpers\ArrayHelper::map($tipoVerI, 'id', 'traduccion');//------Orden de Servicio------------------------
        $Ver_c = \yii\helpers\ArrayHelper::map($tipoVerC, 'id', 'traduccion');
        $Ver_u = \yii\helpers\ArrayHelper::map($tipoVerU, 'id', 'traduccion');
        $Ver_d = \yii\helpers\ArrayHelper::map($tipoVerD, 'id', 'traduccion');
        $Ver_v = \yii\helpers\ArrayHelper::map($tipoVerV, 'id', 'traduccion');
        $Ve_vt = \yii\helpers\ArrayHelper::map($tipoVeI, 'id', 'traduccion');//------Vehículo------------------------
        $Ve_c = \yii\helpers\ArrayHelper::map($tipoVeC, 'id', 'traduccion');
        $Ve_u = \yii\helpers\ArrayHelper::map($tipoVeU, 'id', 'traduccion');
        $Ve_d = \yii\helpers\ArrayHelper::map($tipoVeD, 'id', 'traduccion');
        $Ve_v = \yii\helpers\ArrayHelper::map($tipoVeV, 'id', 'traduccion');
        $Fa_vt = \yii\helpers\ArrayHelper::map($tipoFaI, 'id', 'traduccion');//------Categoría------------------------
        $Fa_c = \yii\helpers\ArrayHelper::map($tipoFaC, 'id', 'traduccion');
        $Fa_u = \yii\helpers\ArrayHelper::map($tipoFaU, 'id', 'traduccion');
        $Fa_d = \yii\helpers\ArrayHelper::map($tipoFaD, 'id', 'traduccion');
        $Fa_v = \yii\helpers\ArrayHelper::map($tipoFaV, 'id', 'traduccion');
        $Se_vt = \yii\helpers\ArrayHelper::map($tipoSeI, 'id', 'traduccion');//------Servicios------------------------
        $Se_c = \yii\helpers\ArrayHelper::map($tipoSeC, 'id', 'traduccion');
        $Se_u = \yii\helpers\ArrayHelper::map($tipoSeU, 'id', 'traduccion');
        $Se_d = \yii\helpers\ArrayHelper::map($tipoSeD, 'id', 'traduccion');
        $Se_v = \yii\helpers\ArrayHelper::map($tipoSeV, 'id', 'traduccion');
        $Pr_vt = \yii\helpers\ArrayHelper::map($tipoPrI, 'id', 'traduccion');//------Proveedores------------------------
        $Pr_c = \yii\helpers\ArrayHelper::map($tipoPrC, 'id', 'traduccion');
        $Pr_u = \yii\helpers\ArrayHelper::map($tipoPrU, 'id', 'traduccion');
        $Pr_d = \yii\helpers\ArrayHelper::map($tipoPrD, 'id', 'traduccion');
        $Pr_v = \yii\helpers\ArrayHelper::map($tipoPrV, 'id', 'traduccion');
        $P_vt = \yii\helpers\ArrayHelper::map($tipoPI, 'id', 'traduccion');//------Productos------------------------
        $P_c = \yii\helpers\ArrayHelper::map($tipoPC, 'id', 'traduccion');
        $P_u = \yii\helpers\ArrayHelper::map($tipoPU, 'id', 'traduccion');
        $P_d = \yii\helpers\ArrayHelper::map($tipoPD, 'id', 'traduccion');
        $P_v = \yii\helpers\ArrayHelper::map($tipoPV, 'id', 'traduccion');

        $marpro_i = \yii\helpers\ArrayHelper::map($tipomarpro, 'id', 'traduccion'); /*marcas producto*/

        $OrSe_vt = \yii\helpers\ArrayHelper::map($tipoOrSeI, 'id', 'traduccion');//------Orden de Servicio------------------------
        $OrSe_c = \yii\helpers\ArrayHelper::map($tipoOrSeC, 'id', 'traduccion');
        $OrSe_u = \yii\helpers\ArrayHelper::map($tipoOrSeU, 'id', 'traduccion');
        $OrSe_d = \yii\helpers\ArrayHelper::map($tipoOrSeD, 'id', 'traduccion');
        $OrSe_v = \yii\helpers\ArrayHelper::map($tipoOrSeV, 'id', 'traduccion');

        $transp_vt = \yii\helpers\ArrayHelper::map($tipoTranspI, 'id', 'traduccion');//------Transportes------------------------
        $transp_c = \yii\helpers\ArrayHelper::map($tipoTranspC, 'id', 'traduccion');
        $transp_u = \yii\helpers\ArrayHelper::map($tipoTranspU, 'id', 'traduccion');
        $transp_d = \yii\helpers\ArrayHelper::map($tipoTranspD, 'id', 'traduccion');

        $Com_vt = \yii\helpers\ArrayHelper::map($tipoComI, 'id', 'traduccion');//------Compras------------------------
        $Com_c = \yii\helpers\ArrayHelper::map($tipoComC, 'id', 'traduccion');
        $Com_u = \yii\helpers\ArrayHelper::map($tipoComU, 'id', 'traduccion');
        $Com_d = \yii\helpers\ArrayHelper::map($tipoComD, 'id', 'traduccion');
        $Com_v = \yii\helpers\ArrayHelper::map($tipoComV, 'id', 'traduccion');

        $O_com_vt = \yii\helpers\ArrayHelper::map($tipoOrComI, 'id', 'traduccion');//------Ordenes de compra------------------------
        $O_com_c = \yii\helpers\ArrayHelper::map($tipoOrComC, 'id', 'traduccion');

        $Cue_b = \yii\helpers\ArrayHelper::map($tipoCueB, 'id', 'traduccion');//----------Cuentas bancarias (Bancos) -------------

        $Lib_b = \yii\helpers\ArrayHelper::map($tipoLibB, 'id', 'traduccion');//---------- Movimiento en libros -------------

        $TPI = \yii\helpers\ArrayHelper::map($tipoTPI, 'id', 'traduccion');//----------Tramite de pago cuentas por pagar -------------
        $TPM = \yii\helpers\ArrayHelper::map($tipoTPMo, 'id', 'traduccion');
        $TPCP = \yii\helpers\ArrayHelper::map($tipoTPCP, 'id', 'traduccion');

        $CaPa = \yii\helpers\ArrayHelper::map($tipoCaPa, 'id', 'traduccion');//---------- Trámite de pago -------------
        $CoPa = \yii\helpers\ArrayHelper::map($tipoCoPa, 'id', 'traduccion');

        $TM_vt = \yii\helpers\ArrayHelper::map($tipoTipmI, 'id', 'traduccion');//------Tipo de movimientos------------------------
        $TM_c = \yii\helpers\ArrayHelper::map($tipoTipmC, 'id', 'traduccion');
        $TM_u = \yii\helpers\ArrayHelper::map($tipoTipmU, 'id', 'traduccion');
        $TM_d = \yii\helpers\ArrayHelper::map($tipoTipmD, 'id', 'traduccion');
        $TM_v = \yii\helpers\ArrayHelper::map($tipoTipmV, 'id', 'traduccion');

        $Movi_vt = \yii\helpers\ArrayHelper::map($tipoMovI, 'id', 'traduccion');//------Movimientos de inventario------------------------
        $Movi_c = \yii\helpers\ArrayHelper::map($tipoMovC, 'id', 'traduccion');
        $Movi_u = \yii\helpers\ArrayHelper::map($tipoMovU, 'id', 'traduccion');
        $Movi_d = \yii\helpers\ArrayHelper::map($tipoMovD, 'id', 'traduccion');
        $Movi_a = \yii\helpers\ArrayHelper::map($tipoMovA, 'id', 'traduccion');

        $Facd_vt = \yii\helpers\ArrayHelper::map($tipoFacD, 'id', 'traduccion');//------Facturas día------------------------
        $devpro = \yii\helpers\ArrayHelper::map($tipoDevPro, 'id', 'traduccion');//------Devolución producto------------------------

        $Cuco_vt = \yii\helpers\ArrayHelper::map($tipoMovCreI, 'id', 'traduccion');//------Cuentas por cobrar------------------------
        $Cuco_Ab = \yii\helpers\ArrayHelper::map($tipoMovCreA, 'id', 'traduccion');
        $Cuco_NC = \yii\helpers\ArrayHelper::map($tipoMovCreNC, 'id', 'traduccion');
        $Cuco_CF = \yii\helpers\ArrayHelper::map($tipoMovCreCF, 'id', 'traduccion');

        $Re_dx = \yii\helpers\ArrayHelper::map($tipoRes_doXML, 'id', 'traduccion');//-------Resolucion de xml---------------

        $Prof_vt = \yii\helpers\ArrayHelper::map($tipoProf_vt, 'id', 'traduccion');//------Proforma------------------------
        $Prof_c = \yii\helpers\ArrayHelper::map($tipoProf_c, 'id', 'traduccion');
        $Prof_u = \yii\helpers\ArrayHelper::map($tipoProf_u, 'id', 'traduccion');

        $prefa_vt = \yii\helpers\ArrayHelper::map($tipoprefa_vt, 'id', 'traduccion');//------Pre factura------------------------
        $prefa_c = \yii\helpers\ArrayHelper::map($tipoprefa_c, 'id', 'traduccion');
        $prefa_u = \yii\helpers\ArrayHelper::map($tipoprefa_u, 'id', 'traduccion');
        $prefa_d = \yii\helpers\ArrayHelper::map($tipoprefa_d, 'id', 'traduccion');

        $caja_vt = \yii\helpers\ArrayHelper::map($tipocaja_vt, 'id', 'traduccion');//------Caja------------------------
        $caja_dev = \yii\helpers\ArrayHelper::map($tipocaja_dev, 'id', 'traduccion');
        $caja_can = \yii\helpers\ArrayHelper::map($tipocaja_can, 'id', 'traduccion');
        $caja_rev = \yii\helpers\ArrayHelper::map($tipocaja_rev, 'id', 'traduccion');

        $categas_vt = \yii\helpers\ArrayHelper::map($tipocategas_vt, 'id', 'traduccion');//------Categoria Gastos------------------------
        $categas_c = \yii\helpers\ArrayHelper::map($tipocategas_c, 'id', 'traduccion');
        $categas_u = \yii\helpers\ArrayHelper::map($tipocategas_u, 'id', 'traduccion');
        $categas_d = \yii\helpers\ArrayHelper::map($tipocategas_d, 'id', 'traduccion');

        $tipogas_vt = \yii\helpers\ArrayHelper::map($tipotipgas_vt, 'id', 'traduccion');//------Tipo Gastos------------------------
        $tipogas_c = \yii\helpers\ArrayHelper::map($tipotipgas_c, 'id', 'traduccion');
        $tipogas_u = \yii\helpers\ArrayHelper::map($tipotipgas_u, 'id', 'traduccion');
        $tipogas_d = \yii\helpers\ArrayHelper::map($tipotipgas_d, 'id', 'traduccion');

        $proveegas_vt = \yii\helpers\ArrayHelper::map($tipoprovgas_vt, 'id', 'traduccion');//------Proveedores de Gastos------------------------
        $proveegas_c = \yii\helpers\ArrayHelper::map($tipoprovgas_c, 'id', 'traduccion');
        $proveegas_u = \yii\helpers\ArrayHelper::map($tipoprovgas_u, 'id', 'traduccion');
        $proveegas_d = \yii\helpers\ArrayHelper::map($tipoprovgas_d, 'id', 'traduccion');

        $registgastos_vt = \yii\helpers\ArrayHelper::map($tiporegistgas_d, 'id', 'traduccion');//------Registro de Gastos------------------------

        $rbancos_m = \yii\helpers\ArrayHelper::map($tiporbancos_m, 'id', 'traduccion');//------Reportes------------------------
        $rcaja_m = \yii\helpers\ArrayHelper::map($tiporcaja_m, 'id', 'traduccion');
        $rcompras_m = \yii\helpers\ArrayHelper::map($tiporcompras_m, 'id', 'traduccion');
        $rinventario_m = \yii\helpers\ArrayHelper::map($tiporinventario_m, 'id', 'traduccion');
        $orden_servicio_m = \yii\helpers\ArrayHelper::map($tipoorden_servicio_m, 'id', 'traduccion');
        $proveedores_m = \yii\helpers\ArrayHelper::map($tipoproveedores_m, 'id', 'traduccion');
        $usuarios_m = \yii\helpers\ArrayHelper::map($tipousuarios_m, 'id', 'traduccion');
         //   $asercade = $form->field($model, 'operaciones')->checkboxList($Ade, ['unselect'=>NULL, 'id'=>""])->label(false);
/*clientes*/            $clienteindex = $form->field($model, 'operaciones')->checkboxList($C_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                          $checked = $checked==1 ? 'checked': '';
                          return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                        }])->label(false);
                        $clientecreate = $form->field($model, 'operaciones')->checkboxList($C_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                          return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                        $clienteupdate = $form->field($model, 'operaciones')->checkboxList($C_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                  $checked = $checked==1 ? 'checked': '';
                                                  return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                }])->label(false);
                         $clientedelete = $form->field($model, 'operaciones')->checkboxList($C_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                         $clienteview = $form->field($model, 'operaciones')->checkboxList($C_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
/*Funcionario*/          $funcindex = $form->field($model, 'operaciones')->checkboxList($F_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                          $checked = $checked==1 ? 'checked': '';
                          return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                        }])->label(false);
                         $funccreate = $form->field($model, 'operaciones')->checkboxList($F_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                         $funcupdate = $form->field($model, 'operaciones')->checkboxList($F_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                         $funcdelete = $form->field($model, 'operaciones')->checkboxList($F_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                         $funcview = $form->field($model, 'operaciones')->checkboxList($F_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
/*Mecánico*/             $mecanicoindex = $form->field($model, 'operaciones')->checkboxList($M_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                          $checked = $checked==1 ? 'checked': '';
                          return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                        }])->label(false);
                         $mecanicocreate = $form->field($model, 'operaciones')->checkboxList($M_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                         $mecanicoupdate = $form->field($model, 'operaciones')->checkboxList($M_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                         $mecanicodelete = $form->field($model, 'operaciones')->checkboxList($M_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                         $mecanicoview = $form->field($model, 'operaciones')->checkboxList($M_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){
                                                   $checked = $checked==1 ? 'checked': '';
                                                   return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>";
                                                 }])->label(false);
                       //  $funcview = $form->field($model, 'operaciones')->widget(CheckboxX::classname(), checkboxList($F_v, ['initInputType' => CheckboxX::INPUT_CHECKBOX,'autoLabel' => true]))->label(false);
/*  Marca */             $marcaindex = $form->field($model, 'operaciones')->checkboxList($Ma_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $marcacreate = $form->field($model, 'operaciones')->checkboxList($Ma_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $marcaupdate = $form->field($model, 'operaciones')->checkboxList($Ma_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $marcadelete = $form->field($model, 'operaciones')->checkboxList($Ma_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $marcaview = $form->field($model, 'operaciones')->checkboxList($Ma_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/* Modelo */             $modeloindex = $form->field($model, 'operaciones')->checkboxList($Mo_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $modelocreate = $form->field($model, 'operaciones')->checkboxList($Mo_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $modeloupdate = $form->field($model, 'operaciones')->checkboxList($Mo_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $modelodelete = $form->field($model, 'operaciones')->checkboxList($Mo_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $modeloview = $form->field($model, 'operaciones')->checkboxList($Mo_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Versiones*/            $versionindex = $form->field($model, 'operaciones')->checkboxList($Ver_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $versioncreate = $form->field($model, 'operaciones')->checkboxList($Ver_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $versionupdate = $form->field($model, 'operaciones')->checkboxList($Ver_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $versiondelete = $form->field($model, 'operaciones')->checkboxList($Ver_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $versionview = $form->field($model, 'operaciones')->checkboxList($Ver_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Vehiculo*/             $vehiculoindex = $form->field($model, 'operaciones')->checkboxList($Ve_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $vehiculocreate = $form->field($model, 'operaciones')->checkboxList($Ve_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $vehiculoupdate = $form->field($model, 'operaciones')->checkboxList($Ve_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $vehiculodelete = $form->field($model, 'operaciones')->checkboxList($Ve_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $vehiculoview = $form->field($model, 'operaciones')->checkboxList($Ve_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Categoria*/            $familiaindex = $form->field($model, 'operaciones')->checkboxList($Fa_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $familiacreate = $form->field($model, 'operaciones')->checkboxList($Fa_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $familiaupdate = $form->field($model, 'operaciones')->checkboxList($Fa_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $familiadelete = $form->field($model, 'operaciones')->checkboxList($Fa_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $familiaview = $form->field($model, 'operaciones')->checkboxList($Fa_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Servicios*/            $servicioindex = $form->field($model, 'operaciones')->checkboxList($Se_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $serviciocreate = $form->field($model, 'operaciones')->checkboxList($Se_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $servicioupdate = $form->field($model, 'operaciones')->checkboxList($Se_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $serviciodelete = $form->field($model, 'operaciones')->checkboxList($Se_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $servicioview = $form->field($model, 'operaciones')->checkboxList($Se_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Proveedores*/          $proveedorindex = $form->field($model, 'operaciones')->checkboxList($Pr_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveedorcreate = $form->field($model, 'operaciones')->checkboxList($Pr_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveedorupdate = $form->field($model, 'operaciones')->checkboxList($Pr_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveedordelete = $form->field($model, 'operaciones')->checkboxList($Pr_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveedorview = $form->field($model, 'operaciones')->checkboxList($Pr_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Productos  */          $productoindex = $form->field($model, 'operaciones')->checkboxList($P_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $productocreate = $form->field($model, 'operaciones')->checkboxList($P_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $productoupdate = $form->field($model, 'operaciones')->checkboxList($P_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $productodelete = $form->field($model, 'operaciones')->checkboxList($P_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $productoview = $form->field($model, 'operaciones')->checkboxList($P_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $marcaproductoindex = $form->field($model, 'operaciones')->checkboxList($marpro_i, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                          return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*orden ser  */          $orseindex = $form->field($model, 'operaciones')->checkboxList($OrSe_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $orsecreate = $form->field($model, 'operaciones')->checkboxList($OrSe_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $orseupdate = $form->field($model, 'operaciones')->checkboxList($OrSe_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $orsedelete = $form->field($model, 'operaciones')->checkboxList($OrSe_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $orseview = $form->field($model, 'operaciones')->checkboxList($OrSe_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Compras*/              $comprasindex = $form->field($model, 'operaciones')->checkboxList($Com_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $comprascreate = $form->field($model, 'operaciones')->checkboxList($Com_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $comprasupdate = $form->field($model, 'operaciones')->checkboxList($Com_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $comprasdelete = $form->field($model, 'operaciones')->checkboxList($Com_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $comprasview = $form->field($model, 'operaciones')->checkboxList($Com_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Transportes*/          $transporteindex = $form->field($model, 'operaciones')->checkboxList($transp_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $transportecreate = $form->field($model, 'operaciones')->checkboxList($transp_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $transporteupdate = $form->field($model, 'operaciones')->checkboxList($transp_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $transportedelete = $form->field($model, 'operaciones')->checkboxList($transp_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Orden de compra*/      $ordencomprasindex = $form->field($model, 'operaciones')->checkboxList($O_com_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $ordencomprascompleta = $form->field($model, 'operaciones')->checkboxList($O_com_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Bancos*/               $cuentabancariaindex = $form->field($model, 'operaciones')->checkboxList($Cue_b, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Movimiento en libros*/ $librosindex = $form->field($model, 'operaciones')->checkboxList($Lib_b, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Cuentas pagar*/        $cuentaporpagarindex = $form->field($model, 'operaciones')->checkboxList($TPI, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $movimientosporpagar = $form->field($model, 'operaciones')->checkboxList($TPM, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $pagartramitepago = $form->field($model, 'operaciones')->checkboxList($TPCP, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*tramite de pago*/      $cancelar_pago = $form->field($model, 'operaciones')->checkboxList($CaPa, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $confirmar_pago = $form->field($model, 'operaciones')->checkboxList($CoPa, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Tipo movim*/           $tipomoindex = $form->field($model, 'operaciones')->checkboxList($TM_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $tipomocreate = $form->field($model, 'operaciones')->checkboxList($TM_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $tipomoupdate = $form->field($model, 'operaciones')->checkboxList($TM_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $tipomodelete = $form->field($model, 'operaciones')->checkboxList($TM_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $tipomoview = $form->field($model, 'operaciones')->checkboxList($TM_v, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Movimi Inven*/         $Movimientoinventarioindex = $form->field($model, 'operaciones')->checkboxList($Movi_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Movimientoinventariocreate = $form->field($model, 'operaciones')->checkboxList($Movi_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Movimientoinventarioupdate = $form->field($model, 'operaciones')->checkboxList($Movi_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Movimientoinventariodelete = $form->field($model, 'operaciones')->checkboxList($Movi_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Movimientoinventarioaplicar = $form->field($model, 'operaciones')->checkboxList($Movi_a, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Facturasdiaindex = $form->field($model, 'operaciones')->checkboxList($Facd_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $devolucionproductoindex = $form->field($model, 'operaciones')->checkboxList($devpro, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Cuentas cobrar*/       $Cuentasporcobrarindex = $form->field($model, 'operaciones')->checkboxList($Cuco_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Cuentasporcobrarabonos = $form->field($model, 'operaciones')->checkboxList($Cuco_Ab, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Cuentasporcobrarnotascredito = $form->field($model, 'operaciones')->checkboxList($Cuco_NC, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $Cuentasporcobrarcancelarfacturas = $form->field($model, 'operaciones')->checkboxList($Cuco_CF, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Recepcion de xml*/     $Recepcionindex = $form->field($model, 'operaciones')->checkboxList($Re_dx, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
/*Proforma*/             $profindex = $form->field($model, 'operaciones')->checkboxList($Prof_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $profcreate = $form->field($model, 'operaciones')->checkboxList($Prof_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $profupdate = $form->field($model, 'operaciones')->checkboxList($Prof_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

/*Prefactura*/           $prefaindex = $form->field($model, 'operaciones')->checkboxList($prefa_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $prefacreate = $form->field($model, 'operaciones')->checkboxList($prefa_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $prefaupdate = $form->field($model, 'operaciones')->checkboxList($prefa_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $prefadesechar = $form->field($model, 'operaciones')->checkboxList($prefa_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

/*Caja*/                 $cajaindex = $form->field($model, 'operaciones')->checkboxList($caja_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $cajadevolver = $form->field($model, 'operaciones')->checkboxList($caja_dev, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $cajacancelar = $form->field($model, 'operaciones')->checkboxList($caja_can, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $cajarevocar = $form->field($model, 'operaciones')->checkboxList($caja_rev, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

/*Categoria gastos*/     $categoriagastosindex = $form->field($model, 'operaciones')->checkboxList($categas_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $categoriagastoscreate = $form->field($model, 'operaciones')->checkboxList($categas_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $categoriagastosupdate = $form->field($model, 'operaciones')->checkboxList($categas_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $categoriagastosdelete = $form->field($model, 'operaciones')->checkboxList($categas_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

/*Tipo de gastos*/       $tipogastosindex = $form->field($model, 'operaciones')->checkboxList($tipogas_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $tipogastoscreate = $form->field($model, 'operaciones')->checkboxList($tipogas_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $tipogastosupdate = $form->field($model, 'operaciones')->checkboxList($tipogas_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $tipogastosdelete = $form->field($model, 'operaciones')->checkboxList($tipogas_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

/*Proveedores de gastos*/$proveegastosindex = $form->field($model, 'operaciones')->checkboxList($proveegas_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveegastoscreate = $form->field($model, 'operaciones')->checkboxList($proveegas_c, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveegastosupdate = $form->field($model, 'operaciones')->checkboxList($proveegas_u, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveegastosdelete = $form->field($model, 'operaciones')->checkboxList($proveegas_d, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

/*Registro de gastos*/   $registgastosindex = $form->field($model, 'operaciones')->checkboxList($registgastos_vt, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

/*Reportes*/             $rbancos = $form->field($model, 'operaciones')->checkboxList($rbancos_m, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $rcaja = $form->field($model, 'operaciones')->checkboxList($rcaja_m, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $rcompras = $form->field($model, 'operaciones')->checkboxList($rcompras_m, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $rinventario = $form->field($model, 'operaciones')->checkboxList($rinventario_m, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $orden_servicio = $form->field($model, 'operaciones')->checkboxList($orden_servicio_m, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $proveedores = $form->field($model, 'operaciones')->checkboxList($proveedores_m, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);
                         $usuarios = $form->field($model, 'operaciones')->checkboxList($usuarios_m, ['unselect'=>NULL,'item'=>function ($index, $label, $name, $checked, $value){ $checked = $checked==1 ? 'checked': '';
                           return "<label class='switch'><input type='checkbox' ".$checked." name='".$name."' value='".$value."'><div class='slider round'></div></label>"; }])->label(false);

        ?>
    <!--button type="button" class="btn btn-default"><i href="#user" data-toggle="tab" class="fa fa-flask">Usuarios</i></button>
    <button type="button" class="btn btn-default"><i href="#vehi" data-toggle="tab" class="fa fa-gears">Vehículos</i></button>
    <button type="button" class="btn btn-default"><i href="#regi" data-toggle="tab" class="fa fa-gears">Registro</i></button>
    <button type="button" class="btn btn-default"><i href="#acci" data-toggle="tab" class="fa fa-gears">Acciones</i></button-->
    <div id="refrescar">
      <div class="btn-group btn-group-justified">
        <a href="#user" data-toggle="tab" class="btn btn-primary">Usuarios</a>
        <a href="#vehi" data-toggle="tab" class="btn btn-primary">Vehículos</a>
        <a href="#regcom" data-toggle="tab" class="btn btn-primary">Registro y trámites</a>
        <!--a href="#regi" data-toggle="tab" class="btn btn-primary">Registro</a-->
        <div class="btn-group">
          <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
          Inventario <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#inv" data-toggle="tab" class="refin" >Inventario y proveedores</a></li>
            <li><a href="#mov" data-toggle="tab" class="refin">Movimientos</a></li>
          </ul>
        </div>
        <!--a href="#acci" data-toggle="tab" class="btn btn-primary">Acciones</a-->
        <div class="btn-group">
          <a  class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
          Acciones <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#fact" data-toggle="tab" class="reffa" >Facturacion y caja</a></li>
            <li><a href="#gastos" data-toggle="tab" class="reffa">Gastos</a></li>
            <li><a href="#reportes" data-toggle="tab" class="reffa">Reportes</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div id="semitransparente">
      <div class="table-responsive tab-content">
      <br>
        <div class="tab-pane fade" id="user">
          <center><h2>Usuarios</h2></center>
          <table class = "table table-condensed" >
            <!--tr>
                <td><h4>Página</h4></td>
                <td><?php //echo $asercade; ?></td>
            </tr-->
            <tr>
                <td><h4>Gestión de Usuarios</h4></td>
                <td colspan="5"><center><strong>Sólo Admnistrador</strong></center></td>
            </tr>
            <tr class="danger">
                <td></td>
                <td><center><h4>Listar</h4></center></td>
                <td><center><h4>Crear</h4></center></td>
                <td><center><h4>Actualizar</h4></center></td>
                <td><center><h4>Eliminar</h4></center></td>
                <td><center><h4>Consultar</h4></center></td>
            </tr>
            <tr id="fila_clientes">
                <td class="success"><h4>Módulo de Clientes</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_clientes()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $clienteindex; ?></td>
                <td style="text-align:center"><?php echo $clientecreate; ?></td>
                <td style="text-align:center"><?php echo $clienteupdate; ?></td>
                <td style="text-align:center"><?php echo $clientedelete; ?></td>
                <td style="text-align:center"><?php echo $clienteview; ?></td>
            </tr>
            <tr id="fila_funcionarios">
                <td class="success"><h4>Módulo de Funcionario</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_funcionario()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $funcindex; ?></td>
                <td style="text-align:center"><?php echo $funccreate; ?></td>
                <td style="text-align:center"><?php echo $funcupdate; ?></td>
                <td style="text-align:center"><?php echo $funcdelete; ?></td>
                <td style="text-align:center"><?php echo $funcview; ?></td>
            </tr>
            <tr id="fila_mecanicos">
                <td class="success"><h4>Módulo de Mecánico</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_mecanico()">Marcar todo</a></span></td>
                <td style="text-align:center"><h6><?php echo $mecanicoindex; ?></h6></td>
                <td style="text-align:center"><h6>No aplica</h6></td>
                <td style="text-align:center"><h6><?php echo $mecanicoupdate; ?></h6></td>
                <td style="text-align:center"><h6>No aplica</h6></td>
                <td style="text-align:center"><h6>No aplica</h6></td>
            </tr>
          </table>
        </div>
        <div class="tab-pane fade" id="vehi">
        <center><h2>Vehículos</h2></center>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Listar</h4></center></td>
                <td><center><h4>Crear</h4></center></td>
                <td><center><h4>Actualizar</h4></center></td>
                <td><center><h4>Eliminar</h4></center></td>
                <td><center><h4>Consultar</h4></center></td>
            </tr>
            <tr id="fila_marcas">
                <td class="success"><h4>Módulo de Marca</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_marcas()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $marcaindex; ?></td>
                <td style="text-align:center"><?php echo $marcacreate; ?></td>
                <td style="text-align:center"><?php echo $marcaupdate; ?></td>
                <td style="text-align:center"><?php echo $marcadelete; ?></td>
                <td style="text-align:center"><?php echo $marcaview; ?></td>
            </tr>
            <tr id="fila_modelos">
                <td class="success"><h4>Módulo de Modelos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_modelos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $modeloindex; ?></td>
                <td style="text-align:center"><?php echo $modelocreate; ?></td>
                <td style="text-align:center"><?php echo $modeloupdate; ?></td>
                <td style="text-align:center"><?php echo $modelodelete; ?></td>
                <td style="text-align:center"><?php echo $modeloview; ?></td>
            </tr>
            <tr id="fila_versionvehicular">
                <td class="success"><h4>Módulo de versión vehicular</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_versionvehicular()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $versionindex; ?></td>
                <td style="text-align:center"><?php echo $versioncreate; ?></td>
                <td style="text-align:center"><?php echo $versionupdate; ?></td>
                <td style="text-align:center"><?php echo $versiondelete; ?></td>
                <td style="text-align:center"><?php echo $versionview; ?></td>
            </tr>
            <tr id="fila_vehiculos">
                <td class="success"><h4>Módulo de Vehículos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_vehiculos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $vehiculoindex; ?></td>
                <td style="text-align:center"><?php echo $vehiculocreate; ?></td>
                <td style="text-align:center"><?php echo $vehiculoupdate; ?></td>
                <td style="text-align:center"><?php echo $vehiculodelete; ?></td>
                <td style="text-align:center"><?php echo $vehiculoview; ?></td>
            </tr>
          </table>
        </div>
        <div class="tab-pane fade" id="regcom">
          <center><h2>Registro compras y Trámites</h2></center>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Listar</h4></center></td>
                <td><center><h4>Crear</h4></center></td>
                <td><center><h4>Actualizar</h4></center></td>
                <td><center><h4>Eliminar</h4></center></td>
                <td><center><h4>Consultar Orden</h4></center></td>
                <td><center><h4>Completar Orden</h4></center></td>
            </tr>
            <tr id="fila_transportes">
                <td class="success"><h4>Transportes</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_transportes()">Marcar todo</a></span></td>
                <td style="text-align:center">	<?php echo $transporteindex; ?></td>
                <td style="text-align:center">	<?php echo $transportecreate; ?></td>
                <td style="text-align:center">	<?php echo $transporteupdate; ?></td>
                <td style="text-align:center">	<?php echo $transportedelete; ?></td>
                <td style="text-align:center">  No aplica</td>
                <td style="text-align:center">  No aplica</td>
            </tr>
            <tr id="fila_ord_compras">
                <td class="success"><h4>Ordenes de compra</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_or_compras()">Marcar todo</a></span></td>
                <td style="text-align:center">	No aplica</td>
                <td style="text-align:center">	No aplica</td>
                <td style="text-align:center">	No aplica</td>
                <td style="text-align:center">	No aplica</td>
                <td style="text-align:center"><?php echo $ordencomprasindex; ?></td>
                <td style="text-align:center"><?php echo $ordencomprascompleta; ?></td>
            </tr>
            <tr id="fila_compras">
                <td class="success"><h4>Cómpras</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_compras()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $comprasindex; ?></td>
                <td style="text-align:center"><?php echo $comprascreate; ?></td>
                <td style="text-align:center"><?php echo $comprasupdate; ?></td>
                <td style="text-align:center">	No aplica</td>
                <td style="text-align:center"> No aplica </td>
                <td style="text-align:center">No aplica</td>
            </tr>
          </table>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Consultar módulo</h4></center></td>
                <td><center><h4>Crear movimientos por pagar</h4></center></td>
                <td><center><h4>Crear pagos a proveedores</h4></center></td>
            </tr>
            <tr id="fila_tramitepagos">
                <td class="success"><h4>Trámite de pagos a proveedores</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_tramitepagos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $cuentaporpagarindex ?></td>
                <td style="text-align:center"><?= $movimientosporpagar ?></td>
                <td style="text-align:center"><?= $pagartramitepago ?></td>
            </tr>
            <tr id="fila_canceltramitepagos">
                <td class="success"><h4>Cancelar trámite de pagos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_canceltramitepagos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $cancelar_pago ?></td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
            </tr>
            <tr id="fila_aplictramitepagos">
                <td class="success"><h4>Confirmar trámite de pagos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_aplictramitepagos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $confirmar_pago ?></td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
            </tr>
            <tr id="fila_bancos">
                <td class="success"><h4>Bancos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_bancos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $cuentabancariaindex ?></td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
            </tr>
            <tr id="fila_libros">
                <td class="success"><h4>Movimiento en libros</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_libros()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $librosindex ?></td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
            </tr>
          </table>
        </div>
        <div class="tab-pane fade" id="inv">
          <center><h2>Inventario y proveedores</h2></center>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Listar</h4></center></td>
                <td><center><h4>Crear</h4></center></td>
                <td><center><h4>Actualizar</h4></center></td>
                <td><center><h4>Eliminar</h4></center></td>
                <td><center><h4>Consultar</h4></center></td>
            </tr>
            <tr id="fila_categorias">
                <td class="success"><h4>Módulo de Categorías</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_categorias()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $familiaindex; ?></td>
                <td style="text-align:center"><?php echo $familiacreate; ?></td>
                <td style="text-align:center"><?php echo $familiaupdate; ?></td>
                <td style="text-align:center"><?php echo $familiadelete; ?></td>
                <td style="text-align:center"><?php echo $familiaview; ?></td>
            </tr>
            <tr id="fila_servicios">
                <td class="success"><h4>Módulo de Servicios</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_servicios()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $servicioindex; ?></td>
                <td style="text-align:center"><?php echo $serviciocreate; ?></td>
                <td style="text-align:center"><?php echo $servicioupdate; ?></td>
                <td style="text-align:center"><?php echo $serviciodelete; ?></td>
                <td style="text-align:center"><?php echo $servicioview; ?></td>
            </tr>
            <tr id="fila_proveedores">
                <td class="success"><h4>Módulo de Proveedores</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_proveedores()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $proveedorindex; ?></td>
                <td style="text-align:center"><?php echo $proveedorcreate; ?></td>
                <td style="text-align:center"><?php echo $proveedorupdate; ?></td>
                <td style="text-align:center"><?php echo $proveedordelete; ?></td>
                <td style="text-align:center"><?php echo $proveedorview; ?></td>
            </tr>
            <tr id="fila_productos">
                <td class="success"><h4>Módulo de Productos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_productos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $productoindex; ?></td>
                <td style="text-align:center"><?php echo $productocreate; ?></td>
                <td style="text-align:center"><?php echo $productoupdate; ?></td>
                <td style="text-align:center"><?php echo $productodelete; ?></td>
                <td style="text-align:center"><?php echo $productoview; ?></td>
            </tr>
            <tr id="fila_marcaproducto">
                <td class="success"><h4>Módulo de Marcas de Producto</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_marcaproducto()">Marcar todo</a></span></td>
                <td style="text-align:center"><?php echo $marcaproductoindex; ?></td>
                <td style="text-align:center"> No aplica </td>
                <td style="text-align:center"> No aplica </td>
                <td style="text-align:center"> No aplica </td>
                <td style="text-align:center"> No aplica </td>
            </tr>
          </table>
        </div>
        <div class="tab-pane fade" id="mov">
          <center><h2>Movimientos</h2></center>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Listar</h4></center></td>
                <td><center><h4>Crear</h4></center></td>
                <td><center><h4>Actualizar</h4></center></td>
                <td><center><h4>Eliminar</h4></center></td>
                <td><center><h4>Consultar</h4></center></td>
                <td><center><h4>Aplicar</h4></center></td>
            </tr>
            <tr id="fila_tipomovimiento">
                <td class="success"><h4>Tipos movimientos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_tipomovimiento()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $tipomoindex ?></td>
                <td style="text-align:center"><?= $tipomocreate ?></td>
                <td style="text-align:center"><?= $tipomoupdate ?></td>
                <td style="text-align:center"><?= $tipomodelete ?></td>
                <td style="text-align:center"><?= $tipomoview ?></td>
                <td style="text-align:center"> No aplica </td>
            </tr>
            <tr id="fila_moviminventario">
                <td class="success"><h4>Movimiento a inventario</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_moviminventario()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $Movimientoinventarioindex ?></td>
                <td style="text-align:center"><?= $Movimientoinventariocreate ?></td>
                <td style="text-align:center"><?= $Movimientoinventarioupdate ?></td>
                <td style="text-align:center"><?= $Movimientoinventariodelete ?></td>
                <td style="text-align:center"> No aplica </td>
                <td style="text-align:center"><?= $Movimientoinventarioaplicar ?></td>
            </tr>
          </table>
        </div>
        <div class="tab-pane fade" id="fact">
        <center><h2>Facturación y caja</h2></center>
          <table class = "table table-condensed">
            <tr class="danger">
                  <td></td>
                  <td><center><h4>Listar</h4></center></td>
                  <td><center><h4>Crear</h4></center></td>
                  <td><center><h4>Actualizar</h4></center></td>
                  <td><center><h4>Eliminar / desechar</h4></center></td>
                  <td><center><h4>Consultar</h4></center></td>
              </tr>
              <tr id="fila_ordenservicio">
                  <td class="success"><h4>Orden de Servicio</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_ordenservicio()">Marcar todo</a></span></td>
                  <td style="text-align:center"><?php echo $orseindex; ?></td>
                  <td style="text-align:center"><?php echo $orsecreate; ?></td>
                  <td style="text-align:center"><?php echo $orseupdate; ?></td>
                  <td style="text-align:center"><?php echo $orsedelete; ?></td>
                  <td style="text-align:center"><?php echo $orseview; ?></td>
              </tr>
              <tr id="fila_proforma">
                  <td class="success"><h4>Factura proforma</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_proforma()">Marcar todo</a></span></td>
                  <td style="text-align:center"><?= $profindex; ?></td>
                  <td style="text-align:center"><?= $profcreate; ?></td>
                  <td style="text-align:center"><?= $profupdate; ?></td>
                  <td style="text-align:center">No aplica</td>
                  <td style="text-align:center">No aplica</td>
              </tr>
              <tr id="fila_prefactura">
                  <td class="success"><h4>Pre-factura</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_prefactura()">Marcar todo</a></span></td>
                  <td style="text-align:center"><?= $prefaindex ?></td>
                  <td style="text-align:center"><?= $prefacreate ?></td>
                  <td style="text-align:center"><?= $prefaupdate ?></td>
                  <td style="text-align:center"><?= $prefadesechar ?></td>
                  <td style="text-align:center">No aplica</td>
              </tr>
          </table>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Listar</h4></center></td>
                <td><center><h4>Devolver a prefactura</h4></center></td>
                <td><center><h4>Cancelar factura</h4></center></td>
                <td><center><h4>Revocar</h4></center></td>
            </tr>
            <tr id="fila_facturacion">
                <td class="success"><h4>Facturacion / Caja</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_facturacion()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $cajaindex ?></td>
                <td style="text-align:center"><?= $cajadevolver ?></td>
                <td style="text-align:center"><?= $cajacancelar ?></td>
                <td style="text-align:center"><?= $cajarevocar ?></td>
            </tr>
            <tr id="fila_facturasdiarias">
                <td class="success"><h4>Facturas díarias</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_facturasdiarias()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $Facturasdiaindex ?></td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
            </tr>
            <tr id="fila_devolucionproducto">
                <td class="success"><h4>Devolución Productos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_devolucionproducto()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $devolucionproductoindex ?></td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
            </tr>
          </table>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Consultar módulo</h4></center></td>
                <td><center><h4>Crear abonos</h4></center></td>
                <td><center><h4>Aplicar notas de crédito</h4></center></td>
                <td><center><h4>Cancelar facturas crédito</h4></center></td>
            </tr>
            <tr id="fila_cuentascobrar">
                <td class="success"><h4>Cuentas por cobrar</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_cuentascobrar()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $Cuentasporcobrarindex ?></td>
                <td style="text-align:center"><?= $Cuentasporcobrarabonos  ?></td>
                <td style="text-align:center"><?= $Cuentasporcobrarnotascredito ?></td>
                <td style="text-align:center"><?= $Cuentasporcobrarcancelarfacturas ?></td>
            </tr>
            <tr id="fila_recepcion">
                <td class="success"><h4>Recepción de documentos XML</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_recepcion()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $Recepcionindex ?></td>
                <td style="text-align:center"></td>
                <td style="text-align:center"></td>
                <td style="text-align:center"></td>
            </tr>
          </table>
        </div>
        <div class="tab-pane fade" id="gastos">
        <center><h2>Gastos</h2></center>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>Consultar / listar</h4></center></td>
                <td><center><h4>Crear</h4></center></td>
                <td><center><h4>Actualizar</h4></center></td>
                <td><center><h4>Eliminar</h4></center></td>
            </tr>
            <tr id="fila_categoriagastos">
                <td class="success"><h4>Categoría de gastos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_categoriagastos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $categoriagastosindex ?></td>
                <td style="text-align:center"><?= $categoriagastoscreate ?></td>
                <td style="text-align:center"><?= $categoriagastosupdate ?></td>
                <td style="text-align:center"><?= $categoriagastosdelete ?></td>
            </tr>
            <tr id="fila_tipogastos">
                <td class="success"><h4>Tipo Gastos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_tipogastos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $tipogastosindex ?></td>
                <td style="text-align:center"><?= $tipogastoscreate ?></td>
                <td style="text-align:center"><?= $tipogastosupdate ?></td>
                <td style="text-align:center"><?= $tipogastosdelete ?></td>
            </tr>
            <tr id="fila_proveegastos">
                <td class="success"><h4>Proveedores Gastos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_proveegastos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $proveegastosindex ?></td>
                <td style="text-align:center"><?= $proveegastoscreate ?></td>
                <td style="text-align:center"><?= $proveegastosupdate ?></td>
                <td style="text-align:center"><?= $proveegastosdelete ?></td>
            </tr>
            <tr id="fila_regisgastos">
                <td class="success"><h4>Registro Gastos</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_regisgastos()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $registgastosindex ?></td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
                <td style="text-align:center">No aplica</td>
            </tr>
          </table>
        </div>
        <div class="tab-pane fade" id="reportes">
        <center><h2>Reportes</h2></center>
          <table class = "table table-condensed">
            <tr class="danger">
                <td></td>
                <td><center><h4>BANCOS</h4></center></td>
                <td><center><h4>CAJA</h4></center></td>
                <td><center><h4>COMPRAS</h4></center></td>
                <td><center><h4>INVENTARIO</h4></center></td>
                <td><center><h4>ORDEN DE SERVICIO</h4></center></td>
                <td><center><h4>PROVEEDORES</h4></center></td>
                <td><center><h4>USUARIOS</h4></center></td>
                <td><center><h4>VEHICULOS</h4></center></td>
            </tr>
            <tr id="fila_reporte">
                <td class="success"><h4>Mostrar reporte de</h4><span style="float:right"><a type="button" class="btn btn-default btn-xs" onclick="ma_reporte()">Marcar todo</a></span></td>
                <td style="text-align:center"><?= $rbancos ?></td>
                <td style="text-align:center"><?= $rcaja ?></td>
                <td style="text-align:center"><?= $rcompras ?></td>
                <td style="text-align:center"><?= $rinventario ?></td>
                <td style="text-align:center"><?= $orden_servicio ?></td>
                <td style="text-align:center"><?= $proveedores ?></td>
                <td style="text-align:center"><?= $usuarios ?></td>
                <td style="text-align:center">No disponible</td>
            </tr>
          </table>
        </div>
      </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Guardar cambios', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>



<!--div class="switch demo3">
  <input type="checkbox">
  <label><i></i>
  </label>
</div>

<div class="switch demo3">
  <input type="checkbox" checked>
  <label><i></i>
  </label>
</div>
<button type="button" class="btn btn-default btn-lg">
  <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Star
</button-->







    </div>
    <?php ActiveForm::end(); ?>

</div>
