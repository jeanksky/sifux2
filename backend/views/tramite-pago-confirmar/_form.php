<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TramitePagoConfirmar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tramite-pago-confirmar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idProveedor')->textInput() ?>

    <?= $form->field($model, 'cantidad_monto_documento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'monto_saldo_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'porcentaje_descuento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'monto_tramite_pagar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recibo_cancelacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prioridad_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_registra')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_registra')->textInput() ?>

    <?= $form->field($model, 'usuario_cancela')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_cancela')->textInput() ?>

    <?= $form->field($model, 'usuario_aplica')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_aplica')->textInput() ?>

    <?= $form->field($model, 'detalle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado_tramite')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
