<?php
use backend\models\ComprasInventario;
use backend\models\TramitePagoConfirmar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

//obtengo instancia de compras de inventario para saber las facturas de estre tramite
$facturasCredito = ComprasInventario::find()->where(['=','idTramite_pago', $idTramite_pago])->orderBy(['idCompra' => SORT_DESC])->all();

?>
<script type="text/javascript">
  $(document).ready(function () {

  });
  function habilitarbtn(idTramite_pago) {
    doc_banco = document.getElementById("doc_banco").value;
		observaciones = document.getElementById("observaciones").value;
    if (doc_banco=='' || observaciones=='') {
		    $('#b_cancelar_tramite').addClass('disabled');
        $('#notif_doc_banc').html('');
		} else {
      //comprovamos con ajax si el numero de comprovante ya existe para ese banco
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/valida_documento_banco') ?>",
                    type:"post",
                    data: { 'doc_banco' : doc_banco, 'idTramite_pago' : idTramite_pago },
                    success: function(data){
                        if (data=='no') {
                            $('#notif_doc_banc').html('<center><font size=4 color="red">Documento repetido</font></center>');
                            $('#b_cancelar_tramite').addClass('disabled');
                        }
                        else {
                            $('#notif_doc_banc').html('<center><font size=4 color="green">Documento aceptado</font></center>');
                            $('#b_cancelar_tramite').removeClass('disabled');
                        }
                    },
                    error: function(msg, status,err){
                     $('#noti_pagos_tr').html('Error');
                    }
            });
    }
  }

  var visual_nc = null;
	function ver_anular_tramite() {
		obj = document.getElementById('contenido_anular_tramite');
		obj.style.display = (obj==visual_nc) ? 'none' : 'block';
		if (visual_nc != null)
		visual_nc.style.display = 'none';
		visual_nc = (obj==visual_nc) ? null : obj;
	}

  //espacio que solo permite numero
    function isNumber(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || (key >= 48 && key <= 57));
    }

</script>
<style type="text/css">
	.oculto_anular_tramite {
		display:none;
	}
</style>

  <div class="col-lg-6 alert alert-info">
    <center><a style="cursor:pointer" onclick="ver_anular_tramite()">Anular este trámite </a></center>
    <div id="contenido_anular_tramite" class="oculto_anular_tramite">
      <div class="col-lg-9">
        <label>Escriba el motivo de anulación:</label>
        <?= Html::input('text', '', '', ['size' => '30', 'id' => 'detalle_anulacion', 'class' => 'form-control']) ?>
      </div>
      <div class="col-lg-3"><br>
        <?= Html::a('<i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Anular tramite', '#', ['class' => 'btn btn-danger', 'onclick'=>"anular_tramite(".$idTramite_pago.",".$idProv.")"]) ?>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <p style="text-align:right">
      <?php
      $tramite = TramitePagoConfirmar::find()->where(['=','idTramite_pago', $idTramite_pago])->one();
      if ($tramite->estado_tramite == 'Pendiente') {
        # code...
      ?>
      <?= Html::a('<i class="fa fa-reply-all" aria-hidden="true"></i> Reversar trámite', '#', ['class' => 'btn btn-info', 'onclick'=>"reversar_tramite(".$idTramite_pago.")"]) ?>
      <?= Html::a('<i class="fa fa-paper-plane-o" aria-hidden="true"></i> Pagar trámite pendiente', '#', ['class' => 'btn btn-success',
                                                'id' => 'activity-index-link-cancelar-tramite',
                                                'data-toggle' => 'modal',
                                                'data-target' => '#modalcancelartramite',
                                                'data-pjax' => '0',
                                                'onclick'=>"modal_pagar_tramite(".$idTramite_pago.")"]) ?>
      <?php } else { ?>
      <?= Html::a('<i class="fa fa-reply" aria-hidden="true"></i> Devolver trámite', ['devolver_tramite', 'idTramite_pago' => $idTramite_pago], [
          'class' => 'btn btn-info',
          'data' => [
              'confirm' => '¿Seguro que quieres devolver este trámite?',
              'method' => 'post',
          ],
      ]) ?>
      <?= Html::a('<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Confirmar pago', '#', ['class' => 'btn btn-primary', 'onclick'=>'aplicar_tramite('.$idTramite_pago.', this)', 'data-loading-text'=>"Espere..."]) ?>
      <?php } ?>
    </p>
  </div>
<?php
echo '<div class="col-lg-12"><div class="well">
      <table class="items table table-striped" id="tabla_facturas_pendientes"  >';
                echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                    <th class="actions button-column">&nbsp;</th></tr>',
                        'N° INTER PROV',
                        'N° FACTURA',
                        'FECH.REG',
                        'FECH.VEN',
                        'MONTO',
                        'NOTA DÉBITO',
                        'NOTA CRÉDITO',
                        'ABONO',
                        'SALDO FACTURA'
                        );
                echo '</thead>';
                echo '<tbody class="buscar1">';
                foreach($facturasCredito as $position => $factura) {
                	$idCompra = $factura['idCompra'];
                    $command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND idCompra = " . $idCompra);
                    $command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND idCompra = " . $idCompra);
                    $command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND idCompra = " . $idCompra);
                    $suma_monto_nc = floatval($command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
                    $suma_monto_nd = floatval($command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
                    $suma_monto_ab = floatval($command_ab->queryScalar()); //ingreso la suma del monto de abonos

                    $fecha_registro = date('d-m-Y', strtotime( $factura['fechaRegistro'] ));
                    $fecha_vencimiento = date('d-m-Y', strtotime( $factura['fechaVencimiento'] ));
                    $mostrarFactura = Html::a('', ['#'], [
                                    'class'=>'glyphicon glyphicon-search',
                                    'id' => 'activity-index-link-factura-tramite',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalFacturaTramite',
                                    'data-url' => Url::to(['tramite-pago/vista_factura', 'id' => $factura['idCompra']]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Procede a mostrar la factura')]);
                    printf('<tr>
                            <td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=1>%s</font></td>
                            <td align="center"><font face="arial" size=1>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                                $factura['n_interno_prov'],
                                $factura['numFactura'],
                                $fecha_registro,
                                $fecha_vencimiento,
                                number_format($factura['total'],2),
                                number_format($suma_monto_nd,2),
                                number_format($suma_monto_nc,2),
                                number_format($suma_monto_ab,2),
                                number_format($factura['credi_saldo'],2),
                                $mostrarFactura
                                );
                }
                echo '</tbody>';
        echo '</table>
        </div></div>';
        ?>
        <?php
        //muestra modal para actualizar nota de credito pendiente
            Modal::begin([
                    'id' => 'modalcancelartramite',
                    //'size'=>'modal-sm',
                    'header' => '<center><h4 class="modal-title">Cancelación de trámite</h4></center>',
                    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.
                    Html::a('Cancelar trámite', '#', ['class' => 'btn btn-success', 'id'=>'b_cancelar_tramite' ,'onclick'=>'cancelar_tramite_pendiente(this)','data-loading-text'=>"Espere..."]),
                ]);
            echo '<div class="well">';
                echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">';
                    echo '<div id="cancelacion-tramite">';
                      echo '<div class="col-lg-2">';
                        echo' <input type="hidden" name="idtcambio" id="idtcambio">';
                        echo '<label>No.Trámite: </label>';
                        echo ' '.Html::input('text', '', '', ['class' => 'form-control', 'id' => 'no_tramite', 'readonly' => true]);
                      echo '</div>';
                      echo '<div class="col-lg-3">';
                        echo '<label>Fecha trámite: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'fecha_registra', 'readonly' => true]);
                      /*echo yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                          'options' => ['class'=>'form-control', 'placeholder'=>'FECHA',
                          'id'=>'fecha_cancelacion']]);*/
                      echo '</div>';
                      echo '<div class="col-lg-7">';
                        echo '<label>Proveedor: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'proveedor', 'readonly' => true]);
                      echo '<br></div>';
                      echo '<div class="col-lg-3">';
                        echo '<label>Monto: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'monto_pagar', 'readonly' => true]);
                      echo '</div>';
                      echo '<div class="col-lg-4">';
                        echo '<label>Tipo Movimiento: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'tipo_movimiento', 'readonly' => true]);
                      echo '</div>';
                      echo '<div class="col-lg-5">';
                        echo '<label>Documento del banco: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'doc_banco', 'onkeypress'=>"return isNumber(event)", 'onkeyup'=>"habilitarbtn(".$idTramite_pago.")", 'readonly' => false]);
                      echo '<div id="notif_doc_banc"></div><br></div>';
                      echo '<div class="col-lg-4">';
                        echo '<label>Usuario que cancela: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'usuario_cancela', 'readonly' => true]);
                      echo '</div>';
                      echo '<div class="col-lg-4">';
                        echo '<label>Fecha a cancelar: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'fecha_cancelacion', 'readonly' => true]);
                      echo '</div>';
                      echo '<div class="col-lg-4">';
                        echo '<label>Cuenta bancaria debita: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'cuenta_bancaria_local', 'readonly' => true]);
                      echo '<br></div>';
                      echo '<div class="col-lg-12">';
                        echo '<label>Observaciones: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'observaciones', 'onkeyup'=>"habilitarbtn(".$idTramite_pago.")", 'readonly' => false]);
                      echo '<br></div>';
                      echo '<div class="col-lg-3">';
                        echo '<label>Estado trámite: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'estado_tramite', 'readonly' => true]);
                      echo '</div>';
                      echo '<div class="col-lg-4">';
                        echo '<label>Cuenta proveedor: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'cuenta_proveedor', 'readonly' => true]);
                      echo '<br></div>';
                      echo '<div class="col-lg-6">';
                        echo '<label>Contacto: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'contacto', 'readonly' => true]);
                      echo '</div>';
                      echo '<div class="col-lg-6">';
                        echo '<label>Email contacto: </label>';
                        echo Html::input('text', '', '', ['class' => 'form-control', 'id' => 'e_contacto', 'readonly' => true]);
                      echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';

            Modal::end();
        ?>
