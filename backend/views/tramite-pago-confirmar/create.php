<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TramitePagoConfirmar */

$this->title = 'Create Tramite Pago Confirmar';
$this->params['breadcrumbs'][] = ['label' => 'Tramite Pago Confirmars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tramite-pago-confirmar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
