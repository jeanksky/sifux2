<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="rev"]').popover();//muestra un popover para ver cuando un tramite fue reversado
        //me checa todas las facturas pendientes
        $('#checkAll').on('click', function() {
            if (this.checked == true)
                $('#tabla_tramites_pendientes').find('input[name="checkboxRow[]"]').prop('checked', true);
            else
                $('#tabla_tramites_pendientes').find('input[name="checkboxRow[]"]').prop('checked', false);
        });
    });
</script>
<?php
use yii\helpers\Html;
use backend\models\TramitePagoConfirmar;
use backend\models\ComprasInventario;
use backend\models\Proveedores;

    //$tramitesPendientes = TramitePagoConfirmar::find()->where(['=','estado_tramite', 'Pendiente'])->orderBy(['idTramite_pago' => SORT_DESC])->limit(100)->all();

      $tramitesPendientes = TramitePagoConfirmar::find()->where(['=','estado_tramite', 'Precancelado'])
      ->andFilterWhere(['=', 'idProveedor', $idProveedor ])
      ->andFilterWhere(['like', 'idTramite_pago', $id_tramite ])
      ->andFilterWhere(['like', 'fecha_registra', $fecha_busqueda ])
      ->andFilterWhere(['like', 'prioridad_pago', $prioridad ])
      ->orderBy(['idTramite_pago' => SORT_DESC])->limit(100)->all();

    echo '<table class="items table table-striped" id="tabla_tramites_pendientes"  >';
    echo '<thead>';
    printf('<tr>
        <th><font face="arial" size=4>%s</font></th>
        <th style="text-align:center"><font face="arial" size=4>%s</font></th>
        <th style="text-align:center"><font face="arial" size=4>%s</font></th>
        <th style="text-align:right"><font face="arial" size=4>%s</font></th>
        <th style="text-align:right"><font face="arial" size=4>%s</font></th>
        <th>%s</th>
        <th style="text-align:right"><font face="arial" size=4>%s</font></th></tr>',
            'ID TRÁMITE',
            'FECHA',
            'PROVEEDOR',
            'MONTO',
            'CANT.FACT',
            '',
            'PRIORIDAD'
            );
    echo '</thead>';
    echo '<tbody class="buscar1">';

    foreach($tramitesPendientes as $position => $tramite) {
      $proveedor = Proveedores::findOne($tramite['idProveedor']);
      $nombreEmpresa = Html::a($proveedor->nombreEmpresa, '../web/index.php?r=proveedores%2Fview&id='.$tramite['idProveedor']);
      $facturas_tramite = ComprasInventario::find()->where(['=','idTramite_pago', $tramite['idTramite_pago']])->all();
      $cantidad_factura = 0;
      $detalle_reverso = Html::a('', '#', [
              'class' => 'glyphicon glyphicon-refresh',
              'id' => 'activity-index-link-inventario',
              'data-toggle' => 'rev',//rev seria el codigo que yo elijo para decir que tipo de popover es
              'data-placement' => 'left',
              'data-trigger'=>'hover', //esto es para que muestre el tootip sin darle clic
              'data-content'=>'<strong>Motivo:</strong> '.$tramite['detalle_reverso'],
              'data-html'=>'true',
              'title' => Yii::t('app', '<strong>Trámite reversado</strong>'),
            ]);
      $rev = $tramite['detalle_reverso'] == '' ? '' : $detalle_reverso;
      foreach ($facturas_tramite as $key) { $cantidad_factura += 1; }
        $idTramite_activo = $tramite['idTramite_pago'];
        $border = '';
        if ($tramite['prioridad_pago']=='Media') { $border = 'orange 10px solid;'; } //si el tramite es prioridad media los bordes naranja
        else if ($tramite['prioridad_pago']=='Alta') { $border = 'red 10px solid;'; } //si el tramite es prioridad alta los bordes rojos
        printf('<tr style = "border-left: '.$border.' border-right: '.$border.' cursor:pointer " onclick="marcartramite(this, '.$tramite['idTramite_pago'].','.$tramite['idProveedor'].')">
                <td align="center" style="background: ;"><font face="arial" size=3>%s</font></td>
                <td align="center"><font face="arial" size=3>%s</font></td>
                <td align="center"><font face="arial" size=3>%s</font></td>
                <td align="right"><font face="arial" size=3>%s</font></td>
                <td align="center"><font face="arial" size=3>%s</font></td>
                <td align="center"><font face="arial" size=3>%s</font></td>
                <td align="right"><font face="arial" size=3>%s</font></td>',
                    $tramite['idTramite_pago'],
                    $tramite['fecha_registra'],
                    $nombreEmpresa,
                    number_format($tramite['monto_tramite_pagar'],2),
                    $cantidad_factura,
                    $rev,
                    $tramite['prioridad_pago']
                    );
    }
    echo '</tbody>';
    echo '</table>';

?>
