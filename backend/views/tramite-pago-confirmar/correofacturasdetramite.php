<?php
use backend\models\ComprasInventario;
use backend\models\TramitePago;
use backend\models\MedioPagoProveedor;
use backend\models\TipoDocumento;
use backend\models\CuentasBancarias;
use backend\models\CuentasBancariasProveedor;
use backend\models\ContactosPagos;
use backend\models\MovimientoPagar;
use yii\helpers\Html;
use yii\helpers\Url;

	$tramite_p = TramitePago::find()->where(['idTramite_pago'=>$tramite->idTramite_pago])->one();
	$mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$tramite->idTramite_pago])->one();
	$documento = TipoDocumento::find()->where(['idTipo_documento'=>$mediopago->idTipo_documento])->one();
	$cuenta_lo = CuentasBancarias::find()->where(['idBancos'=>$mediopago->cuenta_bancaria_local])->one();
	$cuenta_pr = CuentasBancariasProveedor::find()->where(['id'=>$mediopago->cuenta_bancaria_proveedor])->one();
	$contacto_pag = ContactosPagos::find()->where(['idContacto_pago'=>$mediopago->idContacto_pago])->one();

	$_suma_monto_nc = 0;
	$_suma_monto_nd = 0;
	$_suma_monto_ab = 0;
	//obtengo instancia de compras de inventario para saber cuantas facturas de estre proveedor estan en proceso
	$facturasCredito = ComprasInventario::find()->where(['=','idTramite_pago', $tramite->idTramite_pago])->orderBy(['idCompra' => SORT_DESC])->all();

    	foreach($facturasCredito as $factura_proceso) {
            $numFactura = "'".$factura_proceso['numFactura']."'";
		    $_command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND numFactura = " . $numFactura);
	        $_command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND numFactura = " . $numFactura);
	        $_command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND numFactura = " . $numFactura);
	        $_suma_monto_nc += floatval($_command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
	        $_suma_monto_nd += floatval($_command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
	        $_suma_monto_ab += floatval($_command_ab->queryScalar()); //ingreso la suma del monto de abonos
	    }

    $_monto_credito = $_suma_monto_nc + $_suma_monto_ab;//con eso obtengo el monto total de monto_credito
    $_monto_debito = $_suma_monto_nd;//y declaro que $monto_debito sea la suma de notss de eito
    $_descuento = $tramite->porcentaje_descuento * 0.01;//obtendo el descuento del %
    $_monto_descuento = $tramite->monto_saldo_pago * $_descuento; //multiplico el %descuento por el monto del saldo
?>
<style type="text/css">
</style>

	<div class="col-lg-12 alert alert-info" role="alert">
		<table>
		<tr>
			<td width="210">
				<h4>Registro:</h4>
				Fecha: <?= date('d-m-Y h:i:s A', strtotime( $tramite->fecha_registra )) ?>
			</td>
			<td width="210">
				<h4>Cancelación:</h4>
				Fecha: <?= $tramite->fecha_cancela == '' ? '' : date('d-m-Y h:i:s A', strtotime( $tramite->fecha_cancela )) ?>
			</td>
			<td width="210">
				<h4>Se aplico:</h4>
				Fecha: <?= $tramite->fecha_aplica == '' ? '' : date('d-m-Y h:i:s A', strtotime( $tramite->fecha_aplica )) ?>
			</td>
		</tr>
	</table>
	</div>

<table>
	<tr>
		<td width="490">
			<div class="panel panel-primary">
			    <div class="panel-heading">
			        <h3 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">Datos del trámite</h3>
			    </div>
			    <div class="panel-body">

				    	<strong>Monto documento:</strong> <span style="float:right"><?= number_format($tramite->cantidad_monto_documento,2) ?></span><br>
				    	<strong>Monto crédito:</strong> <span style="float:right"><?= number_format($_monto_credito,2) ?></span><br>
				    	<strong>Monto débito:</strong> <span style="float:right"><?= number_format($_monto_debito,2) ?></span><br>
				    	<strong>Saldo:</strong> <span style="float:right"><?= number_format($tramite->monto_saldo_pago,2) ?></span><br>

				    	<strong>% descuento:</strong> <span style="float:right"><?= $tramite->porcentaje_descuento ?></span><br>
				    	<strong>Monto descuento:</strong> <span style="float:right"><?= number_format($_monto_descuento,2) ?></span><br>
				    	<strong>Monto de pago:</strong> <span style="float:right"><?= number_format($tramite->monto_tramite_pagar,2) ?></span><br>
				    	<strong>Detalle:</strong> <span style="float:right"><?= $tramite->detalle ?></span><br>
				    	<strong>Prioridad:</strong> <span style="float:right"><?= $tramite->prioridad_pago ?></span><br>

			    </div>
			</div>
		</td>
		<td width="20">
		</td>
		<td width="490">
			<div class="panel panel-primary">
			    <div class="panel-heading">
			        <h3 style="text-align:right; color: #0e4595; font-family: Segoe UI, sans-serif;">Datos del pago</h3>
			    </div>
			    <div class="panel-body">
		    		<strong>Medio pago:</strong> <span style="float:right"><?= strtoupper($mediopago->medio_pago) ?></span><br>
		    		<strong>Tip.Docto:</strong> <span style="float:right"><?= $mediopago->medio_pago == 'efectivo' ? 'No aplica' : $documento->descripcion ?></span><br>
		    		<strong>Núm.Cheque:</strong> <span style="float:right"><?= $mediopago->medio_pago == 'cheque' ? $mediopago->numero_cheque : 'No aplica' ?></span><br>
		    		<strong><?= $mediopago->medio_pago == 'transferencia' ? 'Documento banco:' : 'Núm.Recibo Cancel:' ?></strong> <span style="float:right"><?= $mediopago->numero_recibo_cancelacion ?></span><br>

				    <strong>Cnta.banc:</strong> <span style="float:right"><?= $mediopago->medio_pago == 'efectivo' ? 'No aplica' : $cuenta_lo->numero_cuenta ?></span><br>
		    		<strong>Nombre agente:</strong> <span style="float:right"><?= $mediopago->medio_pago == 'transferencia' ? 'No aplica' : $mediopago->nombre_agente ?></span><br>
		    		<strong>Contacto pago:</strong> <span style="float:right"><?= $contacto_pag->nombre ?></span><br>
		    		<strong>Fecha documento:</strong> <span style="float:right"><?= $mediopago->medio_pago == 'cheque' ? date('d-m-Y', strtotime( $mediopago->fecha_documento )) : 'No aplica' ?></span><br>
		    		<strong>Cnta.banc proveedor:</strong> <span style="float:right"><?= $mediopago->medio_pago == 'transferencia' ? $cuenta_pr->cuenta_bancaria : 'No aplica' ?></span><br>

			    </div>
			</div>
		</td>
	</tr>
</table>
<?php
$movimientofactura = MovimientoPagar::find()->where(['idTramite_pago' => $tramite->idTramite_pago])->all();
if (@$movimientofactura) {

echo '<table><tr><th width="450"><h4 style="text-align:left; color: #0e4595; font-family: Segoe UI, sans-serif;">Notas de crédito aplicadas al trámite</h4>
        <table class="items table table-striped" id="tabla_facturas_proceso"  >';
    echo '<thead>';
    printf('<tr>
        <th><font face="arial" size=1>%s</font></th>
        <th><font face="arial" size=1>%s</font></th>
        <th><font face="arial" size=1>%s</font></th></tr>',
            'N° NOTA CREDITO',
            'DETALLE',
            'MONTO'
            );
    echo '</thead>';
    echo '<tbody>';
    foreach($movimientofactura as $position => $movi) {

        printf('<tr>
                    <td><font face="arial" size=2>%s</font></td>
                    <td><font face="arial" size=1>%s</font></td>
                    <td align="right"><font face="arial" size=2>%s</font></td></tr>',
                $movi['idMov'],
                $movi['detalle'],
                number_format($movi['monto_movimiento'],2)
            );
    }
    echo '</tbody>';
echo '</table></th></tr></table>';
}
echo '<div class="col-lg-12"><div class="well"><br><br>
			<table Border=1 bordercolor="blue" Width=100% cellspacing="0" >';
                echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    <th><font face="arial" size=2>%s</font></th>
                    </tr>',
                        'N° ENTR',
                        'N° FACT',
                        'FECH.REG',
                        'FECH.VEN',
                        'MONTO',
                        'NOT.DÉB',
                        'NOT.CRÉD',
                        'ABONO',
                        'SALDO.FACT'
                        );
                echo '</thead>';
                echo '<tbody>';
                foreach($facturasCredito as $position => $factura) {
                    $numFactura = "'".$factura['numFactura']."'";
                    $command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND numFactura = " . $numFactura);
                    $command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND numFactura = " . $numFactura);
                    $command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND numFactura = " . $numFactura);
                    $suma_monto_nc = floatval($command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
                    $suma_monto_nd = floatval($command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
                    $suma_monto_ab = floatval($command_ab->queryScalar()); //ingreso la suma del monto de abonos

                    $fecha_registro = date('d-m-Y', strtotime( $factura['fechaRegistro'] ));
                    $fecha_vencimiento = date('d-m-Y', strtotime( $factura['fechaVencimiento'] ));
                    printf('<tr>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            </tr>',
                                $factura['idCompra'],
                                $factura['numFactura'],
                                $fecha_registro,
                                $fecha_vencimiento,
                                number_format($factura['total'],2),
                                number_format($suma_monto_nd,2),
                                number_format($suma_monto_nc,2),
                                number_format($suma_monto_ab,2),
                                number_format($factura['credi_saldo'],2)
                                );
                }
                echo '</tbody>';
        echo '</table>
        </div></div>';

?>
