<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TramitePagoConfirmar */

$this->title = 'Update Tramite Pago Confirmar: ' . ' ' . $model->idTramite_pago;
$this->params['breadcrumbs'][] = ['label' => 'Tramite Pago Confirmars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTramite_pago, 'url' => ['view', 'id' => $model->idTramite_pago]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tramite-pago-confirmar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
