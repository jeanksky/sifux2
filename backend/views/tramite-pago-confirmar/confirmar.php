<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Proveedores;
use backend\models\ComprasInventario;
use backend\models\TramitePagoConfirmar;
use yii\bootstrap\Modal;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TramitePagoConfirmarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Confirmar cancelación de trámites de pagos';
$this->params['breadcrumbs'][] = ['label' => 'Trámite de pagos a proveedores', 'url' => ['tramite-pago/index']];
$this->params['breadcrumbs'][] = ['label' => 'Cancelar trámites de pagos pendientes', 'url' => ['tramite-pago-confirmar/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  #modalFacturaTramite .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
  #modalcancelartramite .modal-dialog{
    width: 40%!important;
    }
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">

  function buscar_tramite_confirmar() {
    id_tramite = document.getElementById('id_tramite').value;
    fecha_busqueda = document.getElementById('fecha_busqueda').value;
    id_proveedor = document.getElementById('id_proveedor').value;
    prioridad = document.getElementById('prioridad').value;

    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/buscar_tramite_confirmar') ?>" ,
        { 'id_tramite' : id_tramite, 'fecha_busqueda' : fecha_busqueda, 'id_proveedor' : id_proveedor,
        'prioridad' : prioridad },
        function( data ) {
          $('#panel-listatramiteconfirmar').html(data);
        });
  }
  //Marcador de facturas para obtener datos a manipular
  function marcartramite(x, id, idProv) {
    var elementos = document.getElementById('tabla_tramites_pendientes').
    getElementsByTagName('tbody')[0].getElementsByTagName('tr');

    // Por cada TR empezando por el segundo, ponemos fondo blanco.
    for (var i = 0; i < elementos.length; i++) {
        elementos[i].style.background='white';
    }
    // Al elemento clickeado le ponemos fondo amarillo.
    x.style.background="yellow";
    inyectardatostramite(id, idProv);
  }
  //inyecta los datos del tramite que selecciono en la lista de tramites
  function inyectardatostramite(id, idProv) {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/inyectardatostramite') ?>",
        type:"post",
        dataType: 'json',
        data: { 'id' : id, 'idProv' : idProv },
        success: function(notif){
            //$("#cant_nc_tr").load(location.href+' #cant_nc_tr');
            document.getElementById("div_proveedor").innerHTML = notif.proveedor;
            document.getElementById("div_entidad_financiera").innerHTML = notif.entidad_financiera;
            document.getElementById("div_dcp_cu_ba_pr").innerHTML = notif.dcp_cu_ba_pr;
            document.getElementById("div_cuent_ba_pr").innerHTML = notif.cuent_ba_pr;
            document.getElementById("div_total_cancelar").innerHTML = notif.total_cancelar;
            //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
            $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/tabla_facturas') ?>" , { 'id' : id, 'idProv' : idProv } ,
            function( data ) {
              $('#tabla_facturas').html(data);
              if (notif.documento_banco == 'no') {
                $('#b_cancelar_tramite').addClass('disabled');
              }
            });
        },
        error: function(msg, status,err){
         alert('Error linea 67');
        }
    });
  }
  //Modal que me muestra la factura de facturas pendientes
    $(document).on('click', '#activity-index-link-factura-tramite', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $( "#mostrar_factura_tramite" ).html( data );
                        $('#modalFacturaTramite').modal();
                    }
                );
            }));
  //Funsion para reversar un tramite
  function reversar_tramite(idTramite_pago) {
    var confirm_nc_create = confirm("¿Está seguro de reversar este trámite?");
    if (confirm_nc_create == false) {}
    else {
      $.ajax({
              url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/reversar_tramite') ?>",
              type:"post",
              data: { 'idTramite_pago' : idTramite_pago },
              success: function(notif){
              },
              error: function(msg, status,err){
               //alert('No pasa, ver linea 74');
              }
          });
    }
  }

  //funcion para mostrar modal que muestra el tramite antes de la cancelación
  function modal_pagar_tramite(idTramite_pago) {
    $.ajax({
              url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/modal_pagar_tramite') ?>",
              type:"post",
              dataType: 'json',
              data: { 'idTramite_pago': idTramite_pago },
              success: function(data){
                $('#no_tramite').val(data.no_tramite);
                $('#fecha_registra').val(data.fecha_registra);
                $('#proveedor').val(data.proveedor);
                $('#monto_pagar').val(data.monto_pagar);
                $('#tipo_movimiento').val(data.tipo_movimiento);
                $('#doc_banco').val(data.documento_banco);
                $('#usuario_cancela').val(data.usuario_cancela);
                $('#fecha_cancelacion').val(data.fecha_cancelacion);
                $('#cuenta_bancaria_local').val(data.cuenta_bancaria_local);
                $('#observaciones').val(data.observaciones);
                $('#estado_tramite').val(data.estado_tramite);
                $('#cuenta_proveedor').val(data.cuenta_proveedor);
                $('#contacto').val(data.contacto);
                $('#e_contacto').val(data.e_contacto);
                //$('#ddl-idCategoria_').val(data.idCategoria).change();
              },
              error: function(msg, status,err){
               alert('Error, consulte linea 177 (informar a Nelux)');
              }
          });
  }
  $(document).ready(function () {
    //$('#b_cancelar_tramite').addClass('disabled');//mantenemos el boton de cancelar tramite desabilitado
  });

  //accion que me permite cancelar el tramite
  function cancelar_tramite_pendiente() {
    no_tramite = document.getElementById('no_tramite').value;
    doc_banco = document.getElementById('doc_banco').value;
    observaciones = document.getElementById('observaciones').value;
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/cancelar_tramite_pendiente') ?>",
              type:"post",
              data: { 'no_tramite' : no_tramite, 'doc_banco' : doc_banco, 'observaciones' : observaciones },
              success: function(data){
              },
              error: function(msg, status,err){
               alert('Error, consulte linea 195 (informar a Nelux)');
              }
      });
  }

  //accion que me permite anular el tramite
  function anular_tramite(idTramite_pago, idProv) {
    detalle_anulacion = document.getElementById('detalle_anulacion').value;
    if (detalle_anulacion=='') {
      alert('Por favor escriba el motivo de anulación');
    } else {
      var confirm_anul_tramite = confirm("¿Está seguro de anular este trámite?");
      if (confirm_anul_tramite == false) {}
      else {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/anular_tramite') ?>",
                type:"post",
                data: { 'idTramite_pago' : idTramite_pago, 'detalle_anulacion' : detalle_anulacion, 'idProv' : idProv },
                success: function(notif){
                },
                error: function(msg, status,err){
                 //alert('No pasa, ver linea 74');
                }
            });
      }
    }

  }

  //aplicar cancelacion de tramite
  function aplicar_tramite(idTramite_pago, this_boton) {
    var confirm_pago_tramite = confirm("¿Está seguro de confirmar el pago de este trámite?");
    if (confirm_pago_tramite == false) {}
    else {
      var $btn = $(this_boton).button('loading');//me obtiene el boton para ponerlo a cargar
      setTimeout(function () {//reseteamos el boton
          $btn.button('reset');
      }, 60000 );
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago-confirmar/aplicar_tramite_pendiente') ?>",
              type:"post",
              data: { 'idTramite_pago' : idTramite_pago },
              success: function(data){
              },
              error: function(msg, status,err){
               //alert('Error, consulte linea 209 (informar a Nelux)');
              }
      });
    }
  }

  //funcion para refrescar el div panel-listatramiteconfirmar y refresque los tramites pendientes de confirmar pago
  function refrescarpanelistatramiteconfirmar(){
      $("#panel-listatramiteconfirmar").load(location.href+' #panel-listatramiteconfirmar',''); //refrescar el div
  }
  setInterval('refrescarpanelistatramiteconfirmar()',60000);

</script>
<div class="tramite-pago-confirmar-confirmar">
  <center><h1><?= Html::encode($this->title) ?></h1></center>
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
  <?= AlertBlock::widget([
          'type' => AlertBlock::TYPE_ALERT,
          'useSessionFlash' => true,
          'delay' => 20000
      ]);?>
      <div class="col-lg-8 alert alert-info">
        <div class="col-lg-2">
          <?= Html::input('text', '', '', ['size' => '30', 'id' => 'id_tramite', 'class' => 'form-control', 'placeholder' =>'ID TRÁMITE', 'onKeyUp' => 'javascript:buscar_tramite_confirmar()']) ?>
        </div>
        <div class="col-lg-2">
          <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
            'options' => ['class'=>'form-control', 'placeholder'=>'FECHA', 'onchange'=>'javascript:buscar_tramite_confirmar()',
            'id'=>'fecha_busqueda']])
          ?>
        </div>
        <div class="col-lg-5">
          <?= Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
            'name' => '',
            'data' => ArrayHelper::map(Proveedores::find()->all(), 'codProveedores',
                function($element) {
                return $element['codProveedores'].' - '.$element['nombreEmpresa'];
            }),
            'options' => [
                'placeholder' => 'PROVEEDOR',
                'id'=>'id_proveedor',
                'onchange' => 'javascript:buscar_tramite_confirmar()',
                'multiple' => false //esto me ayuda a que solo se obtenga uno
            ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
          ]) ?>
        </div>
        <div class="col-lg-3">
          <?= Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
            'name' => '',
            'data' => ["Alta" => "Alta", "Media" => "Media", "Baja" => "Baja"],
            'options' => [
                'placeholder' => 'PRIORIDAD',
                'id'=>'prioridad',
                'onchange' => 'javascript:buscar_tramite_confirmar()',
                'multiple' => false //esto me ayuda a que solo se obtenga uno
            ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
          ]) ?>
        </div>
        <div id="notificacion_lista_tramite"></div><!-- en este div me muestra las alertas -->
      </div>
      <div class="col-lg-12 table-responsive">
        <?php  //llamo la vista con los tramites pendientes de cancelar
            echo '<div style="height: 300px;width: 100%; overflow-y: auto; ">';
            echo '<div id="panel-listatramiteconfirmar">';
            echo $this->render('listatramiteconfirmar', [
                'id_tramite' => null, 'fecha_busqueda' => '',
                'idProveedor' => null, 'prioridad' => '']);
            echo '</div>';
            echo '</div>';
        ?>
        <center><h1><div id="div_proveedor"></div></h1></center>
        <div class="col-lg-3">
          <h3>Entidad financiera: <br><span style="float:right"><small><font size=5><div id="div_entidad_financiera">----</div></font></small></span></h3>
        </div>
        <div class="col-lg-3">
          <h3>Dcp cnt.banc proveedor: <br><span style="float:right"><small><font size=5><div id="div_dcp_cu_ba_pr">----</div></font></small></span></h3>
        </div>
        <div class="col-lg-3">
          <h3>Cnt.banc proveedor: <br><span style="float:right"><small><font size=5><div id="div_cuent_ba_pr">----</div></font></small></span></h3>
        </div>
        <div class="col-lg-3">
          <h3>Total a cancelar:  <br><span style="float:right"><small><font size=5><div id="div_total_cancelar">0.00</div></font></small></span></h3><br><br>
        </div>

        <div id="tabla_facturas">
          <?php
          echo '<div class="col-lg-12"><div class="well">
                <table class="items table table-striped" id="tabla_facturas_pendientes"  >';
                          echo '<thead class="thead-inverse">';
                          printf('<tr>
                              <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:center"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                              <th style="text-align:right"><font face="arial" size=2>%s</font></th>
                              <th class="actions button-column">&nbsp;</th></tr>',
                                  'N° ENTRADA',
                                  'N° FACTURA',
                                  'FECH.REG',
                                  'FECH.VEN',
                                  'MONTO',
                                  'NOTA DÉBITO',
                                  'NOTA CRÉDITO',
                                  'ABONO',
                                  'SALDO FACTURA'
                                  );
                          echo '</thead>';
                          echo '<tbody class="buscar1">';
                          echo '</tbody>';
                  echo '</table>
                  </div></div>';
          ?>
        </div>
      </div>
</div>
<?php
//-------------------------------------------------Pantallas modales-----------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------
    //Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalFacturaTramite',
                //'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Compra / Factura</h4></center>',
                'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='panel-body'><div id='mostrar_factura_tramite'></div></div>";

            Modal::end();
?>
