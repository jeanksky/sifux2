<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\F_Proforma;
use yii\widgets\MaskedInput;
use yii\bootstrap\Modal;
use backend\models\Clientes;
use backend\models\ProductoServicios;
use yii\helpers\Url;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $model backend\models\Proforma */
$estado = F_Proforma::getEstadoFacturau();

$this->title = 'Actualización de proforma: ' . ' ' . $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Proformas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idCabeza_Factura, 'url' => ['view', 'id' => $model->idCabeza_Factura]];
$this->params['breadcrumbs'][] = 'Actualizar';

?>

<?php $products = F_Proforma::getContenidoComprau(); ?>
<?php $vendedor = F_Proforma::getVendedoru(); ?>
<?php $cliente = F_Proforma::getClienteu(); ?>
<?php //F_Proforma::setTotalauxiliar($model->total_a_pagar); ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
window.onkeydown = tecla;

	function tecla(event) {
		//event.preventDefault();
		num = event.keyCode;

		if(num==113)
			$('#modalinventario').modal('show');//muestro la modal

	}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function(){
   // $('[data-toggle="modal"]').tooltip();
    $('[data-toggle="modal"]').popover();
});

$(document).on('click', '#orden-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //document.getElementById("well-create-movimiento").innerHTML="";//tengo que destruir el div de well-create-movimiento para que well-update-movimiento esté limpio para la modal
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-update-orden').html(data);//muestro en el div los datos del create
                        $('#actualizarordenmodal').modal('show');//muestro la modal

                    }
                );
            }));


function obtenerClienteCod(){

        var idCliente = document.getElementById("cod-cliente").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/cliente2-u') ?>",
            type:"post",
            data: {'idCliente': idCliente},
        }) ;
    }

function obtenerCliente(){

    var idCliente = document.getElementById("ddl-cliente").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/cliente2-u') ?>",
        type:"post",
        data: {'idCliente': idCliente},
        /*success: function(data){
         $('#resultado').val(data);
        },
        error: function(msg, status,err){
                        $('#resultado').val('Dato no encontrado');
                    }*/
    }) ;
}


function agrega_producto(codProdServicio){
    //var codProdServicio = document.getElementById("codProdServiciou").value;
    var descuento = $("#descuentou").val();
    var id = "";
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/additemu') ?>",
        type:"post",
        data: { 'codProdServicio': codProdServicio, 'cantidad': 1, 'descuento' : descuento , 'idCabeza' : "<?php echo $model->idCabeza_Factura ?>"},
        success: function(data){
        //$('#result').html(data);
        //alert('si pasa '+ data);
        $("#codProdServiciou").select2("val", ""); //vaciar contenido de select2
        //location.reload();
        //$("#conten_pro").load(location.href+' #conten_pro',''); //refrescar el div
        //document.getElementById("alerta_r").innerHTML = '<div class="alert alert-info alert-dismissable">                      <button type="button" class="close" data-dismiss="alert">&times;</button><strong>¡Cuidado!</strong> Es muy importante que leas este mensaje de alerta.</div>';
        },
        error: function(msg, status,err){
                        //alert('no pasa');
                        //$("#conten_pro").load(location.href+' #conten_pro',''); //refrescar el div
                        //$("#conten_pro").load(location.href+' #conten_pro', {id:id});
                        //location.reload();
                    }
    }) ;
}
//setTimeout('obtenerCliente()', 500);

//---------------------------------CALCULOS PARA PRECIOS DEL PRODUCTO TEMPORAL---------------------
function abilitar_btn_agregar_temporal() {
  codigo_temporal = document.getElementById("codigo_temporal").value;
  descripcion_temporal = document.getElementById("descripcion_temporal").value;
  cantidad_temporal = document.getElementById("cantidad_temporal").value;
  precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
  porc_ganancias_temporal = document.getElementById("porc_ganancias_temporal").value;
  precio_unit_gan_temporal = document.getElementById("precio_unit_gan_temporal").value;
  precio_mas_iva_temporal = document.getElementById("precio_mas_iva_temporal").value;
  if (codigo_temporal==''	|| descripcion_temporal=='' || cantidad_temporal=='' || precio_unit_prov_temporal=='' ||
  porc_ganancias_temporal=='' || precio_unit_gan_temporal=='' || precio_mas_iva_temporal=='') {
    $('#btn_agregar_temporar').addClass('disabled');
  } else {
    $('#btn_agregar_temporar').removeClass('disabled');
  }
}

function obtenerCantidad(esc) {
  obtenerPrecioUnitario(esc);
}

function obtenerPrecioUnitario(esc) {
  abilitar_btn_agregar_temporal();
  var descuento = $("#descuentou").val();
  precio_unit_prov_temporal = cantidad_temporal = porc_ganancias_temporal = '';
  if (esc==1) {
    precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
    cantidad_temporal = document.getElementById("cantidad_temporal").value;
    porc_ganancias_temporal = document.getElementById("porc_ganancias_temporal").value;
  } else {
    precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
    cantidad_temporal = document.getElementById("_cantidad_temporal").value;
    porc_ganancias_temporal = document.getElementById("_porc_ganancias_temporal").value;
    descuento = document.getElementById("_porciento_descuento").value;
  }

  $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/preciounitario') ?>",
          type:"post",
          dataType: 'json',
          data: { 'precio_unit_prov_temporal' : precio_unit_prov_temporal, 'cantidad_temporal' : cantidad_temporal,
                    'porc_ganancias_temporal' : porc_ganancias_temporal, 'descuento' : descuento },
          success: function(notif){
            if (esc==1) {
              $('#precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
              $('#precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
              $("#descuento_div").html(notif.monto_descuento);
              $('#descuento_hi').val(notif.monto_descuento);//hidden oculto
              $("#precio_total_ganacia").html(notif.precio_total_ganacia);
              $('#precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
              $("#precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
              $('#precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
            } else {
              $('#_precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
              $('#_precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
              $("#_descuento_div").html(notif.monto_descuento);
              $('#_descuento_hi').val(notif.monto_descuento);//hidden oculto
              $("#_precio_total_ganacia").html(notif.precio_total_ganacia);
              $('#_precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
              $("#_precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
              $('#_precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
            }
          },
          error: function(msg, status,err){
           //alert('Error linea 148');
          }
      });
}

function obtenerGanancia(esc) {
  obtenerPrecioUnitario(esc);
}

function obtenerPrecioGanancia(esc) {
  abilitar_btn_agregar_temporal()
  precio_unit_gan_temporal = cantidad_temporal = precio_unit_prov_temporal = 0;
  var descuento = $("#descuentou").val();
  if (esc==1) {
    precio_unit_gan_temporal = document.getElementById("precio_unit_gan_temporal").value;
    cantidad_temporal = document.getElementById("cantidad_temporal").value;
    precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
  } else {
    precio_unit_gan_temporal = document.getElementById("_precio_unit_gan_temporal").value;
    cantidad_temporal = document.getElementById("_cantidad_temporal").value;
    precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
    descuento = document.getElementById("_porciento_descuento").value;
  }
  $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/precioganancia') ?>",
          type:"post",
          dataType: 'json',
          data: { 'precio_unit_gan_temporal' : precio_unit_gan_temporal, 'cantidad_temporal' : cantidad_temporal,
                    'precio_unit_prov_temporal' : precio_unit_prov_temporal, 'descuento' : descuento },
          success: function(notif){
            if (esc==1) {
              $('#porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
              $('#precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
              $("#descuento_div").html(notif.monto_descuento);
              $('#descuento_hi').val(notif.monto_descuento);//hidden oculto
              $("#precio_total_ganacia").html(notif.precio_total_ganacia);
              $('#precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
              $("#precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
              $('#precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
            } else {
              $('#_porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
              $('#_precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
              $("#_descuento_div").html(notif.monto_descuento);
              $('#_descuento_hi').val(notif.monto_descuento);//hidden oculto
              $("#_precio_total_ganacia").html(notif.precio_total_ganacia);
              $('#_precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
              $("#_precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
              $('#_precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
            }
          },
          error: function(msg, status,err){
           //alert('Error linea 180');
          }
      });
}

$(function(){
	$('input[name="checkiva"]').on('click', function() {
			if ( $(this).is(':checked') )
					$('#checkiva').val('si');
			else
					$('#checkiva').val('no');
	});

	$('input[name="checkiva_"]').on('click', function() {
			if ( $(this).is(':checked') )
					$('#checkiva_').val('si');
			else
					$('#checkiva_').val('no');
	});
});

function obtenerPrecioIVA(esc) {
  abilitar_btn_agregar_temporal()
  precio_mas_iva_temporal = cantidad_temporal = precio_unit_prov_temporal = '';
  var descuento = $("#descuentou").val();
  if (esc==1) {
    precio_mas_iva_temporal = document.getElementById("precio_mas_iva_temporal").value;
    cantidad_temporal = document.getElementById("cantidad_temporal").value;
    precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
  } else {
    precio_mas_iva_temporal = document.getElementById("_precio_mas_iva_temporal").value;
    cantidad_temporal = document.getElementById("_cantidad_temporal").value;
    precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
    descuento = document.getElementById("_porciento_descuento").value;
  }
  $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/precioiva') ?>",
          type:"post",
          dataType: 'json',
          data: { 'precio_mas_iva_temporal' : precio_mas_iva_temporal, 'cantidad_temporal' : cantidad_temporal,
                    'precio_unit_prov_temporal' : precio_unit_prov_temporal, 'descuento' : descuento },
          success: function(notif){
            if (esc==1) {
              $('#porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
              $('#precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
              $("#descuento_div").html(notif.monto_descuento);
              $('#descuento_hi').val(notif.monto_descuento);//hidden oculto
              $("#precio_total_ganacia").html(notif.precio_total_ganacia);
              $('#precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
              $("#precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
              $('#precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
            } else {
              $('#_porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
              $('#_precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
              $("#_descuento_div").html(notif.monto_descuento);
              $('#_descuento_hi').val(notif.monto_descuento);//hidden oculto
              $("#_precio_total_ganacia").html(notif.precio_total_ganacia);
              $('#_precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
              $("#_precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
              $('#_precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
            }
          },
          error: function(msg, status,err){
           alert('Error linea 123');
          }
      });
}

//envia la categoria que selecciono para obtener los datos de dicha categoria
function actualiza_producto_temporal(position,codigo_temporal, descripcion_temporal, cantidad_temporal,
                                     precio_unit_prov_temporal, porc_ganancias_temporal, precio_unit_gan_temporal,
                                     precio_mas_iva_temporal, descuento_div, precio_total_ganacia, precio_mas_iva_total_temporal, checkiva, dcto) {

  $('#codigo_id').val(position);
  $('#_codigo_temporal').val(codigo_temporal);
  $('#_descripcion_temporal').val(descripcion_temporal);
  $('#_cantidad_temporal').val(cantidad_temporal);
  $('#_precio_unit_prov_temporal').val(precio_unit_prov_temporal);
  $('#_porc_ganancias_temporal').val(porc_ganancias_temporal);
  $('#_precio_unit_gan_temporal').val(precio_unit_gan_temporal);
  $('#_precio_mas_iva_temporal').val(precio_mas_iva_temporal);
  $("#_descuento_div").html(descuento_div);
  $("#_precio_total_ganacia").html(precio_total_ganacia);
  $("#_precio_mas_iva_total_temporal").html(precio_mas_iva_total_temporal);
  $("#_porciento_descuento").val(dcto);
  $("#_descuento_hi").val(descuento_div);
  $("#_precio_total_ganacia_hi").val(precio_total_ganacia);
  $("#_precio_mas_iva_total_temporal_hi").val(precio_mas_iva_total_temporal);
	$('#checkiva_').val(checkiva);
	if (checkiva == 'si') {
		$('input[name="checkiva_"]').prop('checked',true);
	} else {
		$('input[name="checkiva_"]').prop('checked' , false);
	}
}
//Transformar a mayúsculas todos los caracteres
function Mayuculas(tx){
	//Retornar valor convertido a mayusculas
	return tx.toUpperCase();
}
//-------------------------------------------------------------------------------------------------

//AGREGAR PRODUCTO TEMPORAL
function agrega_producto_temporal() {
  var idCabeza = '<?= $model->idCabeza_Factura ?>';
  var descuento = $("#descuentou").val();
  var codigo_temporal = document.getElementById("codigo_temporal").value;
  var descripcion_temporal = document.getElementById("descripcion_temporal").value;
  var cantidad_temporal = document.getElementById("cantidad_temporal").value;
  var precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
  var porc_ganancias_temporal = document.getElementById("porc_ganancias_temporal").value;
  var precio_unit_gan_temporal = document.getElementById("precio_unit_gan_temporal").value;
  var precio_mas_iva_temporal = document.getElementById("precio_mas_iva_temporal").value;
  var descuento_hi = document.getElementById("descuento_hi").value;//hidden
  var precio_total_ganacia_hi = document.getElementById("precio_total_ganacia_hi").value;//hidden
  var precio_mas_iva_total_temporal_hi = document.getElementById("precio_mas_iva_total_temporal_hi").value;//hidden
	var checkiva = document.getElementById("checkiva").value;
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/additem_temporal_update') ?>",
      type:"post",
      data: { 'idCabeza' : idCabeza, 'descuento': descuento, 'codigo_temporal': codigo_temporal, 'descripcion_temporal' : descripcion_temporal,
              'cantidad_temporal': cantidad_temporal, 'precio_unit_prov_temporal': precio_unit_prov_temporal,
              'porc_ganancias_temporal': porc_ganancias_temporal, 'precio_unit_gan_temporal': precio_unit_gan_temporal,
              'precio_mas_iva_temporal': precio_mas_iva_temporal, 'descuento_hi' : descuento_hi,
              'precio_total_ganacia_hi' : precio_total_ganacia_hi, 'precio_mas_iva_total_temporal_hi' : precio_mas_iva_total_temporal_hi,
							'checkiva' : checkiva },
      success: function(data){
      },
      error: function(msg, status,err){
        //alert('Error 206');
      }
  });
}

//modificar producto temporal seleccionado
function modificar_producto_temporal() {
  //var codigo_id = document.getElementById("codigo_id").value;
  var idCabeza = '<?= $model->idCabeza_Factura ?>';
  var descuento = document.getElementById("_porciento_descuento").value;
  var codigo_temporal = document.getElementById("_codigo_temporal").value;
  var descripcion_temporal = document.getElementById("_descripcion_temporal").value;
  var cantidad_temporal = document.getElementById("_cantidad_temporal").value;
  var precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
  var porc_ganancias_temporal = document.getElementById("_porc_ganancias_temporal").value;
  var precio_unit_gan_temporal = document.getElementById("_precio_unit_gan_temporal").value;
  var precio_mas_iva_temporal = document.getElementById("_precio_mas_iva_temporal").value;
  var descuento_hi = document.getElementById("_descuento_hi").value;//hidden
  var precio_total_ganacia_hi = document.getElementById("_precio_total_ganacia_hi").value;//hidden
  var precio_mas_iva_total_temporal_hi = document.getElementById("_precio_mas_iva_total_temporal_hi").value;//hidden
	var checkiva_ = document.getElementById("checkiva_").value;
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/moditem_temporal_update') ?>",
      type:"post",
      data: { /*'codigo_id' : codigo_id, */'idCabeza': idCabeza ,'descuento': descuento, 'codigo_temporal': codigo_temporal, 'descripcion_temporal' : descripcion_temporal,
              'cantidad_temporal': cantidad_temporal, 'precio_unit_prov_temporal': precio_unit_prov_temporal,
              'porc_ganancias_temporal': porc_ganancias_temporal, 'precio_unit_gan_temporal': precio_unit_gan_temporal,
              /*'precio_mas_iva_temporal': precio_mas_iva_temporal, 'descuento_hi' : descuento_hi,*/
              'precio_total_ganacia_hi' : precio_total_ganacia_hi, 'precio_mas_iva_total_temporal_hi' : precio_mas_iva_total_temporal_hi,
						 	'checkiva_' : checkiva_ },
      success: function(data){
      },
      error: function(msg, status,err){
        //alert('Error 206');
      }
  });
}

function obtenerVendedor(){

    var idFuncionario = document.getElementById("ddl-codigoVendedor").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/vendedor2-u') ?>",
        type:"post",
        data: {'idFuncionario': idFuncionario},
        success: function(data){
         $('#resultado2').val(data);
        },
        error: function(msg, status,err){
                        $('#resultado2').val('Dato no encontrado');
                    }
    }) ;
}
//setTimeout('obtenerVendedor()', 500);
function refrescarbotones(){
    $("#rebotons").load(location.href+' #rebotons',''); //refrescar el div
}
setTimeout('refrescarbotones()',50);


$(document).on('click', '#activity-index-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.well-ord-serv-prof').html(data);
                        $('#modalOrden').modal();
                    }
                );
            }));
$(document).on('click', '#activity-index-link-inventario', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalinventario').modal();
                    }
                );
            }));
//activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
    });

    //me permite buscar por like los productos a BD
		function buscar() {
        var textoBusqueda_cod = $("input#busqueda_codi").val();
        var textoBusqueda_des = $("input#busqueda_des").val();
				var textoBusqueda_fam = $("input#busqueda_fam").val();
        var textoBusqueda_cat = $("input#busqueda_cat").val();
        var textoBusqueda_cpr = $("input#busqueda_cpr").val();
        var textoBusqueda_ubi = $("input#busqueda_ubi").val();
        if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
            $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');

        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/busqueda_producto') ?>",
                type:"post",
                data: {
                    'valorBusqueda_cod' : textoBusqueda_cod,
                    'valorBusqueda_des' : textoBusqueda_des,
										'valorBusqueda_fam' : textoBusqueda_fam,
                    'valorBusqueda_cat' : textoBusqueda_cat,
                    'valorBusqueda_cpr' : textoBusqueda_cpr,
                    'valorBusqueda_ubi' : textoBusqueda_ubi },
                beforeSend : function() {
                  $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 286');
                }
            });
        }
    }
//Esta funcion filtra los datos en la tabla html pero en este momento no se está en uso
/*$(document).ready(function () {
            (function ($) {
                $('#filtrar').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));
            $('#tabla_lista_productos').dataTable({
            "sPaginationType" : "full_numbers"//DAMOS FORMATO A LA PAGINACION(NUMEROS)
            });
        });*/


function regresarindex(){
    var url = "<?php echo Url::toRoute('proforma/index') ?>";
    window.location.href = url;
}

//funcion para obtener observacion
function agrega_observacion(observacion){
    var idCabeza = '<?php echo $model->idCabeza_Factura ?>';
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/addobservacion-u') ?>",
        type:"post",
        data: {'observacion': observacion, 'idCabeza': idCabeza},
    }) ;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function enviar_correo_proforma(id) {
	var confirm_pago = confirm("¿Está seguro de enviar esta proforma?");
	if (confirm_pago == true) {
	var email_cliente = document.getElementById("email_cliente").value;
	if (validateEmail(email_cliente)) {
	$.ajax({
							url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/enviar_correo_proforma') ?>",
							type:"post",
							data: { 'id' : id, 'email_cliente' : email_cliente },
							beforeSend: function() {
									document.getElementById("notificacion_envio_correo").innerHTML = '<center><span class="label label-info" style="font-size:15pt;">Espere, envío de proforma en proceso...</span></center><br>';
							},
							success: function(notif){
									document.getElementById("notificacion_envio_correo").innerHTML = notif;
									setTimeout(function() {//aparece la alerta en un milisegundo
											$("#notificacion_envio_correo").fadeIn();
									});
									setTimeout(function() {//se oculta la alerta luego de 4 segundos
											$("#notificacion_envio_correo").fadeOut();
									},5000);
							},
							error: function(msg, status,err){
								 document.getElementById("notificacion_envio_correo").innerHTML = '<center><span class="label label-danger" style="font-size:15pt;">Problemas al enviar cotización.</span></center><br>';
							}
					});
				} else {
					document.getElementById("notificacion_envio_correo").innerHTML = '<center><span class="label label-danger" style="font-size:15pt;">Se requiere una dirección de correo válida, verifique que la escribió correctamente.</span></center><br>';
					setTimeout(function() {//aparece la alerta en un milisegundo
							$("#notificacion_envio_correo").fadeIn();
					});
					setTimeout(function() {//se oculta la alerta luego de 4 segundos
							$("#notificacion_envio_correo").fadeOut();
					},5000);
				}
	}
}

//espacio que solo permite numero y decimal (.)
function isNumberDe(evt) {
    var nav4 = window.Event ? true : false;
    var key = nav4 ? evt.which : evt.keyCode;
    return (key <= 13 || key==46 || (key >= 48 && key <= 57));
}
</script>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }
    #modal_producto_temporal .modal-dialog{
    width: 90%!important;
    /*margin: 0 auto;*/
    }
		#modal_producto_temporal_update .modal-dialog{
    width: 90%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="proforma-update">

    <span style="float:right; font-size: 18pt; color: #5396A4;">Proforma ID: <?= $model->idCabeza_Factura ?></span>
        <div id="rebotons">
            <?= Html::a('<span class="fa fa-arrow-left"></span> Salir', ['regresarborrar-u'], ['class' => 'btn btn-default']) ?>
            <!--?= //Html::a('Imprimir', ['view', 'id'=>$model->idCabeza_Factura], ['class' => 'btn btn-primary']) ?-->
            <?= Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['report', 'id' => $model->idCabeza_Factura], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Imprimir proforma'
                ]) ?>
						<?= Html::a('<i class="fa fa-envelope" aria-hidden="true"></i> Enviar por email', null, [
                    'class'=>'btn btn-info',
                    'data-toggle'=>'tooltip',
										'onclick' => 'javascript:enviar_correo_proforma('.$model->idCabeza_Factura.')',
                    'title'=>'Esta opción permite enviar la cotización al cliente, si es un cliente temporal no registrado asegúrese de escribir el email antes de usar esta opción'
                ]) ?>
						<?= Html::a('<span class="fa fa-floppy-o"></span> Generar pre-factura', ['prefactura', 'id'=>$model->idCabeza_Factura], ['class' => 'btn btn-warning',
						'data' => [
	                      'confirm' => '¿Seguro de generar una pre-factura de esta proforma?',
	                      'method' => 'post',
	                  ]]) ?>
        </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php
//------------------------modal para mostrar orden de servicio
        Modal::begin([
            'id' => 'modalOrden',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Orden de servicio #'.$model->idOrdenServicio.'</h4></center>',
            'footer' => '
            <a href="'.Url::to(['orden-servicio/update', 'id' => $model->idOrdenServicio]).'" class="btn btn-primary" data-toggle="modal" target="_blank" onclick="location.reload()" >Ir a la Orden de Servicio para actualizar</a>
            <a href="#" class="btn btn-primary" onclick="location.reload()" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div class='well-ord-serv-prof'></div></div>";

        Modal::end();

//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        //http://blog.collectivecloudperu.com/buscar-en-tiempo-real-con-jquery-y-bootstrap/
        echo '<div class="well">
				<div class="col-lg-2">
					'.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_codi', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
				</div>
				<div class="col-lg-2">
					'.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
				</div>
				<div class="col-lg-2">
	        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
	      </div>
				<div class="col-lg-2">
					'.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
				</div>
				<div class="col-lg-2">
					'.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
				</div>
				<div class="col-lg-2">
					'.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
				</div>

                <table class="table table-hover" id="tabla_lista_productos">';
        echo '<thead>';
				printf('<tr><th width="150">%s</th>
                    <th width="380">%s</th>
                    <th width="100">%s</th>
                    <th width="130">%s</th>
                    <th width="120">%s</th>
                    <th width="50">%s</th>
                    <th width="140">%s</th>
                    <th class="actions button-column" width="60">&nbsp;</th>
                </tr>',
                        'CÓDIGO LOCAL',
                        'DESCRIPCIÓN',
                        'CANT.INV',
                        'COD.ALTER',
                        'CÓD.PROV',
                        'UBICACIÓN',
                        'PRECIO + IV'
                        );
                echo '</thead>';
              /*  $model_Prod = ProductoServicios::find()->where(["=",'tipo','Producto'])->all();
                echo '<tbody class="buscar">';
                foreach($model_Prod as $position => $product) {
                    $accion = Html::a('', '', ['class'=>'glyphicon glyphicon-plus', 'data-value'=>$product['codProdServicio'],'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Agregar este producto', 'onclick'=>'javascript:agrega_productou($(this).data("value"))']);
                    $presiopro = '₡ '.number_format($product['precioVentaImpuesto'],2);
                    printf('<tr><td>%s</td><td class="des">%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="actions button-column">%s</td></tr>',


                            $product['codProdServicio'],
                            $product['nombreProductoServicio'],
                            $product['cantidadInventario'],
                            $presiopro,
                            $accion
                            );
                }//fin de foreach
                echo '</tbody>';
                */
                echo '</table>
                <div id="resultadoBusqueda" style="height: 400px;width: 100%; overflow-y: auto; "></div>
                </div>';

        Modal::end();

        //------------------------modal para actualizar orden de servicio de proforma
        Modal::begin([
            'id' => 'actualizarordenmodal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Actualizar movimiento en libros</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-update-orden'></div></div>";

        Modal::end();

        //muestra modal para agregar un producto temporal
		    Modal::begin([
		            'id' => 'modal_producto_temporal',
		            //'size'=>'modal-sm',
		            'header' => '<center><h4 class="modal-title">Agregar nuevo producto temporal</h4></center>',
		            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar', '#', ['class' => 'btn btn-success', 'id'=>'btn_agregar_temporar', 'onclick'=>'agrega_producto_temporal()',]),
		        ]);
						echo '<div class="well">';
							echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">
							<div class="col-lg-12"><center>Nota: si desea aplicar un descuento a este producto selecciona el porcentaje donde dice "% descuento" y luego regrese para ingresar el producto.</center><br></div>';
								echo '<div class="col-lg-1"><label>Cód.Producto</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'codigo_temporal', 'class' => 'form-control', 'onkeyup'=>'javascript:abilitar_btn_agregar_temporar()', 'placeholder' =>'Código']);
								echo '</div>';
								echo '<div class="col-lg-3"><label>Descripción del producto</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'descripcion_temporal', 'class' => 'form-control', 'onkeyup'=>'javascript:abilitar_btn_agregar_temporar()', 'placeholder' =>'Descripcion del producto']);
								echo '</div>';
								echo '<div class="col-lg-1"><label>Cantidad</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'cantidad_temporal', 'class' => 'form-control', 'placeholder' =>'Cantidad', 'onkeyup'=>'javascript:obtenerPrecioUnitario(1)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (Proveedor)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_prov_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (Prov)', 'onkeyup'=>'javascript:obtenerPrecioUnitario()']);
									echo MaskedInput::widget(['name' => 'precio_unit_prov_temporal',
											'options' => ['id'=>'precio_unit_prov_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioUnitario(1)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-1"><label>% ganancia</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'porc_ganancias_temporal', 'class' => 'form-control', 'placeholder' =>'% ganancia', 'onkeyup'=>'javascript:obtenerPrecioUnitario(1)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (más ganacia)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_gan_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (+ ganancia)', 'onkeyup'=>'javascript:obtenerPrecioGanancia()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => 'precio_unit_gan_temporal',
											'options' => ['id'=>'precio_unit_gan_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioGanancia(1)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio más IV</label> <span style="float:right">Excluir <input type="checkbox" name="checkiva" /></span><input type="hidden" id="checkiva" value="no">';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_mas_iva_temporal', 'class' => 'form-control', 'placeholder' =>'Precio+iva', 'onkeyup'=>'javascript:obtenerPrecioIVA()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => 'precio_mas_iva_temporal',
											'options' => ['id'=>'precio_mas_iva_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioIVA(1)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '<br></div>';
								echo '<div class="col-lg-12">';
								echo '<div class="col-lg-4"><h3>Descuento: <div id="descuento_div"></div><input type="hidden" id="descuento_hi"></h3></div>
											<div class="col-lg-4"><h3>Precio total más ganancia: <div id="precio_total_ganacia"></div><input type="hidden" id="precio_total_ganacia_hi"></h3></div>
											<div class="col-lg-4"><h3>Precio total más IV: <div id="precio_mas_iva_total_temporal"></div><input type="hidden" id="precio_mas_iva_total_temporal_hi"></h3></div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
				Modal::end();

				//muestra modal para actualizar un producto temporal
		    Modal::begin([
		            'id' => 'modal_producto_temporal_update',
		            //'size'=>'modal-sm',
		            'header' => '<center><h4 class="modal-title">Actualizar producto temporal</h4></center>',
		            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Actualizar', '#', ['class' => 'btn btn-success', 'id'=>'btn_actualizar_temporar', 'onclick'=>'modificar_producto_temporal()',]),
		        ]);
						echo '<div class="well">';
							echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">
							<div class="col-lg-12"><center>Nota: si desea aplicar un descuento a este producto selecciona el porcentaje donde dice "% descuento" y luego regrese para ingresar el producto.</center><br></div>';
								echo '<div class="col-lg-1"><label>Cód.Producto</label><input type="hidden" id="codigo_id">';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_codigo_temporal', 'class' => 'form-control', 'placeholder' =>'Código']);
								echo '</div>';
								echo '<div class="col-lg-3"><label>Descripción del producto</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_descripcion_temporal', 'class' => 'form-control', 'placeholder' =>'Descripcion del producto']);
								echo '</div>';
								echo '<div class="col-lg-1"><label>Cantidad</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_cantidad_temporal', 'class' => 'form-control', 'placeholder' =>'Cantidad', 'onkeyup'=>'javascript:obtenerPrecioUnitario(2)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (Proveedor)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_prov_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (Prov)', 'onkeyup'=>'javascript:obtenerPrecioUnitario()']);
									echo MaskedInput::widget(['name' => '_precio_unit_prov_temporal',
											'options' => ['id'=>'_precio_unit_prov_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioUnitario(2)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-1"><label>% ganancia</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_porc_ganancias_temporal', 'class' => 'form-control', 'placeholder' =>'% ganancia', 'onkeyup'=>'javascript:obtenerPrecioUnitario(2)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (más ganacia)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_gan_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (+ ganancia)', 'onkeyup'=>'javascript:obtenerPrecioGanancia()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => '_precio_unit_gan_temporal',
											'options' => ['id'=>'_precio_unit_gan_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioGanancia(2)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio más IV</label><span style="float:right">Excluir <input type="checkbox" name="checkiva_" /></span><input type="hidden" id="checkiva_">';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_mas_iva_temporal', 'class' => 'form-control', 'placeholder' =>'Precio+iva', 'onkeyup'=>'javascript:obtenerPrecioIVA()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => '_precio_mas_iva_temporal',
											'options' => ['id'=>'_precio_mas_iva_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioIVA(2)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '<br></div>';
								echo '<div class="col-lg-12">';
								echo '<div class="col-lg-4"><h3>Descuento: <div id="_descuento_div"></div><input type="hidden" id="_descuento_hi"><input type="hidden" id="_porciento_descuento"></h3></div>
											<div class="col-lg-4"><h3>Precio total más ganancia: <div id="_precio_total_ganacia"></div><input type="hidden" id="_precio_total_ganacia_hi"></h3></div>
											<div class="col-lg-4"><h3>Precio total más IV: <div id="_precio_mas_iva_total_temporal"></div><input type="hidden" id="_precio_mas_iva_total_temporal_hi"></h3></div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
				Modal::end();
    ?>
