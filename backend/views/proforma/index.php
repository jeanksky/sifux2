<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Clientes;//para obtener el cliente

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProformaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proformas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proforma-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear nueva Proforma', ['create'], ['class' => 'btn btn-success']) ?>
        <!--?= Html::a('Crear una proforma desde una nueva orden de servicio', ['orden-servicio/create_proforma'], ['class' => 'btn btn-info']) ?-->
    </p>
    <div class="table-responsive">
    <div id="semitransparente">
         <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
              'attribute' => 'idCabeza_Factura',
              'label' => 'Código',
              'options'=>['style'=>'width:20px'],
            ],
            [
              'attribute' => 'fecha_inicio',
              'label' => 'Fecha',
              'options'=>['style'=>'width:100px'],
            ],
            [
                        'attribute' => 'tbl_clientes.nombreCompleto',
                        'label' => 'Cliente',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            if($cliente = Clientes::findOne($model->idCliente))
                            {
                                return Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$model->idCliente);
                            }else{
                                if ($model->idCliente=="")
                                return 'Cliente no asignado';
                                else
                                return $model->idCliente;
                            }
                        },
            ],
            ['attribute' =>'estadoFactura','label' => 'Tipo','options'=>['style'=>'width:10%']],
            // 'porc_descuento',
            // 'iva',
            // 'total_a_pagar',
            // 'estadoFactura',
            // 'tipoFacturacion',
            // 'codigoVendedor',
            // 'subtotal',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-print"></span>', ['/proforma/report', 'id' => $model->idCabeza_Factura], [
                                'id' => 'activity-index-link-update',
                                'data-pjax' => '0',
                                'target'=>'_blank',
                                'title' => Yii::t('app', 'Mostrar proforma en PDF'),
                                ]);
                        },
                'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/proforma/update', 'id' => $model->idCabeza_Factura], [
                            'id' => 'activity-index-link-update',
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Actualizar proforma'),
                            ]);
                    },
            ],
            'options'=>['style'=>'width:70px'],],
        ],
    ]); ?>
</div>
</div>
</div>
