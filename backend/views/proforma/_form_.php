<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
use kartik\widgets\Select2;
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\F_Proforma;//Prueba
use backend\models\FormasPago;//Prueba
use backend\models\Funcionario;
use backend\models\ProductoServicios;
use unclead\widgets\MultipleInput;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\DepDrop;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Proforma */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $products = F_Proforma::getContenidoCompra(); ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
</script>
<style media="screen">
table.scroll {
   width: 100%;  /* Optional */
  /* border-collapse: collapse; */
  border-spacing: 0;
  /*border: 5px solid #E7D8D8;*/
}

table.scroll tbody,
table.scroll thead { display: block; }
table.scroll tbody {
  height: 300px;
  overflow-y: auto;
  overflow-x: hidden;
}

tbody #td10, thead #td10 {
	 width: 10%;
}
tbody #td40, thead #td40 {
	 width: 40%;
}
tbody #td5, thead #td5 {
	 width: 5%;
}

tbody td:last-child, thead th:last-child {
	border-right: none;
}
</style>
<div class="proforma-form"><br>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-lg-12">
                <?php
                        if(@$vendedor = F_Proforma::getVendedor()){
                        if(@$model = Funcionario::find()->where(['idFuncionario'=>$vendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                            {
                                echo "<div class='col-lg-4'>";
                                echo "<h4><strong>Vendedor</strong>: ".$model->nombre." ".$model->apellido1." ".$model->apellido2." ";
                                echo Html::a('', ['/proforma/deletevendedor'],['class'=>'glyphicon glyphicon-pencil']);
                                echo "</h4>";
                                echo "</div>";
                            }
                        }
                        else
                        {
                    ?>
                    <div class="col-lg-4">
                    <?php
                        echo '<h4><strong>Código vendedor</strong></h4>';
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                //'model' => $model,
                                //'attribute' => 'codigoVendedor',
                                'name' => '',
                                'data' => ArrayHelper::map(Funcionario::find()->all(), 'idFuncionario',
                                    function($element) {
                                    return $element['idFuncionario'].' - '.$element['nombre'].' '.$element['apellido1'].' '.$element['apellido2'];
                                }),
                                'options' => [
                                    'placeholder' => 'Código',
                                    'id'=>'ddl-codigoVendedor',
                                    'onchange'=>'javascript:obtenerVendedor()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                            ]);
                    ?>
                    </div>
                    <?php } ?>
                    <div class="col-lg-4"><!-- ___________________AGREGAR ESTADO______________________________________ -->
                    <?php
                        F_Proforma::setEstadoFactura('Proforma');
                        if(@$estado = F_Proforma::getEstadoFactura()){
                        echo '<h4><strong>Tipo de factura</strong>: '.$estado;
                        echo "</h4>";
                       // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                        }
                    ?>
                    </div>
                    <div class="col-lg-4"><!-- ___________________AGREGAR ESTADO______________________________________ -->
                    <?php
                        F_Proforma::setFecha(date('d-m-Y '));
                        if(@$fech = F_Proforma::getFecha()){
                        echo '<h4><strong>Fecha ingreso</strong>: '.$fech;
                        echo "</h4>";
                       // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                        }
                    ?>
                    </div>
            </div>
            <div class="col-lg-12">
                <?php
                    if(@$cliente = F_Proforma::getCliente())
                    {   $clientSis = 0;
                        if(@$model = Clientes::find()->where(['idCliente'=>$cliente])->one())
                        {
                            $clientSis = 1;
                            if($clientSis!=0) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                {
                                     echo "<div class='col-lg-5'>";
                                     echo "<h4><strong>Cliente</strong>: ".$model->nombreCompleto." ";
                                     echo Html::a('', ['/proforma/deletecliente'],['class'=>'glyphicon glyphicon-pencil']);
                                     echo "</h4>";
                                     echo "</div>";
                                 }
                        }
                        else{
                            echo "<div class='col-lg-5'>";
                            echo "<h4><strong>Cliente</strong>: ".$cliente." ";
                            echo Html::a('', ['/proforma/deletecliente'],['class'=>'glyphicon glyphicon-pencil']);
                            echo "</h4>";
                            echo "</div>";
                        }
                    }
                    else {
                ?>
                <!--div class="col-lg-2"-->
                        <!--?php
                            echo '<h4><strong>Código cliente</strong></h4>';
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'idCliente'),
                                'options' => [
                                    'placeholder' => 'Código',
                                    'id'=>'cod-cliente',
                                    'onchange'=>'javascript:obtenerClienteCod()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                            ]);
                        ?-->

                <!--/div-->
                <div class="col-lg-4">
                        <?php

                            echo '<h4><strong>Cliente en el sistema</strong></h4>';
                            echo Select2::widget([
                                //'model' => $model,
                                //'attribute' => 'idCliente',
                                'name' => '',
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'nombreCompleto'),
                                'options' => ['id' => 'ddl-cliente', 'onchange'=>'javascript:obtenerCliente()','placeholder' => 'Seleccione un cliente'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        ?>
                </div>
                <div class="col-lg-4">
                        <?php
                            $form = ActiveForm::begin([
                                'id' => 'clienteN-form',
                                //'formConfig' => ['style' => 'POSITION: absolute', 'labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                                'action'=>['/proforma/clientenew'],
                                'enableAjaxValidation'=>false,
                                'enableClientValidation'=>false,
                                ]);

                                echo '<h4><strong>Código Cliente / Identificación / Nuevo cliente</strong></h4>';
                                    echo '<div class="input-group">';
                                       echo Html::input('text', 'new-cliente', '', ['id' => '', 'class' => 'form-control']);
                                    echo '<div class="input-group-btn">';
                                        echo '<input type="submit" value="Agregar" class="btn">';
                                    echo '</div>';
                                echo '</div>';
                        ?>

                        <?php ActiveForm::end(); ?>
                </div>
                <?php } ?>
            </div>
            <div class="col-lg-12"><hr/>
            <?= AlertBlock::widget([
                'type' => AlertBlock::TYPE_ALERT,
                'useSessionFlash' => true,
                'delay' => 5000
            ]);?>
                <div class="col-lg-2"><!-- ___________________AGREGAR PRODUCTO______________________________________ -->
                    <h4><strong>% descuento</strong></h4>
                    <?php
                    $empresa = Empresa::findOne(1);
                        $cantidad_descuento = $empresa->cantidad_descuento;
                        $array_descuento = null;
                        for ($i=1; $i <= $cantidad_descuento; $i++) {
                          $array_descuento[$i] = $i;
                        }
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => $array_descuento,//[1 => "1%", 2 => "2%", 3 => "3%", 4 => "4%", 5 => "5%", 6 => "6%", 7 => "7%", 8 => "8%", 9 => "9%", 10 => "10%"],
                                'options' => [
                                    'placeholder' => 'Seleccione %',
                                    'id'=>'descuento',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                            ]);
                        ?>
                </div>
                <div class="col-lg-6">
                    <h4>
                        <strong>Agregar Producto</strong>
                        <?php echo Html::a('', '#', [
                                'class' => 'glyphicon glyphicon-barcode',
                                'id' => 'activity-index-link-inventario',
                                'data-toggle' => 'modal',
                                'data-trigger'=>'hover', 'data-content'=>'Selecciona para mostrar los productos en el inventario',
                                'data-target' => '#modalinventario',
                                'data-url' => Url::to(['#']),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Inventario'),
                                ]); ?>
                    </h4>
                    <?php
                        echo Html::hiddenInput('codProdServicio',0);
                        echo Html::hiddenInput('cantidad',1);
                    ?>
                    <?php
                          /*  echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => 'state_10',
                                'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',
                                function($element) {
                                    return $element['codProdServicio'].' - '.$element['nombreProductoServicio']. ' [¢' .$element['precioVenta']. '] ---- Cantidad en stock: '.$element['cantidadInventario'];
                                }),
                                'options' => [
                                    'id' => 'codProdServicio',
                                    'placeholder' => 'Seleccione productos',
                                    'onchange'=>'javascript: agrega_producto(this.value)',
                                    //'onClick'=>'javascript: if (this.checked) setTimeout("refresca()", 500)',
                                    'pluginOptions' => ['allowClear' => true],
                                    'multiple' => false, //esto me ayuda a que se obtenga mas de uno
                                    //"update"=>"#conten_pro"
                                ],
                            ]);*/
                            echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'id' => 'medio', 'class' => 'form-control', 'placeholder' =>'Ingrese código de producto aqui', 'onkeyup'=>'javascript:this.value=Mayuculas(this.value)', 'onchange' => 'javascript:agrega_producto(this.value)']);
                    ?>
                </div>
                <div align="right" class="col-lg-4">
                  <?= Html::a('<span class="fa fa-linode"></span> Agregar producto temporal', '#', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal_producto_temporal']) ?>
                </div>
            </div>
            <div class="col-lg-9"><br>
                <div class="table-responsive">
                    <?php
                    //http://www.programacion.com.py/tag/yii
                        //echo '<div class="grid-view">';

                        echo '<table class="items table scroll table-striped">';
                        echo '<thead>';
                        printf('<tr><th id="td10">%s</th><th id="td10">%s</th><th id="td40">%s</th><th id="td10"><center>%s</center></th><th id="td10"><center>%s</center></th><th id="td5"><center>%s</center></th><th id="td10"><center>%s</center></th><th class="actions button-column" id="td5">&nbsp;</th></tr>',
                                'CANTIDAD',
                                'CÓDIGO',
                                'DESCRIPCIÓN',
                                'PREC.UNIT',
                                'DESCUENTO',
                                'IV %',
                                'TOTAL'
                                );
                        echo '</thead>';
                    if($products) {
                    ?>
                    <?php
                    echo '<tbody>';
                        $products = array_reverse($products, true);
                        foreach($products as $position => $product) {
                            $consultar = '';
                            $iva = 0.13;
                            $dcto = $product['dcto']=='' ? '' : number_format($product['dcto'],2);
                            if ($product['temporal']=='x') {
                              $descuento = number_format($product['cantidad']*(($product['precio']*$product['dcto'])/100),2);
                              $porc_ganancias_temporal = number_format($product['porc_ganancias_temporal'],2);
                              $precio_mas_iva_temporal = $product['precio_unit_gan_temporal'] + ( $product['precio_unit_gan_temporal'] * $iva );
                              $consultar = Html::a('', '#', [
                                      'class' => 'fa fa-search',
                                      'id' => 'activity-modal-update-temporal',
                                      'data-toggle' => 'modal',
                                      'data-trigger'=>'hover', 'data-content'=>'Selecciona para mostrar y modificar producto temporal',
                                      'data-target' => '#modal_producto_temporal_update',
                                      'onclick'=>'javascript:actualiza_producto_temporal('.$position.',"'.$product['cod'].'",
                                                                                         "'.$product['desc'].'",
                                                                                         "'.$product['cantidad'].'",
                                                                                         "'.number_format($product['precio_unit_prov_temporal'],2).'",
                                                                                         "'.$porc_ganancias_temporal.'",
                                                                                         "'.number_format($product['precio_unit_gan_temporal'],2).'",
                                                                                         "'.number_format($precio_mas_iva_temporal,2).'",
                                                                                         "'.$descuento.'",
                                                                                         "'.number_format($product['precio_total_ganacia_mod'],2).'",
                                                                                         "'.number_format($product['precio_mas_iva_total_temporal_mod'],2).'",
                                                                                         "'.$product['checkiva'].'",
                                                                                         "'.$dcto.'"
                                                                                         )',

                                      //'data-url' => Url::to(['#']),
                                      'data-pjax' => '0',
                                      'title' => Yii::t('app', 'Producto temporal'),
                                      ]);
                            }
                            $espaciocantidad = $product['temporal']=='x' ? $product['cantidad'] :
                            Html::textInput('cantidad_'.$position,
                                $product['cantidad'], array(
                                  'style'=>'text-align: center;',
                                  'onkeypress'=>'return isNumberDe(event)',
                                    'class' => 'cantidad_'.$position, 'size' => '4'
                                    )
                                );
                            printf('<tr><td id="td10">%s</td><td id="td10">%s</td><td id="td40">%s</td><td align="right" id="td10">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td10" class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td5" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td10" class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td5" class="actions button-column">%s</td></tr>',
                                    $espaciocantidad,
                                    $product['cod'],
                                    $product['desc'],
                                    number_format($product['precio'],2),
                                    number_format($product['cantidad']*(($product['precio']*$product['dcto'])/100),2),//Descuento
                                    number_format($product['iva'],2),
                                    number_format(($product['precio']*$product['cantidad']),2),
                                    //round((($product['precio']-(($product['precio']*$product['dcto'])/100))*$product['cantidad']),2),

                                       Html::a('', ['/proforma/deleteitem', 'id' => $position], ['class'=>'glyphicon glyphicon-remove', 'data-toggle'=>'tooltip','data-placement'=>'right', 'title'=>'Descartar este producto','data' => [
                                            'confirm' => '¿Está seguro de quitar este producto de la proforma?']]).' '.$consultar
                                    );
                                //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
                                $this->registerJs("
                                            $('.cantidad_".$position."').keyup(function() {
                                                $.ajax({
                                                    url:'".Yii::$app->getUrlManager()->createUrl('/proforma/updatedescuento')."',
                                                    data: $('.cantidad_".$position."'),
                                                    success: function(result) {
                                                    $('.descuent_".$position."').html(result);
                                                    $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                                    '/proforma/getpreciototal')."');
                                                    },
                                                    error: function() {
                                                    },
                                                });
                                                $.ajax({
                                                    url:'".Yii::$app->getUrlManager()->createUrl('/proforma/updatecantidad')."',
                                                    data: $('.cantidad_".$position."'),
                                                    success: function(result) {
                                                    $('.total_".$position."').html(result);
                                                    $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                                    '/proforma/getpreciototal')."');
                                                    },
                                                    error: function() {
                                                    },
                                                });
                                            });

                                        ");
                        }
                      }
                        if (@$products) {
                          printf('<tr><td colspan="8" bgcolor="#CAF5AA" id="td10" align="center">%s</td></tr>',
                                           'El orden de ingreso es de abajo hacia arriba (el último ingresado es el primero en la lista)');
                        } else {
                          printf('<tr><td colspan="8" bgcolor="#5396A4" id="td10" align="center">%s</td></tr>',
                                           '<h3>Área vacía, esperando un producto.</h3>');
                        }

                    echo '</tbody>';
                    ?>
                    <?php

                        /*$alerta = '';
                        if(@$clientealert = F_Proforma::getCliente()) {
                            if(@$modelclient = Clientes::find()->where(['idCliente'=>$clientealert])->one())
                            {
                                if ($modelclient->credito == 'Si') {
                                    $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                                    $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                                    $alerta = '
                                    <div class="alert alert-info alert-dismissable">
                                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <strong>¡Atención!</strong> Si esta prefactura será acreditada considere que '.$modelclient->nombreCompleto.' tiene un monto disponible para crédito de ₡'.number_format($montodisponiblecredito,2).'. Si se excede ese monto el sistema no permitirá acreditar la factura. Si esta prefactura no será acreditada ignore este informe.
                                    </div> ';
                                }
                            }

                            }*/
                        echo '</table>';
                    ?>
                </div>
            </div>
            <div class="col-lg-3"><br>
              <div class="well">
                <div id="resumen">
                  <?= F_Proforma::getTotal() ?>
                </div>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="input-group">
                     <span class="input-group-addon" id="sizing-addon3">Observaciones</span>
                  <?php
                  $observacion = F_Proforma::getObserva() ? F_Proforma::getObserva() : 'Ninguna';
                  echo Html::input('text', '', $observacion, ['id' => 'observacion', 'class' => 'form-control', 'placeholder' =>'Agregue alguna observación', 'onKeyUp' => 'javascript:agrega_observacion(this.value)']); ?>
              </div>
            </div>
        </div>

    </div>

</div>
