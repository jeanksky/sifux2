<?php

use yii\helpers\Html;
use yii\widgets\MaskedInput;
use kartik\widgets\AlertBlock;
use backend\models\F_Proforma;
use yii\bootstrap\Modal;
use backend\models\ProductoServicios;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $model backend\models\Proforma */

$this->title = 'Crear factura proforma';
$this->params['breadcrumbs'][] = ['label' => 'Proformas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $products = F_Proforma::getContenidoCompra(); ?>
<?php $vendedor = F_Proforma::getVendedor(); ?>
<?php $cliente = F_Proforma::getCliente(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
window.onkeydown = tecla;

	function tecla(event) {
		//event.preventDefault();
		num = event.keyCode;

		if(num==113)
			$('#modalinventario').modal('show');//muestro la modal

	}
	$(document).ready(function(){
        $('[data-toggle="modal"]').popover();
    });
		//espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

	function obtenerClienteCod(){

        var idCliente = document.getElementById("cod-cliente").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/cliente2') ?>",
            type:"post",
            data: {'idCliente': idCliente},
        }) ;
    }
    function obtenerCliente(){
        var idCliente = document.getElementById("ddl-cliente").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/cliente2') ?>",
            type:"post",
            data: {'idCliente': idCliente},
        }) ;
    }

    //activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
				$('#btn_agregar_temporar').addClass('disabled');//mantenemos el boton de realizar pagos desabilitado
    });

    //me permite buscar por like los productos a BD
		function buscar() {
        var textoBusqueda_cod = $("input#busqueda_cod").val();
        var textoBusqueda_des = $("input#busqueda_des").val();
				var textoBusqueda_fam = $("input#busqueda_fam").val();
        var textoBusqueda_cat = $("input#busqueda_cat").val();
        var textoBusqueda_cpr = $("input#busqueda_cpr").val();
        var textoBusqueda_ubi = $("input#busqueda_ubi").val();
        if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
            $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');

        } else {

            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/busqueda_producto') ?>",
                type:"post",
                data: {
                    'valorBusqueda_cod' : textoBusqueda_cod,
                    'valorBusqueda_des' : textoBusqueda_des,
										'valorBusqueda_fam' : textoBusqueda_fam,
                    'valorBusqueda_cat' : textoBusqueda_cat,
                    'valorBusqueda_cpr' : textoBusqueda_cpr,
                    'valorBusqueda_ubi' : textoBusqueda_ubi },
                beforeSend : function() {
                  $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 286');
                }
            });
        }
    }

		//AGREGAR PRODUCTO DE INVENTARIO
    function agrega_producto(codProdServicio){
	    //var codProdServicio = document.getElementById("codProdServicio").value;
	    var descuento = $("#descuento").val();
	    $.ajax({
	        url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/additem') ?>",
	        type:"post",
	        data: { 'codProdServicio': codProdServicio, 'cantidad': 1, 'descuento' : descuento },
	        success: function(data){
	         //alert('Si pasa');
	         //$("#conten_pro").html('index.php?r=cabeza-prefactura%2Ftabla');

	        //$("#conten_pro").load(location.href+' #conten_pro','');
	        //$("#codProdServicio").select2("val", ""); //vaciar contenido de select2
	        //$("#descuento").select2("val", "");
	         //location.reload();
	        },
	        error: function(msg, status,err){

	        }
	    });
	}

	//---------------------------------CALCULOS PARA PRECIOS DEL PRODUCTO TEMPORAL---------------------
	function abilitar_btn_agregar_temporal() {
		codigo_temporal = document.getElementById("codigo_temporal").value;
		descripcion_temporal = document.getElementById("descripcion_temporal").value;
		cantidad_temporal = document.getElementById("cantidad_temporal").value;
		precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
		porc_ganancias_temporal = document.getElementById("porc_ganancias_temporal").value;
		precio_unit_gan_temporal = document.getElementById("precio_unit_gan_temporal").value;
		precio_mas_iva_temporal = document.getElementById("precio_mas_iva_temporal").value;
		if (codigo_temporal==''	|| descripcion_temporal=='' || cantidad_temporal=='' || precio_unit_prov_temporal=='' ||
		porc_ganancias_temporal=='' || precio_unit_gan_temporal=='' || precio_mas_iva_temporal=='') {
			$('#btn_agregar_temporar').addClass('disabled');
		} else {
			$('#btn_agregar_temporar').removeClass('disabled');
		}
	}

	function obtenerCantidad(esc) {
		obtenerPrecioUnitario(esc);
	}

	function obtenerPrecioUnitario(esc) {
		abilitar_btn_agregar_temporal();
		var descuento = $("#descuento").val();
		precio_unit_prov_temporal = cantidad_temporal = porc_ganancias_temporal = '';
		if (esc==1) {
			precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
			cantidad_temporal = document.getElementById("cantidad_temporal").value;
			porc_ganancias_temporal = document.getElementById("porc_ganancias_temporal").value;
		} else {
			precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
			cantidad_temporal = document.getElementById("_cantidad_temporal").value;
			porc_ganancias_temporal = document.getElementById("_porc_ganancias_temporal").value;
			descuento = document.getElementById("_porciento_descuento").value;
		}

		$.ajax({
						url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/preciounitario') ?>",
						type:"post",
						dataType: 'json',
						data: { 'precio_unit_prov_temporal' : precio_unit_prov_temporal, 'cantidad_temporal' : cantidad_temporal,
					 						'porc_ganancias_temporal' : porc_ganancias_temporal, 'descuento' : descuento },
						success: function(notif){
							if (esc==1) {
								$('#precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
								$('#precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
								$("#descuento_div").html(notif.monto_descuento);
								$('#descuento_hi').val(notif.monto_descuento);//hidden oculto
								$("#precio_total_ganacia").html(notif.precio_total_ganacia);
								$('#precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
								$("#precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
								$('#precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
							} else {
								$('#_precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
								$('#_precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
								$("#_descuento_div").html(notif.monto_descuento);
								$('#_descuento_hi').val(notif.monto_descuento);//hidden oculto
								$("#_precio_total_ganacia").html(notif.precio_total_ganacia);
								$('#_precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
								$("#_precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
								$('#_precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
							}
						},
						error: function(msg, status,err){
						 //alert('Error linea 148');
						}
				});
	}

	function obtenerGanancia(esc) {
		obtenerPrecioUnitario(esc);
	}

	function obtenerPrecioGanancia(esc) {
		abilitar_btn_agregar_temporal()
		precio_unit_gan_temporal = cantidad_temporal = precio_unit_prov_temporal = 0;
		var descuento = $("#descuento").val();
		if (esc==1) {
			precio_unit_gan_temporal = document.getElementById("precio_unit_gan_temporal").value;
			cantidad_temporal = document.getElementById("cantidad_temporal").value;
			precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
		} else {
			precio_unit_gan_temporal = document.getElementById("_precio_unit_gan_temporal").value;
			cantidad_temporal = document.getElementById("_cantidad_temporal").value;
			precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
			descuento = document.getElementById("_porciento_descuento").value;
		}
		$.ajax({
						url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/precioganancia') ?>",
						type:"post",
						dataType: 'json',
						data: { 'precio_unit_gan_temporal' : precio_unit_gan_temporal, 'cantidad_temporal' : cantidad_temporal,
					 						'precio_unit_prov_temporal' : precio_unit_prov_temporal, 'descuento' : descuento },
						success: function(notif){
							if (esc==1) {
								$('#porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
								$('#precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
								$("#descuento_div").html(notif.monto_descuento);
								$('#descuento_hi').val(notif.monto_descuento);//hidden oculto
								$("#precio_total_ganacia").html(notif.precio_total_ganacia);
								$('#precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
								$("#precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
								$('#precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
							} else {
								$('#_porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
								$('#_precio_mas_iva_temporal').val(notif.precio_mas_iva_temporal);
								$("#_descuento_div").html(notif.monto_descuento);
								$('#_descuento_hi').val(notif.monto_descuento);//hidden oculto
								$("#_precio_total_ganacia").html(notif.precio_total_ganacia);
								$('#_precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
								$("#_precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
								$('#_precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
							}
						},
						error: function(msg, status,err){
						 //alert('Error linea 180');
						}
				});
	}
	$(function(){
		$('input[name="checkiva"]').on('click', function() {
				if ( $(this).is(':checked') )
						$('#checkiva').val('si');
				else
						$('#checkiva').val('no');
		});

		$('input[name="checkiva_"]').on('click', function() {
				if ( $(this).is(':checked') )
						$('#checkiva_').val('si');
				else
						$('#checkiva_').val('no');
		});
	});
	function obtenerPrecioIVA(esc) {
		abilitar_btn_agregar_temporal()
		precio_mas_iva_temporal = cantidad_temporal = precio_unit_prov_temporal = '';
		var descuento = $("#descuento").val();
		if (esc==1) {
			precio_mas_iva_temporal = document.getElementById("precio_mas_iva_temporal").value;
			cantidad_temporal = document.getElementById("cantidad_temporal").value;
			precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
		} else {
			precio_mas_iva_temporal = document.getElementById("_precio_mas_iva_temporal").value;
			cantidad_temporal = document.getElementById("_cantidad_temporal").value;
			precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
			descuento = document.getElementById("_porciento_descuento").value;
		}
		$.ajax({
						url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/precioiva') ?>",
						type:"post",
						dataType: 'json',
						data: { 'precio_mas_iva_temporal' : precio_mas_iva_temporal, 'cantidad_temporal' : cantidad_temporal,
					 						'precio_unit_prov_temporal' : precio_unit_prov_temporal, 'descuento' : descuento },
						success: function(notif){
							if (esc==1) {
								$('#porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
								$('#precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
								$("#descuento_div").html(notif.monto_descuento);
								$('#descuento_hi').val(notif.monto_descuento);//hidden oculto
								$("#precio_total_ganacia").html(notif.precio_total_ganacia);
								$('#precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
								$("#precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
								$('#precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
							} else {
								$('#_porc_ganancias_temporal').val(notif.porc_ganancias_temporal);
								$('#_precio_unit_gan_temporal').val(notif.precio_unit_gan_temporal);
								$("#_descuento_div").html(notif.monto_descuento);
								$('#_descuento_hi').val(notif.monto_descuento);//hidden oculto
								$("#_precio_total_ganacia").html(notif.precio_total_ganacia);
								$('#_precio_total_ganacia_hi').val(notif.precio_total_ganacia);//hidden oculto
								$("#_precio_mas_iva_total_temporal").html(notif.precio_mas_iva_total_temporal);
								$('#_precio_mas_iva_total_temporal_hi').val(notif.precio_mas_iva_total_temporal);//hidden oculto
							}
						},
						error: function(msg, status,err){
						 alert('Error linea 123');
						}
				});
	}

	//envia la categoria que selecciono para obtener los datos de dicha categoria
	function actualiza_producto_temporal(position,codigo_temporal, descripcion_temporal, cantidad_temporal,
																			 precio_unit_prov_temporal, porc_ganancias_temporal, precio_unit_gan_temporal,
																			 precio_mas_iva_temporal, descuento_div, precio_total_ganacia, precio_mas_iva_total_temporal, checkiva, dcto) {

		$('#codigo_id').val(position);
		$('#_codigo_temporal').val(codigo_temporal);
		$('#_descripcion_temporal').val(descripcion_temporal);
		$('#_cantidad_temporal').val(cantidad_temporal);
		$('#_precio_unit_prov_temporal').val(precio_unit_prov_temporal);
		$('#_porc_ganancias_temporal').val(porc_ganancias_temporal);
		$('#_precio_unit_gan_temporal').val(precio_unit_gan_temporal);
		$('#_precio_mas_iva_temporal').val(precio_mas_iva_temporal);
		$("#_descuento_div").html(descuento_div);
		$("#_precio_total_ganacia").html(precio_total_ganacia);
		$("#_precio_mas_iva_total_temporal").html(precio_mas_iva_total_temporal);
		$("#_porciento_descuento").val(dcto);
		$("#_descuento_hi").val(descuento_div);
	  $("#_precio_total_ganacia_hi").val(precio_total_ganacia);
	  $("#_precio_mas_iva_total_temporal_hi").val(precio_mas_iva_total_temporal);
		$('#checkiva_').val(checkiva);
		if (checkiva == 'si') {
			$('input[name="checkiva_"]').prop('checked',true);
	  } else {
			$('input[name="checkiva_"]').prop('checked' , false);
		}
	  /*$.ajax({
	            url:"<?php //echo Yii::$app->getUrlManager()->createUrl('cuentas-bancarias/actualiza_modal_documento') ?>",
	            type:"post",
	            dataType: 'json',
	            data: { 'idTipo_documento': idTipo_documento },
	            success: function(data){
	              $('#idTipo_documento_').val(data.idTipo_documento);
	                $('#descripcion_').val(data.descripcion);
	                $("input[name=tipo_][value='"+data.tipo_movimiento+"']").prop("checked",true);
	                $("input[name=estado_][value='"+data.estado+"']").prop("checked",true);
	                //$("#update-monedas").load(location.href+' #update-monedas','');
	            },
	            error: function(msg, status,err){
	             alert('No pasa - linea 52');
	            }
	        }); */
	}

	//Transformar a mayúsculas todos los caracteres
	function Mayuculas(tx){
		//Retornar valor convertido a mayusculas
		return tx.toUpperCase();
	}
	//-------------------------------------------------------------------------------------------------

	//AGREGAR PRODUCTO TEMPORAL
	function agrega_producto_temporal() {
		var descuento = $("#descuento").val();
		var codigo_temporal = document.getElementById("codigo_temporal").value;
		var descripcion_temporal = document.getElementById("descripcion_temporal").value;
		var cantidad_temporal = document.getElementById("cantidad_temporal").value;
		var precio_unit_prov_temporal = document.getElementById("precio_unit_prov_temporal").value;
		var porc_ganancias_temporal = document.getElementById("porc_ganancias_temporal").value;
		var precio_unit_gan_temporal = document.getElementById("precio_unit_gan_temporal").value;
		var precio_mas_iva_temporal = document.getElementById("precio_mas_iva_temporal").value;
		var descuento_hi = document.getElementById("descuento_hi").value;//hidden
		var precio_total_ganacia_hi = document.getElementById("precio_total_ganacia_hi").value;//hidden
		var precio_mas_iva_total_temporal_hi = document.getElementById("precio_mas_iva_total_temporal_hi").value;//hidden
		var checkiva = document.getElementById("checkiva").value;
		$.ajax({
				url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/additem_temporal') ?>",
				type:"post",
				data: { 'descuento': descuento, 'codigo_temporal': codigo_temporal, 'descripcion_temporal' : descripcion_temporal,
			 					'cantidad_temporal': cantidad_temporal, 'precio_unit_prov_temporal': precio_unit_prov_temporal,
							 	'porc_ganancias_temporal': porc_ganancias_temporal, 'precio_unit_gan_temporal': precio_unit_gan_temporal,
								'precio_mas_iva_temporal': precio_mas_iva_temporal, 'descuento_hi' : descuento_hi,
								'precio_total_ganacia_hi' : precio_total_ganacia_hi, 'precio_mas_iva_total_temporal_hi' : precio_mas_iva_total_temporal_hi,
							 	'checkiva' : checkiva },
				success: function(data){
				},
				error: function(msg, status,err){
					//alert('Error 206');
				}
		});
	}

	//modificar producto temporal seleccionado
	function modificar_producto_temporal() {
		var codigo_id = document.getElementById("codigo_id").value;
		var descuento = document.getElementById("_porciento_descuento").value;
		var codigo_temporal = document.getElementById("_codigo_temporal").value;
		var descripcion_temporal = document.getElementById("_descripcion_temporal").value;
		var cantidad_temporal = document.getElementById("_cantidad_temporal").value;
		var precio_unit_prov_temporal = document.getElementById("_precio_unit_prov_temporal").value;
		var porc_ganancias_temporal = document.getElementById("_porc_ganancias_temporal").value;
		var precio_unit_gan_temporal = document.getElementById("_precio_unit_gan_temporal").value;
		var precio_mas_iva_temporal = document.getElementById("_precio_mas_iva_temporal").value;
		var descuento_hi = document.getElementById("_descuento_hi").value;//hidden
		var precio_total_ganacia_hi = document.getElementById("_precio_total_ganacia_hi").value;//hidden
		var precio_mas_iva_total_temporal_hi = document.getElementById("_precio_mas_iva_total_temporal_hi").value;//hidden
		var checkiva_ = document.getElementById("checkiva_").value;
		$.ajax({
				url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/moditem_temporal') ?>",
				type:"post",
				data: { 'codigo_id' : codigo_id, 'descuento': descuento, 'codigo_temporal': codigo_temporal, 'descripcion_temporal' : descripcion_temporal,
			 					'cantidad_temporal': cantidad_temporal, 'precio_unit_prov_temporal': precio_unit_prov_temporal,
							 	'porc_ganancias_temporal': porc_ganancias_temporal, 'precio_unit_gan_temporal': precio_unit_gan_temporal,
								'precio_mas_iva_temporal': precio_mas_iva_temporal, 'descuento_hi' : descuento_hi,
								'precio_total_ganacia_hi' : precio_total_ganacia_hi, 'precio_mas_iva_total_temporal_hi' : precio_mas_iva_total_temporal_hi,
							 	'checkiva_' : checkiva_ },
				success: function(data){
				},
				error: function(msg, status,err){
					//alert('Error 206');
				}
		});
	}

	function obtenerVendedor(){

	    var idFuncionario = document.getElementById("ddl-codigoVendedor").value;
	    $.ajax({
	        url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/vendedor2') ?>",
	        type:"post",
	        data: {'idFuncionario': idFuncionario},
	        /*success: function(data){
	         $('#resultado2').val(data);
	        },
	        error: function(msg, status,err){
	                        $('#resultado2').val('Dato no encontrado');
	                    }*/
	    }) ;
	}
	/*$(document).ready(function () {
            (function ($) {
                $('#filtrar').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));
            $('#tabla_lista_paises').dataTable({
            "sPaginationType" : "full_numbers"//DAMOS FORMATO A LA PAGINACION(NUMEROS)
            });
        });*/
    function agrega_observacion(observacion){
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('proforma/addobservacion') ?>",
        type:"post",
        data: {'observacion': observacion},
    }) ;
}
</script>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }

    #modal_producto_temporal .modal-dialog{
    width: 90%!important;
    /*margin: 0 auto;*/
    }
		#modal_producto_temporal_update .modal-dialog{
    width: 90%!important;
    /*margin: 0 auto;*/
    }
</style>

<div class="proforma-create">
    <!--h3> Html::encode($this->title) </h3-->
    <span style="float:right; font-size: 18pt; color: #5396A4;">Crear Proforma</span>
    	<?= Html::a('<span class="fa fa-arrow-left"></span> Salir sin borrar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('<span class="fa fa-arrow-left"></span> <span class="fa fa-eraser"></span> Borrar y salir', ['regresarborrar'], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres desechar los datos ya escritos y salir de la proforma?',
                'method' => 'post',
            ],
        ]) ?>
        <?php if($products && $vendedor && $cliente) : ?>
            <?= Html::a('<span class="fa fa-floppy-o"></span> Generar Proforma', ['complete'], ['class' => 'btn btn-primary']) ?>
            <!--?= //Html::a('Acreditar', ['index'], ['class' => 'btn btn-primary']) ?-->
        <?php endif ?>
        <?= Html::a('<span class="fa fa-refresh"></span> Desechar y limpiar', ['deleteall'], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres desechar y limpiar los datos de esta proforma?',
                'method' => 'post',
            ],
        ]) ?>

    <?= $this->render('_form_', [
        //'model' => $model,
    ]) ?>

</div>
<?php
//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        //http://blog.collectivecloudperu.com/buscar-en-tiempo-real-con-jquery-y-bootstrap/
        echo '<div class="well"><div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
                                </div>
																<div class="col-lg-2">
													        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
													      </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
                                </div>

                <table class="table table-hover" id="tabla_lista_productos">';
        echo '<thead>';
				printf('<tr><th width="150">%s</th>
                    <th width="380">%s</th>
                    <th width="100">%s</th>
                    <th width="130">%s</th>
                    <th width="120">%s</th>
                    <th width="50">%s</th>
                    <th width="140">%s</th>
                    <th class="actions button-column" width="60">&nbsp;</th>
                </tr>',
												'CÓDIGO LOCAL',
												'DESCRIPCIÓN',
												'CANT.INV',
												'COD.ALTER',
												'CÓD.PROV',
												'UBICACIÓN',
												'PRECIO + IV'
												);
								echo '</thead>';
                echo '</thead>';
               /* $model_Prod = ProductoServicios::find()->where(["=",'tipo','Producto'])->all();
                echo '<tbody class="buscar">';
                foreach($model_Prod as $position => $product) {
                    $accion = Html::a('', '', ['class'=>'glyphicon glyphicon-plus', 'data-value'=>$product['codProdServicio'],'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Agregar este producto', 'onclick'=>'javascript:agrega_producto($(this).data("value"))']);
                    $presiopro = '₡ '.number_format($product['precioVentaImpuesto'],2);
                    printf('<tr><td>%s</td><td class="des">%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="actions button-column">%s</td></tr>',


                            $product['codProdServicio'],
                            $product['nombreProductoServicio'],
                            $product['cantidadInventario'],
                            $presiopro,
                            $accion
                            );
                }//fin de foreach
                echo '</tbody>';*/

                echo '</table>
                <div id="resultadoBusqueda" style="height: 400px;width: 100%; overflow-y: auto; "></div>
                </div>';

        Modal::end();

				//muestra modal para agregar un producto temporal
		    Modal::begin([
		            'id' => 'modal_producto_temporal',
		            //'size'=>'modal-sm',
		            'header' => '<center><h4 class="modal-title">Agregar nuevo producto temporal</h4></center>',
		            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Agregar', '#', ['class' => 'btn btn-success', 'id'=>'btn_agregar_temporar', 'onclick'=>'agrega_producto_temporal()',]),
		        ]);
						echo '<div class="well">';
							echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">
							<div class="col-lg-12"><center>Nota: si desea aplicar un descuento a este producto selecciona el porcentaje donde dice "% descuento" y luego regrese para ingresar el producto.</center><br></div>';
								echo '<div class="col-lg-1"><label>Cód.Producto</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'codigo_temporal', 'class' => 'form-control', 'onkeyup'=>'javascript:abilitar_btn_agregar_temporar()', 'placeholder' =>'Código']);
								echo '</div>';
								echo '<div class="col-lg-3"><label>Descripción del producto</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'descripcion_temporal', 'class' => 'form-control', 'onkeyup'=>'javascript:abilitar_btn_agregar_temporar()', 'placeholder' =>'Descripcion del producto']);
								echo '</div>';
								echo '<div class="col-lg-1"><label>Cantidad</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'cantidad_temporal', 'class' => 'form-control', 'placeholder' =>'Cantidad', 'onkeyup'=>'javascript:obtenerPrecioUnitario(1)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (Proveedor)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_prov_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (Prov)', 'onkeyup'=>'javascript:obtenerPrecioUnitario()']);
									echo MaskedInput::widget(['name' => 'precio_unit_prov_temporal',
											'options' => ['id'=>'precio_unit_prov_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioUnitario(1)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-1"><label>% ganancia</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => 'porc_ganancias_temporal', 'class' => 'form-control', 'placeholder' =>'% ganancia', 'onkeyup'=>'javascript:obtenerPrecioUnitario(1)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (más ganacia)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_gan_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (+ ganancia)', 'onkeyup'=>'javascript:obtenerPrecioGanancia()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => 'precio_unit_gan_temporal',
											'options' => ['id'=>'precio_unit_gan_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioGanancia(1)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio más IV</label> <span style="float:right">Excluir <input type="checkbox" name="checkiva" /></span><input type="hidden" id="checkiva" value="no">';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_mas_iva_temporal', 'class' => 'form-control', 'placeholder' =>'Precio+iva', 'onkeyup'=>'javascript:obtenerPrecioIVA()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => 'precio_mas_iva_temporal',
											'options' => ['id'=>'precio_mas_iva_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioIVA(1)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '<br></div>';
								echo '<div class="col-lg-12">';
								echo '<div class="col-lg-4"><h3>Descuento: <div id="descuento_div"></div><input type="hidden" id="descuento_hi"></h3></div>
											<div class="col-lg-4"><h3>Precio total más ganancia: <div id="precio_total_ganacia"></div><input type="hidden" id="precio_total_ganacia_hi"></h3></div>
											<div class="col-lg-4"><h3>Precio total más IV: <div id="precio_mas_iva_total_temporal"></div><input type="hidden" id="precio_mas_iva_total_temporal_hi"></h3></div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
				Modal::end();

				//muestra modal para actualizar un producto temporal
		    Modal::begin([
		            'id' => 'modal_producto_temporal_update',
		            //'size'=>'modal-sm',
		            'header' => '<center><h4 class="modal-title">Actualizar producto temporal</h4></center>',
		            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('Actualizar', '#', ['class' => 'btn btn-success', 'id'=>'btn_actualizar_temporar', 'onclick'=>'modificar_producto_temporal()',]),
		        ]);
						echo '<div class="well">';
							echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">
							<div class="col-lg-12"><center>Nota: si desea aplicar un descuento a este producto selecciona el porcentaje donde dice "% descuento" y luego regrese para ingresar el producto.</center><br></div>';
								echo '<div class="col-lg-1"><label>Cód.Producto</label><input type="hidden" id="codigo_id">';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_codigo_temporal', 'class' => 'form-control', 'placeholder' =>'Código']);
								echo '</div>';
								echo '<div class="col-lg-3"><label>Descripción del producto</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_descripcion_temporal', 'class' => 'form-control', 'placeholder' =>'Descripcion del producto']);
								echo '</div>';
								echo '<div class="col-lg-1"><label>Cantidad</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_cantidad_temporal', 'class' => 'form-control', 'placeholder' =>'Cantidad', 'onkeyup'=>'javascript:obtenerPrecioUnitario(2)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (Proveedor)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_prov_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (Prov)', 'onkeyup'=>'javascript:obtenerPrecioUnitario()']);
									echo MaskedInput::widget(['name' => '_precio_unit_prov_temporal',
											'options' => ['id'=>'_precio_unit_prov_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioUnitario(2)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-1"><label>% ganancia</label>';
									echo Html::input('text', '', '', ['size' => '30', 'id' => '_porc_ganancias_temporal', 'class' => 'form-control', 'placeholder' =>'% ganancia', 'onkeyup'=>'javascript:obtenerPrecioUnitario(2)', "onkeypress"=>"return isNumberDe(event)"]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio unitario (más ganacia)</label>';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_unit_gan_temporal', 'class' => 'form-control', 'placeholder' =>'Precio unitario (+ ganancia)', 'onkeyup'=>'javascript:obtenerPrecioGanancia()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => '_precio_unit_gan_temporal',
											'options' => ['id'=>'_precio_unit_gan_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioGanancia(2)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '</div>';
								echo '<div class="col-lg-2"><label>Precio más IV</label><span style="float:right">Excluir <input type="checkbox" name="checkiva_" /></span><input type="hidden" id="checkiva_">';
									//echo Html::input('text', '', '', ['size' => '30', 'id' => 'precio_mas_iva_temporal', 'class' => 'form-control', 'placeholder' =>'Precio+iva', 'onkeyup'=>'javascript:obtenerPrecioIVA()', "onkeypress"=>"return isNumberDe(event)"]);
									echo MaskedInput::widget(['name' => '_precio_mas_iva_temporal',
											'options' => ['id'=>'_precio_mas_iva_temporal', 'class'=>'form-control',
											'onkeyup'=>'javascript:obtenerPrecioIVA(2)', "onkeypress"=>"return isNumberDe(event)"],
											'clientOptions' => [
															'alias' =>  'decimal',
															'groupSeparator' => ',',
															'autoGroup' => true
													]]);
								echo '<br></div>';
								echo '<div class="col-lg-12">';
								echo '<div class="col-lg-4"><h3>Descuento: <div id="_descuento_div"></div><input type="hidden" id="_descuento_hi"><input type="hidden" id="_porciento_descuento"></h3></div>
											<div class="col-lg-4"><h3>Precio total más ganancia: <div id="_precio_total_ganacia"></div><input type="hidden" id="_precio_total_ganacia_hi"></h3></div>
											<div class="col-lg-4"><h3>Precio total más IV: <div id="_precio_mas_iva_total_temporal"></div><input type="hidden" id="_precio_mas_iva_total_temporal_hi"></h3></div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
				Modal::end();
