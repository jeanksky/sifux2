<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Funcionario;
use backend\models\Clientes;
use backend\models\FormasPago;
use backend\models\DetalleProformas;
use backend\models\ProductoServicios;

/* @var $this yii\web\View */
/* @var $model backend\models\Proforma */

$this->title = $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Proformas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proforma-view">
<?php
$products = DetalleProformas::find()->where(["=",'idCabeza_factura', $model->idCabeza_Factura])->all();
if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
    echo '<strong>Cliente: </strong>'.$cliente->nombreCompleto.'<br>
      <strong>Teléfono: </strong>'.$cliente->telefono.'<br>
      <strong>Dirección: </strong>'.$cliente->direccion.'<br>';
} else{
    echo '<strong>Cliente: </strong>'.$model->idCliente.'<br><br><br>';
}
?>
<div class="panel panel-default">
        <div class="panel-heading">

            <table>
                <tr>
                    <td>
                        <?php
                            echo '<strong>Fecha</strong>: '.$model->fecha_inicio;
                            echo '&nbsp;&nbsp;&nbsp;&nbsp;<strong>Estado de la factura</strong>: '.$model->estadoFactura;
                        ?>
                        <br>
                        <?php
                            if(@$modelF = Funcionario::find()->where(['idFuncionario'=>$model->codigoVendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                {

                                    echo "<strong>Vendedor</strong>: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2." ";
                                    echo "<br>";
                                }
                        ?>

                        <?php

                            echo '<strong>Observaciones:</strong>: '.$model->observaciones;
                        ?>

                    </td>
                </tr>
            </table>
            </div>
            <div class="panel-body">
            <div class="col-lg-12">
                <?php
                        echo '<table class="items table table-striped">';
                        echo '<thead>';
                        printf('<tr><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th></tr>',
                                'CAN',
                                //'CÓD',
                                'DESCRIPCIÓN',
                                'PREC UNIT',
                                'DESC',
                                'IV %',
                                'TOTAL'
                                );
                        echo '</thead>';

                        if($products) {
                        echo '<tbody>';
                            foreach($products as $position => $product) {
                                $descrip = '';
                                $detalle_prof = DetalleProformas::find()->where(['=','codProdServicio', $product->codProdServicio])->andWhere("idCabeza_factura = :idCabeza_factura", [":idCabeza_factura"=>$model->idCabeza_Factura])->one();
                                if (@$detalle_prof->temporal=='x') {
                                  $descrip = $detalle_prof->descrip_temporal;
                                } else {
                                  $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                                  $descrip = $product['codProdServicio'] != "0" ? $modelProd->nombreProductoServicio : 'Orden de servicio';
                                }
                                printf('<tr><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>',
                                        $product['cantidad'],
                                        $descrip,
                                        number_format($product['precio_unitario'], 2),
                                        number_format($product['descuento_producto'],2),//Descuento
                                        number_format($product['subtotal_iva'],2),
                                        number_format(($product['precio_unitario']*$product['cantidad']),2)

                                        );
                            }
                        echo '</tbody>';
                    }
                        echo '</table><div class="" >';
                        echo '<div class="resumen">
                           <h3 align="right">RESUMEN</h3>
                           <table class="" align="right">
                            <tbody>
                                <tr>
                                     <td class="detail-view">
                                        <table>
                                            <tr>
                                                <th align="left">SUBTOTAL: </th>
                                                <td align="right">'.number_format($model->subtotal,2).'</td>
                                            </tr>
                                            <tr>
                                                <th align="left">TOTAL DESCUENTO: &nbsp;</th>
                                                <td align="right">'.number_format($model->porc_descuento,2).'</td>
                                            </tr>
                                            <tr>
                                                <th align="left">TOTAL IV: </th>
                                                <td align="right"><span>'.number_format($model->iva,2).'</span></td>
                                            </tr>
                                             <tr>
                                                <th align="left">TOTAL A PAGAR: </th>
                                                <td align="right"><span><strong>'.number_format($model->total_a_pagar,2).'</strong></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                            </div>';
                        echo '</div>';
                ?>
            </div>
        </div>
    </div>

</div>
