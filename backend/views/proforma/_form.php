<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\F_Proforma;
use kartik\widgets\Select2;
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\Funcionario;
use backend\models\ProductoServicios;
use backend\models\DetalleProformas;
use yii\helpers\Url;
use kartik\widgets\AlertBlock;

/* @var $this yii\web\View */
/* @var $model backend\models\Proforma */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $products = F_Proforma::getContenidoComprau();
    F_Proforma::setCabezaFactura($model->idCabeza_Factura);
    $estado = F_Proforma::getEstadoFacturau();
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<style media="screen">
table.scroll {
   width: 100%;  /* Optional */
  /* border-collapse: collapse; */
  border-spacing: 0;
  /*border: 5px solid #E7D8D8;*/
}

table.scroll tbody,
table.scroll thead { display: block; }
table.scroll tbody {
  height: 300px;
  overflow-y: auto;
  overflow-x: hidden;
}

tbody #td10, thead #td10 {
	 width: 10%;
}
tbody #td40, thead #td40 {
	 width: 40%;
}
tbody #td5, thead #td5 {
	 width: 5%;
}

tbody td:last-child, thead th:last-child {
	border-right: none;
}
</style>
<div class="proforma-form"><br>
    <div class="panel panel-default">
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="col-lg-12">
                <?php
                    F_Proforma::setVendedoru($model->codigoVendedor);
                    if(@$vendedor = F_Proforma::getVendedoru()){
                    if(@$modelF = Funcionario::find()->where(['idFuncionario'=>$vendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                        {
                            echo "<div class='col-lg-4'>";
                            echo "<h4><strong>Vendedor</strong>: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2." ";
                            echo Html::a('', ['/proforma/deletevendedor-u', 'id' => $model->idCabeza_Factura],['class'=>'glyphicon glyphicon-pencil']);
                            echo "</h4>";
                            echo "</div>";
                        }
                    }
                    else
                    {
                ?>
                <div class="col-lg-4"><!-- ___________________AGREGAR VENDEDOR POR CODIGO______________________________________ -->
                    <?php
                        echo '<h4><strong>Código vendedor</strong></h4>';
                        echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                            //'model' => $model,
                            //'attribute' => 'codigoVendedor',
                            'name' => '',
                            'data' => ArrayHelper::map(Funcionario::find()->all(), 'idFuncionario',
                                function($element) {
                                return $element['idFuncionario'].' - '.$element['nombre'].' '.$element['apellido1'].' '.$element['apellido2'];
                            }),
                            'options' => [
                                'placeholder' => 'Código',
                                'id'=>'ddl-codigoVendedor',
                                'onchange'=>'javascript:obtenerVendedor()',
                                'multiple' => false //esto me ayuda a que solo se obtenga uno
                            ],
                        ]);
                    ?>
                </div>
                <?php }?>
                <div class="col-lg-4"><!-- ___________________AGREGAR ESTADO______________________________________ -->
                <?php
                    F_Proforma::setEstadoFacturau($model->estadoFactura);
                    if(@$estado = F_Proforma::getEstadoFacturau()){
                    echo '<h4><strong>Estado de la factura</strong>: '.$estado;
                    echo "</h4>";
                   // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                    }
                ?>
                </div>
                <div class="col-lg-4"><!-- ___________________AGREGAR ESTADO______________________________________ -->
                <?php
                    F_Proforma::setFechau($model->fecha_inicio);
                    if(@$fech = F_Proforma::getFechau()){
                    echo '<h4><strong>Fecha ingreso</strong>: '.$fech;
                    echo "</h4>";
                   // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                    }
                ?>
                </div>
            </div>
            <div class="col-lg-12">
                <?php
                    F_Proforma::setClienteu($model->idCliente);
                    if(@$cliente = F_Proforma::getClienteu())
                    {   $clientSis = 0;
                        if(@$modelC = Clientes::find()->where(['idCliente'=>$cliente])->one())
                        {
                            $clientSis = 1;
                            if($clientSis!=0) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                {
                                     echo "<div class='col-lg-5'>";
                                     echo "<h4><strong>Cliente</strong>: ".$modelC->nombreCompleto." ";
                                     echo Html::a('', ['/proforma/deletecliente-u', 'id' => $model->idCabeza_Factura],['class'=>'glyphicon glyphicon-pencil']);
                                       echo "</h4>". Html::input('text', '', $modelC->email, ['class' => 'form-control', 'id' => 'email_cliente', 'placeholder' =>'Ingrese la dirección del cliente aquí si desea enviar la cotización por email.']);
                                       echo '<br><div id="notificacion_envio_correo"></div>';
                                     echo "</div>";
                                 }
                        }
                        else{
                            echo "<div class='col-lg-5'>";
                            echo "<h4><strong>Cliente</strong>: ".$cliente." ";
                            echo Html::a('', ['/proforma/deletecliente-u', 'id' => $model->idCabeza_Factura],['class'=>'glyphicon glyphicon-pencil']);
                            echo "</h4>". Html::input('text', '', '', ['size' => '70', 'id' => 'email_cliente', 'class' => 'form-control', 'placeholder' =>'Ingrese la dirección del cliente aquí si desea enviar la cotización por email.']);
                            echo '<br><div id="notificacion_envio_correo"></div>';
                            echo "</div>";
                        }
                    }
                    else {
                ?>
                <!--div class="col-lg-2"-->
                        <!--?php
                            echo '<h4><strong>Código cliente</strong></h4>';
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'idCliente'),
                                'options' => [
                                    'placeholder' => 'Código',
                                    'id'=>'cod-cliente',
                                    'onchange'=>'javascript:obtenerClienteCod()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                            ]);
                        ?-->

                <!--/div-->
                <div class="col-lg-4">
                        <?php

                            echo '<h4><strong>Cliente en el sistema</strong></h4>';
                            echo Select2::widget([
                                //'model' => $model,
                                //'attribute' => 'idCliente',
                                'name' => '',
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'nombreCompleto'),
                                'options' => ['id' => 'ddl-cliente', 'onchange'=>'javascript:obtenerCliente()','placeholder' => 'Seleccione un cliente'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        ?>
                </div>
                <div class="col-lg-4">
                        <?php
                            $form = ActiveForm::begin([
                                'id' => 'clienteN-form',
                                //'formConfig' => ['style' => 'POSITION: absolute', 'labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                                'action'=>['/proforma/clientenew-u'],
                                'enableAjaxValidation'=>false,
                                'enableClientValidation'=>false,
                                ]);

                                echo '<h4><strong>Código Cliente / Identificación / Nuevo cliente</strong></h4>';
                                    echo '<div class="input-group">';
                                       echo Html::input('text', 'new-cliente', '', ['id' => '', 'class' => 'form-control']);
                                    echo '<div class="input-group-btn">';
                                        echo '<input type="submit" value="Agregar" class="btn">';
                                //echo Html::submitButton('Submit', ['style' => 'clear:both;', 'class' => 'submit']);
                                    echo '</div>';
                                echo '</div>';
                             /*   echo yii\bootstrap\Button::widget([
                                            'label' => 'Agregar',
                                            'options' => [ 'style' => 'clear:both;', 'class' => 'btn','buttonType' => 'submit'],
                                        ]);*/
                        ?>
                        <!--div class="input-group">
                          <input type="text" name="new-cliente" class="form-control">
                          <div class="input-group-btn">
                            <submit type="submit" class="btn btn-default">Agregar</submit>
                          </div>
                        </div-->

                        <?php ActiveForm::end(); ?>
                </div>
                <?php } ?>
            </div>
            <div class="col-lg-12"><hr/>
                <?= AlertBlock::widget([
                    'type' => AlertBlock::TYPE_ALERT,
                    'useSessionFlash' => true,
                    'delay' => 5000
                ]);?>
                <!-- ___________________AREA DE AGREGAR PRODUCTO Y TIPO DE PAGO______________________________________ -->

                <div class="col-lg-2"><!-- ___________________AGREGAR PRODUCTO______________________________________ -->
                    <h4><strong>% descuento</strong></h4>
                    <?php
                    $empresa = Empresa::findOne(1);
                        $cantidad_descuento = $empresa->cantidad_descuento;
                        $array_descuento = null;
                        for ($i=1; $i <= $cantidad_descuento; $i++) {
                          $array_descuento[$i] = $i;
                        }
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => $array_descuento,//[1 => "1%", 2 => "2%", 3 => "3%", 4 => "4%", 5 => "5%", 6 => "6%", 7 => "7%", 8 => "8%", 9 => "9%", 10 => "10%"],
                                'options' => [
                                    'placeholder' => 'Seleccione %',
                                    'id'=>'descuentou',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                            ]);
                        ?>
                </div>
                <div class="col-lg-6">
                    <h4>
                        <strong>Agregar Producto </strong>
                        <?php echo Html::a('', '#', [
                                'class' => 'glyphicon glyphicon-barcode',
                                'id' => 'activity-index-link-inventario',
                                'data-toggle' => 'modal',
                                'data-trigger'=>'hover', 'data-content'=>'Selecciona para mostrar los productos en el inventario',
                                'data-target' => '#modalinventario',
                                'data-url' => Url::to(['#']),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Inventario'),
                                ]); ?>
                    </h4>

                    <?php
                        echo Html::hiddenInput('codProdServicio',0);
                        echo Html::hiddenInput('cantidad',1);
                    ?>
                    <!--?= /*$form->field($model, 'descripcion')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio', 'nombreProductoServicio'),
                                'options'=>['placeholder'=>'', 'onchange'=>''],
                                'pluginOptions' => ['allowClear' => true]]);*/ ?-->
                    <?php
                           /* echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => 'state_10',
                                'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',
                                function($element) {
                                    return $element['codProdServicio'].' - '.$element['nombreProductoServicio']. ' [¢' .$element['precioVenta']. '] ---- Cantidad en stock: '.$element['cantidadInventario'];
                                }),
                                'options' => [
                                    'id' => 'codProdServiciou',
                                    'placeholder' => 'Seleccione productos',
                                    'onchange'=>'javascript:agrega_productou(this.value)',
                                    'pluginOptions' => ['allowClear' => true],
                                    'multiple' => false, //esto me ayuda a que se obtenga mas de uno
                                    //"update"=>"#conten_pro"
                                ],
                            ]);*/
                            echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'id' => 'medio', 'class' => 'form-control', 'placeholder' =>'Ingrese código de producto aqui', 'onkeyup'=>'javascript:this.value=Mayuculas(this.value)', 'onchange' => 'javascript:agrega_producto(this.value)']);
                    ?>
                </div>
                <div align="right" class="col-lg-4">
                  <?= Html::a('<span class="fa fa-linode"></span> Agregar producto temporal', '#', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal_producto_temporal']) ?>
                </div>
            </div>
            <div class="col-lg-9"><br>
              <div class="table-responsive">
                <?php
                //http://www.programacion.com.py/tag/yii
                    //echo '<div class="grid-view">';
                    echo '<table class="items table scroll table-striped">';
                    echo '<thead>';
                    printf('<tr><th id="td10">%s</th><th id="td10">%s</th><th id="td40">%s</th><th id="td10"><center>%s</center></th><th id="td10"><center>%s</center></th><th id="td5"><center>%s</center></th><th id="td10"><center>%s</center></th><th class="actions button-column" id="td5">&nbsp;</th></tr>',
                            'CANTIDAD',
                            'CÓDIGO',
                            'DESCRIPCIÓN',
                            'PREC.UNIT',
                            'DESCUENTO',
                            'IV %',
                            'TOTAL'
                            );
                    echo '</thead>';
                if($products) {
                $nivel = 'Presiona para mostrar la orden de servicio, verifique que esté al 100% antes de facturar o acreditar';
                ?>

                <?php
                $orden = Html::a('Orden de Servicio', ['#'], [
                                    'id' => 'activity-index-link-update',
                                    'data-toggle' => 'modal',//'data-placement'=>'left',
                                    'data-trigger'=>'hover', 'data-content'=>$nivel,
                                    'data-target' => '#modalOrden',
                                    'data-url' => Url::to(['orden-servicio/view', 'id' => $model->idOrdenServicio]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Orden de Servicio correspondiente'),
                                    ]);
                //$orden = Html::a('Orden de Servicio', '../web/index.php?r=orden-servicio%2Fview&id='.$model->idOrdenServicio);
                echo '<tbody>';
                  $products = array_reverse($products, true);
                    foreach($products as $position => $product) {
                      //-------------------------CONDICION PARA PRODUCTOS SOLO TEMPORALES
                      $espaciocantidad = $product['desc'] == $orden ? $product['cantidad'] : Html::textInput('cantidad_'.$position,
                                                                                                              $product['cantidad'], array(
                                                                                                                'style'=>'text-align: center;',
                                                                                                                'onkeypress'=>'return isNumberDe(event)',
                                                                                                                  'class' => 'cantidad_'.$position, 'size' => '4'
                                                                                                                  )
                                                                                                              );
                      $consultar = '';
                      $iva = 0.13;
                      $dcto = $product['dcto']=='' ? '' : number_format($product['dcto'],2);
                      $detalle_prof = DetalleProformas::find()->where(['=','codProdServicio', $product['cod']])->andWhere("idCabeza_factura = :idCabeza_factura", [":idCabeza_factura"=>$model->idCabeza_Factura])->one();
                      if (@$detalle_prof->temporal=='x') {
                        //$descuento = number_format($product['cantidad']*(($product['precio']*$product['dcto'])/100),2);
                        //necesito obtener el valor intermedio del precio unitario por ganancia para aquellos productos que ya vienes con descuento pero no viene con el valor descuento que se les sacó
                        $monto_solo_con_ganancia = $product['precio_unit_prov_temporal']+($product['precio_unit_prov_temporal']*$product['porc_ganancias_temporal']/100);
                        //esta ecuacion me obtiene el valor de descuento (del 1 al 10)
                        $valor_descuento = (1 - ($product['precio_unit_gan_temporal']/$monto_solo_con_ganancia))*100;
                        $porc_ganancias_temporal = number_format($product['porc_ganancias_temporal'],2);
                        $precio_mas_iva_temporal = $product['precio_unit_gan_temporal'] + ( $product['precio_unit_gan_temporal'] * $iva );
                        $consultar = Html::a('', '#', [
                                'class' => 'fa fa-search',
                                'id' => 'activity-modal-update-temporal',
                                'data-toggle' => 'modal',
                                'data-trigger'=>'hover', 'data-content'=>'Selecciona para mostrar y modificar producto temporal',
                                'data-target' => '#modal_producto_temporal_update',
                                'onclick'=>'javascript:actualiza_producto_temporal('.$position.',"'.$product['cod'].'",
                                                                                   "'.$product['desc'].'",
                                                                                   "'.$product['cantidad'].'",
                                                                                   "'.number_format($product['precio_unit_prov_temporal'],2).'",
                                                                                   "'.$porc_ganancias_temporal.'",
                                                                                   "'.number_format($product['precio_unit_gan_temporal'],2).'",
                                                                                   "'.number_format($precio_mas_iva_temporal,2).'",
                                                                                   "'.$product['dcto'].'",
                                                                                   "'.number_format($product['precio_total_ganacia_mod'],2).'",
                                                                                   "'.number_format($product['precio_mas_iva_total_temporal_mod'],2).'",
                                                                                   "'.$product['checkiva'].'",
                                                                                   "'.round($valor_descuento,2).'"
                                                                                   )',

                                //'data-url' => Url::to(['#']),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Producto temporal'),
                                ]);
                                $espaciocantidad = $product['cantidad'];
                      }
                    //------------------FIN DE CONDICION DE PRODUCTOS TEMPORALES


                        $eliminarlineaproducto = $product['desc'] == $orden ? "" : Html::a('', ['/proforma/deleteitemu', 'id0'=>$product['cod'], 'id1'=>$position, 'id2'=>$model->idCabeza_Factura, 'can'=>$product['cantidad']], ['class'=>'glyphicon glyphicon-remove', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Descartar este producto', 'data' => [
                                        'confirm' => '¿Está seguro de quitar este producto de la proforma?',],]);

                        printf('<tr><td id="td10">%s</td><td id="td10">%s</td><td id="td40">%s</td><td align="right" id="td10">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td10" class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td5" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td10" class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td id="td5" class="actions button-column">%s</td></tr>',
                                $espaciocantidad,
                                $product['cod'],
                                $product['desc'],
                                number_format($product['precio'], 2),
                                number_format($product['dcto'], 2),//Descuento
                                $product['iva'],
                                number_format(($product['precio']*$product['cantidad']),2),
                                //round((($product['precio']-(($product['precio']*$product['dcto'])/100))*$product['cantidad']),2),
                                $eliminarlineaproducto.' '.$consultar

                                );
                            //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
                            $this->registerJs("
                                        $('.cantidad_".$position."').keyup(function() {
                                            $.ajax({
                                                url:'".Yii::$app->getUrlManager()->createUrl('/proforma/updatedescuentou')."',
                                                data: $('.cantidad_".$position."'),
                                                success: function(result) {
                                                $('.descuent_".$position."').html(result);
                                                $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                                '/proforma/getpreciototalu')."');
                                                },
                                                error: function() {
                                                },
                                            });
                                            $.ajax({
                                                url:'".Yii::$app->getUrlManager()->createUrl('/proforma/updatecantidadu')."',
                                                data: $('.cantidad_".$position."'),
                                                success: function(result) {
                                                $('.total_".$position."').html(result);
                                                $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                                '/proforma/getpreciototalu')."');
                                                },
                                                error: function() {
                                                },
                                            });

                                        });

                                    ");
                    }
                  }
                    if (@$products) {
                      printf('<tr><td colspan="8" bgcolor="#CAF5AA" id="td10" align="center">%s</td></tr>',
                                       'El orden de ingreso es de abajo hacia arriba (el último ingresado es el primero en la lista)');
                    } else {
                      printf('<tr><td colspan="8" bgcolor="#5396A4" id="td10" align="center">%s</td></tr>',
                                       '<h3>Área vacía, esperando un producto.</h3>');
                    }
                echo '</tbody>';

                echo '</table>';
                ?>
              </div>
            </div>
            <div class="col-lg-3"><br>
              <div class="well">
                <div id="resumen">
                  <?= F_Proforma::getTotalu() ?>
                </div>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="input-group">
                 <span class="input-group-addon" id="sizing-addon3">Observaciones</span>
                 <?php
                  echo Html::input('text', '', $model->observaciones, ['id' => 'observacion', 'class' => 'form-control', 'placeholder' =>'Agregue alguna observación', 'onKeyUp' => 'javascript:agrega_observacion(this.value)']); ?>
              </div>
            </div>
        </div>
      <!--fin del panel-->
    </div>
</div>
