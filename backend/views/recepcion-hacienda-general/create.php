<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\RecepcionHaciendaGeneral */

$this->title = 'Create Recepcion Hacienda General';
$this->params['breadcrumbs'][] = ['label' => 'Recepcion Hacienda Generals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recepcion-hacienda-general-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
