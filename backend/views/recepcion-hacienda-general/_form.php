<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RecepcionHaciendaGeneral */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recepcion-hacienda-general-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reha_factun_id')->textInput() ?>

    <?= $form->field($model, 'reha_tipo_documento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reha_resolucion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reha_detalle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reha_respuesta_hacienda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reha_fecha_hora')->textInput() ?>

    <?= $form->field($model, 'reha_clave_recepcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reha_xml_recepcion')->textInput() ?>

    <?= $form->field($model, 'reha_xml_respuesta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
