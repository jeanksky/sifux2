<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
use backend\models\Notificar_compra;
use backend\models\Xml_file;
use backend\models\CompraInventarioSession;
use kartik\widgets\AlertBlock;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\RecepcionHaciendaGeneralSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recepción de documentos presentados ante Hacienda';
$this->params['breadcrumbs'][] = $this->title;
$url_enviar_xml_not = Yii::$app->getUrlManager()->createUrl('recepcion-hacienda-general/enviar_xml_not');
$url_view = Yii::$app->getUrlManager()->createUrl('recepcion-hacienda-general/view');
?>
<div class="recepcion-hacienda-general-index">
  <h1 style="text-align: center;"><?= Html::encode($this->title) ?></h1>
  <div class="col-lg-4">
    <?php
        $form = ActiveForm::begin([
            'id'=>'form_get_file',
            'action'=>['/recepcion-hacienda-general/get_file'],
            'options'=>['enctype'=>'multipart/form-data']]); // important
        $model_file = new Xml_file();
             ?>
        <?= $form->field($model_file, 'archivo')->widget(FileInput::classname(), [
          'options' => [  'class'=>'archivo_', 'onchange'=>'document.getElementById("form_get_file").submit()' ],
          'pluginOptions' => [
          //'allowedFileExtensions'=>['xml'],
          'showPreview' => true,
          'showCaption' => true,
          'showRemove' => false,
          'showUpload' => false
        ]])->label('Archivo XML que recibio por la compra:') ?>
        <?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_ALERT,
            'useSessionFlash' => true,
            'delay' => 120000
        ]) ?>
        <?php if (CompraInventarioSession::getRecepcion_doc()): ?>
          <div class="alert alert-info" role="alert">
            <?= CompraInventarioSession::getDetalle_recepcion() ?>
            <br><br>
          </div>
          <div class="col-lg-4">
            <?= Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                'name' => '',
                'data' => [1 => "Aceptado", 2 => "Parcialmente aceptado", 3 => "Rechazado"],
                'options' => [
                    'placeholder' => '- Resolución -',
                    'id'=>'resolucion_',
                    //'onchange'=>'javascript:activar_input_ingreso_producto()',
                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                ],
                'pluginOptions' => ['initialize'=> true,'allowClear' => true]
              ])
            ?>
          </div>
          <div class="col-lg-6">
            <?= Html::input('text', '', '', ['autocomplete'=>'off', 'id' => 'detalle_', 'class' => 'form-control', 'placeholder' =>'Detalle']) ?>
          </div>
          <div class="col-lg-2">
            <button class="btn btn-default" type="button" onclick="enviar_xml_not(this, '<?= $url_enviar_xml_not ?>')" data-loading-text="Enviando...">Enviar</button>
          </div>
        <?php endif; ?>
        <?php ActiveForm::end(); ?>
        <div id="nota_xml"></div><br>
  </div>
  <div class="col-lg-8">
    <input type="hidden" id="estado_h" value="NULLO">
    <h2 style="text-align: center;">Lista de documentos recibidos</h2>
    <div class="col-md-2">
    <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
    'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy',
    'id'=>'find_fecha_re', 'autocomplete'=>'off', 'onchange' => 'javascript:buscar_recepcion("' .$url_view. '")']]) ?>
    </div>
    <div class="col-md-3">
      <?= Html::input('text', '', '', ['autocomplete'=>'off', 'id' => 'find_proveedores', 'class' => 'form-control', 'placeholder' =>'Buscar proveedor', 'onchange' => 'javascript:buscar_recepcion("' .$url_view. '")']) ?>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-3">
      <?= Html::input('text', '', '', ['autocomplete'=>'off', 'id' => 'find_consecutivo', 'class' => 'form-control', 'placeholder' =>'Buscar consecutivo', 'onchange' => 'javascript:buscar_recepcion("' .$url_view. '")']) ?>
      <br>
    </div>
    <div id="tbl_recepcion" style="height: 600px;width: 100%; overflow-y: auto; ">
      <?= $this->render('view', [
          'reha_fecha_hora' => date('Y-m-d 00:00:00'),
          'reha_nombre_emisor'=>'',
          'reha_consecutivo_electronico'=>''
      ]) ?>
    </div>
  </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //re_enviar_xml_not();
    verificarEstadoHAciendoGometa();
    setInterval('verificarEstadoHAciendoGometa()',60000); /*1 min*/
    setTimeout("re_enviar_xml_not()", 8000 );
    setInterval('re_enviar_xml_not()',120000); /*120 segundos*/
  });

  //enviar a hacienda los datos
  function enviar_xml_not(this_, url_enviar_xml_not){
    var resolucion = $('#resolucion_').val();
    var detalle = $('#detalle_').val();
    if (resolucion!='' && detalle!='') {
      $(this_).button('loading');
      $.ajax({
        url: url_enviar_xml_not,
        type:"post",
        data: { 'resolucion' : resolucion, 'detalle': detalle },
      });
    } else {
      alert('¡Atencion, no deje campos vacios!');
    }
  }

  function buscar_recepcion(url_view){
    var fecha_re = $('#find_fecha_re').val();
    var proveedores = $('#find_proveedores').val();
    var consecutivo = $('#find_consecutivo').val();
    $.get(url_view, {
      'reha_fecha_hora' : fecha_re,
      'reha_nombre_emisor' : proveedores,
      'reha_consecutivo_electronico' : consecutivo
    },
      function(data){
        $('#tbl_recepcion').html(data);
      });
  }

  /*vehiel 05-12-2018
  metodo que verificacion del estado del tarro de haciendo con el api de gometa*/
  function verificarEstadoHAciendoGometa(){
    gometa = 'http://apis.gometa.org/status/status.json';
    proxy = 'https://cors-anywhere.herokuapp.com/';
    url = proxy+gometa;
    $.ajax({
      'url': url,
      'dataType':'json',
      'crossDomain':false,
      'type':'get',
      beforeSend: function(xhr){
        xhr.setRequestHeader("Access-Control-Allow-Origin","*");
      },
      success: function(data){
        document.getElementById("estado_h").value = data['api-prod']['status'];
      },
      error:function(){
      }
    });
  }

  //confirma el estado de resolucio en hacienda en segundo plano, o reenvia la resolucio si está estancada
  function re_enviar_xml_not() {
    var estado_hacienda =  document.getElementById("estado_h").value;
    if (estado_hacienda == 'OK') {
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('recepcion-hacienda-general/renviar_xml_not') ?>",
          type:"post",
          success: function(data){
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/renviar_xml_not') ?>",
                type:"post",
                success: function(data){
                  $.ajax({
                      url:"<?php echo Yii::$app->getUrlManager()->createUrl('gastos/renviar_xml_not') ?>",
                      type:"post",
                      success: function(data){ },
                  });//fin ajax gastos
                },
            });//fin ajax compras-inventario
          },
      });//fin ajax recepcion-hacienda-general
    }
  }
</script>
