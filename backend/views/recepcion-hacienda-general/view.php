<?php
use yii\helpers\Html;
use backend\models\RecepcionHaciendaGeneral;

$fecha_hora_d = $reha_fecha_hora != '' ? date('Y-m-d 00:00:00', strtotime( $reha_fecha_hora )) : date('Y-m-d 00:00:00');
$fecha_hora_h = $reha_fecha_hora != '' ? date('Y-m-d 23:59:59', strtotime( $reha_fecha_hora )) : date('Y-m-d 23:59:59');
$consulta = RecepcionHaciendaGeneral::find()
->where('reha_fecha_hora BETWEEN :fecha_hora_d AND :fecha_hora_h', [':fecha_hora_d' => $fecha_hora_d, ':fecha_hora_h' => $fecha_hora_h])
->andWhere('reha_nombre_emisor LIKE :nombre_emisor AND reha_consecutivo_electronico LIKE :consecutivo_electronico',
[':nombre_emisor'=>'%'.$reha_nombre_emisor.'%', ':consecutivo_electronico'=>'%'.$reha_consecutivo_electronico.'%'])
->orderBy(['reha_fecha_hora' => SORT_DESC])->all();
/*$sql = "SELECT * FROM tbl_recepcion_hacienda";
$command = \Yii::$app->db->createCommand($sql);
//$command->bindValue(":idGastos", $model->idGastos);
$consulta = $command->queryAll();*/
$detalle_notificacion = '';

foreach ($consulta as $key => $value) {
  if ($value['reha_tipo_documento'] == 'Factura electrónica') { $tipo_documento = 'FE - '; }
  elseif($value['reha_tipo_documento'] == 'Nota de débito electrónica') { $tipo_documento = 'NDE - '; }
  elseif($value['reha_tipo_documento'] == 'Nota de crédito electrónica') { $tipo_documento = 'NCE - '; }
  else  { $tipo_documento = 'TE - '; }
  $clave_recepcion = Html::a($value['reha_factun_id'], null, [
                //'class'=>'fa-2x',
                'style'=>'cursor:pointer;',
                'data-content' => $value['reha_clave_recepcion'],
                'data-toggle' => 'popover',
                'title' => Yii::t('app', 'Clave recepción'),
              ]);
  $consulta = Html::a('<span class=""></span>', ['recepcion-hacienda-general/pdf_resolucion', 'id'=>$value['reha_id']], [
                'class'=>'fa fa-file-pdf-o fa-2x',
                'id' => 'pdf',
                'target'=>'_blank',
                'data-pjax' => '0',
                'title' => Yii::t('app', 'Mostrar Resolución en PDF'),
              ]);
  if ($value['reha_resolucion'] == 'Aceptado') {
    $reha_resolucion = '<strong><font color="#26DD54">'.$value['reha_resolucion'].'</font></strong>';
  } else if ($value['reha_resolucion'] == 'Rechazado') {
    $reha_resolucion = '<strong><font color="red">'.$value['reha_resolucion'].'</font></strong>';
  } else if ($value['reha_resolucion'] == 'Parcialmente aceptado') {
    $reha_resolucion = '<strong><font color="#FFBF00">'.$value['reha_resolucion'].'</font></strong>';
  }
  if ( $value['reha_respuesta_hacienda'] == '01' ) {
    $respuesta_hacienda = '<font color="#26DD54">ACEPTADA POR HACIENDA</font>';
  } elseif ( $value['reha_respuesta_hacienda'] == '03' ) {
    $respuesta_hacienda = '<font color="red">RECHAZADA POR HACIENDA</font>';
  } else {
    $respuesta_hacienda = 'ESPERANDO RESPUESTA';
  }
  $detalle_notificacion .= '<tr>
                              <td>'.date('d-m-Y h:i:s A', strtotime( $value['reha_fecha_hora'] )).'</td>
                              <td>'.$value['reha_nombre_emisor'].'</td>
                              <td>'.$reha_resolucion.'</td>
                              <td>'.$tipo_documento.$value['reha_consecutivo_electronico'].'</td>
                              <td>'.$respuesta_hacienda.'</td>
                              <td>'.$consulta.'</td>
                            </tr>';
}
echo '
<table class="items table table-striped" >
  <thead>
    <tr>
      <th>Fecha recepción</th>
      <th>Proveedor</th>
      <th>Resolución</th>
      <th>Tipo - Consecutivo</th>
      <th>Respuesta de Hacienda</th>
      <th></th>
    <tr>
  </thead>
  <tbody>
  '.$detalle_notificacion.'
  </tbody>
</table>';
?>
