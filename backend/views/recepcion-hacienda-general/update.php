<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RecepcionHaciendaGeneral */

$this->title = 'Update Recepcion Hacienda General: ' . ' ' . $model->reha_id;
$this->params['breadcrumbs'][] = ['label' => 'Recepcion Hacienda Generals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reha_id, 'url' => ['view', 'id' => $model->reha_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recepcion-hacienda-general-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
