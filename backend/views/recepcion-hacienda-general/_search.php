<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\RecepcionHaciendaGeneralSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recepcion-hacienda-general-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reha_id') ?>

    <?= $form->field($model, 'reha_factun_id') ?>

    <?= $form->field($model, 'reha_tipo_documento') ?>

    <?= $form->field($model, 'reha_resolucion') ?>

    <?= $form->field($model, 'reha_detalle') ?>

    <?php // echo $form->field($model, 'reha_respuesta_hacienda') ?>

    <?php // echo $form->field($model, 'reha_fecha_hora') ?>

    <?php // echo $form->field($model, 'reha_clave_recepcion') ?>

    <?php // echo $form->field($model, 'reha_xml_recepcion') ?>

    <?php // echo $form->field($model, 'reha_xml_respuesta') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
