<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\models\Clientes;
use backend\models\Vehiculos;
use backend\models\ListaFacturaVehicular;
use backend\models\Marcas;
use backend\models\Modelo;
//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $model backend\models\Vehiculos */
$marca = Marcas::findOne($model->marca);
$modelo = Modelo::findOne($model->modelo);
$cliente = Clientes::findOne($model->idCliente);
if ($cliente!= ''){
   $nombre_cliente = Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$model->idCliente);
}else
{
    $nombre_cliente = "Sin asignar";
}

$facturas_ = ListaFacturaVehicular::getContenidoFacturaVe();
$this->title = 'N° de placa: '.$model->placa. ' / '. $marca->nombre_marca . ' ' . $modelo->nombre_modelo;
$this->params['breadcrumbs'][] = ['label' => 'Vehículos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
    $(document).on('click', '#activity-index-link-orden', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalOrden').modal();
                    }
                );
            }));
    $(document).on('click', '#activity-index-link-factura', (function() {
                    $.get(
                        $(this).data('url'),
                        function (data) {
                            $('.modal-body').html(data);
                            $('#modalFactura').modal();
                        }
                    );
                }));

    //curiosamente esta funcion debe ir primero antes de la siguiente sino no funsiona
    $(document).ready(function(){
        $('[data-toggle="modal"]').tooltip();
    });

    //Esta funcion me filtra los datos de la tabla
    $(document).ready(function () {
            (function ($) {
                $('#filtrar').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));
            $('#tabla_lista_facturas').dataTable({
            "sPaginationType" : "full_numbers"//DAMOS FORMATO A LA PAGINACION(NUMEROS)
            });
            $('[data-toggle="modal"]').tooltip();
        });



</script>

<div class="vehiculos-view">
<div class="col-lg-12">
    <h3><?= Html::encode($this->title) ?><br> <?php echo "Propietario: " . $nombre_cliente;  ?></h3>

     <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->placa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['#'], [
            'id' => 'activity-index-link-delete',
            'class' => 'btn btn-danger',
            'data-toggle' => 'modal',
            'data-target' => '#modalDelete',
            'onclick' => 'javascript:enviaResultado('.$model->placa.')',
            'href' => Url::to('#'),
            'data-pjax' => '0',
        ]) ?>


    </p>
</div>
<div class="col-lg-6">
    <div id="semitransparente">
        <?=
         DetailView::widget([
            'model'=>$model,
            'hAlign'=> DetailView::ALIGN_LEFT ,
            'attributes' => [
                    ['attribute'=>'placa',  'valueColOptions'=>['style'=>'width:40%'] ],
                    'version',
                    'anio',
                ]
        ]);
        ?>

    </div>
</div>
<div class="col-lg-6">
    <div id="semitransparente">
        <?=
         DetailView::widget([
            'model'=>$model,
            'hAlign'=> DetailView::ALIGN_LEFT ,
                'attributes' => [
                    'combustible',
                    'motor',
                    ['attribute'=>'cilindrada_motor',  'valueColOptions'=>['style'=>'width:40%'] ],
                    [
                        'attribute' => 'idCliente',
                        'label' => 'Código',
                      /*  'value' => function ($model, $key, $index, $grid) {
                           // $cliente = Clientes::findOne($model->idCliente);
                            return Html::a($model->idCliente, '../web/index.php?r=clientes%2Fview&id='.$model->idCliente);
                        },*/
                    ],
                   // 'idCliente',
                ]
        ]);
        ?>
    </div>
</div>
</div>
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 style="text-align:center">Facturas correspondientes a este vehículo</h2>
            <p style="text-align:center">Accede a las Facturas y Orden de Servicio correspondiente para ver su respectivo detalle, para su mayor control y seguimiento.</p>

            <div class="input-group">
              <span class="input-group-addon">Buscar</span>
              <input id="filtrar" type="text" class="form-control" placeholder="Ingrese una busqueda, ejemplo: 12-08-2016">
            </div>


        </div>
        <div id="conten_pro">
                <?php
                //http://www.programacion.com.py/tag/yii
                    //echo '<div class="grid-view">';
                    echo '<table class="items table table-striped" id="tabla_lista_facturas" >';
                    echo '<thead>';
                    printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th class="actions button-column">&nbsp;</th></tr>',
                            'ID',
                            'ESTADO',
                            'FECHA INICIO',
                            'FECHA FINAL',
                            'SUBTOTAL',
                            'DESCUENTO',
                            'IV',
                            'TOTAL'
                            );
                    echo '</thead>';
                if($facturas_) {
                    echo '<tbody class="buscar">';
                        foreach($facturas_ as $position => $factura) {

                            $mostrarOrden = Html::a('', ['#'], [
                                'class'=>'glyphicon glyphicon-scale',
                                'id' => 'activity-index-link-orden',
                                'data-toggle' => 'modal',
                                'data-target' => '#modalOrden',
                                'data-url' => Url::to(['orden-servicio/view', 'id' => $factura['idOrdenServicio']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Procede a mostrar la orden de servicio')]);
                            $mostrarFactura = Html::a('', ['#'], [
                                'class'=>'glyphicon glyphicon-list-alt',
                                'id' => 'activity-index-link-factura',
                                'data-toggle' => 'modal',
                                'data-target' => '#modalFactura',
                                'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Procede a mostrar la factura')]);

                            $mostrarAccion = $mostrarOrden.' '.$mostrarFactura;

                            $fecha_inicio = date('d-m-Y', strtotime( $factura['fecha_inicio'] ));
                            $fecha_final = '';
                            if ($factura['fecha_final']=='') {
                                $fecha_final = 'Pendiente';
                            } else {
                                $fecha_final = date('d-m-Y', strtotime( $factura['fecha_final'] ));
                            }

                            printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="actions button-column">%s</td></tr>',
                            $factura['idCabeza_Factura'],
                            $factura['estadoFactura'],
                            $fecha_inicio,
                            $fecha_final,
                            number_format($factura['subtotal'],2),
                            number_format($factura['porc_descuento'],2),
                            number_format($factura['iva'],2),
                            number_format($factura['total_a_pagar'],2),
                            $mostrarAccion
                            );
                        }
                    echo '</tbody>';
                }
                echo '</table>';
                ?>
        </div>
    </div>
</div>


<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un vehículo necesita del permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "placa" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Vehículo', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este vehículo?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
 <?php //Modal para mostrar orden de servicio
         Modal::begin([
                'id' => 'modalOrden',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Orden de servicio</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
            ]);
            echo "<div class='well'></div>";

            Modal::end();
    ?>
    <?php // modal para mostrar factura
         Modal::begin([
                'id' => 'modalFactura',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Factura</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
            ]);
            echo "<div class='well'></div>";

            Modal::end();
    ?>
</div>
