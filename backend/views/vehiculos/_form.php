<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Clientes;
use backend\models\Version;
use backend\models\Modelo;
use backend\models\Marcas;
use kartik\widgets\DepDrop;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Vehiculos */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div id="semitransparente">
<div class="col-lg-12">
       <div class="panel panel-default">
            <div class="panel-heading">
                Ingrese los datos que se le solicitan en el siguiente formulario.
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

<div class="vehiculos-form">
<div class="col-lg-4">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'placa')->textInput(['maxlength' =>10])  ?>


<!--?= $form->field($model, 'marca')->dropdownList(
            ArrayHelper::map(Marcas::find()->all(),'codMarca','codMarca'),
            [
                'id'=>'ddl-marca',
                'prompt'=>'- Selecccione -'
]); ?-->

<?= $form->field($model, 'marca')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Marcas::find()->all(), 'codMarca', function($element){
                                  return $element['nombre_marca'];
                                }),
                                'options'=>['id'=>'ddl-marca', 'placeholder'=>'- Seleccione -'],
                                'pluginOptions' => ['allowClear' => true],
                            ]);?>
<?= $form->field($model, 'modelo')->widget(DepDrop::classname(), [
            //'type'=>DepDrop::TYPE_SELECT2,
            'options'=>['id'=>'ddl-modelo'],
            'data'=> ArrayHelper::map(Modelo::find()->all(), 'codModelo', 'nombre_modelo'),
            'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            'pluginOptions'=>[
                'depends'=>['ddl-marca'],
                'placeholder'=>'- Seleccione -',
                'initialize'=> true,
                'url'=>Url::to(['vehiculos/modelo'])
            ]
        ]); ?>



</div><div class="col-lg-4">
    <?= $form->field($model, 'version')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Version::find()->all(),'codVersion','codVersion'),
    'language'=>'es',
    'options' => ['placeholder' => '- Selecccione -'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);
    ?>

    <?= $form->field($model, 'anio')->widget(\yii\widgets\MaskedInput::className(), [
                'name' => 'input-3',
                'mask' => '9',
                'clientOptions' => ['repeat' => 4, 'greedy' => false]]); ?>

    <?= $form->field($model, 'combustible')->radioList(['Gas'=>'Gasolina','Diesel'=>'Diesel'])?>
</div><div class="col-lg-4">
    <?= $form->field($model, 'motor')->textInput(['maxlength' => 50]) ?>


    <?= $form->field($model, 'cilindrada_motor')
                ->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999CC',
                 ]) ?>
    <!--?= $form->field($model, 'idCliente')->dropDownList(
                ArrayHelper::map(Clientes::find()->all(),'idCliente','nombreCompleto'),['prompt' => 'Selecccione']
                )?-->

<?=
    // Usage with ActiveForm and model
$form->field($model, 'idCliente')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Clientes::find()->orderBy(['idCliente' => SORT_DESC])->all(),'idCliente','nombreCompleto'),
    'language'=>'es',
    'options' => ['placeholder' => '- Selecccione -'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);
    ?>



    <div class="form-group" style="text-align:right">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

            </div> </div> </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
