<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Clientes;
use backend\models\Vehiculos;
use yii\widgets\Pjax;
use kartik\widgets\AlertBlock;
use backend\models\Marcas;
use backend\models\Modelo;
//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\VehiculosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehículos';
$this->params['breadcrumbs'][] = $this->title;
//echo date('l jS \of F Y h:i:s A');
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<div class="vehiculos-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p style="text-align:right">
        <?= Html::a('Crear vehículos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div id="semitransparente">
      <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?php //Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'placa',
            [
                        'attribute' => 'tbl_marcas.nombre_marca',
                        'label' => 'Marcas',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $marca = Marcas::findOne($model->marca);
                            if (@$marca)
                              return $marca->nombre_marca;
                            else {
                              return 'Sin marca';
                            }
                        },
            ],
            [
                        'attribute' => 'tbl_modelo.nombre_modelo',
                        'label' => 'Modelos',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $modelo = Modelo::findOne($model->modelo);
                            if (@$modelo)
                              return $modelo->nombre_modelo;
                            else {
                              return 'Sin modelo';
                            }
                        },
            ],
            // 'version',
            'anio',
            // 'combustible',
            // 'motor',
            // 'cilindrada_motor',
            //'idCliente',
            [
                        'attribute' => 'tbl_clientes.nombreCompleto',
                        'label' => 'Cliente',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $cliente = Clientes::findOne($model->idCliente);
                            if($cliente == '')
                            return 'Propietario no asignado';
                            else
                            return Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$model->idCliente);
                        },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                                'delete' => function ($url, $model, $key) {

                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                        'id' => 'activity-index-link-delete',
                                        'class' => 'activity-delete-link',
                                        'title' => Yii::t('app', 'Eliminar vehículo '.$model->placa),
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modalDelete',
                                        'onclick' => 'javascript:enviaResultado('.$model->placa.')',
                                        'href' => Url::to('#'),
                                        'data-pjax' => '0',
                                        ]);
                                },
                             ]
            ],
        ],
    ]); ?>
    <?php //Pjax::end(); ?>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un vehículo necesita del permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "placa" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Vehículo', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este vehículo?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
</div>
