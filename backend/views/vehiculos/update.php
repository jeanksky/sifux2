<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
/* @var $this yii\web\View */
/* @var $model backend\models\Vehiculos */

$this->title = 'Actualizar vehículo: ' . ' ' . $model->placa;
$this->params['breadcrumbs'][] = ['label' => 'Vehículos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->placa, 'url' => ['view', 'id' => $model->placa]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="vehiculos-update">

<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
