    <?php
        use backend\models\ComprasInventario;
        use yii\helpers\Html;
        use yii\helpers\Url;
    ?>
    <script type="text/javascript">
    $(document).ready(function () {
            //me checa todas las facturas pendientes
            $('#checkAll').on('click', function() {
                if (this.checked == true)
                    $('#tabla_facturas_pendientes').find('input[name="checkboxRow[]"]').prop('checked', true);
                else
                    $('#tabla_facturas_pendientes').find('input[name="checkboxRow[]"]').prop('checked', false);
            });
    });
    </script>
    <div class="panel panel-info">
        <div style="height: 300px;width: 100%; overflow-y: auto; ">

            <?php
                $facturasCredito = '';
                if ($conditionfecha == 'todas') {
                    $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago", [":estado_pago"=>'Pendiente'])->orderBy(['idCompra' => SORT_DESC])->all();
                }
                if ($conditionfecha == 'hoy') {
                    $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago AND fechaVencimiento = :tipofecha", [":estado_pago"=>'Pendiente', ':tipofecha' => date('Y-m-d')])->orderBy(['idCompra' => SORT_DESC])->all();
                }
                if ($conditionfecha == 'vencidas') {
                    $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago AND fechaVencimiento < :tipofecha", [":estado_pago"=>'Pendiente', ':tipofecha' => date('Y-m-d')])->orderBy(['idCompra' => SORT_DESC])->all();
                }
                if ($conditionfecha == 'porvencer') {
                    $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago AND
                        (fechaVencimiento = :tipofecha + INTERVAL 5 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 4 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 3 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 2 DAY OR
                        fechaVencimiento = :tipofecha + INTERVAL 1 DAY)", [":estado_pago"=>'Pendiente', ':tipofecha' => date('Y-m-d')])->orderBy(['idCompra' => SORT_DESC])->all();
                }

                echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
                echo '<thead>';
                printf('<tr>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th class="actions button-column">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="checkAll" /></th></tr>',
                        'N° IN-PROV',
                        'N° FACTURA',
                        'FECH.REG',
                        'FECH.VEN',
                        'MONTO',
                        'NOTA DÉBITO',
                        'NOTA CRÉDITO',
                        'ABONO',
                        'SALDO FACTURA'
                        );
                echo '</thead>';
                echo '<tbody class="buscar1">';
                foreach($facturasCredito as $position => $factura) {
                    $idCompra = "'".$factura['idCompra']."'";
                    $command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND idCompra = " . $idCompra);
                    $command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND idCompra = " . $idCompra);
                    $command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND idCompra = " . $idCompra);
                    $suma_monto_nc = floatval($command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
                    $suma_monto_nd = floatval($command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
                    $suma_monto_ab = floatval($command_ab->queryScalar()); //ingreso la suma del monto de abonos
                    $fecha_actual = date('d-m-Y');
                    list ($dia_hoy, $mes_hoy, $anio_hoy) = explode("-", $fecha_actual);//separo dias mes año de hoy
                    $border = '';
                    $fecha_registro = date('d-m-Y', strtotime( $factura['fechaRegistro'] ));
                    $fecha_vencimiento = date('d-m-Y', strtotime( $factura['fechaVencimiento'] ));
                    list ($dia_ven, $mes_ven, $anio_ven) = explode("-", $fecha_vencimiento);
                        $fecha_antes_vencimiento5 = date ( 'd-m-Y' , strtotime ( '-5 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento4 = date ( 'd-m-Y' , strtotime ( '-4 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento3 = date ( 'd-m-Y' , strtotime ( '-3 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento2 = date ( 'd-m-Y' , strtotime ( '-2 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento1 = date ( 'd-m-Y' , strtotime ( '-1 day' , strtotime ( $fecha_vencimiento ) ) );
                    if(strtotime($fecha_vencimiento) < strtotime($fecha_actual)){
                        $border = 'red 10px solid;';
                        }
                    if($fecha_actual == $fecha_vencimiento){
                        $border = 'orange 10px solid;';
                    }
                    if(($fecha_actual == $fecha_antes_vencimiento5) || ($fecha_actual == $fecha_antes_vencimiento4) || ($fecha_actual == $fecha_antes_vencimiento3) || ($fecha_actual == $fecha_antes_vencimiento2) || ($fecha_actual == $fecha_antes_vencimiento1)){
                        $border = 'blue 10px solid;';
                    }
                    $mostrarFactura = Html::a('', ['#'], [
                                    'class'=>'glyphicon glyphicon-search',
                                    'id' => 'activity-index-link-factura',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalFactura',
                                    'data-url' => Url::to(['tramite-pago/vista_factura', 'id' => $factura['idCompra']]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Procede a mostrar la factura')]);
                    $idFactura_activa = $factura['idCompra'];
                    $checkbox = '<input id="striped" type="checkbox" name="checkboxRow[]" value="'.$idFactura_activa.'">';
                    printf('<tr style = "border-left: '.$border.' border-right: '.$border.'" onclick="marcarfactura(this, '.$factura['idCompra'].')">
                            <td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                                $factura['n_interno_prov'],
                                $factura['numFactura'],
                                $fecha_registro,
                                $fecha_vencimiento,
                                number_format($factura['total'],2),
                                number_format($suma_monto_nd,2),
                                number_format($suma_monto_nc,2),
                                number_format($suma_monto_ab,2),
                                number_format($factura['credi_saldo'],2),
                                $mostrarFactura . '&nbsp;' . $checkbox
                                );
                }
                echo '</tbody>';
                echo '</table>';
            ?>

        </div>
    </div>
