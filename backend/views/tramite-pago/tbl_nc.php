<script type="text/javascript">
	$('[data-toggle="popover_nc"]').popover();//me dice el tipo de popover que quiero correspondiente al id
	$('[data-toggle="popover_nc_u"]').popover();//me dice el tipo de popover que quiero correspondiente al id

	//esta funcion va aqui porque se debe refrescar en el momento que se llama tbl_nc
	function proceso_actualizar_nc(idMov, detalle, monto_movimiento, numero_nc) {
		$('#activity-update-movimiento'+idMov).popover('show');
		$('#detalle_nc_u').val(detalle);
        $('#numero_nc_u').val(numero_nc);
        $('#monto_nc_u').val(monto_movimiento);
        $('#id_nc_u').val(idMov);
        //$('#btn_accion_nc').val('Actualizar Mov #'+idMov);
	}
</script>
<?php
use yii\helpers\Html;
use backend\models\MovimientoPagar;

    $movimientofactura = MovimientoPagar::find()->where(['idCompra'=>$idCompra])->andWhere("tipo_mov = :tipo_mov", [":tipo_mov"=>'nc'])->orderBy(['idMov' => SORT_DESC])->all();
    $update_nc = '  <div class="col-lg-12">
                        Detalle:<br>
                        <textarea id="detalle_nc_u" rows="2" class="form-control" onkeypress="if(this.value.length == 120){return false;}else{return toUpper(event,this);}"></textarea><br>
                    </div>
                    <div class="col-lg-6">
                        N°.Not.Cré:<br>
                        <input id="numero_nc_u" type="text" name="numero_nc_u" class="form-control" onkeypress="return isNumberDe(event)" >
                    </div>
                    <div class="col-lg-6">
                        Monto:<br>
                        <input id="monto_nc_u" type="numbre" name="monto_nc_u" class="form-control" onkeypress="return isNumberDe(event)" ><br>
                        <input id="id_nc_u" type="hidden" name="id_nc" >
                    </div>
                    <div class="col-lg-6"><br>
                        <input id="" type="button" class="btn btn-link" onclick="actualizar_nc()" value="Actualizar" >

                    </div>';
    echo '<div style="height: 150px; width: 100%; overflow-y: auto; ">';
    echo '<table class="items table table-striped" id="tabla_facturas_proceso"  >';
    echo '<thead>';
    printf('<tr>
        <th><font face="arial" size=1>%s</font></th>
        <th><font face="arial" size=1>%s</font></th>
        <th><font face="arial" size=1>%s</font></th>
        <th class="actions button-column"></th></tr>',
            'N° NOTA',
            'DETALLE',
            'MONTO'
            );
    echo '</thead>';
    echo '<tbody>';
    foreach($movimientofactura as $position => $movi) {
        $info_nc = 'Fecha: '.date('d-m-Y', strtotime($movi['fecha_mov'])).'<br>
                    N°.NC Proveedor: '.$movi['numero_mov'].'<br>
                    Monto anterior: '.number_format($movi['monto_anterior'],2).'<br>
                    Monto movimiento: '.number_format($movi['monto_movimiento'],2).'<br>
                    Saldo pendiente: '.number_format($movi['saldo_pendiente'],2).'<br>
                    Usuario: '.$movi['usuario'];
        $jtimes_view = "$('#activity-info-movimiento".$movi['idMov']."').popover('hide')";//scrip oculta view nc
        $jtimes_update = "$('#activity-update-movimiento".$movi['idMov']."').popover('hide')";//scrip oculta update nc
        $consultar = Html::a('', null, [
                        'class'=>'glyphicon glyphicon-search',
                        'id' => 'activity-info-movimiento'.$movi['idMov'],
                        'data-toggle' => 'popover_nc',
                        'data-placement' => 'left',
                        'data-html'=>'true',
                        'data-content'=>$info_nc,
                        'title' => Yii::t('app', 'Movimiento #'.$movi['idMov']).'&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
					        <i onclick="'.$jtimes_view.'" class="fa fa-times fa-1x" data-placement="bottom" ></i>
					    ' ]);
        $editar = Html::a('', null, [
                        'class'=>'fa fa-pencil',
                        'id' => 'activity-update-movimiento'.$movi['idMov'],
                        'onclick'=>'proceso_actualizar_nc('.$movi['idMov'].',"'.$movi['detalle'].'",'.$movi['monto_movimiento'].','.$movi['numero_mov'].')',
                        'data-toggle' => 'popover_nc_u',
                        'data-placement' => 'left',
                        'data-html'=>'true',
                        'data-content'=>$update_nc,
                        'title' => Yii::t('app', 'Actualizar movimiento #'.$movi['idMov']).'&emsp;&emsp;&emsp;&emsp;
					        <i onclick="'.$jtimes_update.'" class="fa fa-times fa-1x" data-placement="bottom" ></i>
					    ' ]);
        $eliminar = Html::a('', null, [
                        'class'=>'fa fa-trash',
                        'id' => 'activity-delete-movimiento',
                        'onclick'=>'eliminar_nc('.$movi['idMov'].','.$movi['monto_movimiento'].')',
                        'title' => Yii::t('app', 'Eliminar nota de crédito') ]);
        /*$imprimir = Html::a('<span class=""></span>', null, [
                        'class'=>'fa fa-print',
                        'id' => 'activity-index-link-report',
                        'onclick'=>'imprimir_nc('.$movi['idMov'].')',
                        //'target'=>'_blank',
                        'data-toggle' => 'titulo',
                        //'data-url' => Url::to(['create']),
                        'data-pjax' => '0',
                        'title' => Yii::t('app', 'Imprimir movimiento'),
                        ]);*/
        $accion_mov = $consultar . ' ' . $editar . ' ' . $eliminar /*. ' ' . $imprimir*/;

        printf('<tr>
                    <td><font face="arial" size=2>%s</font></td>
                    <td><font face="arial" size=1>%s</font></td>
                    <td align="right"><font face="arial" size=2>%s</font></td>
                    <td class="actions button-column">%s</td></tr>',
                $movi['idMov'],
                $movi['detalle'],
                number_format($movi['monto_movimiento'],2),
                $accion_mov
            );
    }
    echo '</tbody>';
    echo '</table>';
    echo '</div>';
?>
