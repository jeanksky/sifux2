<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TramitePago */

$this->title = $model->idTramite_pago;
$this->params['breadcrumbs'][] = ['label' => 'Tramite Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tramite-pago-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idTramite_pago], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idTramite_pago], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTramite_pago',
            'idProveedor',
            'cantidad_saldo_documento',
            'monto_tramite_pago',
            'procentaje_descuento',
            'monto_pagar',
            'recibo_cancelacion',
            'email_proveedor:email',
            'prioridad_pago',
            'usuario_registra',
            'fecha_registra',
            'usuario_cancela',
            'fecha_cancela',
            'usuario_aplica',
            'fecha_aplica',
        ],
    ]) ?>

</div>
