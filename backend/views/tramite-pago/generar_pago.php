<?php
	use backend\models\ComprasInventario;
	use backend\models\CuentasBancarias;
	use backend\models\CuentasBancariasProveedor;
	use kartik\widgets\DatePicker;
	use backend\models\ContactosPagos;
	use backend\models\Proveedores;
	use backend\models\MovimientoPagar;
	use backend\models\TramitePago;
	use backend\models\MedioPagoProveedor;
	use yii\helpers\ArrayHelper;
	use kartik\widgets\Select2;
	use yii\bootstrap\Modal;
	use yii\helpers\Html;
  use backend\models\DebitoSession;

	$suma_total_proceso = 0;
	$suma_saldo_proceso = 0;
	$suma_facturas = 0;
	$suma_monto_nc = 0;
	$suma_monto_nd = 0;
	$suma_monto_ab = 0;
	$usuario = '';

	//obtengo instancia de compras de inventario para saber cuantas facturas de estre proveedor estan en proceso
	$facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago", [":estado_pago"=>'Proceso'])->orderBy(['idCompra' => SORT_DESC])->all();
	if ($facturasCredito) {
		$usuario = Yii::$app->user->identity->username;
    	foreach($facturasCredito as $factura_proceso) {
    	$numerofac = "'".$factura_proceso['numFactura']."'";
	    $suma_total_proceso += $factura_proceso['total'];
	    $suma_saldo_proceso += $factura_proceso['credi_saldo'];
	    $suma_facturas += 1;
	    $command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND numFactura = " . $numerofac);
        $command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND numFactura = " . $numerofac);
        $command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND numFactura = " . $numerofac);
        $suma_monto_nc += floatval($command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
        $suma_monto_nd += floatval($command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
        $suma_monto_ab += floatval($command_ab->queryScalar()); //ingreso la suma del monto de abonos
	    }
	}
    $monto_credito = $suma_monto_nc + $suma_monto_ab;//con eso obtengo el monto total de monto_credito
    $monto_debito = $suma_monto_nd;//y declaro que $monto_debito sea la suma de notss de eito

	//obtengo instancia de Movimiento a pagar para saber cuantas notas de credito sin asignacion se han hecho para este proveedor
	$proveedor = Proveedores::find()->where(['codProveedores'=>$idProveedor])->one();
    $movi_pagar = MovimientoPagar::find()->where(['proveedor'=>$proveedor->nombreEmpresa])
    ->andWhere("numFactura = :numFactura AND idTramite_pago = :idTramite_pago",
    	[":numFactura"=>"", ":idTramite_pago"=>0])->orderBy(['idMov' => SORT_DESC])->all();
    $cantidad_nc_tramite = 0;
    $montototal_nc = $montototal_nd = 0;
    //$suma_saldo_y_nc = 0;
    //obtenemos los datos de las notas de credito al monto del tramite
    if ($movi_pagar) {
    	foreach ($movi_pagar as $movimiento) {
    		$cantidad_nc_tramite +=1;
				if ($movimiento->tipo_mov == 'nd') {
					$montototal_nd += $movimiento['monto_movimiento'];
				} else {
	    		$montototal_nc += $movimiento['monto_movimiento'];
				}
    	}
    }
    $suma_saldo_y_nc = $suma_saldo_proceso - $montototal_nc + $montototal_nd;
?>

<script type='text/javascript'>
	$(document).ready(function () {
		document.getElementById("cant_nc_tr").innerHTML = "<?= $cantidad_nc_tramite ?>";
		$('#porcen_decuento').prop('readonly', true);//por defecto el campo de porcentaje está desabilidado
		//efecto para la accion que necesioto en el checke true y false
	    $('#checkdescuento').on('click', function() {

					if (this.checked == true)
	        	{
              //saldo_pagar = parseFloat(document.getElementById('saldo_pagar').value) - parseFloat(data);
	        		$('#porcen_decuento').prop('readonly', false);
	        		document.getElementById('porcen_decuento').select();//permite que el input esté seleccionado
	        		//$('#monto_total_pagar').val(saldo_pagar);//necesito refrescar el dato para no perder el valor en el camino
	        	}
	        else {
						$.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/nc_tramite') ?>" , { 'nombreEmpresa' : '<?= $proveedor->nombreEmpresa ?>' } ,
						function( data ) {
                saldo_pagar = parseFloat(document.getElementById('saldo_pagar').value) - parseFloat(data);
		        	  $('#porcen_decuento').prop('readonly', true);
		            $('#monto_descuento').val('');
		            $('#porcen_decuento').val('');
		            $('#monto_total_pagar').val(saldo_pagar);//necesito refrescar el dato para no perder el valor en el camino
						});
						 }

	    });
	    $('#realizar_pago_ef').addClass('disabled');//mantenemos el boton de realizar pagos desabilitado
	    $('#realizar_pago_ch').addClass('disabled');//mantenemos el boton de realizar pagos desabilitado
	    $('#realizar_pago_tr').addClass('disabled');//mantenemos el boton de realizar pagos desabilitado
			$('#actualizar_pago_tr').addClass('disabled');//mantenemos el boton de realizar pagos desabilitado
	});

	//Agrega nota de credito para tramitey refresca tablas
    function agregar_nc_tramite() {
        var idProveedor = '<?= $idProveedor ?>';
				var monto_credito = '<?= $monto_credito ?>';
				var monto_debito = '<?= $monto_debito ?>';
        total_monto_documento = document.getElementById('total_monto_documento').value;
        monto_nc = document.getElementById("monto_nc_tr");
        detalle_nc = document.getElementById("concepto_nc_tr");
        monto_tramite_pagar = document.getElementById('monto_total_pagar').value;
        porcen_decuento = document.getElementById('porcen_decuento').value;
        var confirm_nc_create = confirm("¿Está seguro de crear una nota de crédito?");
        if (confirm_nc_create == true) {
        if (monto_nc.value=='' || detalle_nc.value=='') {
            monto_nc.style.border = "5px solid red";
            detalle_nc.style.border = "5px solid red";
        }
        else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/agrega_nota_credito_tramite') ?>",
                type:"post",
                dataType: 'json',
                data: { 'monto_nc' : monto_nc.value, 'detalle_nc': detalle_nc.value,
                'codProveedores' : idProveedor, 'monto_tramite_pagar' : monto_tramite_pagar,
                'total_monto_documento' : total_monto_documento, 'porcen_decuento' : porcen_decuento,
								'monto_credito' : monto_credito, 'monto_debito' : monto_debito },
                success: function(notif){

                    //$("#cant_nc_tr").load(location.href+' #cant_nc_tr');
                    document.getElementById("cant_nc_tr").innerHTML = notif.cantidad_nc_tramite;
                    $('#saldo_pagar').val(notif.suma_saldo_y_nc);
                    $('#monto_total_pagar').val(notif.suma_saldo_y_nc2);
                    $('#monto_descuento').val(notif.monto_descuento);

                    $('#concepto_nc_tr').val('');
                    $('#monto_nc_tr').val('');
                    document.getElementById("notif_nc_tramite").innerHTML = notif.bandera;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notif_nc_tramite").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notif_nc_tramite").fadeOut();
                    },3000);

                    //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nc_tramite') ?>" , { 'codProveedores' : idProveedor } ,
                    function( data ) {
                      $('#tbl_nc_tramite').html(data);
                    });

            		$('#modalnuevanotacred').modal('hide');
                },
                error: function(msg, status,err){
                 alert('Error linea 67');
                }
            });
        }
    } else {

    }
    }

    //actualiza una nota de credito
    function actualizar_nc_tramite() {
        var idProveedor = '<?= $idProveedor ?>';
				var monto_credito = '<?= $monto_credito ?>';
				var monto_debito = '<?= $monto_debito ?>';
        total_monto_documento = document.getElementById('total_monto_documento').value;
        monto_nc = document.getElementById("monto_nc_u_");
        detalle_nc = document.getElementById("detalle_nc_u_");
        idMov = document.getElementById("id_nc_u_").value;
        porcen_decuento = document.getElementById('porcen_decuento').value;
        //$('#activity-update-movimiento'+idMov).popover('hide');
        if (monto_nc.value=='' || detalle_nc.value=='') {
            monto_nc.style.border = "5px solid red";
            detalle_nc.style.border = "5px solid red";
        }
        else {
            $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/actualiza_nota_credito_tramite') ?>",
                    type:"post",
                    dataType: 'json',
                    data: { 'idMov' : idMov, 'monto_nc' : monto_nc.value, 'monto_credito' : monto_credito, 'monto_debito' : monto_debito,
                    'detalle_nc': detalle_nc.value, 'codProveedores' : idProveedor,
                    'total_monto_documento' : total_monto_documento, 'porcen_decuento' : porcen_decuento },
                    success: function(notif){
                    $('#saldo_pagar').val(notif.suma_saldo_y_nc);
                    $('#monto_total_pagar').val(notif.suma_saldo_y_nc2);
                    $('#monto_descuento').val(notif.monto_descuento);
                    document.getElementById("notif_nc_tramite").innerHTML = notif.bandera;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                            $("#notif_nc_tramite").fadeIn();
                        });
                        setTimeout(function() {//se oculta la alerta luego de 4 segundos
                            $("#notif_nc_tramite").fadeOut();
                        },3000);

                        //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
	                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nc_tramite') ?>" , { 'codProveedores' : idProveedor } ,
	                    function( data ) {
	                      $('#tbl_nc_tramite').html(data);
	                    });
                    },
                    error: function(msg, status,err){
                     alert('Error linea 105');
                    }
                });
        }
        //$('#btn_accion_nc').val('Actualizar Mov #'+idMov);
    }

    //elimina la nota de credito
    function eliminar_nc_tramite(idMov) {
        idProveedor = '<?= $idProveedor ?>';
				var monto_credito = '<?= $monto_credito ?>';
				var monto_debito = '<?= $monto_debito ?>';
        total_monto_documento = document.getElementById('total_monto_documento').value;
        porcen_decuento = document.getElementById('porcen_decuento').value;
        var confirm_nc_delete = confirm("¿Está seguro de eliminar esta nota de crédito?");
        if (confirm_nc_delete == true) {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/elimina_nota_credito_tramite') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idMov' : idMov, 'idProveedor' : idProveedor,
                'total_monto_documento' : total_monto_documento, 'porcen_decuento' : porcen_decuento,
							 	'monto_credito' : monto_credito, 'monto_debito' : monto_debito },
                success: function(notif){
                $('#saldo_pagar').val(notif.suma_saldo_y_nc);
                $('#monto_total_pagar').val(notif.suma_saldo_y_nc2);
                $('#monto_descuento').val(notif.monto_descuento);
                document.getElementById("cant_nc_tr").innerHTML = notif.cantidad_nc_tramite;
                document.getElementById("notif_nc_tramite").innerHTML = notif.bandera;
                setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notif_nc_tramite").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notif_nc_tramite").fadeOut();
                    },3000);

                	//obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nc_tramite') ?>" , { 'codProveedores' : idProveedor } ,
                    function( data ) {
                      $('#tbl_nc_tramite').html(data);
                    });
                },
                error: function(msg, status,err){
                 alert('Error linea 137');
                }
            });
        } else { }
    }

	//obtiene el monto del descuento y monto total a pagar con formato decimal
	function accion_descuento(porcen_decuento) {
		total_monto_documento = document.getElementById('total_monto_documento').value;
		var idProveedor = '<?= $idProveedor ?>';
		var monto_credito = '<?= $monto_credito ?>';
		var monto_debito = '<?= $monto_debito ?>';
        if (porcen_decuento=='') {
            porcen_decuento = 0;
        }

		var descuento = porcen_decuento*0.01;//el porcentage de descuento lo pasamos a decimales
		var subtotal = (total_monto_documento - monto_credito) / '<?= Yii::$app->params["reverso_impuesto"] ?>';//le quitamos el impuesto
		var monto_descuento = subtotal * descuento;//sacamos el monto de descuento
		var monto_pagar_con_iv = (total_monto_documento - monto_credito) - monto_descuento; //al total mas impuestos le restamos el descuento
		$.ajax({
			url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/montopagar') ?>",
                type:"post",
                dataType: 'json',
                data: { 'monto_descuento' : monto_descuento,
								'monto_pagar' : monto_pagar_con_iv,
								'monto_credito' : monto_credito,
								'monto_debito' : monto_debito,
								'idProveedor' : idProveedor },
                success: function(data){
                	$('#monto_descuento').val(data.monto_descuento);
					$('#monto_total_pagar').val(data.monto_total_pagar);
                }
		});

	}

	//para abilitar el boton cuando sean las condiciones dadas respectivas al orden con el que se tienen que llenar el formulario
	function habilitarbtn(pago) {
		//este es activado cuando se llena el form de Efectivo
		if (pago == 'efectivo') {
			numero_recibo_cancelacion_ef = document.getElementById("numero_recibo_cancelacion_ef").value;
			idContacto_pago_ef = document.getElementById("idContacto_pago_ef").value;
			nombre_agente_ef = document.getElementById("nombre_agente_ef").value;
			if (numero_recibo_cancelacion_ef=='' || idContacto_pago_ef=='' || nombre_agente_ef=='') {
				$('#realizar_pago_ef').addClass('disabled');
			} else {
				$('#realizar_pago_ef').removeClass('disabled');
			}
		}//fin por tipo efectivo

		//este es activado cuando se llena el form de cheque
		else if (pago == 'cheque') {
			numero_cheque = document.getElementById("numero_cheque").value;
			id_cuenta_bancaria_lo_ch = document.getElementById("id_cuenta_bancaria_lo_ch").value;
			fecha_documento_cheque = document.getElementById("fecha_documento_cheque").value;
			numero_recibo_cancelacion_ch = document.getElementById("numero_recibo_cancelacion_ch").value;
			idContacto_pago_ch = document.getElementById("idContacto_pago_ch").value;
			nombre_agente_ch = document.getElementById("nombre_agente_ch").value;
			if ( numero_cheque=='' || id_cuenta_bancaria_lo_ch=='' || fecha_documento_cheque=='' || numero_recibo_cancelacion_ch=='' || idContacto_pago_ch=='' || nombre_agente_ch=='') {
			    $('#realizar_pago_ch').addClass('disabled');
	            $('#noti_pagos_ch').html('');
			} else {
	            //comprobamos con ajax si el numero de cheque ya existe para ese banco
				$.ajax({
	                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/valida_numero_cheque') ?>",
	                    type:"post",
	                    data: { 'numero_cheque' : numero_cheque, 'id_cuenta_bancaria_lo_ch' : id_cuenta_bancaria_lo_ch },
	                    success: function(data){
	                        if (data=='no') {
	                            $('#noti_pagos_ch').html('<center><font size=4 color="red">Atención, el número de cheque ya fue registrado con este banco</font></center>');
	                            $('#realizar_pago_ch').addClass('disabled');
	                        }
	                        else {
	                            $('#noti_pagos_ch').html('<center><font size=4 color="green">Número de cheque aceptado</font></center>');
	                            $('#realizar_pago_ch').removeClass('disabled');
	                        }
	                    },
	                    error: function(msg, status,err){
	                     $('#noti_pagos_ch').html('Error');
	                    }
	            });
			}
		}//fin por tipo de cheque

		//este es activado cuando se llena el form de transferencia
		else if (pago == 'transferencia') {
			id_cuenta_bancaria_lo_tb = document.getElementById("id_cuenta_bancaria_lo_tb").value;
			id_cuenta_bancaria_pr = document.getElementById("id_cuenta_bancaria_pr").value;
			idContacto_pago_tb = document.getElementById("idContacto_pago_tb").value;
	    //numero_comprovante = document.getElementById("numero_comprovante").value;
			if (id_cuenta_bancaria_lo_tb=='' || id_cuenta_bancaria_pr=='' || idContacto_pago_tb==''/* || numero_comprovante == ''*/) {
			    $('#realizar_pago_tr').addClass('disabled');
	            $('#noti_pagos_tr').html('');
			} /*else {
				//comprovamos con ajax si el numero de comprovante ya existe para ese banco
	            $.ajax({
	                url:"<?php //echo Yii::$app->getUrlManager()->createUrl('tramite-pago/valida_numero_comprovante_trans') ?>",
	                    type:"post",
	                    data: { 'numero_comprovante' : numero_comprovante, 'id_cuenta_bancaria_lo_tb' : id_cuenta_bancaria_lo_tb },
	                    success: function(data){
	                        if (data=='no') {
	                            $('#noti_pagos_tr').html('<center><font size=4 color="red">Atención, el comprobante ya fue registrado con este banco</font></center>');
	                            $('#realizar_pago_tr').addClass('disabled');
	                        }
	                        else {
	                            $('#noti_pagos_tr').html('<center><font size=4 color="green">Comprobante aceptado</font></center>');
	                            $('#realizar_pago_tr').removeClass('disabled');
	                        }
	                    },
	                    error: function(msg, status,err){
	                     $('#noti_pagos_tr').html('Error');
	                    }
	            });
			}*/ else {
				$('#realizar_pago_tr').removeClass('disabled');
			}
//----------------------------------------------------------------------------------------------------------------------------
			//este es cuando se llena el form de trasferencia pero para actualizacion de tramite actualizar_pago_tr
			tramiteupdate = '<?= DebitoSession::getTramiteupdate() ?>';

			if (tramiteupdate != '') {
				detalle_reverso = document.getElementById("detalle_reverso").value;
				if (id_cuenta_bancaria_lo_tb=='' || id_cuenta_bancaria_pr=='' || idContacto_pago_tb=='' /*|| numero_comprovante == '' */|| detalle_reverso == '') {
				    $('#actualizar_pago_tr').addClass('disabled');
		          //  $('#noti_pagos_tr').html('');
				}/* else {
					$('#actualizar_pago_tr').removeClass('disabled');
					//comprovamos con ajax si el numero de comprovante ya existe para ese banco
		            $.ajax({
		                url:"<?php //echo Yii::$app->getUrlManager()->createUrl('tramite-pago/valida_numero_comprovante_trans') ?>",
		                    type:"post",
		                    data: { 'numero_comprovante' : numero_comprovante, 'id_cuenta_bancaria_lo_tb' : id_cuenta_bancaria_lo_tb },
		                    success: function(data){
		                        if (data=='no') {
		                            $('#noti_pagos_tr_up').html('<center><font size=4 color="red">Atención, el comprobante ya fue registrado con este banco</font></center>');
		                            $('#actualizar_pago_tr').addClass('disabled');
		                        }
		                        else {
		                            $('#noti_pagos_tr_up').html('<center><font size=4 color="green">Comprobante aceptado</font></center>');
		                            $('#actualizar_pago_tr').removeClass('disabled');
		                        }
		                    },
		                    error: function(msg, status,err){
		                     $('#noti_pagos_tr').html('Error');
		                    }
		            });
				}*/else {
					$('#actualizar_pago_tr').removeClass('disabled');
				}
			}//fin del tipo tramite pero en poroceso de actualizacion de uno pensiente

		}//fin por tipo de transferencia

	}

	//para mostrar el correspondiente tipo de pago
	function mostrar_medio_pago(tipo_medio) {
		var detalle = document.getElementById("detalle_pago").value;
    	var prioridad = $('input:radio[name=prioridad]:checked').val();
			if (tipo_medio=='efectivo') {
				//$('#realizar_pago_ef').removeClass('disabled');
				$('#tipo_pago').val('efectivo');
			} else if (tipo_medio=='cheque') {
				//$('#realizar_pago_ch').removeClass('disabled');
				$('#tipo_pago').val('cheque');
			} else if (tipo_medio=='transferencia') {
				//$('#realizar_pago_tr').removeClass('disabled');
				$('#tipo_pago').val('transferencia');
			}

	}
	function actualizar_pago(this_boton) {
		idProveedor = '<?= $idProveedor ?>';
		cantidad_monto_documento = document.getElementById('total_monto_documento').value;
		monto_saldo_pago = '<?= $suma_saldo_proceso ?>';//document.getElementById('saldo_pagar').value;
		porcentaje_descuento = document.getElementById('porcen_decuento').value;
		monto_tramite_pagar = document.getElementById('monto_total_pagar').value;
		detalle = document.getElementById('detalle_pago').value;
		prioridad = $('input:radio[name=prioridad]:checked').val();
		tipo_pago = document.getElementById('tipo_pago').value;
		var confirm_pago = confirm("¡Confirme que ya actualizó el pago!");
		if (confirm_pago == true) {
			var $btn = $(this_boton).button('loading');
            // simulating a timeout
            setTimeout(function () {
                $btn.button('reset');
            }, 60000);
						id_cuenta_bancaria_lo_tb = document.getElementById('id_cuenta_bancaria_lo_tb').value;
						id_cuenta_bancaria_pr = document.getElementById('id_cuenta_bancaria_pr').value;
						idContacto_pago_tb = document.getElementById('idContacto_pago_tb').value;
						//numero_comprovante = document.getElementById('numero_comprovante').value;
						detalle_reverso = document.getElementById('detalle_reverso').value;
						$.ajax({
						url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/actualizar_pago') ?>",
											type:"post",
											data: { 'cantidad_monto_documento' : cantidad_monto_documento, 'monto_saldo_pago' : monto_saldo_pago,
													'porcentaje_descuento' : porcentaje_descuento, 'monto_tramite_pagar' : monto_tramite_pagar,
													'tipo_pago' : tipo_pago, 'prioridad' : prioridad, 'idProveedor' : idProveedor,
													'id_cuenta_bancaria_lo_tb' : id_cuenta_bancaria_lo_tb, /*'numero_comprovante' : numero_comprovante,*/
													'id_cuenta_bancaria_pr' : id_cuenta_bancaria_pr, 'idContacto_pago_tb' : idContacto_pago_tb,
													'detalle': detalle, 'detalle_reverso' : detalle_reverso },
											success: function(data){
											},
											error: function(msg, status,err){
											}
						});
		}
	}
	//completar pago cuando se seleccione le boton de generar trmite de pago
	function completar_pago(this_boton) {

			idProveedor = '<?= $idProveedor ?>';
			cantidad_monto_documento = document.getElementById('total_monto_documento').value;
			monto_saldo_pago = document.getElementById('saldo_pagar').value;//'<?= $suma_saldo_proceso ?>';
			porcentaje_descuento = document.getElementById('porcen_decuento').value;
			monto_tramite_pagar = document.getElementById('monto_total_pagar').value;
			detalle = document.getElementById('detalle_pago').value;
			prioridad = $('input:radio[name=prioridad]:checked').val();
			tipo_pago = document.getElementById('tipo_pago').value;
			var confirm_pago = confirm("¿Está seguro de generar este pago?");
		if (confirm_pago == true) {
			//pone el estado del boton en espera para que no se buelva a precionar otra ver y genere conflicto
			var $btn = $(this_boton).button('loading');
            // simulating a timeout
            setTimeout(function () {
                $btn.button('reset');
            }, 60000);
	        //hacemos las condiciones para saber cual es el tipo de pago que se está usando
			if (tipo_pago=='efectivo') {
				numero_recibo_cancelacion_ef = document.getElementById('numero_recibo_cancelacion_ef').value;
				idContacto_pago_ef = document.getElementById('idContacto_pago_ef').value;
				nombre_agente_ef = document.getElementById('nombre_agente_ef').value;
				$.ajax({
				url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/completar_pago') ?>",
	                type:"post",
	                data: { 'cantidad_monto_documento' : cantidad_monto_documento, 'monto_saldo_pago' : monto_saldo_pago,
	                		'porcentaje_descuento' : porcentaje_descuento, 'monto_tramite_pagar' : monto_tramite_pagar,
	                		'tipo_pago' : tipo_pago, 'prioridad' : prioridad,
	                		'idProveedor' : idProveedor, 'numero_recibo_cancelacion_ef' : numero_recibo_cancelacion_ef,
	                		'idContacto_pago_ef' : idContacto_pago_ef, 'nombre_agente_ef' : nombre_agente_ef, 'detalle': detalle },
	                success: function(data){
	                },
	                error: function(msg, status,err){
	                 //alert('No pasa 36');
	                }
				});
			} else if (tipo_pago=='cheque') {
				numero_cheque = document.getElementById('numero_cheque').value;
				id_cuenta_bancaria_lo_ch = document.getElementById('id_cuenta_bancaria_lo_ch').value;
				fecha_documento_cheque = document.getElementById('fecha_documento_cheque').value;
				numero_recibo_cancelacion_ch = document.getElementById('numero_recibo_cancelacion_ch').value;
				idContacto_pago_ch = document.getElementById('idContacto_pago_ch').value;
				nombre_agente_ch = document.getElementById('nombre_agente_ch').value;
				$.ajax({
				url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/completar_pago') ?>",
	                type:"post",
	                data: { 'cantidad_monto_documento' : cantidad_monto_documento, 'monto_saldo_pago' : monto_saldo_pago,
	                		'porcentaje_descuento' : porcentaje_descuento, 'monto_tramite_pagar' : monto_tramite_pagar,
	                		'tipo_pago' : tipo_pago, 'prioridad' : prioridad, 'idProveedor' : idProveedor,
			                'numero_cheque' : numero_cheque, 'id_cuenta_bancaria_lo_ch' : id_cuenta_bancaria_lo_ch,
			                'fecha_documento_cheque' : fecha_documento_cheque, 'numero_recibo_cancelacion_ch' : numero_recibo_cancelacion_ch,
			                'idContacto_pago_ch' : idContacto_pago_ch, 'nombre_agente_ch' : nombre_agente_ch, 'detalle': detalle },
	                success: function(data){
	                },
	                error: function(msg, status,err){
	                 //alert('No pasa 36');
	                }
				});
			} else if (tipo_pago=='transferencia') {

				id_cuenta_bancaria_lo_tb = document.getElementById('id_cuenta_bancaria_lo_tb').value;
				id_cuenta_bancaria_pr = document.getElementById('id_cuenta_bancaria_pr').value;
				idContacto_pago_tb = document.getElementById('idContacto_pago_tb').value;
        //numero_comprovante = document.getElementById('numero_comprovante').value;
				$.ajax({
				url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/completar_pago') ?>",
	                type:"post",
	                data: { 'cantidad_monto_documento' : cantidad_monto_documento, 'monto_saldo_pago' : monto_saldo_pago,
	                		'porcentaje_descuento' : porcentaje_descuento, 'monto_tramite_pagar' : monto_tramite_pagar,
	                		'tipo_pago' : tipo_pago, 'prioridad' : prioridad, 'idProveedor' : idProveedor,
			                'id_cuenta_bancaria_lo_tb' : id_cuenta_bancaria_lo_tb,/* 'numero_comprovante' : numero_comprovante,*/
			                'id_cuenta_bancaria_pr' : id_cuenta_bancaria_pr, 'idContacto_pago_tb' : idContacto_pago_tb,
			                'detalle': detalle },
	                success: function(data){
	                },
	                error: function(msg, status,err){
	                }
				});
			}
		} else { }
	}

	//me muestra y oculta el div que contiene las notas de credito del tramite
	var visual_nc = null;
	function ver_nc_tramite() {
		idProveedor = '<?= $idProveedor ?>';
		obj = document.getElementById('contenido_nota_monto');
		obj.style.display = (obj==visual_nc) ? 'none' : 'block';
		if (visual_nc != null)
		visual_nc.style.display = 'none';
		visual_nc = (obj==visual_nc) ? null : obj;

		//obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nc_tramite') ?>" , { 'codProveedores' : idProveedor } ,
        function( data ) {
          $('#tbl_nc_tramite').html(data);
        });
	}

	//espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }
</script>
<style type="text/css">
	.oculto_nota_monto {
		display:none;
	}
</style>
<div class="panel panel-info">
    <div class="panel-heading">
        <font face="arial" size=4>Generar pago</font>
				<?php
				$flecha = \Yii::$app->request->BaseUrl.'/img/flecha.gif';
				@$idprob_tramite = DebitoSession::getIDproveedor_tramite_upd();
				$idprob_local = DebitoSession::getIDproveedor();
				$bandera_generar_pago_tramite_gargado = 'inactiva';
				//con esto compruebo si es un tramite reversado para actualizar
				if(@$idprob_tramite == $idprob_local)
				 if (@$idTramite_pago = DebitoSession::getTramiteupdate()) {
					 		$bandera_generar_pago_tramite_gargado = 'activa';
					 		$tramite = TramitePago::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
							$mediopago = MedioPagoProveedor::find()->where(['idTramite_pago'=>$idTramite_pago])->one();
							$porc_desto = null;
							$monto_desto = null;
							if (@$tramite->porcentaje_descuento) {
								$_descuento = $tramite->porcentaje_descuento * 0.01;//obtendo el descuento del %
						    $_monto_descuento = /*$tramite->monto_saldo_pago*/$suma_saldo_y_nc * $_descuento; //multiplico el %descuento por el monto del saldo
								$porc_desto = $tramite->porcentaje_descuento;//me alimenta el porcentaje de descuento
								$monto_desto = $_monto_descuento; //me alimenta el monto del descuento
							}
							$prioridad_pago = '';
							if ($tramite->prioridad_pago == 'Alta') { $prioridad_pago = 'alta'; } else if ($tramite->prioridad_pago == 'Media') {
								$prioridad_pago = 'media'; } else { $prioridad_pago = 'baja'; }
							 $monto_total_pagar = $suma_saldo_y_nc - $monto_desto;
							 echo '<span style="float:right"><font face="arial" size=4><img src="'.$flecha.'" style="height: 32px;"> Trámite <strong>'.$idTramite_pago.'</strong> cargado</font></span>';
							 echo '
	                 <script type="text/javascript">
	                    // $(window).load(function(){
											$(document).ready(function () {
												   $("#tipo_pago").val("transferencia");
													 $("#detalle_pago").val("'.$tramite->detalle.'");
													 $("#porcen_decuento").val("'.$porc_desto.'");
													 $("#monto_descuento").val("'.$monto_desto.'");
													 $("#monto_total_pagar").val("'.$monto_total_pagar.'");
													 $("#'.$prioridad_pago.'").attr("isChecked","true");
            	 						 $("#'.$prioridad_pago.'").attr("checked",true);
													 $("#numero_comprovante").val("'.$mediopago->numero_recibo_cancelacion.'");
													 $("#id_cuenta_bancaria_lo_tb").val("'.$mediopago->cuenta_bancaria_local.'").change();
													 $("#id_cuenta_bancaria_pr").val("'.$mediopago->cuenta_bancaria_proveedor.'").change();
													 $("#idContacto_pago_tb").val("'.$mediopago->idContacto_pago.'").change();
													 /*oculto botones de medio pago*/
													 var efect_a = document.getElementById("efect_a");
													 var chequ_a = document.getElementById("chequ_a");
													 $(efect_a).addClass("disabled");
													 $(chequ_a).addClass("disabled");

	                         $("#transferencia").addClass("active"); /*activo la pestaña tav*/
													 $("#transf_a").tab("show"); /*muestro lo que contiene la pestaña*/
	                     });
	                 </script>';
				 } ?>
    </div>
    <div class="panel-body">

	        <div style="height: 100%;width: 100%; overflow-y: auto; ">
		        <?php
		        	/*$total_fact_proceso = Yii::$app->db->createCommand("SELECT sum(total) FROM tbl_compras WHERE estado_pago = 'Proceso' AND idProveedor = " . $idProveedor);
		        	$suma_total_proceso = floatval($total_fact_proceso->queryScalar());*/


					echo '<h4>Cantidad de documentos:<span style="float:right">'.$suma_facturas.'</span></h4>';
		        	echo '<h4>Total monto documentos:<span style="float:right"> '.number_format($suma_total_proceso,2).'</span></h4>';
		        	echo '<h4>Monto Créditos (-):<span style="float:right">'.number_format($monto_credito,2).'</span></h4>';
		        	echo '<h4>Monto Débitos (+):<span style="float:right">'.number_format($monto_debito,2).'</span></h4>';
		        	echo '<h4>Saldo a pagar:<span style="float:right">'.number_format($suma_saldo_proceso,2).'</span></h4>';

		        	echo '<div class="alert alert-info">
		        			<center><a style="cursor:pointer" onclick="ver_nc_tramite()">Notas de créditos al monto del trámite <span class="badge"> <div id="cant_nc_tr"></div> </span></a></center>
		        			<div id="contenido_nota_monto" class="oculto_nota_monto">
			        			<div style="height: 280px;width: 100%; overflow-y: auto; ">
			        				<br>
			        				<div class="pull-right">' .Html::a('', '#', ['class' => 'fa fa-folder-o fa-2x',
			        					'data-toggle' => 'modal', 'data-target' => '#modalnuevanotacred',
			        					'data-placement' => 'left', 'title' => Yii::t('app', 'Crear nueva Nota de Crédito al trámite')]) .
			        				'</div>';
			        ?>
				        			<div id="tbl_nc_tramite">
					                <!-- con este div puedo refrescar tbl_nc al igual que llamarlo y montarlo en tiempo real-->

					                </div>
					                <div id="notif_nc_tramite"></div>
			        <?php
			        echo 	   '</div>
		        			</div>
		        		  </div>';
		        	echo '
		        	<div class="col-lg-7">
		        	<input type="hidden" name="total_monto_documento" id="total_monto_documento" value="'.floatval($suma_total_proceso).'">
		        	<input type="hidden" name="saldo_pagar" id="saldo_pagar" value="'.floatval($suma_saldo_proceso + $montototal_nd).'">
		        	<div class="input-group">
					  <span class="input-group-addon"><input type="checkbox" id="checkdescuento" /> Desto </span>
					  <input type="text" class="form-control" id="porcen_decuento" onkeyup="accion_descuento(this.value)" onkeypress="if(this.value.length == 2){return false;}else{return toUpper(event,this);}">
					  <span class="input-group-addon">%</span>
					</div>
					</div>
					<div class="col-lg-5">
					<input type="text" id="monto_descuento" class="form-control" readonly>
					</div>';
					//Monto total a pagar
					echo '
					<div class="col-lg-12"><br>
		        	<div class="input-group">
					  <span class="input-group-addon"> Monto total a pagar </span>
					  <input type="text" class="form-control" id="monto_total_pagar" value="'.number_format($suma_saldo_y_nc,2).'" readonly>
					</div>
					</div>';
					echo '
					<div class="col-lg-12"><br>Detalle:<br>
              <textarea id="detalle_pago" rows="2" class="form-control" onkeypress="if(this.value.length == 120){return false;}else{return toUpper(event,this);}"></textarea>
          </div>';
					if (@$idTramite_pago = DebitoSession::getTramiteupdate()) {
						$tra = "'transferencia'";
						echo '<div class="col-lg-12"><br>Motivo por el que se reversó el trámite:<br>
                    <textarea id="detalle_reverso" rows="2" class="form-control" onkeyup="habilitarbtn('.$tra.')" onkeypress="if(this.value.length == 120){return false;}else{return toUpper(event,this);}"></textarea>
                  </div>';
					}
	        echo '<div class="col-lg-12"><br>Prioridad:<br>
	                <label class="radio-inline"><input type="radio" name="prioridad" id="baja" value="Baja">Baja</label>
	                <label class="radio-inline"><input type="radio" name="prioridad" id="media" value="Media">Media</label>
									<label class="radio-inline"><input type="radio" name="prioridad" id="alta" value="Alta">Alta</label>
	                <br><br>
	                </div>
	                ';

	                //echo '<div class="col-lg-12">Cuenta bancaria <strong>PROVEEDOR:</strong></div>';

		        	//echo '<br><br><h4>Generado por:<span style="float:right">'.$usuario.'</span></h4>';
		        ?>
			    <div class="btn-group btn-group-justified" role="group" aria-label="...">
					  <div class="btn-group" role="group">
					    <a href="#efectivo" data-toggle="tab" class="btn btn-default" id="efect_a" onclick="mostrar_medio_pago('efectivo')">Efectivo</a>
					  </div>
					  <div class="btn-group" role="group">
					    <a href="#cheque" data-toggle="tab" class="btn btn-default" id="chequ_a" onclick="mostrar_medio_pago('cheque')">Cheque</a>
					  </div>
					  <div class="btn-group" role="group">
					    <a href="#transferencia" data-toggle="tab" class="btn btn-default" id="transf_a" onclick="mostrar_medio_pago('transferencia')">Transferencia</a>
					  </div>
					</div>
				<div class="table-responsive tab-content">
					<!---______________________________EFECTIVO____________________________________________________________-->
					<div class="tab-pane fade" id="efectivo">
						<h3>Efectivo</h3>
						Número de recibo de cancelación del proveedor<br>
						<input type="text" class="form-control" name="numero_recibo_cancelacion" id="numero_recibo_cancelacion_ef" onkeypress="return isNumberDe(event)" onkeyup="habilitarbtn('efectivo')"><br>
						Contacto pago<br>
						<?= Select2::widget([
		                    'name' => '',
		                    //'value' => '',
		                    'data' => ArrayHelper::map(ContactosPagos::find()->where(['=','codProveedores', $idProveedor])->all(), 'idContacto_pago',
		                        function($element) {
		                        return $element['nombre'].' - '.$element['email'];
		                    }),
		                    'options' => [
		                        'id'=>'idContacto_pago_ef',
		                        'placeholder'=>'- Seleccione nombre de agente -',
		                        'onchange'=>'javascript:habilitarbtn("efectivo")',
		                        'multiple' => false //esto me ayuda a que solo se obtenga uno
		                    ],
		                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
		                ]) ?>
		                <br>
		                Nombre del agente<br>
						<input type="text" class="form-control" name="nombre_agente_ef" id="nombre_agente_ef" onkeyup="habilitarbtn('efectivo')">
		                <br>
		                <?php if($suma_facturas > 0) { ?>
		                <a class="btn btn-success btn-lg btn-block" id="realizar_pago_ef" onclick="completar_pago(this)" data-loading-text="Espere, trámite en proceso..."><span class="fa fa-money" aria-hidden="true"></span> Realizar Pago</a>
						<?php } ?>
					</div>
					<!---______________________________CHEQUE____________________________________________________________-->
					<div class="tab-pane fade" id="cheque">
						<h3>Cheque</h3>
						Número de Cheque<br>
						<input type="text" class="form-control" name="numero_cheque" id="numero_cheque" onkeypress="return isNumberDe(event)" onkeyup="habilitarbtn('cheque')"><br>
						Cuenta bancaria local<br>
						<?= Select2::widget([
		                    'name' => '',
		                    'value' => '',
		                    'data' => ArrayHelper::map(CuentasBancarias::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->all(), 'idBancos',
		                        function($element) {
		                        return $element['numero_cuenta'].' - '.$element['descripcion'];
		                    }),
		                    'options' => [
		                        'id'=>'id_cuenta_bancaria_lo_ch',
		                        'placeholder'=>'- Seleccione una cuenta local -',
		                        'onchange'=>'javascript:habilitarbtn("cheque")',
		                        //'multiple' => false //esto me ayuda a que solo se obtenga uno
		                    ],
		                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
		                ]) ?><br>
		                Fecha de documento<br>
		                <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
		                	'value'=> date('d-m-Y'),
		                'options' => ['class'=>'form-control', 'placeholder'=>'dd-mm-yyyy', 'onchange'=>'javascript:habilitarbtn("cheque")', 'id'=>'fecha_documento_cheque']]) ?><br>
		                Número de recibo de cancelación del proveedor<br>
		                <input type="text" class="form-control" name="numero_recibo_cancelacion" id="numero_recibo_cancelacion_ch" onkeypress="return isNumberDe(event)" onkeyup="habilitarbtn('cheque')"><br>
						Contacto de pago<br>
						<?= Select2::widget([
		                    'name' => '',
		                    'value' => '',
		                    'data' => ArrayHelper::map(ContactosPagos::find()->where(['=','CodProveedores', $idProveedor])->all(), 'idContacto_pago',
		                        function($element) {
		                        return $element['nombre'].' - '.$element['email'];
		                    }),
		                    'options' => [
		                        'id'=>'idContacto_pago_ch',
		                        'placeholder'=>'- Seleccione nombre de agente -',
		                        'onchange'=>'javascript:habilitarbtn("cheque")',
		                        //'multiple' => false //esto me ayuda a que solo se obtenga uno
		                    ],
		                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
		                ]) ?>
		                <br>
		                Nombre del agente<br>
						<input type="text" class="form-control" name="nombre_agente_ch" id="nombre_agente_ch" onkeyup="habilitarbtn('cheque')">
		                <br><?php if($suma_facturas > 0) { ?>
                        <div id="noti_pagos_ch"></div>
		                <a class="btn btn-success btn-lg btn-block" id="realizar_pago_ch" onclick="completar_pago(this)" data-loading-text="Espere, trámite en proceso..."><span class="fa fa-money" aria-hidden="true"></span> Realizar Pago</a>
						<?php } ?>
					</div>
					<!---______________________________TRANSFERENCIA ELECTRONICA_________________________________________-->
					<div class="tab-pane fade" id="transferencia">
						<h3>Transferencia Bancaria</h3>
                        <!--br>Número de comprobante<br>
                        <input type="text" class="form-control" name="numero_comprovante" id="numero_comprovante" onkeypress="return isNumberDe(event)" onkeyup="habilitarbtn('transferencia')"><br-->
						Cuenta bancaria local<br>
						<?= Select2::widget([
		                    'name' => '',
		                    'value' => '',
		                    'data' => ArrayHelper::map(CuentasBancarias::find()->where(['idTipo_moneda'=>$proveedor->idMoneda])->all(), 'idBancos',
		                        function($element) {
		                        return $element['numero_cuenta'].' - '.$element['descripcion'];
		                    }),
		                    'options' => [
		                        'id'=>'id_cuenta_bancaria_lo_tb',
		                        'placeholder'=>'- Seleccione una cuenta local -',
		                        'onchange'=>'javascript:habilitarbtn("transferencia")',
		                        //'multiple' => false //esto me ayuda a que solo se obtenga uno
		                    ],
		                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
		                ]) ?><br>
						Cuenta bancaria <strong>PROVEEDOR:</strong><br>
						<?= Select2::widget([
		                    'name' => '',
		                    'value' => '',
		                    'data' => ArrayHelper::map(CuentasBancariasProveedor::find()->where(['=','CodProveedores', $idProveedor])->all(), 'id',
		                        function($element) {
		                        return $element['cuenta_bancaria'].' - '.$element['descripcion'];
		                    }),
		                    'options' => [
		                        'id'=>'id_cuenta_bancaria_pr',
		                        'placeholder'=>'- Seleccione una cuenta del proveedor -',
		                        'onchange'=>'javascript:habilitarbtn("transferencia")',
		                        //'multiple' => false //esto me ayuda a que solo se obtenga uno
		                    ],
		                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
		                ]) ?>
		                <br>
		                Contacto de pago<br>
						<?= Select2::widget([
		                    'name' => '',
		                    'value' => '',
		                    'data' => ArrayHelper::map(ContactosPagos::find()->where(['=','CodProveedores', $idProveedor])->all(), 'idContacto_pago',
		                        function($element) {
		                        return $element['nombre'].' - '.$element['email'];
		                    }),
		                    'options' => [
		                        'id'=>'idContacto_pago_tb',
		                        'placeholder'=>'- Seleccione nombre de agente -',
		                        'onchange'=>'javascript:habilitarbtn("transferencia")',
		                        //'multiple' => false //esto me ayuda a que solo se obtenga uno
		                    ],
		                    'pluginOptions' => ['initialize'=> true,'allowClear' => true]
		                ]) ?>
		                <br>
		                <?php if($suma_facturas > 0) {
														if($bandera_generar_pago_tramite_gargado=='inactiva'){
											?>
                        <div id="noti_pagos_tr"></div>
		                		<a class="btn btn-success btn-lg btn-block" id="realizar_pago_tr" onclick="completar_pago(this)" data-loading-text="Espere, trámite en proceso..."><span class="fa fa-money" aria-hidden="true"></span> Realizar Pago</a>
						        <?php } else {
											 echo '<div id="noti_pagos_tr_up"></div>';
												echo '<a class="btn btn-primary btn-lg btn-block" id="actualizar_pago_tr" onclick="actualizar_pago(this)" data-loading-text="Espere, trámite en proceso..."><span class="fa fa-money" aria-hidden="true"></span> Actualizar Pago</a>';
													}
												}//fin if $suma_facturas > 0
                    ?>
					</div>
					<input type="hidden" name="tipo_pago" id="tipo_pago" ><!--me almacenará el tipo de pago a escojer el usuario-->

				</div>
	        </div>
    </div>
    <?php
	//Muestra una modal para crear nueva nota de credito
         Modal::begin([
                'id' => 'modalnuevanotacred',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Nueva Nota de Crédito</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar', null, [
                    'class'=>'btn btn-success',
                    'onclick'=>'agregar_nc_tramite()',
                    //'target'=>'_blank',
                    //'data-toggle'=>'tooltip',
                    //'data-dismiss'=>'modal',
                    'title'=>'Agrega una nueva nota pendiente'
                ]),
            ]);

            echo '<div class="well">';

            echo '<label>Concepto:</label><br>';
            echo '<textarea class="form-control" rows="5" id="concepto_nc_tr" required></textarea>';
            echo '<br><label>Monto: </label>';
            echo ' '.Html::input('text', '', '', ['size' => '8', 'class' => 'form-control', 'id' => 'monto_nc_tr', 'placeholder' =>' Monto', 'readonly' => false, 'onchange' => '', "onkeypress"=>"return isNumberDe(event)"]);
            echo '</div>';

            Modal::end();
    ?>
    <?php
    //muestra modal para actualizar nota de credito pendiente
        Modal::begin([
                'id' => 'modalActualizacredpen',
                'size'=>'modal-sm',
                'header' => '<center><h4 class="modal-title">Detalle de Entrada</h4></center>',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>'.Html::a('<i class="glyphicon glyphicon-plus"></i> Actualizar', '', [
                    'class'=>'btn btn-success',
                    'onclick'=>'actualizar_nota_credito()',
                    //'target'=>'_blank',
                    //'data-toggle'=>'tooltip',
                    'data-dismiss'=>'modal',
                    'title'=>'Actualizar nota de credito'
                ]),
            ]);
        echo '<div class="well">';
        echo '<div id="modal_update_notasc_pen"></div>';
        echo '</div>';

        Modal::end();
    ?>
</div>
