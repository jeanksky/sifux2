<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\DebitoSession;
use kartik\widgets\AlertBlock;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\Proveedores;
use backend\models\Moneda;
use backend\models\ComprasInventario;

$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TramitePagoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trámite de pagos a proveedores';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => 'Cancelar trámites de pagos pendientes', 'url' => ['tramite-pago-confirmar/index']];
$this->params['breadcrumbs'][] = ['label' => 'Confirmar cancelación de trámites de pagos', 'url' => ['tramite-pago-confirmar/confirmar']];

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });

//Carga un proveedor
function agrega_proveedor(idProveedor){
    $('#panel').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
    $('#panel2').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
    $('#panel3').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
    $('#panel4').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/addproveedor') ?>",
        type:"post",
        data: { 'codProveedor': idProveedor },
        success: function(data){
        },
        error: function(msg, status,err){
        }
    });

}
//me permite buscar por like los proveedores a BD
    function buscar() {
        var textoBusqueda = $("input#busqueda").val();
        if (textoBusqueda != "") {

            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/busqueda_proveedor') ?>",
                type:"post",
                data: { 'valorBusqueda' : textoBusqueda },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 alert('No pasa 47');
                }
            });
        } else {
            ("#resultadoBusqueda").html('');
        }
    }

    //buscar tramite
    function buscar_tramite() {
        busqueda_prio = document.getElementById('busqueda_prio').value;
        busqueda_est = document.getElementById('busqueda_est').value;
        idProveedor = document.getElementById('id_proveedor').value;
        fecha_reg_tramite = document.getElementById('fecha_reg_tramite').value;
        factura = document.getElementById('factura').value;
        document.getElementById("tbl_listatramite").innerHTML="";//tengo que destruir el contenido del div de
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/listatramite') ?>" ,
            { 'idProveedor' : idProveedor, 'busqueda_prio' : busqueda_prio, 'busqueda_est' : busqueda_est,
            'fecha_reg_tramite' : fecha_reg_tramite, 'factura' : factura } ,
            function( data ) {
              $('#tbl_listatramite').html(data);
            });
    }

    function buscar_factura_cancelada() {
        numFactura = document.getElementById('factura_can').value;
        idProveedor = document.getElementById('id_proveedor').value;
        proceso_forma_pago = document.getElementById('proceso_forma_pago').value;
        fecha_reg_factura = document.getElementById('fecha_reg_factura').value;
        fecha_ven_factura = document.getElementById('fecha_ven_factura').value;
        document.getElementById("tbl_facturas_canceladas").innerHTML="";//tengo que destruir el contenido del div de
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/buscar_factura_cancelada') ?>" ,
            { 'numFactura' : numFactura, 'idProveedor' : idProveedor, 'proceso_forma_pago' : proceso_forma_pago,
            'fechaRegistro' : fecha_reg_factura, 'fechaVencimiento' : fecha_ven_factura } ,
            function( data ) {
              $('#tbl_facturas_canceladas').html(data);
            });
    }

</script>
<div class="tramite-pago-index">

    <center><h1><?= Html::encode($this->title) ?> (Cuentas por pagar)</h1></center>


    <div class="col-lg-12">
        <div class="panel panel-warning">
            <div class="panel-heading">
                Seleccione el proveedor
                <?php
                 if (@$idproveedor = DebitoSession::getIDproveedor()) {
                       echo '<div class="pull-right">' .Html::a('Actualizar datos de este Proveedor', ['proveedores/update', 'id'=>$idproveedor], ['class' => 'btn btn-primary btn-xs', 'target'=>'_blank']) .
                       ' ' . Html::a('Limpiar para agregar otro proveedor', ['limpiar'], ['class' => 'btn btn-warning btn-xs']) . '</div>';
                 } ?>
            </div>
            <div class="panel-body" id="datos_proveedor">
                <div class="col-lg-2">
                <label>ID Proveedor</label>
                <?php
                    if (@$idproveedor = DebitoSession::getIDproveedor()) {
                        echo '<h4>'.$idproveedor.'</h4><input type="hidden" id="id_proveedor" value="'.$idproveedor.'">';

                    } else
                         echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'class' => 'form-control', 'placeholder' =>'ID Proveedor', 'onchange' => 'javascript:agrega_proveedor(this.value)']);
                ?>
                </div>
                <div class="col-lg-4">
                <label>Proveedor</label>
                <div class="input-group">
                <span class="input-group-btn">
                    <button class="btn btn-default" data-toggle="modal" data-placement="top" title="Buscar proveedor para pago de facturas" type="button" data-target="#modalproveedor"><span class="glyphicon glyphicon-search"></span></button>
                </span>
                <?php
                if (@$proveedor = DebitoSession::getProveedor()) {
                    echo Html::input('text', '', $proveedor, ['class' => 'form-control', 'placeholder' => $proveedor, 'readonly' => true, 'onchange' => '']);
                } else
                    echo Html::input('text', '', '', ['class' => 'form-control', 'placeholder' =>'Nombre del proveedor', 'readonly' => true, 'onchange' => '']);
                ?>
                </div>
                </div>
                <div class="col-lg-6">
                <?php
                if (@$idproveedor = DebitoSession::getIDproveedor()) {//Muestro el saldo que se debe al proveedor
                    echo '<span style="float:right">
                            <label>Saldo Proveedor</label>
                            <div style="background-color:#FFC; padding:12px">';
                                $proveedor_ = Proveedores::find()->where(['=','codProveedores', $idproveedor])->one();
                                $mo = Moneda::findOne($proveedor_->idMoneda);
                                $credi_saldo = 0;
                                $facturas_compras = ComprasInventario::find()->where(['=','idProveedor', $idproveedor])->andWhere("estado_pago IN ('Pendiente','Proceso')", [])->all();
                                foreach ($facturas_compras as $key => $value) { $credi_saldo += $value->credi_saldo; }
                                echo '<div id="saldo_proveedor">'.$mo->simbolo." ".number_format($credi_saldo,2).'</div>';
                    echo   '</div>
                          </span>';
                      }
                ?>
                </div>
            </div>
        </div>
        <?= AlertBlock::widget([
                'type' => AlertBlock::TYPE_ALERT,
                'useSessionFlash' => true,
                'delay' => 20000
            ]);?>
    </div>

    <ul class="nav nav-tabs nav-justified">
        <li class="active" role="presentation" ><a href="#tramitar" data-toggle="tab" id="tramitar_tab">Tramitar pagos</a></li>
        <li role="presentation"><a href="#listatramite" data-toggle="tab" id="listatramite_tab">Lista de trámites de pago</a></li>
        <li role="presentation"><a href="#facturascanceladas" data-toggle="tab" id="facturascanceladas_tab">Facturas canceladas</a></li>
    </ul>
    <!-- Módulo seleccionado y apartado de facturas a proveedor-->
    <div class="table-responsive tab-content"><!-- IMPORTANTE este div me ayuda a que la informacion en los tab mantengan su postura-->
        <!-- ____________________________________________________Tramitar pagos-->
        <div class="tab-pane fade in active" id="tramitar">
        <br>
            <div class="col-lg-8">
                <div class="panel panel-info" id="panel">

                        <?php  //llamo la vista con las facturas pendientes de cancelar del proveedor seleccionado
                            if (@$idproveedor = DebitoSession::getIDproveedor()) {
                                echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">';
                                echo $this->render('listafacturas', [
                                    'idProveedor' => $idproveedor,
                                   ]);
                                echo '</div>';
                            } else
                                echo '<center><h3>Esperando un proveedor<h3></center>';
                        ?>

                </div>
            </div>
            <!-- Módulo de movimientos a tramites-->
            <div class="col-lg-4">
                <!-- Notas crédito, Notas débito y abonos -->
                <?php
                    if (@$idproveedor = DebitoSession::getIDproveedor()) {
                        echo '<div id="movimientos">
                                <div class="panel panel-info" id="panel2">
                                <center><h3>Movimiento de factura<small><br>Esperando una factura</small></h3><br></center>
                                </div>
                              </div>';
                    } else
                        echo '
                                <div class="panel panel-info" id="panel2">
                                    <center><h3>Esperando un proveedor</h3><br></center>
                                </div>
                              ';
                ?>
                <?php  //llamo la vista con el formulario para generar pago
                    if (@$idproveedor = DebitoSession::getIDproveedor()) {
                        echo '<div id="generar_pago">'.$this->render('generar_pago', [
                        'idProveedor' => $idproveedor,
                       ]).'</div>';
                    } else
                        echo '
                                <div class="panel panel-info" id="panel3">
                                    <center><h3>Esperando un proveedor</h3><br></center>
                                </div>
                            ';
                ?>
            </div>
        </div>
        <!-- ____________________________________________________Lista de tramites de pago-->
        <div class="tab-pane fade" id="listatramite">
        <br>
            <div class="col-lg-12">
                <?php  //llamo la vista con las facturas pendientes de cancelar del proveedor seleccionado
                    if (@$idproveedor = DebitoSession::getIDproveedor()) {
                        echo '<div class="col-lg-12 alert alert-info" role="alert">
                                <div class="col-lg-3">
                                '.Html::input('text', '', '', [/*'autofocus' => 'autofocus', */'size' => '30', 'id' => 'factura', 'class' => 'form-control', 'placeholder' =>'Ingrese No Factura y ENTER', 'onchange' => 'javascript:buscar_tramite()']).'
                                </div>
                                <div class="col-lg-3">
                                '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                                'options' => ['class'=>'form-control', 'placeholder'=>'FECHA REGISTRO dd-mm-yyyy', 'onchange'=>'javascript:buscar_tramite()',
                                'id'=>'fecha_reg_tramite']]).'
                                </div>
                                <div class="col-lg-3">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => ["Baja" => "Baja", "Media" => "Media", "Alta" => "Alta"],
                                'options' => [
                                    'placeholder' => 'PRIORIDAD',
                                    'id'=>'busqueda_prio',
                                    'onchange' => 'javascript:buscar_tramite()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
                            ]).'</div>';
                        echo '<div class="col-lg-3">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => ["Cancelado" => "Cancelado", "Pendiente" => "Pendiente"],
                                'options' => [
                                    'placeholder' => 'ESTADO',
                                    'id'=>'busqueda_est',
                                    'onchange' => 'javascript:buscar_tramite()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
                          ]).'</div>
                            </div>';
                        echo '<div id="tbl_listatramite">'.$this->render('listatramite', [
                        'idProveedor' => $idproveedor, 'prioridad_pago' => '', 'estado_tramite' => ''
                       ]).'</div>';


                    } else
                        echo '
                        <div class="panel panel-info" id="panel">
                        <center><h3>Esperando un proveedor<h3></center>
                        </div>
                        ';
                ?>


            </div>
        </div>
        <!-- ____________________________________________________Lista de facturas canceladas-->
        <div class="tab-pane fade" id="facturascanceladas">
        <br>
            <div class="col-lg-12">
                <?php  //llamo la vista con las facturas canceladas del proveedor seleccionado
                    if (@$idproveedor = DebitoSession::getIDproveedor()) {
                        echo '<div class="col-lg-12 alert alert-info" role="alert">
                                <div class="col-lg-3">
                                '.Html::input('text', '', '', ['size' => '30', 'id' => 'factura_can', 'class' => 'form-control', 'placeholder' =>'Ingrese No Factura', 'onKeyUp' => 'javascript:buscar_factura_cancelada()']).'
                                </div>
                                <div class="col-lg-3">'.Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => ["Contado" => "Contado", "Crédito" => "Crédito"],
                                'options' => [
                                    'placeholder' => 'PROCESO',
                                    'id'=>'proceso_forma_pago',
                                    'onchange' => 'javascript:buscar_factura_cancelada()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],'pluginOptions' => ['initialize'=> true,'allowClear' => true]//permite eliminar la seleccion
                            ]).'</div>
                                <div class="col-lg-3">
                                '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                                'options' => ['class'=>'form-control', 'placeholder'=>'FECHA REGISTRO dd-mm-yyyy', 'onchange'=>'javascript:buscar_factura_cancelada()',
                                'id'=>'fecha_reg_factura']]).'
                                </div>
                                <div class="col-lg-3">
                                '.yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
                                'options' => ['class'=>'form-control', 'placeholder'=>'FECHA VENCIMIENTO dd-mm-yyyy', 'onchange'=>'javascript:buscar_factura_cancelada()',
                                'id'=>'fecha_ven_factura']]).'
                                </div>
                              </div>';

                        echo '<div style="height: 400px;width: 100%; overflow-y: auto; ">
                        <div id="tbl_facturas_canceladas">'.$this->render('facturascanceladas', [
                        'idProveedor' => $idproveedor,  'numFactura' => '', 'formaPago' => '', 'fechaRegistro'=> '', 'fechaVencimiento'=> ''
                       ]).'</div>
                        </div>';
                    } else
                        echo '
                        <div class="panel panel-info" id="panel">
                        <center><h3>Esperando un proveedor<h3></center>
                        </div>
                        ';
                ?>


            </div>
        </div>
    </div>
    <!-- Modal para proveedores -->
    <div class="modal fade" id="modalproveedor" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Lista de Proveedores</h4>
        </div>
        <div class="modal-body">
            <?php
                echo Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '80', 'id' => 'busqueda', 'class' => 'form-control', 'placeholder' =>'Busque el ID o nombre del proveedor que desea agregar', 'onKeyUp' => 'javascript:buscar()']);
            ?>
            <br>
            <div id="resultadoBusqueda" style="height: 200px;width: 100%; overflow-y: auto; "></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>
      </div>
    </div>
  </div>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!--p>
        <?php //Html::a('Create Tramite Pago', ['create'], ['class' => 'btn btn-success']) ?>
    </p-->
    <?php /*GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idTramite_pago',
            'idProveedor',
            'cantidad_saldo_documento',
            'monto_tramite_pago',
            'porcentaje_descuento',
            // 'monto_pagar',
            // 'recibo_cancelacion',
            // 'email_proveedor:email',
            // 'prioridad_pago',
            // 'usuario_registra',
            // 'fecha_registra',
            // 'usuario_cancela',
            // 'fecha_cancela',
            // 'usuario_aplica',
            // 'fecha_aplica',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */?>

</div>
