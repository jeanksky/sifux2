<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TramitePago */

$this->title = 'Create Tramite Pago';
$this->params['breadcrumbs'][] = ['label' => 'Tramite Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tramite-pago-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
