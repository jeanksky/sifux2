<?php
use yii\helpers\Html;
use backend\models\ComprasInventario;
use yii\helpers\Url;
use yii\bootstrap\Modal;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
$info_modulo = 'Los botones de colores son filtros.
                Cada color corresponde a la búsqueda instantánea de
                facturas por vencer, vencidas hoy o con más de un día vencidas.<br><br>
                Para crear un movimiento a una factura seleccione la línea correspondiente a la factura.<br><br>
                Para cancelar una o varias facturas seleccione con un check las facturas a cancelar y presione
                la flecha hacia abajo.<br> De lo contrario seleccione las facturas que ya no desea cancelar y presione la flecha hacia arriba.<br> También puede escoger las facturas a cancelar de forma individual ingresando el número de factura y presionando Enter.';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
//me pone el numero de compra en el encabezado de la modal

//Modal que me muestra la factura de facturas pendientes
    $(document).on('click', '#activity-index-link-factura', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $( "#mostrar_factura_modal" ).html( data );
                        $('#modalFactura').modal();
                    }
                );
            }));

    $(document).ready(function () {
        document.getElementById('paso_factura').select();//permite que el input esté seleccionado
            //me checa todas las facturas pendientes
            $('#checkAll').on('click', function() {
                if (this.checked == true)
                    $('#tabla_facturas_pendientes').find('input[name="checkboxRow[]"]').prop('checked', true);
                else
                    $('#tabla_facturas_pendientes').find('input[name="checkboxRow[]"]').prop('checked', false);
            });
            //me checa todas las facturas en oproceso de cancelacion
            $('#checkAll2').on('click', function() {
                if (this.checked == true)
                    $('#tabla_facturas_proceso').find('input[name="checkboxRowinv[]"]').prop('checked', true);
                else
                    $('#tabla_facturas_proceso').find('input[name="checkboxRowinv[]"]').prop('checked', false);
            });
    });

    $(document).ready(function () {
       /* detalle_pago = document.getElementById("detalle_pago").value;
        if (detalle_pago == "") {
             $('#realizar_pago').addClass('disabled');
        } else {
            $('#realizar_pago').removeClass('disabled');
        }*/
    });

    //Marcador de facturas para obtener datos a manipular
    function marcarfactura(x, id) {
        //http://foro.elhacker.net/desarrollo_web/evento_onfocus_en_fila_de_una_tabla-t381665.0.html
        // Obtenemos todos los TR de la tabla con id "tabla_facturas_pendientes"
        // despues del tbody.
        var elementos = document.getElementById('tabla_facturas_pendientes').
        getElementsByTagName('tbody')[0].getElementsByTagName('tr');

        // Por cada TR empezando por el segundo, ponemos fondo blanco.
        for (var i = 0; i < elementos.length; i++) {
            elementos[i].style.background='white';
        }
        // Al elemento clickeado le ponemos fondo amarillo.
        x.style.background="yellow";

        div = 'nc';//esto solo serviria si dejo activo el tab a la hora de seleccionar la linea de una factura (nc, nd, ab)
        inyectarpagoparcial(id, div);//inyecto los valores al area de movimiento de factura
        //$('#monto_nc').val('');
        //$('#detalle_nc').val('');
       /* $("#nuevo_saldo_div").load(location.href+" #nuevo_saldo_div","");
        $("#refr_nc_div").load(location.href+" #refr_nc_div","");
        $("#movi_credit").load(location.href+" #movi_credit","");

        $('#referencia').val('ABONO A FACTURA');*/
    }

    function h_compra(id) {
     $( "#h_factura_modal" ).html( 'Compra ID: '+id );
    }

    //inyecto los datos del cliente que necesito para manipular un movimiento de credito
    function inyectarpagoparcial(id, div){

        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/movimientos') ?>" , { 'id' : id, 'div' : div } ,
            function( data ) {
              $('#movimientos').html(data);
            });
    }

//obtengo la o las facturas que voy a cancelar
    function obtener_facturas_marcadas(){
        var checkboxValues = "";//
        //var checkboxValues = new Array();//

        $('input[name="checkboxRow[]"]:checked').each(function() {//
            checkboxValues += $(this).val() + ",";//
        });//
        //eliminamos la última coma.
        checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/obtener_facturas_marcadas') ?>",
                type:"post",
                data: { 'checkboxValues' : checkboxValues },
                beforeSend: function() {
                    //$('#load_buton').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
                    $("#movimientos").load(location.href+" #movimientos","");
                },
                success: function(notif){
                    generar_pago();

                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_uno') ?>" , { 'idProveedor' : '<?= $idProveedor ?>' , 'conditionfecha' : 'todas'} , function( data ) {
                      $('#tbl1').html(data);
                    });
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //en proceso en tiempo real, de esta forma me permite hacer seleccion multiple
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_dos') ?>" , { 'idProveedor' : '<?= $idProveedor ?>' } , function( data ) {
                      $('#tbl2').html(data);
                    });
                    document.getElementById("notificacion_lista_factura").innerHTML = notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notificacion_lista_factura").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);

                },
                error: function(msg, status,err){
                 //alert('No pasa 36');
                }
            });
        document.getElementById('paso_factura').select();//permite que el input esté seleccionado

    }
    //obtengo la o las facturas a cancelar que selecciono para no cancelarlas, enviarlas de regreso a Facturas pendientes
    function obtener_facturas_marcadas_regreso(){
        var checkboxValues = "";//

        $('input[name="checkboxRowinv[]"]:checked').each(function() {//
            checkboxValues += $(this).val() + ",";
        });//
        //eliminamos la última coma.
        checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/obtener_facturas_marcadas_regreso') ?>",
                type:"post",
                data: { 'checkboxValues' : checkboxValues },
                beforeSend: function() {
                    //$('#load_buton').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
                },
                success: function(notif){
                    generar_pago();
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_uno') ?>" , { 'idProveedor' : '<?= $idProveedor ?>' , 'conditionfecha' : 'todas'} , function( data ) {
                      $('#tbl1').html(data);
                    });
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //en proceso en tiempo real, de esta forma me permite hacer seleccion multiple
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_dos') ?>" , { 'idProveedor' : '<?= $idProveedor ?>' } , function( data ) {
                      $('#tbl2').html(data);
                    });
                    document.getElementById("notificacion_lista_factura").innerHTML = notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notificacion_lista_factura").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);
                },
                error: function(msg, status,err){
                 //alert('No pasa 56');
                }
            });
        document.getElementById('paso_factura').select();//permite que el input esté seleccionado
    }

    //obtengo el html de tbl_uno para refrescarlo en el div tbl1
    function refrescar_tbl_uno() {
        $("#saldo_proveedor").load(location.href+' #saldo_proveedor');
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_uno') ?>" , { 'idProveedor' : '<?= $idProveedor ?>', 'conditionfecha' : 'todas' } , function( data ) {
                      $('#tbl1').html(data);
                    });
    }
    //obtengo el html de tbl_dos para refrescarlo en el div tbl2
    function refrescar_tbl_dos() {
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_dos') ?>" , { 'idProveedor' : '<?= $idProveedor ?>' } , function( data ) {
                      $('#tbl2').html(data);
                    });
    }

    //refresca el div de generar pago
    function generar_pago() {
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/generar_pago') ?>" , { 'idProveedor' : '<?= $idProveedor ?>' } , function( data ) {
                      $('#generar_pago').html(data);
                      document.getElementById("media").checked = true;//asigno que la prioridad de pago es media
                    });

    }

    //
    /*function refrescar_tbl_nc() {
        $("#not_cr_tbl").empty().load(location.href+' #not_cr_tbl','');
    }*/

    //obtengo y cargo la factura a la lista por cancelar con el enter
    function agrega_factura(paso_factura){
        idProveedor = "<?php echo $idProveedor ?>";
        if (paso_factura!='') {
           $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/agrega_factura_lista') ?>",
                type:"post",
                data: { 'factura' : paso_factura, 'idProveedor' : idProveedor },
                success: function(notif){
                    generar_pago();
                    $("#movimientos").load(location.href+" #movimientos","");
                    //$( "#tbl1" ).remove();
                     //$("#tbl1").empty();
                    //$("#tbl1").empty().load(location.href+" #tbl1","");
                    //$("#tbl2").empty().load(location.href+" #tbl2","");
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //pendientes en tiempo real, de esta forma me permite hacer seleccion multiple
                    refrescar_tbl_uno();
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //en proceso en tiempo real, de esta forma me permite hacer seleccion multiple
                    refrescar_tbl_dos();
                    //$("#area_ingresar_factura").load(location.href+" #area_ingresar_factura","");
                    $('#paso_factura').val('');
                    document.getElementById('paso_factura').select();//permite que el input esté seleccionado
                    document.getElementById("notificacion_lista_factura").innerHTML = notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notificacion_lista_factura").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);
                },
                error: function(msg, status,err){
                 document.getElementById("notificacion_lista_factura").innerHTML = 'Sin resultados';
                    setTimeout(function() {
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);
                }
            }); //fin ajax
        }
    }

    //obtengo y cargo la factura a la lista por cancelar con el boton
    function agrega_factura_btn(){
        paso_factura = document.getElementById("paso_factura").value;
        idProveedor = "<?php echo $idProveedor ?>";
        if (paso_factura!='') {
           $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/agrega_factura_lista') ?>",
                type:"post",
                data: { 'factura' : paso_factura, 'idProveedor' : idProveedor },
                success: function(notifi){
                    generar_pago();
                    $("#movimientos").load(location.href+" #movimientos","");
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //pendientes en tiempo real, de esta forma me permite hacer seleccion multiple
                    refrescar_tbl_uno();
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //en proceso en tiempo real, de esta forma me permite hacer seleccion multiple
                    refrescar_tbl_dos();
                    $('#paso_factura').val('');
                    document.getElementById('paso_factura').select();//permite que el input esté seleccionado
                    document.getElementById("notificacion_lista_factura").innerHTML = notifi;
                    setTimeout(function() {
                        $("#notificacion_lista_factura").fadeIn();
                    });
                    setTimeout(function() {
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);
                },
                error: function(msg, status,err){
                 document.getElementById("notificacion_lista_factura").innerHTML = 'Sin resultados';
                    setTimeout(function() {
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);
                }
            });
        }
    }

    //Me permite enviar el tipo de fech que quiero mostrar ya sea vencidas, por vencer...
    function facturafecha(tipofecha){
        idProveedor = "<?php echo $idProveedor ?>";
        //envio los parametros con los que quiero que me filtre las facturas que quiero mostrar en pendiente
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_uno') ?>" , { 'idProveedor' : '<?= $idProveedor ?>', 'conditionfecha' : tipofecha } , function( data ) {
          $('#tbl1').html(data);
        });
    }

    function mov_facturafecha(tipofecha) {

        idProveedor = "<?php echo $idProveedor ?>";

           $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/mov_facturafecha') ?>",
                type:"post",
                data: { 'conditionfecha' : tipofecha, 'idProveedor' : idProveedor },
                success: function(notifi){
                    generar_pago();//funcion que se encarga de refrescar el div que cuenta con el resumen de los datos de pago
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //pendientes en tiempo real, de esta forma me permite hacer seleccion multiple
                    refrescar_tbl_uno();
                    //luego de modificar el estado de la factura en bd traigo la tabla por aparte de las facturas
                    //en proceso en tiempo real, de esta forma me permite hacer seleccion multiple
                    refrescar_tbl_dos();
                    $('#paso_factura').val('');
                    document.getElementById('paso_factura').select();//permite que el input esté seleccionado
                    document.getElementById("notificacion_lista_factura").innerHTML = notifi;
                    setTimeout(function() {
                        $("#notificacion_lista_factura").fadeIn();
                    });
                    setTimeout(function() {
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);
                },
                error: function(msg, status,err){
                 document.getElementById("notificacion_lista_factura").innerHTML = 'Sin resultados';
                    setTimeout(function() {
                        $("#notificacion_lista_factura").fadeOut();
                    },3000);
                }
            });

    }

</script>
<style>
    #modalFactura .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="col-lg-12">
<br>
<p style="text-align:right">
    <a data-placement="top">
        <span id="ol" title="Información del módulo" data-toggle="popover" class="fa fa-map-signs fa-3x" data-placement="bottom" data-html="true" data-content="<?= $info_modulo ?>" ></span>
    </a>
    <a class="btn btn-primary btn-sm" onclick="facturafecha('porvencer')" >Por vencer</a>
    <a class="btn btn-warning btn-sm" onclick="facturafecha('hoy')" >Vencidas hoy</a>
    <a class="btn btn-danger btn-sm" onclick="facturafecha('vencidas')" >+1 dia Vencidas</a>
    <a class="btn btn-default btn-sm" onclick="facturafecha('todas')" >Todas</a>
</p>
<div id="tbl1">
    <div class="panel panel-info">
        <div style="height: 300px;width: 100%; overflow-y: auto; ">

            <?php
                $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago", [":estado_pago"=>'Pendiente'])->orderBy(['idCompra' => SORT_DESC])->all();
                echo '<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
                echo '<thead>';
                printf('<tr>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th class="actions button-column">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="checkAll" /></th></tr>',
                        'N° IN-PROV',
                        'N° FACTURA',
                        'FECH.REG',
                        'FECH.VEN',
                        'MONTO',
                        'NOTA DÉBITO',
                        'NOTA CRÉDITO',
                        'ABONO',
                        'SALDO FACTURA'
                        );
                echo '</thead>';
                echo '<tbody class="buscar1">';

                foreach($facturasCredito as $position => $factura) {
                    $idCompra = "'".$factura['idCompra']."'";
                    $command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND idCompra = " . $idCompra);
                    $command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND idCompra = " . $idCompra);
                    $command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND idCompra = " . $idCompra);
                    $suma_monto_nc = floatval($command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
                    $suma_monto_nd = floatval($command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
                    $suma_monto_ab = floatval($command_ab->queryScalar()); //ingreso la suma del monto de abonos
                    $fecha_actual = date('d-m-Y');
                    list ($dia_hoy, $mes_hoy, $anio_hoy) = explode("-", $fecha_actual);//separo dias mes año de hoy
                    $border = '';
                    $fecha_registro = date('d-m-Y', strtotime( $factura['fechaRegistro'] ));
                    $fecha_vencimiento = date('d-m-Y', strtotime( $factura['fechaVencimiento'] ));
                    list ($dia_ven, $mes_ven, $anio_ven) = explode("-", $fecha_vencimiento);
                        $fecha_antes_vencimiento5 = date ( 'd-m-Y' , strtotime ( '-5 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento4 = date ( 'd-m-Y' , strtotime ( '-4 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento3 = date ( 'd-m-Y' , strtotime ( '-3 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento2 = date ( 'd-m-Y' , strtotime ( '-2 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento1 = date ( 'd-m-Y' , strtotime ( '-1 day' , strtotime ( $fecha_vencimiento ) ) );
                    if(strtotime($fecha_vencimiento) < strtotime($fecha_actual)){
                        $border = 'red 10px solid;';
                        }
                    if($fecha_actual == $fecha_vencimiento){
                        $border = 'orange 10px solid;';
                    }
                    if(($fecha_actual == $fecha_antes_vencimiento5) || ($fecha_actual == $fecha_antes_vencimiento4) || ($fecha_actual == $fecha_antes_vencimiento3) || ($fecha_actual == $fecha_antes_vencimiento2) || ($fecha_actual == $fecha_antes_vencimiento1)){
                        $border = 'blue 10px solid;';
                    }
                    $mostrarFactura = Html::a('', ['#'], [
                                    'class'=>'glyphicon glyphicon-search',
                                    'id' => 'activity-index-link-factura',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalFactura',
                                    'onClick' => 'h_compra('.$factura['idCompra'].')',
                                    'data-url' => Url::to(['tramite-pago/vista_factura', 'id' => $factura['idCompra']]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Procede a mostrar la factura')]);
                    $idFactura_activa = $factura['idCompra'];
                    $checkbox = '<input id="striped" type="checkbox" name="checkboxRow[]" value="'.$idFactura_activa.'">';
                    printf('<tr style = "border-left: '.$border.' border-right: '.$border.'" onclick="marcarfactura(this, '.$factura['idCompra'].')">
                            <td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                                $factura['n_interno_prov'],
                                $factura['numFactura'],
                                $fecha_registro,
                                $fecha_vencimiento,
                                number_format($factura['total'],2),
                                number_format($suma_monto_nd,2),
                                number_format($suma_monto_nc,2),
                                number_format($suma_monto_ab,2),
                                number_format($factura['credi_saldo'],2),
                                $mostrarFactura . '&nbsp;' . $checkbox
                                );
                }
                echo '</tbody>';
                echo '</table>';
            ?>

        </div>
    </div>
</div>
</div>
<div class="col-lg-6">
    <div id="area_ingresar_factura">
    <div class="input-group">
        <?= Html::input('text', '', '', [/*'autofocus' => 'autofocus', */'size' => '30', 'id' => 'paso_factura', 'class' => 'form-control', 'placeholder' =>'Ingresar factura', 'onchange' => 'javascript:agrega_factura(this.value)']) ?>
        <span class="input-group-btn">
            <button class="btn btn-default" data-toggle="titulo" data-placement="right" title="Agregar factura" type="button" onclick="agrega_factura_btn()"><span class="glyphicon glyphicon-arrow-down"></span></button>
        </span>
    </div><br>
    <div id="notificacion_lista_factura"></div>
    </div>
</div>
<div class="col-lg-6">
    <a class="btn btn-primary btn-sm" onclick="mov_facturafecha('porvencer')" title="Mover facturas por vencer" data-toggle="titulo" data-placement="top">
        <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
    </a>
    <a class="btn btn-warning btn-sm" onclick="mov_facturafecha('hoy')" title="Mover facturas vencidas de hoy" data-toggle="titulo" data-placement="top">
        <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
    </a>
    <a class="btn btn-danger btn-sm" onclick="mov_facturafecha('vencidas')" title="Mover facturas con +1 dias vencidas" data-toggle="titulo" data-placement="top">
        <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
    </a>

    <div class="pull-right" id="load_buton">
        <a data-placement="top">
            <span onclick="obtener_facturas_marcadas()" title="Agregar facturas seleccionadas a Facturas a cancelar" data-toggle="titulo" class="fa fa-arrow-circle-o-down fa-3x" aria-hidden="true"></span>
        </a>
        <a data-placement="top">
            <span onclick="obtener_facturas_marcadas_regreso()" title="Agregar facturas seleccionadas a Facturas en espera" data-toggle="titulo" class="fa fa-arrow-circle-o-up fa-3x" aria-hidden="true"></span>
        </a>
    </div>
</div>
<div class="col-lg-12">
<br>
<div id="tbl2">
    <div class="panel panel-info">
        <div style="height: 300px;width: 100%; overflow-y: auto; ">

            <?php
                $facturasCredito = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_pago = :estado_pago", [":estado_pago"=>'Proceso'])->orderBy(['idCompra' => SORT_DESC])->all();
                echo '<table class="items table table-striped" id="tabla_facturas_proceso"  >';
                echo '<thead>';
                printf('<tr>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th class="actions button-column">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="checkAll2" /></th></tr>',
                        'N° IN-PROV',
                        'N° FACTURA',
                        'FECH.REG',
                        'FECH.VEN',
                        'MONTO',
                        'NOTA DÉBITO',
                        'NOTA CRÉDITO',
                        'ABONO',
                        'SALDO FACTURA'
                        );
                echo '</thead>';
                echo '<tbody class="buscar2">';
                foreach($facturasCredito as $position => $factura) {
                    $idCompra = "'".$factura['idCompra']."'";
                    $command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND idCompra = " . $idCompra );
                    $command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND idCompra = " . $idCompra);
                    $command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND idCompra = " . $idCompra);
                    $suma_monto_nc = floatval($command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
                    $suma_monto_nd = floatval($command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
                    $suma_monto_ab = floatval($command_ab->queryScalar()); //ingreso la suma del monto de abonos
                    $fecha_actual = date('d-m-Y');
                    list ($dia_hoy, $mes_hoy, $anio_hoy) = explode("-", $fecha_actual);//separo dias mes año de hoy
                    $border = '';
                    $fecha_registro = date('d-m-Y', strtotime( $factura['fechaRegistro'] ));
                    $fecha_vencimiento = date('d-m-Y', strtotime( $factura['fechaVencimiento'] ));
                    list ($dia_ven, $mes_ven, $anio_ven) = explode("-", $fecha_vencimiento);
                        $fecha_antes_vencimiento5 = date ( 'd-m-Y' , strtotime ( '-5 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento4 = date ( 'd-m-Y' , strtotime ( '-4 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento3 = date ( 'd-m-Y' , strtotime ( '-3 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento2 = date ( 'd-m-Y' , strtotime ( '-2 day' , strtotime ( $fecha_vencimiento ) ) );
                        $fecha_antes_vencimiento1 = date ( 'd-m-Y' , strtotime ( '-1 day' , strtotime ( $fecha_vencimiento ) ) );
                    if(strtotime($fecha_vencimiento) < strtotime($fecha_actual)){
                        $border = 'red 10px solid;';
                        }
                    if($fecha_actual == $fecha_vencimiento){
                        $border = 'orange 10px solid;';
                    }
                    if(($fecha_actual == $fecha_antes_vencimiento5) || ($fecha_actual == $fecha_antes_vencimiento4) || ($fecha_actual == $fecha_antes_vencimiento3) || ($fecha_actual == $fecha_antes_vencimiento2) || ($fecha_actual == $fecha_antes_vencimiento1)){
                        $border = 'blue 10px solid;';
                    }
                    $mostrarFactura = Html::a('', ['#'], [
                                    'class'=>'glyphicon glyphicon-search',
                                    'id' => 'activity-index-link-factura',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalFactura',
                                    'data-url' => Url::to(['tramite-pago/vista_factura', 'id' => $factura['idCompra']]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Procede a mostrar la factura')]);
                    $idFactura_activa = $factura['idCompra'];
                    $checkbox = '<input id="striped" type="checkbox" name="checkboxRowinv[]" value="'.$idFactura_activa.'">';
                    printf('<tr style = "border-left: '.$border.' border-right: '.$border.'" >
                            <td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                                $factura['n_interno_prov'],
                                $factura['numFactura'],
                                $fecha_registro,
                                $fecha_vencimiento,
                                number_format($factura['total'],2),
                                number_format($suma_monto_nd,2),
                                number_format($suma_monto_nc,2),
                                number_format($suma_monto_ab,2),
                                number_format($factura['credi_saldo'],2),
                                $mostrarFactura . '&nbsp;' . $checkbox
                                );
                }
                echo '</tbody>';
                echo '</table>';
            ?>

        </div>
    </div>
</div>
<?php
    //-------------------------------------------------Pantallas modales-----------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------
    //Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalFactura',
                //'size'=>'modal-lg',
                'header' => '<center><h2 class="modal-title"><div id="h_factura_modal"></div></h2></center>',
                'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='panel-body'><div id='mostrar_factura_modal'></div></div>";

            Modal::end();
    ?>
</div>
