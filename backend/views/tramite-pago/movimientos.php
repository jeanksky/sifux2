<?php
use yii\helpers\Html;
use yii\widgets\MaskedInput;
//use backend\models\MovimientoPagar;
    
    $idCompra = $factura_compra->idCompra;
    $numerofactura = $factura_compra->numFactura;
?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->
<script type="text/javascript">
    $(document).ready(function () {
        var div_movimiento = '<?= $div ?>';
      /*  if (div_movimiento == 'nc') {
            $("#credito").addClass("active"); //activo la pestaña tav 
            $("#nc_tab").tab("show"); //muestro lo que contiene la pestaña
        }

        if (div_movimiento == 'nd') {
            $("#debito").addClass("active"); //activo la pestaña tav 
            $("#nd_tab").tab("show"); //muestro lo que contiene la pestaña
        }
        
         */
    });

    //Agrega nota de credito y refresca tablas
    function agregar_nc() {
        direccionlocal = "<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/index') ?>"
        idCompra = '<?= $idCompra ?>';
        div_tab = 'nd';
        monto_nc = document.getElementById("monto_nc"); 
        detalle_nc = document.getElementById("detalle_nc");
        numero_nc = document.getElementById("numero_nc");
        var confirm_nc_create = confirm("¿Está seguro de crear una nota de crédito?");
        if (confirm_nc_create == true) {
        if (monto_nc.value=='' || detalle_nc.value=='' || numero_nc.value=='') {
            monto_nc.style.border = "5px solid red";
            detalle_nc.style.border = "5px solid red";
            numero_nc.style.border = "5px solid red";
        }
        else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/agrega_nota_credito') ?>",
                type:"post",
                dataType: 'json',
                data: { 'monto_nc' : monto_nc.value, 'detalle_nc': detalle_nc.value, 'idCompra': idCompra, 'numero_nc' : numero_nc.value },
                success: function(notif){
                    
                    //con esto me refresca tbl_uno, cuya funsion está en listafacturas.php 
                    refrescar_tbl_uno(); 
                    //con esto me refresca tbl_dos, cuya funsion está en listafacturas.php 
                    refrescar_tbl_dos(); 
                    monto_nc.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    detalle_nc.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    numero_nc.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    $('#detalle_nc').val('');
                    $('#monto_nc').val('');
                    $('#numero_nc').val('');
                    document.getElementById("notif_nc").innerHTML = notif.bandera;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notif_nc").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notif_nc").fadeOut();
                    },3000);

                    //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nc') ?>" , { 'id' : idCompra } , 
                    function( data ) {
                      $('#tbl_nc').html(data);
                    });
                    //refrescar_movimiento(numerofactura, div_tab);
                    //$("#tbl_nc").load(location.href+' #tbl_nc');
                    if (notif.estado_pago == 'Cancelada') {
                        $("#movimientos").load(location.href+" #movimientos","");
                    }
                },
                error: function(msg, status,err){
                 alert('Error linea 67');
                }
            }); 
        }
    } else {

    }
    }

    //actualiza una nota de credito
    function actualizar_nc() {
        idCompra = '<?= $idCompra ?>';
        div_tab = 'nd';
        monto_nc = document.getElementById("monto_nc_u"); 
        detalle_nc = document.getElementById("detalle_nc_u");
        numero_nc = document.getElementById("numero_nc_u");
        idMov = document.getElementById("id_nc_u").value;
        //$('#activity-update-movimiento'+idMov).popover('hide');
        if (monto_nc.value=='' || detalle_nc.value=='' || numero_nc.value=='') {
            monto_nc.style.border = "5px solid red";
            detalle_nc.style.border = "5px solid red";
            numero_nc.style.border = "5px solid red";
        }
        else {
            $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/actualiza_nota_credito') ?>",
                    type:"post",
                    dataType: 'json',
                    data: { 'idMov' : idMov, 'monto_nc' : monto_nc.value, 'detalle_nc': detalle_nc.value, 'idCompra': idCompra, 'numero_nc': numero_nc.value },
                    success: function(notif){
                    //con esto me refresca tbl_uno, cuya funsion está en listafacturas.php 
                    refrescar_tbl_uno(); 
                    //con esto me refresca tbl_dos, cuya funsion está en listafacturas.php 
                    refrescar_tbl_dos();
                    document.getElementById("notif_nc").innerHTML = notif.bandera; 
                    setTimeout(function() {//aparece la alerta en un milisegundo
                            $("#notif_nc").fadeIn();
                        });
                        setTimeout(function() {//se oculta la alerta luego de 4 segundos
                            $("#notif_nc").fadeOut();
                        },3000);

                        //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nc') ?>" , { 'id' : idCompra } , 
                                function( data ) {
                                  $('#tbl_nc').html(data);
                                });
                    if (notif.estado_pago == 'Cancelada') {
                        $("#movimientos").load(location.href+" #movimientos","");
                    }

                    },
                    error: function(msg, status,err){
                     alert('Error linea 105');
                    }
                });
        }
        //$('#btn_accion_nc').val('Actualizar Mov #'+idMov);
    }

    //elimina la nota de credito
    function eliminar_nc(idMov, monto_movimiento) {
        idCompra = '<?= $idCompra ?>';
        var confirm_nc_delete = confirm("¿Está seguro de eliminar esta nota de crédito?");
        if (confirm_nc_delete == true) {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/elimina_nota_credito') ?>",
                type:"post",
                data: { 'idMov' : idMov, 'idCompra': idCompra, 'monto_movimiento' : monto_movimiento },
                success: function(notif){
                //con esto me refresca tbl_uno, cuya funsion está en listafacturas.php 
                refrescar_tbl_uno(); 
                //con esto me refresca tbl_dos, cuya funsion está en listafacturas.php 
                refrescar_tbl_dos();
                document.getElementById("notif_nc").innerHTML = notif; 
                setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notif_nc").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notif_nc").fadeOut();
                    },3000);

                    //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nc') ?>" , { 'id' : idCompra } , 
                            function( data ) {
                              $('#tbl_nc').html(data);
                            });
                },
                error: function(msg, status,err){
                 alert('Error linea 137');
                }
            });
        } else { }
    }

    //Agrega nota de débito y refresca tablas
    function agregar_nd() {
        idCompra = '<?= $idCompra ?>';
        monto_nd = document.getElementById("monto_nd"); 
        detalle_nd = document.getElementById("detalle_nd");
        numero_nd = document.getElementById("numero_nd");
        var confirm_nd_create = confirm("¿Está seguro de crear una nota de débito?");
        if (confirm_nd_create == true) {
        if (monto_nd.value=='' || detalle_nd.value=='' || numero_nd.value=='') {
            monto_nd.style.border = "5px solid red";
            detalle_nd.style.border = "5px solid red";
            numero_nd.style.border = "5px solid red";
        }
        else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/agrega_nota_debito') ?>",
                type:"post",
                data: { 'monto_nd' : monto_nd.value, 'detalle_nd': detalle_nd.value, 'idCompra': idCompra, 'numero_nd' : numero_nd.value },
                success: function(notif){
                    
                    //con esto me refresca tbl_uno, cuya funsion está en listafacturas.php 
                    refrescar_tbl_uno(); 
                    //con esto me refresca tbl_dos, cuya funsion está en listafacturas.php 
                    refrescar_tbl_dos(); 
                    monto_nd.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    detalle_nd.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    numero_nd.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    $('#detalle_nd').val('');
                    $('#monto_nd').val('');
                    $('#numero_nd').val('');
                    document.getElementById("notif_nd").innerHTML = notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notif_nd").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notif_nd").fadeOut();
                    },3000);

                    //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nd') ?>" , { 'id' : idCompra } , 
                    function( data ) {
                      $('#tbl_nd').html(data);
                    });
                },
                error: function(msg, status,err){
                 alert('Error linea 67');
                }
            }); 
        }
    } else {

    }
    }//fin crear nota de debito

    //actualiza una nota de credito
    function actualizar_nd() {
        idCompra = '<?= $idCompra ?>';
        div_tab = 'nd';
        monto_nd = document.getElementById("monto_nd_u"); 
        detalle_nd = document.getElementById("detalle_nd_u");
        numero_nd = document.getElementById("numero_nd_u");
        idMov = document.getElementById("id_nd_u").value;
        //$('#activity-update-movimiento'+idMov).popover('hide');
        if (monto_nd.value=='' || detalle_nd.value=='' || detalle_nd.numero_nd=='') {
            monto_nd.style.border = "5px solid red";
            detalle_nd.style.border = "5px solid red";
            numero_nd.style.border = "5px solid red";
        }
        else {
            $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/actualiza_nota_debito') ?>",
                    type:"post",
                    data: { 'idMov' : idMov, 'monto_nd' : monto_nd.value, 'detalle_nd': detalle_nd.value, 'idCompra': idCompra, 'numero_nd' : numero_nd.value },
                    success: function(notif){
                    //con esto me refresca tbl_uno, cuya funsion está en listafacturas.php 
                    refrescar_tbl_uno(); 
                    //con esto me refresca tbl_dos, cuya funsion está en listafacturas.php 
                    refrescar_tbl_dos();
                    document.getElementById("notif_nd").innerHTML = notif; 
                    setTimeout(function() {//aparece la alerta en un milisegundo
                            $("#notif_nd").fadeIn();
                        });
                        setTimeout(function() {//se oculta la alerta luego de 4 segundos
                            $("#notif_nd").fadeOut();
                        },3000);

                        //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nd') ?>" , { 'id' : idCompra } , 
                                function( data ) {
                                  $('#tbl_nd').html(data);
                                });
                    },
                    error: function(msg, status,err){
                     alert('Error linea 105');
                    }
                });
        }
    }//fin actualiza nd

    //elimina la nota de débito
    function eliminar_nd(idMov, monto_movimiento) {
        idCompra = '<?= $idCompra ?>';
        var confirm_nd_delete = confirm("¿Está seguro de eliminar esta nota de débito?");
        if (confirm_nd_delete == true) {
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/elimina_nota_debito') ?>",
                type:"post",
                data: { 'idMov' : idMov, 'idCompra': idCompra, 'monto_movimiento' : monto_movimiento },
                success: function(notif){
                //con esto me refresca tbl_uno, cuya funsion está en listafacturas.php 
                refrescar_tbl_uno(); 
                //con esto me refresca tbl_dos, cuya funsion está en listafacturas.php 
                refrescar_tbl_dos();
                document.getElementById("notif_nd").innerHTML = notif; 
                setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notif_nd").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notif_nd").fadeOut();
                    },3000);

                    //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_nd') ?>" , { 'id' : idCompra } , 
                            function( data ) {
                              $('#tbl_nd').html(data);
                            });
                },
                error: function(msg, status,err){
                 alert('Error linea 137');
                }
            });
        } else { }
    }//fin eliminar nota debito

    //Agrega nota de credito y refresca tablas
    function agregar_ab() {
        direccionlocal = "<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/index') ?>"
        idCompra = '<?= $idCompra ?>';
        div_tab = 'ab';
        numero_ab = document.getElementById("numero_ab");
        monto_ab = document.getElementById("monto_ab"); 
        detalle_ab = document.getElementById("detalle_ab");
        var confirm_nc_create = confirm("¿Está seguro de crear un abono?");
        if (confirm_nc_create == true) {
        if (monto_ab.value=='' || detalle_ab.value=='' || numero_ab.value=='') {
            monto_ab.style.border = "5px solid red";
            detalle_ab.style.border = "5px solid red";
            numero_ab.style.border = "5px solid red";
        }
        else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('tramite-pago/agrega_abono') ?>",
                type:"post",
                data: { 'monto_ab' : monto_ab.value, 'detalle_ab': detalle_ab.value, 'idCompra': idCompra, 'numero_ab': numero_ab.value },
                success: function(notif){
                    
                    //con esto me refresca tbl_uno, cuya funsion está en listafacturas.php 
                    refrescar_tbl_uno(); 
                    //con esto me refresca tbl_dos, cuya funsion está en listafacturas.php 
                    refrescar_tbl_dos(); 
                    monto_ab.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    detalle_ab.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    numero_ab.style.border = "1px solid #ccc";//regresamos color borde normal si fuera el caso de error por nulos
                    $('#detalle_ab').val('');
                    $('#monto_ab').val('');
                    $('#numero_ab').val('');
                    document.getElementById("notif_ab").innerHTML = notif;
                    setTimeout(function() {//aparece la alerta en un milisegundo
                        $("#notif_ab").fadeIn();
                    });
                    setTimeout(function() {//se oculta la alerta luego de 4 segundos
                        $("#notif_ab").fadeOut();
                    },3000);

                    //obtengo la tabla de nota de credito y lo refreco en el div tbl_nc
                    $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/tbl_ab') ?>" , { 'id' : idCompra } , 
                    function( data ) {
                      $('#tbl_ab').html(data);
                    });
                    //refrescar_movimiento(numerofactura, div_tab);
                    //$("#tbl_nc").load(location.href+' #tbl_nc');
                },
                error: function(msg, status,err){
                 alert('Error linea 67');
                }
            }); 
        }
    } else {

    }
    }
    //imprime la nota de crédito
    /*function imprimir_nc(idMov) {
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/imprimir_nc') ?>" , { 'idMov' : idMov } , 
                            function( data ) {
                              $('#imprimir_mov').html(data);
                            });
    }*/

    //espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
        var nav4 = window.Event ? true : false; 
        var key = nav4 ? evt.which : evt.keyCode; 
        return (key <= 13 || key==46 || (key >= 48 && key <= 57)); 
    }
</script>
<div class="panel panel-info">
    <p><center><h3>Factura N°: <?= $numerofactura ?> </h3></center></p>
	<ul class="nav nav-tabs nav-justified">
	    <li role="presentation"><a href="#debito" data-toggle="tab" id="nd_tab">Notas débito</a></li>
        <li role="presentation" ><a href="#credito" data-toggle="tab" id="nc_tab">Notas crédito</a></li>
	    <li role="presentation"><a href="#abono" data-toggle="tab" id="ab_tab">Abonos</a></li>
	</ul>
	<div style="height: 355px; width: 100%; overflow-y: auto; ">
    <div class="table-responsive tab-content"><!-- IMPORTANTE este div me ayuda a que la informacion en los tab mantengan su postura-->
    
        <div class="tab-pane fade" id="credito">
                <div class="col-lg-12">
                    <p><center><strong>Agregar notas de crédito</strong></center></p>
                    <div class="col-lg-12">
                        Detalle:<br>
                        <textarea id="detalle_nc" rows="2" class='form-control' onkeypress="if(this.value.length == 120){return false;}else{return toUpper(event,this);}"></textarea><br>
                    </div>
                    <div class="col-lg-6">
                        N°.NC Proveedor:<br>
                        <input id="numero_nc" type="number" name="numero_nc" class='form-control' onkeypress='return isNumberDe(event)' >
                    </div>
                    <div class="col-lg-6">
                        Monto:<br>
                        <?= MaskedInput::widget(['name' => 'monto_nc',
                            'options' => ['id'=>'monto_nc', 'class'=>'form-control',
                            'onchange'=>'javascript:agregar_nc()', "onkeypress"=>"return isNumberDe(event)"],
                            'clientOptions' => [
                                    'alias' =>  'decimal',
                                    'groupSeparator' => ',',
                                    'autoGroup' => true        
                                ]]) ?>
                        <p style="text-align:right">
                            <input id="" type="button" class="btn btn-link" onclick="agregar_nc()" value="Agregar" >
                        </p>
                    </div>
                    
                    <p style="text-align:right">
                        <?= Html::a('<span class=""></span>', ['/tramite-pago/imprimir_movimientos', 'idCompra'=>$idCompra, 'tipo_mov'=>'nc'], [
                            'class'=>'fa fa-print fa-2x',
                            'id' => 'activity-index-link-report',
                            //'onclick'=>'imprimir_nc('.$numerofactura.')',
                            'target'=>'_blank',
                            'data-toggle' => 'titulo',
                            //'data-url' => Url::to(['create']),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Imprimir movimientos'),
                            ]) ?>
                    </p>
                    <div id="notif_nc"></div>
                    
                </div>
                <div id="tbl_nc">
                <!-- con este div puedo refrescar tbl_nc al igual que llamarlo y montarlo en tiempo real-->
                    <?= $this->render('tbl_nc', [
                        'idCompra' => $idCompra,
                    ]) ?>
                </div>
        </div>
        <div class="tab-pane fade" id="debito">
            <div class="col-lg-12">
                <p><center><strong>Agregar notas de débito</strong></center></p>
                <div class="col-lg-12">
                    Detalle:<br>
                    <textarea id="detalle_nd" rows="2" class='form-control' onkeypress="if(this.value.length == 120){return false;}else{return toUpper(event,this);}"></textarea><br>
                </div>
                <div class="col-lg-6">
                    N°.ND Proveedor:<br>
                        <input id="numero_nd" type="number" name="numero_nd" class='form-control' onkeypress='return isNumberDe(event)' >
                </div>
                <div class="col-lg-6">
                    Monto:<br>
                    <?= MaskedInput::widget(['name' => 'monto_nd',
                            'options' => ['id'=>'monto_nd', 'class'=>'form-control',
                            'onchange'=>'javascript:agregar_nd()', "onkeypress"=>"return isNumberDe(event)"],
                            'clientOptions' => [
                                    'alias' =>  'decimal',
                                    'groupSeparator' => ',',
                                    'autoGroup' => true        
                                ]]) ?>
                    <p style="text-align:right">
                        <input id="" type="button" class="btn btn-link" onclick="agregar_nd()" value="Agregar" >
                    </p>
                </div>
                <p style="text-align:right">
                        <?= Html::a('<span class=""></span>', ['/tramite-pago/imprimir_movimientos', 'idCompra'=>$idCompra, 'tipo_mov'=>'nd'], [
                            'class'=>'fa fa-print fa-2x',
                            'id' => 'activity-index-link-report',
                            //'onclick'=>'imprimir_nc('.$numerofactura.')',
                            'target'=>'_blank',
                            'data-toggle' => 'titulo',
                            //'data-url' => Url::to(['create']),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Imprimir movimientos'),
                        ]) ?>
                </p>
                <div id="notif_nd"></div>
            </div>
            <div id="tbl_nd">
            <!-- con este div puedo refrescar tbl_nc al igual que llamarlo y montarlo en tiempo real-->
                <?= $this->render('tbl_nd', [
                    'idCompra' => $idCompra,
                ]) ?>
            </div>
        </div>
        <div class="tab-pane fade" id="abono">
            <div class="col-lg-12">
                <p><center><strong>Agregar abono a factura</strong></center></p>
                <div class="col-lg-12">
                    Detalle:<br>
                    <textarea id="detalle_ab" rows="2" class='form-control' onkeypress="if(this.value.length == 120){return false;}else{return toUpper(event,this);}"></textarea><br>
                </div>
                <div class="col-lg-6">
                    N°.AB Proveedor:<br>
                        <input id="numero_ab" type="number" name="numero_ab" class='form-control' onkeypress='return isNumberDe(event)' >
                </div>
                <div class="col-lg-6">
                    Monto:<br>
                    <?= MaskedInput::widget(['name' => 'monto_ab',
                        'options' => ['id'=>'monto_ab', 'class'=>'form-control',
                        'onchange'=>'javascript:agregar_ab()', "onkeypress"=>"return isNumberDe(event)"],
                        'clientOptions' => [
                                'alias' =>  'decimal',
                                'groupSeparator' => ',',
                                'autoGroup' => true        
                            ]]) ?>
                    <p style="text-align:right">
                        <input id="" type="button" class="btn btn-link" onclick="agregar_ab()" value="Agregar" >
                    </p>
                </div>
                
                <p style="text-align:right">
                    <?= Html::a('<span class=""></span>', ['/tramite-pago/imprimir_movimientos', 'idCompra'=>$idCompra, 'tipo_mov'=>'ab'], [
                        'class'=>'fa fa-print fa-2x',
                        'id' => 'activity-index-link-report',
                        //'onclick'=>'imprimir_nc('.$numerofactura.')',
                        'target'=>'_blank',
                        'data-toggle' => 'titulo',
                        //'data-url' => Url::to(['create']),
                        'data-pjax' => '0',
                        'title' => Yii::t('app', 'Imprimir movimientos'),
                        ]) ?>
                </p>
                <div id="notif_ab"></div>
                
            </div>
            <div id="tbl_ab">
            <!-- con este div puedo refrescar tbl_nc al igual que llamarlo y montarlo en tiempo real-->
                <?= $this->render('tbl_ab', [
                    'idCompra' => $idCompra,
                ]) ?>
            </div>
        </div>
    </div>
    </div>
    <!--div id="imprimir_mov"></div-->
</div>