<?php
use backend\models\Proveedores;
use backend\models\MovimientoPagar;

/* @var $this yii\web\View */
/* @var $model backend\models\FacturasDia */

?>
<?php 

if (@$proveedor = Proveedores::find()->where(['codProveedores'=>$factura_compra->idProveedor])->one()) {
    echo '<strong>Proveedor: </strong>'.$proveedor->nombreEmpresa.'<br>
      <strong>Teléfono: </strong>'.$proveedor->telefono.'<br>
      <strong>Contacto: </strong>'.$proveedor->nombreContacto.'<br>';
} 
$movimientofactura = MovimientoPagar::find()->where(['numFactura'=>$factura_compra->numFactura])->andWhere("tipo_mov = :tipo_mov", [":tipo_mov"=>$tipo_mov])->orderBy(['idMov' => SORT_DESC])->all(); 

?>


<div class="facturas-dia-view">
    <div class="panel panel-default"> 
        <!--div class="panel-heading">
            </div-->
            <div class="panel-body">
            <div class="col-lg-12">
                <?php
                        echo '<table class="items table table-striped">';
                        echo '<thead>';
                        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th></tr>',
                                'N° Mov',
                                'Tip.Mov',
                                'Detalle',
                                'Fec.Reg',
                                'Monto',
                                'Saldo'
                                );
                        echo '</thead>';

                        if($movimientofactura) {
                        echo '<tbody>';
                            foreach($movimientofactura as $position => $movi) { 
                                
                                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>',                    
                                        $movi['idMov'],
                                        $movi['tipo_mov'],
                                        $movi['detalle'],
                                        date('d-m-Y', strtotime( $movi['fecha_mov'] )), 
                                        number_format($movi['monto_movimiento'], 2),
                                        number_format($movi['saldo_pendiente'],2)                                          
                                        );     
                            }
                        echo '</tbody>';
                    }
                        echo '</table><div class="" >';
                        echo '</div>';
                ?>
            </div>
        </div>
    </div>

</div>
