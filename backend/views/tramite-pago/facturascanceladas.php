<?php
use backend\models\ComprasInventario;
use backend\models\TramitePago;
use backend\models\MedioPagoProveedor;
use backend\models\TipoDocumento;
use backend\models\CuentasBancarias;
use backend\models\CuentasBancariasProveedor;
use backend\models\ContactosPagos;
use backend\models\MovimientoPagar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

	//obtengo instancia de compras de inventario para saber cuantas facturas de estre proveedor estan en proceso
	$facturasCanceladas = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])
    ->andWhere("estado_pago = :estado_pago", [":estado_pago"=>'Cancelada'])
    ->andFilterWhere(['like', 'numFactura', $numFactura ])
    ->andFilterWhere(['like', 'formaPago', $formaPago ])
    ->andFilterWhere(['like', 'fechaRegistro', $fechaRegistro ])
    ->andFilterWhere(['like', 'fechaVencimiento', $fechaVencimiento ])
    ->orderBy(['idCompra' => SORT_DESC])->limit(200)->all();


?>
<style>
	#modalFactura3 .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
</style>
<script type="text/javascript">
	//Modal que me muestra la factura de facturas canceladas
    $(document).on('click', '#activity-index-link-factura_3', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $( "#mostrar_factura_modal3" ).html( data );
                        $('#modalFactura3').modal();
                    }
                );
            }));
    //Modal que me muestra los movimientos de la factura
    $(document).on('click', '#activity-index-link-movimiento-fc', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $( "#mostrar_movimiento_modal" ).html( data );
                        $('#modalMovimientoFC').modal();
                    }
                );
            }));
</script>
<div class="panel panel-info">
    <div class="panel-heading">
        Facturas canceladas
    </div>
    <div class="panel-body" id="facturas_canceladas">

            <?php
            	echo '<div class="col-lg-12"><div class="well">
			<table class="items table table-striped" id="tabla_facturas_pendientes"  >';
                echo '<thead class="thead-inverse">';
                printf('<tr>
                    <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th style="text-align:right"><font face="arial" size=1>%s</font></th>
                    <th class="actions button-column">&nbsp;</th></tr>',
                        'N° ENTRADA',
                        'N° FACTURA',
                        'PROCESO',
                        'FECH.REG',
                        'FECH.VEN',
                        'MONTO',
                        'NOTA DÉBITO',
                        'NOTA CRÉDITO',
                        'ABONO',
                        'SALDO FACTURA'
                        );
                echo '</thead>';
                echo '<tbody class="buscar1">';
                foreach($facturasCanceladas as $position => $factura) {
                	$idCompra = "'".$factura['idCompra']."'";
                    $command_nc = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nc' AND idCompra = " . $idCompra);
                    $command_nd = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'nd' AND idCompra = " . $idCompra);
                    $command_ab = Yii::$app->db->createCommand("SELECT sum(monto_movimiento) FROM tbl_movi_pagar WHERE tipo_mov = 'ab' AND idCompra = " . $idCompra);
                    $suma_monto_nc = floatval($command_nc->queryScalar()); //ingreso la suma del monto de notas de credito
                    $suma_monto_nd = floatval($command_nd->queryScalar()); //ingreso la suma del monto de notas de debito
                    $suma_monto_ab = floatval($command_ab->queryScalar()); //ingreso la suma del monto de abonos

                    $fecha_registro = date('d-m-Y', strtotime( $factura['fechaRegistro'] ));
                    $fecha_vencimiento = $factura['formaPago'] == 'Contado' ? '-' : date('d-m-Y', strtotime( $factura['fechaVencimiento'] ));
                    $mostrarFactura = Html::a('', ['#'], [
                                    'class'=>'glyphicon glyphicon-search',
                                    'id' => 'activity-index-link-factura_3',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalFactura3',
                                    'data-url' => Url::to(['tramite-pago/vista_factura', 'id' => $factura['idCompra']]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Procede a mostrar la factura')]);
                    $mostrarMovimiento = Html::a('', ['#'], [
                                    'class'=>'glyphicon glyphicon-indent-left',
                                    'id' => 'activity-index-link-movimiento-fc',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalMovimientoFC',
                                    'data-url' => Url::to(['tramite-pago/vista_movimientos', 'id' => $factura['idCompra']]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Procede a mostrar los movimientos de la compra-factura')]);
                    printf('<tr>
                            <td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=3>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                                $factura['idCompra'],
                                $factura['numFactura'],
                                $factura['formaPago'],
                                $fecha_registro,
                                $fecha_vencimiento,
                                number_format($factura['total'],2),
                                number_format($suma_monto_nd,2),
                                number_format($suma_monto_nc,2),
                                number_format($suma_monto_ab,2),
                                number_format($factura['credi_saldo'],2),
                                $mostrarFactura . ' ' . $mostrarMovimiento
                                );
                }
                echo '</tbody>';
        echo '</table>
        </div></div>';
            ?>

    </div>
</div>

<?php
//-------------------------------------------------Pantallas modales-----------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------
    //Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalFactura3',
                //'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Compra / Factura</h4></center>',
                'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='panel-body'><div id='mostrar_factura_modal3'></div></div>";

            Modal::end();

    //Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalMovimientoFC',
                'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Movimientos / Factura</h4></center>',
                'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='panel-body'><div id='mostrar_movimiento_modal'></div></div>";

            Modal::end();
?>
