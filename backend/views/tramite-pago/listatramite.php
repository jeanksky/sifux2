<?php
use backend\models\TramitePago;
use backend\models\ComprasInventario;
use yii\bootstrap\Modal;
?>
<style>
	/* clase para ocultar el div al inicio */
	.oculto {
	display:none;
	margin-left:1em;
	margin-right:1em;
	}

	#modalFactura2 .modal-dialog{
    width: 60%!important;
    /*margin: 0 auto;*/
    }
    /*colorear ubicacion al pasar el cursor en la lineas inpares color azul*/
    /*tbody tr:nth-child(even):hover {
        background-color: #fff;
        }*/
	.info_tramite{
		height: 1vh !important;/*El mismo alto para todas las celdas**/
	}
</style>

<script type="text/javascript">
	<!--
	/*****************************Ocultar div**************************/
	var visto = null;
	function ver(num) {
		obtenerfacturasdetramite(num);
		obj = document.getElementById(num);
		obj.style.display = (obj==visto) ? 'none' : 'block';
		if (visto != null)
		visto.style.display = 'none';
		visto = (obj==visto) ? null : obj;
	}

	//agrega en el div las facturas del tramite correspondiente
	function obtenerfacturasdetramite(idtramite) {
			document.getElementById(idtramite).innerHTML="";//tengo que destruir el contenido del div de
			$.get( "<?= Yii::$app->getUrlManager()->createUrl('tramite-pago/obtenerfacturasdetramite') ?>" , { 'idtramite' : idtramite } ,
            function( data ) {
              $('#'+idtramite).html(data);
            });
		}

	//Modal que me muestra la factura de facturas pendientes
    $(document).on('click', '#activity-index-link-factura_2', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        //$('.modal-body').html(data);
                        $( "#mostrar_factura_modal2" ).html( data );
                        $('#modalFactura2').modal();
                    }
                );
            }));
	-->
</script>
<!--http://www.forosdelweb.com/f4/ocultar-mostrar-filas-tabla-1033328/-->
<div class="panel panel-info">
    <div class="panel-heading">
        Lista de Trámites
    </div>
    <div class="panel-body" id="lista_tramite">
        <div style="height: 500px;width: 100%; overflow-y: auto; ">
        <?php

        	$tramites = TramitePago::find()->where(['=','idProveedor', $idProveedor])->orderBy(['idTramite_pago' => SORT_DESC])->limit(20)->all();
        	/*if (@$fecha_reg_tramite) {
        		$tramites = TramitePago::find()->where(['=','idProveedor', $idProveedor])->andWhere("fecha_registra = :fecha_registra", [":fecha_registra"=>$fecha_reg_tramite ])->all();
        	} else*/
        	if (@$factura) {
        			$factura_tramite = ComprasInventario::find()->where(['=','idProveedor', $idProveedor])->andWhere("numFactura = :numFactura", [":numFactura"=>$factura ])->one();
        			$tramites = TramitePago::find()->where(['=','idProveedor', $idProveedor])->andWhere("idTramite_pago = :idTramite_pago", [":idTramite_pago"=>$factura_tramite->idTramite_pago ])->all();
        	} else {
        			if (@$prioridad_pago && @$estado_tramite) {
    	        		$tramites = TramitePago::find()->where(['=','idProveedor', $idProveedor])
    	        		->andWhere("prioridad_pago = :prioridad_pago and estado_tramite = :estado_tramite", [":prioridad_pago"=>$prioridad_pago,":estado_tramite"=>$estado_tramite])->orderBy(['idTramite_pago' => SORT_DESC])->limit(20)->all();
    	        	}else if (@$prioridad_pago) {
    	        		$tramites = TramitePago::find()->where(['=','idProveedor', $idProveedor])->andWhere("prioridad_pago = :prioridad_pago", [":prioridad_pago"=>$prioridad_pago])->orderBy(['idTramite_pago' => SORT_DESC])->limit(20)->all();
    	        	} else if (@$estado_tramite) {
    	        		$tramites = TramitePago::find()->where(['=','idProveedor', $idProveedor])->andWhere("estado_tramite = :estado_tramite", [":estado_tramite"=>$estado_tramite])->orderBy(['idTramite_pago' => SORT_DESC])->limit(20)->all();
    	        	} else if (@$fecha_reg_tramite) {
	        			$tramites = TramitePago::find()->where(['=','idProveedor', $idProveedor])->andWhere("fecha_registra >= :fecha_registra", [":fecha_registra"=>$fecha_reg_tramite ])->limit(20)->all();
	        		}
    	        }
/*
        	 */


        echo '<table class = "table table-bordered">
				<thead >
	        		<tr>
	        			<th>ID</th>
	        			<th>Cant.Fact</th>
	        			<th>Monto</th>
	        			<th>Saldo</th>
	        			<th>% descto</th>
	        			<th>Monto a pagar</th>
	        			<th>Usuar.Reg</th>
	        			<th>Fech.Reg</th>
	        			<th>Prioridad</th>
	        			<th>Estado</th>
	        		</tr>
	        	</thead>
	            <tbody>';
	    foreach($tramites as $position => $tramite) {
	    	$fecha_registra = date('d-m-Y', strtotime( $tramite['fecha_registra'] ));
	    	$facturas_tramite = ComprasInventario::find()->where(['=','idTramite_pago', $tramite['idTramite_pago']])->all();
	    	$cantidad_factura = 0;
	    	foreach ($facturas_tramite as $key) { $cantidad_factura += 1; }
	    	echo '
					<tr style="cursor:pointer" onclick="ver('.$tramite['idTramite_pago'].')" >
						<td align="center"> '.$tramite['idTramite_pago'].' </td>
						<td> '.$cantidad_factura.' </td>
						<td align="right"> '.number_format($tramite['cantidad_monto_documento'],2).' </td>
						<td align="right"> '.number_format($tramite['monto_saldo_pago'],2).' </td>
						<td align="center"> '.$tramite['porcentaje_descuento'].'% </td>
						<td align="right"> '.number_format($tramite['monto_tramite_pagar'],2).' </td>
						<td align="center"> '.$tramite['usuario_registra'].' </td>
						<td align="center"> '.$fecha_registra.' </td>
						<td align="center"> '.$tramite['prioridad_pago'].' </td>
						<td align="center"> '.$tramite['estado_tramite'].' </td>
					</tr>
					<tr  >
						<td colspan="10" > <div id="'.$tramite['idTramite_pago'].'" class="oculto">Esperando facturas...</div></td>
					</tr>';
				}
				echo '
				</tbody>
			  </table>';
        ?>

        </div>
    </div>
</div>
<?php
//-------------------------------------------------Pantallas modales-----------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------
    //Muestra una modal de la factura seleccionada
         Modal::begin([
                'id' => 'modalFactura2',
                //'size'=>'modal-lg',
                'header' => '<center><h4 class="modal-title">Compra / Factura</h4></center>',
                'footer' => '<a href="" class="btn btn-primary" data-dismiss="modal">Salir</a>'/*.Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['/facturas-dia/report'], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Genera un PDF de la factura para imprimir'
                ]),*/
            ]);
            echo "<div class='panel-body'><div id='mostrar_factura_modal2'></div></div>";

            Modal::end();
?>
