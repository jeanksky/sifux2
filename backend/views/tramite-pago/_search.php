<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\TramitePagoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tramite-pago-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idTramite_pago') ?>

    <?= $form->field($model, 'idProveedor') ?>

    <?= $form->field($model, 'cantidad_saldo_documento') ?>

    <?= $form->field($model, 'monto_tramite_pago') ?>

    <?= $form->field($model, 'procentaje_descuento') ?>

    <?php // echo $form->field($model, 'monto_pagar') ?>

    <?php // echo $form->field($model, 'recibo_cancelacion') ?>

    <?php // echo $form->field($model, 'email_proveedor') ?>

    <?php // echo $form->field($model, 'prioridad_pago') ?>

    <?php // echo $form->field($model, 'usuario_registra') ?>

    <?php // echo $form->field($model, 'fecha_registra') ?>

    <?php // echo $form->field($model, 'usuario_cancela') ?>

    <?php // echo $form->field($model, 'fecha_cancela') ?>

    <?php // echo $form->field($model, 'usuario_aplica') ?>

    <?php // echo $form->field($model, 'fecha_aplica') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
