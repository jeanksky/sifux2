<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\DetalleCompra;
use backend\models\ProductoServicios;
use backend\models\Proveedores;
use backend\models\Usuarios;

/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */


$products = DetalleCompra::find()->where(["=",'idCompra', $model->idCompra])->all();
$provee = Proveedores::findOne($model->idProveedor);
?>
<div class="compras-inventario-view">

   
<div class="col-lg-12">
	<div class="col-lg-6"><strong>Proveedor: </strong><?= $provee->nombreEmpresa ?></div>
	<div class="col-lg-6"><div class="pull-right"><strong>No Factura: </strong><?= $model->numFactura ?></div></div>
</div>
<div class="col-lg-12"><br>
	<div class="col-lg-4"><strong>Fecha Documento: </strong><?= $model->fechaDocumento ?></div>    
	<div class="col-lg-4"><strong>Fecha Registro: </strong><?= $model->fechaRegistro ?></div>
	<div class="col-lg-4"><strong>Fecha Vencimiento: </strong><?= $model->fechaVencimiento ?></div>
</div>
<div class="col-lg-12"><br>
	<div class="col-lg-3"><strong>Subtotal: </strong><?= number_format($model->subTotal,2) ?></div>
	<div class="col-lg-3"><strong>Descuento: </strong><?= number_format($model->descuento,2) ?></div>
	<div class="col-lg-3"><strong>Impuesto: </strong><?= number_format($model->impuesto,2) ?></div> 
	<div class="col-lg-3"><strong>Total: </strong><?= number_format($model->total,2) ?></div>
</div>
    
<div class="col-lg-12" id="conten_prom">
            <br>
            <?php
            //http://www.programacion.com.py/tag/yii
                //echo '<div class="grid-view">';
                echo '<table class="items table table-striped">';
                echo '<thead>';
                printf('<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr>',
                        'CÓDIGO',
                        'PRODUCTO',
                        'CANT',
                        'PREC. COMPRA',
                        'PREC. VENTA',
                        'PREC. VENT+IMP'
                        );
                echo '</thead>';
            //if($products) {
            ?>
            <?php
            if($products) {
            echo '<tbody>';
                foreach($products as $position => $product) { 
                $nueva_cantidad = $product['cantidadAnterior'] + $product['cantidadEntrada'];
                $modelpro = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',                    
                            //$product['noEntrada'],
                            $product['codProdServicio'],
                            $modelpro->nombreProductoServicio,
                            $product['cantidadEntrada'],
                            number_format($product['precioCompra'],2),
                            number_format($product['preciosinImp'],2),
                            //number_format($product['precioCompra'],2),
                            //$product['descuento'],
                            //$product['porcentUtilidad'],
                            //number_format($product['preciosinImp'],2),
                            //number_format($product['precioconImp'],2),
                            number_format($product['precioconImp'],2)
                            );
                        //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/      
                       
                                
                }
            echo '</tbody>';
        }
            ?>
            <?php
            //}
                echo '</table>';
            ?>
            </div>
</div>
