<script type="text/javascript">
	$('[data-toggle="popover_nc_"]').popover();//me dice el tipo de popover que quiero correspondiente al id
	$('[data-toggle="popover_nd_"]').popover();//me dice el tipo de popover que quiero correspondiente al id
	$('[data-toggle="popover_ab_"]').popover();//me dice el tipo de popover que quiero correspondiente al id
</script>
<?php 
use yii\helpers\Html;
use backend\models\MovimientoPagar;

if (@$model_mov->idCompra) {
	$notas_debito = MovimientoPagar::find()->where(['idCompra'=>$model_mov->idCompra])->andWhere("tipo_mov = :tipo_mov", [":tipo_mov"=>'nd'])->orderBy(['idMov' => SORT_DESC])->all();
	$notas_credito = MovimientoPagar::find()->where(['idCompra'=>$model_mov->idCompra])->andWhere("tipo_mov = :tipo_mov", [":tipo_mov"=>'nc'])->orderBy(['idMov' => SORT_DESC])->all();
	$abonos = MovimientoPagar::find()->where(['idCompra'=>$model_mov->idCompra])->andWhere("tipo_mov = :tipo_mov", [":tipo_mov"=>'ab'])->orderBy(['idMov' => SORT_DESC])->all();
	$suma_debito = 0;
	$suma_credito = 0;
	$suma_abono = 0;
	foreach($notas_debito as $position => $movi) { $suma_debito += $movi['monto_movimiento']; }
	foreach($notas_credito as $position => $movi) { $suma_credito += $movi['monto_movimiento']; }
	foreach($abonos as $position => $movi) { $suma_abono += $movi['monto_movimiento']; }


	echo '<div class="col-lg-4">
				<div class="panel panel-success">
				    <div class="panel-heading">
				        Notas débito <span style="float:right">'.number_format($suma_debito,2).'</span>
				    </div>
				    <div class="panel-body">';
				    echo '<div style="height: 150px; width: 100%; overflow-y: auto; ">';
					    echo '<table class="items table table-striped" id="tabla_facturas_proceso"  >';
					    echo '<thead>';
					    printf('<tr>
					        <th><font face="arial" size=1>%s</font></th>
					        <th style="text-align:right"><font face="arial" size=1>%s</font></th>
					        <th class="actions button-column"></th></tr>',
					            'N° NOTA',
					            'MONTO'
					            );
					    echo '</thead>';
					    echo '<tbody>';
					    foreach($notas_debito as $position => $movi) { 
					        $info_nc = 'Fecha: '.date('d-m-Y', strtotime($movi['fecha_mov'])).'<br>
					                    Monto anterior: '.number_format($movi['monto_anterior'],2).'<br>
					                    Monto movimiento: '.number_format($movi['monto_movimiento'],2).'<br>
					                    Saldo pendiente: '.number_format($movi['saldo_pendiente'],2).'<br>
					                    Usuario: '.$movi['usuario'].'<br>
					                    Detalle: '.$movi['detalle'];
					        $jtimes_view = "$('#activity-info-movimiento".$movi['idMov']."').popover('hide')";//scrip oculta view nc
					       
					        $consultar = Html::a('', null, [
					                        'class'=>'glyphicon glyphicon-search',
					                        'id' => 'activity-info-movimiento'.$movi['idMov'],
					                        'data-toggle' => 'popover_nd_',
					                        'data-placement' => 'left',
					                        'data-html'=>'true',
					                        'data-content'=>$info_nc,
					                        'title' => Yii::t('app', 'Movimiento #'.$movi['idMov']).'&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
										        <i onclick="'.$jtimes_view.'" class="fa fa-times fa-1x" data-placement="bottom" ></i>
										    ' ]);
					        
					        printf('<tr>
					                    <td><font face="arial" size=2>%s</font></td>
					                    <td align="right"><font face="arial" size=2>%s</font></td>
					                    <td class="actions button-column">%s</td></tr>',
					                $movi['idMov'],
					                number_format($movi['monto_movimiento'],2),
					                $consultar
					            );
					    }
					    echo '</tbody>';
					    echo '</table>';
				    echo '</div>';

	echo 		    '</div>
				</div>
		  </div>
		  <div class="col-lg-4">
				<div class="panel panel-success">
				    <div class="panel-heading">
				        Notas Crédito <span style="float:right">'.number_format($suma_credito,2).'</span>
				    </div>
				    <div class="panel-body">';
				    	echo '<div style="height: 150px; width: 100%; overflow-y: auto; ">';
					    echo '<table class="items table table-striped" id="tabla_facturas_proceso"  >';
					    echo '<thead>';
					    printf('<tr>
					        <th><font face="arial" size=1>%s</font></th>
					        <th style="text-align:right"><font face="arial" size=1>%s</font></th>
					        <th class="actions button-column"></th></tr>',
					            'N° NOTA',
					            'MONTO'
					            );
					    echo '</thead>';
					    echo '<tbody>';
					    foreach($notas_credito as $position => $movi) { 
					        $info_nc = 'Fecha: '.date('d-m-Y', strtotime($movi['fecha_mov'])).'<br>
					                    Monto anterior: '.number_format($movi['monto_anterior'],2).'<br>
					                    Monto movimiento: '.number_format($movi['monto_movimiento'],2).'<br>
					                    Saldo pendiente: '.number_format($movi['saldo_pendiente'],2).'<br>
					                    Usuario: '.$movi['usuario'].'<br>
					                    Detalle: '.$movi['detalle'];
					        $jtimes_view = "$('#activity-info-movimiento".$movi['idMov']."').popover('hide')";//scrip oculta view nc
					       
					        $consultar = Html::a('', null, [
					                        'class'=>'glyphicon glyphicon-search',
					                        'id' => 'activity-info-movimiento'.$movi['idMov'],
					                        'data-toggle' => 'popover_nc_',
					                        'data-placement' => 'left',
					                        'data-html'=>'true',
					                        'data-content'=>$info_nc,
					                        'title' => Yii::t('app', 'Movimiento #'.$movi['idMov']).'&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
										        <i onclick="'.$jtimes_view.'" class="fa fa-times fa-1x" data-placement="bottom" ></i>
										    ' ]);
					        
					        printf('<tr>
					                    <td><font face="arial" size=2>%s</font></td>
					                    <td align="right"><font face="arial" size=2>%s</font></td>
					                    <td class="actions button-column">%s</td></tr>',
					                $movi['idMov'],
					                number_format($movi['monto_movimiento'],2),
					                $consultar
					            );
					    }
					    echo '</tbody>';
					    echo '</table>';
				    echo '</div>';
	echo		    '</div>
				</div>
		  </div>
		  <div class="col-lg-4">
				<div class="panel panel-success">
				    <div class="panel-heading">
				        Abonos <span style="float:right">'.number_format($suma_abono,2).'</span>
				    </div>
				    <div class="panel-body">';
				    	echo '<div style="height: 150px; width: 100%; overflow-y: auto; ">';
					    echo '<table class="items table table-striped" id="tabla_facturas_proceso"  >';
					    echo '<thead>';
					    printf('<tr>
					        <th><font face="arial" size=1>%s</font></th>
					        <th style="text-align:right"><font face="arial" size=1>%s</font></th>
					        <th class="actions button-column"></th></tr>',
					            'N° NOTA',
					            'MONTO'
					            );
					    echo '</thead>';
					    echo '<tbody>';
					    foreach($abonos as $position => $movi) { 
					        $info_nc = 'Fecha: '.date('d-m-Y', strtotime($movi['fecha_mov'])).'<br>
					                    Monto anterior: '.number_format($movi['monto_anterior'],2).'<br>
					                    Monto movimiento: '.number_format($movi['monto_movimiento'],2).'<br>
					                    Saldo pendiente: '.number_format($movi['saldo_pendiente'],2).'<br>
					                    Usuario: '.$movi['usuario'].'<br>
					                    Detalle: '.$movi['detalle'];
					        $jtimes_view = "$('#activity-info-movimiento".$movi['idMov']."').popover('hide')";//scrip oculta view nc
					       
					        $consultar = Html::a('', null, [
					                        'class'=>'glyphicon glyphicon-search',
					                        'id' => 'activity-info-movimiento'.$movi['idMov'],
					                        'data-toggle' => 'popover_ab_',
					                        'data-placement' => 'left',
					                        'data-html'=>'true',
					                        'data-content'=>$info_nc,
					                        'title' => Yii::t('app', 'Movimiento #'.$movi['idMov']).'&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
										        <i onclick="'.$jtimes_view.'" class="fa fa-times fa-1x" data-placement="bottom" ></i>
										    ' ]);
					        
					        printf('<tr>
					                    <td><font face="arial" size=2>%s</font></td>
					                    <td align="right"><font face="arial" size=2>%s</font></td>
					                    <td class="actions button-column">%s</td></tr>',
					                $movi['idMov'],
					                number_format($movi['monto_movimiento'],2),
					                $consultar
					            );
					    }
					    echo '</tbody>';
					    echo '</table>';
				    echo '</div>';
	echo		    '</div>
				</div>
		  </div>';



} else {
	echo '<div class="alert alert-info" role="alert">
			  Esta factura no cuenta con ningun movimiento
		  </div>';
}

 ?>
