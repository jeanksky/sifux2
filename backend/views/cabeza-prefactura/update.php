
<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use backend\models\Empresa;
use backend\models\Compra;
use backend\models\Compra_update;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use yii\bootstrap\Modal;
use backend\models\FormasPago;
use backend\models\Clientes;
use backend\models\ProductoServicios;
use backend\models\Moneda;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\jui\DatePicker;
use backend\models\FacturaOt;
$empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
$caja = \Yii::$app->request->BaseUrl.'/js/vistas/cabeza_caja/caja.js';
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $model backend\models\EncabezadoPrefactura */
$estado = Compra_update::getEstadoFacturau();
$moneda_local = '°';
  $this->title = 'Atención a Prefactura: ID ' . ' ' . $model->idCabeza_Factura;
  if ($estado=='Caja' || $estado=='Crédito Caja') {
    $this->title = 'Atención a Factura: ID ' . ' ' . $model->idCabeza_Factura;
     $this->params['breadcrumbs'][] = ['label' => 'Caja (cancelación de facturas pendientes)', 'url' => ['regresarborrar-caja']];
     $moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
     if (@$moneda) {
      $moneda_local = $moneda->simbolo;
     }
  } else if($estado=='Apartado'){
    $this->params['breadcrumbs'][] = ['label' => 'Lista de Prefacturas', 'url' => ['regresarborrar-u']];
  }else{
    $this->params['breadcrumbs'][] = ['label' => 'Lista de Prefacturas', 'url' => ['regresarborrar-u']];
  }

$this->params['breadcrumbs'][] = ['label' => $model->idCabeza_Factura/*, 'url' => ['view', 'id' => $model->idCabeza_Factura]*/];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<?php $products = Compra_update::getContenidoComprau(); ?>
<?php $vendedor = Compra_update::getVendedoru(); ?>
<?php $cliente = Compra_update::getClienteu(); ?>
<?php $pago = Compra_update::getPagou(); ?>
<?php Compra_update::setTotalauxiliar($model->total_a_pagar); ?>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
function bloquear_btn(this_boton) {
  var $btn = $(this_boton).button('loading');
}
//Funcion que hace abrir modal desde la tecla[F2]
    window.onkeydown = tecla;
      function tecla(event) {
        //event.preventDefault();
        num = event.keyCode;

        if(num==113){
          $('#modalinventario').modal('show');//muestro la modal
          //$("#busqueda_cod").focus();
        }

      }
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function(){
   // $('[data-toggle="modal"]').tooltip();
    $('[data-toggle="modal"]').popover();
});

//espacio que solo permite numero y decimal (.)
function isNumberDe(evt) {
  var nav4 = window.Event ? true : false;
  var key = nav4 ? evt.which : evt.keyCode;
  return (key <= 13 || key==46 || (key >= 48 && key <= 57));
}

function obtenerClienteCod(){

        var idCliente = document.getElementById("cod-cliente").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/cliente2-u') ?>",
            type:"post",
            data: {'idCliente': idCliente},
        }) ;
    }

function obtenerCliente(){

    var idCliente = document.getElementById("ddl-cliente").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/cliente2-u') ?>",
        type:"post",
        data: {'idCliente': idCliente},
        /*success: function(data){
         $('#resultado').val(data);
        },
        error: function(msg, status,err){
                        $('#resultado').val('Dato no encontrado');
                    }*/
    }) ;
}

function run_pago(){
    var tipoFacturacion = document.getElementById("tipoFacturacion").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addpago-u') ?>",
        type:"post",
        data: {'tipoFacturacion': tipoFacturacion},
    }) ;
}

/*Agrega el agente comision*/
        function run_AgenteComisionu(){
        var AgenteComi = document.getElementById("idAgenteComision").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addagentecomision-u') ?>",
            type:"post",
            data: {'idAgenteComision': AgenteComi},
        }) ;
    }

//activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
        $("#correo_cliente").prop('disabled', true);//mantengo inhavilidado el espacio de correo (direccion que se usa para enviar la factura por correo)
    });

   //cargo automatico para los botones de pago para la cancelacion o acteditacion de la factura
    function cargo() {
        $('#rebotons').html('<img src="<?php echo $load_buton; ?>" style="height: 32px;">');
    }
    setTimeout('cargo()');
    //window.onload = cargo;
    //me permite buscar por like los productos a BD
    function buscar() {
        var textoBusqueda_cod = $("input#busqueda_codi").val();
        var textoBusqueda_des = $("input#busqueda_des").val();
        var textoBusqueda_fam = $("input#busqueda_fam").val();
        var textoBusqueda_cat = $("input#busqueda_cat").val();
        var textoBusqueda_cpr = $("input#busqueda_cpr").val();
        var textoBusqueda_ubi = $("input#busqueda_ubi").val();
        if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
            $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');

        } else {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/busqueda_producto') ?>",
                type:"post",
                data: {
                    'valorBusqueda_cod' : textoBusqueda_cod,
                    'valorBusqueda_des' : textoBusqueda_des,
                    'valorBusqueda_fam' : textoBusqueda_fam,
                    'valorBusqueda_cat' : textoBusqueda_cat,
                    'valorBusqueda_cpr' : textoBusqueda_cpr,
                    'valorBusqueda_ubi' : textoBusqueda_ubi },
                beforeSend : function() {
                  $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 286');
                }
            });
        }
    }

    //Transformar a mayúsculas todos los caracteres
    function Mayuculas(key, tx){
      if(key == 13 && tx=='') {//si la tecla que toco es enter y el inout está vacio entonces...
        document.getElementById("facturar").click();//...que proceda a facturar
      }
      //Retornar valor convertido a mayusculas
    	return tx.toUpperCase();
    }

    //Funcion para que me permita ingresar solo numeros
    function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58))
                return true;
            return false;
        }

function agrega_producto(codProdServicio){
    //var codProdServicio = document.getElementById("codProdServiciou").value;
    id = '<?= $model->idCabeza_Factura ?>';
    var descuento = $("#descuentou").val();
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/additemu') ?>",
        type:"post",
        data: { 'codProdServicio': String(codProdServicio), 'cantidad': 1, 'descuento' : descuento , 'idCabeza' : "<?php echo $model->idCabeza_Factura ?>"},
        success: function(data){
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle_update') ?>", {'id' : id },
                        function( data ) {
                          $('#modalinventario').modal('hide');
                          $('#detalle').html(data);
                          $('#descuentou').val('').change();
                          $('#codProd').val(null);

                          $("input#busqueda_codi").val(null);
                          $("input#busqueda_des").val(null);
                          $("input#busqueda_ubi").val(null);
                          activar_input_ingreso_producto();
                        });
        //$("#codProdServiciou").select2("val", ""); //vaciar contenido de select2
        //location.reload();
        //$("#conten_pro").load(location.href+' #conten_pro',''); //refrescar el div
        //document.getElementById("alerta_r").innerHTML = '<div class="alert alert-info alert-dismissable">                      <button type="button" class="close" data-dismiss="alert">&times;</button><strong>¡Cuidado!</strong> Es muy importante que leas este mensaje de alerta.</div>';
        },
        error: function(msg, status,err){
                        //alert('no pasa');
                        //$("#conten_pro").load(location.href+' #conten_pro',''); //refrescar el div
                        //$("#conten_pro").load(location.href+' #conten_pro', {id:id});
                        //location.reload();
                    }
    }) ;
}

function activar_input_ingreso_producto() {
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addpro') ?>",
    function( data_prod ) {
      //agregamos nuevo input para agregar productos
      document.getElementById("addpro").innerHTML = data_prod;
      //activamos el cursor para ingresar el producto
      document.getElementById("codProd").select();
    });
}
//setTimeout('obtenerCliente()', 500);

function obtenerVendedor(){

    var idFuncionario = document.getElementById("ddl-codigoVendedor").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/vendedor2-u') ?>",
        type:"post",
        data: {'idFuncionario': idFuncionario},
        success: function(data){
         $('#resultado2').val(data);
        },
        error: function(msg, status,err){
                        $('#resultado2').val('Dato no encontrado');
                    }
    }) ;
}
//setTimeout('obtenerVendedor()', 500);
function refrescarbotones(){
    $("#rebotons").load(location.href+' #rebotons',''); //refrescar el div
}
setTimeout('refrescarbotones()');

//funciones para la cancelacion de facturas--------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//funcion para optener el vuelto por el pago
function obtenerPago(key,valor,id){
    if(key == 13) {
      document.getElementById("boton-cancelar").click();
    }
    //var total = "<?php //echo $model->total_a_pagar ?>";
    //document.getElementById("vuelto").innerHTML = '₡'+parseFloat(valor - total).toFixed(2);
    var dollar = 'no';
    $('.checkdolar').each( //con esto compruebo si el valor es dolar
      function() {
      if ($(this).is(':checked'))//Este if me dice que si esta marcado es porque el valor ingresado es dolar
        {
          dollar = 'si';
        }
    });
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/addpago') ?>",
        type:"post",
        dataType: 'json',
        data: {'valor': valor, 'dollar' : dollar, 'id' : id},
        success: function(data){
            document.getElementById("vuelto").innerHTML = data.resultado_simbolo;
            document.getElementById("valorencolones").innerHTML = data.valorencolones;
            $('#vueltoinput').val(data.resultado);
        }
    }) ;
}

//obtener el vuelto por pago al hacer check en dolar o moneda extrangera
$(document).ready(function () {
  $('.checkdolar').on('click', function() {
      var valor = document.getElementById("pago").value;
      var dollar = 'no';
      if (this.checked == true)
          dollar = 'si';
      else
          dollar = 'no';
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/addpago') ?>",
          type:"post",
          dataType: 'json',
          data: {'valor': valor, 'dollar' : dollar, 'id' : '<?= $model->idCabeza_Factura ?>'},
          success: function(data){
              document.getElementById("vuelto").innerHTML = data.resultado_simbolo;
              document.getElementById("valorencolones").innerHTML = data.valorencolones;
              $('#vueltoinput').val(data.resultado);
              $("#autofocus").slideDown(function(){
                  $("#pago").focus();
              });
          }
      }) ;
  });

//Funcion para obtener el correo del cliente (si es un cliente registrado)
/*  $('.checkcorreo').on('click', function() {
      var idCliente = '<!--?= $model->idCliente ?-->';
      if (this.checked == true)
          {
            $.ajax({
                url:"<!--?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/obtener_correo') ?-->",
                type:"post",
                data: { 'idCliente': idCliente },
                success: function(data){
                    $('#correo_cliente').val(data);
                    $("#correo_cliente").prop('disabled', false);
                    $("#div_correo_cliente").slideDown(function(){
                        $("#correo_cliente").focus();
                    });
                }
            }) ;
          }
      else
          {
            $('#correo_cliente').val('');
            $("#correo_cliente").prop('disabled', true);
            $("#autofocus").slideDown(function(){
                $("#pago").focus();
            });
          }

  });*/

});

//obtiene el medio para enviarlo al controlador y almacenarlo en session, la entidad financiera.
function obtenMedio(valor, tipo)
{
   // document.getElementById("resultado1").innerHTML=valor;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addmedio-u') ?>",
        type:"post",
        data: {'medio': valor},
    }) ;
    compruebatipopago(tipo)
}
//Obtiene el comprobante para enviarlo al controlador y almacenarlo ensession
function obtenCom(valor, tipo)
{
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addcom-u') ?>",
            type:"post",
            data: {'comprobacion': valor},
        });
        compruebatipopago(tipo)
   // document.getElementById("resultado1").innerHTML=valor;
}
//me matiene bloqueado el boton de cancelar factura hasta que se de la orden
    $(document).ready(function () {
        ddl_entidad = document.getElementById("ddl-entidad").value;
        if (ddl_entidad == "efectivo") {
            $('#boton-cancelar').removeClass('disabled');
        } else
        $('#boton-cancelar').addClass('disabled');
    });
//envia datos a controlador para cargar cuenta bancaria en session y
//desabilita el campo de comprovacion si la entidad y la cuenta estan completas
function obtenCuenta(valor, tipo) {
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addcuenta-u') ?>",
            type:"post",
            data: {'cuenta': valor},
        });
        compruebatipopago(tipo);
}
//obtengo la fecha de deposito  y la envio al controlador para ser almacenadaen session
function obtenFecha(valor) {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addfechadepo-u') ?>",
        type:"post",
        data: {'fechadepo': valor},
    });
}
//compruebo que los datos estan llenos a la hora de cancelas de acuerdo al tipo de pago para abilitar o no el boton
function compruebatipopago(tipo) {

    var entidadFinanc = document.getElementById("ddl-entidad").value;
    var cuenta = document.getElementById("tipoCuentaBancaria").value;
    var comprobacion = document.getElementById("comprobacion").value;

    if (entidadFinanc == '' ||  comprobacion == '' || cuenta == '') {
        $('#boton-cancelar').addClass('disabled');
    }
    else {
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/compruebatipopago') ?>",
            type:"post",
            data: {'comprobacion': comprobacion, 'cuenta_deposito': cuenta, 'entidadFinanc': entidadFinanc, 'tipo': tipo},
            success: function(data){
                if (data=='repetido') {
                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación rechasada, ya esta en registro';
                    $('#boton-cancelar').addClass('disabled');
                } else {
                    $('#boton-cancelar').removeClass('disabled');
                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación aceptada';
                }

            },
            error: function(msg, status,err){
                            alert('error en linea 328, consulte a nelux');
                        }
        });
        //$('#boton-cancelar').removeClass('disabled');
    }

}
//setInterval('compruebatipopago()'/*,1000*/);//Recorro a un segundo la funcion de comprobación
//------------------------------------------------------

//funcion para obtener observacion
function agrega_observacion(observacion){
    var idCabeza = '<?php echo $model->idCabeza_Factura ?>';
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addobservacion-u') ?>",
        type:"post",
        data: {'observacion': observacion, 'idCabeza': idCabeza},
    }) ;
}

$(document).on('click', '#activity-index-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.well-orden-servicio').html(data);
                        $('#modalOrden').modal();
                    }
                );
            }));
$(document).on('click', '#activity-index-link-inventario', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('.modal-body').html(data);
                        $('#modalinventario').modal();
                    }
                );
            }));
$(document).on('click', '#activity-link-cancelar-factura', (function() {
      $.fn.modal.Constructor.prototype.enforceFocus = function() {};//me permite que el select2 tenga el filtro activo
      $('#modalCancelar').modal('show');//muestro la modal
      setTimeout(function() {//aparece el autofocus en pago luego de un segundo
        $('#autofocus').slideDown(function(){
            $('#pago').focus();
        });
      },500);
    }));

    //para mostrar la modal de facturación
    function modal_facturar(url_condicion_venta, url_index, id) {//llamo la mdal de facturacion
      $.get( url_condicion_venta, { 'id' : id },
        function( data ) {
          if (data == 'incompleto') {
            alert('Se encuentran datos incompletos');
            window.location.href = url_index;
          } else {
            $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
            $('#condicion_venta').html(data);
            $('#modal_facturar').modal('show');//muestro la modal de facturacion
            setTimeout(function() {//aparece el autofocus en pago luego de un segundo
              $('#autofocus').slideDown(function(){
                  $('#pago').focus();
              });
            },500);
          }
        });
    }

$(document).ready(function () {
            (function ($) {
                $('#filtrar').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));
            $('#tabla_lista_productos').dataTable({
            "sPaginationType" : "full_numbers"//DAMOS FORMATO A LA PAGINACION(NUMEROS)
            });
        });
function regresarindex(idCa){
    var url = "<?php echo Url::toRoute('cabeza-caja/index') ?>&id="+idCa;
    window.location.href = url;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function cancelar_factura(idCa, this_boton, correo_cliente, vueltoinput) {
  var orden = "<?= Url::toRoute('cabeza-caja/orden')?>&id="+idCa;
  var $btn = $(this_boton).button('loading'); //ponemos en suspencion el boton de cancelar
  $("#pago").attr('disabled','disabled');
  $.ajax({
    'type': "post",//enviamos a factun para la firma electronica
    'url': "<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/factura_electronica') ?>",
    'dataType': 'json',
    data: {
      'idCa' : idCa,
      'vueltoinput' : vueltoinput,
    },
    beforeSend: function(xhr) {
      document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-info" style="font-size:15pt;">[ Creando Factura/Tiquete electrónico (XML) ]</span><br><br><span class="label label-default" style="font-size:10pt;">Espere, facturación en proceso...</span></center><br>';
    },
    success: function(data_factura){//devuelve en json los datos de respuesta de factun
      console.log(data_factura.json_result_facturacion);
      //console.log(data_factura.detalle);
      console.log(data_factura.mensajes_error);
      var obj = jQuery.parseJSON( data_factura.json_result_facturacion );//creamos un objeto con los datos de factun
      if (obj.exito == false) {//si existen errores se presentan aqui y se imprime el detalle
        document.getElementById("alerta_cancelar").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+obj.mensajes.mensaje+':</strong> '+obj.mensajes.detalle_mensaje+'.</center>'+data_factura.mensajes_error+'</div>';
        $btn.button('reset');
      } else {
        //var ticket = "<!--?= Url::toRoute('cabeza-caja/ticket')?-->&id="+idCa;
        //window.open(ticket,"_blank");
        var orden = "<?= Url::toRoute('cabeza-caja/orden')?>&id="+idCa;
        //window.open(orden,"_blank");
        document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-success" style="font-size:15pt;">[ Documento electrónico creado y firmado ]</span><br><br><span class="label label-default" style="font-size:10pt;">Espere que se recargue la página...</span></center><br>';
        izq = (screen.width-450)/2;
        sup= (screen.height-450)/2;
        var ventimp=window.open('#',"MsgWindow", "menubars=no,toolbars=no,width=600,height=300,left=" + izq + ",top=" + sup);//'MsgWindow'
        ventimp.document.write(data_factura.resultado_vuelto);
        ir_a_index(idCa);
      }
    },
  });
}

function finalizar_credito(idCa, tipo, this_boton) {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/factura_electronica') ?>",
        type:"post",
        dataType: 'json',
        data: {'idCa' : idCa, 'vueltoinput': 0},
        beforeSend: function() {
          var $btn = $(this_boton).button('loading');
          document.getElementById("rebotons").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Acreditando y firmando factura, Espere...</span></center><br>';
          },
        success: function(data_factura){
          console.log(data_factura.json_result_facturacion);
          //console.log(data_factura.detalle);
          console.log(data_factura.mensajes_error);
          var obj = jQuery.parseJSON( data_factura.json_result_facturacion );//creamos un objeto con los datos de factun
          if (obj.id == 0) {//si existen errores se presentan aqui y se imprime el detalle
            document.getElementById("rebotons").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+obj.mensajes.mensaje+':</strong> '+obj.mensajes.detalle_mensaje+'.</center>'+data_factura.mensajes_error+'</div>';
            $btn.button('reset');
          } else {
            document.getElementById("rebotons").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Factura firmada y acreditada, espere que se recargue la pantalla...</span></center><br>';
            var orden = "<?= Url::toRoute('cabeza-caja/orden')?>&id="+idCa;
            //window.open(orden,"_blank");
            izq = (screen.width-450)/2;
            sup= (screen.height-450)/2;
            var ventimp=window.open('#',"MsgWindow", "menubars=no,toolbars=no,width=600,height=150,left=" + izq + ",top=" + sup);//'MsgWindow'
            ventimp.document.write(data_factura.resultado_vuelto);
            var url_ = "<?php echo Url::toRoute('cabeza-caja/index') ?>";
            regresarindex(idCa);//window.location.href = url_;
            //setTimeout(ir_a_index(),2000);
            /*$.get( "<!--?= Yii::$app->getUrlManager()->createUrl('cabeza-caja/enviar_correo_factura') ?-->" ,
            {'id' : idCa, 'email' : data_canc.email , 'id_factun' : data_canc.id_factun},
              function( data ) {});*/
          }
        },
        error: function(msg, status,err){
                      //  alert('error en linea 204, consulte a nelux');
                    }
    });
}

function ir_a_index(){
  var url_ = "<?php echo Url::toRoute('cabeza-caja/index') ?>";
  window.location.href = url_;
}

//espacio que solo permite numero y decimal (.)
function isNumberDe(evt) {
    var nav4 = window.Event ? true : false;
    var key = nav4 ? evt.which : evt.keyCode;
    return (key <= 13 || key==46 || (key >= 48 && key <= 57));
}

//funcion para guardar en base de datos la informacion OT
function datos_ot(valor, inp, fa) {

  $.ajax({
    url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/datos_otu') ?>",
    type:"post",
    data: { 'valor' : valor, 'inp' : inp, 'idCabeza_Factura' : fa },
    beforeSend: function() {
    },
    success: function(valores){
      if (inp=='cl') {
        if (valores != 'aseptado') {
            $('#ddl-cliente-ot').val('').trigger('change.select2');
            $("#notif_ot").html(valores);
        } else {
          $("#notif_ot").html('');
        }
      } else if (inp=='ma') {
        $('.modelo_ot').html(valores).fadeIn();
      }
    }
  });
}

//ignorar el % de servicioRestaurante
$(document).on('change', 'input[name="ign_por_ser"]', function(e) {
    if (this.checked == true){
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/ignorar_u') ?>",
      {'tipo': 'porc_se', 'estado' : 'checked', 'id' : '<?= $model->idCabeza_Factura ?>'},
        function( data_prod ) {
          $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle_update') ?>", {'id' : '<?= $model->idCabeza_Factura ?>' },
                          function( data ) {
                            $('#modalinventario').modal('hide');
                            $('#detalle').html(data);
                            $('#descuentou').val('').change();
                            $('#codProd').val(null);

                            $("input#busqueda_codi").val(null);
                            $("input#busqueda_des").val(null);
                            $("input#busqueda_ubi").val(null);
                            activar_input_ingreso_producto();
                          });
        });
      }
  else {
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/ignorar_u') ?>",
    {'tipo': 'porc_se', 'estado' : '', 'id' : '<?= $model->idCabeza_Factura ?>'},
      function( data_prod ) {
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle_update') ?>", {'id' : '<?= $model->idCabeza_Factura ?>' },
                        function( data ) {
                          $('#modalinventario').modal('hide');
                          $('#detalle').html(data);
                          $('#descuentou').val('').change();
                          $('#codProd').val(null);

                          $("input#busqueda_codi").val(null);
                          $("input#busqueda_des").val(null);
                          $("input#busqueda_ubi").val(null);
                          activar_input_ingreso_producto();
                        });
      });
      }
      });

  //acordeon de modificar producto
  var visto = null;
    function modificar_producto(position) {
      div = document.getElementById('tr_'+position);
      div.style.display = (div==visto) ? 'none' : 'block';
      if (visto != null)
      visto.style.display = 'none';
      visto = (div==visto) ? null : div;
    }

    function aplicardescuentogeneral_up(idCa){
      var descuento = $("#descuentou").val();
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/aplicardescuentogeneral_up') ?>",
        {'porc_descuento' : descuento, 'idCa' : idCa},
        function( result ) {

        });
    }

    function modificar_linea_up(linea, position, idCa) {
      var cantidad = document.getElementById("can"+linea).value;
      var porc_descuento = document.getElementById("dct"+linea).value;
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/modificar_linea_up') ?>",
      {'cantidad_input' : cantidad, 'porc_descuento' : porc_descuento, 'position' : position},
      function( result ) {
        if (result!='vacio') {
          $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle_update') ?>", {'id': idCa},
              function( data ) {
                $('#detalle').html(data);
              });
              $("#campo_notificacion").html(result);
        } else {
          $("#campo_notificacion").html('<br><div class="alert alert-danger" role="alert"> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.</div>');
        }
        setTimeout(function() {//aparece la alerta en un milisegundo
            $("#campo_notificacion").fadeIn();
        });
        setTimeout(function() {//se oculta la alerta luego de 3 segundos
            $("#campo_notificacion").fadeOut();
        },8000);
      });
    }
</script>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }
    #modal_facturar .modal-dialog{ width: 30%!important; }
</style>

<div class="encabezado-prefactura-update">

    <span style="float:right; font-size: 18pt; color: #797B3B;">Prefactura ID: <?= $model->idCabeza_Factura ?></span>
    <?php
   /* $this->registerJs("
    $('#modalinventario').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title')
        var href = button.attr('href')
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
        })
");*/
    ?>
    <?php
    $ot = Yii::$app->params['ot'];
  /*  if ($ot == true) {
      if (@$get_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->one()) {
        echo '<span style="float:right">' .Html::a('', '#', ['class' => 'fa fa-share fa-5x', 'onclick' => 'habilitar_ot(false)', 'data-toggle' => 'titulo', 'data-placement' => 'left', 'title' => Yii::t('app', 'DESHABILITAR O.T')]) . '</span>';
      } else {
        echo '<span style="float:right">' .Html::a('', '#', ['class' => 'fa fa-openid fa-5x', 'onclick' => 'habilitar_ot(true)', 'data-toggle' => 'titulo', 'data-placement' => 'left', 'title' => Yii::t('app', 'HABILITAR O.T')]) . '</span>';
      }
    }*/ ?>
    <p>
        <div id="rebotons" align='left'>
            <?php
                if ($estado == 'Pendiente' || $estado == 'Apartado') {
                    echo "
                    <script type='text/javascript'>
                    window.onkeydown = tecla;
                      function tecla(event) {
                        //event.preventDefault();
                        num = event.keyCode;

                        if(num==113)
                          $('#modalinventario').modal('show');//muestro la modal
                          $('#busqueda_cod').focus();
                      }
                    </script>";
                    echo Html::a('<span class="fa fa-arrow-left"></span> Salir', ['regresarborrar-u'], ['class' => 'btn btn-default']);
                }
            ?>
            <?php if ($estado == 'Pendiente') : ?>
            <?= Yii::$app->params['apartados'] == true ?  Html::a('<span class="fa fa-heart-o"></span> Crear Apartado', ['crear_apartado'], [
              'class' => 'btn btn-primary',
              'data' => [
                  'confirm' => '¿Seguro de crear esta prefactura como apartado?',
                  'method' => 'post',
              ]
              ]) : '' ?>
            <?php endif ?>
            <?php if(/*$products && $vendedor && $cliente && $pago && */$estado == 'Pendiente' || $estado == 'Apartado') : ?>

                <?= $estado == 'Pendiente' && Yii::$app->params['caja_multiple']==false ? '&nbsp;&nbsp;&nbsp;'.Html::a('<span class="fa fa-inbox"></span> Enviar a Caja', ['caja'], [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'confirm' => '¿Seguro de enviar a caja para cancelar?',
                        'method' => 'post',
                    ],
                ]) : '' ?>

                <?= $estado == 'Apartado' ? Html::a('<i class="fa fa-print"></i> Imprimir Ticket', ['facturas-dia/ticket', 'id' => $model->idCabeza_Factura ], [
                    'class'=>'btn btn-primary',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Imprimir'
                ]).'&nbsp;&nbsp;&nbsp;' : '' ?>

                <?= Html::a('<i class="glyphicon glyphicon-print"></i> Imprimir', ['report', 'id' => $model->idCabeza_Factura], [
                    'class'=>'btn btn-info',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Imprimir'
                ]) ?>

                <?= Html::a('<span class="fa fa-ban"></span> Desechar', ['desechar'], [
                'class' => 'btn btn-danger',
                'data-loading-text'=>'OK...',
                'onclick' => 'javascript:bloquear_btn(this)',
                'data' => [
                    'confirm' => '¿Seguro que quieres desechar esta pre-factura?',
                    'method' => 'post',
                ],
                ]) ?>
                <!--?= /*Html::a('Acreditar', ['credito'], [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'confirm' => '¿Seguro de acreditar esta pre-factura?',
                        'method' => 'post',
                    ],
                ])*/ ?-->
                <!--?= //Html::a('Acreditar', ['index'], ['class' => 'btn btn-primary']) ?-->
            <?php endif ?>
            <?php if($products && $vendedor && $cliente && $pago && ($estado == 'Caja' || $estado == 'Crédito Caja')) { ?>
                <?= Html::a('<span class="fa fa-arrow-left"></span> Regresar', ['regresarborrar-caja'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('<span class="fa fa-mail-reply-all"></span> Devolver a prefactura', ['devolverfactura', 'idC'=>$model->idCabeza_Factura], [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'confirm' => '¿Seguro que quieres devolver esta factura a prefactura?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?php if($estado == 'Caja') { ?>
                <!--?= Html::a('Cancelar factura (FACTURAR)', '#', [
                    'id' => 'activity-link-cancelar-factura',
                    'class' => 'btn btn-success',
                    'data-toggle' => 'modal',
                    //'data-target' => '#modalCancelar',
                    //'data-url' => Url::to(['create']),
                    'data-pjax' => '0',
                ]) ?-->
            <?php } else {
              /*  echo Html::a('Imprimir factura', ['cabeza-caja/index', 'id'=>$model->idCabeza_Factura], [
                    //'id' => 'activity-link-cancelar-factura',
                    'class' => 'btn btn-info',
                    //'target'=>'_blank',
                    //'onclick'=>'javascript:regresarindex()',
                    //'data-url' => Url::to(['create']),
                    'data-pjax' => '0',
                ]);*/
                //echo Html::a('<span class="fa fa-send-o"></span> Terminar y acreditar factura', '#', [ 'class'=>"btn btn-info", 'data-loading-text'=>"Espere...", 'onclick'=>'javascript:finalizar_credito('.$model->idCabeza_Factura.', 1, this)']);

                }  ?>

            <?= $empresa->factun_conect == 1 ? Html::a('<i class="glyphicon glyphicon-print"></i> Documento Contingencia', ['facturas-dia/ticket', 'id' => $model->idCabeza_Factura ], [
                'class'=>'btn btn-warning',
                'target'=>'_blank',
                'data-toggle'=>'tooltip',
                'title'=>'Imprimir documento Contingencia'
            ]) : '' ?>

            <?= Html::a('<span class="fa fa-ban"></span> Desechar', ['desechar'], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Seguro que quieres desechar esta factura?',
                    'method' => 'post',
                ],
            ]) ?>
          <?php }
            if (Yii::$app->params['caja_multiple'] == true && $vendedor && $cliente && $pago && ($estado != 'Caja' || $estado != 'Crédito Caja')) {
              echo Html::a('<i class="glyphicon glyphicon-print"></i> Documento Contingencia', ['facturas-dia/ticket', 'id' => $model->idCabeza_Factura ], [
                  'class'=>'btn btn-warning',
                  'target'=>'_blank',
                  'data-toggle'=>'tooltip',
                  'title'=>'Imprimir documento Contingencia'
              ]);
            }
         ?>
        </div>

    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php
//------Modal para Cancelar factura
       Modal::begin([
            'id' => 'modal_facturar',
            'size'=>'modal-lg',
            'header' => '<h4 class="modal-title">Facturar (Cancelacion o Crédito)</h4>',
            'footer' => '<div id="alerta_cancelar"></div><a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        ?>
        <div class='panel'>
          <div class="panel-body">
            <center><span style="font-size: 1.5em; color:#757777;">Condicion de venta y medio de pago</span></center>
            <div id="condicion_venta"><br><br><center><span style="font-size: 2em; color:#E49E53;">(Seleccione la factura a emitir)</span></center><br><br>
              <strong>Nota:</strong> No emita una factura cuando Hacienda este Fuera de servicio.
            </div>
          </div>
        </div>
        <?php
        Modal::end();

//------------------------modal para mostrar orden de servicio
        Modal::begin([
            'id' => 'modalOrden',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Orden de servicio #'.$model->idOrdenServicio.'</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div class='well-orden-servicio'></div></div>";

        Modal::end();

//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        //http://blog.collectivecloudperu.com/buscar-en-tiempo-real-con-jquery-y-bootstrap/
        echo '<div class="well">
      <div class="col-lg-2">
        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_codi', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
      </div>
      <div class="col-lg-2">
        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
      </div>
      <div class="col-lg-2">
        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
      </div>
      <div class="col-lg-2">
        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
      </div>
      <div class="col-lg-2">
        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
      </div>
      <div class="col-lg-2">
        '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
      </div>

                <table class="table table-hover" id="tabla_lista_productos">';
        echo '<thead>';
        printf('<tr><th width="150">%s</th>
                    <th width="380">%s</th>
                    <th width="100">%s</th>
                    <th width="130">%s</th>
                    <th width="120">%s</th>
                    <th width="50">%s</th>
                    <th width="140">%s</th>
                    <th class="actions button-column" width="60">&nbsp;RESERVADO</th>
                </tr>',
                        'CÓDIGO LOCAL',
                        'DESCRIPCIÓN',
                        'CANT.INV',
                        'COD.ALTER',
                        'CÓD.PROV',
                        'UBICACIÓN',
                        'PRECIO + IV'
                        );
                echo '</thead>';
               /* $model_Prod = ProductoServicios::find()->where(["=",'tipo','Producto'])->all();
                echo '<tbody class="buscar">';
                foreach($model_Prod as $position => $product) {
                    $accion = Html::a('', '', ['class'=>'glyphicon glyphicon-plus', 'data-value'=>$product['codProdServicio'],'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Agregar este producto', 'onclick'=>'javascript:agrega_productou($(this).data("value"))']);
                    $presiopro = '₡ '.number_format($product['precioVentaImpuesto'],2);
                    printf('<tr><td>%s</td><td class="des">%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="actions button-column">%s</td></tr>',


                            $product['codProdServicio'],
                            $product['nombreProductoServicio'],
                            $product['cantidadInventario'],
                            $presiopro,
                            $accion
                            );
                }//fin de foreach
                echo '</tbody>';*/

                echo '</table>
                        <div id="resultadoBusqueda" style="height: 400px;width: 100%; overflow-y: auto; ">
                </div></div>';

        Modal::end();
    ?>
<script type="text/javascript" src="<?= $caja ?>"></script>
