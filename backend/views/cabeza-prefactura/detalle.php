<?php
use backend\models\Clientes;
use backend\models\Empresa;
use backend\models\Compra;//Prueba
use yii\helpers\Html;
use backend\models\MovimientoCredito;
use kartik\widgets\Select2;
use backend\models\DetalleFacturasTemp;
use yii\helpers\Json;
?>
<style media="screen">

table.scroll {
	 width: 100%;  /* Optional */
	/* border-collapse: collapse; */
	border-spacing: 0;
	/*border: 5px solid #E7D8D8;*/
}

table.scroll tbody,
table.scroll thead { display: block; }



table.scroll tbody {
	height: 300px;
	overflow-y: auto;
	overflow-x: hidden;
}

/*tbody { border-top: 2px solid black; }*/

tbody td, thead th {
	/* width: 20%; */ /* Optional */
	border-right: 1px solid white;
	/*border-bottom: 4px solid #E0E0E0;*/
	/* white-space: nowrap; */
}

tbody #td10, thead #td10 {
	 width: 10%;
}
tbody #td40, thead #td40 {
	 width: 40%;
}
tbody #td5, thead #td5 {
	 width: 5%;
}

tbody td:last-child, thead th:last-child {
	border-right: none;
}
</style>
	<div class="col-lg-9"><br>
    <div class="table-responsive">
      <div id="conten_pro">
            <?php
            //http://www.programacion.com.py/tag/yii
                //echo '<div class="grid-view">';
                echo '<div class="">';
                echo '<table class="items table scroll table-inverse">';
                echo '<thead>';
                echo '<tr>
												<th id="td10">CANT</th>
												<th id="td10">CÓDIGO</th>
												<th id="td40">DESCRIPCIÓN</th>
												<th id="td10" style="text-align:right;">PREC.UNIT</th>
												<th id="td10" style="text-align:right;">DESCUENTO</th>
												<th id="td5">IV %</th>
												<th id="td10" style="text-align:right;">TOTAL</th>
												<th id="td5" class="actions button-column">&nbsp;</th>
											</tr>';
                echo '</thead>';

            if($products) {
            ?>
            <?php
            echo '<tbody>';
								$products = array_reverse($products, true);
                foreach($products as $position => $product) {
									//$descuento_producto = $product['cantidad']*(($product['precio']*$product['dcto'])/100);
									//$porcentaje_descuento = (int) round(($descuento_producto/$product['cantidad']) * 100 / $product['precio'],0);
									$empresa = Empresa::findOne(1);
											$cantidad_descuento = $empresa->cantidad_descuento;
											$array_descuento = null;
											for ($i=1; $i <= $cantidad_descuento; $i++) {
												$array_descuento[$i] = $i;
											}
									$this_factura_temp = DetalleFacturasTemp::find()->where(['cod'=>$product['cod']])->andWhere('usuario = :usuario', [':usuario'=>Yii::$app->user->identity->username])->one();
									//este parche es para el momento que el sistema detecta que hay un producto en sesion pero no en la tabla temporal de base de datos
									if (!$this_factura_temp) {
										//Descodificamos el array JSON
		                $compra = JSON::decode(Yii::$app->session->get('compra'), true);
		                //Eliminamos el atributo pasado por parámetro
		                unset($compra[$position]);
		                //Volvemos a codificar y guardar el contenido
		                Yii::$app->session->set('compra', JSON::encode($compra));
									}
									$this_factura_temp = DetalleFacturasTemp::find()->where(['cod'=>$product['cod']])->andWhere('usuario = :usuario', [':usuario'=>Yii::$app->user->identity->username])->one();
									$desc = Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
													'name' => '',
													'value'=> (int)$this_factura_temp->dcto,
													'data' => $array_descuento,//[1 => "1%", 2 => "2%", 3 => "3%", 4 => "4%", 5 => "5%", 6 => "6%", 7 => "7%", 8 => "8%", 9 => "9%", 10 => "10%"],
													'options' => [
															'placeholder' => 'Seleccione %',
															'id'=>'dct'.$product['cod'],
															'onchange'=>'javascript:activar_input_ingreso_producto()',
															'multiple' => false //esto me ayuda a que solo se obtenga uno
													],
													'pluginOptions' => ['initialize'=> true,'allowClear' => true]
											]);

										$facturas_temp = DetalleFacturasTemp::find()->where(['cod'=>$product['cod']])->all();
										$usuarios_reserva = '';
										foreach ($facturas_temp as $value_temp) {
											$usuarios_reserva .= ' > '.$value_temp->usuario.'<br>';
										}
										echo '<tr style="display: none;" id="tr_'.$position.'">
														<td colspan="8" id="td10" bgcolor="#CAF5AA">
															<div class="col-md-7">
																<h2>'.$product['cod'].'<br><small>'.$product['desc'].'</small><h2>
															</div>
															<div class="col-md-3">
																Reservado por: '.$usuarios_reserva.'
															</div>
															<div class="col-md-2">
															Cantidad:
															<input type="text" value="'.$product['cantidad'].'" id="can'.$product['cod'].'" class="form-control form-control-sm" />
															Descuento(%):
															'.$desc.'
																'.Html::a('<span class="fa fa-floppy-o"></span> Guardar', NULL, ['class' => 'btn', 'style'=>'cursor:pointer;', 'onclick'=>'modificar_linea_cr("'.$product['cod'].'", '.$position.')']).'
															</div>
														</td>
													</tr>
													<tr>
		                    		<td id="td10">'.Html::textInput('cantidad_'.$position,
																$product['cantidad'], array(
																		'style'=>'text-align: center;',
																		'onkeypress'=>'return isNumberDe(event)',
																		'class' => 'cantidad_'.$position, 'size' => '4'
																		)
																).'</td>
		                    		<td id="td10"><a href="javascript:modificar_producto('.$position.');" >'.$product['cod'].'</a></td>
		                    		<td id="td40">'.$product['desc'].'</td>
		                    		<td id="td10" align="right">'.number_format($product['precio'],2).'</td>
		                    		<td id="td10" class = "textright descuent_'.$position.'" align="right">'.number_format($product['cantidad']*(($product['precio']*$product['dcto'])/100),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		                    		<td id="td5" align="right">'.number_format($product['iva'],2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
														<td id="td10" class="textright total_'.$position.'" align="right">'.number_format(($product['precio']*$product['cantidad']),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		                    		<td id="td5"class="actions button-column">'.Html::a('', null, ['class'=>'glyphicon glyphicon-trash',
														'onclick'=>'deleteitem('.$position.',"'.$product['cod'].'", '.$product['cantidad'].')', 'data-toggle'=>'tooltip','data-placement'=>'right', 'title'=>'Descartar este producto', 'style'=>'cursor:pointer;'])
																 .'&nbsp;&nbsp;<!--input type="checkbox" id="'.$position.'" name="'.$position.'" /--></td>
													</tr>';

                           /* Html::textInput('precio_'.$position,
                                $product['precio'], array(
                                    'class' => 'span2 precio_'.$position,
                                    )
                                ), */
                            //$product['dcto'],
                            /*Html::textInput('descuent_'.$position,
                                round($product['cantidad']*(($product['precio']*$product['dcto'])/100),2),//Descuento
                                array(
                                    'class' => 'descuent_'.$position, 'readonly' => true,
                                    )
                                ), */

                        //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
											/*	$(document).on('click', function(){
													if(('.cantidad_".$position."').focus()){

													}
												});*/
                        $this->registerJs("
																			$(document).on('change', '.cantidad_".$position."', function(){
                                        $.ajax({
                                            url:'".Yii::$app->getUrlManager()->createUrl('/cabeza-prefactura/updatecantidad')."',
                                            data: $('.cantidad_".$position."'),
																						dataType: 'json',
                                            success: function(result) {
                                            $('.total_".$position."').html(result.total);
																						$('.descuent_".$position."').html(result.descuento);
																						$('#can".$product['cod']."').val($('.cantidad_".$position."').val());
                                            getTotal();
                                            },
                                            error: function() {
                                            },
                                        });
                                    });

                                ");
                       /* $this->registerJs("
                                    $('.precio_".$position."').keyup(function() {
                                        $.ajax({
                                            url:'".Yii::$app->getUrlManager()->createUrl('/cabeza-prefactura/updateprecio')."',
                                            data: $('.precio_".$position."'),
                                            success: function(result) {
                                            $('.total_".$position."').html(result);
                                            $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                            '/cabeza-prefactura/getpreciototal')."');
                                            },
                                            error: function() {
                                            },

                                            });
                                    });
                                ");*/
                    /*Yii::$app->clientScript->registerScript('cantidad_'.$position,"
                            $('.cantidad_".$position."').keyup(function() {
                                $.ajax({
                                    url:'".$this->createUrl('/cabcompra/updateCantidad')."',
                                    data: $('#cantidad_".$position."'),
                                    success: function(result) {
                                    $('.total_".$position."').html(result);
                                    $('.resumen').load('".$this->createUrl(
                                    '/cabcompra/getPrecioTotal')."');
                                    },
                                    error: function() {
                                    },

                                    });
                        });
                            ");

                    Yii::$app->clientScript->registerScript('precio_'.$position,"
                            $('.precio_".$position."').keyup(function() {
                                $.ajax({
                                    url:'".$this->createUrl('/cabcompra/updatePrecio')."',
                                    data: $('#precio_".$position."'),
                                    success: function(result) {
                                    $('.total_".$position."').html(result);
                                    $('.resumen').load('".$this->createUrl(
                                    '/cabcompra/getPrecioTotal')."');
                                    },
                                    error: function() {
                                    },

                                    });
                        });
                            "); */

                }
            //echo '</div>';

            ?>
            <?php

            }
						echo @$products ? '<tr>
																	<td colspan="8" bgcolor="#CAF5AA" id="td10" align="center">El orden de ingreso es de abajo hacia arriba (el último ingresado es el primero en la lista)</td>
															</tr>
															' : '<tr>
																			<td colspan="8" bgcolor="#F4AE6C" id="td10" align="center"><h3>Área vacía, esperando un producto.</h3></td>
																	</tr>';
						echo "</tbody>";
                $alerta = ''; $checked_cred_venc = '';
                if(@$clientealert = Compra::getCliente()) {
                    if(@$modelclient = Clientes::find()->where(['idCliente'=>$clientealert])->one())
                    {
                        if ($modelclient->credito == 'Si') {
                            $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                            $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                            $alerta = '
                            <div class="alert alert-info alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>¡Atención!</strong> Si esta prefactura será acreditada considere que '.$modelclient->nombreCompleto.' tiene un monto disponible para crédito de ₡'.number_format($montodisponiblecredito,2).'. Si se excede ese monto el sistema no permitirá acreditar la factura. Si esta prefactura no será acreditada ignore este informe.
                            </div> ';
                            $facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $modelclient->idCliente])->andWhere("estadoFactura = :estadoFactura AND fecha_vencimiento < :tipofecha", [':estadoFactura'=>'Crédito', ':tipofecha' => date('Y-m-d')])->one();
                            if (@$facturasCredito) {
                              $checked_cred_venc = 'checked';
                            }
                        }
                    }

                    }
	$servicio_ignorado = Compra::getServicio_porc_ign();

	$ignorar_servicio = Yii::$app->params['servicioRestaurante'] == false ? '' : '<h4>Ignorar % servicio. <span style="float:right"><input type="checkbox" style="transform: scale(2);" name="ign_por_ser" '.$servicio_ignorado.'/></span></h4>';
                echo '</table>
				</div>
		  </div>
		</div>
	</div>
	<div class="col-lg-3"><br>
		<div class="well">
			<h4>Crédito vencido. <span style="float:right"><input type="checkbox" style="transform: scale(2);" name="ign_cre_ven" '.$checked_cred_venc.' disabled /></span></h4>
			'.$ignorar_servicio.'
		</div>
	</div>
	<div class="col-lg-3">
		<div class="well">
			<div id="resumen">'
			.Compra::getTotal(false).
			'</div>
		</div>
	</div>
  <div class="col-lg-12">
      <div class="alerta_r">'.$alerta.'</div>
  </div>
      ';
            ?>
						<div class="col-lg-12">
						<div class="input-group">
							 <span class="input-group-addon" id="sizing-addon3">Observaciones</span>
						<?php
						$observacion = Compra::getObserva() ? Compra::getObserva() : 'Ninguna';
						echo Html::input('text', '', $observacion, ['id' => 'observacion', 'class' => 'form-control', 'placeholder' =>'Agregue alguna observación', 'onKeyUp' => 'javascript:agrega_observacion(this.value)']); ?>
						</div>
						</div>
<script type="text/javascript">
function getTotal() {
	$.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/getpreciototal') ?>",
							function( data ) {
								$('#resumen').html(data);
							});
}
// Change the selector if needed
/*var $table = $('table.scroll'),
	$bodyCells = $table.find('tbody tr:first').children(),
	colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
	// Get the tbody columns width array
	colWidth = $bodyCells.map(function() {
			return $(this).width();
	}).get();

	// Set the width of thead columns
	$table.find('thead tr').children().each(function(i, v) {
			$(v).width(colWidth[i]);
	});
}).resize();*/ // Trigger resize handler
</script>
