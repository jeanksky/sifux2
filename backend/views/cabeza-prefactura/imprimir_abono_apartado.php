<?php

use yii\helpers\Html;
use backend\models\Empresa;
use yii\helpers\Url;
use backend\models\MovimientoApartado;
use backend\models\NumberToLetterConverter;
//Clases para usar librería de etiquetas
use backend\models\WebClientPrint;
use backend\models\Utils;
use backend\models\DefaultPrinter;
use backend\models\InstalledPrinter;
use backend\models\ClientPrintJob;
use backend\models\Clientes;
$empresa_funcion = new Empresa();
/* @var $this yii\web\View */
/* @var $model backend\models\ComprasInventario */

//_________________________________________________________________________________________________________-->
//________________________________________IMPRECION DIRECTA____________________________________-->
//_________________________________________________________________________________________________________-->
// Generar ClientPrintJob? Sólo si parámetro clientPrint está en la cadena de consulta
    $urlParts = parse_url($_SERVER['REQUEST_URI']);

    if (isset($urlParts['query'])){
        //
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){

            $movimiento_adelanto = MovimientoApartado::find()->where(['idMov'=>$id])->one();//busco el movimiento a cobrar
            $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();//busco la empresa para el encabezado
            $cliente = Clientes::findOne($movimiento_adelanto->idCliente);
            $nombre_impresora = 'FACTURASNPS';
            //Create ESC/POS commands for sample receipt
            $esc = '0x1B'; //ESC byte in hex notation
            $newLine = '0x0A'; //LF byte in hex notation
            $cajon = $esc ."0x700x0";//abrir cajon
            $derecha = $esc . '!' . '0x00';//Establece el margen izquierdo en la columna n (donde n está entre 0 y 255) en el tono de carácter actual.
            $cmds = $esc . "@"; //Initializes the printer (ESC @)
            $cmds .= $newLine . $newLine;
            $cmds .= $esc . '0x00'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
            $cmds .= $empresa->nombre.$newLine; //Nombre de la empresa
            $cmds .= $empresa->direccion.$newLine; //Nombre de la empresa
            $cmds .= $empresa->telefono.' / '. $empresa->fax; //Nombre de la empresa

            $cmds .= $newLine . $newLine.$newLine;

            $cmds .= $esc . '!' . '0x31'; //Fuente de caracteres B seleccionado (ESC! 0)
            $cmds .= "ABONO DE APARTADO ".'# '. $movimiento_adelanto->idMov.$newLine.$newLine;
            $cmds .= $esc . '!' . '0x00'; //Fuente de caracteres A seleccionado (ESC! 0)
            $cmds .= date('d-m-Y | h:m:s A', strtotime($movimiento_adelanto->fecmov)).$newLine.$newLine;
            $cmds .= "Cliente:".$newLine. $cliente->nombreCompleto.$newLine.$newLine;
            $cmds .= "Por Concepto de:".$newLine. $movimiento_adelanto->concepto.$newLine.$newLine;
            $cmds .= "----------------------------------------------".$newLine;
            $cmds .= "Aplicada al Documento: ".$movimiento_adelanto->idCabeza_Factura.$newLine;
            $cmds .= "----------------------------------------------".$newLine;
            $cmds .= "MONTO ABONO: ". $esc . '!' . '0x31' .number_format($movimiento_adelanto->monto_movimiento,2).$newLine.$newLine.$newLine;
            $cmds .= $esc . '!' . '0x00';
            $cmds .= NumberToLetterConverter::ValorEnLetras($movimiento_adelanto->monto_movimiento,"COLONES").$newLine.$newLine.$newLine;
            $cmds .= "SALDO: ".number_format($movimiento_adelanto->saldo_pendiente,2);
            $cmds .= $newLine . $newLine . $newLine . $newLine . $newLine . $newLine;
            $cmds .= "_______________________". $newLine ."Autoriza" . $newLine . $newLine . $newLine . $newLine . $newLine;
            $cmds .= "Us-Reg:".$movimiento_adelanto->usuario;
            $cmds .= $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine . $newLine;

            $cmds .= $esc . '0x69';//con esto corta el papel al final

            //se crea un objeto de ClientPrintJob que se procesará en el lado del cliente por el WCPP
            $cpj = new ClientPrintJob();
            //establece comandos Zebra ZPL para imprimir...
            $cpj->printerCommands = $cajon.$cmds;
            $cpj->formatHexValues = true;
            //establece impresora cliente

                $cpj->clientPrinter = new InstalledPrinter($nombre_impresora);


            //Enviar ClientPrintJob al cliente
            ob_start();
            ob_clean();
            echo $cpj->sendToClient();

            ob_end_flush();
            exit();
        }//fin isset($qs[WebClientPrint::CLIENT_PRINT_JOB])
    }//fin isset($urlParts['query']
//fin de etiqueta-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
        //ejecuta la llamada de impresión
        jsWebClientPrint.print('sid=<?= Yii::$app->user->identity->last_session_id ?>');
    });
    setTimeout("self.close()", 900 );
</script>
<?php
//Especifique la URL ABSOLUTA al archivo php que creará el objeto ClientPrintJob
//En este caso, esta misma página
$webClientPrintControllerAbsoluteURL = Utils::getRoot().$empresa_funcion->getWCP();
$demoPrintCommandsProcessAbsoluteURL = Utils::getRoot().Url::home().'?r=cabeza-prefactura/imprimir_abono_apartado&id='.$id;
echo WebClientPrint::createScript($webClientPrintControllerAbsoluteURL, $demoPrintCommandsProcessAbsoluteURL, Yii::$app->user->identity->last_session_id);

?>
