<?php
//http://yii2-framework.readthedocs.org/en/latest/guide-es/helper-html/
//http://www.yiiframework.com/wiki/674/managing-your-nested-dropdown-dependency-with-depdrop-widget/
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use \kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
use kartik\widgets\Select2;
use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\Compra;//Prueba
use backend\models\FormasPago;//Prueba
use backend\models\Funcionario;
use backend\models\ProductoServicios;
use unclead\widgets\MultipleInput;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\DepDrop;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use kartik\widgets\AlertBlock;
use backend\models\AgentesComisiones;
use backend\models\EncabezadoPrefactura;
use backend\models\Marcas;
use backend\models\Modelo;
use backend\models\Version;
use backend\models\Ot;
/* @var $this yii\web\View */
/* @var $model backend\models\EncabezadoPrefactura */
/* @var $form yii\widgets\ActiveForm */
$url_condicion_venta = Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/condicion_venta');
$url_create = Url::toRoute('cabeza-prefactura/create');
?>
<?php $products = Compra::getContenidoCompra(); ?>

<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->

<script type="text/javascript">
  /*  $(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
        console.log("beforeInsert");
    });

    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        console.log("afterInsert");
    });

    $(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
        if (! confirm("Are you sure you want to delete this item?")) {
            return false;
        }
        return true;
    });

    $(".dynamicform_wrapper").on("afterDelete", function(e) {
        console.log("Deleted item!");
    });

    $(".dynamicform_wrapper").on("limitReached", function(e, item) {
        alert("Limit reached");
    });*/
</script>
<style type="text/css">
    #verticalLine {
    border-left: thick solid #E7D8D8;
    }
</style>

<div class="encabezado-prefactura-form">
    <!--?php// $form = ActiveForm::begin(['id' => 'dynamic-form']); ?-->
    <p style="text-align:right">
     <?php $fecha_inicio = date('d-m-Y');?>
     <?php // echo Html::activeInput('text' , $model,'fecha_inicio',[ 'value' => '','readonly'=>true]);?>
     <?php
     $panel = 'success';
     $modo_ot = '';
     $ot = Yii::$app->params['ot'];
     if ($ot == true) {
      if (@$get_ot = Compra::getOT()) {
          $panel = 'info';
          $nombre_ot = Compra::getNombre_ot() ? Compra::getNombre_ot() : '';
          $placa_ot = Compra::getPlaca_ot() ? Compra::getPlaca_ot() : '';
          $marca_ot = Compra::getMarca_ot() ? Compra::getMarca_ot() : '';
          $modelo_ot = Compra::getModelo_ot() ? Compra::getModelo_ot() : '';
          $version_ot = Compra::getVersion_ot() ? Compra::getVersion_ot() : '';
          $ano_ot = Compra::getAno_ot() ? Compra::getAno_ot() : '';
          $motor_ot = Compra::getMotor_ot() ? Compra::getMotor_ot() : '';
          $cilindrada_ot = Compra::getCilindrada_ot() ? Compra::getCilindrada_ot() : '';
          $combustible_ot = Compra::getCombustible_ot() ? Compra::getCombustible_ot() : '';
          $transmision_ot = Compra::getTransmision_ot() ? Compra::getTransmision_ot() : '';
          $responsables_ot_permitidos = [];
          $clientes_ot_perm = Clientes::find()->where(['ot'=>'Si'])->all();
          foreach ($clientes_ot_perm as $key => $clientes_ot) {
            $Ot = Ot::find()->where(['idCliente'=>$clientes_ot->idCliente])->one();
            if ( ($Ot->estado_ot == 'Abierta') || ( $Ot->estado_ot == 'Cerrada' && $Ot->autorizacion_ot == 'Si' )) {
              $responsables_ot_permitidos[] = ['idCliente'=>$clientes_ot->idCliente, 'nombreCompleto'=>$clientes_ot->nombreCompleto];
            }

          }
          $modelos = Modelo::find()->where(['codMarca'=>$marca_ot])->all();
          $modo_ot = ' <div class="well col-lg-12">
                        <h4><center>O.T Activado</center></h4>
                        <div class="col-lg-4"><!-- ___________________AGREGAR RESPONSABLE OT______________________________________ -->

                                <strong>Nombre responsable O.T</strong>
                                '.Select2::widget([
                                //Esto lo vamos a usar sin modelo porque solo será visual
                                    'value' => $nombre_ot,
                                    'name' => '',
                                    'data' => ArrayHelper::map($responsables_ot_permitidos, 'idCliente',
                                        function($element) {
                                        return $element['idCliente'].' - '.$element['nombreCompleto'];
                                    }),
                                    'options' => [
                                        'id'=>'ddl-cliente-ot',
                                        'onchange'=>'javascript:datos_ot(this.value, "cl")',
                                        'placeholder' => 'Seleccione un cliente O.T',
                                        'multiple' => false //esto me ayuda a que solo se obtenga uno
                                    ],
                                    'pluginOptions' => [
                                          'allowClear' => true
                                      ]
                                ]).'
                        </div>
                        <div class="col-lg-2"><!-- ___________________AGREGAR PLACA OT______________________________________ -->
                          <strong>Placa</strong>
                          '.Html::input('text', '', $placa_ot, ['id' => 'placa_ot', 'class' => 'form-control', 'placeholder' =>'', 'onKeyUp' => 'javascript:datos_ot(this.value, "pl")']).'
                        </div>
                        <div class="col-lg-2"><!-- ___________________AGREGAR MARCA OT______________________________________ -->
                          <strong>Marca</strong>
                          '.Select2::widget([
                                      'value' => $marca_ot,
                                     // 'attribute' => 'tipoFacturacion',
                                      'name' => '',
                                      'data' => ArrayHelper::map(Marcas::find()->all(), 'codMarca', function($element){
                                        return $element['nombre_marca'];
                                      }),
                                      'options' => ['id'=>'marca_ot', 'onchange'=>'datos_ot(this.value, "ma")','placeholder' => 'Seleccione una marca'],
                                      'pluginOptions' => [
                                          'initialize'=> true,
                                          'allowClear' => true
                                      ],
                                  ]).'
                        </div>
                        <div class="col-lg-2"><!-- ___________________AGREGAR MODELO OT______________________________________ -->
                          <strong>Modelo</strong>
                          '.Select2::widget([
                                      'value' => $modelo_ot,
                                     // 'attribute' => 'tipoFacturacion',
                                      'name' => '',
                                      'data' => ArrayHelper::map($modelos, 'codModelo', function($element){
                                        return $element['nombre_modelo'];
                                      }),
                                      'options' => ['class'=>'modelo_ot', 'onchange'=>'datos_ot(this.value,"mo")','placeholder' => 'Seleccione un modelo'],
                                      'pluginOptions' => [
                                          'initialize'=> true,
                                          'allowClear' => true
                                      ],
                                  ]).'
                        </div>
                        <div class="col-lg-2"><!-- ___________________AGREGAR VERSION OT______________________________________ -->
                          <strong>Versión</strong>
                          '.Select2::widget([
                                      'value' => $version_ot,
                                     // 'attribute' => 'tipoFacturacion',
                                      'name' => '',
                                      'data' => ArrayHelper::map(Version::find()->all(),'codVersion', function($element){
                                        return $element['codVersion'];
                                      }),
                                      'options' => ['id'=>'version_ot', 'onchange'=>'datos_ot(this.value,"ve")','placeholder' => 'Seleccione una versión'],
                                      'pluginOptions' => [
                                          'initialize'=> true,
                                          'allowClear' => true
                                      ],
                                  ]) .'
                        </div>
                        <div class="col-lg-12"><br>
                        </div>
                        <div class="col-lg-4">
                          <div id="notif_ot">
                            <div class="alert alert-info alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>!Atento! </strong> Los responsables con facturas O.T vencidas no aparecen aqui, al menos que no tengan vencidas o vencidas con autorización.
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-1"><!-- ___________________AGREGAR AÑO OT______________________________________ -->
                          <strong>Año</strong>
                          '.Html::input('text', '', $ano_ot, ['id' => 'ano_ot', 'class' => 'form-control', 'placeholder' =>'', 'onKeyUp' => 'javascript:datos_ot(this.value,"an")', 'onkeypress'=>'return isNumber(event)']).'
                        </div>
                        <div class="col-lg-2"><!-- ___________________AGREGAR MOTOR OT______________________________________ -->
                          <strong>Motor</strong>
                          '.Html::input('text', '', $motor_ot, ['id' => 'motor_ot', 'class' => 'form-control', 'placeholder' =>'', 'onKeyUp' => 'javascript:datos_ot(this.value, "mot")']).'
                        </div>
                        <div class="col-lg-1"><!-- ___________________AGREGAR CILINDRADA OT______________________________________ -->
                          <strong>Cilindrada</strong>
                          '.Html::input('text', '', $cilindrada_ot, ['id' => 'cilindrada_ot', 'class' => 'form-control', 'placeholder' =>'', 'onKeyUp' => 'javascript:datos_ot(this.value, "ci")']).'
                        </div>
                        <div class="col-lg-2"><!-- ___________________AGREGAR COMBUSTIBLE OT______________________________________ -->
                          <strong>Combustible</strong>
                          '.Html::input('text', '', $combustible_ot, ['id' => 'combustible_ot', 'class' => 'form-control', 'placeholder' =>'', 'onKeyUp' => 'javascript:datos_ot(this.value, "co")']).'
                        </div>
                        <div class="col-lg-2"><!-- ___________________AGREGAR TRANSMISION OT______________________________________ -->
                          <strong>Transmisión</strong>
                          '.Html::input('text', '', $transmision_ot, ['id' => 'transmision_ot', 'class' => 'form-control', 'placeholder' =>'', 'onKeyUp' => 'javascript:datos_ot(this.value, "tr")']).'
                        </div>
                      </div>';
      } else {
         $panel = 'success';
         $modo_ot = '';
      }
     } ?>
     <!--?= /*$form->field($model,'fecha_inicio')->textInput([/*'value' => $fecha_inicio,*/'readonly'=>true])->label(false)*/ ?-->
    </p>
  <div class="panel panel-<?= $panel ?>">
    <!-- /.panel-heading -->
    <div class="panel-body">
            <div class="col-lg-12"><!-- ___________________AREA DE AGREGAR VENDEDOR Y ESTADO DE FACTURA______________________________________ -->
                <div class="form-actions" id="yw3">
                    <?php
                        if(@$vendedor = Compra::getVendedor()){
                        if(@$model = Funcionario::find()->where(['idFuncionario'=>$vendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                            {
                                echo "<div class='col-lg-4'>";
                                echo "<h4><strong>Vendedor</strong>: ".$model->nombre." ".$model->apellido1." ".$model->apellido2." ";
                                echo Html::a('', ['/cabeza-prefactura/deletevendedor'],['class'=>'glyphicon glyphicon-pencil']);
                                echo "</h4>";
                                echo "</div>";
                            }
                        }
                        else
                        {
                    ?>
                    <div class="col-lg-4"><!-- ___________________AGREGAR VENDEDOR POR CODIGO______________________________________ -->
                        <?php
                            echo '<h4><strong>Código vendedor</strong></h4>';
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                //'model' => $model,
                                //'attribute' => 'codigoVendedor',
                                'name' => '',
                                'data' => ArrayHelper::map(Funcionario::find()->all(), 'idFuncionario',
                                    function($element) {
                                    return $element['idFuncionario'].' - '.$element['nombre'].' '.$element['apellido1'].' '.$element['apellido2'];
                                }),
                                'options' => [
                                    'placeholder' => 'Código',
                                    'id'=>'ddl-codigoVendedor',
                                    'onchange'=>'javascript:obtenerVendedor()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                            ]);
                        ?>
                    </div>
                    <?php }?>
                    <!-- _________________________________AGREGAR VENDEDOR POR NOMBRE______________________________________ -->
                    <!--div class="col-lg-4">
                        <?php
                           // echo '<label class="control-label">Vendedor</label>';
                           // echo Html::input('text', '', null, ['class'=>'form-control', 'id' => 'resultado2' , 'readonly' => true]);
                        ?>
                    </div-->
                </div>
                <div class="form-actions" id="yw3">
                    <div class="col-lg-4"><!-- ___________________AGREGAR ESTADO______________________________________ -->
                    <?php

                        Compra::setEstadoFactura('Pendiente');


                        if(@$estado = Compra::getEstadoFactura()){
                        echo '<h4><strong>Estado de la factura</strong>: '.$estado;
                        echo " (Prefactura)</h4>";
                       // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                        }
                    ?>
                    </div>
                </div>
                <div class="form-actions" id="yw3">
                    <div class="col-lg-4"><!-- ___________________AGREGAR ESTADO______________________________________ -->
                    <?php
                        Compra::setFechaInicial($fecha_inicio);
                        if(@$fech = Compra::getFechaInicial()){
                        echo '<h4><strong>Fecha ingreso</strong>: '.$fech;
                        echo "</h4>";
                       // echo Html::activeInput('text', $model, 'estadoFactura', ['class'=>'form-control', 'readonly' => true, 'value' => 'Pendiente']);
                        }
                    ?>
                    </div>
                </div>
            </div>
            <?= $modo_ot ?>
            <div class="col-lg-12"> <!-- ___________________AREA DE AGREGAR CLIENTE______________________________________ -->
              <div class="form-actions" id="yw3">
                <?php
                    if(@$cliente = Compra::getCliente())
                    {   $clientSis = 0;
                        if(@$model = Clientes::find()->where(['idCliente'=>$cliente])->one())
                        {
                            $clientSis = 1;
                            if($clientSis!=0) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                {
                                  if ($model->iv == true) {
                                    $btn_exonerado = ' <button type="button" id="modal_exonerar" class="btn btn-warning btn-xs"><span style="font-size: 1.4em; ">Este cliente presenta exoneración</span></button>';
                                  } else {
                                    $btn_exonerado = '';
                                  }
                                     echo "<div class='col-lg-12'>";
                                     echo "<h4><strong>Cliente</strong>: <font size=6>".$model->nombreCompleto."</font><br><font color='#28B463'>(ESTE USUARIO SI ESTA EN EL SISTEMA)</font> ";
                                     echo Html::a('', ['/cabeza-prefactura/deletecliente'],['class'=>'glyphicon glyphicon-pencil']).$btn_exonerado;
                                     echo "</h4>";
                                     echo "</div>";
                                 }
                        }
                        else{
                            echo "<div class='col-lg-12'>";
                            echo "<h4><strong>Cliente</strong>: <font size=6>".$cliente."</font><br><font color='#8B8FCF'>(ESTE USUARIO <strong>NO ESTA</strong> EN EL SISTEMA)</font> ";
                            echo Html::a('', ['/cabeza-prefactura/deletecliente'],['class'=>'glyphicon glyphicon-pencil']);
                            echo "</h4>";
                            echo "</div>";
                        }
                    }
                    else {
                ?>
                <!--div class="col-lg-2"-->
                        <!--?php
                            echo '<h4><strong>Código cliente</strong></h4>';
                            echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => '',
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', 'idCliente'),
                                'options' => [
                                    'placeholder' => 'Código',
                                    'id'=>'cod-cliente',
                                    'onchange'=>'javascript:obtenerClienteCod()',
                                    'multiple' => false //esto me ayuda a que solo se obtenga uno
                                ],
                            ]);
                        ?-->
                <!--/div-->
                <div class="col-lg-4">
                        <?php

                            echo '<h4><strong>Código Cliente / Cliente en el sistema</strong></h4>';
                            echo Select2::widget([
                                //'model' => $model,
                                //'attribute' => 'idCliente',
                                'name' => '',
                                'data' => ArrayHelper::map(Clientes::find()->all(), 'idCliente', function($element) {
                                    return $element['idCliente'].' - '.$element['nombreCompleto'];
                                }),
                                'options' => ['id' => 'ddl-cliente', 'onchange'=>'javascript:obtenerCliente()','placeholder' => 'Seleccione un cliente'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        ?>
                </div>
                <div class="col-lg-4">
                        <?php
                            $form = ActiveForm::begin([
                                'id' => 'clienteN-form',
                                //'formConfig' => ['style' => 'POSITION: absolute', 'labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                                'action'=>['/cabeza-prefactura/clientenew'],
                                'enableAjaxValidation'=>false,
                                'enableClientValidation'=>false,
                                ]);

                                echo '<h4><strong>Identificación / Nuevo cliente</strong></h4>';
                                    echo '<div class="input-group">';
                                       echo Html::input('text', 'new-cliente', '', ['id' => '', 'class' => 'form-control']);
                                    echo '<div class="input-group-btn">';
                                        echo '<input type="submit" value="Agregar" class="btn">';
                                //echo Html::submitButton('Submit', ['style' => 'clear:both;', 'class' => 'submit']);
                                    echo '</div>';
                                echo '</div>';
                             /*   echo yii\bootstrap\Button::widget([
                                            'label' => 'Agregar',
                                            'options' => [ 'style' => 'clear:both;', 'class' => 'btn','buttonType' => 'submit'],
                                        ]);*/
                        ?>
                        <!--div class="input-group">
                          <input type="text" name="new-cliente" class="form-control">
                          <div class="input-group-btn">
                            <submit type="submit" class="btn btn-default">Agregar</submit>
                          </div>
                        </div-->

                        <?php ActiveForm::end(); ?>
                </div>
                <div class="col-lg-4">
                  <br><?= Html::a('Factura sin cliente', ['/cabeza-prefactura/sincliente', 'cliente'=>'Factura sin cliente'],['class'=>'btn btn-default btn-lg btn-block']) ?>
                </div>
                <?php } ?>
              </div>
            </div>

            <div class="col-lg-12"><hr/>
            <?= AlertBlock::widget([
                'type' => AlertBlock::TYPE_ALERT,
                'useSessionFlash' => true,
                'delay' => 15000
            ]);?>
            <div id="campo_notificacion"></div>
            <div id="cantidadminima"></div>
            <!-- ___________________AREA DE AGREGAR PRODUCTO Y TIPO DE PAGO______________________________________ -->
                <div class="col-lg-2"><!-- ___________________AGREGAR PRODUCTO______________________________________ -->
                    <h4><strong>% descuento</strong></h4>
                    <?php
                    $empresa = Empresa::findOne(1);
                        $cantidad_descuento = $empresa->cantidad_descuento;
                        $array_descuento = null;
                        for ($i=1; $i <= $cantidad_descuento; $i++) {
                          $array_descuento[$i] = $i;
                        }
                        echo '<div class="input-group">'.
                              Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                              'name' => '',
                              'data' => $array_descuento,//[1 => "1%", 2 => "2%", 3 => "3%", 4 => "4%", 5 => "5%", 6 => "6%", 7 => "7%", 8 => "8%", 9 => "9%", 10 => "10%"],
                              'options' => [
                                  'placeholder' => 'Seleccione %',
                                  'id'=>'descuento',
                                  'onchange'=>'javascript:activar_input_ingreso_producto()',
                                  'multiple' => false //esto me ayuda a que solo se obtenga uno
                                  ],
                                  'pluginOptions' => ['initialize'=> true,'allowClear' => true]
                              ]).'<div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li onClick="aplicardescuentogeneral_cr()"><a href="#">Aplicar descuento a todos los productos</a></li>
                                    </ul>
                                  </div>
                              </div>';
                    ?>
                </div>
                <div class="col-lg-4">
                    <h4>
                        <strong>Agregar Producto</strong>
                        <?php echo Html::a('', '#', [
                                'class' => 'glyphicon glyphicon-barcode',
                                'id' => 'activity-index-link-inventario',
                                'data-toggle' => 'modal',
                                'data-trigger'=>'hover', //esto es para que muestre el tootip sin darle clic
                                'data-content'=>'Selecciona para mostrar los productos en el inventario',
                                'data-target' => '#modalinventario',
                                'data-url' => Url::to(['#']),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Inventario'),
                                ]); ?>
                    </h4>
                    <?php
                        echo Html::hiddenInput('codProdServicio',0);
                        echo Html::hiddenInput('cantidad',1);
                    ?>
                    <!--?= /*$form->field($model, 'descripcion')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio', 'nombreProductoServicio'),
                                'options'=>['placeholder'=>'', 'onchange'=>''],
                                'pluginOptions' => ['allowClear' => true]]);*/ ?-->
                    <?php
                            /*echo Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
                                'name' => 'state_10',
                                'data' => ArrayHelper::map(ProductoServicios::find()->andwhere(['=','tipo','Producto'])->asArray()->all(), 'codProdServicio',
                                function($element) {
                                    return $element['codProdServicio'].' - '.$element['nombreProductoServicio']. ' [¢' .$element['precioVenta']. '] ---- Cantidad en stock: '.$element['cantidadInventario'];
                                }),
                                'options' => [
                                    'id' => 'codProdServicio',
                                    'placeholder' => 'Seleccione productos',
                                    'onchange'=>'javascript: agrega_producto(this.value)',
                                    //'onClick'=>'javascript: if (this.checked) setTimeout("refresca()", 500)',
                                    'pluginOptions' => ['allowClear' => true],
                                    'multiple' => false, //esto me ayuda a que se obtenga mas de uno
                                    //"update"=>"#conten_pro"
                                ],
                            ]);*/
                            echo '<div id="addpro">'.Html::input('text', '', '', ['autofocus' => 'autofocus', 'size' => '30', 'id' => 'codProd', 'class' => 'form-control', 'placeholder' =>'Ingrese código de producto aqui', 'onkeyup'=>'javascript:this.value=Mayuculas(event.keyCode, this.value)', 'onchange' => 'javascript:agrega_producto(this.value)']).'</div>';
                    ?>
                </div>
                <div class="col-lg-1"></div>
                <div id="verticalLine" class="col-lg-2"> <!-- ___________________AGREGAR PAGO______________________________________ -->

                            <!--?= /*$form->field($model, 'tipoFacturacion')->widget(Select2::classname(), [
                                            'value' => 'Contado', // initial value
                                            'data' => $data,
                                            'options'=>['placeholder'=>''],
                                            'pluginOptions' => ['allowClear' => true],
                                            ]);*/?-->

                <!--div class="form-actions" id="yw3"-->

                            <?php
                                //$doc = Compra::getDoc();
                            ?>
                            <?php /* $form = ActiveForm::begin([
                                'id' => 'pago-form',
                                'action'=>['/cabeza-prefactura/addpago'],
                                'enableAjaxValidation'=>false,
                                'enableClientValidation'=>false,
                                ]);*/ ?>
                            <?php //http://www.bsourcecode.com/yiiframework2/session-handling-in-yii-framework-2-0/
                            $vendedornullv = Compra::getVendedornull();
                            //esta condicion me permite traer el pago efectivo en primera instancia
                                    if ($vendedornullv!='1') {
                                        Compra::setPago(6);
                                    }//fin if

                                if (@$pago = Compra::getPago()) {
                                    if(@$model = FormasPago::find()->where(['id_forma'=>$pago])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                    {
                                        echo "<h4><strong>Condicion de venta</strong></h4><h4>".$model->desc_forma." ";
                                        echo Html::a('', ['/cabeza-prefactura/deletepago'],['class'=>'glyphicon glyphicon-pencil']);
                                        echo "</h4>";
                                    }
                                }
                                else
                                {
                                    echo '<h4><strong>Condicion de venta</strong></h4>' ;
                                    $data = ['5'=>'Crédito', '6'=>'Contado'];//ArrayHelper::map(FormasPago::find()->all(), 'id_forma', 'desc_forma');
                                    echo Select2::widget([
                                        'name' => '',
                                        'data' => $data,
                                        'options' => ['id'=>'tipoFacturacion', 'onchange'=>'javascript:run_pago()','placeholder' => 'Seleccione...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                }
                            ?>
                            <?php //ActiveForm::end(); .....................................................?>

                <!--/div-->
                </div>

                <div id="verticalLine" class="col-lg-3">
                   <?php
                        /*Para obtener params de config/main.php*/
                        $valueComiones = Yii::$app->params['comisiones'];
                        $agComiones = $valueComiones == true ? true: false;

                        if (@$agComiones) {

                           echo '<h4><strong>Agente</strong></h4>' ;
                            if (@$agntComision = Compra::getAgenteComision()) {
                                    if(@$model = AgentesComisiones::find()->where(['idAgenteComision'=>$agntComision])->andWhere("estadoAgenteComision = 'Activo'",[])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                                    {
                                        echo "<h5>".$model->nombre." ";
                                        echo Html::a('', ['/cabeza-prefactura/deleteagente'],['class'=>'glyphicon glyphicon-pencil']);
                                        echo "</h5>";
                                    }
                                }else                                {
                                    //echo '<h4><strong>Tipo Pago</strong></h4>' ;
                                   // $data = ArrayHelper::map(AgentesComisiones::find()->all()/*, 'idAgenteComision', 'nombre'*/);
                                    echo Select2::widget([
                                        'name' => '',
                                        'data' => ArrayHelper::map(AgentesComisiones::find()->where(['estadoAgenteComision'=>'Activo'])->asArray()->all(), 'idAgenteComision',  function($element) {
                                    return $element['idAgenteComision'] . ' - ' .$element['nombre'];
                                }),
                                        'options' => ['id'=>'idAgenteComision', 'onchange'=>'javascript:run_AgenteComision()','placeholder' => 'Seleccione...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                }

                        }//if del parametro de modulo COMISIONES
                    if (Yii::$app->params['caja_multiple']==true) {
                    ?><br>
                            <button type="button" id="facturar" onclick="modal_facturar('<?= $url_condicion_venta ?>', '<?= $url_create ?>')" class="btn btn-primary btn-sm btn-block"><span style="font-size: 1.4em; ">FACTURAR</span></button>
                    <?php } ?>
                </div>
            </div>
            <!--div class="col-lg-4"-->
                <!--?= /*yii\bootstrap\Button::widget([
                        'label' => 'Agregar +',
                        'options' => ['class' => 'btn','buttonType' => 'submit'],
                    ]);*/
                ?-->
            <!--/div-->
            <div id="detalle">
            <!-- con este div puedo refrescar detalles al igual que llamarlo y montarlo en tiempo real-->
                <?= $this->render('detalle', [
                    'products' => $products,
                ]) ?>

            </div>

        </div>
                <!--  <div class="clearfix"></div> -->
                <!-- <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Productos</h4></div> -->

    </div>
  </div>
