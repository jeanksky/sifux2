<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
use yii\jui\DatePicker;
use backend\models\Compra;//Prueba
use backend\models\Clientes;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use backend\models\ProductoServicios;
use backend\models\EncabezadoPrefactura;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
$load = \Yii::$app->request->BaseUrl.'/img/load.gif';
$create = \Yii::$app->request->BaseUrl.'/js/vistas/cabeza_prefactura/create.js';
/* @var $this yii\web\View */
/* @var $model backend\models\EncabezadoPrefactura */

$this->title = 'Crear Factura o Prefactura';
$this->params['breadcrumbs'][] = ['label' => 'Prefacturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if (isset($_REQUEST['id'])){
     echo $this->render('/cabeza-caja/ticket', [//aqui llama tiquet que imprime la factura correspondiente a este id
             'id' => $_GET['id'],
         ]);
   }
?>

<?php $products = Compra::getContenidoCompra(); ?>
<?php $vendedor = Compra::getVendedor(); ?>
<?php $cliente = Compra::getCliente(); ?>
<?php $tipopago = Compra::getPago(); ?>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!--script src="http://ajax.googleapis.com/ajax/libs/prototype/1.7.3.0/prototype.js"></script-->
<script type="text/javascript">
function bloquear_btn(this_boton) {
  var $btn = $(this_boton).button('loading');
}
//Funcion que hace abrir modal desde la tecla[F2]
    window.onkeydown = tecla;
      function tecla(event) {
        //event.preventDefault();
        num = event.keyCode;

        if(num==113)
          $('#modalinventario').modal('show');//muestro la modal

      }
    $(document).ready(function(){
       // $('[data-toggle="modal"]').tooltip();
        $('[data-toggle="modal"]').popover();
        $('[data-toggle="tooltip"]').tooltip();
    //para poder usarlo en caso de que necesitemos incluir html en data-content de popover
    //http://stackoverflow.com/questions/13202762/html-inside-twitter-bootstrap-popover
        $('[data-toggle="popover"]').popover();

        var caja_multiple = "<?= Yii::$app->params['caja_multiple'] ?>";
        if(caja_multiple == 1){
          verificarEstadoHAciendoGometa();
          //confirmar_facturas_hacienda();
          //setTimeout('confirmar_facturas_hacienda()', 900 );
          //setInterval('verificarEstadoHAciendoGometa()',60000); /*1 min*/
          //setInterval('confirmar_facturas_hacienda()', 120000);
        }
    });

    function confirmar_facturas_hacienda() {
      var estado_hacienda =  document.getElementById("estado_h").value;
      if (estado_hacienda == 'OK') {
        url_confirmar_facturas = "<?= Yii::$app->getUrlManager()->createUrl('cabeza-caja/confirmar_facturas_hacienda') ?>";
        $.ajax({
          url: url_confirmar_facturas,
          type:"post",
            data: { },
            beforeSend: function() {},
            success: function(data){ confirmar_movimientos_hacienda(); },
            error: function(msg, status,err){}
        });
      }
    }

    /*vehiel 05-12-2018
    metodo que verificacion del estado del tarro de haciendo con el api de gometa*/
    function verificarEstadoHAciendoGometa(){
      $.ajax({
        'url': "<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-caja/consulta_estado_hacienda') ?>",
        'dataType':'json',
        success: function(data){
          document.getElementById("estado_h").value = data.status;/*los estados que podemos obtener son OK, BAD*/
          confirmar_facturas_hacienda();
        },
        error:function(){
        }
      });
    }

    function confirmar_movimientos_hacienda(){
      url_confirmar_movimientos = "<?= Yii::$app->getUrlManager()->createUrl('devoluciones-producto/consultar_estados_movimiento') ?>";
      $.ajax({
          url: url_confirmar_movimientos,
          type:"post",
          data: { },
          beforeSend: function() {},
          success: function(data){},
          error: function(msg, status,err){ //alert('Algo no esta bien');
          }
      });
    }

    //espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
      var nav4 = window.Event ? true : false;
      var key = nav4 ? evt.which : evt.keyCode;
      return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

    function obtenerClienteCod(){

        var idCliente = document.getElementById("cod-cliente").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/cliente2') ?>",
            type:"post",
            data: {'idCliente': idCliente},
        }) ;
    }

   function obtenerCliente(){
        var idCliente = document.getElementById("ddl-cliente").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/cliente2') ?>",
            type:"post",
            data: {'idCliente': idCliente},
            /*success: function(data){
             $('#resultado').select2('val', data);
            },
            error: function(msg, status,err){
                            $('#resultado').val('Dato no encontrado');
                        }*/
        }) ;
    }

    function run_pago(){
        var tipoFacturacion = document.getElementById("tipoFacturacion").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addpago') ?>",
            type:"post",
            data: {'tipoFacturacion': tipoFacturacion},
        }) ;
    }

/*Agrega el agente comision*/
        function run_AgenteComision(){
        var AgenteComi = document.getElementById("idAgenteComision").value;
        $.ajax({
            url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addagentecomision') ?>",
            type:"post",
            data: {'idAgenteComision': AgenteComi},
        }) ;
    }

    //activo el id redultadoBusqueda pendiente de una nueva busqueda
    $(document).ready(function() {
        $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');
        //document.getElementById("codProd").select();
    });

    //me permite buscar por like los productos a BD
    function buscar() {
        var textoBusqueda_cod = $("input#busqueda_cod").val();
        var textoBusqueda_des = $("input#busqueda_des").val();
        var textoBusqueda_fam = $("input#busqueda_fam").val();
        var textoBusqueda_cat = $("input#busqueda_cat").val();
        var textoBusqueda_cpr = $("input#busqueda_cpr").val();
        var textoBusqueda_ubi = $("input#busqueda_ubi").val();
        if (textoBusqueda_cod == "" && textoBusqueda_des == "" && textoBusqueda_fam == "" && textoBusqueda_cat == "" && textoBusqueda_cpr == "" && textoBusqueda_ubi == "") {
            $("#resultadoBusqueda").html('<p>ESPERANDO BUSQUEDA</p>');

        } else {

            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/busqueda_producto') ?>",
                type:"post",
                data: {
                    'valorBusqueda_cod' : textoBusqueda_cod,
                    'valorBusqueda_des' : textoBusqueda_des,
                    'valorBusqueda_fam' : textoBusqueda_fam,
                    'valorBusqueda_cat' : textoBusqueda_cat,
                    'valorBusqueda_cpr' : textoBusqueda_cpr,
                    'valorBusqueda_ubi' : textoBusqueda_ubi },
                beforeSend : function() {
                  $("#resultadoBusqueda").html('<center><img src="<?= $load_buton; ?>" style="height: 32px;"></center>');
                },
                success: function(data){
                    $("#resultadoBusqueda").html(data);
                },
                error: function(msg, status,err){
                 //alert('No pasa 286');
                }
            });
        }
    }

//Transformar a mayúsculas todos los caracteres
function Mayuculas(key, tx){
  if(key == 13 && tx=='') {//si la tecla que toco es enter y el inout está vacio entonces...
    document.getElementById("facturar").click();//...que proceda a facturar
  }
  //Retornar valor convertido a mayusculas
	return tx.toUpperCase();
}

//Funcion para que me permita ingresar solo numeros
function isNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ( (charCode > 47 && charCode < 58))
            return true;
        return false;
    }

//agrega productos al dar enter
function agrega_producto(codProdServicio){
    var descuento = $("#descuento").val();
    //var codProd = document.getElementById('codProd');
    //alert(codProdServicio);
    var codigolimpio_pro = String(codProdServicio);
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/additem') ?>",
        type:"post",
        data: { 'codProdServicio': codigolimpio_pro, 'cantidad': 1, 'descuento' : descuento },
        success: function(data){
            //obtengo el detalle y lo refreco en el div detalle

            $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle') ?>",
                        function( data ) {
                          $('#modalinventario').modal('hide');
                          $('#detalle').html(data);
                          $('#descuento').val('').change();
                          $('#codProd').val(null)

                          $("input#busqueda_cod").val(null);
                          $("input#busqueda_des").val(null);
                          $("input#busqueda_ubi").val(null);
                          activar_input_ingreso_producto();
                          //codProd.value = null;
                          //$("#addpro").load(location.href+' #addpro','');
                         //permite que el input esté seleccionado
                          //document.getElementById("codProd").select();

                        });
         //alert('Si pasa');
         //$("#conten_pro").html('index.php?r=cabeza-prefactura%2Ftabla');

        //$("#conten_pro").load(location.href+' #conten_pro','');
        //$("#codProdServicio").select2("val", ""); //vaciar contenido de select2
        //$("#descuento").select2("val", "");
         //location.reload();
        },
        error: function(msg, status,err){
         //alert('No pasa');
         //$("#conten_pro").load('cabeza-prefactura/create');
         //$.update('#texto-msg');

        }
    });
}

//muncion que me permite refrescar el input donde se esta ingresandoi productos
function activar_input_ingreso_producto() {
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addpro') ?>",
    function( data_prod ) {
      //agregamos nuevo input para agregar productos
      document.getElementById("addpro").innerHTML = data_prod;
      //activamos el cursor para ingresar el producto
      document.getElementById("codProd").select();
    });
}

function deleteitem(id, idpro, can) {
  confirmar=confirm("¿Seguro que deseas regresar este producto?");
  if (confirmar){
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/deleteitem') ?>",
    {'id' : id, 'idpro' : idpro, 'can' : can},
    function( data_prod ) {
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle') ?>",
                  function( data ) {
                    $('#modalinventario').modal('hide');
                    $('#detalle').html(data);
                    $('#descuento').val('').change();
                    $('#codProd').val(null)

                    $("input#busqueda_cod").val(null);
                    $("input#busqueda_des").val(null);
                    $("input#busqueda_ubi").val(null);
                    activar_input_ingreso_producto();
                  });
    });
  } else {}
}

//setTimeout("refresca()", 500);
function obtenerVendedor(){

    var idFuncionario = document.getElementById("ddl-codigoVendedor").value;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/vendedor2') ?>",
        type:"post",
        data: {'idFuncionario': idFuncionario},
        /*success: function(data){
         $('#resultado2').val(data);
        },
        error: function(msg, status,err){
                        $('#resultado2').val('Dato no encontrado');
                    }*/
    }) ;
}

function obtenMedio(valor)
{
   // document.getElementById("resultado1").innerHTML=valor;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addmedio') ?>",
        type:"post",
        data: {'medio': valor},
    }) ;
}

function habilitar_ot(modo) {
  $.ajax({
      url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/habilitar_ot') ?>",
      type:"post",
      data: {'modo': modo},
  }) ;
}

function obtenCom(valor)
{
   // document.getElementById("resultado1").innerHTML=valor;
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addcom') ?>",
        type:"post",
        data: {'comprobacion': valor},
    }) ;
}

function agrega_observacion(observacion){
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addobservacion') ?>",
        type:"post",
        data: {'observacion': observacion},
    }) ;
}

$(document).ready(function () {
            (function ($) {
                $('#filtrar').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));
            $('#tabla_lista_productos').dataTable({
            "sPaginationType" : "full_numbers"//DAMOS FORMATO A LA PAGINACION(NUMEROS)
            });


        });

//funcion para guardar en sesion la informacion OT
function datos_ot(valor, inp) {

  $.ajax({
    url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/datos_ot') ?>",
    type:"post",
    data: { 'valor' : valor, 'inp' : inp },
    beforeSend: function() {
    },
    success: function(valores){
      if (inp=='cl') {
        if (valores != 'aseptado') {
            $("#notif_ot").html(valores);
            $('#ddl-cliente-ot').val('').trigger('change.select2');
        } else {
          $("#notif_ot").html('');
        }
      } else if (inp=='ma') {
        $('.modelo_ot').html(valores).fadeIn();
      }
    }
  });
}


//ignorar el % de servicioRestaurante
$(document).on('change', 'input[name="ign_por_ser"]', function(e) {
    if (this.checked == true){
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/ignorar') ?>",
      {'tipo': 'porc_se', 'estado' : 'checked'},
        function( data_prod ) {
          $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle') ?>",
                      function( data ) {
                        $('#modalinventario').modal('hide');
                        $('#detalle').html(data);
                        $('#descuento').val('').change();
                        $('#codProd').val(null)
                        $("input#busqueda_cod").val(null);
                        $("input#busqueda_des").val(null);
                        $("input#busqueda_ubi").val(null);
                        activar_input_ingreso_producto();
                      });
        });
      }
  else {
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/ignorar') ?>",
    {'tipo': 'porc_se', 'estado' : ''},
      function( data_prod ) {
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle') ?>",
                    function( data ) {
                      $('#modalinventario').modal('hide');
                      $('#detalle').html(data);
                      $('#descuento').val('').change();
                      $('#codProd').val(null)
                      $("input#busqueda_cod").val(null);
                      $("input#busqueda_des").val(null);
                      $("input#busqueda_ubi").val(null);
                      activar_input_ingreso_producto();
                    });
      });
      }
      });

//acordeon de modificar producto
var visto = null;
  function modificar_producto(position) {
    div = document.getElementById('tr_'+position);
    div.style.display = (div==visto) ? 'none' : 'block';
    if (visto != null)
    visto.style.display = 'none';
    visto = (div==visto) ? null : div;
  }

  function aplicardescuentogeneral_cr(){
    var descuento = $("#descuento").val();
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/aplicardescuentogeneral_cr') ?>",
      {'porc_descuento' : descuento},
      function( result ) {

      });
  }

  function modificar_linea_cr(linea, position) {
    var cantidad = document.getElementById("can"+linea).value;
    var porc_descuento = document.getElementById("dct"+linea).value;
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/modificar_linea_cr') ?>",
    {'cantidad_input' : cantidad, 'porc_descuento' : porc_descuento, 'position' : position},
    function( result ) {
      if (result!='vacio') {
        $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/detalle') ?>",
            function( data ) {
              $('#detalle').html(data);
            });
            $("#campo_notificacion").html(result);
      } else {
        $("#campo_notificacion").html('<br><div class="alert alert-danger" role="alert"> <strong>Error!</strong> Disculpa, no hay más cantidad en stock para este producto.</div>');
      }
      setTimeout(function() {//aparece la alerta en un milisegundo
          $("#campo_notificacion").fadeIn();
      });
      setTimeout(function() {//se oculta la alerta luego de 3 segundos
          $("#campo_notificacion").fadeOut();
      },8000);
    });
  }


</script>
<style>
    #modalinventario .modal-dialog{
    width: 74%!important;
    /*margin: 0 auto;*/
    }
    #modal_facturar .modal-dialog{ width: 30%!important; }
</style>
<div class="encabezado-prefactura-create">
    <!--h3> Html::encode($this->title) </h3-->
    <?php if (Yii::$app->params['caja_multiple']==true): ?>
      <span style="float:right; font-size: 40pt; color: #797B3B; background-color: #e8eaee; padding-left: 10px; padding-right: 10px;">CAJA #1</span>
    <?php else: ?>
      <span style="float:right; font-size: 40pt; color: #797B3B; background-color: #e8eaee; padding-left: 10px; padding-right: 10px;">RECEPCIÓN</span>
    <?php endif; ?>
        <?= Html::a('<span class="fa fa-arrow-left"></span> Salir sin borrar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('<span class="fa fa-arrow-left"></span> <span class="fa fa-eraser"></span> Borrar y salir', ['regresarborrar'], [
            'class' => 'btn btn-danger',
            'data-loading-text'=>'OK...',
            'onclick' => 'javascript:bloquear_btn(this)',
            'data' => [
                'confirm' => '¿Seguro que quieres desechar los datos ya escritos y salir de la prefactura?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="fa fa-refresh"></span> Desechar y limpiar', ['deleteall'], [
            'class' => 'btn btn-danger',
            'data-loading-text'=>'OK...',
            'onclick' => 'javascript:bloquear_btn(this)',
            'data' => [
                'confirm' => '¿Seguro que quieres desechar y limpiar los datos de esta prefactura?',
                'method' => 'post',
            ],
        ]) ?>

        <!--?php if($products && $tipopago && $vendedor && $cliente) : ?-->

          <?= Html::a('<span class="fa fa-floppy-o"></span> Guardar como pre-factura', ['complete'], ['class' => 'btn btn-primary']) ?>
          <?= Yii::$app->params['caja_multiple']==false ? Html::a('<span class="fa fa-inbox"></span> Enviar a Caja', ['completerminado'], [
          'class' => 'btn btn-primary',
          'data' => [
                      'confirm' => '¿Seguro de enviar a caja para cancelar?',
                      'method' => 'post',
                  ],
              ]) : '' ?>
          <?php
          $ot = Yii::$app->params['ot'];
          if ($ot == true) {
            if (@$get_ot = Compra::getOT()) {
              echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .Html::a('', '#', ['class' => 'fa fa-share fa-5x', 'onclick' => 'habilitar_ot(false)', 'data-toggle' => 'titulo', 'data-placement' => 'left', 'title' => Yii::t('app', 'DESHABILITAR O.T')]) ;
            } else {
              echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .Html::a('', '#', ['class' => 'fa fa-openid fa-5x', 'onclick' => 'habilitar_ot(true)', 'data-toggle' => 'titulo', 'data-placement' => 'left', 'title' => Yii::t('app', 'HABILITAR O.T')]) ;
            }
          } ?>

        <!--?php endif ?-->
    <input type="hidden" id="estado_h" value="NULLO">
    <?= $this->render('_form_', [
        //'model' => $model,
    ]) ?>
</div>
<?php
//-----------------------modal para mostrar productos y agregarlos
        Modal::begin([
            'id' => 'modalinventario',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Inventario de productos</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        //http://blog.collectivecloudperu.com/buscar-en-tiempo-real-con-jquery-y-bootstrap/

        echo '<div class="well"><div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cod', 'class' => 'form-control', 'placeholder' =>'CÓDIGO LOCAL', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_des', 'class' => 'form-control', 'placeholder' =>'DESCRIPCIÓN', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_fam', 'class' => 'form-control', 'placeholder' =>'CATEGORÍA', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cat', 'class' => 'form-control', 'placeholder' =>'CÓDIGO ALTERNO', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_cpr', 'class' => 'form-control', 'placeholder' =>'CODIGO PROVEEDOR', 'onchange' => 'javascript:buscar()']).'
                                </div>
                                <div class="col-lg-2">
                                  '.Html::input('text', '', '', [/*'autofocus' => 'autofocus',*/ 'size' => '80', 'id' => 'busqueda_ubi', 'class' => 'form-control', 'placeholder' =>'UBICACIÓN', 'onchange' => 'javascript:buscar()']).'&nbsp;
                                </div>

                <table class="table table-hover" id="tabla_lista_productos">';
        echo '<thead>';
        printf('<tr><th width="150">%s</th>
                    <th width="380">%s</th>
                    <th width="100">%s</th>
                    <th width="130">%s</th>
                    <th width="120">%s</th>
                    <th width="50">%s</th>
                    <th width="140">%s</th>
                    <th class="actions button-column" width="60">&nbsp;RESERVADO</th>
                </tr>',
                        'CÓDIGO LOCAL',
                        'DESCRIPCIÓN',
                        'CANT.INV',
                        'COD.ALTER',
                        'CÓD.PROV',
                        'UBICACIÓN',
                        'PRECIO + IV'
                        );
                echo '</thead>';
               /* $encabezado = new EncabezadoPrefactura();
                $model_Prod = $encabezado->productoslista();
                echo '<tbody class="buscar">';
                foreach($model_Prod as $position => $product) {
                    $accion = Html::a('', '', ['class'=>'glyphicon glyphicon-plus', 'data-value'=>$product['codProdServicio'],'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Agregar este producto', 'onclick'=>'javascript:agrega_producto($(this).data("value"))']);
                    $presiopro = '₡ '.number_format($product['precioVentaImpuesto'],2);
                    printf('<tr><td>%s</td><td class="des">%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="actions button-column">%s</td></tr>',


                            $product['codProdServicio'],
                            $product['nombreProductoServicio'],
                            $product['cantidadInventario'],
                            $presiopro,
                            $accion
                            );
                }//fin de foreach
                echo '</tbody>';
                  */
                echo '</table>
                        <div style="height: 200px;width: 100%; overflow-y: auto; ">

                            <div id="resultadoBusqueda">
                            </div>

                        </div>
                    </div>';

        Modal::end();
//----modal para fa facturacion
Modal::begin([
    'id' => 'modal_facturar',
    'size'=>'modal-lg',
    'header' => '<center><h4 class="modal-title">Facturar (Cancelacion o Crédito)</h4></center>',
    'footer' => Html::a('Salir', ['cabeza-prefactura/create'], ['class' => 'btn btn-primary']),
]);
?>
<div class='panel'>
  <div class="panel-body">
    <center><span style="font-size: 1.5em; color:#757777;">Condicion de venta y medio de pago</span></center>
    <?php //AQUI VOY A VERIFICAR SI EL CLIENTE ES EXONERADO PARA PROCEDER A LLENAR EL DOCUMENTO DE EXONERACIÓN
    if(@$cliente = Compra::getCliente())
    {
      if(@$cliente_ob = Clientes::find()->where(['idCliente'=>$cliente])->one())
      {
        /*if ($cliente_ob->iv == true) {
          $num_comprovante = Html::input('text', 'No. Comprovante', '',
              [
                'size' => '10',
                'id' => 'no_comprovante_exonerado',
                'placeholder'=>'Núm de comprovante',
                'style'=>'font-family: Fantasy; font-size: 18pt; text-align: center;',
                'class' => 'form-control input-sm',
                'autocomplete' => "off"
              ]);

          $fecha_documento = DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName','value'=>date("d-m-Y"),
          'options' => ['class'=>'form-control input-sm', 'style'=>'font-family: Fantasy; font-size: 18pt; text-align: center;', 'readonly' => true, 'placeholder'=>'Fecha documento contigencia', 'id'=>'fecha_documento', 'onchange'=>'']]);

          $razon_emision = Html::input('text', 'Razon emisión', '',
              [
                'size' => '10',
                'id' => 'razon_emision',
                'placeholder'=>'Razón de emisión',
                'style'=>'font-family: Fantasy; font-size: 18pt; text-align: center;',
                'class' => 'form-control input-sm',
                'autocomplete' => "off"
              ]);
          $documento_contingencia = Html::fileInput('name_file', 'Archivo de contingencia', ['id'=>'file_doc_contingencia', 'accept'=>'image/*,.pdf']);
          echo '<div class="col-lg-12">
                <br>
                  <div class="panel panel-primary">
                    <div class="panel-body">
                      <h4>Este cliente es exonerado, por favor llene el siguiente formulario:<h4>
                      <div class="col-sm-6">
                        '.$num_comprovante.'<br>
                      </div>
                      <div class="col-sm-6">
                        '.$fecha_documento.'<br>
                      </div>
                      <div class="col-sm-12">
                        '.$razon_emision.'<br>
                      </div>
                      <div class="col-sm-12">
                        '.$documento_contingencia.'
                      </div>
                    </div><br>
                  </div>
                </div>';
        }*/
      }
    }
    ?>
    <div id="condicion_venta"><br><br><center><span style="font-size: 2em; color:#E49E53;">(Seleccione la factura a emitir)</span></center><br><br>
      <strong>Nota:</strong> No emita una factura cuando Hacienda este Fuera de servicio.
    </div>
  </div>
</div>
<?php
Modal::end();
?>
<script type="text/javascript" src="<?= $create ?>"></script>
<script type="text/javascript">

$(document).keydown(function (event) {
  switch(event.keyCode)
  {
    var currentRow = $(".row_selected").get(0);
    //arrow down
    case 40:
      $(currentRow).next().addClass("row_selected");
      $(currentRow).removeClass("row_selected");
      break;
    //arrow up
    case 38:
      $(currentRow).prev().addClass("row_selected");
      $(currentRow).removeClass("row_selected");
      break;
  }
});

</script>
