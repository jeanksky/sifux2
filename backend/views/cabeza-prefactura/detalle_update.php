<?php
use backend\models\Clientes;
use backend\models\Compra_update;
use backend\models\DetalleOrdServicio;
use backend\models\Servicios;
use backend\models\Familia;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\MovimientoCredito;
use backend\models\Empresa;
use kartik\widgets\Select2;
$products = Compra_update::getContenidoComprau();

//echo $model->idCabeza_Factura;
?>
<style media="screen">

table.scroll {
	 width: 100%;  /* Optional */
	/* border-collapse: collapse; */
	border-spacing: 0;
	/*border: 5px solid #E7D8D8;*/
}

table.scroll tbody,
table.scroll thead { display: block; }



table.scroll tbody {
	height: 300px;
	overflow-y: auto;
	overflow-x: hidden;
}

/*tbody { border-top: 2px solid black; }*/

tbody td, thead th {
	/* width: 20%; */ /* Optional */
	border-right: 1px solid white;

	/* white-space: nowrap; */
}
tbody #td10, thead #td10 {
	 width: 10%;
}
tbody #td40, thead #td40 {
	 width: 40%;
}
tbody #td5, thead #td5 {
	 width: 5%;
}

tbody td:last-child, thead th:last-child {
	border-right: none;
}
</style>
<script type="text/javascript">
	function getTotal() {
		$.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/getpreciototalu') ?>",
                function( data ) {
                  $('#resumen').html(data);
                });
	}
</script>
	<div class="col-lg-9"><br>
    <div id="result"></div>
      <div class="table-responsive">
        <div id="conten_pro">
            <?php
            //http://www.programacion.com.py/tag/yii
                //echo '<div class="grid-view">';
                echo '<table class="items table scroll table-inverse">';
                echo '<thead>';
								echo '<tr>
												<th id="td10">CANTIDAD</th>
												<th id="td10">CÓDIGO</th>
												<th id="td40">DESCRIPCIÓN</th>
												<th id="td10" style="text-align:right;">PREC.UNIT</th>
												<th id="td10" style="text-align:right;">DESCUENTO</th>
												<th id="td5">IV %</th>
												<th id="td10" style="text-align:right;">TOTAL</th>
												<th id="td5" class="actions button-column">&nbsp;</th>
											</tr>';
                echo '</thead>';
            if($products) {
            $nivel = 'Presiona para mostrar la orden de servicio, verifique que esté al 100% antes de facturar o acreditar';
            ?>

            <?php
            $orden = Html::a('Orden de Servicio', ['#'], [
                                'id' => 'activity-index-link-update',
                                'data-toggle' => 'modal',//'data-placement'=>'left',
                                'data-trigger'=>'hover', 'data-content'=>$nivel,
                                'data-target' => '#modalOrden',
                                'data-url' => Url::to(['orden-servicio/view', 'id' => $model->idOrdenServicio]),
                                'data-pjax' => '0',
                                'title' => Yii::t('app', 'Orden de Servicio correspondiente'),
                                ]);
            //$orden = Html::a('Orden de Servicio', '../web/index.php?r=orden-servicio%2Fview&id='.$model->idOrdenServicio);
            echo '<tbody>';
							//arsort($products);
                foreach($products as $position => $product) {
										$servicios = ''; $precio_serv = ''; $cod_serv = '';
										if ($product['desc'] == $orden) {
											$det_ord_ser = DetalleOrdServicio::find()->where(['idOrdenServicio'=>$model->idOrdenServicio])->all();
											foreach($det_ord_ser as $row){
												//Obtengo una instancia de Servicios donde el codigo de servicio es igual al servicio que recorre el bucle
												$modelServicio = Servicios::find()->where(['codProdServicio'=>$row['producto']])->one();
												//Obtenemos una instancia de familia donde el código de familia es igual al servicio obtenido de la instancia anterior
												$fami = Familia::find()->where(['codFamilia'=>$row['familia']])->one();
												$cod_serv .= '<br><span style="color: #288f28;">' . $modelServicio->codProdServicio.'</span>';
												$servicios .= '<br><span style="color: #288f28;">' . $fami->descripcion . ' - ' . $modelServicio->nombreProductoServicio.'</span>';
												$precio_serv .= '<br><span style="float:right; color: #288f28;">' . number_format($modelServicio->precioMinServicio,2).'</span>';
											}
										}
                    $espaciocantidad = Compra_update::getEstadoFacturau()=='Crédito Caja' || Compra_update::getEstadoFacturau()=='Caja' || $product['desc'] == $orden ? $product['cantidad'] :
										Html::textInput('cantidad_'.$position,
                                 $product['cantidad'], array(
                                    'onkeypress'=>'return isNumberDe(event)',
																		'style'=>'text-align: center;',
                                    'class' => 'cantidad_'.$position, 'size' => '4'
                                    ));
                    $eliminarlineaproducto = Compra_update::getEstadoFacturau()=='Crédito Caja' || Compra_update::getEstadoFacturau()=='Caja' || Compra_update::getEstadoFacturau()=='Apartado' || $product['desc'] == $orden ? "" : Html::a('', ['/cabeza-prefactura/deleteitemu', 'id0'=>$product['cod'],
										'id1'=>$position, 'id2'=>$model->idCabeza_Factura, 'can'=>$product['cantidad']], ['class'=>'glyphicon glyphicon-trash', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Descartar este producto', 'data' => [
                                    'confirm' => '¿Está seguro de quitar este producto de la prefactura?',],]);

										$empresa = Empresa::findOne(1);
												$cantidad_descuento = $empresa->cantidad_descuento;
												$array_descuento = null;
												for ($i=1; $i <= $cantidad_descuento; $i++) {
													$array_descuento[$i] = $i;
												}
										$dcto = number_format(($product['dcto']*100)/$product['precio'],2);
										$desc = Select2::widget([ //Esto lo vamos a usar sin modelo porque solo será visual
														'name' => '',
														'value'=> (int)$dcto,
														'data' => $array_descuento,//[1 => "1%", 2 => "2%", 3 => "3%", 4 => "4%", 5 => "5%", 6 => "6%", 7 => "7%", 8 => "8%", 9 => "9%", 10 => "10%"],
														'options' => [
																'placeholder' => 'Seleccione %',
																'id'=>'dct'.$product['cod'],
																'onchange'=>'javascript:activar_input_ingreso_producto()',
																'multiple' => false //esto me ayuda a que solo se obtenga uno
														],
														'pluginOptions' => ['initialize'=> true,'allowClear' => true]
												]);
                    echo '<tr style="display: none;" id="tr_'.$position.'">
														<td colspan="8" id="td10" bgcolor="#CAF5AA">
															<div class="col-md-7">
																<h2>'.$product['cod'].'<br><small>'.$product['desc'].'</small><h2>
															</div>
															<div class="col-md-3">

															</div>
															<div class="col-md-2">
															Cantidad:
															<input type="text" value="'.$product['cantidad'].'" id="can'.$product['cod'].'" class="form-control form-control-sm" />
															Descuento(%):
															'.$desc.'
																'.Html::a('<span class="fa fa-floppy-o"></span> Guardar', NULL, ['class' => 'btn', 'style'=>'cursor:pointer;', 'onclick'=>'modificar_linea_up("'.$product['cod'].'", '.$position.','.$model->idCabeza_Factura.')']).'
															</div>
														</td>
													</tr>
													<tr>
														<td id="td10">'.$espaciocantidad.'</td>
			                    	<td id="td10"><a href="javascript:modificar_producto('.$position.');" >'.$product['cod'].'</a></td>
			                    	<td id="td40">'.$product['desc'].$servicios.'</td>
			                    	<td id="td10" align="right" >'.number_format($product['precio'], 2).$precio_serv.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			                    	<td id="td10" class = "textright descuent_'.$position.'" align="right">'.number_format($product['cantidad']*$product['dcto'],2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			                    	<td id="td5" align="right">'.$product['iva'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			                    	<td id="td10" class="textright total_'.$position.'" align="right">'.number_format(($product['precio']*$product['cantidad']),2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
														<td id="td5" class="actions button-column">'.$eliminarlineaproducto.'</td>
													</tr>';
                        //http://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
                        $this->registerJs("
																			$(document).on('change', '.cantidad_".$position."', function(){
																				$.ajax({
																				    url:'".Yii::$app->getUrlManager()->createUrl('/cabeza-prefactura/updatecantidadu')."',
																				    data: $('.cantidad_".$position."'),
																						dataType: 'json',
																				    success: function(result) {
																				    $('.total_".$position."').html(result.total);
																						$('.descuent_".$position."').html(result.descuento);
																						$('#can".$product['cod']."').val($('.cantidad_".$position."').val());
																				    getTotal();
																				    },
																				    error: function() {
																				    },
																				});
																			});


                                ");
                      /*  $this->registerJs("
                                    $('.precio_".$position."').keyup(function() {
                                        $.ajax({
                                            url:'".Yii::$app->getUrlManager()->createUrl('/cabeza-prefactura/updatepreciou')."',
                                            data: $('.precio_".$position."'),
                                            success: function(result) {
                                            $('.total_".$position."').html(result);
                                            $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                            '/cabeza-prefactura/getpreciototalu')."');
                                            },
                                            error: function() {
                                            },

                                            });
                                    });
                                ");*/

                }
            ?>
            <?php
            }
						echo @$products ? '<tr>
																	<td colspan="8" bgcolor="#CAF5AA" id="td10" align="center">El orden de ingreso es de abajo hacia arriba (el último ingresado es el primero en la lista)</td>
															</tr>
															' : '<tr>
																			<td colspan="8" bgcolor="#F4AE6C" id="td10" align="center"><h3>Área vacía, esperando un producto.</h3></td>
																	</tr>';
						echo "</tbody>";
                $alerta = ''; $checked_cred_venc = '';
                if(@$clientealert = Compra_update::getClienteu()) {
                    if(@$modelclient = Clientes::find()->where(['idCliente'=>$clientealert])->one())
                    {
                        if ($modelclient->credito == 'Si' && $model->estadoFactura == 'Pendiente') {
                            $monto_ya_acreditado = $modelclient->montoTotalEjecutado;
                            $montodisponiblecredito = $modelclient->montoMaximoCredito - $monto_ya_acreditado;
                            $alerta = '
                            <div class="alert alert-info alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>¡Atención!</strong> Si esta prefactura será acreditada considere que '.$modelclient->nombreCompleto.' tiene un monto disponible para crédito de ₡'.number_format($montodisponiblecredito,2).'. Si se excede ese monto el sistema no permitirá acreditar la factura. Si esta prefactura no será acreditada ignore este informe.
                            </div> ';
                            $facturasCredito = MovimientoCredito::find()->where(['=','idCliente', $modelclient->idCliente])->andWhere("estadoFactura = :estadoFactura AND fecha_vencimiento < :tipofecha", [':estadoFactura'=>'Crédito', ':tipofecha' => date('Y-m-d')])->one();
                            if (@$facturasCredito) {
                              $checked_cred_venc = 'checked';
                            }
                        }

                    }

                    }
								$ignorar_servicio = Yii::$app->params['servicioRestaurante'] == false ? '' : '<h4>Ignorar % servicio. <span style="float:right; "><input type="checkbox" style="transform: scale(2);" name="ign_por_ser" '.$model->ignorar_porc_ser.'/></span></h4>';
                echo '</table>
				</div>
		  </div>
		</div>
		<div class="col-lg-3"><br>
			<div class="well">
				<h4>Crédito vencido. <span style="float:right"><input type="checkbox" style="transform: scale(2);" name="ign_cre_ven" '.$checked_cred_venc.' disabled /></span></h4>
				'.$ignorar_servicio.'
			</div>
		</div>
		<div class="col-lg-3">
      <div class="well">
      	<div id="resumen">'
      	.Compra_update::getTotalu().
    		'</div>
    	</div>
    </div>
    <div class="col-lg-12">
        <div class="alerta_r">'.$alerta.'</div>
    </div>';
            ?>
		<div class="col-lg-12">
        <div class="input-group">
           <span class="input-group-addon" id="sizing-addon3">Observaciones</span>
           <?php
            echo Html::input('text', '', $model->observacion, ['id' => 'observacion', 'class' => 'form-control', 'placeholder' =>'Agregue alguna observación', 'onKeyUp' => 'javascript:agrega_observacion(this.value)']); ?>
        </div>
    </div>
