<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Clientes;//para obtener el cliente
use backend\models\OrdenServicio;//para obtener el id del mecanico
use backend\models\Mecanicos;//para obtener el nombre del mecanico
use backend\models\EncabezadoPrefactura; //para obtener los atributos
use backend\models\FacturaOt;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\widgets\AlertBlock;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EncabezadoPrefacturaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de Prefacturas';
$this->params['breadcrumbs'][] = $this->title;

if (isset($_REQUEST['id'])){
     echo $this->render('/cabeza-caja/ticket', [//aqui llama tiquet que imprime la factura correspondiente a este id
             'id' => $_GET['id'],
         ]);
   }
?>
<style media="screen">
#scroller-wrapper {
  width: 100%;
  height: 100%;
}

#lista_apartados {
  height: 100%;
  overflow-x: auto;
  white-space: nowrap;
}

.elem {
  height: 100%;
  display: inline-block;
  width: 280px; margin: 0 auto 0 auto; margin-left: 20px;
}

#mov_apartados .modal-dialog{
width: 30%!important;
/*margin: 0 auto;*/
}
</style>
<div class="encabezado-prefactura-index">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

     <p style="text-align:right">
        <?= Yii::$app->params['apartados'] == true ? Html::a('<i class="fa fa-heart" aria-hidden="true"></i> Lista de apartados', '#', ['class' => 'btn btn-default', 'onclick'=>'javascript:lista_apartados()']) : '' ?>
        <?= Html::a('Crear nueva Prefactura', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="panel panel-default" id="apartados" style="display: none;">
      <div class="col-lg-12">
        <h4>Busqueda de Apartados <small>(Con un máximo de 25 resultados por consulta)</small></h4>
        <div class="pull-right">
          <a href="#" class="fa fa-times fa-3x" onclick='$("#apartados").hide("fast");'></a>
        </div>
        <div class="col-xs-3"><br>
          <input class="form-control input-sm" onchange="javascript:filtro_apartados()" id="id_apartado" placeholder="ID Prefactura (Apartado) + [Enter]" type="text">
        </div>
        <div class="col-xs-3"><br>
          <input class="form-control input-sm" onchange="javascript:filtro_apartados()" id="cliente" placeholder="Nombre del cliente + [Enter]" type="text">
        </div>
        <div class="col-xs-3"><br>
          <?= yii\jui\DatePicker::widget(['clientOptions' => ['dateFormat' => 'dd-mm-yy'], 'name' => 'attributeName',
          'options' => ['class'=>'form-control input-sm', 'autocomplete'=>'off', 'placeholder'=>'Fecha de registro', 'onchange'=>'javascript:filtro_apartados()',
          'id'=>'fecha_registro']]) ?>
        </div>
      </div>
      <div class="col-lg-12"><br>
        <div id="scroller-wrapper">
          <div id="lista_apartados"></div>
        </div>
      </div>
    </div>
    <p align="center"><strong>Nota:</strong> Cada ves que una orden de servicio se actualice se debe ingresar de nuevo a la pre-factura para que esta muestre el nuevo monto total a pagar.</p>
<div class="table-responsive">
    <div id="semitransparente">
         <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10;
$clienteOt = new Clientes();
$mecanicos = new Mecanicos();
$responsable_ot = Yii::$app->params['ot'] == true ? [
  'attribute' => 'tbl_factura_ot.idCliente',
  'label' => 'Responsable O.T',
  'format' => 'raw',
  'value' => function ($model, $key, $index, $grid) {
      if (@$get_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->one())
      {
          $nomb_cliente_ot = Clientes::findOne($get_ot->idCliente);
          if(@$nomb_cliente_ot)
          return $nomb_cliente_ot->nombreCompleto;
          else
          return 'No identificado';
      } else return 'No identificado';
  },
  'filter'=>Html::activeDropDownList($searchModel, 'tbl_factura_ot.idCliente',
    ArrayHelper::map($clienteOt->getNombreClienteOt(),'idCliente','nombreCompleto'),['class' => 'form-control','prompt' => 'Todos'])

  /*Select2::widget([
               'name' => $searchModel,
               'data' => ArrayHelper::map($clienteOt->getNombreClienteOt(),'idCliente','nombreCompleto'),
               'options' => ['placeholder' => '- Seleccione -','style'=>'width:150px'],
               'pluginOptions' => [
                   'allowClear' => true
               ],
               ])*/
] : ['value'=>function ($model, $key, $index, $grid) {return '►';}];

$mecanico_encargado = Yii::$app->params['tipo_sistema'] == 'lutux' ? [
            'attribute' => 'tbl_orden_servicio.idMecanico',
            'label' => 'Mecánico encargado',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $grid) {
                if ($id_mecanico = OrdenServicio::findOne($model->idOrdenServicio))
                {
                    $nomb_mecanico = Mecanicos::findOne($id_mecanico->idMecanico);
                    if($id_mecanico == '')
                    return 'Mecánico no asignado';
                    else
                    return $nomb_mecanico->nombre;
                } else return 'Mecánico no asignado';
              },
              'filter'=>Html::activeDropDownList($searchModel, 'tbl_orden_servicio.idMecanico',
              ArrayHelper::map($mecanicos->getNombreMecanicos(),'idMecanico','nombre'),['class' => 'form-control','prompt' => 'Todos'])
            ] : ['value'=>function ($model, $key, $index, $grid) {return '►';}];
?>
    <!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idCabeza_Factura',
            [
              'attribute' => 'idCabeza_Factura',
              'options'=>['style'=>'width:20px'],
            ],
            [
              'attribute' => 'fecha_inicio',
              'options'=>['style'=>'width:60px'],
            ],
            //'fecha_inicio',
            //'fecha_final',
            $responsable_ot,
            [
              'attribute' => 'idCliente',
              'label'=>'Cliente No Registrado',
            ],
            [
                        'attribute' => 'tbl_clientes.nombreCompleto',
                        'label' => 'Cliente Registrado',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            if($cliente = Clientes::findOne($model->idCliente))
                            {
                                return Html::a($cliente->nombreCompleto, '../web/index.php?r=clientes%2Fview&id='.$model->idCliente);
                            }else{
                                if ($model->idCliente=="")
                                return 'Cliente no asignado';
                                else
                                return $model->idCliente;
                            }
                        },

                          'filter'=>Html::activeInput('text', $searchModel, 'tbl_clientes.nombreCompleto', ['class' => 'form-control']),


            ],
            /*[
              'attribute' => 'tbl_encabezado_factura.idCliente',
            ],*/
            $mecanico_encargado,
            [
                        'attribute' => 'idOrdenServicio',
                        'options'=>['style'=>'width:100px'],
                        'label' => '# Orden Servicio',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $cliente = Clientes::findOne($model->idCliente);
                            if($model->idOrdenServicio == '')
                            return 'Sin Orden';
                            else
                            return Html::a($model->idOrdenServicio, '../web/index.php?r=orden-servicio%2Fupdate&id='.$model->idOrdenServicio);
                        },
            ],
            //'idOrdenServicio',
            // 'porc_descuento',
            // 'iva',
          //  ['attribute' =>'estadoFactura','options'=>['style'=>'width:10%']],

             [
                'attribute' => 'total_a_pagar',//'format' => ['decimal',2],
                'value' => function ($model, $key, $index, $grid) {return number_format($model->total_a_pagar,2);},
                'options'=>['style'=>'width:15%'],
             ],

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                  $ot = Yii::$app->params['ot'];
                  $ot_ident = '';
                  if ($ot == true) {
                    if (@$get_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->one()) {
                      $ot_ident = '<span style="float:right"><font size="4">O.T</font></span>';
                    }
                  }
                        return Html::a('<span class="">Atender</span>', ['/cabeza-prefactura/update', 'id' => $model->idCabeza_Factura], [
                            'id' => 'activity-index-link-update',
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Atender pre-factura pendiente'),
                            ]).$ot_ident;
                    },
            ],
            'options'=>['style'=>'width:7%'],],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>
<?php
Modal::begin([
    'id' => 'mov_apartados',
    'size'=>'modal-lg',
    'header' => '<center><h4 class="modal-title">ABONOS DE APARTADOS</h4><div id="notifica_apartados"><div></center>',
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
]);
echo "<div id='modal_mov_apartado'></div>";
Modal::end();
 ?>
<script type="text/javascript">

function lista_apartados() {

  $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/apartados') ?>",
    {'idCabeza_Factura': '%%', 'cliente': '%%', 'fecha_inicio': ''},
                  function( data ) {
                    $("#apartados").show("fast");
                    $('#lista_apartados').html(data);
  });
}

function filtro_apartados() {
  id_apartado = $("input#id_apartado").val();
  cliente = $("input#cliente").val();
  fecha_registro = $("input#fecha_registro").val();
  $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/apartados') ?>",
    {'idCabeza_Factura': '%'+id_apartado+'%', 'cliente': '%'+cliente+'%', 'fecha_inicio': fecha_registro},
                  function( data ) {
                    $("#apartados").show("fast");
                    $('#lista_apartados').html(data);
  });
}

function abrir_apartado(id) {
  window.location.href = "<?= Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/update') ?>&id="+id;
}

$(document).on('click', '#mostrar_mov_apartados', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $('#modal_mov_apartado').html(data);
                        $('#mov_apartados').modal();
                    }
                );
            }));
//espacio que solo permite numero y decimal (.)
function isNumberDe(evt) {
      var nav4 = window.Event ? true : false;
      var key = nav4 ? evt.which : evt.keyCode;
      return (key != 45 && (key <= 13 || key==46 || (key >= 48 && key <= 57)));
  }

  //envia datos a controlador para cargar cuenta bancaria en session y
  //desabilita el campo de comprovacion si la entidad y la cuenta estan completas
  function obtenCuenta(valor, tipo) {
          $.ajax({
              url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addcuenta-u') ?>",
              type:"post",
              data: {'cuenta': valor},
          });
          compruebatipopago(tipo);
  }

  //obtener el medio de pago
  function obtenMedio(valor, tipo)
  {
     // document.getElementById("resultado1").innerHTML=valor;
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addmedio-u') ?>",
          type:"post",
          data: {'medio': valor},
      }) ;
        compruebatipopago(tipo);
  }

  //Obtiene el comprobante para enviarlo al controlador y almacenarlo ensession
  function obtenCom(valor, tipo)
  {
    $.ajax({
        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/addcom-u') ?>",
        type:"post",
        data: {'comprobacion': valor},
    });
    compruebatipopago(tipo)
     // document.getElementById("resultado1").innerHTML=valor;
  }

  //compruebo que los datos estan llenos a la hora de cancelas de acuerdo al tipo de pago para abilitar o no el boton
  function compruebatipopago(tipo) {
      var entidadFinanc = '';
      var cuenta = '0';
      var comprobacion = '';
      //abono
      if (tipo==2) {
        entidadFinanc = document.getElementById("ddl-entidad").value;
        cuenta = document.getElementById("tipoCuentaBancaria").value;
        comprobacion = document.getElementById("comprobacion").value;
      } else if (tipo==3) {
        entidadFinanc = document.getElementById("medio_tarjeta").value;
        comprobacion = document.getElementById("comprobacion_tarjeta").value;
      } else if (tipo==4) {
        entidadFinanc = document.getElementById("ddl-entidad-cheque").value;
        comprobacion = document.getElementById("comprobacion_cheque").value;
      }

      if (entidadFinanc == '' ||  comprobacion == '' || cuenta == '') {
          if (tipo==2) $('#boton-abonar-deposito').addClass('disabled');
          else if (tipo==3) $('#boton-abonar-tarjeta').addClass('disabled');
          else if (tipo==4) $('#boton-abonar-cheque').addClass('disabled');
      }
      else {
          $.ajax({
              url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/compruebatipopago') ?>",
              type:"post",
              data: {'comprobacion': comprobacion, 'cuenta_deposito': cuenta, 'entidadFinanc': entidadFinanc, 'tipo': tipo},
              success: function(data){
                  if (data=='repetido') {
                    if (tipo==2) {
                      document.getElementById("nota_cancelacion").innerHTML = 'Comprobación rechasada, ya esta en registro';
                      $('#boton-abonar-deposito').addClass('disabled');
                    } else if (tipo==3) {
                      document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación rechasada, ya esta en registro';
                      $('#boton-abonar-tarjeta').addClass('disabled');
                    } else if (tipo==4) {
                      document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación rechasada, ya esta en registro';
                      $('#boton-abonar-cheque').addClass('disabled');
                    }
                  } else {
                    if (tipo==2) {
                      $('#boton-abonar-deposito').removeClass('disabled');
                      document.getElementById("nota_cancelacion").innerHTML = 'Comprobación aceptada';
                    } else if (tipo==3) {
                      $('#boton-abonar-tarjeta').removeClass('disabled');
                      document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación aceptada';
                    } else if (tipo==4) {
                      $('#boton-abonar-cheque').removeClass('disabled');
                      document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación aceptada';
                    }
                  }

              },
              error: function(msg, status,err){
                              alert('error en linea 328, consulte a nelux');
                          }
          });
          //$('#boton-abonar-deposito').removeClass('disabled');
      }

  }

  //Agrega un nuevo abono a la factura seleccionada
  function agregar_mov_apartado(tipo_pago_abono, idCa){
    var pago = document.getElementById("pagoparcial");
    var referencia = document.getElementById("referencia");
    if (tipo_pago_abono == 1) { //estos depositos som para cuando el tipo de pago de abono es con efectivo
      if (pago.value=='' || referencia.value=='') {
        pago.style.border = "3px solid red";
        referencia.style.border = "3px solid red";
      } else {
        var r = confirm("Está seguro de realizar este pago!");
        if (r == true) {
          $('#botones_abono').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
            $.ajax({
                  url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/agregar_mov_apartado') ?>",
                  type:"post",
                  data: { 'idCa': idCa, 'pago': pago.value, 'referencia': referencia.value, 'tipo_pago_abono': tipo_pago_abono },
                  beforeSend: function() {
                    $( "#notifica_apartados" ).html( '<span style="font-size:10pt;" class="label label-info"> Espere un momento mientras se realiza el proceso... </span>' );
                  },
                  success: function(data){
                    $.get(
                        "<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/mov_apartados') ?>",
                        { 'id': idCa },
                        function (modal_mov_apartado) {
                            $('#modal_mov_apartado').html(modal_mov_apartado);
                            filtro_apartados();//refresca los apartados mostrados al usuario
                        }
                    );
                      $( "#notifica_apartados" ).html( data );
                      setTimeout(function() {//aparece la alerta en un milisegundo
            							$("#notifica_apartados").fadeIn();
            					});
            					setTimeout(function() {//se oculta la alerta luego de 4 segundos
            							$("#notifica_apartados").fadeOut();
            					},15000);
                  },
                  error: function(msg, status,err){
                    alert(err);
                  }
              });
        } else { }

      }
    } else { //esta opcion sería para los abonos que son con deposoto  cheque o tarjeta
      if (pago.value=='' || referencia.value=='' ) {
        pago.style.border = "3px solid red";
        referencia.style.border = "3px solid red";
      } else {//*****asegurar que diferencie cuando es un abono de deposito con uno de tarjeta y con uno de cheque*++++
        var r = confirm("Está seguro de realizar este pago!");
        ddl_entidad = ''; tipoCuentaBancaria = '0'; fechadepo = ''; comprobacion = '';
        if (tipo_pago_abono==4) {
          ddl_entidad = document.getElementById("ddl-entidad").value;
          tipoCuentaBancaria = document.getElementById("tipoCuentaBancaria").value;
          fechadepo = document.getElementById("fechadepo").value;
          comprobacion = document.getElementById("comprobacion").value;
        } else if (tipo_pago_abono==2) {
          ddl_entidad = document.getElementById("medio_tarjeta").value;
          comprobacion = document.getElementById("comprobacion_tarjeta").value;
        } else if (tipo_pago_abono==3) {
          ddl_entidad = document.getElementById("ddl-entidad-cheque").value;
          comprobacion = document.getElementById("comprobacion_cheque").value;
        }
        if (r == true) {
          $('#botones_abono').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
          //alert(ddl_entidad + ' ' + tipoCuentaBancaria + ' ' + fechadepo + ' ' + comprobacion);
          $.ajax({
                  url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/agregar_mov_apartado') ?>",
                  type:"post",
                  data: { 'idCa': idCa, 'pago': pago.value , 'referencia': referencia.value, 'tipo_pago_abono': tipo_pago_abono,
                  'ddl_entidad':	ddl_entidad, 'tipoCuentaBancaria': tipoCuentaBancaria, 'fechadepo': fechadepo, 'comprobacion': comprobacion},
                  beforeSend: function() {
                    $( "#notifica_apartados" ).html( '<span style="font-size:10pt;" class="label label-info"> Espere un momento mientras se realiza el proceso... </span>' );
                  },
                  success: function(data){
                    $.get(
                        "<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/mov_apartados') ?>",
                        { 'id': idCa },
                        function (modal_mov_apartado) {
                            $('#modal_mov_apartado').html(modal_mov_apartado);
                            filtro_apartados();//refresca los apartados mostrados al usuario
                        }
                    );
                      $( "#notifica_apartados" ).html( data );
                      setTimeout(function() {//aparece la alerta en un milisegundo
            							$("#notifica_apartados").fadeIn();
            					});
            					setTimeout(function() {//se oculta la alerta luego de 4 segundos
            							$("#notifica_apartados").fadeOut();
            					},5000);
                  },
                  error: function(msg, status,err){
                   //alert('No pasa 150');
                  }
              });

        } else { }

      }
    }
  }

  function enviar_correo(idCa, email, id_factun) {
    if (email=='') {
      $( "#notifica_correo" ).html( '<span style="font-size:10pt;" class="label label-danger"><strong>Error!</strong> No se encuenta la dirección email.</span>' );
      setTimeout(function() {//aparece la alerta en un milisegundo
          $("#notifica_correo").fadeIn();
      });
      setTimeout(function() {//se oculta la alerta luego de 4 segundos
          $("#notifica_correo").fadeOut();
      },5000);
    } else {
      $( "#notifica_correo" ).html( '<span style="font-size:10pt;" class="label label-info"> Espere un momento, enviando correo...</span>' );
      $.get( "<?= Yii::$app->getUrlManager()->createUrl('cabeza-caja/enviar_correo_factura') ?>" ,
      {'id' : idCa, 'email' : email , 'id_factun' : id_factun},
        function( data ) {
          $( "#notifica_correo" ).html( data );

        });
    }
  }
</script>
