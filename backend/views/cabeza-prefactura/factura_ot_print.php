<?php
$style_imprime_fact = \Yii::$app->request->BaseUrl.'/css/style_imprime_fact.css';
use backend\models\Empresa;
use backend\models\DetalleFacturas;
use backend\models\ProductoServicios;
use backend\models\Clientes;
use backend\models\FormasPago;
use backend\models\Factun;
use backend\models\Ot;
use backend\models\MovimientoCredito;
use backend\models\FacturaOt;
use backend\models\Marcas;
use backend\models\Modelo;
use backend\models\DetalleOrdServicio;
use backend\models\Servicios;
use backend\models\Familia;
$mv = new MovimientoCredito();
$empresaimagen = new Empresa();
$empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
$products = DetalleFacturas::find()->where(["=",'idCabeza_factura', $model->idCabeza_Factura])->all();
$detalle_factura = '';
if($products) {
  foreach($products as $position => $product) {
    $modelProd = ProductoServicios::find()->where(['codProdServicio'=>$product['codProdServicio']])->one();
    $descrip = $product['codProdServicio'] != "0" ? $modelProd->ubicacion.' / '.$modelProd->nombreProductoServicio : 'ORDEN DE SERVICIO';
    $servicios = ''; $precio_serv = ''; $cod_serv = '';
    if ($product['codProdServicio'] == "0") {
      $det_ord_ser = DetalleOrdServicio::find()->where(['idOrdenServicio'=>$model->idOrdenServicio])->all();
      foreach($det_ord_ser as $row){
        //Obtengo una instancia de Servicios donde el codigo de servicio es igual al servicio que recorre el bucle
        $modelServicio = Servicios::find()->where(['codProdServicio'=>$row['producto']])->one();
        //Obtenemos una instancia de familia donde el código de familia es igual al servicio obtenido de la instancia anterior
        $fami = Familia::find()->where(['codFamilia'=>$row['familia']])->one();
        $cod_serv .= '<br><small><span style="color: #288f28;">' . $modelServicio->codProdServicio.'</span></small>';
        $servicios .= '<br><small><span style="color: #288f28;">' . $fami->descripcion . ' - ' . $modelServicio->nombreProductoServicio.'</span></small>';
        $precio_serv .= '<br><small><span style="float:right; color: #288f28;">' . number_format($modelServicio->precioMinServicio,2).'</span></small>';
      }
    }
    $fecha_hora = $product['codProdServicio'] == "0" ? '' : date('d-m-Y/h:i:s A', strtotime( $product['fecha_hora'] ));
    $detalle_factura .= '<tr>
      <td style="font-size: 0.8em; text-align:center;">'.$product['cantidad'].'</td>
      <td style="font-size: 0.8em; text-align:left;">'.$product['codProdServicio'].$cod_serv.'</td>
      <td width="25%" style="font-size: 0.7em; text-align:center;">'.$fecha_hora.'</td>
      <td width="50%" style="font-size: 0.8em; text-align:left;">'.$descrip.$servicios.'</td>
      <td style="font-size: 0.8em; text-align:right;">'.number_format($product['precio_unitario'], 2).$precio_serv.'</td>
      <td width="5%" style="font-size: 0.8em; text-align:right;">'.number_format($product['descuento_producto'],2).'</td>
      <td style="font-size: 0.8em; text-align:right;">'.number_format($product['subtotal_iva'],2).'</td>
      <td>'.number_format(($product['precio_unitario']*$product['cantidad']),2).'</td>
    </tr>';
  }
}
$fact_ot = FacturaOt::find()->where(['idCabeza_Factura'=>$model->idCabeza_Factura])->one();
$info_cliente = $info_invoice = $tipo_documento = '';
if (@$cliente = Clientes::find()->where(['idCliente'=>$model->idCliente])->one()) {
    $info_cliente = '
    <h2 class="name">'.$cliente->nombreCompleto.'</h2>
    <div class="address">'.$cliente->direccion.'</div>
    <div class="email">'.$cliente->celular.' / '.$cliente->telefono.'<br>'.$cliente->email.'</div>';
} else{
    $info_cliente = '<h2 class="name">'.$model->idCliente.'</h2>';
    if ($model->idCliente=='Factura sin cliente') {
      $info_cliente = @$fact_ot ? '<h2 class="name">ORDEN DE TRABAJO SIN CLIENTE</h2>' : '<h2 class="name">PREFACTURA SIN CLIENTE</h2';
    }
}
//obtenemos dtos de cliente O.T

$info_cliente_ot = $fecha_vencimiento_ot = $dias_vencidos = '';
if (@$fact_ot) {
  $cliente_ot = Ot::find()->where(['idCliente'=>$fact_ot->idCliente])->one();
  $client = Clientes::find()->where(['idCliente'=>$cliente_ot->idCliente])->one();
  $marcas = Marcas::find()->where(['codMarca'=>$fact_ot->codMarca])->one();
  $modelo = Modelo::find()->where(['codModelo'=>$fact_ot->codModelo])->one();
  $ma = $mo = '';
  if (@$marcas && @$modelo) {
    $ma = $marcas->nombre_marca;
    $mo = $modelo->nombre_modelo;
  }
  $info_cliente_ot = '
  <h2 class="name">'.$client->nombreCompleto.'</h2>
  PLACA: '.$fact_ot->placa.' | MARCA: '.$ma.' | MODELO: '.$mo.'
  <br>VERSIÓN: '.$fact_ot->codVersion.' | AÑO: '.$fact_ot->ano.' | MOTOR: '.$fact_ot->motor.'
  <br>CILINDRADA: '.$fact_ot->cilindrada.' | COMBUSTIBLE: '.$fact_ot->combustible.' | TRANSMISIÓN: '.$fact_ot->transmision.' ';
  $fecha_vencimiento_ot = date ( 'Y-m-d' , strtotime ( '+'.$cliente_ot->dias_ot.' day' , strtotime ( $model->fecha_inicio ) ) );
  $dias_vencidos = strtotime($fecha_vencimiento_ot) < strtotime(date('Y-m-d')) ? $mv->getMovimiento_de_dias( $fecha_vencimiento_ot , date('Y-m-d')) : 0;
} else

//---------------------------------------
$tipo_pago = $feven = '';
if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
    $tipo_pago = $modelP->desc_forma;
    if ($model->estadoFactura!='Crédito') {
      $feven = $model->fecha_final;
    }
    else {
      $feven = $model->fecha_vencimiento;
    }
}
$id_factun = $model->factun_id;



if (@$fact_ot) {
  $info_invoice = '
  <div class="date">Fecha O.T: '.$model->fecha_inicio.'</div>
  <div class="date">Fecha de vencimiento: '.date('d-m-Y', strtotime( $fecha_vencimiento_ot )).'</div>
  <div> Días vencidos: '.$dias_vencidos.'</div>';
} else {
  $info_invoice = '
  <div class="date">Fecha Registro: '.$model->fecha_inicio.'</div>';

}

$footer_pdf = $id_factun == -1 ? 'Autorizado mediante resolución 11-97 del 05/09/1997 de la D.G.T.D' : 'Autorización mediante Resolución No. DGT-R-48-2016 del 07/10/2016 de la D.G.T.D';

 ?>
<link href="<?= $style_imprime_fact ?>" rel="stylesheet">
<main>
  <div id="details" class="clearfix">
    <table width="130%">
      <tr>
        <td style="text-align:left" width="60%">
          <div>
            <?php echo @$fact_ot ? '<div class="to">Responsable O.T:</div>' : ''; ?>
            <?= $info_cliente_ot ?>
          </div>
        </td>
        <td style="text-align:right" width="70%">
          <div id="client">
            <div class="to"><?= @$fact_ot ? 'Orden de trabajo a:' : 'Cliente' ?></div>
            <?= $info_cliente ?>
          </div>
        </td>
      </tr>
    </table>
    <div id="invoice">
      <?php
      $orden_servicio = $model->idOrdenServicio > 0 ? '<br>- [ Cuenta con orden de servicio ID. '.$model->idOrdenServicio.' ]' : '';
      if (@$fact_ot) {
        echo '<h1>Orden de Trabajo ID. '.$model->idCabeza_Factura.$orden_servicio.'</h1>';
      } else {
        echo '<h1>Prefactura ID. '.$model->idCabeza_Factura.$orden_servicio.' </h1>';
      }

      if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
        $servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
        $servicio_restaurante = '<tr>
                         <td colspan="3"></td>
                         <td colspan="3">SERVICIO 10%: </td>
                         <td colspan="2">'.number_format($servicio,2).'</td>
                     </tr>';
         $subtotal = $model->subtotal+$servicio;
         $total_a_pagar = $model->total_a_pagar+$servicio;
      } else {
        $servicio_restaurante = '';
        $subtotal = $model->subtotal;
        $total_a_pagar = $model->total_a_pagar;
      }
      ?>

      <?= $info_invoice ?>
      <!--h2>Condición Venta: <?= $model->estadoFactura ?> | Tipo de Pago:	<?= $tipo_pago ?></h2-->
    </div>

  </div>
  <table border="0" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th style="text-align:center;">CANT</th>
        <th style="text-align:center;">CODIGO</th>
        <th style="text-align:center;">FEC/HOR</th>
        <th style="text-align:left;">UBICACIÓN / DESCRIPCIÓN</th>
        <th style="text-align:right">PREC UNIT</th>
        <th style="text-align:right">DESCTO</th>
        <th style="text-align:right">IV %</th>
        <th style="text-align:right">TOTAL</th>
      </tr>
    </thead>
    <tbody>
      <?= $detalle_factura ?>
    </tbody>
    <tfoot>
      <?= $servicio_restaurante ?>
      <tr>
        <td colspan="3"></td>
        <td colspan="3">SUBTOTAL</td>
        <td colspan="2" style="text-align:right"><?= number_format($subtotal,2) ?></td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td colspan="3">TOTAL DESCUENTO</td>
        <td colspan="2" style="text-align:right"><?= number_format($model->porc_descuento,2) ?></td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td colspan="3">TOTAL IV</td>
        <td colspan="2" style="text-align:right"><?= number_format($model->iva,2) ?></td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td colspan="3"><h2>TOTAL A PAGAR</h2></td>
        <td><h2><?= number_format($total_a_pagar,2) ?></h2></td>
      </tr>
    </tfoot>
  </table>
  <div id="thanks">¡Gracias por su preferencia!</div>
  <div id="notices">
    <div>Observación:</div>
    <div class="notice"><?= $model->observacion ?></div>
  </div>
</main>
