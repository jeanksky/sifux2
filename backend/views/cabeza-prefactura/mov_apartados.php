<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\jui\DatePicker;
use backend\models\MovimientoApartado;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use backend\models\FacturasDia;
use backend\models\Clientes;
//PARA ABONO POR DEPOSITO --------------------------------------------------------------------------------------------

$data = ArrayHelper::map(EntidadesFinancieras::find()->all(), 'idEntidad_financiera', 'descripcion');
$data2 = ArrayHelper::map(CuentasBancarias::find()->all(), 'numero_cuenta', function($element) {
		return $element['numero_cuenta'].' - '.$element['descripcion'];
});
$medio = Select2::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data,
						'options' => ['id'=>'ddl-entidad', 'onchange'=>'obtenMedio(this.value,2)','placeholder' => 'Seleccione entidad financiera...'],
						'pluginOptions' => [
								'initialize'=> true,
								'allowClear' => true
						],
				]).'<br>';
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$cuenta = DepDrop::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data2,
						'type' => DepDrop::TYPE_SELECT2,
						'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
						'options' => ['id'=>'tipoCuentaBancaria', 'onchange'=>'obtenCuenta(this.value,2)','placeholder' => 'Seleccione...'],
						'pluginOptions' => [
								'depends'=>['ddl-entidad'],
								'initialize'=> true, //esto inicializa instantaniamente junto a entidad financiera
								'placeholder' => 'Seleccione cuenta...',
								'url' => Url::to(['cabeza-prefactura/cuentas']),
								'loadingText' => 'Cargando...',
								//'allowClear' => true
						],
				]);
$fechadepo = DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName','value'=>date("d-m-Y"), 'options' => ['class'=>'form-control', 'readonly' => true, 'placeholder'=>'Fecha depósito', 'id'=>'fechadepo', 'onchange'=>'obtenFecha(this.value)']]);
$comprobacion = '<br>'.Html::input('text', '', '', ['size' => '20', 'onkeypress'=>'return isNumberDe(event)', 'id' => 'comprobacion', 'disabled'=>false, 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value,2);']);
//--------------FIN ABONO POR DEPOSITO

//--------------PARA ABONO CON TARJETA
$medio_tarjeta = Html::input('text', '', '', ['size' => '19', 'id' => 'medio_tarjeta', 'class' => 'form-control', 'placeholder' =>' Medio ( MasterCard, VISA, otra )', 'onkeyup' => 'obtenMedio(this.value,3);']);
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$comprobacion_tarjeta = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_tarjeta', 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'obtenCom(this.value,3);']);
//-------------FIN ABONO POR TARJETA

//--------------PARA ABONO POR CHEQUE
$medio_cheque = Select2::widget([
					 // 'model' => $model,
					 // 'attribute' => 'tipoFacturacion',
						'name' => '',
						'data' => $data,
						'options' => ['id'=>'ddl-entidad-cheque', 'onchange'=>'obtenMedio(this.value,4)','placeholder' => 'Seleccione entidad financiera...'],
						'pluginOptions' => [
								'allowClear' => true
						],
				]);
//Y la comprobación de pago dependiendo del medio en que se hizo el cobro
$comprobacion_cheque = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_cheque', 'class' => 'form-control', 'placeholder' =>' # Cheque', 'onkeyup' => 'obtenCom(this.value,4);']);
//--------------FIN ABONO POR CHEQUE
$factura = FacturasDia::findOne($id);
$cliente = Clientes::findOne($factura->idCliente);
if ($factura->estadoFactura == 'Cancelada') {
	$imprimir_tiquete = '<div id="notifica_correo"></div><div class="pull-right">'.
	/*Html::a('<i class="fa fa-paper-plane"></i> Enviar por correo', '#', [
			'class'=>'btn btn-info btn-sm',
			'onclick'=>'javascript:enviar_correo('.$id.', "'.$cliente->email.'", '.$factura->factun_id.')',
			'data-toggle'=>'tooltip',
			'title'=>'Imprime factura por Ticket'
	]).'&nbsp;&nbsp;'.*/
	Html::a('<i class="fa fa-print"></i> Imprimir por Ticket', ['/facturas-dia/ticket', 'id'=>$id], [
			'class'=>'btn btn-primary btn-sm',
			'target'=>'_blank',
			'data-toggle'=>'tooltip',
			'title'=>'Imprime factura por Ticket'
	]).'</div>';
} else {
	$imprimir_tiquete = '';
}
?>
<?= $imprimir_tiquete ?>
<center><h4>Apartado ID: <?= $id ?></h4></center>
<div class="panel" id="botones_abono" >
  <div class="col-xs-5">
    <label>Monto</label>
    <?= \yii\widgets\MaskedInput::widget([
					'id' => 'pago_abono',
					'name' => '',
					'options' => ['class'=>'form-control', 'autocomplete' => "off", 'id' => 'pagoparcial', 'placeholder' =>'Abono Parcial o Total', 'onkeyup'=>'', "onkeypress"=>"return isNumberDe(event)"],
					'clientOptions' => [
						'alias' =>  'decimal',
						'groupSeparator' => ',',
						'autoGroup' => true
					],
			]) ?>
	</div>
  <div class="col-xs-7">
    <label>Referencia</label>
    <?= Html::input('text', '', 'ABONOS DE APARTADOS', ['class' => 'form-control', 'placeholder' =>'Referencia', 'readonly' => false, 'id' => 'referencia']) ?>
    <br>
  </div>
  <div class="btn-group btn-group-justified">
    <a onclick="agregar_mov_apartado(1, <?= $id ?>)" title="Click para completar el abono en efectivo" class="btn btn-primary">Efectivo</a>
    <a href="#deposito" title="Click para crear un abono mediante un depósito bancario" data-toggle="tab" class="btn btn-default">Depósito</a>
    <a href="#tarjeta" title="Click para crear un abono pagando con tarjeta" data-toggle="tab" class="btn btn-default">Tarjeta</a>
    <a href="#cheque" title="Click para crear un abono pagando con cheque" data-toggle="tab" class="btn btn-default">Cheque</a>
    <a href="#lista" title="Click para listar los movimientos de la factura seleccionada" data-toggle="tab" class="btn">Listar <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>
  </div>
  <div class="well table-responsive tab-content">

    <div class="tab-pane fade" id="deposito">
      <p>
         <center><label>Abono por depósito bancario</label><br>(Con movimiento en libro automatizado)<hr style="background-color: black; height: 1px; border: 0;"></center>
         <div class='col-xs-12'><?= $medio ?></div>
         <div class='col-xs-7'><?= $cuenta ?></div>
         <div class='col-xs-5'><?= $fechadepo ?></div>
         <div class='col-xs-12'><?= $comprobacion ?></div>
         <div class='col-xs-12'>
          <div id='nota_cancelacion'></div><br>
          <div class="pull-right">
           <?= Html::a('Aplicar abono con Depósito', '#', ['id'=>'boton-abonar-deposito','class' => 'btn btn-primary', 'onclick'=>'agregar_mov_apartado(4, '.$id.')']) ?>
          </div>
        </div>

      </p>

    </div>
    <div class="tab-pane fade" id="tarjeta">
      <p>
        <center><label>Abono por tarjeta</label><hr style="background-color: black; height: 1px; border: 0;"></center>

        <div class='col-xs-12'><?= $medio_tarjeta ?></div>
        <div class='col-xs-12'><?= $comprobacion_tarjeta ?></div>
        <div class='col-xs-12'>
         <div id='nota_cancelacion_2'></div><br>
         <div class="pull-right">
          <?= Html::a('Aplicar abono con Tarjeta', '#', ['id'=>'boton-abonar-tarjeta','class' => 'btn btn-primary', 'onclick'=>'agregar_mov_apartado(2, '.$id.')']) ?>
         </div>
       </div>
       <center><img src="../images/tarjetas.png" height="70" width=""><br>
         <!--i class="fa fa-cc-visa fa-3x" aria-hidden="true"></i>
         <i class="fa fa-cc-mastercard fa-3x" aria-hidden="true"></i>
         <i class="fa fa-cc-amex fa-3x" aria-hidden="true"></i>
         <i class="fa fa-cc-discover fa-3x" aria-hidden="true"></i-->
       </center>
      </p>
    </div>
    <div class="tab-pane fade" id="cheque">
      <p>
        <center><label>Abono por cheque</label><hr style="background-color: black; height: 1px; border: 0;"></center>
        <div class='col-xs-12'><?= $medio_cheque ?></div>
        <div class='col-xs-12'><?= $comprobacion_cheque ?></div>
        <div class='col-xs-12'>
         <div id='nota_cancelacion_3'></div><br>
         <div class="pull-right">
          <?= Html::a('Aplicar abono con Cheque', '#', ['id'=>'boton-abonar-cheque','class' => 'btn btn-primary', 'onclick'=>'agregar_mov_apartado(3, '.$id.')']) ?>
         </div>
       </div>
      </p>
    </div>
    <div class="tab-pane fade in active" id="lista">
      <p>
        <center><label>Lista de abonos de apartados</label></center>
        <div style="height: 200px;width: 100%; overflow-y: auto; ">
          <div id="movi_credit">
            <?php
              $movimientofactura = MovimientoApartado::find()->where(['idCabeza_Factura'=>$id])->orderBy(['idMov' => SORT_DESC])->all();
              $lista_movi = '<table class="items table table-striped">';
              if($movimientofactura) {
							$lista_movi .= '<thead>
												        <th style="text-align:left">ID</th>
												        <th style="text-align:center">FECHA</th>
												        <th style="text-align:right">ANTERIOR</th>
												        <th style="text-align:right">ABONO</th>
												        <th style="text-align:right">SALDO</th>
												        <th style="text-align:right"></th>
											        </thead>';
              $lista_movi .= '<tbody>';
                foreach($movimientofactura as $position => $movi) {
                  $consultar = Html::a('', ['', 'id' => $position], [
                                          'class'=>'glyphicon glyphicon-search',
                                          'id' => 'activity-index-link-agregacredpen',
                                          'data-toggle' => 'modal',
                                          'onclick'=>'javascript:consulta_modal_movi("'.$movi['idMov'].'")',
                                          'data-target' => '#modalconsultamovi',
                                          //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                          'data-pjax' => '0',
                                          'title' => Yii::t('app', 'Mostrar movimiento de crédito') ]);
                  $imprimir = Html::a('<span class=""></span>', ['cabeza-prefactura/imprimir_abono_apartado', 'id'=>$movi['idMov']], [
                                          'class'=>'fa fa-print',
                                          'id' => 'activity-index-link-report',
                                          'target'=>'_blank',
                                          'data-toggle' => 'titulo',
                                          'onclick' => 'jsWebClientPrint.print()',
                                          'data-pjax' => '0',
                                          'title' => Yii::t('app', 'Imprimir movimiento'),
                                          ]);
                  $accion_mov = $consultar . ' ' . $imprimir;

                $lista_movi .= '<tr>
                                  <td><font face="arial" size=2>'.$movi['idMov'].'</font></td>
                                  <td><font face="arial" size=2>'.date('d-m-Y | h:m:s A', strtotime( $movi['fecmov'] )).'</font></td>
                                  <td align="right"><font face="arial" size=2>'.number_format($movi['monto_anterior'],2).'</font></td>
                                  <td align="right"><font face="arial" size=2>'.number_format($movi['monto_movimiento'],2).'</font></td>
																	<td align="right"><font face="arial" size=2>'.number_format($movi['saldo_pendiente'],2).'</font></td>
                                  <td class="actions button-column">'.$imprimir.'</td></tr>';
                }
              $lista_movi .= '</tbody>';
              }
              $lista_movi .= '</table>';
              echo $lista_movi;

            ?>
          </div>
        </div>
      </p>
    </div>
  </div><br>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#boton-abonar-deposito').addClass('disabled');//desabilito el boton de abonar, desde el index para que me afecte a todo hasta que el form esté lleno
  $('#boton-abonar-tarjeta').addClass('disabled');// ''
  $('#boton-abonar-cheque').addClass('disabled');// ''
});
</script>
