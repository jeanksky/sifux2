
<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\models\Funcionario;
use backend\models\FormasPago;
use backend\models\Compra_view;
use backend\models\Factun;
use backend\models\Empresa;
$empresa = Empresa::findOne(1);
$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
/* @var $this yii\web\View */
/* @var $model backend\models\EncabezadoPrefactura */

$this->title = $model->idCabeza_Factura;
$this->params['breadcrumbs'][] = ['label' => 'Prefacturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$estadoFactura = '';
if ($model->factun_id > 0) {

if ($model->estado_hacienda=='01') { $estadoFactura = '<span class="label label-success" style="font-size:15pt;">Factura: '.$model->fe.' Aceptada por Hacienda.</span>';
} else if ($model->estado_hacienda=='07') { $estadoFactura = '<span class="label label-primary" style="font-size:15pt;">Recibido por Hacienda</span><br><br><p><strong>Detalle: </strong>'.$model->estado_detalle.'</p>';
} else if ($model->estado_hacienda=='03') { $estadoFactura = '<span class="label label-danger" style="font-size:15pt;">Factura: '.$model->fe.' Rechazada por Hacienda.</span><br><br><p><strong>Detalle del error: </strong>'.$model->estado_detalle.'</p>';
} else if ($model->estado_hacienda=='04') { $estadoFactura = '<span class="label label-info" style="font-size:15pt;">Enviado y en espera</span><br><br><p><strong>Detalle: </strong>'.$model->estado_detalle.'</p>';
} else {
  $estadoFactura = $model->estado_hacienda.', ESPERE A QUE SE RE-ENVIE<br><br><p><strong>Detalle: </strong>'.$model->estado_detalle.'</p>';
}

}
?>

<?php $products = Compra_view::getContenidoFactura(); ?>

<div class="encabezado-prefactura-view">
    <div class="panel panel-default">
        <div class="panel-heading">
          <div id="relt_prueba"><center><?= $estadoFactura ?></center></div>
            <center><h2>ID Factura. <?= Html::encode($this->title).'</h2>' ?></center>
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                    <?php
                        if(@$modelF = Funcionario::find()->where(['idFuncionario'=>$model->codigoVendedor])->one()) //http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html
                            {
                                echo "<div class='col-lg-4'>";
                                echo "<h4><strong>Vendedor</strong>: ".$modelF->nombre." ".$modelF->apellido1." ".$modelF->apellido2." ";
                                echo "</h4>";
                                echo "</div>";
                            }
                    ?>
                    <div class="col-lg-4">
                    <?php
                        echo '<h4><strong>Estado de la factura</strong>: '.$model->estadoFactura.'</h4>';
                    ?>
                    </div>
                    <div class="col-lg-4">
                    <?php
                        echo '<h4><strong>Fecha ingreso</strong>: '.$model->fecha_inicio;
                        echo "</h4>";
                    ?>
                    </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-4">
                <?php
                    if(@$modelP = FormasPago::find()->where(['id_forma'=>$model->tipoFacturacion])->one()){
                        if ($model->estadoFactura!='Crédito') {
                        echo "<h4><strong>Tipo de pago</strong>: ".$modelP->desc_forma." ";
                        echo "</h4>";
                        }
                    }
                ?>
                </div>
                <div class="col-lg-2">

                </div>
                <div class="col-lg-2">

                </div>
                <div class="col-lg-4">
                    <?php
                        $fecha_final = '';
                            if ($model->fecha_final=='') {
                                $fecha_final = 'Pendiente';
                            } else {
                                $fecha_final = $model->fecha_final;
                            }
                        echo '<h4><strong>Fecha Final</strong>: '.$fecha_final;
                        echo "</h4>";
                    ?>
                </div>
            </div>
            <div class="col-lg-12">
                <?php
                        echo '<table class="items table table-striped">';
                        echo '<thead>';
                        printf('<tr><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th></tr>',
                                'CANTIDAD',
                                'CODIGO',
                                'DESCRIPCIÓN',
                                'PRECIO UNITARIO',
                                'DESCUENTO',
                                'IV %',
                                'TOTAL'
                                );
                        echo '</thead>';

                        if($products) {
                        echo '<tbody>';
                            foreach($products as $position => $product) {

                                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class = "textright descuent_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="textright total_'.$position.'" align="right">%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>',
                                        $product['cantidad'],
                                        $product['cod'],
                                        $product['desc'],
                                        number_format($product['precio'], 2),
                                        number_format($product['cantidad']*(($product['dcto'])),2),//Descuento
                                        $product['iva'],
                                        number_format(($product['precio']*$product['cantidad']),2)

                                        );
                            }
                        echo '</tbody>';
                    }
                    $servicio = ($model->subtotal - $model->porc_descuento)*(Yii::$app->params['porciento_servicio']/100);
              			if (Yii::$app->params['servicioRestaurante']==true && $model->ignorar_porc_ser != 'checked') {
                      $servicio_restaurante = '<tr>
                                       <th align="left">SERVICIO 10%: </th>
                                       <td align="right"><span>'.number_format($servicio,2).'</span></td>
                                   </tr>';
                       $subtotal = number_format($model->subtotal+$servicio,2);
                       $total_a_pagar = number_format($model->total_a_pagar+$servicio,2);
              			} else {
                      $servicio_restaurante = '';
                      $subtotal = number_format($model->subtotal,2);
                      $total_a_pagar = number_format($model->total_a_pagar,2);
              			}
                        echo '</table><div class="well pull-right extended-summary resumen">';
                        echo '<h3 align="right">RESUMEN</h3>
                           <table class="" align="right">
                            <tbody>
                                <tr>
                                     <td class="detail-view">
                                        <table>
                                            '.$servicio_restaurante.'
                                            <tr>
                                                <th align="left">SUBTOTAL: </th>
                                                <td align="right">'.$subtotal.'</td>
                                            </tr>
                                            <tr>
                                                <th align="left">TOTAL DESCUENTO: &nbsp;</th>
                                                <td align="right">'.number_format($model->porc_descuento,2).'</td>
                                            </tr>
                                            <tr>
                                                <th align="left">TOTAL IV: </th>
                                                <td align="right"><span>'.number_format($model->iva,2).'</span></td>
                                            </tr>
                                            <tr>
                                               <th align="left">TOTAL A PAGAR: </th>
                                               <td align="right"><span><strong>'.$total_a_pagar.'</strong></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            </table>';
                        echo '</div>';
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo $jquery_min_js; ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
  var clave = '<?= $model->clave_fe ?>';
  if (clave > 0) {
    //estado_hacienda(clave);
  }
});


</script>
