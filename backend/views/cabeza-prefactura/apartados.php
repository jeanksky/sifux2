<?php
use backend\models\FacturasDia;
use backend\models\Clientes;
use backend\models\ClientesApartados;
use backend\models\MovimientoApartado;
use yii\helpers\Url;

$apartados = FacturasDia::find()
->leftJoin(['tbl_clientes'], 'tbl_clientes.idCliente = tbl_encabezado_factura.idCliente')
->where(['estadoFactura'=>'Apartado'])
->andWhere('idCabeza_Factura LIKE :idCabeza_Factura AND fecha_inicio LIKE :fecha_inicio AND nombreCompleto LIKE :cliente',
[':idCabeza_Factura'=>$idCabeza_Factura, ':cliente'=>$cliente, ':fecha_inicio'=>$fecha_inicio])
->orderBy(['idCabeza_Factura' => SORT_DESC])
->limit(25)->all();
foreach ($apartados as $key => $apartado) {
  $cliente = Clientes::find()->where(['idCliente'=>$apartado->idCliente])->one();
  $cliente_apartado = ClientesApartados::find()->where(['idCliente'=>$apartado->idCliente])->one();
  $movi_apartado = MovimientoApartado::find()->where(['idCabeza_Factura'=>$apartado->idCabeza_Factura])->all();
  $mov_apartados = 0;
  foreach ($movi_apartado as $key => $m_apartado) { $mov_apartados += $m_apartado->monto_movimiento; }
  $saldo = $apartado->total_a_pagar - $mov_apartados;
  $fecha_vencimiento = strtotime ( '+'.$cliente_apartado->dias_apartado.' day' , strtotime ( $apartado->fecha_inicio ) ) ;
  if ($fecha_vencimiento < strtotime(date('Y-m-d'))) {
    $label_fech_ven = 'label-danger';
  }else {
    $label_fech_ven = 'label-success';
  }
  $fecha_vencimiento = date ( 'd-m-Y' , $fecha_vencimiento );
  echo '<div class="elem">
          <div class="panel panel-info" >
            <div class="panel-body" tabindex="0" style="cursor:pointer" onkeypress="$(document).keypress(function(event){ if(event.which == 13) abrir_apartado('.$apartado->idCabeza_Factura.') })" onclick="javascript:abrir_apartado('.$apartado->idCabeza_Factura.'); this.onclick = null;">
              <center><strong>Apartado ID: '.$apartado->idCabeza_Factura.'</strong></center><br>
              <p>Cliente: <span style="float:right">'.substr($cliente->nombreCompleto, 0, 18).'</span></p>
              <p>Monto: <span style="float:right">'.number_format($apartado->total_a_pagar,2).'</span></p>
              <p>Abono: <span style="float:right">'.number_format($mov_apartados,2).'</span></p>
              <p>Saldo: <span style="float:right">'.number_format($saldo,2).'</span></p>
            </div>
            <div class="panel-footer">
              <a href="#" data-toggle="modal" data-url="'.Url::to(['cabeza-prefactura/mov_apartados', 'id'=>$apartado->idCabeza_Factura]).'" title="Agregar un abono a este apartado" class="fa fa-credit-card fa-2x" id="mostrar_mov_apartados"></a>
              <span style="float:right; font-size:10pt;" class="label '.$label_fech_ven.'">Fecha vencimiento: '.$fecha_vencimiento.'</span>
            </div>
          </div>
        </div>';
}
 ?>
