<?php
use backend\models\Compra;//Prueba
use yii\helpers\Html;
?>

<?php $products = Compra::getContenidoCompra(); ?>
            <?php
                echo '<div class="grid-view">';
                echo '<table class="items table table-striped">';
                echo '<thead>';
                printf('<tr><th>%s</th><th>%s</th><th>%s</th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th><center>%s</center></th><th class="actions button-column">&nbsp;</th></tr>',
                        'CANTIDAD',
                        'CÓDIGO',
                        'DESCRIPCIÓN',
                        'PRECIO UNITARIO',
                        'DESCUENTO',
                        'IV %',
                        'TOTAL'
                        );
                echo '</thead>';
            if($products) {
            ?>
            <?php
            echo '<tbody>';
                foreach($products as $position => $product) {
                    printf('<tr><td>%s</td><td>%s</td><td>%s</td><td align="right">%s</td><td class = "textright descuent_'.$position.'" align="right">%s</td><td align="right">%s</td><td class="textright total_'.$position.'" align="right">%s</td><td class="actions button-column">%s</td></tr>',
                            Html::textInput('cantidad_'.$position,
                                $product['cantidad'], array(
                                    'class' => 'cantidad_'.$position,
                                    )
                                ),
                            $product['cod'],
                            $product['desc'],
                            number_format($product['precio'],2),
                            number_format($product['cantidad']*(($product['precio']*$product['dcto'])/100),2),//Descuento
                            $product['iva'],
                            number_format(($product['precio']*$product['cantidad']),2),

                               Html::a('', ['/cabeza-prefactura/deleteitem', 'id' => $position], ['class'=>'glyphicon glyphicon-trash','data' => [
                                    'confirm' => '¿Está seguro de quitar este producto de la prefactura?',],])
                            );

                        $this->registerJs("
                                    $('.cantidad_".$position."').keyup(function() {
                                        this.value = (this.value + '').replace(/[^0-9]/g, '');
                                        $.ajax({
                                            url:'".Yii::$app->getUrlManager()->createUrl('/cabeza-prefactura/updatedescuento')."',
                                            data: $('.cantidad_".$position."'),
                                            success: function(result) {
                                            $('.descuent_".$position."').html(result);
                                            $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                            '/cabeza-prefactura/getpreciototal')."');
                                            },
                                            error: function() {
                                            },
                                        });
                                        $.ajax({
                                            url:'".Yii::$app->getUrlManager()->createUrl('/cabeza-prefactura/updatecantidad')."',
                                            data: $('.cantidad_".$position."'),
                                            success: function(result) {
                                            $('.total_".$position."').html(result);
                                            $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                            '/cabeza-prefactura/getpreciototal')."');
                                            },
                                            error: function() {
                                            },
                                        });
                                    });

                                ");
                        $this->registerJs("
                                    $('.precio_".$position."').keyup(function() {
                                        $.ajax({
                                            url:'".Yii::$app->getUrlManager()->createUrl('/cabeza-prefactura/updateprecio')."',
                                            data: $('.precio_".$position."'),
                                            success: function(result) {
                                            $('.total_".$position."').html(result);
                                            $('.resumen').load('".Yii::$app->getUrlManager()->createUrl(
                                            '/cabeza-prefactura/getpreciototal')."');
                                            },
                                            error: function() {
                                            },

                                            });
                                    });
                                ");
                }
            echo '</div>';
            echo '</tbody>';
            ?>
            <?php
            }
                echo '</table><div class="well pull-right extended-summary resumen">';
                echo Compra::getTotal();
                echo '</div>';
            ?>
