<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\jui\DatePicker;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use backend\models\FacturasDia;
use backend\models\Clientes;
use backend\models\Compra_view;
use backend\models\Compra;
use backend\models\FormasPago;
$url_obtener_pago = Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/obtener_pago');
$url_agregar_monto_pago = Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/agregar_monto_pago');
$url_condicion_venta = Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/condicion_venta');
$url_compruebatipopago = Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/compruebatipopago');
$url_delete_medio_pago = Yii::$app->getUrlManager()->createUrl('cabeza-caja/delete_medio_pago');
$url_completando_facturacion = Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/completando_facturacion');
$url_factura_electronica = Yii::$app->getUrlManager()->createUrl('cabeza-caja/factura_electronica');
$url_create = Url::toRoute('cabeza-prefactura/create');
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';

$resumen_total = Compra::getTotal(true);
$servicio_ignorado = Compra::getServicio_porc_ign();
if (Yii::$app->params['servicioRestaurante']==true && $servicio_ignorado != 'checked') {
  $servicio = ($resumen_total['subtotal'] - $resumen_total['dcto'])*(Yii::$app->params['porciento_servicio']/100);
  $total_a_pagar = $resumen_total['total'] + $servicio;
} else {
  $total_a_pagar = $resumen_total['total'];
}

echo '<div class="col-lg-12"><div id="alerta_cancelar"></div></div>';
  if ($tipoFacturacion == '5') {//CREDITO
    echo '<center><div class="col-lg-12"><br><br>
            <div class="btn-group" role="group" aria-label="...">'.
              Html::a('<span class="fa fa-send-o"></span> <span style="font-size: 1.4em;">EMITIR FACTURA CRÉDITO</span>', '#', [ 'id'=>'emitir_credito_btn', 'class'=>"btn btn-primary", 'data-loading-text'=>"<span style='font-size: 1.4em;'>Acreditando espere...</span>", 'onclick'=>'javascript:finalizar_credito(this,"'.$url_completando_facturacion.'","'.$url_factura_electronica.'","'.$url_create.'")']).
          '</div><br>
        </div></center>';
  } else {//CONTADO

          //PARA ABONO POR DEPOSITO --------------------------------------------------------------------------------------------

          $data = ArrayHelper::map(EntidadesFinancieras::find()->all(), 'idEntidad_financiera', 'descripcion');
          $data2 = ArrayHelper::map(CuentasBancarias::find()->all(), 'numero_cuenta', function($element) {
          		return $element['numero_cuenta'].' - '.$element['descripcion'];
          });
          $medio = Select2::widget([
          					 // 'model' => $model,
          					 // 'attribute' => 'tipoFacturacion',
          						'name' => '',
          						'data' => $data,
          						'options' => ['id'=>'ddl-entidad', 'onchange'=>'compruebatipopago(4, "'.$url_compruebatipopago.'")','placeholder' => 'Seleccione entidad financiera...'],
          						'pluginOptions' => [
          								'initialize'=> true,
          								'allowClear' => true
          						],
          				]).'<br>';
          //Y la comprobación de pago dependiendo del medio en que se hizo el cobro
          $cuenta = DepDrop::widget([
          					 // 'model' => $model,
          					 // 'attribute' => 'tipoFacturacion',
          						'name' => '',
          						'data' => $data2,
          						'type' => DepDrop::TYPE_SELECT2,
          						'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
          						'options' => ['id'=>'tipoCuentaBancaria', 'onchange'=>'compruebatipopago(4,"'.$url_compruebatipopago.'")','placeholder' => 'Seleccione...'],
          						'pluginOptions' => [
          								'depends'=>['ddl-entidad'],
          								'initialize'=> true, //esto inicializa instantaniamente junto a entidad financiera
          								'placeholder' => 'Seleccione cuenta...',
          								'url' => Url::to(['cabeza-prefactura/cuentas']),
          								'loadingText' => 'Cargando...',
          								//'allowClear' => true
          						],
          				]);
          $fechadepo = DatePicker::widget(['clientOptions' => ['dateFormat' => 'd-m-yy'], 'name' => 'attributeName','value'=>date("d-m-Y"),
          'options' => ['class'=>'form-control', 'readonly' => true, 'placeholder'=>'Fecha depósito', 'id'=>'fechadepo', 'onchange'=>'']]);
          $comprobacion = '<br>'.Html::input('text', '', '', ['size' => '20', 'onkeypress'=>'return isNumberDe(event)', 'id' => 'comprobacion', 'disabled'=>false, 'class' => 'form-control', 'placeholder' =>' Comprobación',
          'onkeyup' => 'compruebatipopago(4,"'.$url_compruebatipopago.'");']);
          //--------------FIN ABONO POR DEPOSITO

          //--------------PARA ABONO CON TARJETA
          $medio_tarjeta = Html::input('text', '', '', ['size' => '19', 'id' => 'medio_tarjeta', 'class' => 'form-control', 'placeholder' =>' Medio ( MasterCard, VISA, otra )', 'onkeyup' => 'compruebatipopago(2,"'.$url_compruebatipopago.'");']);
          //Y la comprobación de pago dependiendo del medio en que se hizo el cobro
          $comprobacion_tarjeta = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_tarjeta', 'class' => 'form-control', 'placeholder' =>' Comprobación', 'onkeyup' => 'compruebatipopago(2,"'.$url_compruebatipopago.'");']);
          //-------------FIN ABONO POR TARJETA

          //--------------PARA ABONO POR CHEQUE
          $medio_cheque = Select2::widget([
          					 // 'model' => $model,
          					 // 'attribute' => 'tipoFacturacion',
          						'name' => '',
          						'data' => $data,
          						'options' => ['id'=>'ddl-entidad-cheque', 'onchange'=>'compruebatipopago(3,"'.$url_compruebatipopago.'")','placeholder' => 'Seleccione entidad financiera...'],
          						'pluginOptions' => [
          								'allowClear' => true
          						],
          				]);
          //Y la comprobación de pago dependiendo del medio en que se hizo el cobro
          $comprobacion_cheque = '<br>'.Html::input('text', '', '', ['size' => '20', 'id' => 'comprobacion_cheque', 'class' => 'form-control', 'placeholder' =>' # Cheque', 'onkeyup' => 'compruebatipopago(3,"'.$url_compruebatipopago.'");']);
          //--------------FIN ABONO POR CHEQUE
?>
<div class="panel" id="botones_abono" >
  <div class="col-xs-5" id='autofocus'>
    <label>Monto pago</label>
    <td align='right' width='150'>( Dolar: <input type='checkbox' class='checkdolar' name='checkdolar' /> )
    <?= Html::input('text', 'cantidad de pago', '',
        [
          'size' => '10',
          'id' => 'pago',
          'onkeypress'=>'return isNumberDe(event)',
          'style'=>'font-family: Fantasy; font-size: 30pt; text-align: center;',
          'class' => 'form-control input-lg',
          'autocomplete' => "off",
          'onkeyup' => 'obtenerPago(event.keyCode,this.value,"'.$url_obtener_pago.'", "'.$url_agregar_monto_pago.'","'.$url_condicion_venta.'","'.$load_buton.'");'
        ]) ?>
	</div>
  <div class="col-xs-7" >
    <input type='hidden' id='vueltoinput'><input type='hidden' id='pago_real'>
    <div id='valorencolones' style="float: right;"></div>
    <h1 style="text-align: right;"><div id='vuelto_final'></div></h1>
    <h3 style="text-align: right;"><div id='vuelto'></div></h3>
  </div>
  <div class="col-xs-12"><br>
    <div class="btn-group btn-group-justified">
      <a onclick="agregar_monto_pago(1, '<?= $url_agregar_monto_pago ?>','<?= $url_condicion_venta ?>','<?= $load_buton ?>')" title="Click para completar el abono en efectivo" class="btn btn-primary">Efectivo</a>
      <a href="#deposito" title="Click para crear un abono mediante un depósito bancario" data-toggle="tab" class="btn btn-default">Depósito</a>
      <a href="#tarjeta" title="Click para crear un abono pagando con tarjeta" data-toggle="tab" class="btn btn-default">Tarjeta</a>
      <a href="#cheque" title="Click para crear un abono pagando con cheque" data-toggle="tab" class="btn btn-default">Cheque</a>
      <a href="#lista" title="Click para listar los movimientos de la factura seleccionada" data-toggle="tab" class="btn">Listar <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>
    </div>
    <div class="well table-responsive tab-content">

      <div class="tab-pane fade" id="deposito">
        <p>
           <center><label>Pago por depósito bancario</label><br>(Con movimiento en libro automatizado)<hr style="background-color: black; height: 1px; border: 0;"></center>
           <div class='col-xs-12'><?= $medio ?></div>
           <div class='col-xs-7'><?= $cuenta ?></div>
           <div class='col-xs-5'><?= $fechadepo ?></div>
           <div class='col-xs-12'><?= $comprobacion ?></div>
           <div class='col-xs-12'>
            <div id='nota_cancelacion'></div><br>
            <div class="pull-right">
             <?= Html::a('Agregar pago con Depósito', '#', ['id'=>'boton-agregar-deposito','class' => 'btn btn-primary', 'onclick'=>'agregar_monto_pago(4, "'.$url_agregar_monto_pago.'","'.$url_condicion_venta.'","'.$load_buton.'")']) ?>
            </div>
          </div>

        </p>

      </div>
      <div class="tab-pane fade" id="tarjeta">
        <p>
          <center><label>Pago por tarjeta</label><hr style="background-color: black; height: 1px; border: 0;"></center>
          <div class='col-xs-12'><?= $medio_tarjeta ?></div>
          <div class='col-xs-12'><?= $comprobacion_tarjeta ?></div>
          <div class='col-xs-12'>
           <div id='nota_cancelacion_2'></div><br>
           <div class="pull-right">
            <?= Html::a('Agregar pago con Tarjeta', '#', ['id'=>'boton-agregar-tarjeta','class' => 'btn btn-primary', 'onclick'=>'agregar_monto_pago(2, "'.$url_agregar_monto_pago.'","'.$url_condicion_venta.'","'.$load_buton.'")']) ?>
           </div>
         </div>
         <center><img src="../images/tarjetas.png" height="70" width=""><br>
         </center>
        </p>
      </div>
      <div class="tab-pane fade" id="cheque">
        <p>
          <center><label>Pago por cheque</label><hr style="background-color: black; height: 1px; border: 0;"></center>
          <div class='col-xs-12'><?= $medio_cheque ?></div>
          <div class='col-xs-12'><?= $comprobacion_cheque ?></div>
          <div class='col-xs-12'>
           <div id='nota_cancelacion_3'></div><br>
           <div class="pull-right">
            <?= Html::a('Agregar pago con Cheque', '#', ['id'=>'boton-agregar-cheque','class' => 'btn btn-primary', 'onclick'=>'agregar_monto_pago(3, "'.$url_agregar_monto_pago.'","'.$url_condicion_venta.'","'.$load_buton.'")']) ?>
           </div>
         </div>
        </p>
      </div>
      <div class="tab-pane fade in active" id="lista">
        <p style="text-align: center;">
          <label>Lista de los medios a pagar (Máximo 4 tipos)</label>

            <div id="movi_credit" class="table-responsive-sm">
              <table class="" id="tabla_prefacturas_caja" WIDTH="100%">
                <thead>
                  <th style="text-align:left">MEDIO PAGO</th>
                  <th style="text-align:right">MONTO</th>
                  <th style="text-align:right"></th>
                </thead>
                <tbody>
                  <?php
                  $medio_pago = Compra_view::getContenidoMedioPago();
                  $suma_monto_pago = 0.00;
                  if ($medio_pago) {
                    foreach ($medio_pago as $key => $value) {
                      $formas_pago = FormasPago::findOne($value['formpa_cj']);
                      $suma_monto_pago += $value['monto_cj'];
                      echo '
                          <tr>
                            <td style="text-align:left; color:#07A380; font-size: 1.5em;">'.$formas_pago->desc_forma.'</td>
                            <td style="text-align:right; color:#07A380; font-size: 1.5em;">'.number_format($value['monto_cj'],2).'</td>
                            <td style="text-align:right;"> &nbsp;&nbsp;&nbsp;'.Html::a('', null, ['class'=>'glyphicon glyphicon-remove',
														'onclick'=>'delete_medio_pago('.$key.', "'.$url_delete_medio_pago.'", "'.$url_condicion_venta.'", "'.$load_buton.'")',
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'right',
                            'title'=>'Descartar este medio de pago', 'style'=>'cursor:pointer;']).'</td>
                          </tr>
                      ';
                    }
                  } else {
                    echo '<tr>
                            <td style="text-align:center; color:#E49E53;" colspan="3"><br>Todavía no se ha cargado ningun medio de pago</td>
                          </tr>';
                  }
                  $monto_faltante_cobro = round($total_a_pagar - $suma_monto_pago, 2);
                  ?>
                </tbody>
                <tfoot>
                  <th colspan="3" style="text-align:right"><?= '<h3><small>Faltante a cobrar:</small> '.number_format($monto_faltante_cobro,2).'</h3>' ?></th>
                </tfoot>
              </table>
            </div>
          <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <?= $monto_faltante_cobro == 0.00 ? Html::a('Cancelar factura', /*['/cabeza-caja/index', 'id'=>$model->idCabeza_Factura]*/'#',
              ['id'=>'boton-cancelar',
              'class' => 'btn btn-primary',
              'data-loading-text'=>"Espere, Cancelación en proceso...",
              /*'target'=>'_blank', 'disabled' => true,*/
              'onclick'=>'javascript:cancelar_factura(this,"'.$url_completando_facturacion.'","'.$url_factura_electronica.'","'.$url_create.'")']) : ''
              ?>
          </div>
        </p>
      </div>
    </div>
  </div>
</div>
<?php
  }
?>
<script type="text/javascript">
$(document).ready(function(){
  $('#boton-agregar-deposito').addClass('disabled');//desabilito el boton de abonar, desde el index para que me afecte a todo hasta que el form esté lleno
  $('#boton-agregar-tarjeta').addClass('disabled');// ''
  $('#boton-agregar-cheque').addClass('disabled');// ''
  $("#autofocus").slideDown(function(){//muestro enfocado el area de monto pago
      $("#pago").focus();
  });
  //funcion para obtener el buelto de pago con solo usar el checkbox de dolar
  $('.checkdolar').on('click', function() {
      var valor = document.getElementById("pago").value;
      var dollar = 'no';
      if (this.checked == true)
          dollar = 'si';
      else
          dollar = 'no';
      $.ajax({
          url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/obtener_pago') ?>",
          type:"post",
          dataType: 'json',
          data: {'valor': valor, 'dollar' : dollar },
          success: function(data){
              document.getElementById("vuelto").innerHTML = data.resultado_simbolo;
              document.getElementById("valorencolones").innerHTML = data.valorencolones;
              $('#vueltoinput').val(data.resultado);
              $("#autofocus").slideDown(function(){
                  $("#pago").focus();
              });
          }
      }) ;
  });
});
</script>
