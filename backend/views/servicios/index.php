<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Familia;
use kartik\widgets\AlertBlock; //para mostrar la alerta
//----------------------------------------------Para la modal de elimacion por permiso admin
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ServiciosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Servicios';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<div class="servicios-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear servicio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
     <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
<!--?php  $dataProvider->pagination->pageSize = 10; ?-->
<div class="table-responsive">
<div id="semitransparente">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codProdServicio',
            //'tipo',
            //'numFacturaProv',
            'nombreProductoServicio',
            //'cantidadInventario',
            // 'cantidadMinima',
            //'codFamilia',
            //'tbl_familia.estado',
            [
            'attribute' => 'codFamilia',
            'value' => 'codFamilia0.descripcion',

              'filter'=>Html::activeDropDownList($searchModel, 'codFamilia', ArrayHelper::map(Familia::find()
                                                                            ->where(['=','tipo','Servicio'])
                                                                           // ->andwhere(['=','estado','Activo'])
                                                                            ->orderBy('descripcion ASC')
                                                                            ->asArray()
                                                                            ->all(),'codFamilia','descripcion'),['class' => 'form-control','prompt' => 'Todos'])
            ],
            // 'codFamilia.descripcion',
            // 'precioCompra',
            // 'porcentUnidad',
            // 'precioVenta',
            // 'exlmpuesto',
            // 'precioVentaImpuesto',
            // 'anularDescuento',
              //'precioMinServicio',
             [
            'attribute' => 'precioMinServicio',
            'format' => ['decimal',2],
             ],

            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view} {update} {delete}',
              'buttons' => [
                              'delete' => function ($url, $model, $key) {

                                  return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                      'id' => 'activity-index-link-delete',
                                      'class' => 'activity-delete-link',
                                      'title' => Yii::t('app', 'Eliminar Servicio '.$model->nombreProductoServicio),
                                      'data-toggle' => 'modal',
                                      'data-target' => '#modalDelete',
                                      'onclick' => 'javascript:enviaResultado("'.$model->codProdServicio.'")',
                                      'href' => Url::to('#'),
                                      'data-pjax' => '0',
                                      ]);
                              },
                           ]
            ],
        ],
    ]); ?>
</div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un servicio necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "codProdServicio" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Servicio', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este servicio?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
</div>
<!--script type="text/javascript">
function mostrarVentana()
{
    var ventana = document.getElementById('miVentana'); // Accedemos al contenedor
    ventana.style.marginTop = "100px";
    ventana.style.marginLeft = ((document.body.clientWidth-350) / 2) +  "px";
    ventana.style.display = 'block';
}

function ocultarVentana()
{
    var ventana = document.getElementById('miVentana'); // Accedemos al contenedor
    ventana.style.display = 'none';
}
</script-->
<!--body onload="mostrarVentana()">
<div id="miVentana" style="position: fixed; width: 350px; height: 190px; top: 0; left: 0; font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; border: #333333 3px solid; background-color: #FAFAFA; color: #000000; display:none;">
 <div style="font-weight: bold; text-align: left; color: #FFFFFF; padding: 5px; background-color:#006394">Bienvenidos</div>
 <p style="padding: 5px; text-align: justify; line-height:normal">Gracias por tu visita! nos vemos pronto.</p>
  <div style="padding: 10px; background-color: #F0F0F0; text-align: center; margin-top: 44px;"><input id="btnAceptar" onclick="ocultarVentana();" name="btnAceptar" size="20" type="button" value="Aceptar" />
 </div>
</div>
</body-->
