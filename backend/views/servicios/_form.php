<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use backend\models\Familia;
use kartik\money\MaskMoney;
/* @var $this yii\web\View */
/* @var $model backend\models\Servicios */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
    function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58 ) )
                return true;
            return false;
        }
    function isNumberDe(evt) {
            var nav4 = window.Event ? true : false;
            var key = nav4 ? evt.which : evt.keyCode;
            return (key <= 13 || key==46 || (key >= 48 && key <= 57));
        }
</script>
<br>
<div id="semitransparente">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        Ingrese los datos que se le solicitan en el siguiente formulario.
    </div>
            <!-- /.panel-heading -->
<div class="panel-body">
<div class="servicios-form">
  <div class="col-lg-6">
      <?php $form = ActiveForm::begin(['options' => ['name'=>'caja'],]); ?>

      <?= $form->field($model, 'tipo')->textInput(['readonly' => true, 'value' => 'Servicio']) ?>

      <!--?= $form->field($model, 'numFacturaProv')->textInput() ?-->


      <!--?= $form->field($model, 'cantidadInventario')->textInput() ?-->

      <!--?= $form->field($model, 'cantidadMinima')->textInput() ?-->

      <!--?= $form->field($model, 'codFamilia')->dropDownList(
                  ArrayHelper::map($model->getNombreFamilia(),'codFamilia','descripcion'),['prompt' => 'Selecccione']
                  )?-->

      <!--?= $form->field($model, 'codFamilia')->widget(TypeaheadBasic::classname(), [
      'data' => ArrayHelper::map($model->getNombreFamilia(),'codFamilia','descripcion'),
       'language'=>'es',
      'options' => ['placeholder' => ''],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);?-->

    <?= $form->field($model, 'codFamilia')->widget(Select2::classname(), [
      'data' => ArrayHelper::map($model->getNombreFamilia(),'codFamilia','descripcion'),
      'language'=>'es',
      'options' => ['placeholder' => ''],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ])->label('Categoría');
      ?>

  <?= $form->field($model, 'nombreProductoServicio')->textInput(['maxlength' => 50]) ?>

  </div>
  <div class="col-lg-6">
    <div class="col-lg-8">
      <?= $form->field($model, 'codProdServicio')->textInput(['maxlength' => 50, "onkeypress"=>"return isNumber(event)"]) ?>
    </div>
      <!--?= $form->field($model, 'precioCompra')->textInput() ?-->

      <!--?= $form->field($model, 'porcentUnidad')->textInput() ?-->

      <!--?= $form->field($model, 'precioVenta')->textInput() ?-->

      <!--?= $form->field($model, 'exlmpuesto')->textInput(['maxlength' => 2]) ?-->

      <!--?= $form->field($model, 'precioVentaImpuesto')->textInput() ?-->

      <!--?= $form->field($model, 'anularDescuento')->textInput() ?-->
    <div class="col-lg-5">
      <?= $form->field($model, 'precioMinServicio')->textInput(['maxlength' => 13, 'id'=>'preciom', "onkeypress"=>"return isNumberDe(event)"]) ?>
    </div>
    <div class="col-lg-3">
      <!--?= $form->field($model, 'exlmpuesto')->radioList(['Si'=>'Si','No'=>'No'])?-->
    </div>
       <!--?= /*$form->field($model, 'precioMinServicio')->
                  widget(MaskMoney::classname(), [
                  'options'  => ['id'=>'preciom'],
                  'pluginOptions' => [
                  'prefix' => '¢',
                  'thousandSeparator' => ',',
                  'decimalSeparator' => '.',
                  'precision' => 2,
                  'allowZero' => false,
                  'allowNegative' => false,
                  'maxlength' => 1]
      ])*/ ?-->
    <div class="col-lg-5">
      <label>Precio + IV</label>
      <div id="precioimpuesto" style="background-color:#FFC; padding:12px"></div><br>
      <div class="form-group">
          <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      </div>
    </div>
      <?php ActiveForm::end(); ?>

  </div>
</div>
            <!-- /.panel-body -->
</div>
        <!-- /.panel -->
</div>

</div>
</div>
