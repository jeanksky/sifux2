<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ServiciosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codProdServicio') ?>

    <?php // echo $form->field($model, 'tipo') ?>

    <?php // echo $form->field($model, 'numFacturaProv') ?>

    <?= $form->field($model, 'nombreProductoServicio') ?>

    <?php // echo $form->field($model, 'cantidadInventario') ?>

    <?php // echo $form->field($model, 'cantidadMinima') ?>

    <?= $form->field($model, 'codFamilia') ?>

    <?php // echo $form->field($model, 'precioCompra') ?>

    <?php // echo $form->field($model, 'porcentUnidad') ?>

    <?php // echo $form->field($model, 'precioVenta') ?>

    <?php // echo $form->field($model, 'exlmpuesto') ?>

    <?php // echo $form->field($model, 'precioVentaImpuesto') ?>

    <?php // echo $form->field($model, 'anularDescuento') ?>

    <?= $form->field($model, 'precioMinServicio') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
