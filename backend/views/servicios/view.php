<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
//----librerias para modal
use yii\bootstrap\Modal;//para activar modal
use yii\helpers\Url;//para enviar por url en la modal
use yii\widgets\ActiveForm;//para activar el formulario
/* @var $this yii\web\View */
/* @var $model backend\models\Servicios */

$this->title = $model->nombreProductoServicio;
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<div class="col-lg-8">
<div class="servicios-view">

    <h3><?= Html::encode($this->title) ?></h3>

   <p style="text-align:right">
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codProdServicio], ['class' => 'btn btn-primary']) ?>
        <!--?= Html::a('Eliminar', ['delete', 'id' => $model->codProdServicio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres borrar este servicio?',
                'method' => 'post',
            ],
        ]) ?-->
        <?= Html::a('Eliminar', ['#'], [
            'id' => 'activity-index-link-delete',
            'class' => 'btn btn-danger',
            'data-toggle' => 'modal',
            'data-target' => '#modalDelete',
            'onclick' => 'javascript:enviaResultado("'.$model->codProdServicio.'")',
            'href' => Url::to('#'),
            'data-pjax' => '0',
        ]) ?>
    </p>
<div id="semitransparente">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign'=> DetailView::ALIGN_LEFT ,
        'attributes' => [
            'codProdServicio',
            //'tipo',
            //'numFacturaProv',
            ['attribute'=>'nombreProductoServicio',  'valueColOptions'=>['style'=>'width:40%'] ],
            //'cantidadInventario',
            // 'cantidadMinima',
             'codFamilia',
            // 'precioCompra',
            // 'porcentUnidad',
            // 'precioVenta',
            // 'exlmpuesto',
            // 'precioVentaImpuesto',
            // 'anularDescuento',
            // 'precioMinServicio',
             [
            'attribute' => 'precioMinServicio',
            'format' => ['decimal',2],
             ],
        ],
    ]) ?>
</div></div>
</div>
<?php //-------------------------------------------------------
//-------------------------------Pantallas modales-------------
//------Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar este servicio necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "codProdServicio" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Servicio', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este servicio?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
     <?php
        Modal::end();
        //-------------------web/index.php?r=clientes%2Fdelete&id=45
?>
