<?php

use yii\helpers\Html;
use kartik\widgets\AlertBlock;
/* @var $this yii\web\View */
/* @var $model backend\models\Servicios */

$this->title = 'Actualizar servicio: ' . ' ' . $model->nombreProductoServicio;
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombreProductoServicio, 'url' => ['view', 'id' => $model->codProdServicio]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
        //Funcion para poner mascara de dicimales
        function currency(value, decimals, separators) {
            decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
            separators = separators || [',', "'", '.'];
            var number = (parseFloat(value) || 0).toFixed(decimals);
            if (number.length <= (4 + decimals))
                return number.replace('.', separators[separators.length - 1]);
            var parts = number.split(/[-.]/);
            value = parts[parts.length > 1 ? parts.length - 2 : 0];
            var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
                separators[separators.length - 1] + parts[parts.length - 1] : '');
            var start = value.length - 6;
            var idx = 0;
            while (start > -3) {
                result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                    + separators[idx] + result;
                idx = (++idx) % 2;
                start -= 3;
            }
            return (parts.length == 3 ? '-' : '') + result;
        }
       $(document).ready(function(){
            $("#preciom").keyup(function(){
                var iva=parseFloat(13/100);
                var preciom = parseFloat($(this).val());
                //var total = (preciom + (preciom * iva)).toFixed(2);
                var impuesto = preciom * iva;
                var total = parseFloat(impuesto+preciom);
                $("#precioimpuesto").html('¢'+ currency(total));
            })
        });
       function run_precioimpuesto() {
                var iva=parseFloat(13/100);
                var preciom = parseFloat($("#preciom").val());
                //var total = (preciom + (preciom * iva)).toFixed(2);
                var impuesto = preciom * iva;
                var total = parseFloat(impuesto+preciom);
                $("#precioimpuesto").html('¢'+ currency(total));
       }
        setInterval(run_precioimpuesto,100);


</script>
<div class="servicios-update">
<div class="col-lg-12">
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <p>
        <?= Html::a('Regresar', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
</div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
