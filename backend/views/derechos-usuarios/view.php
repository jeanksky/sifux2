<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DerechosUsuarios */

$this->title = $model->idDerechoUsuario;
$this->params['breadcrumbs'][] = ['label' => 'Derechos Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="derechos-usuarios-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idDerechoUsuario], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idDerechoUsuario], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div id="semitransparente">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idDerechoUsuario',
            'idUsuario',
            'caja',
            'recepcion',
            'mecanica',
            'Otros',
        ],
    ]) ?>
</div>

</div>
