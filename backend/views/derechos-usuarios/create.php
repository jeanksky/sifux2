<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DerechosUsuarios */

$this->title = 'Create Derechos Usuarios';
$this->params['breadcrumbs'][] = ['label' => 'Derechos Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="derechos-usuarios-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
