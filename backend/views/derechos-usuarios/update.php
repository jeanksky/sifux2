<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DerechosUsuarios */

$this->title = 'Update Derechos Usuarios: ' . ' ' . $model->idDerechoUsuario;
$this->params['breadcrumbs'][] = ['label' => 'Derechos Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idDerechoUsuario, 'url' => ['view', 'id' => $model->idDerechoUsuario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="derechos-usuarios-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
