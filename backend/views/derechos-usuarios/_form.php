<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DerechosUsuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="derechos-usuarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idUsuario')->textInput() ?>

    <?= $form->field($model, 'caja')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'recepcion')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'mecanica')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'Otros')->textInput(['maxlength' => 50]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
