<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\MarcasProducto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marcas-producto-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class='col-xs-5'>
      <?= $form->field($model, 'nombre_marca')->textInput(['maxlength' => true]) ?>
    </div>
    <div class='col-xs-4'>
      <?= $form->field($model, 'estado')->widget(Select2::classname(), [
          'data' => ['Activo'=>'Activo', 'Inactivo'=>'Inactivo'],
          'language'=>'es',
          'options' => ['placeholder' => ''],
          'pluginOptions' => [
              'allowClear' => true,
          ],
      ]) ?>
    </div>
    <div class='col-xs-3'><br>
      <div class="form-group">
          <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
