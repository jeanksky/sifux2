<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\MarcasProducto;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\AlertBlock; //para mostrar la alerta
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MarcasProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marcas de Productos';
$this->params['breadcrumbs'][] = $this->title;
$model = new MarcasProducto();
?>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
<div class="marcas-producto-index">
<style media="screen">

</style>
    <h1 style="text-align: center"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class='well col-xs-6'>
      <h3>Ingresa una nueva marca de un producto</h3>
      <div class="marcas-producto-form">
          <?php $form = ActiveForm::begin(['method' => 'post', 'action'=>['/marcas-producto/create'],]); ?>
          <div class='col-xs-5'>
            <?= $form->field($model, 'nombre_marca')->textInput(['maxlength' => true]) ?>
          </div>
          <div class='col-xs-4'>
            <?= $form->field($model, 'estado')->widget(Select2::classname(), [
    				    'data' => ['Activo'=>'Activo', 'Inactivo'=>'Inactivo'],
    				    'language'=>'es',
    				    'options' => ['placeholder' => ''],
    				    'pluginOptions' => [
    				        'allowClear' => true,
    				    ],
    				]) ?>
          </div>
          <div class='col-xs-3'><br>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

          </div>

          <?php ActiveForm::end(); ?>
      </div>
      <div class='col-xs-12' id="campo_notificacion"></div>
    </div>
  <div class='col-xs-6'>
    <?php  $dataProvider->pagination->pageSize = 8; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute' => 'id_marcas',
              'options'=>['style'=>'width:15%;'],
              'headerOptions' => [ 'style' => 'text-align: center;' ],
              'contentOptions' => ['style'=>'vertical-align: middle;text-align: center;'],
            ],
            [
              'attribute' => 'nombre_marca',
              'options'=>['style'=>'width:55%;'],
              'label' => 'Nombre de Marca',
              'format' => 'raw',
              'value' => function ($model, $key, $index, $grid) {
                  return Html::input('text', '', $model->nombre_marca,
                  [
                    'size' => '30',
                    'id'=>'marca_'.$model->id_marcas,
                    'class' => 'form-control',
                    'placeholder' =>'Marca no puede quedar vacío',
                    'onchange' => 'javascript:actualiza_marca(this.value,'.$model->id_marcas.')']);
              },
            ],
            [
              'attribute' => 'estado',
              'options'=>['style'=>'width:30%'],
              'label' => 'Estado',
              'format' => 'raw',
              'value' => function ($model, $key, $index, $grid) {
                  $estilo = '<style type="text/css">
                    .select2-container--krajee .select2-selection--single .estado_marca_'.$model->id_marcas.' {
                      background-color: #0DBB6E;
                    }
                  </style>';
                  return $estilo.Select2::widget([
                                  'name' => '',
                                  'value' => $model->estado,
                                  'data' => ['Activo'=>'Activo', 'Inactivo'=>'Inactivo'],
                                  'theme' => Select2::THEME_CLASSIC,
                                  'options' => [
                                      //'style'=>'.container--krajee .selection--single {background-color: #0DBB6E}',
                                      'id'=>'estado_marca_'.$model->id_marcas,
                                      'placeholder'=>'- Seleccione un estado -',
                                      'onchange'=>'javascript:cambiar_estado(this.value, '.$model->id_marcas.')',
                                      //'multiple' => false //esto me ayuda a que solo se obtenga uno
                                  ],
                                  'pluginOptions' => ['initialize'=> true,'allowClear' => false]
                              ]);
              },
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
        ],
    ]); ?>
  </div>

</div>
<script type="text/javascript">
  function actualiza_marca(value, id) {
    document.getElementById("campo_notificacion").innerHTML = '';
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('marcas-producto/actualiza_marca') ?>",
    {'value' : value, 'id' : id, 'colum' : 'nombre_marca'},
        function( data ) {
        $("#campo_notificacion").html(data);
        setTimeout(function() {//aparece la alerta en un milisegundo
            $("#campo_notificacion").fadeIn();
        });
        setTimeout(function() {//se oculta la alerta luego de 3 segundos
            $("#campo_notificacion").fadeOut();
        },2000);
    });
  }
  function cambiar_estado(value, id) {
    document.getElementById("campo_notificacion").innerHTML = '';
    $.get( "<?= Yii::$app->getUrlManager()->createUrl('marcas-producto/actualiza_marca') ?>",
    {'value' : value, 'id' : id, 'colum' : 'estado'},
        function( data ) {
          $("#campo_notificacion").html(data);
          setTimeout(function() {//aparece la alerta en un milisegundo
              $("#campo_notificacion").fadeIn();
          });
          setTimeout(function() {//se oculta la alerta luego de 3 segundos
              $("#campo_notificacion").fadeOut();
          },2000);
        });
  }
</script>
