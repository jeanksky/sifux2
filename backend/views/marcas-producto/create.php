<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MarcasProducto */

$this->title = 'Create Marcas Producto';
$this->params['breadcrumbs'][] = ['label' => 'Marcas Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marcas-producto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
