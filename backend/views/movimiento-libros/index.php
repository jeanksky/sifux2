<?php
//este link es para consultar info para modal de create y update
//https://yii2enespanol.com/2015/05/23/abm-con-ajax-en-yii-2/
use backend\models\CuentasBancarias;
use backend\models\TipoDocumento;
use backend\models\Moneda;
use backend\models\TipoCambio;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\widgets\AlertBlock;
//use kartik\widgets\Select2;
//use yii\helpers\ArrayHelper;
//use yii\helpers\Json;
//use yii\web\JsExpression;
//use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
//----------------------------------------------Para la modal de elimacion por permiso admin
use yii\widgets\ActiveForm;//para activar el formulario

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MovimientoLibrosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
$this->title = 'Movimiento en Libros';
$this->params['breadcrumbs'][] = $this->title;
$moneda_local = 0;
$monedaasignada = '';
$monedas = Moneda::find()->all();
foreach($monedas as $moneda) {
    if ($moneda['monedalocal']=='x') {
        $moneda_local = $moneda['idTipo_moneda'];
        $monedaasignada = $moneda['monedalocal'];
    }
}
$tipocambioasignado = '';
$tipocambios = TipoCambio::find()->all();
foreach($tipocambios as $tp) {
    if ($tp['usar']=='x') {
        $tipocambioasignado = $tp['usar'];
    }
}
?>
<script type="text/javascript" src="<?php echo $jquery_min_js; ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
       // $('[data-toggle="modal"]').tooltip();
        $('[data-toggle="toggle"]').popover();
    });

    $(document).on('click', '#activity-link-create', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-create-movimiento').html(data);//muestro en el div los datos del create
                        $('#crearmovimientomodal').modal('show');//muestro la modal

                    }
                );
            }));
    $(document).on('click', '#activity-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        document.getElementById("well-create-movimiento").innerHTML="";//tengo que destruir el div de well-create-movimiento para que well-update-movimiento esté limpio para la modal
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-update-movimiento').html(data);//muestro en el div los datos del create
                        $('#actualizarmovimientomodal').modal('show');//muestro la modal

                    }
                );
            }));
    //fin modal force focus
    //cargamos el id de movimiento en libros a eliminar en la modal
    function enviaResultado(id) {
        $('#org').val(id);
        }
</script>
<style>
    #crearmovimientomodal .modal-dialog{
    width: 70%!important;
    /*margin: 0 auto;*/
    }
    #actualizarmovimientomodal .modal-dialog{
    width: 70%!important;
    /*margin: 0 auto;*/
    }
    .espacio_monto {
        text-align: left;
    }
</style>
<?php if(@$_GET['carga'] == true){ ?>

            <script type="text/javascript">
            $(window).load(function(){
                $.get(//Obtiene los datos por medio de GET
                "<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-libros/create') ?>", //obtiene la URL del create
                    function (data) {
                        document.getElementById("well-create-movimiento").innerHTML="";
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-create-movimiento').html(data);//muestro en el div los datos del create
                        $('#crearmovimientomodal').modal('show');//muestro la modal
                        /*setTimeout(function (){
                            document.getElementById('numero_documento').select();//muestro seleccionado el input numero de documento
                        }, 500);*/
                    }// fin de la funcion que carga los datos a la Body de la modal y la muestra
                );// fin del get
            });
        </script>
        <?php } ?>
<div class="movimiento-libros-index">
    <?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
        ]);?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);
        if ($monedaasignada == 'x' && $tipocambioasignado=='x') {
    ?>

    <p style="text-align:right">
        <?= Html::a('Crear nuevo movimiento en libros', '#', [
            'id' => 'activity-link-create',
            'data-toggle' => 'modal',
            'class' => 'btn btn-success',
            //'data-trigger'=>'hover',
            //'data-target' => '#crearmovimientomodal',
            'data-url' => Url::to(['create']),
            'data-pjax' => '0',
            'title' => Yii::t('app', 'Proceda a crear un nuevo movimiento en libros'),
        ]) ?>
    </p>
<div class="table-responsive">
          <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php Pjax::begin() ?>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
<!--?php  $dataProvider->pagination->pageSize = 10; ?-->
    <?= GridView::widget([
        'id' => 'solicitante-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idMovimiento_libro',
            //'numero_documento',//
            [
              'attribute' => 'numero_documento',
              'label' => 'No Docum',
            ],
            //'idCuenta_bancaria',//
            [
                'attribute' => 'tbl_cuentas_bancarias.numero_cuenta',
                'label' => 'Cnta Bancaria',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                    if($cb = CuentasBancarias::findOne($model->idCuenta_bancaria))
                    {
                        return $cb->numero_cuenta;
                    }else{
                        if ($model->idCuenta_bancaria=="")
                        return 'Cuenta bancaria no asignada';
                        else
                        return $model->idCuenta_bancaria;
                    }
                },
            ],
            //'idTipodocumento',//
            [
                'attribute' => 'tbl_tipo_documento.descripcion',
                'label' => 'Tipo Documento',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                    if($td = TipoDocumento::findOne($model->idTipodocumento))
                    {
                        return $td->descripcion;
                    }else{
                        if ($model->idTipodocumento=="")
                        return 'Tipo de documento no asignado';
                        else
                        return $model->idTipodocumento;
                    }
                },
            ],
            //'idTipoMoneda',//
            [
                'attribute' => 'tbl_moneda.descripcion',
                'label' => 'Moneda',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                    if($mo = Moneda::findOne($model->idTipoMoneda))
                    {
                        return $mo->descripcion;
                    }else{
                        if ($model->idTipoMoneda=="")
                        return 'Moneda no asignada';
                        else
                        return $model->idTipoMoneda;
                    }
                },
            ],
            [   //mónto debito
                'attribute' => 'monto',
                'label' => 'Débito',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                    if($td = TipoDocumento::findOne($model->idTipodocumento))
                    {
                        $mo = Moneda::findOne($model->idTipoMoneda);
                        return $td->tipo_movimiento == 'Débito' ? $mo->simbolo.number_format($model->monto,2) : '';
                    }
                },
            ],
            [   //monto crédito
                'attribute' => 'monto',
                'label' => 'Crédito',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $grid) {
                    if($td = TipoDocumento::findOne($model->idTipodocumento))
                    {
                        $mo = Moneda::findOne($model->idTipoMoneda);
                        return $td->tipo_movimiento == 'Crédito' ? $mo->simbolo.number_format($model->monto,2) : '';
                    }
                },
            ],
            [   //moneda local debito
                'attribute' => 'monto_moneda_local',
                'label' => 'Débito local',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'espacio_monto'
                ],
                'value' => function ($model, $key, $index, $grid) {
                    if($td = TipoDocumento::findOne($model->idTipodocumento))
                    {
                        $simbolo_mon_local = '';
                        $monedas = Moneda::find()->all();
                        foreach($monedas as $moneda) {
                            if ($moneda['monedalocal']=='x') {
                                $simbolo_mon_local = $moneda['simbolo'];
                            }
                        }
                        return $td->tipo_movimiento == 'Débito' ? $simbolo_mon_local.number_format($model->monto_moneda_local,2) : '';
                    }
                },
            ],
            [   //moneda local credito
                'attribute' => 'monto_moneda_local',
                'label' => 'Crédito local',
                'format' => 'raw',
                'options' => [
                    'class' => 'espacio_monto',
                ],
                'value' => function ($model, $key, $index, $grid) {
                    if($td = TipoDocumento::findOne($model->idTipodocumento))
                    {
                        $simbolo_mon_local = '';
                        $monedas = Moneda::find()->all();
                        foreach($monedas as $moneda) {
                            if ($moneda['monedalocal']=='x') {
                                $simbolo_mon_local = $moneda['simbolo'];
                            }
                        }
                        return $td->tipo_movimiento == 'Crédito' ? $simbolo_mon_local.number_format($model->monto_moneda_local,2) : '';
                    }
                },
            ],

            'beneficiario',//
            //'detalle',
            //'fecha_registro',
            [
              'attribute' => 'fecha_registro',
              'label' => 'Fech registro',
            ],
            // 'fecha_documento',
            // 'monto',
            // 'tasa_cambio',
            // 'monto_moneda_local',
            // 'beneficiario',
            // 'detalle',
            // 'fecha_registro',
            // 'usuario_registra',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}{view}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'id' => 'activity-link-update',
                            'data-toggle' => 'modal',
                            //'data-trigger'=>'hover',
                            //'data-target' => '#crearmovimientomodal',
                            'data-url' => Url::to(['update', 'id' => $model->idMovimiento_libro]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Proceda a modificar el movimiento en libro'),
                        ]);
                    },
                'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', '#', [
                            'id' => 'activity-detalle',
                            'data-trigger'=>'hover', 'data-content'=>$model->detalle,
                            //'data-target' => '#crearmovimientomodal',
                            'data-toggle' => 'toggle',
                            'data-placement'=>'left',
                            'data-url' => Url::to(['#']),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Detalle'),
                        ]);
                    },
                'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                            'id' => 'activity-index-link-delete',
                            'class' => 'activity-delete-link',
                            'title' => Yii::t('app', 'Eliminar movimiento en libro '),
                            'data-toggle' => 'modal',
                            'data-target' => '#modalDelete',
                            'onclick' => 'javascript:enviaResultado("'.$model->idMovimiento_libro.'")',
                            'href' => Url::to('#'),
                            'data-pjax' => '0',
                            ]);
                    },
            ],
            ],
        ],
    ]);
Pjax::end();
     ?>
    </div>
<?php

    } else {
        echo '<div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Licencia vencida:</span>
                  Este módulo se mostrará hasta que esté asignada una moneda local y un respectivo tipo de cambio.
                </div>';
    }
 ?>
</div>
<?php
//------------------------modal para crear movimiento en libros
        Modal::begin([
            'id' => 'crearmovimientomodal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Crear movimiento en libros</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-create-movimiento'></div></div>";

        Modal::end();
//------------------------modal para actualizar movimiento en libros
        Modal::begin([
            'id' => 'actualizarmovimientomodal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title">Actualizar movimiento en libros</h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-update-movimiento'></div></div>";

        Modal::end();
//--------------------modal Eliminar
        Modal::begin([
            'id' => 'modalDelete',
            'header' => '<h4 class="modal-title">Para eliminar un movimiento en libros necesita el permiso del administrador</h4>',
            'footer' => '<a href="#" class="btn btn-default" data-dismiss="modal">Salir</a>',
        ]);

         $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'delete-pass-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);
        ?>
                <input type="hidden" id="org" name = "idMovimiento_libro" />
                <?= $form->field($us, 'username')->textInput(['value' => ''])->label('Usuario Admnistrador') ?>
                <?= $form->field($us, 'password')->passwordInput(['value' => ''])->label('Contraseña') ?>
                <div class="form-group" style="text-align:center">
                    <?= Html::a('Eliminar Movimiento en libros', ['delete'], [
                        'class' => 'btn btn-danger btn-block',
                        'name' => 'login-button',
                        'data' => [
                            'confirm' => '¿Está seguro de eliminar este movimiento?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
        <?php  ActiveForm::end();  ?>
        <?php
        Modal::end();
