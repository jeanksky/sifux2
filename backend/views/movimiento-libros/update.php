<?php

use yii\helpers\Html;
use backend\models\Moneda;
use backend\models\TipoDocumento;
/* @var $this yii\web\View */
/* @var $model backend\models\MovimientoLibros */

$this->title = 'Actualizar movimiento en libro: ' . ' ' . $model->idMovimiento_libro;
$this->params['breadcrumbs'][] = ['label' => 'Movimiento Libros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idMovimiento_libro, 'url' => ['view', 'id' => $model->idMovimiento_libro]];
$this->params['breadcrumbs'][] = 'Actualizar movimiento en libro';


$moneda = Moneda::find()->where(['idTipo_moneda'=>$model->idTipoMoneda])->one();
$tipodocumento = TipoDocumento::find()->where(['idTipo_documento'=>$model->idTipodocumento])->one();
$mostrar_moneda = $moneda->descripcion;
$simbolo = $moneda->simbolo;
$tipomovimiento = $tipodocumento->tipo_movimiento;
$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
?>

<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->
<script type="text/javascript" src="<?php echo $jquery_min_js; ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
		$('#mostrar_moneda').html('<?= $mostrar_moneda ?>');
		$('#simbolo').html('<?= $simbolo ?>');
		$('#tipomovimiento').html('<?= $tipomovimiento ?>');
    });
</script>
<div class="movimiento-libros-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
