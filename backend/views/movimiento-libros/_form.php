<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use backend\models\CuentasBancarias;
use backend\models\Moneda;
use backend\models\TipoDocumento;
use backend\models\Funcionario;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\MovimientoLibros */
/* @var $form yii\widgets\ActiveForm */

?>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->

<script type="text/javascript">

    $(document).ready(function () {
        //$("#ddl-moneda").attr("disabled", true);
        //$('select').select2('readonly', true);
        //$('#ddl-moneda').select2().select2("readonly", true);
        //$('#ddl-moneda').select2({readonly: true});
        //$("#tasa_cambio").attr('readonly', 'readonly');
        $('#check_confirm').on('click', function() {

            if (this.checked == true)
                {
                    $('#crear_movimiento').removeClass('disabled');
                }
            else {
                    $('#crear_movimiento').addClass('disabled');
                 }

        });
    });

    //espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

    //Funcion para que me permita ingresar solo numeros
    function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ( (charCode > 47 && charCode < 58))
                return true;
            return false;
        }

    //envio el id de la cuenta al controlador para obtener la tasa de cambio si esta es diferente a la moneda local
    function obtenercuenta(value) {
        consulta_repetido();
        var monto = $(".monto").val();//document.getElementById("monto")
        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-libros/obtenercuenta') ?>",
                type:"post",
                dataType: 'json',
                data: { 'idBancos': value },
                success: function(data){
                    //$("#index-tipo_cambio").load(location.href+' #index-tipo_cambio','');
                    $('#ddl-moneda').val(data.idTipo_moneda).change();
                    $('#mostrar_moneda').html(data.descrip_moneda);
                    $('#simbolo').html(data.simbolo);
                    if (data.local=='x') {
                        $('#tasa_cambio').val('');
                        $('#monto_mon_local').val('');
                    } else {
                        $('#tasa_cambio').val(data.valorcambio);
                        monto_mon_local = data.valorcambio*monto.value;
                        $.ajax({
                            url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-libros/obtenermontomonedalocal') ?>",
                            type:"post",
                            data: { 'monto_mon_local': monto_mon_local },
                            success: function(data){
                                $('#monto_mon_local').val(data);
                            }
                        });
                        //$('#monto_mon_local').val((data.valorcambio*monto.value).toLocaleString('hi-IN', { style: 'decimal', decimal: '2' }));
                    };

                    //$('#moneda').val(data);
                },
                error: function(msg, status,err){
                 //alert('Error, consulte linea 65 (informar a Nelux)');
                  $('#mostrar_moneda').html('');
                  $('#simbolo').html('...');
                  $('#tasa_cambio').val('');
                  $('#monto_mon_local').val('');
                }
            });
    }

    //envio al controlador el id del tipo de documento para que el sistema muestre al usuario el tipo de movimiento
    function obtenertipodocumento(value){

        $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-libros/obtenertipodocumento') ?>",
                type:"post",
                data: { 'idTipo_documento': value },
                success: function(data){
                    $('#tipomovimiento').html(data);
                },
                error: function(msg, status,err){
                  $('#tipomovimiento').html('');
                }
            });
    }

    //calculadora me devuelve el monto en moneda local multiplicando por la tasa de cambio
    function obtenermontomonedalocal(){
        monto = $(".monto").val();//document.getElementById("monto");
        tasa_cambio = document.getElementById("tasa_cambio");
        if (tasa_cambio!='') {
            monto = Number(monto);
            tasa_cambio = Number(tasa_cambio.value);
            monto_mon_local = monto*tasa_cambio;
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-libros/obtenermontomonedalocal') ?>",
                type:"post",
                data: { 'monto_mon_local': monto_mon_local },
                success: function(data){
                    $('#monto_mon_local').val(data);
                }
            });

        }

    }

    function habilito(argument) {
        if (argument == true) {
            $('#crear_movimiento').removeClass('disabled');
        }
        else {
            $('#crear_movimiento').addClass('disabled');
        }
    }
    function consulta_repetido(){
        id_cuenta_bancaria = $(".ddl-cuenta_bancaria").val();//document.getElementById("ddl-cuenta_bancaria").value;
        numero_documento = $(".numero_documento").val();//document.getElementById("numero_documento").value;
        if (id_cuenta_bancaria!=''&&numero_documento!='') {
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('movimiento-libros/consulta_repetido') ?>",
                    type:"post",
                    data: { 'numero_documento' : numero_documento, 'id_cuenta_bancaria' : id_cuenta_bancaria },
                    success: function(data){
                        if (data=='no') {
                            $('#notif_repetido').html('<center><font size=4 color="blue">Atención, documento repetido</font></center>');
                            $('#confirmo_repetido').html('<div class="alert alert-info" role="alert"> Deseo crear el movimiento con el mismo documento <input type="checkbox" id="check_confirm" onchange="if(this.checked == true){habilito(true);}else{habilito(false);}"/></div>');
                            $('#crear_movimiento').addClass('disabled');
                        }
                        else {
                            $('#notif_repetido').html('');
                            $('#confirmo_repetido').html('');
                            $('#crear_movimiento').removeClass('disabled');
                        }
                    },
                    error: function(msg, status,err){
                     $('#notif_repetido').html('Error');
                    }
            });
        }
    }
</script>
<?php
/*$this->registerJs('
    // obtener la id del formulario y establecer el manejador de eventos
        $("form#movimientolibros-form").on("beforeSubmit", function(e) {
            var form = $(this);
            $.post(
                form.attr("action")+"&submit=true",
                form.serialize()
            )
            .done(function(result) {
                form.parent().html(result.message);
                $.pjax.reload({container:"#solicitante-grid"});
            });
            return false;
        }).on("submit", function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        });
    ');*/
?>
<?php
    $model->fecha_documento = $model->isNewRecord ? date("d-m-Y") : $model->fecha_documento;
    $model->fecha_registro = $model->isNewRecord ? date("d-m-Y") : $model->fecha_registro;
    $model->usuario_registra = '';
    $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
    if ($funcionario) {
        $model->usuario_registra = $funcionario->nombre . ' ' . $funcionario->apellido1 . ' ' . $funcionario->apellido2;
    } else {
        $model->usuario_registra = Yii::$app->user->identity->username;
    }

?>
<div class="movimiento-libros-form">

    <?php $form = ActiveForm::begin([
        'id' => 'movimientolibros-form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
    ]); ?>



    <div class="col-lg-12">
    <div class="col-lg-3">
    <?= $form->field($model, 'idCuenta_bancaria')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(CuentasBancarias::find()->asArray()->all(), 'idBancos',  function($element) {
                                    return $element['numero_cuenta'].' '.$element['descripcion'];
                                }),
                                'options'=>['class'=>'ddl-cuenta_bancaria form-control', 'onchange'=>'javascript:obtenercuenta(this.value)', 'placeholder'=>'- Seleccione -'],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true],
                            ]) ?>
    </div>

    <div class="col-lg-2">
    <label>Tipo Moneda</label>
    <div id="mostrar_moneda" style="background-color:#FFC; padding:12px" ></div>
    <?= $form->field($model, 'idTipoMoneda')->textInput(['id'=>'ddl-moneda','type'=>'hidden','maxlength' => true])->label(false) ?>
    </div>

    <div class="col-lg-5">
    <?php
     if ($model->isNewRecord) {
     echo $form->field($model, 'idTipodocumento')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(TipoDocumento::find()->where(['estado'=>'Activo'])->asArray()->all(), 'idTipo_documento',  function($element) {
                                    return $element['idTipo_documento'] . ' - ' .$element['descripcion'];
                                }),
                                'options'=>['onchange'=>'javascript:obtenertipodocumento(this.value)', 'placeholder'=>'- Seleccione -'],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true],
                            ]);
    } else{
        echo $form->field($model, 'idTipodocumento')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(TipoDocumento::find()->asArray()->all(), 'idTipo_documento',  function($element) {
                                    return $element['idTipo_documento'] . ' - ' .$element['descripcion'];
                                }),
                                'options'=>['onchange'=>'javascript:obtenertipodocumento(this.value)', 'placeholder'=>'- Seleccione -'],
                                'pluginOptions' => ['initialize'=> true,'allowClear' => true],
                            ]);
    }
                             ?>
    </div>

    <div class="col-lg-2">
    <label>Tipo movimiento</label>
    <div id="tipomovimiento" style="background-color:#FFC; padding:12px" ></div>
    </div>
    </div>

    <div class="col-lg-12">
        <div class="col-lg-3">
        <?= $form->field($model, 'numero_documento')->textInput(['class'=>'numero_documento form-control','onkeypress'=>'return isNumber(event)', 'onkeyup'=>"consulta_repetido()"]) ?>
        <div id="notif_repetido"></div>
        </div>

        <!--?= //$form->field($model, 'fecha_documento')->textInput() ?-->

        <div class="col-lg-3">
        <?= $form->field($model,'fecha_documento')->widget(DatePicker::className(),[
                                                               'name' => 'check_issue_date',
                                                                'disabled' => false,
                                                                'pluginOptions' => [
                                                                    'format' => 'dd-m-yyyy',
                                                                    'todayHighlight' => true,
                                                                    'autoclose' => true,
                                                                ]
                                                            ]) ?>
        </div>

        <div class="col-lg-2">
        <label>Monto</label>
        <?= $form->field($model, 'monto', ['template' => '<div class="form-group input-group"><span class="input-group-addon"><div id="simbolo">...</div></span>{input}</div>'])->textInput(['class'  => 'monto form-control', 'maxlength' => true, 'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"]) ?>

        </div>

        <div class="col-lg-2">
        <?= $form->field($model, 'tasa_cambio')->textInput(['readonly'=>true, 'id'  => 'tasa_cambio', 'maxlength' => true]) ?>
        </div>

        <div class="col-lg-2">
        <?php
        if ($model->isNewRecord) {
            echo $form->field($model, 'monto_moneda_local')->textInput(['id' => 'monto_mon_local', 'readonly'=>true, 'maxlength' => true]);
        }else{
            $model->monto_moneda_local = number_format($model->monto_moneda_local,2);
            echo $form->field($model, 'monto_moneda_local')->textInput(['id' => 'monto_mon_local', 'readonly'=>true, 'maxlength' => true]);
        }
        ?>
        </div>
    </div>

    <div class="col-lg-12"><br>
    <div class="col-lg-3">
    <?= $form->field($model, 'beneficiario')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-lg-4">
    <?= $form->field($model, 'detalle')->textarea(['maxlength' => true]) ?>
    </div>

    <div class="col-lg-2">
    <?= $form->field($model, 'fecha_registro')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
    </div>

    <div class="col-lg-3">
    <?= $form->field($model, 'usuario_registra')->textInput(['readonly'=>true, 'maxlength' => true]) ?>
    </div>
    <div class="col-lg-10">
        <div id="confirmo_repetido" style="float: right;"></div>
    </div>
    <div class="col-lg-2">

    <div class="form-group"><br>
    <p style="text-align:right">

        <?= Html::submitButton($model->isNewRecord ? 'Crear movimiento' : 'Actualizar movimiento', ['id'=>"crear_movimiento", 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </p>
    </div>

    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
