<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MovimientoLibros */

$this->title = 'Crear nuevo movimiento en libros';
$this->params['breadcrumbs'][] = ['label' => 'Movimiento en Libros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
?>
<script type="text/javascript" src="<?php echo $jquery_min_js; ?>"></script>
<div class="movimiento-libros-create">

    <!--h1><?= Html::encode($this->title) ?></h1-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
