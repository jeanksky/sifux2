<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MovimientoLibrosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movimiento-libros-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idMovimiento_libro') ?>

    <?= $form->field($model, 'idCuenta_bancaria') ?>

    <?= $form->field($model, 'idTipoMoneda') ?>

    <?= $form->field($model, 'idTipodocumento') ?>

    <?= $form->field($model, 'numero_documento') ?>

    <?php // echo $form->field($model, 'fecha_documento') ?>

    <?php // echo $form->field($model, 'monto') ?>

    <?php // echo $form->field($model, 'tasa_cambio') ?>

    <?php // echo $form->field($model, 'monto_moneda_local') ?>

    <?php // echo $form->field($model, 'beneficiario') ?>

    <?php // echo $form->field($model, 'detalle') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <?php // echo $form->field($model, 'usuario_registra') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
