<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MovimientoLibros */

$this->title = $model->idMovimiento_libro;
$this->params['breadcrumbs'][] = ['label' => 'Movimiento Libros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movimiento-libros-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idMovimiento_libro], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idMovimiento_libro], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idMovimiento_libro',
            'idCuenta_bancaria',
            'idTipoMoneda',
            'idTipodocumento',
            'numero_documento',
            'fecha_documento',
            'monto',
            'tasa_cambio',
            'monto_moneda_local',
            'beneficiario',
            'detalle',
            'fecha_registro',
            'usuario_registra',
        ],
    ]) ?>

</div>
