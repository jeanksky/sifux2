<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProveedoresGastos */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
                Ingrese los datos que se le solicitan en el siguiente formulario.
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="proveedores-gastos-form">
                    
                            <?php $form = ActiveForm::begin(); ?>
                    <div class="col-lg-3">
                            <!--?= $form->field($model, 'tipoEntidad')->textInput(['maxlength' => true]) ?-->
                            <?= $form->field($model, 'tipoEntidad')->radioList(array('Fisico'=>'físico','Juridico'=>'Jurídico')); ?>
                    </div>
                    <div class="col-lg-4">                
                        <?= $form->field($model, 'identificacion')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-5"> 
                        <?= $form->field($model, 'nombreEmpresa')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-3"> 
                        <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-4">
                        <?= $form->field($model, 'sitioWeb')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-3"> 
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?-->
                    <?= $form->field($model, 'estado')->radioList(array('activo'=>'Activo','inactivo'=>'Inactivo')); ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>