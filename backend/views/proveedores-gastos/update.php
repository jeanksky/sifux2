<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ProveedoresGastos */

$this->title = 'Update Proveedores Gastos: ' . ' ' . $model->idProveedorGastos;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idProveedorGastos, 'url' => ['view', 'id' => $model->idProveedorGastos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proveedores-gastos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
