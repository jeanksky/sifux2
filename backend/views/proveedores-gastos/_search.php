<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ProveedoresGastosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-gastos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idProveedorGastos') ?>

    <?= $form->field($model, 'tipoEntidad') ?>

    <?= $form->field($model, 'identificacion') ?>

    <?= $form->field($model, 'nombreEmpresa') ?>

    <?= $form->field($model, 'telefono') ?>

    <?php // echo $form->field($model, 'sitioWeb') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
