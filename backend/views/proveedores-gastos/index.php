<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\AlertBlock; //para mostrar la alerta

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProveedoresGastosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores Gastos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-gastos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p align="right">
        <?= Html::a('Crear Proveedores Gastos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idProveedorGastos',
            'tipoEntidad',
            'identificacion',
            'nombreEmpresa',
            'telefono',
            // 'sitioWeb',
            // 'email:email',
             'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
