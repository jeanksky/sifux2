<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\AlertBlock; //para mostrar la alerta

/* @var $this yii\web\View */
/* @var $model backend\models\ProveedoresGastos */

$this->title = $model->idProveedorGastos;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-gastos-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>
    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idProveedorGastos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idProveedorGastos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProveedorGastos',
            'tipoEntidad',
            'identificacion',
            'nombreEmpresa',
            'telefono',
            'sitioWeb',
            'email:email',
            'estado',
        ],
    ]) ?>

</div>
