<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ProveedoresGastos */

$this->title = 'Crear Proveedores Gastos';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-gastos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
