<?php

//Mega menu http://bootsnipp.com/snippets/featured/mega-menu-with-carousel-for-stores
//otro mega menu http://meganavbar.com/index.html
//otro mega menu sencilllo http://jsfiddle.net/apougher/ydcMQ/
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\checkbox\CheckboxX;
use kartik\nav\NavX;
use kartik\icons\Icon;
use yii\bootstrap\Modal;
//use backend\widgets\Alert;
use kartik\widgets\AlertBlock;
use kartik\alert\Growl;
use kartik\alert\Alert;
use yii\widgets\Menu;
use kartik\widgets\SideNav;
use yii\helpers\Url;
use backend\models\Funcionario;
use backend\models\Empresa;
use backend\models\Sistema_empresa;
use backend\config\params;
Icon::map($this);
/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);
//http://www.yiiframework.com/extension/yii2-backuprestore/ para respaldo de base de datos
$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';
$cssrefresh = \Yii::$app->request->BaseUrl.'/js/cssrefresh.js';
$dir_fuente_batman = \Yii::$app->request->BaseUrl.'/batmfa__.ttf';//obtienen la fuenta batman
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
$mycss = \Yii::$app->request->BaseUrl.'/css/mycss.css';

$sys_emp = new Sistema_empresa();

  //esto es para no almacene en cache
  //header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  //header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado
?>
<?php $this->beginPage() ?>
<?php $empresa = new Empresa(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!--link rel="stylesheet" href="/resources/demos/style.css"-->
    <link href="<?= $mycss ?>" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo $empresa->getFavicon()?>" type="image/x-icon" />
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!--script type="text/javascript" src="<?= $cssrefresh ?>"></script-->
    <!--script src="dist/js/bootstrap-checkbox.min.js"></script-->
    <script type="text/javascript" src="<?php echo $jquery_min_js; ?>"></script>
    <script type="text/javascript">
    // Dropdown Menu Fade
      /*jQuery(document).ready(function(){
        div = document.getElementById('acordeon_notificaciones');
        div.style.display = 'none';
          $(".dropdown").hover(
              function() { $('.dropdown-menu', this).stop().fadeIn("fast");
              },
              function() { $('.dropdown-menu', this).stop().fadeOut("fast");
          });
      });*/

    function usuario_live(id_user) {
      alert(id_user);
    }
    /* .......................................................................
    //Para mostrar a un boton que está cargando
    $('#myButton').on('click', function () {
        var $btn = $(this).button('loading')
        // business logic...
        $btn.button('reset')
      });*/
      //Para enviar notificación de errores
        $(function(){
            //-----------------------------------------------Chat
          var height = 0;
          $('#messages p').each(function(i, value){
              height += parseInt($(this).height());
          });
          height += '';
          $('#messages').animate({scrollTop: height});

          //--------------------------------formulario para notificar problemas http://jsfiddle.net/apougher/rjRwV/
            $('#notiproblem').popover({

                placement: 'top',
                title: 'Notifique su problema',
                html:true,
                content:  $('#myForm').html()
            }).on('click', function(){
              // had to put it within the on click action so it grabs the correct info on submit
              $('.btn-primary').click(function(){
               /* $('#result').after("Notificación presentada por " + $('#usuario_n').val());
                $.post('',  {
                    usuario_n: $('#usuario_n').val(),
                    asunto_n: $('#asunto_n').val(),
                    prioridad_n: $('#prioridad_n').val()
                }, function(r){
                  $('#pops').popover('hide')
                  $('#result').html('resonse from server could be here' )
                })*/
                var about = document.getElementById("about").value;
                if (about!='') {
                $.ajax({
                    url:"<?php echo Yii::$app->getUrlManager()->createUrl('version/enviarnotificacion') ?>",
                    type:"post",
                    data: {'usuario_n': $('#usuario_n').val(),'asunto_n': $('#asunto_n').val(),'prioridad_n': $('#prioridad_n').val(), 'about': $('#about').val()},
                    beforeSend: function() {
                        $('#result').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
                    },
                    success: function(data){
                        //location.reload();
                    $('#pops').popover('hide');

                    $('#result').html('Datos enviados correctamente');
                    setTimeout(function() { $("#result").fadeOut(); },3000); //se muestra solo 3 segundos

                    $('#about').val('');
                    }/*,
                    error: function(msg, status,err){
                                    $('#resultado').val('Dato no encontrado');
                                }*/
                }) ;}

              })
          })
        });//fin funktion
        //me permite ocultar el area que no voy a usar y mostrarlo si lo necesito
        /*function opcion_acordeon_notificaciones() {
            div = document.getElementById('acordeon_notificaciones');
            if (div.style.display != 'none') { div.style.display = 'none'; } else { div.style.display = ''; }
        }*/

        /*$(window).scroll(function(){
            if($(document).scrollTop()>=$(document).height()/5)
                $("#spopup").show("slow");else $("#spopup").hide("slow");
        });*/
        $(document).ready(function(){
          $("#notf").show();
        //  $(".panel").show('slow');

        });
        function closeSPopup(){
            $('#spopup').hide('slow');
        }
    </script>

    <style type="text/css">
    /*menu por resolucion*/
    @media (max-width: 1250px) {
      .navbar-header {/*me mantiene el boton de despliegue flotante a la derecha*/
        float: none;
      }
      .navbar-toggle {/*muestro el boton que despliega el menu*/
        display: block;
      }
      .navbar-collapse.collapse {/*mantiene el despliegue del menu oculto*/
        display: none!important;
      }
      .navbar-nav {/*mantiene el menu a la derecha*/
        float: none!important;
        margin-top: 7.5px;
      }
      .navbar-nav>li {/*me pone el menu en fila vertical*/
        float: none;
      }
      .navbar-nav>li>a {
        padding-top: 5px;
        padding-bottom: 5px;
      }

      .navbar-nav>li>ul {
        padding-right: 50%;
        width: 100%;
        text-align: left;
      }

      .collapse.in{
        display:block !important;
      }
    }
    /*fin menu por resolucion*/

    .panel{/*panel de informacion y manipulacion*/
      /*background-color: #fbf9f9f1;*/
      /*display: none;*/
    }

    .breadcrumb{ /*la barra de direccion*/
      background-color: #e8eaee;
    }
    /*color de fondo a la barra menu */
    .navbar-inverse {
        background-color: #4169e1;
        border-color: #156DFC;
    }

    /*color letra del menú sin seleccionar*/
    .navbar-inverse .navbar-nav > li > a {
      color: #FFFFFF;
    }

    /*cambia color de letra al pasar el cursor por el menú*/
    .navbar-inverse .navbar-nav > li > a:nth-child(odd):hover {
      color: #190707;
    }

    /*color menu sin seleccionar */
    .navbar-inverse .navbar-nav > .open > a,
    .navbar-inverse .navbar-nav > .open > a:hover,
    .navbar-inverse .navbar-nav > .open > a:focus {
      color: #190707;
      background-color: #7194fb;/*menos azul*/
    }

    /*color menu seleccionado */
    .navbar-inverse .navbar-nav > .active > a,
    .navbar-inverse .navbar-nav > .active > a:hover,
    .navbar-inverse .navbar-nav > .active > a:focus {
      color: #190707;
      background-color: #7194fb;/*mas azul*/
    }

    /*Este estilo me permite correr la informacion del borde izquierdo a la derecha*/
    .navbar-fixed-top .navbar-collapse, .navbar-static-top .navbar-collapse, .navbar-fixed-bottom .navbar-collapse {
        padding-right: 1%;
    }

    .navbar-brand {
      margin-left:1em; /*Este estilo me permite correr la informacion del borde derecho a la izquierda*/
      /*float: left;
      height: 50px;*/
      padding: 0px 0px;
      /*font-size: 18px;
      line-height: 20px;*/
    }

    /*estilo de lutux*/
     @font-face{
             font-family: batman;
             font-style: normal;
             font-weight: normal;
             src: url(<?php echo $dir_fuente_batman; ?>);
        }
        /*colorear ubicacion al pasar el cursor en la linea de la tabla*/
                tbody tr:nth-child(odd):hover {
                    background-color: #96dcf6;
                    }

                tbody tr:nth-child(even):hover {
                    background-color: #bee580;
                    }
    </style>
</head>
<!--body oncontextmenu="return false" -->
<body style="background-color: #e9ebee; ">
    <?php $this->beginBody();
    if (!Yii::$app->user->isGuest) : ?>
    <div id="notf" style="display: none;"><i class="fa fa-bell-o" onclick='$("#spopup").show("slow");' style="font-size:50px; cursor:pointer;" ><span class="badge" style="font-size:20px;"><?= $sys_emp->notificaciones_nelux('count') ?></span></i></div>
    <div id="spopup" style="display: none;">
        <i class="fa fa-bell-o" style="font-size:24px"> Notificaciones de <img src="../images/nelux-logo.png" width="20%"></i>
        <a style="position:absolute;top:14px;right:10px;color:#555;font-size:10px;font-weight:bold;" href="javascript:void(0);" onclick="return closeSPopup();">
            <i class="fa fa-close" style="font-size:15px"></i>
        </a>
        <br><br>
      <div style="height: 220px;width: 100%; overflow-y: auto;">
        <?= $sys_emp->notificaciones_nelux('lista') ?>
      </div>
    </div>
    <?php endif ?>
    <div class="wrap">
    <?= AlertBlock::widget([
                'type' => AlertBlock::TYPE_ALERT,
                'useSessionFlash' => true,
                'delay' => 5000
            ]);?>
    <!-- <img src="../images/_logo.jpg"> -->
    <!--div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/site/index"><img src="../images/logo.ico" style="text-align:right" height="50" width="130"></a>
        </div>
        <div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
			<li><a href="#">Home</a></li>
			<li class="dropdown menu-large">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Product Listing <b class="caret"></b> </a>
                  <ul class="dropdown-menu megamenu row">
                    <li>
        <div class="col-sm-6 col-md-3">
          <a href="#" class="thumbnail">
			<img src="http://placehold.it/150x120" />
          </a>
        </div>
        <div class="col-sm-6 col-md-3">
		<a href="#" class="thumbnail">
			<img src="http://placehold.it/150x120" />
          </a>
        </div>
        <div class="col-sm-6 col-md-3">
		<a href="#" class="thumbnail">
			<img src="http://placehold.it/150x120" />
          </a>
        </div>
        <div class="col-sm-6 col-md-3">
         <a href="#" class="thumbnail">
			<img src="http://placehold.it/150x120" />
          </a>
	    </div>
					</li>
                  </ul>
            </li>

			<li class="dropdown menu-large">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories <b class="caret"></b></a>
				<ul class="dropdown-menu megamenu row">
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Glyphicons</li>
							<li><a href="#">Available glyphs</a></li>
							<li class="disabled"><a href="#">How to use</a></li>
							<li><a href="#">Examples</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">Dropdowns</li>
							<li><a href="#">Example</a></li>
							<li><a href="#">Aligninment options</a></li>
							<li><a href="#">Headers</a></li>
							<li><a href="#">Disabled menu items</a></li>
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Button groups</li>
							<li><a href="#">Basic example</a></li>
							<li><a href="#">Button toolbar</a></li>
							<li><a href="#">Sizing</a></li>
							<li><a href="#">Nesting</a></li>
							<li><a href="#">Vertical variation</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">Button dropdowns</li>
							<li><a href="#">Single button dropdowns</a></li>
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Input groups</li>
							<li><a href="#">Basic example</a></li>
							<li><a href="#">Sizing</a></li>
							<li><a href="#">Checkboxes and radio addons</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">Navs</li>
							<li><a href="#">Tabs</a></li>
							<li><a href="#">Pills</a></li>
							<li><a href="#">Justified</a></li>
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Navbar</li>
							<li><a href="#">Default navbar</a></li>
							<li><a href="#">Buttons</a></li>
							<li><a href="#">Text</a></li>
							<li><a href="#">Non-nav links</a></li>
							<li><a href="#">Component alignment</a></li>
							<li><a href="#">Fixed to top</a></li>
							<li><a href="#">Fixed to bottom</a></li>
							<li><a href="#">Static top</a></li>
							<li><a href="#">Inverted navbar</a></li>
						</ul>
					</li>
				</ul>

			</li>
		</ul>
		</div>
      </div>
    </div-->
        <?php
        //Icon::map($this);
        //Icon::map($this, Icon::EL);
           // public $submenuTemplate  = '\n<ul>\n{items}\n</ul>\n';
            $admin = (isset(Yii::$app->user->identity->tipo_usuario) and Yii::$app->user->identity->tipo_usuario == 'Administrador') ? true : false ;
    //http://www.bsourcecode.com/yiiframework2/menu-widget-in-yii-framework-2-0/
    //http://www.yiiframework.com/extension/yii2-icons/#hh1
    // echo '<img src="../images/logo.ico">';
           //Icon::map($this);

            /*Para obtener params de config/main.php*/
        $valueComiones = Yii::$app->params['comisiones'];
        $agComiones = $valueComiones == true ? true: false;
        if (Yii::$app->params['tipo_sistema']=='lutux') {
          $lutux = true;
          $sistema = 'LUTUX';
          $rango_sis = 'FACTURACIÓN & TRÁMITES * CONTROL VEHICULAR * INVENTARIOS';
          $img_sistema = '<img src="../images/LogoAcercaDe.png" height="100%" width="100%">';
        } else {
          $lutux = false;
          $sistema = 'SIFUX';
          $rango_sis = 'FACTURACIÓN & TRÁMITES * MOVIMIENTO CRÉDITO * INVENTARIOS';
          $img_sistema = '<img src="../images/LogoAcercaDe2.png" height="100%" width="100%">';
        }

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => '<span class="fa fa-sign-in fa-lg"></span> Iniciar sesión', 'url' => ['/site/login']];
            } else {
                $menuItems = [
                ['label' => '<span class="glyphicon glyphicon-home"></span>', 'url' => ['/site/index']],
                        ['label' =>'<span class="glyphicon glyphicon-th-list"></span>&nbsp; Acciones',
                                    'items' => [
                                        ['label' => '<span class="glyphicon glyphicon-copy"></span>&nbsp; Ordenes de Servicios pendientes', 'url' => ['/orden-servicio/index'], 'visible' => $lutux],
                                        ['label' => '<span class="glyphicon glyphicon-import"></span>&nbsp; Facturación', 'items' => [
                                            ['label' => '<span class="fa fa-file-text-o"></span>&nbsp; Fact. proforma', 'url' => ['proforma/index']],
                                            ['label' => '<span class="fa fa-clipboard"></span>&nbsp; Pre-Facturas', 'url' => ['/cabeza-prefactura/index']],
                                        ]],
                                        ['label' => '<span class="glyphicon glyphicon-inbox"></span>&nbsp; Caja', 'items' => [
                                            ['label' => '<span class="glyphicon glyphicon-credit-card"></span>&nbsp; Caja', 'url' => ['/cabeza-caja/index']],
                                            ['label' => '<span class="fa fa-shopping-bag"></span>&nbsp; Gastos', 'items' => [
                                              ['label' => 'Categoría de gastos ', 'url' => ['/categoria-gastos/index']],
                                              ['label' => 'Tipos Gastos ', 'url' => ['/tipos-gastos/index']],
                                              ['label' => 'Proveedor de Gastos ', 'url' => ['/proveedores-gastos/index']],
                                              ['label' => 'Registro de Gastos ', 'url' => ['/gastos/index']],

                                            ]],
                                            ['label' => '<span class="fa fa-hourglass-half"></span>&nbsp; Cnt. por cobrar', 'url' => ['/movimiento-credito/index']],
                                            ['label' => '<span class="fa fa-ioxhost"></span>&nbsp; Histor & Devoluc', 'url' => ['/devoluciones-producto/index']],
                                            ['label' => '<span class="fa fa-history"></span>&nbsp; Fact. del día', 'url' => ['/facturas-dia/index']],
                                        ['label' => '<span class="fa fa-user"></span>&nbsp; Agent. Comisiones', 'url' => ['/agentes-comisiones/index'],'visible' => $agComiones],
                                        ]],
                                        ['label' => '<span class="fa fa-file-code-o"></span>&nbsp; Recepción de documentos XML', 'url' => ['/recepcion-hacienda-general/index']],
                                        ['label' => '<span class="glyphicon glyphicon-list-alt"></span>&nbsp; Reportes', 'url' => ['/reporte/index']],
                                       // ['label' => '<span class="glyphicon glyphicon-cog"></span>&nbsp; Configuración General', 'url' => ['#'],'visible' => $admin],
                                    ]],
                        ['label' => '<span class="fa fa-archive"></span>&nbsp; Inventario',
                                    'items' => [
                                        ['label' => '<span class="fa fa-list-alt"></span>&nbsp; Categorías', 'url' => ['/familia/index']],
                                        ['label' => '<span class="fa fa-list-alt"></span>&nbsp; Marcas de productos', 'url' => ['/marcas-producto/index']],
                                        ['label' => '<span class="glyphicon glyphicon-tint"></span>&nbsp; Servicios', 'url' => ['/servicios/index'], 'visible' => $lutux],
                                        ['label' => '<span class="glyphicon glyphicon-barcode"></span>&nbsp; Productos', 'url' => ['/producto-servicios/index']],
                                        ['label' => '<span class="glyphicon glyphicon-refresh"></span>&nbsp; Movimientos',
                                            'items' => [
                                            ['label' => 'Tipos Movimientos', 'url' => ['/tipo-movimientos/index']],
                                            ['label' => 'Mov. a Inventarios', 'url' => ['/movimientos/index']],
                                            ]],
                                    ]],
                        ['label' => '<span class="fa fa-archive"></span>&nbsp; Registro y trámites',
                                    'items' => [
                                        ['label' => '<span class="fa fa-cubes"></span> &nbsp;Proveedores', 'url' => ['/proveedores/index']],
                                        ['label' => '<span class="glyphicon glyphicon-shopping-cart"></span> &nbsp;Compras',
                                            'items' => [
                                              ['label' => 'Ord Compra (Prov)', 'url' => ['/orden-compra-prov/index']],
                                              ['label' => 'Registro compras', 'url' => ['/compras-inventario/index']]
                                              ]],
                                        ['label' => '<span class="fa fa-truck"></span> &nbsp;Transportes', 'url' => ['/transporte/index']],
                                        ['label' => '<span class="fa fa-bank"></span>&nbsp; Bancos',
                                            'items' => [
                                            ['label' => 'Cuentas bancarias', 'url' => ['/cuentas-bancarias/index']],
                                            ['label' => 'Entid financieras', 'url' => ['/cuentas-bancarias/index','entidades']],
                                            ['label' => 'Moneda', 'url' => ['/cuentas-bancarias/index','moneda']],
                                            ['label' => 'Categ tipo Cambio', 'url' => ['/cuentas-bancarias/index','categoria']],
                                            ['label' => 'Tipos Cambio', 'url' => ['/cuentas-bancarias/index','cambio']],
                                            ['label' => 'Tipos Documento', 'url' => ['/cuentas-bancarias/index','documento']],
                                            ['label' => 'Movimiento libros', 'url' => ['/movimiento-libros/index']]
                                            ]],
                                        ['label' => '<span class="fa fa-pied-piper-pp"></span> &nbsp;Trámitar pagos', 'url' => ['/tramite-pago/index']],
                                        ['label' => '<span class="fa fa-hourglass-half"></span>&nbsp; Trámites pendientes',
                                            'items' => [
                                            ['label' => 'Cancel.pendiente', 'url' => ['/tramite-pago-confirmar/index']],
                                            ['label' => 'Confirm.pendiente', 'url' => ['/tramite-pago-confirmar/confirmar']]
                                            ]],

                                        ]],
                        ['label' => '<span class="fa fa-car"></span> Vehículos',
                                    'items' => [
                                        ['label' => '<span class="fa fa-list-alt"></span>&nbsp; Marcas', 'url' => ['/marcas/index']],
                                        ['label' => '<span class="fa fa-list-alt"></span>&nbsp; Modelos', 'url' => ['/modelo/index']],
                                        ['label' => '<span class="fa fa-list-alt"></span>&nbsp; Versiones', 'url' => ['/version/index']],
                                        ['label' => '<span class="fa fa-bus"></span>&nbsp; Vehículos', 'url' => ['/vehiculos/index']],
                                    ], 'visible' => $lutux],
                        ['label' => '<span class="fa fa-users"></span> Usuarios y roles',
                                    'items' => [
                                        ['label' => '<span class="glyphicon glyphicon-briefcase"></span>&nbsp; Funcionarios', 'url' => ['/funcionario/index'],'visible' => $admin],
                                        ['label' => '<span class="glyphicon glyphicon-star"></span>&nbsp; Clientes', 'url' => ['/clientes/index']],
                                        ['label' => '<span class="glyphicon glyphicon-wrench"></span>&nbsp; Mecánicos', 'url' => ['/mecanicos/index'], 'visible' => $lutux],
                                        ['label' => '<span class="glyphicon glyphicon-lock"></span>&nbsp; Derechos de usuarios', 'url' => ['/usuarios-derechos/index'],'visible' => $admin],
                                    ]],

                        '<li>'.Html::a('<i class="glyphicon glyphicon-bookmark"></i> Acerca del sistema', '', [
                                //'class'=>'glyphicon glyphicon-pencil',
                                'id' => 'activity-index-link-acercade',
                                'data-toggle' => 'modal',
                                'onclick'=>'javascript:modalacercade()',
                                'data-target' => '#modalA',
                                //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                'data-pjax' => '0']).'</li>',

                        ['label' => '<span class="glyphicon glyphicon-user"></span>',
                                    'items' => [
                                      '<li>'.Html::a('<i class="glyphicon glyphicon-info-sign"></i> Mi información', '#', [
                                        'id' => 'activity-index-link-acercade',
                                        'data-toggle' => 'modal',
                                        'onclick'=>'javascript:modalinfousuario()',
                                        'data-target' => '#modalB',
                                        //'data-url' => Url::to(['cabeza-prefactura/view', 'id' => $factura['idCabeza_Factura']]),
                                        'data-pjax' => '0']).'</li>',
                                        ['label' => '<span class="glyphicon glyphicon-log-out"></span>&nbsp; Salir [ ' . Yii::$app->user->identity->username . ' ]',
                                        'url' => ['/site/logout'],
                                        'linkOptions' => ['data-method' => 'post']
                                        ],
                                    ]
                                ],
            ];

                /*echo NavX::widget([
                    'options'=>[ 'class' => 'navbar-nav navbar-right'],
                    'encodeLabels' => false,
                    'items' => [
                        // ['label' => '<img src="../images/logo.ico" style="text-align:right" height="50" width="130">', 'url' => ['/site/index']],
                        // '<img src="../images/logo.ico" height="50" width="130">',


                ]]);*/
            }//fin else

            // echo Nav::widget([
            //     'options' => ['class' => 'navbar-nav navbar-right'],
            //     'items' => $menuItems,
            //     'encodeLabels' => false,

            //     //'submenuTemplate' => '\n<ul class="dropdown-menu" role="menu">\n{items}\n</ul>\n',//Submenú
            // ]);
          NavBar::begin([
                'brandLabel' => Yii::$app->params['tipo_sistema']=='lutux' ? '<img src="../images/LogoMenu.png" height="50" width="">' : '<img src="../images/LogoMenu2.png" height="50" width="">',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                //'id' => 'top-menu',//
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
                'innerContainerOptions' => ['class'=>'kv-container'],//
            ]);
                echo NavX::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => $menuItems,
                    'activateParents' => true,
                    'encodeLabels' => false
                ]);
            NavBar::end();
        ?>
         <?php

         Modal::begin([
           'header' => '<h2>&raquo; Acerca de:</h2>',
           'id' => 'modalA', // <-- insert this modal's ID
         ]);
         //echo '<div id="datos_de_hacercade"></div>';
         echo '
                 <center> '.$img_sistema.'<br><br><h2>
                 <font face="batman">'.$sistema.'</font> - Versión 2.0.0 </h2>
                 '.$rango_sis.'<br><br>
                 Se autoriza el uso de este producto a:
                 <pre><strong>'.$empresa->getEmpresa().'<br>Licencia: '.$sys_emp->licencia_sistema().'</strong></pre><br>
                 <img src="../images/nelux-logo.png" height="100%" width="60%"><br>
                 © '. date('Y') .'<strong><a href="http://www.neluxps.com/" target="_blank"> nelux Professional Services</a></strong>. Todos los derechos reservados.
                 </center><br>
                 ';

         Modal::end();
        ?>
        <?php

        Modal::begin([
          'header' => '<h2>&raquo; Información de usuario:</h2>',
          'id' => 'modalB', // <-- insert this modal's ID
        ]);
        if (!Yii::$app->user->isGuest) {
          $funcionario = Funcionario::find()->where(['=','username', Yii::$app->user->identity->username])->one();
          if (@$funcionario) {
            echo '<center>
                        <h1>'.$funcionario->nombre.' '.$funcionario->apellido1.' '.$funcionario->apellido2.'</h1>
                        <strong>Tipo de Funcionario:</strong> '.$funcionario->tipoFuncionario.' ('.$funcionario->tipo_usuario.')<br>
                        <h2>Contacto</h2>
                        <strong>Tel: </strong>'.$funcionario->telefono.' <strong>Correo: </strong>'.$funcionario->email.'
                  </center><br>
                  <p style="text-align: justify;"><strong>Nota:</strong> Si su información está desactualizada o necesita modificar su contraseña de usuario informe al administrador que actualice su información o le indique cómo modificar la contraseña.</p>';
          }
           else { echo "<center><h1>Administrador root Nelux</h1></center>"; }
         }
        Modal::end();
       ?>
        <div class="container-fluid">

            <!--div class="col-lg-12"-->
                <div class="col-lg-1">
                <br><br><br>
                    <?php if (!Yii::$app->user->isGuest) : ?>
                    <!--  http://notas-de-codigo.tumblr.com/post/17893112113/chat-sencillo-jquery-php-mysql  -->
                    <!--div class="panel panel-warning">
                      <div class="panel-heading">
                        <a href="javascript:opcion_acordeon_notificaciones();">
                          Panel de notificaciones
                        </a>
                        <span class="badge">4</span>
                      </div>
                      <div class="panel-body" id="acordeon_notificaciones" style="height: 550px;width: 100%; overflow-y: auto; ">
                        <div class="alert alert-info" role="alert">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <a href="#" class="alert-link">Factura pendiente</a>
                        </div>
                        <div class="alert alert-info" role="alert">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <a href="#" class="alert-link">Pedido pendiente</a>
                        </div>
                        <div class="alert alert-info" role="alert">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <a href="#" class="alert-link">Producto blabla agotado</a>
                        </div>
                        <div class="alert alert-info" role="alert">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <a href="#" class="alert-link">Orden de servicio pendiente</a>
                        </div>
                      </div>
                    </div-->
                <?php endif ?>
                <?php
                ?>
                </div>

                <div class="col-lg-10">
                <br><br><br>
                <?php if (!Yii::$app->user->isGuest) {

                //$empresa_alert = Empresa::find()->where(['idEmpresa'=>1])->one();
                    if ($sys_emp->empresa_alert()=='activo') {
                        echo '
                            <div class="panel panel-default">
                            <div class="panel-body">
                        ';
                    } else if ($sys_emp->empresa_alert()=='alerta') {
                      if (Yii::$app->user->identity->tipo_usuario == 'Administrador') {
                        echo '
                            <div class="alert alert-warning" role="alert">
                             <h4>
                              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                              <span class="sr-only">Licencia por vencer:</span>
                              Estimado administrador, nelux le informa que su producto <font face="batman">'.$sistema.'</font> está a menos de una semana por vencer, renueve su suscripción antes de que se venciese para seguir disfrutando de las garantías de Nelux PS.
                             </h4>
                            </div>
                            <div class="panel panel-default">
                            <div class="panel-body">
                        ';
                      }
                    } else {
                      if (Yii::$app->user->identity->tipo_usuario == 'Administrador') {
                ?>
                <div class="alert alert-danger" role="alert">
                <h4>
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Licencia vencida:</span>
                  Estimado administrador, se le informa que su mensualidad de <font face="batman"><?= $sistema ?></font> venció, renueve su suscripcion para seguir disfrutando las garantias de Nelux PS.
                  </h4>
                </div>
                  <?php } ?>
                <div class="panel panel-default">
                <div class="panel-body">
                <?php } } ?>
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= $content ?>
                <?php if (!Yii::$app->user->isGuest) : ?>
                </div><br><br><br>
                </div>
                <?php endif ?>

                </div>
                <div class="col-lg-1">
                  <br><br><br>

                </div>
            <!--/div-->

            <?php

            if (!Yii::$app->user->isGuest) :
                $usuario_envia = Yii::$app->user->identity->nombre;
                ?>
              <!-- _______CHAT_______ -->
            <!--div class="vFlotante">
              <div style="display:inline-block;" class="">
                  <h1 class="titulo_chat"><span class="label label-primary">Chat </span></h1>&nbsp;
              </div>

              <div class=""  style="display:inline-block;">
                <div style="height: 485px; width: 430px; " >
                  <div class="col-lg-12 panel panel-primary" id="negro_transparente"><br>
                    <p align="center"><strong>Busca el usuario con el que desea chatear</strong></p>

                    <div style="border:2px solid #000; width: 100%; overflow-x: auto;  white-space: nowrap;">
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Ronals" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Marcos" style="cursor:pointer" onclick="usuario_live(2)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Monica" style="cursor:pointer" onclick="usuario_live(3)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Pablo Juares" style="cursor:pointer" onclick="usuario_live(4)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Francisco Martinez" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Cindy" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Johana" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Freddy" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="David" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Raul Moscu" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Heidy Mantos" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Felipe Castro Perez" style="cursor:pointer" onclick="usuario_live(1)"></i>
                        <i class="fa fa-user-circle fa-3x" aria-hidden="true" title="Mariana" style="cursor:pointer" onclick="usuario_live(1)"></i>
                    </div>
                    <div class="col-lg-12"><br>
                        <div id="messages" class="alert alert-success" >
                              <p class="triangle-border left">hola como estas</p>
                              <p class="triangle-border right">hola todo bien</p>
                              <p class="triangle-border left">MAe ocup saber como ve este estilo del chaat, bueno se púede mejorar el diseño pero la forma en como se ve, cómo lo ve??</p>
                              <p class="triangle-border right">di mae se ve pichuo, ahi vemos si se mejora el diseño</p>
                              <p class="triangle-border right">a y falta la funcionalidad</p>
                              <p class="triangle-border left">a bueno si, estoy con eso, bueno por ahora el diseño lo dejo asi</p>
                              <p class="triangle-border right">si no esta tan mal</p>
                              <p class="triangle-border right">bueno pa</p>
                              <p class="triangle-border left">bueno, ahi me desis cuando tenga mas avances del sistema para ir viendo en que aspectos le puedo colaborar, ya sea para QA o para dar algunas ideas</p>
                              <p class="triangle-border right">listo bro, puera vida, ahi estamos en contacto</p>
                        </div>
                    </div>
                    <div class="col-lg-8">
                      <textarea rows="3" name="comunicado" id="comunicado" class="form-control input-md" required></textarea><br>
                    </div>
                    <div class="col-lg-4">
                      <a class="btn btn-primary btn-lg btn-block" data-loading-text="Enviando información...">Enviar</a><br>
                    </div>
                  </div>
                </div>
              </div>
            </div-->
            <!-- FIN CHAT -->

            <div id="floatingRectangle">
            <i class="fa fa-chevron-up fa-3x" aria-hidden="true"></i><br><br>
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="btn-group dropup">
                        <button type="button" id="notiproblem" class="btn btn-warning dropdown-toggle btn-sm" ><span class="glyphicon glyphicon-flag"></span> Deseo notificar un problema...
                        </button>
                        <div id="myForm" class="hide">

                            <form action="" id="popForm" method="get">
                                <div>
                                    <label for="usuario_n">Usuario:</label>
                                    <?php
                                    echo '<input type="text" name="usuario_n" id="usuario_n" value="'.$usuario_envia.'" class="form-control input-md" disabled>';
                                    ?>

                                    <label for="asunto_n">Asunto:</label>
                                    <input type="text" name="asunto_n" id="asunto_n" value="Problema técnico" class="form-control input-md" disabled>
                                    <label for="prioridad_n">Prioridad:</label>
                                    <select name="prioridad_n" id="prioridad_n" class="form-control input-md"><option value="BAJA">Baja</option><option value="NORMAL">Normal</option><option value="ALTA">Alta</option></select>
                                    <label for="about">Mensaje:</label>
                                    <textarea rows="3" name="about" id="about" class="form-control input-md" required></textarea><br>
                                    <center> <a class="btn btn-primary" data-loading-text="Enviando información...">Notificar</a></center>
                                </div>
                            </form>
                        </div>
                        <div id="result"></div>
                    </div>
                    <!--div class="btn-group dropup">
                      <button type="button" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-comment"></span> Notificaciones <span class="badge"> 12 </span></button>
                      <button type="button" class="btn btn-success dropdown-toggle btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div-->
                    &nbsp;
                    <?= $lutux == false ? '' : Html::a('&nbsp;<span class="glyphicon glyphicon-copy"></span>&nbsp;', ['orden-servicio/create'], ['class' => 'btn btn-primary btn-sm', 'data-toggle' => 'titulo', 'type'=>'button', 'id'=>'myButton', 'data-loading-text'=>'Cargando...', 'autocomplete'=>'off', 'title' => 'Nueva Orden de Servicios']) ?>&nbsp;
                    <?= Html::a('&nbsp;<span class="glyphicon glyphicon-erase"></span>&nbsp;', ['proforma/create'], ['class' => 'btn btn-primary btn-sm', 'data-toggle' => 'titulo', 'title' => 'Nueva Proforma']) ?>&nbsp;
                    <?= Html::a('&nbsp;<span class="glyphicon glyphicon-open-file"></span>&nbsp;', ['cabeza-prefactura/create'], ['class' => 'btn btn-primary btn-sm', 'data-toggle' => 'titulo', 'title' => 'Nueva Factura']) ?>&nbsp;
                    <?= Html::a('&nbsp;<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;', ['compras-inventario/create', 'carga'=>false], ['class' => 'btn btn-primary btn-sm', 'data-toggle' => 'titulo', 'title' => 'Nueva Compra']) ?>&nbsp;
                    <?= Html::a('&nbsp;<span class="glyphicon glyphicon-knight"></span>&nbsp;', ['movimientos/create'], ['class' => 'btn btn-primary btn-sm', 'data-toggle' => 'titulo', 'title' => 'Nuevo movimiento'])
                     ?>&nbsp;
                     <!--a href="#" data-toggle="titulo" title="Chat" id="addClass" class= "btn btn-success btn-lg"><span class="fa fa-comments-o"></span></a-->



                </div>
            </div>

          </div>
            <?php endif ?>

        </div>

    </div>







    <?php
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    ?>
    <footer class="footer">

        <div class="container">
        <p class="pull-left">&copy; <strong><a href="http://www.neluxps.com/" target="_blank"><span style="font-size: 12pt;"><strong><span style="color: #9cc44d;">&lt;</span><span style="color: #41739b;">&gt;nel</span><span style="color: #9cc44d;">ux</span><span style="color: #63676E;"> Professional</span><span style="color: #63676E;"> Services</span></strong></span></a></strong>, todos los derechos reservados <?= date('Y') ?></p>
        <p class="pull-right"><?= $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y')//Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
