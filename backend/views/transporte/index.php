<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TransporteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transportes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transporte-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('Crear nuevo transporte', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
     <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
<?php  $dataProvider->pagination->defaultPageSize = 10; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_transporte',
            'nombre_transporte',
            'telefono',
            'direccion',
            'sitio_web',
            // 'contacto',
            // 'email_contacto:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>
