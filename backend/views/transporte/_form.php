<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Transporte */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">
        Ingrese los datos que se le solicitan en el siguiente formulario.
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
              <div class="transporte-form">

                    <?php $form = ActiveForm::begin(); ?>
                    <div class="col-lg-3">
                      <?= $form->field($model, 'nombre_transporte')->textInput(['maxlength' => true]) ?>
                   </div>
                   <div class="col-lg-3">
                      <!--?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?-->
                      <?= $form->field($model, 'telefono')->
                      widget(\yii\widgets\MaskedInput::className(), [
                              'mask' => '9999-99-99',
                      ]) ?>
                    </div>
                    <div class="col-lg-3">
                    
                      <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-3">
                      <?= $form->field($model, 'sitio_web')->textInput(['maxlength' => true]) ?>
                    </div>
                    
                    <div class="col-lg-3">
                      <?= $form->field($model, 'contacto')->textInput(['maxlength' => true]) ?>
                   </div>
                   <div class="col-lg-3">
                      <!--?= $form->field($model, 'email_contacto')->textInput(['maxlength' => true]) ?-->
                      <?= $form->field($model, 'email_contacto')->
                       widget(\yii\widgets\MaskedInput::className(), [
                          'name' => 'input-36',
                          'clientOptions' => [
                          'alias' =>  'email'
                           ],
                      ]) ?>
                    </div>
                    <div class="col-lg-3">
                      <?= $form->field($model, 'estado')->radioList(array('activo'=>'Activo','inactivo'=>'Inactivo')); ?>

                    </div>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                    
                    <?php ActiveForm::end(); ?>

              </div>
    <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
    </div>