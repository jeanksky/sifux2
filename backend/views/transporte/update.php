<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Transporte */

$this->title = 'Actualizar transporte: ' . ' ' . $model->nombre_transporte;
$this->params['breadcrumbs'][] = ['label' => 'Transportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre_transporte, 'url' => ['view', 'id' => $model->id_transporte]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="transporte-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
