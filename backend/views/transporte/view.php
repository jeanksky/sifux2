<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Transporte */

$this->title = $model->nombre_transporte;
$this->params['breadcrumbs'][] = ['label' => 'Transportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transporte-view">
  <div class="col-lg-5">
      <h1><?= Html::encode($this->title) ?></h1>

      <p>
          <?= Html::a('Actualizar', ['update', 'id' => $model->id_transporte], ['class' => 'btn btn-primary']) ?>
          <?= Html::a('Eliminar', ['delete', 'id' => $model->id_transporte], [
              'class' => 'btn btn-danger',
              'data' => [
                  'confirm' => '¿Seguro de eliminar este medio de transporte?',
                  'method' => 'post',
              ],
          ]) ?>
      </p>

      <?= DetailView::widget([
          'model' => $model,
          'attributes' => [
              //'id_transporte',
              'nombre_transporte',
              'telefono',
              'direccion',
              'sitio_web',
              'contacto',
              'email_contacto:email',
          ],
      ]) ?>
  </div>
</div>
