<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\GastosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gastos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idGastos') ?>

    <?= $form->field($model, 'idProveedorGastos') ?>

    <?= $form->field($model, 'idTipoGastos') ?>

    <?= $form->field($model, 'idMedioPagoGastos') ?>

    <?= $form->field($model, 'idBancos') ?>

    <?php // echo $form->field($model, 'idCuentaBancariaLocal') ?>

    <?php // echo $form->field($model, 'numeroDocumentoTransferencias') ?>

    <?php // echo $form->field($model, 'numeroCheque') ?>

    <?php // echo $form->field($model, 'digitosTarjeta') ?>

    <?php // echo $form->field($model, 'numeroAutorizacionTarjeta') ?>

    <?php // echo $form->field($model, 'numeroDocumento') ?>

    <?php // echo $form->field($model, 'fechaDocumento') ?>

    <?php // echo $form->field($model, 'fechaPago') ?>

    <?php // echo $form->field($model, 'detalleGasto') ?>

    <?php // echo $form->field($model, 'fechaRegistro') ?>

    <?php // echo $form->field($model, 'usuarioRegistra') ?>

    <?php // echo $form->field($model, 'fechaAplicada') ?>

    <?php // echo $form->field($model, 'usuarioAplica') ?>

    <?php // echo $form->field($model, 'estadoGasto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
