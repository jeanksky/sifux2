<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\ProveedoresGastos;
use backend\models\TiposGastos;
use backend\models\MedioPagoGastos;
use backend\models\Funcionario;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use backend\models\Moneda;
/* @var $this yii\web\View */
/* @var $model backend\models\Gastos */
//modificacion
$this->title = $model->idGastos;
//$this->params['breadcrumbs'][] = ['label' => 'Gastos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;

//Trae los datos de las tablas ajenas al modulo segun su consulta.
$proveeGasto = ProveedoresGastos::find()->where(['=','idProveedorGastos', $model->idProveedorGastos])->one();
$tipoGasto = TiposGastos::find()->where(['=','idTipoGastos', $model->idTipoGastos])->one();
$medioPagoGasto = MedioPagoGastos::find()->where(['=','idMedioPagoGastos', $model->idMedioPagoGastos])->one();
$EntidadFi =  EntidadesFinancieras::find()->where(['=','idEntidad_financiera',$model->idBancos])->one();
$cuentaBank = CuentasBancarias::find()->where(['=','idBancos',$model->idCuentaBancariaLocal])->one();
$usuarioR = Funcionario::find()->where(['=','idFuncionario',$model->usuarioRegistra ])->one();
$usuarioA = Funcionario::find()->where(['=','idFuncionario',$model->usuarioAplica ])->one();


 //traer los datos moneda local
    $moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
    $moneda_local = '°';
    if (@$moneda) {
     $moneda_local = $moneda->simbolo;
    }


$model->fechaDocumento = $model->isNewRecord ? date("d-m-Y") : $model->fechaDocumento;
?>

<div class="gastos-view">
 <div class="col-lg-3">
    <label>Proveedor:</label>
     <?= Html::input('text', '', $proveeGasto->nombreEmpresa, ['id' => 'medio', 'class' => 'form-control' , 'readonly'=>true]) ?>
</div>

 <div class="col-lg-3">
    <label>Tipo Gasto:</label>
     <?= Html::input('text', '', $tipoGasto->descripcionTipoGastos, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
</div>

 <div class="col-lg-3">
    <label>Medio Pago:</label>
     <?= Html::input('text', '', $medioPagoGasto->descripcionMedioPagoGastos, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
</div>

<?php
 if($medioPagoGasto->idMedioPagoGastos == '2'){//Transferencia
    ?>

    <div class="col-lg-3">
        <label>Banco:</label>
         <?= Html::input('text', '', $EntidadFi->descripcion, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

    <div class="col-lg-3">
        <label>Cuenta Bancaria:</label>
         <?= Html::input('text', '', $cuentaBank->numero_cuenta . ' / ' . $cuentaBank->descripcion, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

     <div class="col-lg-3">
        <label>N° Documento Transferencias:</label>
         <?= Html::input('text', '', $model->numeroDocumentoTransferencias, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>
<?php
 } //fin primer if Transferencia

else if($medioPagoGasto->idMedioPagoGastos == '3'){ // tarjetas
?>
    <div class="col-lg-3">
        <label>Digitos Tarjeta:</label>
         <?= Html::input('text', '', $model->digitosTarjeta, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

      <div class="col-lg-3">
        <label>N° Autorizacion Tarjeta:</label>
         <?= Html::input('text', '', $model->numeroAutorizacionTarjeta, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>
<?php
 }//segundo if
else if($medioPagoGasto->idMedioPagoGastos == '4'){ // cheque
?>

    <div class="col-lg-3">
        <label>Numero Cheque:</label>
         <?= Html::input('text', '', $model->numeroCheque, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

<?php
}//fin del if cheque
?>

     <div class="col-lg-3">
        <label>N° Documento (Factura):</label>
         <?= Html::input('text', '', $model->numeroDocumento, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

    <div class="col-lg-3">
        <label>Monto Gasto:</label>
         <?= Html::input('text', '', $moneda_local.number_format($model->montoGasto,2), ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>
    <div class="col-lg-3">
        <label>Fecha Documento:</label>
         <?= Html::input('text', '', $model->fechaDocumento, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

      <div class="col-lg-3">
        <label>Fecha Pago:</label>
         <?= Html::input('text', '', $model->fechaPago, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>


    <div class="col-lg-6">
        <label>Detalle Gasto:</label>
         <?= Html::input('text', '', $model->detalleGasto, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

     <div class="col-lg-3">
        <label>Fecha Registro:</label>
         <?= Html::input('text', '', $model->fechaRegistro, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

    <div class="col-lg-3">
        <label>Usuario que Registra:</label>
         <?= Html::input('text', '', $usuarioR->nombre .' '. $usuarioR->apellido1 .' '. $usuarioR->apellido2 , ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>


 <div class="col-lg-3">
        <label>Fecha de Aplicación:</label>
         <?= Html::input('text', '', $model->fechaAplicada, ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

    <div class="col-lg-3">
        <label>Usuario que Aplica:</label>
         <?= Html::input('text', '', @$usuarioA->nombre .' '. @$usuarioA->apellido1 .' '. @$usuarioA->apellido2 , ['id' => 'medio', 'class' => 'form-control', 'readonly'=>true]) ?>
    </div>

    <div class="col-lg-12"><hr>
      <center><h2>Resolución de documentos por gastos ante Hacienda</h2></center>
      <?php
      $sql = "SELECT * FROM recepcion_hacienda_gastos WHERE idGastos = :idGastos";
      $command = \Yii::$app->db->createCommand($sql);
      $command->bindValue(":idGastos", $model->idGastos);
      $consulta = $command->queryAll();
      $detalle_notificacion = '';

      foreach ($consulta as $key => $value) {
        $clave_recepcion = Html::a($value['id_factun'], null, [
                      //'class'=>'fa-2x',
                      'style'=>'cursor:pointer;',
                      'data-content' => $value['clave_recepcion'],
                      'data-toggle' => 'popover',
                      'title' => Yii::t('app', 'Clave recepción'),
                    ]);
        $consulta = Html::a('<span class=""></span>', ['gastos/pdf_resolucion', 'id'=>$value['id']], [
                      'class'=>'fa fa-file-pdf-o fa-2x',
                      'id' => 'pdf',
                      'target'=>'_blank',
                      'data-pjax' => '0',
                      'title' => Yii::t('app', 'Mostrar Resolución en PDF'),
                    ]);
        if ( $value['respuesta_hacienda'] == '01' ) {
          $respuesta_hacienda = '<font color="#26DD54">ACEPTADA POR HACIENDA</font>';
        } elseif ( $value['respuesta_hacienda'] == '03' ) {
          $respuesta_hacienda = '<font color="red">RECHAZADA POR HACIENDA</font>';
        } else {
          $respuesta_hacienda = 'ESPERANDO RESPUESTA';
        }
        $detalle_notificacion .= '<tr>

                                    <td>'.date('d-m-Y h:i:s A', strtotime( $value['fecha'] )).'</td>
                                    <td>'.$value['tipo_documento'].'</td>
                                    <td>'.$value['resolucion'].'</td>
                                    <td>'.$value['detalle'].'</td>
                                    <td>'.$respuesta_hacienda.'</td>
                                    <td>'.$consulta.'</td>
                                  </tr>';
      }
      echo '<br>
      <table class="items table table-striped" >
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Tipo documento</th>
            <th>Resolución</th>
            <th>Detalle</th>
            <th>Respuesta de Hacienda</th>
            <th></th>
          <tr>
        </thead>
        <tbody>
        '.$detalle_notificacion.'
        </tbody>
      </table>';
      ?>
    </div>
    <!--h1 --><!--?= Html::encode($this->title) ?--><!--/h1-->
    <!--center><h2>Gasto # <?= Html::encode($this->title) ?></h2></center-->

    <p>
        <!--?= Html::a('Update', ['update', 'id' => $model->idGastos], ['class' => 'btn btn-primary']) ?-->
        <!--?= Html::a('Delete', ['delete', 'id' => $model->idGastos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?-->
    </p>

    <!--?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idGastos',
            'idProveedorGastos',
            'idTipoGastos',
            'idMedioPagoGastos',
            'idBancos',
            'idCuentaBancariaLocal',
            'numeroDocumentoTransferencias',
            'numeroCheque',
            'digitosTarjeta',
            'numeroAutorizacionTarjeta',
            'numeroDocumento',
            'fechaDocumento',
            'fechaPago',
            'detalleGasto',
            'fechaRegistro',
            'usuarioRegistra',
            'fechaAplicada',
            'usuarioAplica',
            'estadoGasto',
        ],
    ]) ?-->

</div>
