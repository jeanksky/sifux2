<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use backend\models\Gastos;
use backend\models\ProveedoresGastos;
use backend\models\TiposGastos;
use backend\models\MedioPagoGastos;
use backend\models\Moneda;
use backend\models\RecepcionHaciendaGastos;
use kartik\widgets\DatePicker;
use kartik\widgets\AlertBlock;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\bootstrap\ActiveForm;
use backend\models\Notificar_compra;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GastosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$jquery_min_js = \Yii::$app->request->BaseUrl.'/js/jquery-2.1.4.min.js';

$this->title = 'Gastos';
$this->params['breadcrumbs'][] = $this->title;
$load_buton = \Yii::$app->request->BaseUrl.'/img/load_buton.gif';
?>


<script type="text/javascript" src="<?php echo $jquery_min_js; ?>"></script>
<script type="text/javascript">

    $(document).ready(function(){
       // $('[data-toggle="modal"]').tooltip();
        $('[data-toggle="toggle"]').popover();
        //re_enviar_xml_not();
        verificarEstadoHAciendoGometa();
        setInterval('verificarEstadoHAciendoGometa()',60000); /*1 min*/
        setTimeout("re_enviar_xml_not()", 8000 );
        setInterval('re_enviar_xml_not()',120000); /*120 segundos*/
    });

    /*vehiel 05-12-2018
    metodo que verificacion del estado del tarro de haciendo con el api de gometa*/
    function verificarEstadoHAciendoGometa(){
      gometa = 'http://apis.gometa.org/status/status.json';
      proxy = 'https://cors-anywhere.herokuapp.com/';
      url = proxy+gometa;
      $.ajax({
        'url': url,
        'dataType':'json',
        'crossDomain':false,
        'type':'get',
        beforeSend: function(xhr){
          xhr.setRequestHeader("Access-Control-Allow-Origin","*");
        },
        success: function(data){
          document.getElementById("estado_h").value = data['api-prod']['status'];
        },
        error:function(){
        }
      });
    }

/*Obtener medio de pago*/
    function obtenMedio(valor)
        {
            compruebatipopago(2);
           // document.getElementById("resultado1").innerHTML=valor;
            $.ajax({
                url:"<?php echo Yii::$app->getUrlManager()->createUrl('gastos/addmedio-u') ?>",
                type:"post",
                data: {'idEntidad_financiera': valor},
                success: function(valores){
                  $('.idCuentaBancariaLocal').html(valores).fadeIn();
                }
            }) ;
        }

        //compruebo que los datos estan llenos a la hora de cancelas de acuerdo al tipo de pago para abilitar o no el boton
        function compruebatipopago(tipo) {

            var entidadFinanc = document.getElementById("idbancos").value;
            var cuenta = document.getElementById("tipoCuentaBancaria").value;
            var comprobacion = document.getElementById("comprobacion").value;

            if (entidadFinanc == '' ||  comprobacion == '' || cuenta == '') {
                $('#boton-actualizar-gasto').addClass('disabled');
            }
            else {
              $.ajax({
                  url:"<?php echo Yii::$app->getUrlManager()->createUrl('gastos/obtener_numero_cuenta') ?>",
                  type:"post",
                  data: { 'idBancos': cuenta },
                  success: function(cuenta_deposito){
                    $.ajax({
                        url:"<?php echo Yii::$app->getUrlManager()->createUrl('cabeza-prefactura/compruebatipopago') ?>",
                        type:"post",
                        data: {'comprobacion': comprobacion, 'cuenta_deposito': cuenta_deposito, 'entidadFinanc': entidadFinanc, 'tipo': tipo},
                        success: function(data){
                            if (data=='repetido') {
                                document.getElementById("nota_repetida").innerHTML = '<span class="label label-danger" style="font-size:15pt;">Comprobación rechasada, ya esta en registro</span>';
                                $('#boton-actualizar-gasto').addClass('disabled');
                            } else {
                                $('#boton-actualizar-gasto').removeClass('disabled');
                                document.getElementById("nota_repetida").innerHTML = '<span class="label label-success" style="font-size:15pt;">Comprobación aceptada</span>';
                            }

                        },
                        error: function(msg, status,err){
                                        alert('error en linea 328, consulte a nelux');
                                    }
                    });
                  }
                });

            }

        }

    $(document).on('click', '#activity-link-create', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        document.getElementById("well-create-gasto").innerHTML = '';
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-create-gasto').html(data);//muestro en el div los datos del create
                        $('#crearGastoModal').modal('show');//muestro la modal
                    }
                );
            }));


    $(document).on('click', '#activity-link-update', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        document.getElementById("well-create-gasto").innerHTML = '';
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-update-gasto').html(data);//muestro en el div los datos del create
                        $('#actualizarGastoModal').modal('show');//muestro la modal
                    }
                );
            }));

       $(document).on('click', '#activity-link-ver', (function() {
                $.get(
                    $(this).data('url'),
                    function (data) {
                        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
                        $('#well-ver-gasto').html(data);//muestro en el div los datos del create
                        $('#verGastoModal').modal('show');//muestro la modal
                    }
                );
            }));


       $(document).ready(function () {
            //me checa todos los Gastos pendientes
            $('#checkAll').on('click', function() {
                if (this.checked == true)
                    $('#tabla_GastosDia').find('input[name="checkboxRow[]"]').prop('checked', true);
                else
                    $('#tabla_GastosDia').find('input[name="checkboxRow[]"]').prop('checked', false);
            });
    });
    function aplicar_gasto_seleccionado() {
      var checkboxValues = "";//
       //var checkboxValues = new Array();//

       $('input[name="checkboxRow[]"]:checked').each(function() {//
           checkboxValues += $(this).val() + ",";//
       });//
       //eliminamos la última coma.
       checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);//
       $.ajax({
               url:"<?php echo Yii::$app->getUrlManager()->createUrl('gastos/aplicar_gasto_seleccionado') ?>",
               type:"post",
               data: { 'checkboxValues' : checkboxValues },
               beforeSend: function() {
                   $('#semitransparente').html('<center><img src="<?php echo $load_buton; ?>" style="height: 32px;"></center>');
                   //$("#movimientos").load(location.href+" #movimientos","");
               },
               success: function(notif){
               },
               error: function(msg, status,err){
                //alert('No pasa 36');
               }
           });

   }

   function agrega(idGastos) {
     $('#idGastos').val(idGastos);
   }

   //muestra el boton enviar en modo espera mientras se envia los datos a hacienda
   function esperar_confirmacion(this_) {
   tipo_documento = $('.tipo_documento_').val();
   resolucion = $('.resolucion_').val();
   detalle = $('.detalle_').val();
   detalle = $('.detalle_').val();
   archivo = $('.archivo_').val();
   if (tipo_documento!='' && resolucion!='' && detalle!='' && archivo!='') {

     $(this_).button('loading');
   }

   }

   //confirma el estado de resolucion en hacienda en segundo plano, o reenvia la resolucion si está estancada
   function re_enviar_xml_not() {
     var estado_hacienda =  document.getElementById("estado_h").value;
     if (estado_hacienda == 'OK') {
       $.ajax({
           url:"<?php echo Yii::$app->getUrlManager()->createUrl('recepcion-hacienda-general/renviar_xml_not') ?>",
           type:"post",
           success: function(data){
             $.ajax({
                 url:"<?php echo Yii::$app->getUrlManager()->createUrl('compras-inventario/renviar_xml_not') ?>",
                 type:"post",
                 success: function(data){
                   $.ajax({
                       url:"<?php echo Yii::$app->getUrlManager()->createUrl('gastos/renviar_xml_not') ?>",
                       type:"post",
                       success: function(data){ },
                   });//fin ajax gastos
                 },
             });//fin ajax compras-inventario
           },
       });//fin ajax recepcion-hacienda-general
     }
   }
</script>
<style>
    #crearGastoModal .modal-dialog{
    width: 80%!important;
    /*margin: 0 auto;*/
    }

    #actualizarGastoModal .modal-dialog{
    width: 80%!important;
    /*margin: 0 auto;*/
    }

    #verGastoModal .modal-dialog{
    width: 80%!important;
    /*margin: 0 auto;*/
    }

    #modal_resolucion_xml .modal-dialog{
    width: 18%!important;
    /*margin: 0 auto;*/
    }
</style>
<div class="gastos-index">
<center><h1><?= Html::encode($this->title) ?></h1></center>
<input type="hidden" id="estado_h" value="NULLO">
    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align:right">
        <?= Html::a('<strong><span class="glyphicon glyphicon-edit"></span> Registrar nuevo Gasto</strong>', '#', [
            'id' => 'activity-link-create',
            'data-toggle' => 'modal',
            'class' => 'btn btn-success',
            //'data-trigger'=>'hover',
            //'data-target' => '#crearmovimientomodal',
            'data-url' => Url::to(['create']),
            'data-pjax' => '0',
            'title' => Yii::t('app', 'Proceda a crear un nuevo gasto'),
        ]) ?>
    </p>

<?= AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true
]);?>

<br>
    <ul class="nav nav-tabs nav-justified">
        <li class="active" role="presentation" ><a href="#GastosDia" data-toggle="tab" id="tramitar_tab"> Gastos del día (Pendientes)</a></li>
        <li   role="presentation"><a href="#HistorialGastos" data-toggle="tab" id="listatramite_tab">Historial de gastos (Aplicados)</a></li>
    </ul>
<div class="table-responsive tab-content"><!-- IMPORTANTE este div me ayuda a que la informacion en los tab mantengan su postura-->
      <!-- ____________________________________________________Gastos dia-->
        <div class="tab-pane fade in active"  id="GastosDia">
            <br>
            <div class="col-lg-12">
                <p style="text-align:right">
                    <a class="btn btn-primary btn-smy" onclick="aplicar_gasto_seleccionado()" ><span class="glyphicon glyphicon-check"></span><strong> Aplicar Gastos</strong></a>
                </p>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-info" id="panel">
                    <div class="table-responsive">
                <div id="semitransparente">
                    <?php

                    $facturaGastos = '';

                    $simbolo_mon_local = '';
                        $monedas = Moneda::find()->all();
                        foreach($monedas as $moneda) {
                            if ($moneda['monedalocal']=='x') {
                                $simbolo_mon_local = $moneda['simbolo'];
                            }
                        }//fin foreach Moneda
                      //llamo la vista con las facturas pendientes de cancelar del proveedor seleccionado
                            //if (@$idproveedor = DebitoSession::getIDproveedor()) {
                             //   echo '<div style="height: 100%;width: 100%; overflow-y: auto; ">';
                               /* echo $this->render('listafacturas', [
                                    'idProveedor' => $idproveedor,
                                   ]);*/
                            //    echo '</div>';
                           // } else
                             //   echo '<center><h3>Esperando un proveedor<h3></center>';

                echo '<table class="items table table-striped" id="tabla_GastosDia"  >';
                    echo '<thead>';
                    printf('<tr>

                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th style="text-align:center"><font face="arial" size=1>%s</font></th>
                        <th  class="actions button-column">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="checkAll" /></th></tr>',
                            'PROVEEDOR',
                            'TIPO GASTO',
                            'MEDIO PAGO',
                            'DOCUMENTO',
                            'MONTO',
                            'FECHA DOCUMENTO',
                            'FECHA PAGO',
                            'DETALLE'
                            //'ESTADO'
                            );
                    echo '</thead>';
                    echo '<tbody class="buscar1">';

                     $facturaGastos = Gastos::find()->where(['=','estadoGasto', 'Pendiente'])->orderBy(['idGastos' => SORT_ASC])->all();

                        foreach($facturaGastos as $position => $facturaG) {
                              $provedorGastos =  ProveedoresGastos::find()->where(['=','idProveedorGastos', $facturaG['idProveedorGastos']])->one();

                              $tipoGastos =  TiposGastos::find()->where(['=','idTipoGastos', $facturaG['idTipoGastos']])->one();

                              $medioPagoGastos =  MedioPagoGastos::find()->where(['=','idMedioPagoGastos', $facturaG['idMedioPagoGastos']])->one();
                              $border = '';

                            $mostrarGasto = Html::a('', '#', [
                                    'class'=>'glyphicon glyphicon-pencil',
                                    'id' => 'activity-link-update',
                                    'data-toggle' => 'modal',
                                    //'data-target' => '#actualizarGastoModal',
                                   // 'data-url' => Url::to(['update']),
                                    'data-url' => Url::to(['update', 'id' => $facturaG['idGastos']]),
                                    'data-pjax' => '0',
                                    'title' => Yii::t('app', 'Procede a actualizar el gasto')]);

                            $eliminarGasto = Html::a('', ['delete', 'id' => $facturaG['idGastos']], [
                                    'class'=>'glyphicon glyphicon-trash',
                                    //'id' => 'activity-link-create',
                                    'data' => [
                                   'confirm' => '¿Seguro que quieres borrar este gasto?',
                                   'method' => 'post'],
                                    'title' => Yii::t('app', 'Procede a eliminar el gasto')]);

                            $idFacturaGasto_activa = $facturaG['idGastos'];
                            $checkbox = '<input id="striped" type="checkbox" name="checkboxRow[]" value="'.$idFacturaGasto_activa.'">';
                    printf('<tr style = "border-left: '.$border.' border-right: '.$border.'" onclick="marcarfactura(this, '.$facturaG['idGastos'].')">
                            <td align="center" style="background: ;"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=1>%s</font></td>
                            <td align="center"><font face="arial" size=1>%s</font></td>
                            <td align="right"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td align="center"><font face="arial" size=2>%s</font></td>
                            <td class="actions button-column">%s</td></tr>',
                                $provedorGastos->nombreEmpresa,
                                $tipoGastos->descripcionTipoGastos,
                                $medioPagoGastos->descripcionMedioPagoGastos,
                                $facturaG['numeroDocumento'],
                                $simbolo_mon_local.number_format($facturaG['montoGasto'],2),
                                $facturaG['fechaDocumento'],
                                $facturaG['fechaPago'],
                                $facturaG['detalleGasto'],
                                $mostrarGasto . '&nbsp;'. $eliminarGasto .'&nbsp;' . $checkbox
                                );
                }
                    echo '</tbody>';
                echo '</table>';

                ?>
            </div>
            </div>
                </div>
            </div>
            <!-- Módulo de movimientos a tramites-->
            <!--div class="col-lg-4">

            </div-->
        </div>

        <!-- ____________________________________________________HistorialGastos-->
        <div class="tab-pane fade" id="HistorialGastos">
            <br>
            <div class="col-lg-12">
                <div class="table-responsive">
                <div id="semitransparente">
                <div class="panel panel-info" id="panel">
                    <div align="right">
            <label>Registro por pantalla: </label>
            <?php echo \nterms\pagesize\PageSize::widget(); ?>
        </div>
        <?php  $dataProvider->pagination->defaultPageSize = 10; ?>
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idGastos',
            //'idProveedorGastos',
            //'idTipoGastos',
             [
                        'attribute' => 'tbl_proveedores_gastos.nombreEmpresa',
                        'label' => 'Proveedor',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $provee = ProveedoresGastos::findOne($model->idProveedorGastos);
                            if (@$provee)
                              return $provee->nombreEmpresa;
                            else {
                              return 'Sin Proveedor';
                            }
                        },
            ],
            //'idMedioPagoGastos',
                [
                        'attribute' => 'tbl_medio_pago_gastos.descripcionMedioPagoGastos',
                        'label' => 'Medio Pago',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $grid) {
                            $MedopPG = MedioPagoGastos::findOne($model->idMedioPagoGastos);
                            if (@$MedopPG)
                              return $MedopPG->descripcionMedioPagoGastos;
                            else {
                              return 'Sin medio de pago';
                            }
                        },
            ],
            //'idBancos',
            // 'idCuentaBancariaLocal',
            // 'numeroDocumentoTransferencias',
            // 'numeroCheque',
            // 'digitosTarjeta',
            // 'numeroAutorizacionTarjeta',
             'detalleGasto',
             'numeroDocumento',
            // 'montoGasto',
               [   //moneda local debito
                'attribute' => 'montoGasto',
                'label' => 'Débito local',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'espacio_monto'
                ],
                'value' => function ($model, $key, $index, $grid) {
                    //if($td = TipoDocumento::findOne($model->idTipodocumento))
                   // {
                        $simbolo_mon_local = '';
                        $monedas = Moneda::find()->all();
                        foreach($monedas as $moneda) {
                            if ($moneda['monedalocal']=='x') {
                                $simbolo_mon_local = $moneda['simbolo'];
                            }
                        }
                        return  $simbolo_mon_local.number_format($model->montoGasto,2);
                    //}
                },
            ],
            // 'fechaDocumento',
            // 'fechaPago',
            // 'fechaRegistro',

             /*[
              'attribute' => 'fechaRegistro',
              'label' => 'Fecha registro',
            ],*/
            [
            'attribute' => 'fechaRegistro',
            'value' => 'fechaRegistro', //Se pone el valor del modelo
            'format' => 'raw',
          //  'options' => ['style' => 'width: 18%;'],
            'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fechaRegistro',
                'options' => ['placeholder' => '', 'readonly'=>true],
                'pluginOptions' => [
                    //'locale'=>['format'=>'Y-m-d'], // Y-m-d / d-m-Y
                   'id' => 'fecha2',
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',  //yyyy-mm-dd  / dd-mm-yyyy
                    'startView' => 'year',
                ]
            ])
        ],
        [
          //'attribute' => 'fechaRegistro',
          'label' => 'Resolución a Hacienda',
          'value' => function ($model, $key, $index, $grid) {
            if($estadoHacienda = RecepcionHaciendaGastos::find()->where(['idGastos'=>$model->idGastos])->andWhere('tipo_documento = "Factura Electrónica"',[])->one())
            {
                if ($estadoHacienda->resolucion == 'Aceptado') {
                    $confirm = '<strong><font color="#26DD54">'.$estadoHacienda->resolucion.'</font></strong>';
                }
                 else if ($estadoHacienda->resolucion == 'Rechazado') {
                    $confirm = '<strong><font color="red">'.$estadoHacienda->resolucion.'</font></strong>';
                }
                 else if ($estadoHacienda->resolucion == 'Parcialmente aceptado') {
                    $confirm = '<strong><font color="#FFBF00">'.$estadoHacienda->resolucion.'</font></strong>';
                }
            }//fin del if
            else{
              $confirm = '';
            }
            return Html::a(' Notificar ', ['#'], [
                    'class'=>'fa fa-file-code-o fa-1x',
                    'id' => 'activity-resolucion',
                    'data-toggle' => 'modal',
                    'onclick'=>'javascript:agrega('.$model->idGastos.')',//asigna id de factura para imprimir
                    'data-target' => '#modal_resolucion_xml',
                    'data-pjax' => '0',
                    'title' => Yii::t('app', 'Notifique este gasto, enviando el xml y su resolución en menos de 8 días despues de la fecha de la factura de compra.')
                    ]).' '.$confirm;
          },
          'format' => 'raw',
        ],

            // 'usuarioRegistra',
            // 'fechaAplicada',
            // 'usuarioAplica',
             //'estadoGasto',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
             'buttons' => [
                'view' => function ($url, $model, $key) {

                        return Html::a('<span class="glyphicon glyphicon-search"></span>', '#', [
                            'id' => 'activity-link-ver',
                            'data-toggle' => 'modal',
                            //'data-trigger'=>'hover',
                            //'data-target' => '#crearmovimientomodal',
                            'data-url' => Url::to(['view', 'id' => $model->idGastos]),
                            'data-pjax' => '0',
                            'title' => Yii::t('app', 'Detalles del gasto'),
                        ]);
                    },
            ],//buttun
            ],// class
        ],// colum
    ]); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!--div align="right"-->
        <!--?= Html::a('Crear Gastos', ['create'], ['class' => 'btn btn-success']) ?-->
    <!--/div-->

    <!--?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idGastos',
            'idProveedorGastos',
            'idTipoGastos',
            'idMedioPagoGastos',
            'idBancos',
            // 'idCuentaBancariaLocal',
            // 'numeroDocumentoTransferencias',
            // 'numeroCheque',
            // 'digitosTarjeta',
            // 'numeroAutorizacionTarjeta',
            // 'numeroDocumento',
            // 'fechaDocumento',
            // 'fechaPago',
            // 'detalleGasto',
            // 'fechaRegistro',
            // 'usuarioRegistra',
            // 'fechaAplicada',
            // 'usuarioAplica',
            // 'estadoGasto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?-->

</div>
<?php
//------------------------modal para crear Gastos
        Modal::begin([
            'id' => 'crearGastoModal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span><strong> Registrar Gasto</strong></h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-create-gasto'></div></div>";

        Modal::end();


        //------------------------modal para crear Gastos
        Modal::begin([
            'id' => 'actualizarGastoModal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span><strong> Actualizar Gasto</strong></h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-update-gasto'></div></div>";

        Modal::end();


        //------------------------modal para Ver Gastos
        Modal::begin([
            'id' => 'verGastoModal',
            'size'=>'modal-lg',
            'header' => '<center><h4 class="modal-title"><span class="glyphicon glyphicon-search"></span><strong> Detalles de Gasto</strong></h4></center>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        echo "<div class='panel-body'><div id='well-ver-gasto'></div></div>";

        Modal::end();

    $form = ActiveForm::begin([
            'action'=>['/gastos/enviar_xml_not'],
            'options'=>['enctype'=>'multipart/form-data']]);
     Modal::begin([//modal que me muestra la factura seleccionada
            'id' => 'modal_resolucion_xml',
            //'size'=>'modal-sm',
            'header' => '<center><h4 class="modal-title">Confirmación de resolución</h4></center>',
            'footer' => '<input type="submit" value="Enviar" class="btn btn-success" data-loading-text="Enviando espere..." onClick="esperar_confirmacion(this)"> <a href="#" class="btn btn-primary" data-dismiss="modal">Salir</a>',
        ]);
        $model_compras = new Notificar_compra();
        echo "<input type='hidden' name='idGastos' id='idGastos'></input>";
        echo $form->field($model_compras, 'tipo_documento')->widget(Select2::classname(), [
        'data' => [
          "Factura Electrónica" => "Factura Electrónica",
          "Nota de débito electronica" => "Nota de débito electronica",
          "Nota de crédito electronica" => "Nota de crédito electronica"
        ],
        'language'=>'es',
        'options' => ['class'=>'tipo_documento_' ,'placeholder' => '- Seleccione -'],
        'pluginOptions' => [
            'initialize'=> true,'allowClear' => true
        ],
        ])->label('Tipo de documento:');
        echo $form->field($model_compras, 'resolucion')->widget(Select2::classname(), [
        'data' => [1 => "Aceptado", 2 => "Parcialmente aceptado", 3 => "Rechazado"],
        'language'=>'es',
        'options' => ['class'=>'resolucion_' ,'placeholder' => '- Seleccione -'],
        'pluginOptions' => [
            'initialize'=> true,'allowClear' => true
        ],
        ])->label('Resolución:');

        echo $form->field($model_compras, 'detalle')->textInput(['class'=>'detalle_ form-control','maxlength' => true])->label('Detalle:');
        echo $form->field($model_compras, 'archivo')->widget(FileInput::classname(), [
            'options' => [  'class'=>'archivo_' ],
            'pluginOptions' => [
            'allowedFileExtensions'=>['xml'],
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => false,
            'showUpload' => false
        ]])->label('Archivo XML que recibio por la compra:');
        Modal::end();
 ActiveForm::end(); ?>
