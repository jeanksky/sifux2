<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\ProveedoresGastos;
use backend\models\TiposGastos;
use backend\models\MedioPagoGastos;
use backend\models\Funcionario;
use backend\models\EntidadesFinancieras;
use backend\models\CuentasBancarias;
use kartik\widgets\Select2;
use backend\models\Moneda;
//use kartik\money\MaskMoney;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\Gastos */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
    $model->fechaDocumento = $model->isNewRecord ? date("d-m-Y") : $model->fechaDocumento;
    $model->fechaRegistro = $model->isNewRecord ? date("d-m-Y") : $model->fechaRegistro;
    $model->fechaPago = $model->isNewRecord ? date("d-m-Y") : $model->fechaPago;
    $model->usuarioRegistra = '';
    $funcionario = Funcionario::find()->where(['username'=>Yii::$app->user->identity->username])->one();
    if ($funcionario) {
        $model->usuarioRegistra = $funcionario->nombre . ' ' . $funcionario->apellido1 . ' ' . $funcionario->apellido2;
    } else {
        $model->usuarioRegistra = Yii::$app->user->identity->username;
    }

    //traer los datos moneda local
    $moneda = Moneda::find()->where(['monedalocal'=>'x'])->one();
    $moneda_local = '°';
    if (@$moneda) {
     $moneda_local = $moneda->simbolo;
    }

    /* Verificacion para lo que es el metodo de pago */
    $comprovacion = ''; // variable que va a comprobar si existe el numero de documentos en libros
    $cuenta = ''; // mostrara las cuentas relacionadas al banco que se tenga
    $data2 = ArrayHelper::map(CuentasBancarias::find()->all(), 'numero_cuenta', 'numero_cuenta');

?>
<script type="text/javascript">

        //espacio que solo permite numero y decimal (.)
    function isNumberDe(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || key==46 || (key >= 48 && key <= 57));
    }

        $(document).ready(function() {
        var tipoMedioPago = "<?= $model->idMedioPagoGastos; ?>";
        mostrarMedioPago(tipoMedioPago); //Le enviamos a la funcion el valor del medio pago para que lo muestre en la modal cuando sea update
    });

    function mostrarMedioPago(tipoMedioPago) {
       //var tipoMedioPago = $('.iMedioPagoGastos').value();
       if (tipoMedioPago == '2')
                  {
                    $('#contenido_a_mostrarTransferencia').show("slow"); //hide
                    $('#contenido_a_mostrarTarjetas').hide("slow"); //hide show
                    $('#contenido_a_mostrarCheque').hide("slow"); //hide show
                    //$('#fecVe').prop('readonly', true);
                    //$('#fecVe').val('');
                  }
            else if (tipoMedioPago == '3')
            {
                $('#contenido_a_mostrarTransferencia').hide("slow"); //hide show
                $('#contenido_a_mostrarTarjetas').show("slow");
                $('#contenido_a_mostrarCheque').hide("slow"); //hide show
                //$('#fecVe').prop('readonly', true);
                //$('#fecVe').val('00-00-0000');
            }
            else if (tipoMedioPago == '4'){
                $('#contenido_a_mostrarTransferencia').hide("slow"); //hide show
                $('#contenido_a_mostrarTarjetas').hide("slow");
                $('#contenido_a_mostrarCheque').show("slow"); //hide show
            }
            else{ // si no se selecciona nada elimina todos los campos
                    $('#contenido_a_mostrarTransferencia').hide("slow"); //hide
                    $('#contenido_a_mostrarTarjetas').hide("slow"); //hide show
                    $('#contenido_a_mostrarCheque').hide("slow"); //hide show
            }
       //alert(tipoMedioPago);
    }//fin de la funcion mostrarMedioPago

    //calculadora me devuelve el monto en moneda local multiplicando por la tasa de cambio
    function activar_desactivar_btn(){
        var saldo_bancos = document.getElementById("saldo_bancos").value;

        $('#saldo_libros').val(saldo_bancos);

        if (saldo_bancos!='') {
            document.getElementById('btn_ac').disabled=false;
        } else document.getElementById('btn_ac').disabled=true;

    }
</script>
<div class="gastos-form">
  <?php $form = ActiveForm::begin([
         'id' => 'gastosForm',
         /*'enableAjaxValidation' => true,
         'enableClientScript' => true,
         'enableClientValidation' => true,*/
     ]); ?>
 <div class="col-lg-3">
    <!--?= $form->field($model, 'idProveedorGastos')->textInput() ?-->
    <?= $form->field($model, 'idProveedorGastos')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($model->getProveedorGastos(),'idProveedorGastos', function($element){
                                        return $element->nombreEmpresa;
                                    }),
                                'language'=>'es',
                                'options' => ['placeholder' => '- Seleccione -'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]);
                                ?>
</div>
 <div class="col-lg-3">
    <!--?= $form->field($model, 'idTipoGastos')->textInput() ?-->
    <?= $form->field($model, 'idTipoGastos')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($model->getTioGastos(),'idTipoGastos', function($element){
                                        return $element->descripcionTipoGastos;
                                    }),
                                'language'=>'es',
                                'options' => ['placeholder' => '- Seleccione -'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]);
                                ?>
</div>
 <div class="col-lg-3">
    <div id="Mpago">
    <!--?= $form->field($model, 'idMedioPagoGastos')->textInput() ?-->
    <?= $form->field($model, 'idMedioPagoGastos')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($model->getMedioPagoGastos(),'idMedioPagoGastos', function($element){
                                        return $element->descripcionMedioPagoGastos;
                                    }),
                                'language'=>'es',
                                'options' => [
                                    //'class'=>'iMedioPagoGastos',
                                    'placeholder' => '- Seleccione -',
                                    'onchange' =>'mostrarMedioPago(this.value)',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]);
                                ?>
         </div>

</div>
<div id="contenido_a_mostrarTransferencia">
         <div class="col-lg-3">
            <!--?= $form->field($model, 'idBancos')->textInput() ?-->
              <?= $form->field($model, 'idBancos')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($model->getEntidadFinanciera(),'idEntidad_financiera', function($element){
                                        return $element->descripcion;
                                    }),
                                'language'=>'es',
                                'options' => ['id'=>'idbancos', 'placeholder' => '- Seleccione -','onchange'=>'obtenMedio(this.value)'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]);
                                ?>
         </div>
          <div class="col-lg-3">
            <!--?= $form->field($model, 'idCuentaBancariaLocal')->textInput() ?-->
             <?= $form->field($model, 'idCuentaBancariaLocal')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($model->getCuentaBancaria(),'idBancos', function($element){
                                        //return $element->numero_cuenta;
                                    return $element['numero_cuenta'].' '.$element['descripcion'];
                                    }),
                                'language'=>'es',
                                'options' => ['class'=>'idCuentaBancariaLocal', 'id'=>'tipoCuentaBancaria', 'placeholder' => '- Seleccione -','onchange'=>'compruebatipopago(2)'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]);
                                ?>
        </div>
         <div class="col-lg-3">
            <?= $form->field($model, 'numeroDocumentoTransferencias')->textInput(['id' => 'comprobacion', 'disabled'=>false, 'maxlength' => true, 'onkeyup' => 'compruebatipopago(2)']) ?>
        </div>
 </div>
 <div id="contenido_a_mostrarCheque">
     <div class="col-lg-3">
        <?= $form->field($model, 'numeroCheque')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div id="contenido_a_mostrarTarjetas">
         <div class="col-lg-3">
            <?= $form->field($model, 'digitosTarjeta')->textInput() ?>
        </div>
         <div class="col-lg-3">
            <?= $form->field($model, 'numeroAutorizacionTarjeta')->textInput(['maxlength' => true]) ?>
        </div>
</div>
 <div class="col-lg-3">
    <?= $form->field($model, 'numeroDocumento')->textInput(['maxlength' => true]) ?>
</div>
  <div class="col-lg-2">
    <?= $form->field($model, 'subtotal')->widget(\yii\widgets\MaskedInput::className(), [
              'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
              'clientOptions' => [
                  'alias' =>  'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true
              ],
          ]); ?>
  </div>
  <div class="col-lg-2">
    <?= $form->field($model, 'descuento')->widget(\yii\widgets\MaskedInput::className(), [
              'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
              'clientOptions' => [
                  'alias' =>  'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true
              ],
          ]); ?>
  </div>
  <div class="col-lg-2">
    <?= $form->field($model, 'impuesto')->widget(\yii\widgets\MaskedInput::className(), [
              'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
              'clientOptions' => [
                  'alias' =>  'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true
              ],
          ]); ?>
  </div>
  <div class="col-lg-2">
    <!--?= $form->field($model, 'montoGasto')->textInput(['maxlength' => true]) ?-->
    <!--?= $form->field($model, 'montoGasto')->widget(MaskMoney::classname(), [
                                                        'pluginOptions' => [
                                                            'prefix' => '¢',
                                                            'thousandSeparator' => ',',
                                                            'decimalSeparator' => '.',
                                                             'precision' => 2,
                                                             'allowZero' => false,
                                                            'allowNegative' => false,
                                                        ]
                                                    ]); ?-->

            <?= $form->field($model, 'montoGasto')->widget(\yii\widgets\MaskedInput::className(), [
                      //'name' => 'input-33',
                      'options' => ['class'=>'form-control', "onkeypress"=>"return isNumberDe(event)"],
                      'clientOptions' => [
                        //'prefix' => $moneda_local,
                          'alias' =>  'decimal',
                          'groupSeparator' => ',',
                         // 'options' => ['id'=>'monto' ,'onkeyup'=>'javascript:obtenermontomonedalocal()', "onkeypress"=>"return isNumberDe(event)"],
                          'autoGroup' => true
                      ],
                  ]); ?>
</div>

 <div class="col-lg-2">
    <!--?= $form->field($model, 'fechaDocumento')->textInput() ?-->
    <?= $form->field($model,'fechaDocumento')->widget(DatePicker::className(),[
                                                               'name' => 'check_issue_date',
                                                                'disabled' => false,
                                                                'pluginOptions' => [
                                                                    'format' => 'dd-m-yyyy',
                                                                    'todayHighlight' => true,
                                                                    'autoclose' => true,
                                                                ]
                                                            ]) ?>
</div>
 <div class="col-lg-2">
    <!--?= $form->field($model, 'fechaPago')->textInput() ?-->
    <?= $form->field($model,'fechaPago')->widget(DatePicker::className(),[
                                                               'name' => 'check_issue_date',
                                                                'disabled' => false,
                                                                'pluginOptions' => [
                                                                    'format' => 'dd-m-yyyy',
                                                                    'todayHighlight' => true,
                                                                    'autoclose' => true,
                                                                ]
                                                            ]) ?>
</div>
 <div class="col-lg-8">
    <?= $form->field($model, 'detalleGasto')->textarea(['maxlength' => true]) ?>
    <br><div id='nota_repetida'></div>
</div>
 <div class="col-lg-3">
    <?= $form->field($model, 'fechaRegistro')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
</div>
 <div class="col-lg-3">
    <?= $form->field($model, 'usuarioRegistra')->textInput(['readonly'=>true,'maxlength' => true]) ?>
</div>

 <!--div class="col-lg-3">
    <?= $form->field($model, 'fechaAplicada')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
</div>
 <div class="col-lg-3">
    <?= $form->field($model, 'usuarioAplica')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
</div>
 <div class="col-lg-3">
    <?= $form->field($model, 'estadoGasto')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
</div-->
<div class="col-lg-11">
    <div class="form-group">
        <p style="text-align:right">
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['id'=>'boton-actualizar-gasto', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </p>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
