<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Gastos */

//$this->title = 'Update Gastos: ' . ' ' . $model->idGastos;
$this->params['breadcrumbs'][] = ['label' => 'Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idGastos, 'url' => ['view', 'id' => $model->idGastos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gastos-update">

    <h1><!--?= Html::encode($this->title) ?--></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
