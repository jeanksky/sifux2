//--------------------------------------------------------------------------------------------
//---------------LAS SIGUIENTES FUNCIONES SON PARA CREAR LA NUEVA FACTURA---------------------

//--------------------------------------------------------------------------------------------
//---------------LAS SIGUIENTES FUNCIONES SON PARA EL MODO FACTURACIÓN------------------------
//para mostrar la modal de facturación
function modal_facturar(url_condicion_venta, url_create) {//llamo la mdal de facturacion
  $.get( url_condicion_venta,//{ 'id' : id },
    function( data ) {
      if (data == 'incompleto') {
        alert('Se encuentran datos incompletos');
        window.location.href = url_create;
      } else {
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
        $('#condicion_venta').html(data);
        $('#modal_facturar').modal('show');//muestro la modal de facturacion
        setTimeout(function() {//aparece el autofocus en pago luego de un segundo
          $('#autofocus').slideDown(function(){
              $('#pago').focus();
          });
        },500);
      }
    });
}

//funcion para optener el vuelto por elinput pago
function obtenerPago(key,valor,url_obtener_pago, url_agregar_monto_pago, url_condicion_venta, load_buton){
  $bandera = true;
  if(key == 13) {
    if (valor=='') {//ejecutamos la cancelación
      document.getElementById("boton-cancelar").click();
      $bandera = false;
    } else {
      agregar_monto_pago(1, url_agregar_monto_pago, url_condicion_venta, load_buton);
    }
  }
  if ($bandera == true) {
    //document.getElementById("vuelto").innerHTML = '₡'+parseFloat(valor - total).toFixed(2);
    var dollar = 'no';
    $('.checkdolar').each( //con esto compruebo si el valor es dolar
      function() {
      if ($(this).is(':checked'))//Este if me dice que si esta marcado es porque el valor ingresado es dolar
        {
          dollar = 'si';
        }
    });
    $.ajax({
        url: url_obtener_pago,
        type:"post",
        dataType: 'json',
        data: {'valor': valor, 'dollar' : dollar},
        success: function(data){
          document.getElementById("vuelto").innerHTML = data.resultado_simbolo;
          document.getElementById("valorencolones").innerHTML = data.valorencolones;
          $('#vueltoinput').val(data.resultado);
          $('#pago_real').val(data.pago_real);
        }
    }) ;
  }
}

//agreagamos el monto pago
function agregar_monto_pago(tipo_pago, url_agregar_monto_pago, url_condicion_venta, load_buton) {
  var vueltoinput = document.getElementById("vueltoinput").value;
  var pago = document.getElementById("pago");
  var pago_real = document.getElementById("pago_real");
  if (pago.value=='') {
    pago.style.border = "3px solid red";
  }else {
    ddl_entidad = ''; tipoCuentaBancaria = '0';  comprobacion = ''; fechadepo = '';
    if (tipo_pago==4) {
      ddl_entidad = document.getElementById("ddl-entidad").value;
      tipoCuentaBancaria = document.getElementById("tipoCuentaBancaria").value;
      comprobacion = document.getElementById("comprobacion").value;
      fechadepo = document.getElementById("fechadepo").value;
    } else if (tipo_pago==2) {
      ddl_entidad = document.getElementById("medio_tarjeta").value;
      comprobacion = document.getElementById("comprobacion_tarjeta").value;
    } else if (tipo_pago==3) {
      ddl_entidad = document.getElementById("ddl-entidad-cheque").value;
      comprobacion = document.getElementById("comprobacion_cheque").value;
    }

  $.ajax({
        url: url_agregar_monto_pago,
        type:"post",
        data: {
          'pago': pago_real.value, 'tipo_pago': tipo_pago, 'vueltoinput' : vueltoinput,
          'ddl_entidad' : ddl_entidad, 'tipoCuentaBancaria' : tipoCuentaBancaria, 'fechadepo' : fechadepo, 'comprobacion' : comprobacion
        },
        beforeSend: function() {
          $('#vuelto_final').html('<center><img src="'+load_buton+'" style="height: 32px;"></center>');
          $("#pago").attr('disabled','disabled');
          //$( "#notifica_apartados" ).html( '<span style="font-size:10pt;" class="label label-info"> Espere un momento mientras se realiza el proceso... </span>' );
        },
        success: function(data_pago){
          if (data_pago==false) {
            alert()
          }
            $.get( url_condicion_venta,//{ 'id' : idCa },
            function( data ) {
                $.fn.modal.Constructor.prototype.enforceFocus = function() {};
                $('#condicion_venta').html(data);
                $('#vueltoinput').val(vueltoinput);
            });

        },
        error: function(msg, status,err){
          alert(err);
        }
    });
  }//fin else comprovacion de que campo monto pago esté lleno
}

//eliminar medio pago
function delete_medio_pago(id, url_delete_medio_pago, url_condicion_venta, load_buton) {
  $('#vuelto_final').html('<center><img src="'+load_buton+'" style="height: 32px;"></center>');
  $.get( url_delete_medio_pago,{ 'id' : id },
  function( data ) {
    $.get( url_condicion_venta,//{ 'id' : idCa },
    function( data ) {
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        $('#condicion_venta').html(data);
    });
  });
}

//compruebo que los datos estan llenos a la hora de cancelas de acuerdo al tipo de pago para abilitar o no el boton
function compruebatipopago(tipo, url_compruebatipopago) {
    var entidadFinanc = '';
    var cuenta = '0';
    var comprobacion = '';
    //agregar medio pago
    if (tipo==4) {
      entidadFinanc = document.getElementById("ddl-entidad").value;
      cuenta = document.getElementById("tipoCuentaBancaria").value;
      comprobacion = document.getElementById("comprobacion").value;
    } else if (tipo==2) {
      entidadFinanc = document.getElementById("medio_tarjeta").value;
      comprobacion = document.getElementById("comprobacion_tarjeta").value;
    } else if (tipo==3) {
      entidadFinanc = document.getElementById("ddl-entidad-cheque").value;
      comprobacion = document.getElementById("comprobacion_cheque").value;
    }

    if (entidadFinanc == '' ||  comprobacion == '' || cuenta == '') {
        if (tipo==4) $('#boton-agregar-deposito').addClass('disabled');
        else if (tipo==2) $('#boton-agregar-tarjeta').addClass('disabled');
        else if (tipo==3) $('#boton-agregar-cheque').addClass('disabled');
    }
    else {
        $.ajax({
            url: url_compruebatipopago,
            type:"post",
            data: {'comprobacion': comprobacion, 'cuenta_deposito': cuenta, 'entidadFinanc': entidadFinanc, 'tipo': tipo},
            success: function(data){
                if (data=='repetido') {
                  if (tipo==4) {
                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación rechasada, ya esta en registro';
                    $('#boton-agregar-deposito').addClass('disabled');
                  } else if (tipo==2) {
                    document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación rechasada, ya esta en registro';
                    $('#boton-agregar-tarjeta').addClass('disabled');
                  } else if (tipo==3) {
                    document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación rechasada, ya esta en registro';
                    $('#boton-agregar-cheque').addClass('disabled');
                  }
                } else {
                  if (tipo==4) {
                    $('#boton-agregar-deposito').removeClass('disabled');
                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación aceptada';
                  } else if (tipo==2) {
                    $('#boton-agregar-tarjeta').removeClass('disabled');
                    document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación aceptada';
                  } else if (tipo==3) {
                    $('#boton-agregar-cheque').removeClass('disabled');
                    document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación aceptada';
                  }
                }

            },
            error: function(msg, status,err){
                            alert('error en linea 328, consulte a nelux');
                        }
        });
        //$('#boton-abonar-deposito').removeClass('disabled');
    }

}


function cancelar_factura(this_boton, url_completando_facturacion, url_factura_electronica, url_create) {
  document.getElementById("alerta_cancelar").innerHTML = '';
  if (navigator.onLine) {
    vueltoinput = document.getElementById("vueltoinput").value;
    //convertir vuelto a decimal y con separadores de miles
    var num2 = formatNumber.new(parseFloat(Math.round(vueltoinput * 100) / 100).toFixed(2));//(""+vueltoinput).replace(/(\d+)(\d{3})(\d{3})$/ ,"$1 $2 $3").replace(/(\d+)(\d{3})$/   ,"$1 $2" ).replace(/(\d+)(\d{3})(\d{3})\./ ,"$1 $2 $3.") .replace(/(\d+)(\d{3})\./  ,"$1 $2." );
    //fin conversion vuelto a decimal
    document.getElementById("vuelto_final").innerHTML = '<small>Vuelto:</small> '+ num2;
    var $btn = $(this_boton).button('loading'); //ponemos en suspencion el boton de cancelar
    $("#pago").attr('disabled','disabled');
    try {
      $.get( url_completando_facturacion,//Guardo en BD la facturo y la pongo en estado de caja
      function( data ) {//"data" me trae el id de la factura que se guardó
        if (data > 0) {//si data es mayor a 0 es porque si se creó la factura y existe un id correspondido
          $.ajax({//esto significa que se debe proceder a la facturación
            'type': "post",//enviamos a factun para la firma electronica
            'url': url_factura_electronica,//aqui se encuentra el controlador donde se ejecuta la FE
            'dataType': 'json',
            data: {
              'idCa' : data,
              'vueltoinput' : vueltoinput,
            },
            beforeSend: function(xhr) {
              document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-info" style="font-size:12pt;">[ Creando Factura/Tiquete electrónico (XML) ]</span><br><br><span class="label label-default" style="font-size:10pt;">Espere, facturación en proceso...</span></center><br>';
            },
            success: function(data_factura){//devuelve en json los datos de respuesta de factun
              //console.log(data_factura.json_result_facturacion);//si
              //console.log(data_factura.detalle);
              //console.log(data_factura.mensajes_error);//si
              var obj = jQuery.parseJSON( data_factura.json_result_facturacion );//creamos un objeto con los datos de factun
              if (obj.data.clave) {//si existe la clave, entonces imprima
                //var ticket = "<!--?= Url::toRoute('cabeza-caja/ticket')?-->&id="+idCa;
                //window.open(ticket,"_blank");
                //var orden = "<!--?= Url::toRoute('cabeza-caja/orden')?-->&id="+idCa;
                //window.open(orden,"_blank");
                document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-success" style="font-size:15pt;">[ Documento electrónico creado y firmado ]</span><br><br><span class="label label-default" style="font-size:10pt;">Espere que se recargue la página...</span></center><br>';
                izq = (screen.width-450)/2;
                sup= (screen.height-450)/2;
                var ventimp=window.open('#',"MsgWindow", "menubars=no,toolbars=no,width=600,height=300,left=" + izq + ",top=" + sup);//'MsgWindow'
                ventimp.document.write(data_factura.resultado_vuelto);
                window.location.href = url_create+"&id="+data;
              } else { //de lo contrario muestre el error
                document.getElementById("alerta_cancelar").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+obj.mensajes.mensaje+':</strong> '+obj.mensajes.detalle_mensaje+'.</center>'+data_factura.mensajes_error+'<br><br><strong>Luego de solucionar el problema diríjase al módulo CAJA para re-intentar facturar.</strong></div>';
                $btn.button('reset');
                $('#boton-cancelar').hide();
              }
            },
          });
        }
      });
    } catch(err) {
      document.getElementById("alerta_cancelar").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+err.message+':</strong></center></div>';
    }
  } else { //si no hay internet que me muestre esta alerta
    document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-warning" style="font-size:15pt;">Alerta! No se encuentra una red de internet</span></center><br>';
    setTimeout(function() {//aparece la alerta en un milisegundo
        $("#alerta_cancelar").fadeIn();
    });
    setTimeout(function() {//se oculta la alerta luego de 4 segundos
        $("#alerta_cancelar").fadeOut();
    },5000);
  }
}

function finalizar_credito(this_boton, url_completando_facturacion, url_factura_electronica, url_create) {
  document.getElementById("alerta_cancelar").innerHTML = '';
  if (navigator.onLine) {
    var $btn = $(this_boton).button('loading');
    $.get( url_completando_facturacion,//{ 'id' : idCa },
    function( data ) {
      if (data > 0) {
        try {
          $.ajax({
            url: url_factura_electronica,
            type:"post",
            dataType: 'json',
            data: {'idCa' : data, 'vueltoinput': 0},
            beforeSend: function() {
              document.getElementById("alerta_cancelar").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Acreditando y firmando factura, Espere...</span></center><br>';
            },
            success: function(data_factura){
              console.log(data_factura.json_result_facturacion);
              //console.log(data_factura.detalle);
              console.log(data_factura.mensajes_error);
              var obj = jQuery.parseJSON( data_factura.json_result_facturacion );//creamos un objeto con los datos de factun
              if (obj.data.clave) {//si existe la clave se imprime
                document.getElementById("alerta_cancelar").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Factura firmada y acreditada, espere que se recargue la pantalla...</span></center><br>';
                //var orden = "<!--?= Url::toRoute('cabeza-caja/orden')?-->&id="+idCa;
                //window.open(orden,"_blank");
                izq = (screen.width-450)/2;
                sup= (screen.height-450)/2;
                var ventimp=window.open('#',"MsgWindow", "menubars=no,toolbars=no,width=600,height=150,left=" + izq + ",top=" + sup);//'MsgWindow'
                ventimp.document.write(data_factura.resultado_vuelto);
                window.location.href = url_create+"&id="+data;
              } else {
                document.getElementById("alerta_cancelar").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+obj.mensajes.mensaje+':</strong> '+obj.mensajes.detalle_mensaje+'.</center>'+data_factura.mensajes_error+'<br><br><strong>Luego de solucionar el problema diríjase al módulo CAJA para re-intentar facturar.</strong></div>';
                $btn.button('reset');
                $('#emitir_credito_btn').hide();
                document.getElementById("notificaciones").innerHTML = '';
              }
            },
            error: function(msg, status,err){
                          //  alert('error en linea 204, consulte a nelux');
                        }
          });
        } catch(err) {
          document.getElementById("alerta_cancelar").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+err.message+':</strong></center></div>';
        }
      }
    });
  } else { //si no hay internet que me muestre esta alerta
    document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-warning" style="font-size:15pt;">Alerta! No se encuentra una red de internet</span></center><br>';
    setTimeout(function() {//aparece la alerta en un milisegundo
        $("#alerta_cancelar").fadeIn();
    });
    setTimeout(function() {//se oculta la alerta luego de 4 segundos
        $("#alerta_cancelar").fadeOut();
    },5000);
  }
}



//para formatear el numero
var formatNumber = {
 separador: ",", // separador para los miles
 sepDecimal: '.', // separador para los decimales
 formatear:function (num){
 num +='';
 var splitStr = num.split(',');
 var splitLeft = splitStr[0];
 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
 var regx = /(\d+)(\d{3})/;
 while (regx.test(splitLeft)) {
 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
 }
 return this.simbol + splitLeft +splitRight;
 },
 new:function(num, simbol){
 this.simbol = simbol ||'';
 return this.formatear(num);
 }
}
