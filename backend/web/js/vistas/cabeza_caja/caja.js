//--------------------------------------------------------------------------------------------
//---------------LAS SIGUIENTES FUNCIONES SON PARA EL MODO FACTURACIÓN------------------------
//muestro el facturador de la factura seleccionada
function seleccionar_factura(x, id, condicion_venta, load) {
var elementos = document.getElementById('tabla_prefacturas_caja').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
// Por cada TR empezando por el segundo, ponemos fondo blanco.
for (var i = 0; i < elementos.length; i++) {
    elementos[i].style.background='white';
}
x.style.background="yellow";// Al elemento clickeado le ponemos fondo amarillo.
$('#condicion_venta').html('<br><br><center><img src="'+load+'" style="height: 300px;"></center>');
$.get( condicion_venta,
{ 'id' : id },
  function( data ) {
      $.fn.modal.Constructor.prototype.enforceFocus = function() {};//permite buscar desde Select2 obtenido desde la modal
      $('#condicion_venta').html(data);
  });
}

//funcion para optener el vuelto por el pago
function obtenerPago(key,valor,id, url_addpago, url_agregar_monto_pago, url_condicion_venta, load_buton){
  $bandera = true;
  if (key == 13) {
    if (valor == '') {
      document.getElementById("boton-cancelar").click();
      $bandera = false;
    } else {
      agregar_monto_pago(1, id , url_agregar_monto_pago, url_condicion_venta, load_buton);
    }
  }
  if ($bandera == true) {
    //document.getElementById("vuelto").innerHTML = '₡'+parseFloat(valor - total).toFixed(2);
    var dollar = 'no';
    $('.checkdolar').each( //con esto compruebo si el valor es dolar
      function() {
      if ($(this).is(':checked'))//Este if me dice que si esta marcado es porque el valor ingresado es dolar
        {
          dollar = 'si';
        }
    });
    $.ajax({
        url: url_addpago,
        type:"post",
        dataType: 'json',
        data: {'valor': valor, 'dollar' : dollar, 'id' : id},
        success: function(data){
          document.getElementById("vuelto").innerHTML = data.resultado_simbolo;
          document.getElementById("valorencolones").innerHTML = data.valorencolones;
          $('#vueltoinput').val(data.resultado);
          $('#pago_real').val(data.pago_real);
        }
    }) ;
  }
}

//Agregamos el monto pago
function agregar_monto_pago(tipo_pago, idCa, url_agregar_monto_pago, url_condicion_venta, load_buton) {
  var vueltoinput = document.getElementById("vueltoinput").value;
  var pago = document.getElementById("pago");
  var pago_real = document.getElementById("pago_real");
  if (pago.value=='') {
    pago.style.border = "3px solid red";
  }else {
    ddl_entidad = ''; tipoCuentaBancaria = '0';  comprobacion = ''; fechadepo = '';
    if (tipo_pago==4) {
      ddl_entidad = document.getElementById("ddl-entidad").value;
      tipoCuentaBancaria = document.getElementById("tipoCuentaBancaria").value;
      comprobacion = document.getElementById("comprobacion").value;
      fechadepo = document.getElementById("fechadepo").value;
    } else if (tipo_pago==2) {
      ddl_entidad = document.getElementById("medio_tarjeta").value;
      comprobacion = document.getElementById("comprobacion_tarjeta").value;
    } else if (tipo_pago==3) {
      ddl_entidad = document.getElementById("ddl-entidad-cheque").value;
      comprobacion = document.getElementById("comprobacion_cheque").value;
    }
  $.ajax({
        url: url_agregar_monto_pago,
        type:"post",
        data: {
          'idCa': idCa, 'pago': pago_real.value, 'tipo_pago': tipo_pago, 'vueltoinput' : vueltoinput,
          'ddl_entidad' : ddl_entidad, 'tipoCuentaBancaria' : tipoCuentaBancaria, 'fechadepo' : fechadepo, 'comprobacion' : comprobacion
        },
        beforeSend: function() {
          $('#vuelto_final').html('<center><img src="'+load_buton+'" style="height: 32px;"></center>');
          $("#pago").attr('disabled','disabled');
          //$( "#notifica_apartados" ).html( '<span style="font-size:10pt;" class="label label-info"> Espere un momento mientras se realiza el proceso... </span>' );
        },
        success: function(data_pago){
            $.get( url_condicion_venta,
            { 'id' : idCa },
            function( data ) {
                $.fn.modal.Constructor.prototype.enforceFocus = function() {};
                $('#condicion_venta').html(data);
                $('#vueltoinput').val(vueltoinput);
            });

        },
        error: function(msg, status,err){
          alert(err);
        }
    });
  }//fin else comprovacion de que campo monto pago esté lleno
}

//eliminar medio pago
function delete_medio_pago(id, idCa, url_delete_medio_pago, url_condicion_venta, load_buton) {
  $('#vuelto_final').html('<center><img src="'+load_buton+'" style="height: 32px;"></center>');
  $.get( url_delete_medio_pago,
  { 'id' : id },
  function( data ) {
    $.get( url_condicion_venta,
    { 'id' : idCa },
    function( data ) {
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        $('#condicion_venta').html(data);
    });
  });
}

//compruebo que los datos estan llenos a la hora de cancelas de acuerdo al tipo de pago para abilitar o no el boton
function compruebatipopago(tipo, url_compruebatipopago) {
    var entidadFinanc = '';
    var cuenta = '0';
    var comprobacion = '';
    //agregar medio pago
    if (tipo==4) {
      entidadFinanc = document.getElementById("ddl-entidad").value;
      cuenta = document.getElementById("tipoCuentaBancaria").value;
      comprobacion = document.getElementById("comprobacion").value;
    } else if (tipo==2) {
      entidadFinanc = document.getElementById("medio_tarjeta").value;
      comprobacion = document.getElementById("comprobacion_tarjeta").value;
    } else if (tipo==3) {
      entidadFinanc = document.getElementById("ddl-entidad-cheque").value;
      comprobacion = document.getElementById("comprobacion_cheque").value;
    }

    if (entidadFinanc == '' ||  comprobacion == '' || cuenta == '') {
        if (tipo==4) $('#boton-agregar-deposito').addClass('disabled');
        else if (tipo==2) $('#boton-agregar-tarjeta').addClass('disabled');
        else if (tipo==3) $('#boton-agregar-cheque').addClass('disabled');
    }
    else {
        $.ajax({
            url: url_compruebatipopago,
            type:"post",
            data: {'comprobacion': comprobacion, 'cuenta_deposito': cuenta, 'entidadFinanc': entidadFinanc, 'tipo': tipo},
            success: function(data){
                if (data=='repetido') {
                  if (tipo==4) {
                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación rechasada, ya esta en registro';
                    $('#boton-agregar-deposito').addClass('disabled');
                  } else if (tipo==2) {
                    document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación rechasada, ya esta en registro';
                    $('#boton-agregar-tarjeta').addClass('disabled');
                  } else if (tipo==3) {
                    document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación rechasada, ya esta en registro';
                    $('#boton-agregar-cheque').addClass('disabled');
                  }
                } else {
                  if (tipo==4) {
                    $('#boton-agregar-deposito').removeClass('disabled');
                    document.getElementById("nota_cancelacion").innerHTML = 'Comprobación aceptada';
                  } else if (tipo==2) {
                    $('#boton-agregar-tarjeta').removeClass('disabled');
                    document.getElementById("nota_cancelacion_2").innerHTML = 'Comprobación aceptada';
                  } else if (tipo==3) {
                    $('#boton-agregar-cheque').removeClass('disabled');
                    document.getElementById("nota_cancelacion_3").innerHTML = 'Comprobación aceptada';
                  }
                }

            },
            error: function(msg, status,err){
                            alert('error en linea 328, consulte a nelux');
                        }
        });
        //$('#boton-abonar-deposito').removeClass('disabled');
    }
}
//procedo a cancelar la factura
function cancelar_factura(idCa, this_boton, url_factura_electronica, url_inicio, url_orden) {
  vueltoinput = document.getElementById("vueltoinput").value;
  //convertir vuelto a decimal y con separadores de miles
  var num2 = formatNumber.new(parseFloat(Math.round(vueltoinput * 100) / 100).toFixed(2));//(""+vueltoinput).replace(/(\d+)(\d{3})(\d{3})$/ ,"$1 $2 $3").replace(/(\d+)(\d{3})$/   ,"$1 $2" ).replace(/(\d+)(\d{3})(\d{3})\./ ,"$1 $2 $3.") .replace(/(\d+)(\d{3})\./  ,"$1 $2." );
  //fin conversion vuelto a decimal
  document.getElementById("vuelto_final").innerHTML = '<small>Vuelto:</small> '+ num2;
  var orden = "<?= Url::toRoute('cabeza-caja/orden')?>&id="+idCa;
  var $btn = $(this_boton).button('loading'); //ponemos en suspencion el boton de cancelar
  $("#pago").attr('disabled','disabled');
  $.ajax({
    'type': "post",//enviamos a factun para la firma electronica
    'url': url_factura_electronica,
    'dataType': 'json',
    data: {
      'idCa' : idCa,
      'vueltoinput' : vueltoinput,
    },
    beforeSend: function(xhr) {
      document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-info" style="font-size:12pt;">[ Creando Factura/Tiquete electrónico (XML) ]</span><br><br><span class="label label-default" style="font-size:10pt;">Espere, facturación en proceso...</span></center><br>';
    },
    success: function(data_factura){//devuelve en json los datos de respuesta de factun
      //console.log(data_factura.json_result_facturacion);//sirven
      //console.log(data_factura.detalle);
      //console.log(data_factura.mensajes_error);//sirven
      var obj = jQuery.parseJSON( data_factura.json_result_facturacion );//creamos un objeto con los datos de factun
      if (obj.data.clave) {//si existen errores se presentan aqui y se imprime el detalle
        //var ticket = "<!--?= Url::toRoute('cabeza-caja/ticket')?-->&id="+idCa;
        //window.open(ticket,"_blank");
        //window.open(url_orden,"_blank");
        document.getElementById("alerta_cancelar").innerHTML = '<center><span class="label label-success" style="font-size:15pt;">[ Documento electrónico creado y firmado ]</span><br><br><span class="label label-default" style="font-size:10pt;">Espere que se recargue la página...</span></center><br>';
        izq = (screen.width-450)/2;
        sup= (screen.height-450)/2;
        var ventimp=window.open('#',"MsgWindow", "menubars=no,toolbars=no,width=600,height=300,left=" + izq + ",top=" + sup);//'MsgWindow'
        ventimp.document.write(data_factura.resultado_vuelto);
        ir_a_index(url_inicio, idCa);
      } else {
        document.getElementById("alerta_cancelar").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+obj.mensajes.mensaje+':</strong> '+obj.mensajes.detalle_mensaje+'.</center>'+data_factura.mensajes_error+'</div>';
        $btn.button('reset');
      }
    },
  });
}

//se ejecuta cuando se ocupa facturar a crédito
function finalizar_credito(idCa, tipo, this_boton, url_factura_electronica, url_inicio, url_orden) {
  var $btn = $(this_boton).button('loading');
    $.ajax({
        url: url_factura_electronica,
        type:"post",
        dataType: 'json',
        data: {'idCa' : idCa, 'vueltoinput': 0},
        beforeSend: function() {
          document.getElementById("alerta_cancelar").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Acreditando y firmando factura, Espere...</span></center><br>';
        },
        success: function(data_factura){
          console.log(data_factura.json_result_facturacion);
          //console.log(data_factura.detalle);
          console.log(data_factura.mensajes_error);
          var obj = jQuery.parseJSON( data_factura.json_result_facturacion );//creamos un objeto con los datos de factun
          if (obj.data.clave) {//si existen errores se presentan aqui y se imprime el detalle
            document.getElementById("alerta_cancelar").innerHTML = '<br><center><span class="label label-info" style="font-size:15pt;">Factura firmada y acreditada, espere que se recargue la pantalla...</span></center><br>';

            //window.open(url_orden,"_blank");
            izq = (screen.width-450)/2;
            sup= (screen.height-450)/2;
            var ventimp=window.open('#',"MsgWindow", "menubars=no,toolbars=no,width=600,height=150,left=" + izq + ",top=" + sup);//'MsgWindow'
            ventimp.document.write(data_factura.resultado_vuelto);
            ir_a_index(url_inicio, idCa);//window.location.href = url_;
            //setTimeout(ir_a_index(),2000);
            /*$.get( "<!--?= Yii::$app->getUrlManager()->createUrl('cabeza-caja/enviar_correo_factura') ?-->" ,
            {'id' : idCa, 'email' : data_canc.email , 'id_factun' : data_canc.id_factun},
              function( data ) {});*/
          } else {
            document.getElementById("alerta_cancelar").innerHTML = '<div class="alert alert-danger" role="alert"><center><strong>'+obj.mensajes.mensaje+':</strong> '+obj.mensajes.detalle_mensaje+'.</center>'+data_factura.mensajes_error+'</div>';
            $btn.button('reset');
            document.getElementById("notificaciones").innerHTML = '';
          }
        },
        error: function(msg, status,err){
                      //  alert('error en linea 204, consulte a nelux');
                    }
    });
}

//me devuelve al inicio segun sea el caso de la factura, (inicio prefactura o inicio caja)
//y envio el parametro del id con el que recibe ṕara poder imprimir
function ir_a_index(url_inicio, idCa){
  var url_ = url_inicio+"&id="+idCa;
  window.location.href = url_;
}

//para formatear el numero
var formatNumber = {
 separador: ",", // separador para los miles
 sepDecimal: '.', // separador para los decimales
 formatear:function (num){
 num +='';
 var splitStr = num.split(',');
 var splitLeft = splitStr[0];
 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
 var regx = /(\d+)(\d{3})/;
 while (regx.test(splitLeft)) {
 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
 }
 return this.simbol + splitLeft +splitRight;
 },
 new:function(num, simbol){
 this.simbol = simbol ||'';
 return this.formatear(num);
 }
}
