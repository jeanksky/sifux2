<?php

namespace backend\Zebra\src;

use RuntimeException;

class CommunicationException extends RuntimeException
{
    //
}
