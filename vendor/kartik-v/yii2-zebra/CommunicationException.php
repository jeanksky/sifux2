<?php

namespace zebra;

use RuntimeException;

class CommunicationException extends RuntimeException
{
    //
}
