
<div class="wrapper main-content-spacing">
	<div class="backup-default-index">

<?php
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Manage',
		'url' => array (
				'index' 
		) 
];
?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
	<?php echo Yii::$app->session->getFlash('success'); ?>
</div>
<?php endif; ?>

		<div class="panel">

					<header class="panel-heading form-spacing clearfix">
						<center>
					<h1 style="margin:0;" class="clearfix"><span class="glyphicon glyphicon-save"></span> Administrador de respaldos de  Base de Datos
						<span class="pull-right">
						<a href="<?= \yii\helpers\Url::toRoute(['create']) ?>" class="btn btn-success"> <i class="fa fa-plus"></i> Crear Respaldo </a>
						</span></h1>
						</center>
						</header>
					<div class="panel-body">
				
						<?php
						
						echo $this->render ( '_list', array (
								'dataProvider' => $dataProvider 
						) );
						?>
								
			
					</div>

		</div>
		
		
	</div>
</div>