<?php

$vendorDir = dirname(__DIR__);

return array (
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead',
    ),
  ),
  'kartik-v/yii2-widget-spinner' => 
  array (
    'name' => 'kartik-v/yii2-widget-spinner',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/spinner' => $vendorDir . '/kartik-v/yii2-widget-spinner',
    ),
  ),
  'kartik-v/yii2-widget-sidenav' => 
  array (
    'name' => 'kartik-v/yii2-widget-sidenav',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/sidenav' => $vendorDir . '/kartik-v/yii2-widget-sidenav',
    ),
  ),
  'kartik-v/yii2-widget-growl' => 
  array (
    'name' => 'kartik-v/yii2-widget-growl',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@kartik/growl' => $vendorDir . '/kartik-v/yii2-widget-growl',
    ),
  ),
  'kartik-v/yii2-widget-affix' => 
  array (
    'name' => 'kartik-v/yii2-widget-affix',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/affix' => $vendorDir . '/kartik-v/yii2-widget-affix',
    ),
  ),
  'kartik-v/yii2-widgets' => 
  array (
    'name' => 'kartik-v/yii2-widgets',
    'version' => '3.4.0.0',
    'alias' => 
    array (
      '@kartik/widgets' => $vendorDir . '/kartik-v/yii2-widgets',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.0.0-beta',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'kartik-v/yii2-widget-rangeinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-rangeinput',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/range' => $vendorDir . '/kartik-v/yii2-widget-rangeinput',
    ),
  ),
  'kartik-v/yii2-widget-depdrop' => 
  array (
    'name' => 'kartik-v/yii2-widget-depdrop',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/depdrop' => $vendorDir . '/kartik-v/yii2-widget-depdrop',
    ),
  ),
  'kartik-v/yii2-widget-switchinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-switchinput',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@kartik/switchinput' => $vendorDir . '/kartik-v/yii2-widget-switchinput',
    ),
  ),
  'kartik-v/yii2-widget-touchspin' => 
  array (
    'name' => 'kartik-v/yii2-widget-touchspin',
    'version' => '1.2.1.0',
    'alias' => 
    array (
      '@kartik/touchspin' => $vendorDir . '/kartik-v/yii2-widget-touchspin',
    ),
  ),
  'wbraganca/yii2-dynamicform' => 
  array (
    'name' => 'wbraganca/yii2-dynamicform',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/wbraganca/yii2-dynamicform',
    ),
  ),
  'kartik-v/yii2-widget-colorinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-colorinput',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/color' => $vendorDir . '/kartik-v/yii2-widget-colorinput',
    ),
  ),
  'kartik-v/yii2-dropdown-x' => 
  array (
    'name' => 'kartik-v/yii2-dropdown-x',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/dropdown' => $vendorDir . '/kartik-v/yii2-dropdown-x',
    ),
  ),
  'kartik-v/yii2-detail-view' => 
  array (
    'name' => 'kartik-v/yii2-detail-view',
    'version' => '1.7.4.0',
    'alias' => 
    array (
      '@kartik/detail' => $vendorDir . '/kartik-v/yii2-detail-view',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.1.1.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'unclead/yii2-multiple-input' => 
  array (
    'name' => 'unclead/yii2-multiple-input',
    'version' => '1.4.1.0',
    'alias' => 
    array (
      '@unclead/widgets/examples' => $vendorDir . '/unclead/yii2-multiple-input/examples',
      '@unclead/widgets' => $vendorDir . '/unclead/yii2-multiple-input/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '1.3.6.0',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker',
    ),
  ),
  'kartik-v/yii2-money' => 
  array (
    'name' => 'kartik-v/yii2-money',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@kartik/money' => $vendorDir . '/kartik-v/yii2-money',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.4.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  'kartik-v/yii2-nav-x' => 
  array (
    'name' => 'kartik-v/yii2-nav-x',
    'version' => '1.2.1.0',
    'alias' => 
    array (
      '@kartik/nav' => $vendorDir . '/kartik-v/yii2-nav-x',
    ),
  ),
  'open-ecommerce/yii2-backuprestore' => 
  array (
    'name' => 'open-ecommerce/yii2-backuprestore',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@oe/modules/backuprestore' => $vendorDir . '/open-ecommerce/yii2-backuprestore',
    ),
  ),
  'kartik-v/yii2-tabs-x' => 
  array (
    'name' => 'kartik-v/yii2-tabs-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/tabs' => $vendorDir . '/kartik-v/yii2-tabs-x',
    ),
  ),
  'alex-bond/yii2-thumbler' => 
  array (
    'name' => 'alex-bond/yii2-thumbler',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@alexBond/thumbler' => $vendorDir . '/alex-bond/yii2-thumbler/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.9.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => '1.3.4.0',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x',
    ),
  ),
  'kartik-v/yii2-widget-alert' => 
  array (
    'name' => 'kartik-v/yii2-widget-alert',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@kartik/alert' => $vendorDir . '/kartik-v/yii2-widget-alert',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'vilochane/yii2-barcode-generator' => 
  array (
    'name' => 'vilochane/yii2-barcode-generator',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@barcode/barcode' => $vendorDir . '/vilochane/yii2-barcode-generator',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'extpoint/yii2-megamenu' => 
  array (
    'name' => 'extpoint/yii2-megamenu',
    'version' => '1.6.6.0',
    'alias' => 
    array (
      '@extpoint/megamenu' => $vendorDir . '/extpoint/yii2-megamenu/lib',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.4.9.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform',
    ),
  ),
  'kartik-v/yii2-checkbox-x' => 
  array (
    'name' => 'kartik-v/yii2-checkbox-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/checkbox' => $vendorDir . '/kartik-v/yii2-checkbox-x',
    ),
  ),
  'kartik-v/yii2-datecontrol' => 
  array (
    'name' => 'kartik-v/yii2-datecontrol',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/datecontrol' => $vendorDir . '/kartik-v/yii2-datecontrol',
    ),
  ),
  'kartik-v/yii2-icons' => 
  array (
    'name' => 'kartik-v/yii2-icons',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/icons' => $vendorDir . '/kartik-v/yii2-icons',
    ),
  ),
  'kartik-v/yii2-slider' => 
  array (
    'name' => 'kartik-v/yii2-slider',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/slider' => $vendorDir . '/kartik-v/yii2-slider',
    ),
  ),
  'robregonm/yii2-pdf' => 
  array (
    'name' => 'robregonm/yii2-pdf',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@robregonm/pdf' => $vendorDir . '/robregonm/yii2-pdf',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'spanjeta/yii2-backup' => 
  array (
    'name' => 'spanjeta/yii2-backup',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@spanjeta/modules/backup' => $vendorDir . '/spanjeta/yii2-backup',
    ),
  ),
  'nterms/yii2-pagesize-widget' => 
  array (
    'name' => 'nterms/yii2-pagesize-widget',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@nterms/pagesize' => $vendorDir . '/nterms/yii2-pagesize-widget',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'nterms/yii2-mailqueue' => 
  array (
    'name' => 'nterms/yii2-mailqueue',
    'version' => '0.0.14.0',
    'alias' => 
    array (
      '@nterms/mailqueue' => $vendorDir . '/nterms/yii2-mailqueue',
    ),
    'bootstrap' => 'nterms\\mailqueue\\Bootstrap',
  ),
  'yii2tech/crontab' => 
  array (
    'name' => 'yii2tech/crontab',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@yii2tech/crontab' => $vendorDir . '/yii2tech/crontab',
    ),
  ),
);
