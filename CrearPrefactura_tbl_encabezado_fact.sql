DELIMITER $$

USE `yii2advanced`$$

DROP TRIGGER /*!50032 IF EXISTS */ `CrearPrefactura_tbl_encabezado_fact`$$

CREATE
    /*!50017 DEFINER = 'root'@'localhost' */
    TRIGGER `CrearPrefactura_tbl_encabezado_fact` AFTER INSERT ON `tbl_orden_servicio` 
    FOR EACH ROW BEGIN
	IF (!NEW.idCliente = "") THEN
		INSERT INTO tbl_encabezado_factura (fecha_inicio,fecha_final,idCliente,idOrdenServicio,porc_descuento, iva, total_a_pagar, estadoFactura) 
		VALUE (NOW(),'',NEW.idCliente,NEW.idOrdenServicio,0,0,NEW.montoServicio,'Pendiente');
	ELSE 
		INSERT INTO tbl_encabezado_factura (fecha_inicio,fecha_final,idCliente,idOrdenServicio,porc_descuento, iva, total_a_pagar, estadoFactura) 
		VALUE (NOW(),'',0,NEW.idOrdenServicio,0,0,NEW.montoServicio,'Pendiente');	
	END IF;
    END;
$$

DELIMITER ;