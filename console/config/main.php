<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'language'=>'es',
    'sourceLanguage'=>'es',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    //'__class' => 'yii\log\FileTarget::class',
                    'categories' => ['yii\swiftmailer\Logger::add'],
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'htmlLayout' => '@common/mail/layouts/html',
            'textLayout' => '@common/mail/layouts/text',  // custome layout
        ],
        'mailqueue' => [
            'class' => 'nterms\mailqueue\MailQueue',
			      'table' => '{{%mail_queue}}',
        ],
    ],
    'params' => $params,
    'params' => [
      'icon-framework' => 'fa',  // Font Awesome Icon framework
    ]
];
