/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 5.7.19-0ubuntu0.16.04.1 : Database - neluxdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`neluxdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `neluxdb`;

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

/*Table structure for table `operacion` */

DROP TABLE IF EXISTS `operacion`;

CREATE TABLE `operacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `traduccion` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `operacion` */

insert  into `operacion`(`id`,`nombre`,`traduccion`) values 
(1,'site-about','Acerca de'),
(2,'clientes-index','Cliente ver todos'),
(3,'clientes-create','Cliente crear'),
(4,'clientes-update','Cliente actualizar'),
(5,'clientes-delete','Cliente eliminar'),
(6,'funcionario-index','Funcionario ver todos'),
(7,'funcionario-create','Funcionario crear'),
(8,'funcionario-update','Funcionario actualizar'),
(9,'funcionario-delete','Funcionario eliminar'),
(10,'funcionario-view','Funcionario consultar'),
(11,'clientes-view','Cliente consultar'),
(12,'usuarios-derechos-index',''),
(13,'usuarios-derechos-create',NULL),
(14,'usuarios-derechos-update',NULL),
(15,'usuarios-derechos-view',NULL),
(16,'mecanicos-index','Mecánico ver todo'),
(17,'mecanicos-create','Mecánico crear'),
(18,'mecanicos-update','Mecánico actualizar'),
(19,'mecanicos-delete','Mecánico eliminar'),
(20,'mecanicos-view','Mecánico consultar'),
(21,'marcas-index','Marca ver todos'),
(22,'marcas-create','Marca crear'),
(23,'marcas-update','Marca actualizar'),
(24,'marcas-delete','Marca eliminar'),
(25,'marcas-view','Marca consultar'),
(26,'modelo-index','Modelo ver todos'),
(27,'modelo-create','Modelo crear'),
(28,'modelo-update','Modelo actualizar'),
(29,'modelo-delete','Modelo eliminar'),
(30,'modelo-view','Modelo consultar'),
(31,'vehiculos-index','Vehículo ver todos'),
(32,'vehiculos-create','Vehículo crear'),
(33,'vehiculos-update','Vehículo actualizar'),
(34,'vehiculos-delete','Vehículo eliminar'),
(35,'vehiculos-view','Vehículo consultar'),
(36,'familia-index','Categoría ver todos'),
(37,'familia-create','Categoría crear'),
(38,'familia-update','Categoría actualizar'),
(39,'familia-delete','Categoría eliminar'),
(40,'familia-view','Categoría consultar'),
(41,'servicios-index','Servicio ver todos'),
(42,'servicios-create','Servicio crear'),
(43,'servicios-update','Servicio actualizar'),
(44,'servicios-delete','Servicio eliminar'),
(45,'servicios-view','Servicio consultar'),
(46,'proveedores-index','Proveedor ver todos'),
(47,'proveedores-create','Proveedor crear'),
(48,'proveedores-update','Proveedor actualizar'),
(49,'proveedores-delete','Proveedor eliminar'),
(50,'proveedores-view','Proveedor consultar'),
(51,'producto-servicios-index','Producto ver todos'),
(52,'producto-servicios-create','Producto crear'),
(53,'producto-servicios-update','Producto actualizar'),
(54,'producto-servicios-delete','Producto eliminar'),
(55,'producto-servicios-view','Producto consultar'),
(56,'orden-servicio-index','Orden servicio ver todos'),
(57,'orden-servicio-create','Orden servicio crear'),
(58,'orden-servicio-update','Orden servicio actualizar'),
(59,'orden-servicio-delete','Orden servicio eliminar'),
(60,'orden-servicio-view','Orden servicio consultar'),
(61,'version-index','Versión ver todos'),
(62,'version-create','Versión crear'),
(63,'version-update','Versión actualizar'),
(64,'version-delete','Versión eliminar'),
(65,'version-view','Versión consultar'),
(66,'compras-inventario-index','Compras inventario ver todos'),
(67,'compras-inventario-create','Compras inventario crear'),
(68,'compras-inventario-update','Compras inventario actualizar'),
(69,'compras-inventario-delete','Compras inventario eliminar'),
(70,'compras-inventario-view','Compras inventario consultar'),
(71,'cuentas-bancarias-index','Consultar módulo de Bancos'),
(72,'tramite-pago-index','Consultar módulo de pagos proveedores'),
(73,'tramite-pago-movimientos','Manipular movimientos por pagar'),
(74,'tramite-pago-completar_pago','Crear Pagos a proveedores'),
(75,'tipo-movimientos-index','Tipo movimientos ver todos'),
(76,'tipo-movimientos-create','Tipo movimientos crear'),
(77,'tipo-movimientos-update','Tipo movimientos actualizar'),
(78,'tipo-movimientos-delete','Tipo movimientos eliminar'),
(79,'tipo-movimientos-view','Tipo movimientos consultar'),
(80,'movimientos-index','Movimiento inventario ver todo'),
(81,'movimientos-create','Movimiento inventario crear'),
(82,'movimientos-update','Movimiento inventario actualizar'),
(83,'movimientos-delete','Movimiento inventario eliminar'),
(84,'movimientos-aplicarmovimiento','Movimiento inventario aplicar'),
(85,'facturas-dia-index','Consultar facturas'),
(86,'movimiento-credito-index','Consulta del módulo'),
(87,'movimiento-credito-agregar_abono','Agregar abono'),
(88,'movimiento-credito-aplicarnotasdecredito','Aplicar notas de crédito'),
(89,'movimiento-credito-cancelarvariasfacturas','Cancelar facturas por cobrar');

/*Table structure for table `rol_operacion` */

DROP TABLE IF EXISTS `rol_operacion`;

CREATE TABLE `rol_operacion` (
  `rol_id` int(11) NOT NULL,
  `operacion_id` int(11) NOT NULL,
  PRIMARY KEY (`rol_id`,`operacion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `rol_operacion` */

insert  into `rol_operacion`(`rol_id`,`operacion_id`) values 
(1,12),
(1,13),
(1,14),
(1,15);

/*Table structure for table `tbl_bancos` */

DROP TABLE IF EXISTS `tbl_bancos`;

CREATE TABLE `tbl_bancos` (
  `idBanco` int(11) NOT NULL AUTO_INCREMENT,
  `nombreBanco` varchar(50) NOT NULL,
  PRIMARY KEY (`idBanco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_bancos` */

/*Table structure for table `tbl_categoria_tipo_cambio` */

DROP TABLE IF EXISTS `tbl_categoria_tipo_cambio`;

CREATE TABLE `tbl_categoria_tipo_cambio` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(40) NOT NULL,
  `idTipo_moneda` int(11) NOT NULL,
  `idEntidad_financiera` int(11) NOT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_categoria_tipo_cambio` */

/*Table structure for table `tbl_clientes` */

DROP TABLE IF EXISTS `tbl_clientes`;

CREATE TABLE `tbl_clientes` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `tipoIdentidad` varchar(9) NOT NULL,
  `nombreCompleto` varchar(80) NOT NULL,
  `identificacion` varchar(20) NOT NULL,
  `genero` varchar(10) DEFAULT NULL,
  `fechNacimiento` date DEFAULT NULL,
  `telefono` varchar(10) NOT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `celular` varchar(10) NOT NULL,
  `email` varchar(130) DEFAULT NULL,
  `direccion` varchar(130) NOT NULL,
  `credito` varchar(2) NOT NULL,
  `documentoGarantia` varchar(2) DEFAULT NULL,
  `diasCredito` varchar(2) DEFAULT NULL,
  `autorizacion` varchar(2) DEFAULT NULL,
  `montoMaximoCredito` decimal(12,2) DEFAULT NULL,
  `montoTotalEjecutado` decimal(12,2) DEFAULT NULL,
  `estadoCuenta` varchar(10) DEFAULT NULL,
  `notificaciones` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_clientes` */

/*Table structure for table `tbl_compras` */

DROP TABLE IF EXISTS `tbl_compras`;

CREATE TABLE `tbl_compras` (
  `idCompra` int(11) NOT NULL AUTO_INCREMENT,
  `numFactura` varchar(11) DEFAULT NULL,
  `fechaDocumento` date DEFAULT NULL,
  `fechaRegistro` date DEFAULT NULL,
  `fechaVencimiento` date DEFAULT NULL,
  `idProveedor` int(11) DEFAULT NULL,
  `formaPago` varchar(10) DEFAULT NULL,
  `subTotal` decimal(12,2) DEFAULT NULL,
  `descuento` decimal(12,2) DEFAULT NULL,
  `impuesto` decimal(12,2) DEFAULT NULL,
  `total` decimal(12,2) DEFAULT NULL,
  `estado` varchar(12) DEFAULT NULL,
  `estado_pago` varchar(25) DEFAULT NULL,
  `idTramite_pago` int(11) DEFAULT NULL,
  `credi_saldo` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_compras` */

/*Table structure for table `tbl_config` */

DROP TABLE IF EXISTS `tbl_config`;

CREATE TABLE `tbl_config` (
  `moneda` varchar(2) DEFAULT NULL,
  `impuesto` int(11) DEFAULT NULL,
  `nombreimpresora` varchar(80) DEFAULT NULL,
  `equipo` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_config` */

insert  into `tbl_config`(`moneda`,`impuesto`,`nombreimpresora`,`equipo`) values 
(NULL,13,NULL,NULL);

/*Table structure for table `tbl_contactos_pagos` */

DROP TABLE IF EXISTS `tbl_contactos_pagos`;

CREATE TABLE `tbl_contactos_pagos` (
  `idContacto_pago` int(11) NOT NULL AUTO_INCREMENT,
  `codProveedores` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `telefono_ofic` varchar(10) NOT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `celular` varchar(10) NOT NULL,
  `email` varchar(80) NOT NULL,
  PRIMARY KEY (`idContacto_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_contactos_pagos` */

/*Table structure for table `tbl_contactos_pedidos` */

DROP TABLE IF EXISTS `tbl_contactos_pedidos`;

CREATE TABLE `tbl_contactos_pedidos` (
  `idContacto_pedido` int(11) NOT NULL AUTO_INCREMENT,
  `codProveedores` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `telefono_ofic` varchar(10) NOT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `celular` varchar(10) NOT NULL,
  `email` varchar(80) NOT NULL,
  PRIMARY KEY (`idContacto_pedido`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_contactos_pedidos` */

/*Table structure for table `tbl_cuentas_bancarias` */

DROP TABLE IF EXISTS `tbl_cuentas_bancarias`;

CREATE TABLE `tbl_cuentas_bancarias` (
  `idBancos` int(10) NOT NULL AUTO_INCREMENT,
  `idTipo_moneda` int(11) NOT NULL,
  `idEntidad_financiera` int(11) NOT NULL,
  `numero_cuenta` varchar(40) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `tipo_cuenta` varchar(10) NOT NULL,
  `cuenta_cliente` varchar(17) NOT NULL,
  `saldo_libros` decimal(12,2) DEFAULT NULL,
  `saldo_bancos` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idBancos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cuentas_bancarias` */

/*Table structure for table `tbl_cuentas_bancarias_proveedor` */

DROP TABLE IF EXISTS `tbl_cuentas_bancarias_proveedor`;

CREATE TABLE `tbl_cuentas_bancarias_proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodProveedores` int(11) NOT NULL,
  `entidad_bancaria` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `cuenta_bancaria` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cuentas_bancarias_proveedor` */

/*Table structure for table `tbl_detalle_compra` */

DROP TABLE IF EXISTS `tbl_detalle_compra`;

CREATE TABLE `tbl_detalle_compra` (
  `idDetalleCompra` int(11) NOT NULL AUTO_INCREMENT,
  `idCompra` int(11) NOT NULL,
  `noEntrada` int(11) DEFAULT NULL,
  `codProdServicio` varchar(30) NOT NULL,
  `precioCompra` decimal(12,2) NOT NULL,
  `cantidadAnterior` int(11) DEFAULT NULL,
  `cantidadEntrada` int(11) NOT NULL,
  `descuento` decimal(12,2) DEFAULT NULL,
  `exImpuesto` varchar(3) NOT NULL,
  `porcentUtilidad` double NOT NULL,
  `preciosinImp` double NOT NULL,
  `precioconImp` double NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `estado` varchar(30) NOT NULL,
  PRIMARY KEY (`idDetalleCompra`),
  KEY `FK_tbl_detalle_compra` (`idCompra`),
  CONSTRAINT `FK_tbl_detalle_compra` FOREIGN KEY (`idCompra`) REFERENCES `tbl_compras` (`idCompra`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_detalle_compra` */

/*Table structure for table `tbl_detalle_facturas` */

DROP TABLE IF EXISTS `tbl_detalle_facturas`;

CREATE TABLE `tbl_detalle_facturas` (
  `idCabeza_factura` int(11) NOT NULL,
  `codProdServicio` varchar(20) DEFAULT NULL,
  `cantidad` int(4) DEFAULT NULL,
  `precio_unitario` decimal(30,2) DEFAULT NULL,
  `precio_por_cantidad` double(30,2) DEFAULT NULL,
  `idDetalleFactura` int(11) NOT NULL AUTO_INCREMENT,
  `descuento_producto` decimal(10,2) DEFAULT NULL,
  `subtotal_iva` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`idDetalleFactura`),
  KEY `FK_tbl_detalle_facturas_Productos` (`codProdServicio`),
  KEY `FK_tbl_detalle_facturas_cabeza` (`idCabeza_factura`),
  CONSTRAINT `FK_tbl_detalle_facturas` FOREIGN KEY (`idCabeza_factura`) REFERENCES `tbl_encabezado_factura` (`idCabeza_Factura`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_detalle_facturas` */

/*Table structure for table `tbl_detalle_movimientos` */

DROP TABLE IF EXISTS `tbl_detalle_movimientos`;

CREATE TABLE `tbl_detalle_movimientos` (
  `idDetalleMovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `idMovimiento` int(11) NOT NULL,
  `codProdServicio` varchar(30) NOT NULL,
  `cantidadinvant` int(11) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `cantidadinv` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDetalleMovimiento`),
  KEY `FK_tbl_detalle_movimientos` (`idMovimiento`),
  CONSTRAINT `FK_tbl_detalle_movimientos` FOREIGN KEY (`idMovimiento`) REFERENCES `tbl_movimientos` (`idMovimiento`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_detalle_movimientos` */

/*Table structure for table `tbl_detalle_ord_servicio` */

DROP TABLE IF EXISTS `tbl_detalle_ord_servicio`;

CREATE TABLE `tbl_detalle_ord_servicio` (
  `idOrdenServicio` int(11) NOT NULL,
  `familia` int(11) NOT NULL,
  `producto` varchar(20) NOT NULL,
  PRIMARY KEY (`idOrdenServicio`,`familia`,`producto`),
  CONSTRAINT `FK_tbl_detalle_ord_servicio` FOREIGN KEY (`idOrdenServicio`) REFERENCES `tbl_orden_servicio` (`idOrdenServicio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_detalle_ord_servicio` */

/*Table structure for table `tbl_detalle_proforma` */

DROP TABLE IF EXISTS `tbl_detalle_proforma`;

CREATE TABLE `tbl_detalle_proforma` (
  `idCabeza_factura` int(11) NOT NULL,
  `codProdServicio` varchar(20) DEFAULT NULL,
  `descrip_temporal` varchar(400) DEFAULT NULL,
  `cantidad` int(4) DEFAULT NULL,
  `precio_unitario` decimal(30,2) DEFAULT NULL,
  `precio_por_cantidad` double(30,2) DEFAULT NULL,
  `idDetalleFactura` int(11) NOT NULL AUTO_INCREMENT,
  `descuento_producto` decimal(10,2) DEFAULT NULL,
  `subtotal_iva` decimal(10,2) DEFAULT NULL,
  `temporal` varchar(1) DEFAULT NULL,
  `precio_unit_prov_temporal` decimal(10,2) DEFAULT NULL,
  `porc_ganancias_temporal` decimal(10,2) DEFAULT NULL,
  `precio_unit_gan_temporal` decimal(10,2) DEFAULT NULL,
  `precio_total_ganacia_mod` decimal(10,2) DEFAULT NULL,
  `precio_mas_iva_total_temporal_mod` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`idDetalleFactura`),
  KEY `FK_tbl_detalle_proforma_Productos` (`codProdServicio`),
  KEY `FK_tbl_detalle_proforma_cabeza` (`idCabeza_factura`),
  CONSTRAINT `FK_tbl_detalle_proforma` FOREIGN KEY (`idCabeza_factura`) REFERENCES `tbl_proforma` (`idCabeza_Factura`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_detalle_proforma` */

/*Table structure for table `tbl_empresa` */

DROP TABLE IF EXISTS `tbl_empresa`;

CREATE TABLE `tbl_empresa` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(130) NOT NULL,
  `localidad` varchar(130) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `fax` varchar(10) NOT NULL,
  `email` varchar(60) NOT NULL,
  `email_gerencia` varchar(60) DEFAULT NULL,
  `email_proveeduria` varchar(60) DEFAULT NULL,
  `email_contabilidad` varchar(60) DEFAULT NULL,
  `email_notificaciones` varchar(60) DEFAULT NULL,
  `sitioWeb` varchar(50) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `logo_etiqueta` varchar(255) DEFAULT NULL,
  `ced_juridica` varchar(50) DEFAULT NULL,
  `sesiones_permitidas` int(4) DEFAULT NULL,
  `idProducto_lutux` varchar(20) DEFAULT NULL,
  `estado_producto_lutux` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idEmpresa`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_empresa` */

insert  into `tbl_empresa`(`idEmpresa`,`nombre`,`direccion`,`localidad`,`estado`,`telefono`,`fax`,`email`,`email_gerencia`,`email_proveeduria`,`email_contabilidad`,`email_notificaciones`,`sitioWeb`,`logo`,`logo_etiqueta`,`ced_juridica`,`sesiones_permitidas`,`idProducto_lutux`,`estado_producto_lutux`) values 
(1,'Nombre de empresa','Dirección','Ciudad','Provincia o Estado','0000-00-00','0000-00-00','ejemplo@mail.com',NULL,NULL,NULL,NULL,'www.susitioweb.com',NULL,NULL,NULL,1,NULL,NULL);

/*Table structure for table `tbl_encabezado_factura` */

DROP TABLE IF EXISTS `tbl_encabezado_factura`;

CREATE TABLE `tbl_encabezado_factura` (
  `idCabeza_Factura` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_final` date DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `idCliente` varchar(80) DEFAULT NULL,
  `idOrdenServicio` int(11) DEFAULT NULL,
  `estadoFactura` varchar(12) NOT NULL,
  `tipoFacturacion` varchar(10) DEFAULT NULL,
  `codigoVendedor` int(11) DEFAULT NULL,
  `observacion` varchar(200) DEFAULT NULL,
  `credi_atencion` varchar(2) DEFAULT NULL,
  `credi_saldo` decimal(12,2) DEFAULT NULL,
  `subtotal` decimal(12,2) DEFAULT NULL,
  `porc_descuento` decimal(12,2) DEFAULT NULL,
  `iva` decimal(12,2) DEFAULT NULL,
  `total_a_pagar` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idCabeza_Factura`),
  UNIQUE KEY `idCabeza_Factura` (`idCabeza_Factura`),
  KEY `FK_tbl_encabezado_factura` (`idCliente`),
  KEY `FK_tbl_encabezado_factura_orden_servicio` (`idOrdenServicio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_encabezado_factura` */

/*Table structure for table `tbl_entidades_financieras` */

DROP TABLE IF EXISTS `tbl_entidades_financieras`;

CREATE TABLE `tbl_entidades_financieras` (
  `idEntidad_financiera` int(11) NOT NULL AUTO_INCREMENT,
  `abreviatura` varchar(80) NOT NULL,
  `descripcion` varchar(80) NOT NULL,
  `telefono1` varchar(10) DEFAULT NULL,
  `telefono2` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idEntidad_financiera`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_entidades_financieras` */

/*Table structure for table `tbl_etiquetas` */

DROP TABLE IF EXISTS `tbl_etiquetas`;

CREATE TABLE `tbl_etiquetas` (
  `codigo` varchar(40) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `fecha_reg` date DEFAULT NULL,
  `ubi` varchar(40) DEFAULT NULL,
  `precio` double(12,2) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_etiquetas` */

/*Table structure for table `tbl_familia` */

DROP TABLE IF EXISTS `tbl_familia`;

CREATE TABLE `tbl_familia` (
  `codFamilia` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(40) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  PRIMARY KEY (`codFamilia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_familia` */

/*Table structure for table `tbl_formas_pago` */

DROP TABLE IF EXISTS `tbl_formas_pago`;

CREATE TABLE `tbl_formas_pago` (
  `id_forma` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `desc_forma` varchar(45) NOT NULL,
  PRIMARY KEY (`id_forma`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_formas_pago` */

insert  into `tbl_formas_pago`(`id_forma`,`desc_forma`) values 
(1,'Efectivo'),
(2,'Depósito'),
(3,'Tarjeta'),
(4,'Crédito');

/*Table structure for table `tbl_funcionario` */

DROP TABLE IF EXISTS `tbl_funcionario`;

CREATE TABLE `tbl_funcionario` (
  `idFuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(20) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido1` varchar(15) NOT NULL,
  `apellido2` varchar(15) DEFAULT NULL,
  `genero` varchar(10) NOT NULL,
  `tipoFuncionario` varchar(30) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `email` varchar(130) DEFAULT NULL,
  `tipo_usuario` varchar(15) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idFuncionario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_funcionario` */

/*Table structure for table `tbl_marcas` */

DROP TABLE IF EXISTS `tbl_marcas`;

CREATE TABLE `tbl_marcas` (
  `codMarcas` varchar(20) NOT NULL,
  PRIMARY KEY (`codMarcas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_marcas` */

/*Table structure for table `tbl_mecanicos` */

DROP TABLE IF EXISTS `tbl_mecanicos`;

CREATE TABLE `tbl_mecanicos` (
  `idMecanico` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `direccion` varchar(130) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `porcentComision` int(2) NOT NULL,
  `codigo` int(11) NOT NULL,
  PRIMARY KEY (`idMecanico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_mecanicos` */

/*Table structure for table `tbl_medio_pago` */

DROP TABLE IF EXISTS `tbl_medio_pago`;

CREATE TABLE `tbl_medio_pago` (
  `idMetPago` int(11) NOT NULL AUTO_INCREMENT,
  `idCabeza_factura` int(11) NOT NULL,
  `idForma_Pago` int(11) DEFAULT NULL,
  `tipo_del_medio` varchar(80) DEFAULT NULL,
  `comprobacion` varchar(80) DEFAULT NULL,
  `cuenta_deposito` varchar(100) DEFAULT NULL,
  `estado` varchar(10) DEFAULT NULL,
  `fecha_deposito` date DEFAULT NULL,
  PRIMARY KEY (`idMetPago`),
  KEY `FK_tbl_medio_pago` (`idCabeza_factura`),
  CONSTRAINT `FK_tbl_medio_pago` FOREIGN KEY (`idCabeza_factura`) REFERENCES `tbl_encabezado_factura` (`idCabeza_Factura`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_medio_pago` */

/*Table structure for table `tbl_medio_pago_proveedor` */

DROP TABLE IF EXISTS `tbl_medio_pago_proveedor`;

CREATE TABLE `tbl_medio_pago_proveedor` (
  `idMedio_pago` int(11) NOT NULL AUTO_INCREMENT,
  `idTramite_pago` int(11) NOT NULL,
  `medio_pago` varchar(40) NOT NULL,
  `idBanco` int(11) DEFAULT NULL,
  `idTipo_documento` int(11) DEFAULT NULL,
  `numero_cheque` varchar(50) DEFAULT NULL,
  `numero_recibo_cancelacion` varchar(50) DEFAULT NULL,
  `idContacto_pago` int(11) DEFAULT NULL,
  `nombre_agente` varchar(60) DEFAULT NULL,
  `fecha_documento` date DEFAULT NULL,
  `posFecha` datetime DEFAULT NULL,
  `cuenta_bancaria_local` int(11) DEFAULT NULL,
  `cuenta_bancaria_proveedor` int(11) DEFAULT NULL,
  `saldo_proveedor` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idMedio_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_medio_pago_proveedor` */

/*Table structure for table `tbl_modelo` */

DROP TABLE IF EXISTS `tbl_modelo`;

CREATE TABLE `tbl_modelo` (
  `codModelo` varchar(80) NOT NULL,
  `codMarca` varchar(20) NOT NULL,
  PRIMARY KEY (`codModelo`),
  KEY `FK_tbl_modelo` (`codMarca`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_modelo` */

/*Table structure for table `tbl_moneda` */

DROP TABLE IF EXISTS `tbl_moneda`;

CREATE TABLE `tbl_moneda` (
  `idTipo_moneda` int(11) NOT NULL AUTO_INCREMENT,
  `abreviatura` varchar(15) NOT NULL,
  `descripcion` varchar(40) NOT NULL,
  `simbolo` varchar(1) NOT NULL,
  `monedalocal` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`idTipo_moneda`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_moneda` */

insert  into `tbl_moneda`(`idTipo_moneda`,`abreviatura`,`descripcion`,`simbolo`,`monedalocal`) values 
(1,'COL','Colón','¢','x'),
(2,'US','Dolar','$',NULL);

/*Table structure for table `tbl_movi_cobrar` */

DROP TABLE IF EXISTS `tbl_movi_cobrar`;

CREATE TABLE `tbl_movi_cobrar` (
  `idMov` int(11) NOT NULL AUTO_INCREMENT,
  `fecmov` datetime NOT NULL,
  `tipmov` varchar(20) NOT NULL,
  `concepto` varchar(200) NOT NULL,
  `monto_anterior` decimal(12,2) DEFAULT NULL,
  `monto_movimiento` decimal(12,2) DEFAULT NULL,
  `saldo_pendiente` decimal(12,2) DEFAULT NULL,
  `usuario` varchar(40) DEFAULT NULL,
  `idCabeza_Factura` int(11) DEFAULT NULL,
  `idCliente` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`idMov`),
  KEY `FK_tbl_nota_credito` (`fecmov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_movi_cobrar` */

/*Table structure for table `tbl_movi_pagar` */

DROP TABLE IF EXISTS `tbl_movi_pagar`;

CREATE TABLE `tbl_movi_pagar` (
  `idMov` int(11) NOT NULL AUTO_INCREMENT,
  `numero_mov` int(11) DEFAULT NULL,
  `idCompra` int(11) DEFAULT NULL,
  `numFactura` varchar(20) DEFAULT NULL,
  `idTramite_pago` int(11) DEFAULT NULL,
  `fecha_mov` date NOT NULL,
  `tipo_mov` varchar(2) NOT NULL,
  `detalle` varchar(120) NOT NULL,
  `monto_anterior` decimal(12,2) DEFAULT NULL,
  `monto_movimiento` decimal(12,2) DEFAULT NULL,
  `saldo_pendiente` decimal(12,2) DEFAULT NULL,
  `usuario` varchar(40) NOT NULL,
  `proveedor` varchar(80) NOT NULL,
  PRIMARY KEY (`idMov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_movi_pagar` */

/*Table structure for table `tbl_movimiento_libros` */

DROP TABLE IF EXISTS `tbl_movimiento_libros`;

CREATE TABLE `tbl_movimiento_libros` (
  `idMovimiento_libro` int(11) NOT NULL AUTO_INCREMENT,
  `idCuenta_bancaria` int(10) NOT NULL,
  `idTipoMoneda` int(11) NOT NULL,
  `idTipodocumento` int(11) NOT NULL,
  `idMedio_pago` int(11) DEFAULT NULL,
  `numero_documento` int(50) NOT NULL,
  `fecha_documento` date NOT NULL,
  `monto` decimal(12,2) NOT NULL,
  `tasa_cambio` decimal(12,2) DEFAULT NULL,
  `monto_moneda_local` decimal(12,2) DEFAULT NULL,
  `saldo` decimal(12,2) DEFAULT NULL,
  `beneficiario` varchar(50) DEFAULT NULL,
  `detalle` varchar(150) NOT NULL,
  `fecha_registro` date NOT NULL,
  `usuario_registra` varchar(50) NOT NULL,
  PRIMARY KEY (`idMovimiento_libro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_movimiento_libros` */

/*Table structure for table `tbl_movimientos` */

DROP TABLE IF EXISTS `tbl_movimientos`;

CREATE TABLE `tbl_movimientos` (
  `idMovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `codigoTipo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `documentoRef` varchar(1000) NOT NULL,
  `observaciones` varchar(1000) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idMovimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_movimientos` */

/*Table structure for table `tbl_nc_pendientes` */

DROP TABLE IF EXISTS `tbl_nc_pendientes`;

CREATE TABLE `tbl_nc_pendientes` (
  `idNCPendiente` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` varchar(40) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `concepto` varchar(200) DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idNCPendiente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_nc_pendientes` */

/*Table structure for table `tbl_orden_servicio` */

DROP TABLE IF EXISTS `tbl_orden_servicio`;

CREATE TABLE `tbl_orden_servicio` (
  `idOrdenServicio` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `montoServicio` float(12,2) DEFAULT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `placa` int(8) NOT NULL,
  `idMecanico` int(11) NOT NULL,
  `radio` varchar(2) NOT NULL,
  `alfombra` varchar(2) NOT NULL,
  `herramienta` varchar(2) NOT NULL,
  `repuesto` varchar(2) NOT NULL,
  `cantOdometro` int(11) NOT NULL,
  `cantCombustible` varchar(6) NOT NULL,
  `observaciones` varchar(150) DEFAULT NULL,
  `observacionesServicio` varchar(5000) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  PRIMARY KEY (`idOrdenServicio`),
  KEY `FK_tbl_orden_servicio_Mecanico` (`idMecanico`),
  KEY `FK_tbl_orden_servicio_vehiculo` (`placa`),
  KEY `FK_tbl_orden_servicio_Cliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_orden_servicio` */

/*Table structure for table `tbl_pago_por_caja` */

DROP TABLE IF EXISTS `tbl_pago_por_caja`;

CREATE TABLE `tbl_pago_por_caja` (
  `idPago` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `monto` double NOT NULL,
  PRIMARY KEY (`idPago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pago_por_caja` */

/*Table structure for table `tbl_producto_servicios` */

DROP TABLE IF EXISTS `tbl_producto_servicios`;

CREATE TABLE `tbl_producto_servicios` (
  `codProdServicio` varchar(20) NOT NULL,
  `tipo` varchar(10) DEFAULT NULL,
  `numFacturaProv` int(11) DEFAULT NULL,
  `nombreProductoServicio` varchar(50) DEFAULT NULL,
  `cantidadInventario` int(11) DEFAULT NULL,
  `cantidadMinima` int(11) DEFAULT NULL,
  `codFamilia` int(11) DEFAULT NULL,
  `precioCompra` decimal(10,2) DEFAULT NULL,
  `porcentUnidad` decimal(10,2) DEFAULT NULL,
  `precioVenta` decimal(10,2) DEFAULT NULL,
  `exlmpuesto` varchar(2) DEFAULT NULL,
  `precioVentaImpuesto` decimal(10,2) DEFAULT NULL,
  `anularDescuento` varchar(2) DEFAULT NULL,
  `precioMinServicio` decimal(10,2) DEFAULT NULL,
  `ubicacion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codProdServicio`),
  KEY `FK_tbl_producto_servicios` (`codFamilia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_producto_servicios` */

/*Table structure for table `tbl_proforma` */

DROP TABLE IF EXISTS `tbl_proforma`;

CREATE TABLE `tbl_proforma` (
  `idCabeza_Factura` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_final` date DEFAULT NULL,
  `idCliente` varchar(80) DEFAULT NULL,
  `idOrdenServicio` int(11) DEFAULT NULL,
  `porc_descuento` decimal(12,2) DEFAULT NULL,
  `iva` decimal(12,2) DEFAULT NULL,
  `total_a_pagar` decimal(12,2) DEFAULT NULL,
  `estadoFactura` varchar(12) NOT NULL,
  `tipoFacturacion` varchar(10) DEFAULT NULL,
  `codigoVendedor` int(11) DEFAULT NULL,
  `subtotal` decimal(12,2) DEFAULT NULL,
  `observaciones` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idCabeza_Factura`),
  UNIQUE KEY `idCabeza_Factura` (`idCabeza_Factura`),
  KEY `FK_tbl_encabezado_proforma` (`idCliente`),
  KEY `FK_tbl_encabezado_proforma_orden_servicio` (`idOrdenServicio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_proforma` */

/*Table structure for table `tbl_proveedores` */

DROP TABLE IF EXISTS `tbl_proveedores`;

CREATE TABLE `tbl_proveedores` (
  `codProveedores` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEmpresa` varchar(40) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `nombreContacto` varchar(80) NOT NULL,
  `observaciones` varchar(130) DEFAULT NULL,
  `diasCredito` int(11) DEFAULT NULL,
  `telefono2` varchar(10) DEFAULT NULL,
  `telefono3` varchar(10) DEFAULT NULL,
  `idMoneda` int(11) DEFAULT NULL,
  `credi_saldo` decimal(12,2) DEFAULT NULL,
  `notific_cntc_pagos` varchar(2) DEFAULT NULL,
  `notific_cntc_pedidos` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`codProveedores`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_proveedores` */

/*Table structure for table `tbl_recibos` */

DROP TABLE IF EXISTS `tbl_recibos`;

CREATE TABLE `tbl_recibos` (
  `idRecibo` int(11) NOT NULL AUTO_INCREMENT,
  `fechaRecibo` datetime DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `cliente` varchar(40) DEFAULT NULL,
  `usuario` varchar(30) DEFAULT NULL,
  `tipoRecibo` varchar(2) DEFAULT NULL,
  `saldo_cuenta` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idRecibo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_recibos` */

/*Table structure for table `tbl_recibos_detalle` */

DROP TABLE IF EXISTS `tbl_recibos_detalle`;

CREATE TABLE `tbl_recibos_detalle` (
  `idDetallerecibo` int(11) NOT NULL AUTO_INCREMENT,
  `idRecibo` int(11) DEFAULT NULL,
  `idDocumento` int(11) DEFAULT NULL,
  `tipo` varchar(2) DEFAULT NULL,
  `referencia` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idDetallerecibo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_recibos_detalle` */

/*Table structure for table `tbl_tarjeta` */

DROP TABLE IF EXISTS `tbl_tarjeta`;

CREATE TABLE `tbl_tarjeta` (
  `idTarjeta` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTarjeta` varchar(50) NOT NULL,
  PRIMARY KEY (`idTarjeta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tarjeta` */

/*Table structure for table `tbl_tipo_cambio` */

DROP TABLE IF EXISTS `tbl_tipo_cambio`;

CREATE TABLE `tbl_tipo_cambio` (
  `idTipo_cabio` int(11) NOT NULL AUTO_INCREMENT,
  `idTipo_moneda` int(11) NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `valor_tipo_cambio` decimal(12,2) NOT NULL,
  `usar` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`idTipo_cabio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tipo_cambio` */

/*Table structure for table `tbl_tipo_documento` */

DROP TABLE IF EXISTS `tbl_tipo_documento`;

CREATE TABLE `tbl_tipo_documento` (
  `idTipo_documento` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(80) NOT NULL,
  `tipo_movimiento` varchar(40) NOT NULL,
  `estado` varchar(10) NOT NULL,
  PRIMARY KEY (`idTipo_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tipo_documento` */

insert  into `tbl_tipo_documento`(`idTipo_documento`,`descripcion`,`tipo_movimiento`,`estado`) values 
(1,'Pago de proveedor','Débito','Activo'),
(2,'Pago factura de cliente','Crédito','Activo');

/*Table structure for table `tbl_tipo_movimientos` */

DROP TABLE IF EXISTS `tbl_tipo_movimientos`;

CREATE TABLE `tbl_tipo_movimientos` (
  `codigoTipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  `acciones` varchar(10) NOT NULL,
  PRIMARY KEY (`codigoTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tipo_movimientos` */

/*Table structure for table `tbl_tramite_pago` */

DROP TABLE IF EXISTS `tbl_tramite_pago`;

CREATE TABLE `tbl_tramite_pago` (
  `idTramite_pago` int(11) NOT NULL AUTO_INCREMENT,
  `idProveedor` int(11) NOT NULL,
  `cantidad_monto_documento` decimal(12,2) DEFAULT NULL,
  `monto_saldo_pago` decimal(12,2) NOT NULL,
  `porcentaje_descuento` decimal(12,2) DEFAULT NULL,
  `monto_tramite_pagar` decimal(12,2) NOT NULL,
  `recibo_cancelacion` varchar(50) DEFAULT NULL,
  `email_proveedor` varchar(50) DEFAULT NULL,
  `prioridad_pago` varchar(10) NOT NULL,
  `usuario_registra` varchar(20) DEFAULT NULL,
  `fecha_registra` datetime DEFAULT NULL,
  `usuario_cancela` varchar(20) DEFAULT NULL,
  `fecha_cancela` datetime DEFAULT NULL,
  `usuario_aplica` varchar(20) DEFAULT NULL,
  `fecha_aplica` datetime DEFAULT NULL,
  `detalle` varchar(120) DEFAULT NULL,
  `detalle_reverso` varchar(120) DEFAULT NULL,
  `detalle_anulacion` varchar(120) DEFAULT NULL,
  `documento_banco` varchar(120) DEFAULT NULL,
  `observaciones` varchar(120) DEFAULT NULL,
  `estado_tramite` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTramite_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tramite_pago` */

/*Table structure for table `tbl_vehiculos` */

DROP TABLE IF EXISTS `tbl_vehiculos`;

CREATE TABLE `tbl_vehiculos` (
  `placa` int(8) NOT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `anio` int(11) NOT NULL,
  `modelo` varchar(80) NOT NULL,
  `marca` varchar(80) NOT NULL,
  `combustible` varchar(20) NOT NULL,
  `motor` varchar(50) NOT NULL,
  `cilindrada_motor` varchar(40) NOT NULL,
  `version` varchar(40) NOT NULL,
  `periodo_en_manten` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`placa`),
  KEY `FK_tbl_vehiculos` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_vehiculos` */

/*Table structure for table `tbl_version` */

DROP TABLE IF EXISTS `tbl_version`;

CREATE TABLE `tbl_version` (
  `codVersion` varchar(10) NOT NULL,
  PRIMARY KEY (`codVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_version` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_usuario` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_code` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sesion` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`nombre`,`codigo`,`tipo_usuario`,`verification_code`,`sesion`) values 
(1,'admin00','o6RaN6aCt_6bvQy-4nnrpKWTcwnyPpBQ','$2y$13$TJLymtoaNNGihwtxZgj9Keac5PW1FeA07.GOQT1ciFLY4DmmHGV4m',NULL,'info@neluxps.com',10,1445293540,1447790078,'Administrador del Sistema','1','Administrador','15628465',0);

/* Trigger structure for table `tbl_clientes` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `Eliminar_Usuario_Cliente` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `Eliminar_Usuario_Cliente` BEFORE DELETE ON `tbl_clientes` FOR EACH ROW BEGIN
	DELETE USER FROM USER WHERE user.username = OLD.identificacion;
    END */$$


DELIMITER ;

/* Trigger structure for table `tbl_funcionario` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `AgregarMecanico` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `AgregarMecanico` AFTER INSERT ON `tbl_funcionario` FOR EACH ROW BEGIN
IF (NEW.tipoFuncionario = 'Mecánico') THEN
INSERT INTO tbl_mecanicos (nombre,direccion,telefono,porcentComision,codigo) 
VALUE (CONCAT (NEW.nombre,' ',NEW.apellido1, ' ', NEW.apellido2),'',NEW.telefono,0,NEW.idFuncionario);
     END IF;
   END */$$


DELIMITER ;

/* Trigger structure for table `tbl_funcionario` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `Actualizar_Usuario_mecanic` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `Actualizar_Usuario_mecanic` AFTER UPDATE ON `tbl_funcionario` FOR EACH ROW BEGIN
UPDATE USER 
SET username = NEW.username, email = NEW.email, nombre = CONCAT (NEW.nombre,' ',NEW.apellido1, ' ', NEW.apellido2), tipo_usuario = NEW.tipo_usuario, codigo = NEW.estado
WHERE username = OLD.username;
IF (NEW.tipoFuncionario = 'Mecánico') THEN
IF EXISTS (SELECT * FROM tbl_mecanicos WHERE codigo = NEW.idFuncionario) THEN
UPDATE tbl_mecanicos
SET nombre = CONCAT (NEW.nombre,' ',NEW.apellido1, ' ', NEW.apellido2), telefono = NEW.telefono 
WHERE codigo = OLD.idFuncionario; 
ELSE 
INSERT INTO tbl_mecanicos (nombre,direccion,telefono,porcentComision,codigo) 
VALUE (CONCAT (NEW.nombre,' ',NEW.apellido1, ' ', NEW.apellido2),'',NEW.telefono,0,NEW.idFuncionario);
END IF;
ELSE 
DELETE FROM tbl_mecanicos WHERE codigo = OLD.idFuncionario;
END IF;
   END */$$


DELIMITER ;

/* Trigger structure for table `tbl_funcionario` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `Eliminar_Usuario_Mecanic` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `Eliminar_Usuario_Mecanic` BEFORE DELETE ON `tbl_funcionario` FOR EACH ROW BEGIN
	DELETE `user` FROM `user` WHERE user.username = OLD.username;
	DELETE tbl_mecanicos FROM tbl_mecanicos WHERE tbl_mecanicos.codigo = OLD.idFuncionario;
    END */$$


DELIMITER ;

/* Trigger structure for table `tbl_marcas` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `ActualizarMarca` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `ActualizarMarca` AFTER UPDATE ON `tbl_marcas` FOR EACH ROW BEGIN
    
	SET @nuevamarca = NEW.codMarcas;
	set @viejamarca = OLD.codMarcas;
	
	UPDATE tbl_modelo
	SET codMarca = @nuevamarca 
	WHERE codMarca = @viejamarca;
	
	UPDATE tbl_vehiculos
	SET marca = @nuevamarca 
	WHERE marca = @viejamarca;
	
    END */$$


DELIMITER ;

/* Trigger structure for table `tbl_modelo` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `ActualizarModelo` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `ActualizarModelo` AFTER UPDATE ON `tbl_modelo` FOR EACH ROW BEGIN
    
	SET @nuevomodelo = NEW.codModelo;
	SET @viejomodelo = OLD.codModelo;
	
	UPDATE tbl_vehiculos
	SET modelo = @nuevomodelo 
	WHERE modelo = @viejomodelo;
    END */$$


DELIMITER ;

/* Trigger structure for table `tbl_orden_servicio` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `ActualizarPrefactura_tbl_ancabezado_fact` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `ActualizarPrefactura_tbl_ancabezado_fact` AFTER UPDATE ON `tbl_orden_servicio` FOR EACH ROW BEGIN
	  /*  IF (tbl_orden_servicio.idCliente = 0) THEN
		update tbl_encabezado_factura
		SET idCliente = NEW.idCliente, total_a_pagar = NEW.montoServicio
		where idCliente = 0;*/
	   ## else 
		UPDATE tbl_encabezado_factura
		SET idCliente = NEW.idCliente -- , total_a_pagar = NEW.montoServicio
		WHERE tbl_encabezado_factura.idOrdenServicio = OLD.idOrdenServicio;
	    ##END IF;
    END */$$


DELIMITER ;

/* Trigger structure for table `tbl_version` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `ActualizarVersion` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `ActualizarVersion` AFTER UPDATE ON `tbl_version` FOR EACH ROW BEGIN
	
	SET @nuevaversion = NEW.codVersion;
	SET @viejaversion = OLD.codVersion;
	
	UPDATE tbl_vehiculos
	SET version = @nuevaversion 
	WHERE version = @viejaversion;
	
    END */$$


DELIMITER ;

/* Trigger structure for table `user` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `IgresarDerechoAdministrador` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `IgresarDerechoAdministrador` AFTER INSERT ON `user` FOR EACH ROW BEGIN
      IF (NEW.tipo_usuario = 'Administrador') THEN
	INSERT INTO rol_operacion (rol_id,operacion_id) 
	VALUE (NEW.id, '12'),
	      (NEW.id, '13'),
	      (NEW.id, '14'),
	      (NEW.id, '15');
      END IF;
    END */$$


DELIMITER ;

/* Trigger structure for table `user` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `ActualizarDerechoAdministrador` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `ActualizarDerechoAdministrador` AFTER UPDATE ON `user` FOR EACH ROW BEGIN
	IF (NEW.tipo_usuario = 'Administrador') THEN
	
	DELETE rol_operacion FROM rol_operacion
	WHERE rol_id = OLD.id
	AND operacion_id < 16
	AND operacion_id > 11;
	
	INSERT INTO rol_operacion (rol_id,operacion_id) 
	VALUE (OLD.id, '12'),
	      (OLD.id, '13'),
	      (OLD.id, '14'),
	      (OLD.id, '15');
      END IF;
      if (NEW.tipo_usuario = 'Operador') THEN
	DELETE rol_operacion FROM rol_operacion
	WHERE rol_id = OLD.id
	and operacion_id < 16
	and operacion_id > 11;
      end if;
    END */$$


DELIMITER ;

/* Trigger structure for table `user` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `EliminarDerechoUsuarios` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `EliminarDerechoUsuarios` AFTER DELETE ON `user` FOR EACH ROW BEGIN
	DELETE rol_operacion FROM rol_operacion
	WHERE rol_operacion.rol_id = OLD.id;
    END */$$


DELIMITER ;

/*!50106 set global event_scheduler = 1*/;

/* Event structure for event `actualice_estado_cuenta_cliente` */

/*!50106 DROP EVENT IF EXISTS `actualice_estado_cuenta_cliente`*/;

DELIMITER $$

/*!50106 CREATE DEFINER=`root`@`localhost` EVENT `actualice_estado_cuenta_cliente` ON SCHEDULE EVERY 2 MINUTE STARTS '2016-10-01 10:57:00' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
	    UPDATE tbl_clientes SET estadoCuenta = 'Cerrada' WHERE idCliente IN (SELECT idCliente FROM tbl_encabezado_factura WHERE estadoFactura = 'Crédito' COLLATE utf8_bin AND fecha_vencimiento = CURDATE());
	END */$$
DELIMITER ;

/* Event structure for event `eliminar_etiquetas` */

/*!50106 DROP EVENT IF EXISTS `eliminar_etiquetas`*/;

DELIMITER $$

/*!50106 CREATE DEFINER=`root`@`localhost` EVENT `eliminar_etiquetas` ON SCHEDULE EVERY 120 MINUTE STARTS '2016-10-01 10:57:00' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
	    DELETE FROM tbl_etiquetas WHERE fecha_reg < CURDATE();
	END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
