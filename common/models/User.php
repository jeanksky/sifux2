<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use backend\models\Rol;
use yii\web\UploadedFile;
use backend\models\Empresa;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $verification_code;


    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

        public $oldpass;
        public $newpass;
        public $repeatnewpass;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['oldpass','newpass','repeatnewpass'],'required'],
            ['newpass', 'match', 'pattern' => "/^.{4,16}$/", 'message' => 'Mínimo 4 y máximo 16 palabras'],
            ['oldpass','findPasswords'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass','message' => 'Las contraseñas no coinciden'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function validatePasswordAdmin($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }


    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    public function setPassword2($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    public function generateAuthKey2()
    {
        return Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getRol()
    {
        return $this->hasOne(Rol::className(), ['id' => 'rol_id']);
    }
    public function getImageurl()
        {
        $sql = "SELECT logo FROM tbl_empresa WHERE idEmpresa = :idE";
        $command = \Yii::$app->db->createCommand($sql);
           $command->bindValue(":idE",1);
           $result = $command->queryOne();
           return Yii::$app->urlManagerBackend->baseUrl.'/'.$result['logo'];

        }

        public function getFavicon()
        {
        return \Yii::$app->urlManagerBackend->baseUrl.'/uploads/favicon.ico';
        }

        public function getCambioContrasena()
        {
        return \Yii::$app->urlManagerBackend->baseUrl.'/index.php?r=site/recoverpass.php';
        }


        public static function Cliente()
        {

           if(!Yii::$app->user->isGuest){
              return $idCliente = Yii::$app->user->identity->username;
            } else {
               echo "";
            } 
        }
        
         public function findPasswords($attribute, $params){
            $u = new User();
            $user = User::find()->where(['username'=>Yii::$app->user->identity->username])->one();
                $passwordDB = $user->password_hash;
                if(!Yii::$app->security->validatePassword($this->oldpass, $passwordDB))//Compara la contraseña con la que esta en la base de datos
                    $this->addError($attribute,'Antigua contraseña es incorrecta');
        }

        public function updatePassword($newpass) 
        {
            $this->password_hash = Yii::$app->security->generatePasswordHash($newpass);
          }

          public function verifyPassword($password)
        {
            $dbpassword = static::findOne(['username' => Yii::$app->user->identity->username, 'status' => self::STATUS_ACTIVE])->password_hash;
             return Yii::$app->security->validatePassword($password, $dbpassword);
        }


        public function attributeLabels(){
            return [
                'oldpass'=>'Antigua contraseña',
                'newpass'=>'Nueva contraseña',
                'repeatnewpass'=>'Repita la nueva contraseña',
            ];
        }


}
