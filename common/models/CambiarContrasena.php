<?php

namespace common\models;
/*vehiel 15/12/2018
modelo de validacion del form para cambiar la contraseña*/
use Yii;
use yii\base\Model;

class CambiarContrasena extends model{

  public $new_password;
  public $repeat;
  public $password;
  public $username;
  private $_user = false;
  // public $email;

    public function rules()
    {
        return [
            [['new_password', 'password','username','repeat'], 'required'],
            /*la nueva contraseña no puede ser mayoy de 20 caracteres*/
            ['new_password','string','max'=>20],
            /*al validar el modelo se verifica la contraseña con el metodo validatePassword*/
            ['password','validatePassword'],
            /*el new_password y repeat deben ser iguales*/
            ['repeat','compare','compareAttribute'=>'new_password', 'operator'=> '==']
        ];
    }

    public function attributeLabels()
    {
        return [
            'new_password' => Yii::t('app', 'Contraseña Nueva'),
            'password' => Yii::t('app', 'Contraseña Actual'),
            'username' => Yii::t('app', 'Usuario'),
            'repeat' => Yii::t('app', 'Repita Contraseña Nueva'),
        ];
    }
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /*busca el modelo con el username por medio del metodo getUser*/
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
              /*valida si existe el usuario y comprueba la contraseña*/
                $this->addError($attribute, Yii::t('app','Usuario o contraseña incorrecto.'));
            }
        }
    } /* fin validatePassword*/

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }
        return $this->_user;
    } /* fin getUser*/
}

?>
