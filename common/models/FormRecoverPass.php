<?php

namespace common\models;
use Yii;
use yii\base\Model;

class FormRecoverPass extends model{

    public $email;
    public $new_password;
    public $repeat;
    public $ramkey;
    public $username;
    private $_user = false;

    public function rules()
    {
        return [
            [['username'],'required'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'Formato no válido'],
            [['new_password','username','repeat','ramkey'], 'required', 'when'=> function($model){
                return $model->email == '';
              }, 'whenClient' => "function (attribute, value) {
                   return $('#formrecoverpass-email').val() == '';
                 }"],
              /*la nueva contraseña no puede ser mayoy de 20 caracteres*/
              ['new_password','string','max'=>20],
              /*al validar el modelo se verifica la contraseña con el metodo validatePassword*/
              ['ramkey','validateCodigo'],
              /*el new_password y repeat deben ser iguales*/
              ['repeat','compare','compareAttribute'=>'new_password', 'operator'=> '==']
        ];
    }
    public function attributeLabels()
    {
        return [
            'new_password' => Yii::t('app', 'Contraseña Nueva'),
            // 'password' => Yii::t('app', 'Contraseña Actual'),
            'username' => Yii::t('app', 'Usuario'),
            'repeat' => Yii::t('app', 'Repita Contraseña Nueva'),
            'email' => Yii::t('app', 'Correo'),
            'ramkey'=>Yii::t('app','Código')
        ];
    }
    public function validateCodigo($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /*busca el modelo con el username por medio del metodo getUser*/
            $user = $this->getUser();
            // var_dump($user);
            // exit();
            // echo "bueno: ".$user->verification_code;
            // try{
              if ($user == NULL || $this->ramkey != $user['verification_code']) {
                /*valida si existe el usuario y comprueba la contraseña*/
                  $this->addError($attribute, Yii::t('app','Usuario o código incorrecto.'));
              }
            // }catch (\Exception $e) {
            //     $this->addError($attribute, Yii::t('app','Código inválido.'));
            //   }

        }
    } /* fin validateCodigo*/

    public function getUser()
    {
        if ($this->_user === false) {
          /*$sql = "SELECT * user WHERE username = :username;";
          $command = \Yii::$app->db->createCommand($sql);
          $command->bindParam(":username", $username);
          $this->_user = $command->execute();*/
            $this->_user = User::find()->where(['username'=>$this->username])->asArray()->one();

        }
        return $this->_user;
    } /* fin getUser*/
}
