<?php
namespace common\models;

use Yii;
use yii\base\Model;
use backend\models\Usuarios;
use backend\models\Empresa;
use yii\helpers\Html;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;
//Para la sesiones
//https://github.com/yiisoft/yii2/blob/master/docs/guide-es/runtime-sessions-cookies.md
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        $empresa = Empresa::find()->where(['idEmpresa'=>1])->one();
        $maximo_sesion = $empresa->sesiones_permitidas;
        $cantidad_sesion = 0;
        $usuarios = Usuarios::find()->where(['!=','tipo_usuario', 'Cliente'])->all();
        foreach($usuarios as $usuario) {
            if ($usuario['sesion']==1 && $usuario['id']!=1) {
                $cantidad_sesion += 1;
            }
        }

        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Usuario o password incorrectos.');
            }
            else if($user->codigo == '0') {
                $this->addError($attribute, 'Este usuario esta inactivo.');
            }
            else if($user->tipo_usuario == 'Cliente') {
                $this->addError($attribute, 'Este acceso no es para clientes.');
            }
          /*  else if($user->sesion == 1 && $user->id != 1) {
                $this->addError($attribute, 'Lutux detecta que este usuario tiene una sesión activa en otro navegador. NO SE PREOCUPE. Usted mismo puede restaurar sesión '
                .Html::a('Haciendo clic aqui.', ['/site/recoverpass'], ['class'=>'']));
            }
            else if ($cantidad_sesion >= $maximo_sesion && $user->id != 1) {
                $this->addError($attribute, 'Su paquete de lutux ya alcanzó el máximo de usuarios activos, si desea mayor cantidad de usuarios activos ponte en contacto con nelux.');
            }*/
            else if ($empresa->estado_producto=='inactivo'){
                $this->addError($attribute, 'Su paquete de lutux está vencido, favor contactar con soporte técnico.');
            }
        }

    }

   /* public function validatePasswordAdmin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user->validatePasswordAdmin($this->password)) {
                $this->addError($attribute, 'Password incorrecto.');
            }
            if($user->codigo == '0') {
            $this->addError($attribute, 'Este usuario esta inactivo.');
            }
            if($user->tipo_usuario != 'Administrador') {
            $this->addError($attribute, 'Esta clave no pertenece a un administrador.');
            }
        }

    }*/

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {

        if ($this->validate()) {
            //return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 0 * 0 * 0 : 0);
        } else {
            return false;
        }

    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
