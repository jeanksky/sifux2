<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginFormc extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;
//Para la sesiones
//https://github.com/yiisoft/yii2/blob/master/docs/guide-es/runtime-sessions-cookies.md
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Usuario o password incorrectos.');
            }
            else if($user->codigo == '0') {
            $this->addError($attribute, 'Este usuario esta inactivo.');
            }
            else if($user->tipo_usuario == 'Administrador' || $user->tipo_usuario == 'Operador') {
            $this->addError($attribute, 'Este acceso es solo para clientes.');
            }
        }
    
    }

   /* public function validatePasswordAdmin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user->validatePasswordAdmin($this->password)) {
                $this->addError($attribute, 'Password incorrecto.');
            }
            if($user->codigo == '0') {
            $this->addError($attribute, 'Este usuario esta inactivo.');
            }
            if($user->tipo_usuario != 'Administrador') {
            $this->addError($attribute, 'Esta clave no pertenece a un administrador.');
            }
        }
    
    }*/

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
