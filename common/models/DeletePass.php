<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class DeletePass extends Model
{
    public $username;
    public $password;

    public function rules()
    {
        return [
            // username and password are both required
            //[['username', 'password'], 'required'],
            ['username', 'required', 'message' => 'Se requiere usuario Administrador.'],
            ['password', 'required', 'message' => 'Se requiere una contraseña.'],
            ['password', 'filtAdmin'],
        ];
    }
    
    public function filtAdmin($attribute, $params)
    {
        $user = User::findByUsername($this->username);
        if(!$user || !$user->validatePassword($this->password)) {
            $this->addError('password', 'Usuario o contraseña incorrecto.');
            return true;
        } 
        else if($user->tipo_usuario != 'Administrador') {
            $this->addError($attribute, 'El usuario ingresado no es Administrador');
            return true;
            }
        else { return false;}
    }

}
