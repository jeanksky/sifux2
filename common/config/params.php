<?php
return [
    'adminEmail' => 'info@neluxps.com',
    'supportEmail' => 'info@neluxps.com',
    'user.passwordResetTokenExpire' => 3600,
];
